/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _VITISTHREAD_H_
#define _VITISTHREAD_H_
#ifndef NULL
#define NULL 0
#endif

#include <stdio.h>
#ifndef WIN32
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#endif
#include <vector>
#include "externDll.h"
// ----------------------------------------------------------------------------
// constants
// ----------------------------------------------------------------------------

enum vtsMutexError
{
	vtsMUTEX_NO_ERROR = 0,
	vtsMUTEX_DEAD_LOCK,      // Mutex has been already locked by THE CALLING thread
	vtsMUTEX_BUSY,           // Mutex has been already locked by ONE thread
	vtsMUTEX_UNLOCKED,
	vtsMUTEX_MISC_ERROR
};

enum vtsThreadError
{
	vtsTHREAD_NO_ERROR = 0,      // No error
	vtsTHREAD_NO_RESOURCE,       // No resource left to create a new thread
	vtsTHREAD_RUNNING,           // The thread is already running
	vtsTHREAD_NOT_RUNNING,       // The thread isn't running
	vtsTHREAD_KILLED,            // Thread we waited for had to be killed
	vtsTHREAD_MISC_ERROR         // Some other error
};

enum vtsThreadKind
{
	vtsTHREAD_DETACHED,
	vtsTHREAD_JOINABLE
};

// defines the interval of priority
enum
{
	VTSTHREAD_MIN_PRIORITY      = 0u,
	VTSTHREAD_DEFAULT_PRIORITY  = 50u,
	VTSTHREAD_MAX_PRIORITY      = 100u
};

// ----------------------------------------------------------------------------
// A mutex object is a synchronization object whose state is set to signaled
// when it is not owned by any thread, and nonsignaled when it is owned. Its
// name comes from its usefulness in coordinating mutually-exclusive access to
// a shared resource. Only one thread at a time can own a mutex object.
// ----------------------------------------------------------------------------

// you should consider vtsMutexLocker whenever possible instead of directly
// working with vtsMutex class - it is safer
class vtsMutexInternal;
class VITIS_EXPORT vtsMutex
{
	public:
		// constructor & destructor
		vtsMutex();
		~vtsMutex();

		// Lock the mutex.
		vtsMutexError Lock();
		// Try to lock the mutex: if it can't, returns immediately with an error.
		vtsMutexError TryLock();
		// Unlock the mutex.
		vtsMutexError Unlock();

		// Returns true if the mutex is locked.
		bool IsLocked() const { return (m_locked > 0); }

	protected:
		friend class vtsCondition;

		// no assignment operator nor copy ctor
		vtsMutex(const vtsMutex&);
		vtsMutex& operator=(const vtsMutex&);

		int m_locked;
		vtsMutexInternal *m_internal;
};

// a helper class which locks the mutex in the ctor and unlocks it in the dtor:
// this ensures that mutex is always unlocked, even if the function returns or
// throws an exception before it reaches the end
class VITIS_EXPORT vtsMutexLocker
{
	public:
		// lock the mutex in the ctor
		vtsMutexLocker(vtsMutex& mutex) : m_mutex(mutex)
		{ m_isOk = m_mutex.Lock() == vtsMUTEX_NO_ERROR; }

		// returns TRUE if mutex was successfully locked in ctor
		bool IsOk() const
		{ return m_isOk; }

		// unlock the mutex in dtor
		~vtsMutexLocker()
		{ if ( IsOk() ) m_mutex.Unlock(); }

	private:
		// no assignment operator nor copy ctor
		vtsMutexLocker(const vtsMutexLocker&);
		vtsMutexLocker& operator=(const vtsMutexLocker&);

		bool     m_isOk;
		vtsMutex& m_mutex;
};

// ----------------------------------------------------------------------------
// Critical section: this is the same as mutex but is only visible to the
// threads of the same process. For the platforms which don't have native
// support for critical sections, they're implemented entirely in terms of
// mutexes.
//
// NB: wxCriticalSection object does not allocate any memory in its ctor
//     which makes it possible to have static globals of this class
// ----------------------------------------------------------------------------

class vtsCriticalSectionInternal;

// in order to avoid any overhead under platforms where critical sections are
// just mutexes make all CriticalSection class functions inline
#if !defined(WIN32)
#define VTSCRITICAL_INLINE   inline

#define vtsCRITSECT_IS_MUTEX 1
#else // Win
#define VTSCRITICAL_INLINE

#define vtsCRITSECT_IS_MUTEX 0
#endif // WIN32

// you should consider vtsCriticalSectionLocker whenever possible instead of
// directly working with vtsCriticalSection class - it is safer
class VITIS_EXPORT vtsCriticalSection
{
	public:
		// ctor & dtor
		VTSCRITICAL_INLINE vtsCriticalSection();
		VTSCRITICAL_INLINE ~vtsCriticalSection();

		// enter the section (the same as locking a mutex)
		VTSCRITICAL_INLINE void Enter();
		// leave the critical section (same as unlocking a mutex)
		VTSCRITICAL_INLINE void Leave();

	private:
		// no assignment operator nor copy ctor
		vtsCriticalSection(const vtsCriticalSection&);
		vtsCriticalSection& operator=(const vtsCriticalSection&);

#if vtsCRITSECT_IS_MUTEX
		vtsMutex m_mutex;
#elif defined(WIN32)
		// we can't allocate any memory in the ctor, so use placement new -
		// unfortunately, we have to hardcode the sizeof() here because we can't
		// include windows.h from this public header
		char m_buffer[24];
#else //Unix/Unix
		vtsCriticalSectionInternal *m_critsect;
#endif // WIN32
};

// keep your preprocessor name space clean
#undef VTSCRITICAL_INLINE

// vtsCriticalSectionLocker is the same to critical sections as vtsMutexLocker is
// to th mutexes
class VITIS_EXPORT vtsCriticalSectionLocker
{
	public:
		inline vtsCriticalSectionLocker(vtsCriticalSection& critsect);
		inline ~vtsCriticalSectionLocker();

	private:
		// no assignment operator nor copy ctor
		vtsCriticalSectionLocker(const vtsCriticalSectionLocker&);
		vtsCriticalSectionLocker& operator=(const vtsCriticalSectionLocker&);

		vtsCriticalSection& m_critsect;
};

// ----------------------------------------------------------------------------
// Condition variable: allows to block the thread execution until something
// happens (== condition is signaled)
// ----------------------------------------------------------------------------

class vtsConditionInternal;
class VITIS_EXPORT vtsCondition
{
	public:
		// constructor & destructor
		vtsCondition();
		~vtsCondition();

		// wait until the condition is signaled
		// waits indefinitely.
		void Wait();
		// waits until a signal is raised or the timeout elapses
		bool Wait(unsigned long sec, unsigned long nsec);

		// signal the condition
		// wakes up one (and only one) of the waiting threads
		void Signal();
		// wakes up all threads waiting onthis condition
		void Broadcast();

	private:
		vtsConditionInternal *m_internal;
};

// ----------------------------------------------------------------------------
// Thread class
// ----------------------------------------------------------------------------

// there are two different kinds of threads: joinable and detached (default)
// ones. Only joinable threads can return a return code and only detached
// threads auto-delete themselves - the user should delete the joinable
// threads manually.



class vtsThreadInternal;
class VITIS_EXPORT vtsThread
{
	public:
		// the return type for the thread function
		typedef void *ExitCode;

		// static functions
		// Returns the wxThread object for the calling thread. NULL is returned
		// if the caller is the main thread (but it's recommended to use
		// IsMain() and only call This() for threads other than the main one
		// because NULL is also returned on error). If the thread wasn't
		// created with wxThread class, the returned value is undefined.
		static vtsThread *This();

		// Returns true if current thread is the main thread.
		static bool IsMain();

		// Release the rest of our time slice leting the other threads run
		// static void Yield();

		// Sleep during the specified period of time in milliseconds
		//
		// NB: at least under MSW worker threads can not call ::wxSleep()!
		static void Sleep(unsigned long milliseconds);

		// get the number of system CPUs - useful with SetConcurrency()
		// (the "best" value for it is usually number of CPUs + 1)
		//
		// Returns -1 if unknown, number of CPUs otherwise
		static int GetCPUCount();

		// sets the concurrency level: this is, roughly, the number of threads
		// the system tries to schedule to run in parallel. 0 means the
		// default value (usually acceptable, but may not yield the best
		// performance for this process)
		//
		// Returns TRUE on success, FALSE otherwise (if not implemented, for
		// example)
		static bool SetConcurrency(size_t level);

		// constructor only creates the C++ thread object and doesn't create (or
		// start) the real thread
		vtsThread(vtsThreadKind kind = vtsTHREAD_DETACHED);

		// functions that change the thread state: all these can only be called
		// from _another_ thread (typically the thread that created this one, e.g.
		// the main thread), not from the thread itself

		// create a new thread - call Run() to start it
		vtsThreadError Create();

		// starts execution of the thread - from the moment Run() is called
		// the execution of wxThread::Entry() may start at any moment, caller
		// shouldn't suppose that it starts after (or before) Run() returns.
		vtsThreadError Run();

		// stops the thread if it's running and deletes the wxThread object if
		// this is a detached thread freeing its memory - otherwise (for
		// joinable threads) you still need to delete wxThread object
		// yourself.
		//
		// this function only works if the thread calls TestDestroy()
		// periodically - the thread will only be deleted the next time it
		// does it!
		//
		// will fill the rc pointer with the thread exit code if it's !NULL
		vtsThreadError Delete(ExitCode *rc = (ExitCode *)NULL);

		// waits for a joinable thread to finish and returns its exit code
		//
		// Returns (ExitCode)-1 on error (for example, if the thread is not
		// joinable)
		ExitCode Wait();

		// kills the thread without giving it any chance to clean up - should
		// not be used in normal circumstances, use Delete() instead. It is a
		// dangerous function that should only be used in the most extreme
		// cases!
		//
		// The wxThread object is deleted by Kill() if the thread is
		// detachable, but you still have to delete it manually for joinable
		// threads.
		vtsThreadError Kill();

		// pause a running thread: as Delete(), this only works if the thread
		// calls TestDestroy() regularly
		vtsThreadError Pause();

		// resume a paused thread
		vtsThreadError Resume();

		// priority
		// Sets the priority to "prio": see WXTHREAD_XXX_PRIORITY constants
		//
		// NB: the priority can only be set before the thread is created
		void SetPriority(unsigned int prio);

		// Get the current priority.
		unsigned int GetPriority() const;

		// thread status inquiries
		// Returns true if the thread is alive: i.e. running or suspended
		bool IsAlive() const;
		// Returns true if the thread is running (not paused, not killed).
		bool IsRunning() const;
		// Returns true if the thread is suspended
		bool IsPaused() const;

		// is the thread of detached kind?
		bool IsDetached() const { return m_isDetached; }

		// Get the thread ID - a platform dependent number which uniquely
		// identifies a thread inside a process
		unsigned long GetId() const;

		// called when the thread exits - in the context of this thread
		//
		// NB: this function will not be called if the thread is Kill()ed
		virtual void OnExit() { }

		// dtor is public, but the detached threads should never be deleted - use
		// Delete() instead (or leave the thread terminate by itself)
		virtual ~vtsThread();

	protected:
		// Returns TRUE if the thread was asked to terminate: this function should
		// be called by the thread from time to time, otherwise the main thread
		// will be left forever in Delete()!
		bool TestDestroy();

		// exits from the current thread - can be called only from this thread
		void Exit(ExitCode exitcode = 0);

		// entry point for the thread - called by Run() and executes in the context
		// of this thread.
		virtual void *Entry() = 0;

	private:
		// no copy ctor/assignment operator
		vtsThread(const vtsThread&);
		vtsThread& operator=(const vtsThread&);

		friend class vtsThreadInternal;

		// the (platform-dependent) thread class implementation
		vtsThreadInternal *m_internal;

		// protects access to any methods of wxThreadInternal object
		vtsCriticalSection m_critsect;

		// true if the thread is detached, false if it is joinable
		bool m_isDetached;
};


class VITIS_EXPORT vtsThreadModule
{
	public :

		static void Init();
		static void Exit();

};

// ----------------------------------------------------------------------------
// Automatic initialization
// ----------------------------------------------------------------------------

// macros for entering/leaving critical sections which may be used without
// having to take them inside "#if wxUSE_THREADS"
#define vtsENTER_CRIT_SECT(cs)   (cs).Enter()
#define vtsLEAVE_CRIT_SECT(cs)   (cs).Leave()
#define vtsCRIT_SECT_DECLARE(cs) static vtsCriticalSection cs
#define vtsCRIT_SECT_LOCKER(name, cs)  vtsCriticalSectionLocker name(cs)

	inline
vtsCriticalSectionLocker:: vtsCriticalSectionLocker(vtsCriticalSection& cs)
	: m_critsect(cs) { m_critsect.Enter(); }
	inline
	vtsCriticalSectionLocker::~vtsCriticalSectionLocker() { m_critsect.Leave(); }


#ifndef WIN32
inline vtsCriticalSection::vtsCriticalSection()
{
}

inline vtsCriticalSection::~vtsCriticalSection()
{
}

inline void vtsCriticalSection::Enter()
{
	(void)m_mutex.Lock();
}

inline void vtsCriticalSection::Leave()
{
	(void)m_mutex.Unlock();
}
#endif

#endif //_VITIS_THREAD
