/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file DecompAxeLevel.h
///			\brief Definition of DecompAxeLevel.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _DECOMPAXELEVEL_H
#define _DECOMPAXELEVEL_H
#include "defVitis.h"
#include "externDll.h"
#include <vector>
#include "ArchivableObject.h"
using namespace Vitis;
using std::vector;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class DecompAxeLevel
///			\brief Decomposition structure of an axe
///
///			Decomposition structure of an axe \n
///			It provides with convenient tools to seek across an axe description.
///			\warning Each axe and his sub-structure must be DecompAxeLevel specialized class
///			\warning It is recommended to generate plant component through a PlantBuilder instance instead of \n
///			using the constructor members. PlantBuilder provides with facilities to automatically insert plant \n
///			components into an axe decomposition and to create custom DecompAxeLevel.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT DecompAxeLevel: public ArchivableObject
{

	protected:

		/// \brief Vector for saving the current index at this level before seeking into this
		vector<int> dposit;

		/// \brief Level of decomposition
		short currentLevel;

		/// \brief The current number of sub-structure inside this structure level
		int sizeElement;

		/// \brief The current index of seeking save index in \e dposit
		short currentGoto;

		/// \brief The current index sub-strcuture in \ss (next decomposition level)
		int currentIndex;

		/// \brief Vector of sub-structures that the current level consists in
		/// \warning this vector must contain only substructures with ss[i]->currentLevel = this->currentLevel + 1
		vector<DecompAxeLevel *>  ss;

		/// \brief Container of the current DecompAxeLevel (NULL at upper level)
		DecompAxeLevel * parent;

		/// \brief unique id for this decompaxeLevel into the plant
		int id;

	public:

		/// \brief Constructor
		/// \param clevel The current level of decomposition
		///	\warning It is recommended to generate plant component through a PlantBuilder instance instead of \n
		///	using the constructor members. PlantBuilder provides with facilities to automatically insert plant \n
		///	components into an axe decomposition and to create specialized DecompAxeLevel.
		DecompAxeLevel(USint clevel=0);

		/// \brief Constructor
		/// \param clevel The current level of decomposition
		/// \param Plant The current plant
		///	\warning It is recommended to generate plant component through a PlantBuilder instance instead of \n
		///	using the constructor members. PlantBuilder provides with facilities to automatically insert plant \n
		///	components into an axe decomposition and to create specialized DecompAxeLevel.
		DecompAxeLevel(class Plant * p, USint clevel=0);

		/// \brief Clone Constructor
		/// \param groc The DecompAxeLevel to clone
		DecompAxeLevel(DecompAxeLevel * groc);

		/// \brief Destructor
		/// \warning Delete the current instance and all sub-structure instance
		virtual ~DecompAxeLevel();

		/// \brief Backup current position and sub-levels current position
///
		/// Backup current position and sub-levels current position
		virtual void startPosit();

		/// \brief Restore backup position and sub-levels position
///
		/// Restore backup position and sub-levels position
		virtual void endPosit();

		/// \brief Accessor to the bearer plant
///
		/// Accessor to the bearer plant
		/// \return The pointer to the bearer plant
		class Plant * getPlant ();

		/// \brief Accessor to the vector of sub-structures that the current level consists in
///
		/// Accessor to the vector of sub-structures that the current level consists in
		vector<DecompAxeLevel *>  Ss() {return ss;};

		/// \brief Accessor to the container of the current DecompAxeLevel (NULL at upper level)
///
		/// Accessor to the container of the current DecompAxeLevel (NULL at upper level)
		DecompAxeLevel * Parent(){return parent;};;

		/// \brief Accessor to the current index sub-strcuture in \ss (next decomposition level)
///
		/// Accessor to the current index sub-strcuture in \ss (next decomposition level)
		int CurrentIndex() {return currentIndex;};

		/// \brief Add a sub-structure at end of current corresponding level of decomposition
///
		/// Add a sub-structure at end of current corresponding level of decomposition
		/// \param declevl The sub-structure to add at level declevl->currentLevel
		/// \warning \e declevl->currentLevel must be > this->currentLevel
		/// \warning this function must be called outside any position section (see startPosit(), endPosit())
		void addSubElementAtLevel(DecompAxeLevel * declevl, int _index=0);

		/// \brief Remove a sub-structure of current corresponding level of decomposition
///
		/// Remove a sub-structure of current corresponding level of decomposition
		/// \param declevl The sub-structure to remove at level declevl->currentLevel
		/// \warning \e declevl->currentLevel must be > this->currentLevel
		/// \warning this function must be called outside any position section (see startPosit(), endPosit())
		/// \return 1 if success, 0 otherwise
		int removeSubElementAtLevel(DecompAxeLevel * declevl);

		/// \brief Get the current sub-structure at the level \e level
///
		/// Get the current sub-structure at the level \e level
		/// \warning \e level must be > currentLevel
		DecompAxeLevel * getCurrentElementOfSubLevel (int level=-1);

		/// \brief Get the parent at the level \e level
///
		/// Get the parent at the level \e level
		/// \warning \e level must be < currentLevel
		DecompAxeLevel * getParentOfLevel (int level=-1);

		/// \brief Get the current index of sub-structure at the level \e level
///
		/// Get the current index of sub-structure at the level \e level
		/// \warning \e level must be > currentLevel
		int getCurrentElementIndexOfSubLevel(int level=-1);

		/// \brief Get the current element number of sub-structure at the level \e level
///
		/// Get the current element number of sub-structure at the level \e level
		/// \warning \e level must be > currentLevel
		int getElementNumberOfSubLevel(int level=-1);

		/// \brief Position on the first sub-structure element at the level \e level
///
		/// Position on the first sub-structure element at the level \e level
		/// \return 1 if success, 0 otherwise
		/// \warning \e level must be > currentLevel
		USint positOnFirstElementOfSubLevel(int level=-1);

		/// \brief Position on the last sub-structure element at the level \e level
///
		/// Position on the last sub-structure element at the level \e level
		/// \return 1 if success, 0 otherwise
		/// \warning \e level must be > currentLevel
		USint positOnLastElementOfSubLevel(int level=-1);

		/// \brief Position on the next sub-structure element at the level \e level
///
		/// Position on the next sub-structure element at the level \e level
		/// \return 1 if success , 0 otherwise ( 0 if we have overstep the element number of current sublevel)
		/// \warning \e level must be > currentLevel
		USint positOnNextElementOfSubLevel(int level=-1);

		/// \brief Position on the previous sub-structure element at the level \e level
///
		/// Position on the previous sub-structure element at the level \e level
		/// \return 1 if success , 0 otherwise ( 0 if we have overstep the elements number of current sublevel)
		/// \warning \e level must be > currentLevel
		USint positOnPreviousElementOfSubLevel(int level=-1);

		/// \brief Set position on the sub-structure element to position \epos for decomposition level \elevel counted into this->currentLevel
///
		/// Set position on the sub-structure element to position \epos for decomposition level \elevel counted into this->currentLevel
		/// \param pos 0 to size-1 of element at given sublevel \elevel into this->currentLevel
		/// \return 1 if success , 0 otherwise ( 0 if we have overstep the elements number of current sublevel)
		/// \warning \e level must be > currentLevel
		USint positOnElementAtSubLevel(int pos, int level=-1);


		/// \brief return true if this structure doesn't have sub-structure, false if not
///
		/// return true if this structure doesn't have sub-structure, false if not
		bool empty();

		/// \brief return The current level of this structure
///
		/// return The current level of this structure
		inline short getLevel (){return currentLevel;}

		/** \brief Serialize this DecompAxeLevel**/
///
		/** Serialize this DecompAxeLevel**/
		/**	\param ar A reference to the archive \see Archive **/
		virtual void serialize(Archive& ar );

		virtual DecompAxeLevel *getDecompAxeLevel (int id);

		virtual std::string getName();

		inline int getId() {return id;};
		inline void setId (int i) {id = i;};

        vector<int> getdposit(){return dposit;};


	private:

		/// \brief if _index == 0 || _index >= sizeElement Add a sub-structure at the end.
		/// if _index != 0 && _index < sizeElement insert a sub-structure at the position of _index if exist.
		void addElement (DecompAxeLevel * elem, int _index=0);

		/// \brief Remove a sub-structure
		/// \return 1 if success and current level remains not empty, -1 if success and current level remains empty, 0 otherwise
		int removeElement (DecompAxeLevel * elem);

};

#endif
