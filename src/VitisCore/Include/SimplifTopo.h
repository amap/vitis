/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file SimplifTopo.h
///			\brief Definition of ListeBr and SimplifTopo.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __SIMPLIFTOPO__
#define __SIMPLIFTOPO__
#include "Branc.h"
#include <map>
#include <vector>
#include <string>



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class ListeBr
///			\brief The list of Branc instances which belong to the same simplification criteria
///
///			The list of Branc instances which belong to the same simplification criteria
///			\see MapCrit.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class ListeBr:public VitisObject
{
	public:

		/// \brief The vector of pointer to the Branc instances ( multiple possible instances to preserve some variability)
		std::vector<Branc *> brancPtr;

		/// \brief Constructor
		ListeBr(Branc * b=NULL);

		/// \brief Destructor
		virtual ~ListeBr();

};


/// \brief A map which associates a critere (string) with a list of Branc instance
VITIS_EXPORT typedef std::map<std::string ,ListeBr *> MapCrit;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class SimplifTopo
///			\brief Manage the construction of Branc classes according to alphanumerical criterias
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT SimplifTopo:public VitisObject
{


	protected:
		/// \brief The map which associates a critere (string) with a list of Branc instance
		MapCrit listeCrit ;

		/// \brief Pointer to the mother plant
		class Plant * v_plt;

	public:

		/// \brief Constructor
		SimplifTopo(class Plant * plt);

		/// \brief Destructor
		virtual ~SimplifTopo(void);

		/// \brief Add the given Branc and his associated criteria to the listeCrit map
		///
		/// Add the given Branc and his associated criteria to the listeCrit map
		/// \param b pointer to the branc
		/// \param critere a string which represents the criteria
		void updateListe(Branc * b,const std::string & critere);

		/// \brief Return the branc at index indexInCriteria in the list of Branc associated to the criteria critere
		///
		/// \return the branc at index \e indexInCriteria in the list of Branc associated to the criteria \e critere
		virtual Branc * getSimplifiedBranc(const std::string & critere, int indexInCriteria=0);

		/// \brief Remove the given Branc and his associated criteria from the listeCrit map
		///
		/// Remove the given Branc and his associated criteria from the listeCrit map
		/// \param b pointer to the branc
		/// \param critere the string which represents the criteria
		void removeSimplifiedBranc(Branc *b, const std::string & critere);


};


#endif

