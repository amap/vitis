/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _MATRIX_H__
#define _MATRIX_H__

#include "ArchivableObject.h"
#include "Vector.h"

/*  --------------------------------------------------------------------- */
namespace Vitis {
    /*!

      \class Matrix4

      \brief A 4x4 matrix stored in row-major order.

*/

    class VITIS_EXPORT Matrix4 : public VitisObject {

    public:


        /// The identity Matrix4.
        static const Matrix4 IDENTITY;

        double length;



        /** Constructs an identity Matrix4 with the values \e m0, \e m1, \e m2, \e m3,
          \e m4, \e m5,\e m6, \e m7, \e m8, \e m9, \e m10, \e m11, \e m12,
          \e m13, \e m14 and \e m15. */
        explicit Matrix4(const float &m0 = 1, const float &m1 = 0,
                         const float &m2 = 0, const float &m3 = 0,
                         const float &m4 = 0, const float &m5 = 1,
                         const float &m6 = 0, const float &m7 = 0,
                         const float &m8 = 0, const float &m9 = 0,
                         const float &m10 = 1, const float &m11 = 0,
                         const float &m12 = 0, const float &m13 = 0,
                         const float &m14 = 0, const float &m15 = 1);

        /// Constructs a Matrix4 with the 16 values array \e matrix.
        Matrix4(const float *matrix);

        /** Constructs a Matrix3 with the 4 getColumn vectors \e v0, \e v1, \e v2
          and \e v3. */
        Matrix4(const Vector4 &v0,
                const Vector4 &v1,
                const Vector4 &v2,
                const Vector4 &v3);


        /// Destructor.
        ~Matrix4();

        /// Returns whether \e self is equal to \e v.
        bool operator==(const Matrix4 &m) const;

        /// Returns whether \e self is not equal to \e v.
        bool operator!=(const Matrix4 &m) const;

        /** Returns a const reference to the value at the \e i-th row
          and \e j-th getColumn.
          \warning
          - \e i must belong to the range [0,4[. */
        const float &operator()(Uchar i, Uchar j) const;

        /** Returns a reference to the value at the \e i-th row
          and \e j-th getColumn.
          \warning
          - \e i must belong to the range [0,4[. */
        float &operator()(Uchar i, Uchar j);

        /// Sets \e self to result of the addition of itself and \e m.
        Matrix4 &operator+=(const Matrix4 &m);

        /// Sets \e self to result of the substraction of itself and \e m.
        Matrix4 &operator-=(const Matrix4 &m);

        /// Sets \e self to result of the multiplication of itself and \e m.
        Matrix4 &operator*=(const Matrix4 &m);

        /** Sets \e self to result of the multiplication of itself by
          the scalar \e s. */
        Matrix4 &operator*=(const float &s);

        /** Sets \e self to result of the division of itself by the scalar \e s.
          \warning
          - \e s must be different from 0. */
        Matrix4 &operator/=(const float &s);

        /// Returns the opposite Matrix4 of \e self.
        Matrix4 operator-() const;

        /// Returns the result of the addition of \e this and \e m.
        Matrix4 operator+(const Matrix4 &m) const;

        /// Returns the result of the substraction of \e this and \e m.
        Matrix4 operator-(const Matrix4 &m) const;

        /// Returns the result of the multiplication of \e this by \e m.
        Matrix4 operator*(const Matrix4 &m) const;

        /// Returns the result of the multiplication of \e m  by the vector \e v.
        friend Vector4 operator*(const Matrix4 &m, const Vector4 &v);

        /// Returns the result of the multiplication of \e m  by the point \e v.
        friend Vector3 operator*(const Matrix4 &m, const Vector3 &v);

        /// Returns the result of the multiplication of \e m by the scalar \e s.
        friend Matrix4 operator*(const Matrix4 &m, const float &s);

        /// Returns the result of the division of \e m by the scalar \e s.
        friend Matrix4 operator/(const Matrix4 &m, const float &s);

        /** Returns the adjoint matrix of \e m.
          \pre
          - \e m must be valid. */
        friend Matrix4 adjoint(const Matrix4 &m);

        /** Returns the det of \e m.
          \pre
          - \e m must be valid. */
        friend float det(const Matrix4 &m);

        /// Prints \e m to the output stream \e stream.
        friend std::ostream &operator<<(std::ostream &stream, const Matrix4 &m);

        /** Returns a const pointer to the datas of \e self.
          \pre
          - \e self must be valid. */
        const float *getData() const;

        /** Returns a pointer to the datas of \e self.
          \pre
          - \e self must be valid. */
        float *getData();

        void transform(Vector3 &normal);


        /** Returns the third column of the matrix which corresponds to the main vector of GeomElem*/
        Vector3 getMainVector() const;

        /** Returns the second column of the matrix which corresponds to the normal vector of GeomElem*/
        Vector3 getNormalVector() const;

        /** Returns the first column of the matrix which corresponds to the secondary vector of GeomElem*/
        Vector3 getSecondaryVector() const;

        /** Returns the fourth column of the matrix which corresponds to the translation vector of GeomElem*/
        Vector3 getTranslation() const;

        void setPlace(int i, float val);
        float getPlace(int i);

        /** Set the translation vector coordinate */
        void setTranslation(float x, float y, float z);

        /** Set the secondary vector coordinate */
        void setSecondaryVector(float x, float y, float z);

        /** Set the normal vector coordinate */
        void setNormalVector(float x, float y, float z);

        /** Set the main vector coordinate */
        void setMainVector(float x, float y, float z);

        Vector4 getTopPosition();


        /** Returns the \e i-th getColumn.
          \warning
          - \e i must belong to the range [0,3]. */
        Vector4 getColumn(Uchar i) const;

        Vector4 getDirection(bool scaled);

        Vector4 getSecondaryDirection();


        /** Returns the diagonal of \e self.
          \pre
          - \e self must be valid. */
        Vector4 getDiagonal() const;

        /** Returns the \e i-th row.
          \warning
          - \e i must belong to the range [0,3]. */
        Vector4 getRow(int i) const;

        /** Returns the det and the inverse of \e m.
          \pre
          - \e m must be valid. */
        friend Matrix4 inverse(const Matrix4 &m);

        /** Returns whether \e self is orthogonal.
          \pre
          - \e self must be valid. */
        bool isOrthogonal() const;

        /** Returns whether \e self is singular.
          \pre
          - \e self must be valid. */
        bool isSingular() const;

        /// Returns whether \e self is valid.
        bool isValid() const;

        /** Sets \e self with the values \e m0, \e m1, \e m2, \e m3, \e m4, \e m5,
          \e m6, \e m7, \e m8, \e m9, \e m10, \e m11, \e m12, \e m13, \e m14 and
          \e m15. */
        void set(const float &m0, const float &m1, const float &m2, const float &m3,
                 const float &m4, const float &m5, const float &m6, const float &m7,
                 const float &m8, const float &m9, const float &m10, const float &m11,
                 const float &m12, const float &m13, const float &m14, const float &m15);

        /** Returns the trace of \e m.
          \pre
          - \e m must be valid. */
        friend float trace(const Matrix4 &m);


        /** Returns the scaling matrix corresponding to a scaling with factor \e v.
          \pre
          - \e s must be valid. */
        static Matrix4 scaling(const Vector3 &s);

        /** Returns the rotation matrix corresponding to a axis rotation with axis \e v.
          and angle \e angle.
          \warning angle must be in radians.
          */
        static Matrix4 axisRotation(const Vector3 &axis, const float &angle);

        /** Returns the euler rotation matrix R(angle) where
          R( angle=(az,ay,ax) ) is the product of 3 matrices Rz(az)*Ry(ay)*Rx(ax).
          The object is rotated around the X axis, then around the Y axis,
          and then around the Z axis.
          \warning angles must be in radians.
          */
        static Matrix4 eulerRotationZYX(const Vector3 &angle);


        /** Returns the euler rotation matrix R(angle) where
          R( angle=(ax,ay,az) ) is the product of 3 matrices Rx(ax)*Ry(ay)*Rz(az).
          The object is rotated around the Z axis, then around the Y axis,
          and then around the X axis.
          \warning angles must be in radians.
          */
        static Matrix4 eulerRotationXYZ(const Vector3 &angle);


        /** Returns the translation matrix corresponding to the translation of
          vector \e v.
          \pre
          - \e v must be valid. */
        static Matrix4 translation(const Vector3 &v);

        /** Returns the transpose of \e m.
          \pre
          - \e m must be valid. */
        friend Matrix4 transpose(const Matrix4 &m);

        float getAzimuth();

        void serialize(Archive &ar);

        double getLength() const;

        void setLength(double length);

        void setIdentity();

    protected:

        /// The matrix values.
        float __M[16];

    }; // Matrix4


}; //end namespace Vitis

// __geom_matrix_h__
#endif
