/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

/** \file def.h
 *
 * \brief some constant value and symbolic name and macro definition
 * */


// Modif SG

#ifndef _DEFVITIS_H
#define _DEFVITIS_H

#ifdef WIN32
#define VITIS_LITTLE_ENDIAN
#else
#define VITIS_BIG_ENDIAN
#endif

#ifdef VITIS_LITTLE_ENDIAN
#define isMachineBigEndian()     (0)
#define isMachineLittleEndian()  (1)
#endif

#ifdef VITIS_BIG_ENDIAN
#define isMachineBigEndian()     (1)
#define isMachineLittleEndian()  (0)
#endif

/* return the short with the reversed byte ordering of s */
#define reverse_short( s ) ( ( (s) << 8 ) | (( (s) >> 8 ) & 0xFF) )

/* return the int with the reversed byte ordering of i */
#define reverse_int( i ) ( ( (i) << 24 ) | \
		( ( (i) & 0x0000FF00 ) <<  8 ) | \
		( ( (i) & 0x00FF0000 ) >>  8 ) | \
		( ( (i) >> 24 ) & 0xFF ) )

#define CONV 0.0174533
#define FCONV (float)0.0174533

#define EPSILON 0.000001

#define VChar char
#define Uchar unsigned char
#define Lint long
#define Sint short
#define Uint unsigned long
#define USint unsigned short
#define Sfloat float
#define Lfloat double

#define FUSint "%hu"
#define FSint "%hd"

#define INSERINIT 0
#define INSER1 1
#define INSER2 2
#define INSER3 3
#define INSER4 4
#define INSER5 5
#define INSER6 6
#define INSER7 7
#define INSER8 8
#define INSER9 9
#define INSER10 10

#define NBCOMPTRAND    50

#define MAX 0.0
#define MIN 999.0

#define NO_SIMP 0
#define SIMP_LEAVES 1
#define SIMP_LEAVES_AND_BRANC_MANY 2
#define SIMP_LEAVES_AND_BRANC_ONCE 3


#endif
