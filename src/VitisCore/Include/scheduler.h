/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file Scheduler.h
///			\brief Definition of Scheduler.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _SCHEDULER_H
#define _SCHEDULER_H
#include "event.h"
#include "heap.h"
#include "ArchivableObject.h"
using namespace Vitis;


/// \brief Definition of the stack of Action eventData
typedef heap<Action>	A_table_t;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class PDescr
///			\brief A convenient serializable VProcess container.
///
///			A convenient VProcess container.
///			It mainly provides with serialization methods.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

class PDescr :public ArchivableObject{

	public :

		VProcess * 	process;

		bool terminated;

		/// \brief Last time this process was activated
		Time available_at;

		PDescr(VProcess * p=NULL)
			: process(p), terminated(false), available_at(0) {}

		/// \brief Serialization function
		void serialize(Archive& ar )
		{
			if( ar.isWriting() )
			{
				if(this->SavePtr(ar,process))
				{
					//deja sauvegard�
					terminated=true;
					std::cout<<"Error ";
				}
				ar <<terminated<< available_at;


			}
			else
			{

				ArchivableObject * arVp;
				if(this->LoadPtr(ar, arVp))
				{
					//d�j� charg�
					std::cout<<"Bad!!";
				}
				ar >>terminated>> available_at;
				process=(VProcess *)arVp;

			}

		}
};

typedef std::vector<PDescr> PsTable;



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class Scheduler
///			\brief Maintains and executes discrete events.
///
///			Maintains and executes discrete events.
///			\brief It consists mainly in a list of VProcess (functional part of an application) and a list of actions (Processes associated to an execution time)
///			\warning It should therefore be seen and used more as a module than a class. This module may be used
///			for discrete event driven models.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT Scheduler: public ArchivableObject {


	private:

		/// \brief the current virtual time for the current process
		double top_clock;

		/// \brief the next virtual time for the current process
		double next_top_clock;

		/// \brief the current priority for the current process
		int current_priority;

		/// \brief the time/priority sorted stack of actions to be held
		A_table_t agenda;

		/// \brief the current process
		ProcessId current_process;

		/// \brief list of processes available for diary
		PsTable processes;

		/// \brief end simulation time
		double hotStop;

		/// \brief mother plant
		class Plant * v_plt;


	public:

		/// \brief Default constructor
		Scheduler(class Plant * plt=NULL);

		/// \brief	Instance destructor
		~Scheduler();

		/// \brief 	Returns the current virtual time for the current process
		double getTopClock();

		/// \brief Returns the next virtual time for the current process
		double getNextTopClock();

		// \brief 	Returns the current prioirty for the current process
		int getTopPriority();

		/// \brief Set hot stop
		void setHotStop(double hstp);

		// \brief 	Return the hot stop simulation time
		double getHotStop();

		/// \brief Set the plant pointer
		inline void setPlant(class Plant * plt) {v_plt=plt;}




		/// \brief Start the simulation
		///
		/// Pop back each eventData of the stack and run corresponding action
		/// until the stack is empty or there is no more action before hotsop.\n
		/// Depending of the type of Action different policy may be chosen : \n
		/// Action type = A_Event : run the process_event function of the corresponding VProcess.\n
		/// Action type = A_Init : run the init function of the corresponding VProcess.\n
		/// Action type = A_Stop : do nothing.\n
		/// In every case the current Action is removed from the Action stack.
		void run_simulation();

		/** @brief creates a new process
		 *
		 *  Creates a new process with the given Process object.  This
		 *  method schedules the execution of the process
		 *
		 */
		/** \return the process id value */
		int create_process(VProcess * pro, EventData * e, Time d, Priority p=0 );

		/** @brief signal an eventData to the given process at the given time
		 *
		 *  Signal an eventData to the given process.  */
		void signal_event(ProcessId  pid, EventData * e, Time d, Priority p=-1) throw() ;

		/** @brief signal an eventData to the current process at the given time
		 *
		 *  Signal a delayed eventData to the current process.  The response is
		 *  scheduled with the given delay. */
		void self_signal_event(EventData * e, Time d, Priority p=-1) throw();


		/** @brief clears out internal data structures
		 *
		 *  Reset the simulator making it available for a completely new
		 *  simulation.  All scheduled actions are deleted together with
		 *  the associated events.
		 *  Notice however that it is the responsability of the simulation
		 *  model to delete process objects used in the simulation.
		 **/
		void clear() throw();

		/** @brief deactivate this process descriptor
		 *
		 *  deactivate this process descriptor
		 **/
		void clear(PDescr pd);

		/** @brief deactivate this process
		 *
		 *  deactivate this process
		 **/
		bool clear(VProcess *v);

		/** @brief stop the execution of a given process */
		///
		/** Stop the execution of a given process */
		/** \return -1 if the process is already terminated, 0 otherwise*/
		///
		/** Stop the execution of the current process */
		int stop_process(ProcessId pid) throw();

		/** @brief stop the execution of the current process */
		///
		/** Stop the execution of the current process */
		void stop_process() throw();


		/** @brief delete the current process */
		///
		/** Delete the current process */
		void delete_process() throw();

		/** @brief delete the current process */
		///
		/** Delete process with ProcessId pid */
		void delete_process(ProcessId pid) throw();


		/** \brief Serialize the scheduler**/
		///
		/** Serialize the scheduler**/
		/**	\param ar A reference to the archive \see Archive */
		void serialize(Archive& ar );



		/** \brief Seek actions stack**/
		///
		/** Seek actions stack**/
		/**	\param pi process id (return by function create_process)
		*	\param d The time for this action
		*	\param p The priority for this action
		*	\param e The datas for this action
		* \return a reference to the corresponding action, otherwise throws an exception Vitis::CException("no such action") */
		Action & getAction (ProcessId pi, Time d=-1, Priority p=0, EventData * e=NULL);

		/** \brief Seek actions stack**/
		///
		/** Seek actions stack**/
		/**	\param pid process id (return by function create_process)
		*	\param d The time for this action
		*	\param p The priority for this action/
		*	\param e The datas for this action
		* \return the corresponding action index into the stack, -1 otherwise */
		int getActionIndex (ProcessId pid, Time d=-1, Priority p=0, EventData * e=NULL);

		/** \brief Replace an action with an other one**/
		///
		/** Replace an action with an other one**/
		/**	\param actionId index of action to be modified
		*	\param pid process id (return by function create_process)
		*	\param d The time for this action
		*	\param p The priority for this action
		*	\param e The datas for this action */
		void changeAction (int actionId, ProcessId pid, Time d, Priority p, EventData * e=NULL);

		/// \cond 0
		void displayAgenda();
		/// \endcond

		static Scheduler* instance();
};

#endif



