/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file Archive.h
///			\brief Definition of Archive class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _ARCHIVE_H
#define _ARCHIVE_H
#include <string>
#include "VitisObject.h"

namespace Vitis {

	//! \class Archive
	//! \brief A pure virtual class for reading/writing binary data from/to a serial stream
	/*! This class is similar to the MFC CArchive class. It provides a
	  simple way for objects derived from Vitis::ArchivableObject to load or store
	  themselves to a serial stream. Each object derived from ArchivableObject
	  that wants load/store functionality needs to overload serialize().

	  \code
	  class Test : public ArchivableObject
	  {
	  int a, b;
	  float x, y;

	  void serialize( Archive& ar )
	  {
	  if( ar.isWriting() )
	  {
	  ar << a << b << x << y;
	  }
	  else
	  {
	  ar >> a >> b >> x >> y;
	  }
	  }
	  }
	  \endcode

	  Descendents should be sure to check whether they need to invoke the serialize() method from their parent class.
	  */
	class VITIS_EXPORT Archive : public VitisObject
	{

		public:

			//! \brief Default constructor
			Archive();

			//! \brief Default destructor
			virtual ~Archive();

			//! \brief Is the archive reading (i.e. loading)?
			/*! \return \c true if the archive is reading, \c false otherwise */
			inline bool isReading() const
			{ return !_saving; }

			//! \brief Is the archive writing (i.e. saving)?
			/*! \return \c true if the archive is writing, \c false otherwise */
			inline bool isWriting() const
			{ return _saving; }

			//! \return \c true if this machine is big-endian, \c false otherwise
			inline bool isBigEndian()
			{ return _bigEndian; }

			//! \return \c true if an error occured during the last read() or write(), \c false otherwise
			inline bool getLastError() const
			{ return _error; }

			//! \brief Write the \b size bytes of data pointed to by \b p to the archive.
			virtual bool write( const void* p, int size );

			//! \brief Read \b size bytes of data from the archive into \b p.
			/*! Descendents will implement this function to read \b size bytes of data from their stream into \b p.
			*/
			virtual bool read( void* p, int size );

			/*! \name Output type overloads
			 *  Use these functions to save basic data types to the archive.
			 *  Note that \c char* types (i.e. c-strings) are not supported for
			 *  output or input. This is a safety measure to avoid buffer under-run or
			 *  over-run. To read/write character arrays, first convert them to the
			 *  STL string data type or use the read() and write() functions.
			 *  For multi-byte types, these functions first reverse the byte ordering
			 *  of the data if necessary to guarantee storage in big-endian format,
			 *  then call write().
			 */
			//@{
			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, char c );
			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, signed char c );
			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, unsigned char c );

			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, short n );
			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, int n );
			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, long n );

			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, unsigned short n );
			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, unsigned int n );
			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, unsigned long n );

			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, float f );
			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, double f );

			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, bool n );
			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, const void* p );

			VITIS_EXPORT friend  Archive& operator <<( Archive& ar, std::string s );


			//@}

			/*! \name Input type overloads
			 *  Use these functions to load basic data types from the archive.
			 *  For multi-byte types, these functions call read(), then reverse the byte ordering
			 *  of the data if necessary to guarantee the endian format of this machine.
			 */
			//@{
			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, char& c );
			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, signed char& c );
			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, unsigned char& c );

			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, short& n );
			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, int& n );
			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, long& n );

			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, unsigned short& n );
			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, unsigned int& n );
			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, unsigned long& n );

			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, float& f );
			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, double& f );

			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, bool& n );
			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, void* &p );

			VITIS_EXPORT friend  Archive& operator >>( Archive& ar, std::string& s );

			//@}

		protected:

			//! \brief Write the \b size bytes of data pointed to by \b p to the archive.
			/*! Descendents MUST implement this function to write the \b size bytes of data pointed to by \b p
			  to their stream.
			  \return \c true on success or \c false on failure
			  */
			virtual bool onWrite( const void* p, int size ) = 0;

			//! \brief Read \b size bytes of data from the archive into \b p.
			/*! Descendents MUST implement this function to read \b size bytes of data from their stream into \b p.
			  \return \c true on success or \c false on failure
			  */
			virtual bool onRead( void* p, int size ) = 0;

			//! \brief Descendents should call this when the archive is set up to write/save data.
			inline void enableWriteMode()
			{ _saving = true; }

			//! \brief Descendents should call this when the archive is set up to read/load data.
			inline void enableReadMode()
			{ _saving = false; }

		private:

			bool _saving, _bigEndian, _error;

	}; /* end class Archive */

} /* end namespace Vitis */



#endif

