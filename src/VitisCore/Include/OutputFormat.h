/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  OutputFormat.h
///			\brief Definition of the OutputFormat class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _OUTPUTFORMAT_H
#define _OUTPUTFORMAT_H
#include "VitisObject.h"
#include <string>


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class OutputFormat
///			\brief Abstract class to define an output format. \n
///				It defines an interface that has to be implemented by descendant classes
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT OutputFormat:public VitisObject
{
	protected:

		//! \brief The output file path
		std::string filename;

		/// \brief Binary writing type (if needed). True stands for BigEndian.
		bool swap;



	public:

		//! \brief Constructor
		OutputFormat(const std::string &finame):filename(finame.c_str()){};

		//! \brief Destructor
		virtual ~OutputFormat(void){};

		/// \brief Pure virtual function that is aimed at preparing data to be output
		virtual int initFormat(VitisObject * vobj)=0;

		/// \brief Pure virtual function that is aimed at outputing data
		virtual int printData(VitisObject *)=0;

		/// \brief Pure virtual function that is aimed at printing a header to the output file
		virtual bool printHeader (const std::string &headerf)=0;

		//! \brief Set the swap flag
		virtual void setSwap (bool s){swap = s;};

		/// \name Convenient function to swap byte into base types
		/// @{
		long swapDatal (long v);
		float swapDataf (float v);
		float  swapData (float v);
		/// @}
};



#endif

