/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file Notifier.h
///			\brief Declaration and implementation of the notifier/subscriber pattern to allow implementation of
///          communication interface between models and plugins (see http://www.codeguru.com/Cpp/Cpp/cpp_mfc/templates/article.php/c7077/).\n
///
///			In order to provide a plugin interface for a particular event, a model has to implement a Vitis::notifier<T_state> that will send a message of type paramMsg to any subscriber to that message. \n
///         In order to plug to an interface a plugin has to implement the corresponding \class subscriber.
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _VITIS_NOTIFIER_H_
#define _VITIS_NOTIFIER_H_
#include <map>

#ifndef WIN32
#include <stdexcept>
#endif
#include <stdexcept>
#include "VitisThread.h"


/*
 **  Declarations
 */

namespace Vitis
{

	template <typename T_state> class notifier;

	/////////////////////////////////////////////////////////////
	///	\class paramMsg
	///	\brief parameter to join when notifying message
	///////////////////////////////////////////////////////////////
	struct paramMsg
	{
		paramMsg(){};

		~paramMsg(){};
	};

/// @cond 0
	/////////////////////////////////////////////////////////////
	///	\class        singleton
	///	\brief        Quick and easy singleton.
	///////////////////////////////////////////////////////////////
	template <typename T>
	struct singleton
	{
	static T& instance();

	};

	template <typename T_state> T_state & singleton<T_state>::instance()
	{
		static T_state t_;
		return t_;
	}
/// @endcond


	/////////////////////////////////////////////////////////////
	///	\class        subscriber
	/**	\brief		Provides plugins with tools to implement message interface.\n
	*               It is linked to notifier class.
	*
	* The subscriber is able to subscribe for a particular message type T_state. after subscribing, each time
	*  this message is sent by the corresponding notifier singleton,
	 * the function on_notify will be called. \n
	 * An example of subscriber implementation may be :
	 * \code
	enum myMessage {mySignal };
	struct myParamMsg:public paramMsg
	{
		void set (MyData *d) {D=d;};
		MyData *get () {return D;};
		private:
		MyData *D;
	};
	struct  mySubscriber:public subscriber<myMessage>
	{
		mySubscriber () { subscribe();};
		virtual ~mySubscriber () {unsubscribe();};
		void on_notify (const state& st, myParamMsg * data){data = NULL;cout<<"I just received some myParamMsg"<<endl;};
	};
	 * \endcode
	 */
	///////////////////////////////////////////////////////////////
	template <typename T_state> struct subscriber
		{
			typedef T_state state;
			subscriber();
			//typedef notifier<T_state> notifier;

			/** \brief begin subscribing, the function on_notify will not be called before subscribing */
			/** **/
			/** \brief begin subscribing, the function on_notify will not be called before subscribing */
			void subscribe();
			void subscribe(void *p);
			void unsubscribe()/** \brief end subscribing */;
			virtual ~subscriber();/** \brief */;
			virtual void on_notify(const state&, paramMsg * data=NULL)/** \brief this function will be called by the corresponding notifier. \n the subscriber has to implement it */=0;

			void *object/** \brief the address of the object that may subscribe */;
		}; // struct subscriber

	/////////////////////////////////////////////////////////////
	///	\class        notifier
	/**	\brief		This class provides models with tools to implement
	*               dynamical message communication interface. \n
	*               It is linked to subscriber class.
	*
	*	The notifier assumes the following tasks :\n
   *     registering subscribers\n
   *     unregistering subscribers\n
   *     notifying subscribers\n
	*    There is a single instance of notifier for each message (T_state) type\n
	*    An example of notifier implementation may be :
*	\code
	// the custom message to be sent
	enum myMessage {mySignal };
	struct myParamMsg:public paramMsg
	{
		void set (MyData *d) {D=d;};
		MyData *get () {return D;};
		private:
		MyData *D;
	};

	// an embeded function to encapsulate the message sending
	MyData* sig (MyData *d)
	{
	// get the single instance of the notifier for myMessage type
		notifier<myMessage> *t= &(notifier<myMessage>::instance());

		if (t->get_nb_subscriber() == 0)
			return d;

	// prepare the message to be sent
		myParamMsg p;
		p.set(d);

	// send the message
		t->notify(mySignal, &p);

	// get back the data
		d=p.get();

		return d;
	};
	\endcode
*/

	///////////////////////////////////////////////////////////////
	template <typename T_state> class  notifier
		{
			protected:  // implementation

				notifier();
				~notifier();

			public:     // interface


				//typedef subscriber<T_state> subscriber;
				/// \brief function that subscribers will use to connect to the singleton notifier of a particular message type T_state
				void register_subscriber(subscriber<T_state>* p_subscriber);
				/// \brief function that subscribers will use to unconnect to the singleton notifier of a particular message type T_state
				void unregister_subscriber(subscriber<T_state>* p_subscriber);
				/// \brief return the number of subscribers to that message type T_state
				int get_nb_subscriber(){return (int)m_c_subscribers.size();};
				/// \brief return a singleton instance of notifier for message type T_state. \n
				/// Any access to a notifier \b must be achieved through this funtion to ensure proper communication
				static notifier<T_state>& instance();
				/** \brief send a message to this subscriber with type state */
				void notify(const T_state& state/**< \brief message type */,
					        paramMsg * data=NULL/**< \brief message body */,
							void *p=NULL/**< \brief the object that must be notified*/);

			private: // implementation
				/// \brief list of subscribers
				typedef std::map<subscriber<T_state>*, /// \brief the subscriber
					             bool /** \brief true if registred, false if not registred*/> subscriber_map;
/// @cond 0

				// modifiers
				static void notify_subscriber(subscriber<T_state>*p_subscriber, const T_state& state, paramMsg * data=NULL);
				void cull_unregistered_subscribers();

				// accessors
				vtsCriticalSection& get_lock() { return m_c_lock; }
				subscriber_map& get_subscriber_map() { return m_c_subscribers; }

				// const accessors
				const vtsCriticalSection& get_lock() const { return m_c_lock; }
				const subscriber_map& get_subscriber_map() const { return m_c_subscribers; }

			private: // data

				vtsCriticalSection m_c_lock;
/// @endcond
				subscriber_map     m_c_subscribers;

		}; // class notifier

	/*
	 **  Implementation
	 */


	///////////////////////////////////////////////////////////////////////
	// subscriber
	///////////////////////////////////////////////////////////////////////
	template <typename T_state> subscriber<T_state>::subscriber()
		{
		}

	template <typename T_state> subscriber<T_state>::~subscriber()
		{
		}

	template <typename T_state> void subscriber<T_state>::unsubscribe()
		{
			notifier<T_state>::instance().unregister_subscriber(this);
		}

	template <typename T_state> void subscriber<T_state>::subscribe()
		{
			object = NULL;
			notifier<T_state>::instance().register_subscriber(this);
		}

	template <typename T_state> void subscriber<T_state>::subscribe(void *p)
		{
			object = p;
			notifier<T_state>::instance().register_subscriber(this);
		}

	///////////////////////////////////////////////////////////////////////
	// notifier
	///////////////////////////////////////////////////////////////////////



	template <typename T_state> notifier<T_state>::notifier()
		{
		}

	template <typename T_state> notifier<T_state>::~notifier()
		{
			// whats the point of culling when our map is going to clear
			// a LOT of timing issues can cause problems here ...
			// diagnostics would be the only reason to turn this on;
			// if that's the case, don't forget to sleep.
			//cull_unregistered_subscribers();
			//_ASSERT(true == get_subscriber_map().empty()); // someone didn't unregister
		}

	template <typename T_state> notifier<T_state>& notifier<T_state>::instance()
		{
			static notifier<T_state> t_;
			return t_;
		}

	template <typename T_state> void notifier<T_state>::register_subscriber(subscriber<T_state>* p_subscriber)
		{
			// validate, lock, register and unlock
			//if (0 == p_subscriber) throw std::invalid_argument("notifier<T_state>::register_subscriber: subscriber");
			if (0 == p_subscriber) return;

			// WARNING: if you are being notified and you register
			// subscribers on the same thread being notified, this will not lock:
			// this may result in unintended consequences
			//        vtsCriticalSectionLocker lock(get_lock());
			std::pair<typename subscriber_map::iterator, bool> c_pair(
					get_subscriber_map().insert(typename subscriber_map::value_type(p_subscriber, true)));

			// if it is already in the map and it is ready to be unregistered, register it
			if (   false == c_pair.second
			    && false == c_pair.first->second)
			{
				c_pair.first->second = true;
			}
		}

	template <typename T_state> void notifier<T_state>::unregister_subscriber(subscriber<T_state>* p_subscriber)
		{
			// validate
			if (0 == p_subscriber) return;
			//throw std::invalid_argument("notifier<T_state>::unregister_subscriber: subscriber");

			// find subscriber; not found return, found flag for removal
			typename subscriber_map::iterator c_subscriber(get_subscriber_map().find(p_subscriber));
			if (c_subscriber == get_subscriber_map().end()) return;
			c_subscriber->second = false;
		}

	//template <typename T_state> void notifier<T_state>::notify(const T_state& state, paramMsg * data)
	//	{
	//		// for each *REGISTERED* subscriber
	//		// (Reminder: subscribers are not registered automatically)
	//		//        vtsCriticalSectionLocker lock(get_lock());
	//		for (typename subscriber_map::iterator c_subscriber(get_subscriber_map().begin());
	//				c_subscriber != get_subscriber_map().end();
	//				++c_subscriber)
	//		{
	//			// if subscriber is registered notify
	//			if (true == c_subscriber->second)
	//			{
	//				notify_subscriber(c_subscriber->first, state, data);
	//			}
	//		}

	//		// remove all subscribers unregistered during notification
	//		// note that this is not the most efficient way, as
	//		// it forces us to walk the subscriber map twice per notification ...
	//		cull_unregistered_subscribers();
	//	}

	template <typename T_state> void notifier<T_state>::notify(const T_state& state, paramMsg * data, void *p)
		{
			// for each *REGISTERED* subscriber
			// (Reminder: subscribers are not registered automatically)
			//        vtsCriticalSectionLocker lock(get_lock());
			for (typename subscriber_map::iterator c_subscriber(get_subscriber_map().begin());
					c_subscriber != get_subscriber_map().end();
					++c_subscriber)
			{
				// if subscriber is registered notify
				if (true == c_subscriber->second)
				{
					if (p == NULL || c_subscriber->first->object == p)
					{
						notify_subscriber(c_subscriber->first, state, data);
                    }
				}
			}

			// remove all subscribers unregistered during notification
			// note that this is not the most efficient way, as
			// it forces us to walk the subscriber map twice per notification ...
			cull_unregistered_subscribers();
		}

	template <typename T_state> void notifier<T_state>::notify_subscriber(subscriber<T_state>*p_subscriber, const T_state& state, paramMsg * data)
		{
			try
			{
				// validate and notify
				if (0 == p_subscriber)
                    return;
				// throw std::invalid_argument("notifier<T_state>::notify_subscriber: subscriber");
				p_subscriber->on_notify(state, data);
			}
			catch(...) {}
		}

	template <typename T_state> void notifier<T_state>::cull_unregistered_subscribers()
		{
			// if not empty, lock and cull
			if (get_subscriber_map().empty()) return;

			//        vtsCriticalSectionLocker lock(get_lock());

			for (typename subscriber_map::iterator c_subscriber(get_subscriber_map().begin());
					c_subscriber != get_subscriber_map().end();)
			{
				// if subscriber has been unregistered
				if (false == c_subscriber->second)
				{
					// remove it
					//                c_subscriber = get_subscriber_map().erase(c_subscriber);
					get_subscriber_map().erase(c_subscriber);
					c_subscriber = get_subscriber_map().begin();
				}
				// otherwise we'll check the next
				else ++c_subscriber;
			}
		}





}


#endif //_VITIS_NOTIFIER_H_
