/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  GeomBrancCone.h
///			\brief Definition of GeomBrancCone Class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _GEOMBRANCCONE_H
#define _GEOMBRANCCONE_H
#include "GeomBranc.h"
#include "GeomElemCone.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GeomBrancCone
///			\brief Specialized geometry of an axe based on a sequence of truncated cones
///
///			Every geometrical functions are declared virtual. They provide a default behaviour but
///			may be overwritten by a client model.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT GeomBrancCone :
	public GeomBranc
{

	protected:

		//! \brief The current transformation matrix along the branc
		///
		/// A Matrix4 is a set of 4X4 floating point values where the first three columns
		/// contains the orientation of the branc along the X,Y,Z world triedron. The last
		/// column contains the position of the translation along the X,Y,Z world triedron.
		Matrix4 currentTrans ;

		//! \brief Pointer the original geombranc -> NULL if this geombranc is computed
		GeomBranc *geoBrClone;

	public:

		//! \brief Constructor
		///
		/*! \param brc Pointer to the hierarchical axe
		  \param gbear Pointer to the geometrical bearer axe
		  \param plt Pointer to the mother plant */
		GeomBrancCone(Hierarc * brc, GeomBranc * gbear, class Plant * plt);

		//! \brief Destructor
		virtual ~GeomBrancCone(void);

		//! \brief Function to initialize geometry
		///
		/*! \return \li 0 This branc can be "super-simplified" \li 1 otherwise */
		virtual int initGeomtry();

		//! \brief Seeking mode for children geometry computing
		enum BrancSeekMode {ASCENDING, DESCENDING};

		//! \brief Function to compute axe geometry and lauch the geometry computing of its children according to bsm order
		///
		/*! \param bsm Branch Seeking Order (ASCENDING or DESCENDING)
		\return \li 0 This branc will be "super-simplified" \li 1 otherwise */
		virtual int computeBranc(BrancSeekMode bsm, bool silent=false);

		//! \brief Function to compute axe geometry and lauch the geometry computing of its children in default ASCENDING order
		///
		/*! \return \li 0 This branc will be "super-simplified" \li 1 otherwise */
		virtual int computeBranc(bool silent=false);

		//! \brief Function to compute the cone geometry of the internode positioned at index \it pos
		virtual void computeEntn(int pos, bool first);

		/// \name basic geometrical functions
		/// @{
		///
		//! \return the top-diameter of the current internode
		///
		/// By default this function returns back the bottom diameter of a
		/// Component equal to the top diameter.
		//! unit : centimeter
		virtual float computeBottomDiam();

		//! \return the up-diameter of the current internode
		///
		/// By default this function returns back 1.
		//! unit : centimeter
		virtual float computeTopDiam();

		//! \return the length of the current internode
		///
		/// By default this function returns back 1.
		//! unit : centimeter
		virtual float computeLength ();

		//! \return the phyllotaxy insertion angle around the bearer axe
		///
		/// By default this function returns back a multiple of PI.
		//! unit : radian
		virtual float computeInsertAnglePhy();

		//! \return the insertion angle between current axe and the bearer axe
		///
		/// By default this function returns back PI/3.
		//! unit : radian
		virtual float computeInsertAngle();
		/// @}

		//! \brief Function to simplify the geometry of the axe
		///
		//! Function to simplify the geometry of the axe
		/// Merge successive cones if they are approximatively colinear and not too distant.
		/// Depending on the value of the geometrical simplification parameter
		/// different threshold will be applied.
		/// simplification 0 : no simplification
		/// simplification 1 : direction are closer than 15� and their connection distance is less than 3mm.
		/// simplification 2 : direction are closer than 21� and their connection distance is less than 6mm.
		/// simplification 3 : direction are closer than 25� and their connection distance is less than 1cm.

		virtual void simplification ();

		//! \brief Function to compute the current element transformation according to the bending of the axe
		///
		//! Function to compute the current element transformation according to the bending of the axe
		/// By default no bending is applied.
		virtual void computeBending (GeomElem *){};

		//! \brief Find the simplified axe wich can represent this axe and launch the copy.
		///
		//! Find the simplified axe wich can represent this axe and launch the copy.
		/// This simplification takes place only if the value of the geometrical simplification parameter (cfg_geoSimp)
		/// is greater than 4. In this case the vertical range is divided in cfg_geoSimp equal sectors and only one
		/// geometrical branch will be computed for each topological branch class and for each elevation sector.
		/// This means that every branches belonging to the same topological class and having an insertion angle into
		/// the same sector will have the same geometrical shape.
		/*	\return \li 0 if this axe has to be simplified \li 1 a simplified instance was already computed */
		virtual int computeSuperSimplif();

		//! \brief Compute the matrix with the correct translation to position the first cone of the GeomBranc on his bearer
		///
		//! Compute the matrix with the correct translation to position the first cone of the GeomBranc on his bearer
		//! \return the matrix with the correct translation to position the first cone of the GeomBranc on his bearer
		/*! \param currentTriedron The current transformation
		/*! \param gprev The geometrical bearer element */
		Matrix4 computeInsertTranslation(Matrix4 & currentTriedron, GeomElem * gprev);

		/// @cond 0
		void computeCurrentTrans (Matrix4 transf);
		/// @endcond

		//! \brief Copy the given GeomBranc geoBr to this , and transform each GeomElem with a translation and a rotation matrix transformation
		///
		//! Copy the given GeomBranc geoBr to this , and transform each GeomElem with a translation and a rotation matrix transformation
		/*! \param geoBr The geometrical axe to clone
		/*! \param transf The transformation matrix
		*/
		void copyFrom(GeomBranc * geoBr,  Matrix4  transf);


		//! \brief Copy the given GeomElem ge , and transform it with a translation and a rotation matrix transformation
		///
		//! Copy the given GeomElem ge , and transform it with a translation and a rotation matrix transformation
		/*! \param ge The geometrical element to clone
		/*! \param transf The transformation matrix
		*/
		void pasteElem(GeomElem * ge, Matrix4  transf);


		//! \brief Set the current transformation matrix
		inline void setCurrentTransform (Matrix4 t){currentTrans=t;}

		//! \brief Get the current transformation matrix
		inline Matrix4 & getCurrentTransform (){return currentTrans;}

		//! \brief Get the current transformation matrix
		inline const Matrix4 & getCurrentTransform () const {return currentTrans;}

		//! \brief Get the current supersimplified geomBranc
		inline GeomBranc * getGeomBrancClone (){return geoBrClone;}
		//! \brief Set the current supersimplified geomBranc
		inline void setGeomBrancClone (GeomBrancCone*b){geoBrClone=b;}


		//! \brief Update the current phyllotaxy along the axe( called after each internode computation)
		virtual void updatePhyllotaxy (){};


		//! \brief Compute bending angle for current node according to global flexion of branch
		/// By default no bending is applied.
		virtual void flexionAtNode (){};


		//! \brief Find the geometrical primitive identifier for each element
		/// By default index 1 is returned.
		virtual int findSymbole (){ return 1;};

		//! \brief Return GeomElem containing the corresponding translation to curvilinear distance
		///
		//! Return GeomElem containing the corresponding translation to curvilinear distance
		virtual GeomElem getTranslationForCurvilinearDistance (double curvilinearDistance);

		//! \brief This function may forbid geometrical simplification due to custom model consideration
		///
		//! This function may forbid geometrical simplification due to custom model consideration
		/// By default return true (simplification is allowed).
		virtual bool customSimplification (GeomElemCone *, GeomElemCone *) {return true;};




};

#endif
