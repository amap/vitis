/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file EventData.h
///			\brief Definition of EventData and VProcess.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _event_h
#define _event_h
#include <iostream>
#include "externDll.h"
#include "ArchivableObject.h"
using namespace Vitis;

/// \brief Priority to order actions scheduled at the same time.
/// Lower values correspond to higher priority.
/// In case of two events at the same time, the action with lower value will be selected.
typedef int		Priority;

/** @brief virtual time type
 *
 *  This type represents the basic time in the virtual (simulated)
 *  world.  Being defined as a double type, virtual time is a
 *  discrete quantity.  The actual semantics of the time unit is
 *  determined by the simulated model.  In other words, a time
 *  interval of 1 may be interpreted as one second, one year, or any
 *  other time interval, depending on the semantics of the simulated
 *  model.
 */
typedef double	Time;

/** @brief process identifier type
 **/
typedef int		ProcessId;


enum ActionType {
	A_Event,
	A_Init,
	A_Stop,
	A_Delete
};


/** @brief Basic eventData in the simulation.
 *
 *  This base class represents a piece of information or a signal
 *  exchanged between two processes through the scheduler (see \Action). \n
 *  This data will transmitted to the target process as a parameter of the process_event function. \n
 *  Every specializeed eventData must inherit from this class so that they can serialize.
 */
class VITIS_EXPORT EventData: public ArchivableObject
{


	public:

	    std::string classType;

		/// \brief Default constructor
		EventData () {
            classType = "EventData";
		};


		/// \brief Destructor
		virtual ~EventData () {}

		virtual void serialize( Archive&  ) {}


		const std::string &getClassType() const {
			return classType;
		}

		void setClassType(const std::string &classTypeString) {
			classType = classTypeString;
		}

};




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class VProcess
///			\brief Virtual class (interface) representing processes running within the simulator.
///
///			Any object that has to be scheduled through the Plant Scheduler must inherit VProcess (for instance, inside VitisCore, the objects Plant, Output and Serializer inherits VProcess).
///			A simulated process must implement this basic interface.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT VProcess: public ArchivableObject
{


	public:

		/// \brief process identifier
		ProcessId process_id;

		/// \brief Default constructor
		VProcess(){};

		/// \brief Destructor
		virtual ~VProcess(){};

		/** \brief Action executed for process initialization or when the process is associated to an Action with A_Init type.
		 *
		 *  This method is not a constructor.  It is rather an
		 *  initialization step that is executed during the simulation
		 *  when the process is created within the simulation. It may be achieved by directly calling this function
		 *  or by registering an Action with this VProcess and A_Init type at the required time.
		 *
		 **/
		virtual void init(void) {};

		/** \brief Action executed when the process is associated to an Action with A_Event type.**/
		virtual void	process_event( EventData * ) {};

		/** \brief Executed when the process is explicitly stopped.
		 *
		 *  A running process may be stopped by a call to this event.
		 **/
		virtual void	stop(void) {};

		/** \brief Serialize a VProcess  **/
		/**	\param ar A reference to the archive \see Archive **/
		void serialize(Archive& ar )
		{
			if( ar.isWriting() )
				ar <<process_id;
			else
				ar >>process_id;
		}

		/// \brief Return the index associated to this process.
		ProcessId getPid () {return process_id;};

};



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class Action
///			\brief Class representing an action scheduled in time.
///
///			When an action is started , the process identified by its ProcessId is launched and a particular eventData
///			may be specified that will be sent to the process.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Action : public ArchivableObject{

	public:
		/// \brief Virtual Time
		/// @see Time
		Time time;

		/// \brief Priority to order actions scheduled at the same time.
		/// Lower values correspond to higher priority.
		/// In case of two events at the same time, the action with lower value will be selected.
		Priority priority;

		/// \brief Current action type
		///
		/// There are three kind of action : \li A_Event : An action calling an eventData,  \li A_Init : An initializarion action,  \li A_Stop : A stop action
		ActionType type;

		/// \brief The process identifier
		ProcessId  pid;

		/// \brief A pointer to a potential eventData (custom data for particular convenience)
		EventData * eventData;

		/// \brief Constructor
		/// @param t flag time to execute action
		/// @param prio priority of this action
		/// @param at Type of action
		/// @param p Process identifier
		/// @param e Pointer to an eventData
		Action(Time t=0, Priority prio=0, ActionType at=A_Init, ProcessId p=-1,  EventData * e = 0) throw()
			: time(t), priority(prio), type(at), pid(p), eventData(e) {};

		/// \brief Specialization of < operator to compare Action (used to sort actions according time and priority)
		bool operator < (const Action & a) const throw() {

			if(time<a.time) return true;
			else
			{
				if (time>a.time)
					return false;
				else //time==a.time
				{
					if (priority>a.priority)
						return true;
					else if (priority==a.priority)
					{
//						if(pid>a.pid)
//							return true;
                        if (rand() > RAND_MAX/2)
                            return true;
					}
				}
			}
			return false;
		}

		/// \brief Serialize an Action
		///	\param ar A reference to the archive \see Archive
		void serialize(Archive& ar )
		{//TODO serialize eventData
			int t;
			if( ar.isWriting() )
			{

				ar <<time<< priority<< pid;
				switch(type)
				{
					case A_Event :	t=0; break;
					case A_Init : t=1; break;
					case A_Stop : t=2; break;
					default: t=-1;break;
				}
				ar<<t;
				SavePtr(ar,eventData);

			}
			else
			{
				ar >>time>> priority>>pid>>t;
				switch(t)
				{
					case 0 : type=A_Event; break;
					case 1 : type=A_Init; break;
					case 2 : type=A_Stop; break;
					default: type=A_Stop;break;
				}

				if(LoadPtr(ar,(ArchivableObject *&)eventData))
				{
					eventData= new EventData();
					AddPtr(ar, (ArchivableObject *)eventData);
				}


			}
		}
};


#endif

