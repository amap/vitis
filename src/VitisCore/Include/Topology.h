/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  Topology.h
///			\brief Definition of hierarchical axe Hierarc and of the topological manager TopoManager.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _TOPOLOGY_H
#define _TOPOLOGY_H

#include "Branc.h"
#include "SimplifTopo.h"
#include <queue>



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class Hierarc
///			\brief Class to describe the topological organization of branches
///
///			Describe the dependency between branches.\n
///			Each intance gives the pointer to the corresponding branch, the pointer to the bearer and the borne.\n
///			This class gives the topological arrangement of the branches.\n
///
///			\li Each Hierarc stores the pointer to the corresponding topological Branc
///			\li Each Branc stores the indices of the corresponding Hierarc
///
///			\warning Due to topological simplification, a single Branc can be linked to more than one Hierarc.
///          Basicaly when topological simplification applies, every branches with a same criteria are linked to
///          a unique Branc.\n \see SimplifTopo.h
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT Hierarc: public ArchivableObject {


	public:

		Branc *   brancPtr;       /**< \brief Pointer to the current axe Branc */

		Hierarc *	bearerPtr;    /**< \brief Pointer to the bearer */

		int    indexOnBearerAtLowestLevel;   /**< \brief Position along the bearer at the lowest decomposition level */

		short	borneNumber;       /**< \brief Current number of borne branches */

		std::vector<Hierarc *> bornePtr;  /**< \brief Vector of pointers to borne branches */

		std::vector<int>    bornePosition;     /**< \brief Vector of borne's position at lowest decomposition level */

		USint   verticilleBranchNumber; /**< \brief Number of branches for verticille */

		char    indexIntoVerticille; /**< \brief Current index within verticille */

		char	simplified;	/**< \brief Flag to know if this axe and his child are simplified (0 or 1)*/

		char    ordre; /**< \brief Global branching order */


		/// \brief Default constructor
		Hierarc ();

		/// \brief Constructor
		/// @param bPtr The bearer pointer
		/// @param b The branch pointer
		/// @param numVertic The current index within verticille
		/// @param nbrVertic The number of branc within verticille
		Hierarc (Hierarc * bPtr, Branc * b, char numVertic=1, USint nbrVertic=1);

		/// \brief Destructor
		/// @warning Delete this hierarchical axe and all its son
		virtual ~Hierarc();

		/// \brief Copy the born axes of given Hierarc into this
		void copyBorneFrom(Hierarc * hierarcPtr);

		/// \brief Delete a borne axe
		void deleteBorne (Hierarc * hierarcPtr);

		/// \brief Print (into the given stream with simple ASCII format) the topoligical hierarc structure starting from the current hierarc
		void printTree (std::ofstream & treestr, std::string ecart);

		/// \brief Add a borne axe at the position \epos according to loweest level of decomposition for associated branch
		void addBorne(Hierarc * h, long pos);

		/// \brief return Number of branches borne by this axe
		/// \warning Return the real number of borne, if the current hierarc is simplified, the method will seek through the "original" hierarc
		short getNbBorne ();

		/// \brief return Branch instance of the borne branch at position next into the borne branch of this hierarc (NULL upon error)
		/// \param next index of borne branch within all borne branches
		Branc *getBorne (int next);

		/// \brief return \li The Hierarc instance of the bearer \li NULL upon error
		Hierarc * getBearer ();

		/// \brief return \li Branch instance for the current Hierarc instance \li NULL upon error
		Branc *getBranc ();


		/// \brief Sort borne branches vector according to position along the current Hierarc
		/// @see borneNumber, borneIndex and bornePosition filed of Hierarc data struct
		void sortPosHierarc ();

		/// \brief Sort borne branches vector according to position along the current Hierarc and his children
		void sortTree();

		/// \brief Serialize the current Hierarc (and call the serialization for all his child)
		///	\param ar A reference to the archive \see Archive
		void serialize(Archive& ar );

};




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class TopoManager
///			\brief Manage the topological organization of branches. An instance of TopoManager is a member of Plant class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT TopoManager: public ArchivableObject
{

	private:


		/// \brief Main branch of the tree structure
		Hierarc * trunc;

		/// \brief Stack for seeking through the branch organization. \n
		/// The number of records in this stack is equal to the number of axes
		/// into the plant. Due to topological simplification, there may be less
		/// computed Branc into memory.
		std::queue<Hierarc *> pileSeek;

		/// \brief Topological simplifier
		SimplifTopo * simpTopo;

		/// \brief Mother plant
		class Plant * v_plt;

		long nbHierarc;

		int currentId;

	public:

		/// \brief Real number of computed axes in the current plant
		long nbBranc;

		long ComputeNbHierarc();
		long ComputeNbHierarc(Branc *b);
		long NbHierarc(){return nbHierarc;};

		/// \brief Number of computed axes since the simulation has started
		long nbBrancBorne;

		/// \enum Defines two mode to seek the topological structure
		/// \li RESET to position on the main branch
		/// \li NEXT to get the next instance of Hierarc in a recursive way
		typedef enum {RESET, NEXT} forestSeekMode;

		/// \brief Default constructor
		TopoManager(class Plant * plt=NULL);

		/// \brief Destructor
		~TopoManager();

		/// \brief Create and insert an axe Hierarc on a specific bearer
		///
		/// Create and insert an axe Hierarc on a specific bearer
		/// \param b A pointer to the new instance of Branc
		/// \param bearerB A pointer to the Hierarc bearer
		/// \param numVertic The current index within verticille
		/// \param nbrVertic The number of branc within verticille
		void fork (Branc * b, Hierarc * bearerB, char numVertic=1, USint nbrVertic=1);

		/// \brief Remove the Hierarc axe of Branc \e b (and all born axe)
		///
		/// Remove the Hierarc axe of Branc \e b (and all born axe)
		/// \param b The branc to remove
		/// \param total The way to remove the branc \li 1 : All instance (simplified) of this axe are removed \li 0 : Only the current axe of \b is removed
		void removeBranc(Branc * b , int total);

		/// Empty the Hierarc axe of Branc \e b (and remove all born axes).
		/// I.e. removing any decomposition level and only keeping the base container
		/// \param b The branc to empty
		void emptyBranc(Branc * b);

		/// \brief Stack seeking function
		///
		/// \return \li The computed Branc corresponding to the main axe of the Plant if seeking mode is RESET
		/// \li The computed Branc associated to the next record if seeking mode is NEXT
		/// \param forestSeekMode Seeking mode
		Branc * seekTree(forestSeekMode mode);

		/// \brief Add the instance of Branc to the simplification criteria
		///
		/// Add the instance of Branc to the class identified with an alphanumerical simplification criteria
		/// \param b the Branc to be added
		/// \param c the alphanumerical criteria associated to this Branc
		void updateSimplifier (Branc * b, const std::string & c);

		/// \return return the first instance of Branc belonging to the class indexed to criteria
		///
		/// Return the first instance of Branc belonging to the class indexed to criteria
		/// \param c the criteria
		/// \return \li the first Branc instance \li null if the class is empty
		Branc * getInstanceBrancSimplified (const std::string & c);


		/// \brief Remove the instance of Branc from the simplification criteria
		///
		/// Remove the instance of Branc from the class identified with an alphanumerical simplification criteria
		/// \param b the Branc to be removed
		/// \param c the alphanumerical criteria associated to this Branc
		void removeInstanceBrancSimplified (Branc *b, const std::string & c);

		/// \brief Return the instance of the topological simplifier
		///
		/// \return The instance of the topological simplifier
		inline SimplifTopo * getTopoSimplifier (){return simpTopo;}

		/// \brief Set the topological simplifier
		///
		///  Set the topological simplifier
		inline void setTopoSimplifier (SimplifTopo * stopo) {simpTopo=stopo;}

		/// \return The main branch of the tree structure
		inline Hierarc * getTrunc() { return trunc;}

		/// \return The main branch of class type in the tree structure
		Hierarc * getTrunc(const std::string type);

		/// \brief Set the mother plant
		inline void setPlant(class Plant * plt) {v_plt=plt;}

		/// \brief Serialize a TopoManager
		/// \param ar A reference to the archive \see Archive
		void serialize(Archive& ar );

		inline int createId(){return currentId++;};

};

#endif
