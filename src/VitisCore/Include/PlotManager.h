/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file Scheduler.h
///			\brief Definition of Scheduler.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _PLOTMANAGER_H
#define _PLOTMANAGER_H
#include "heap.h"
#include "Plant.h"
#include "ArchivableObject.h"
using namespace Vitis;




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class PlotManager
///			\brief Maintains a list of avalaible processed plants.
///
///			Maintains a list of avalaible processed plants.
///			\brief It consists mainly in a list of Plant
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT PlotManager: public ArchivableObject {


	private:

		/// \brief the time/priority sorted stack of actions to be held
		vector <Plant*> listPlant;


	public:

		/// \brief Default constructor
		PlotManager();

		/// \brief	Instance destructor
		~PlotManager();

		/** \brief Serialize the PlotManager**/
		///
		/** Serialize the PlotManager**/
		/**	\param ar A reference to the archive \see Archive */
		void serialize(Archive& ar );

		/// \brief add a plant to the list
		bool add (Plant * plt);

		/// \brief remove a plant from the list
		void remove (Plant * plt);

		/// \brief check if plt has same coordinates compared to an other plant of the plot
		bool checkCoordinates (Plant *plt);

		/// \brief return the list
		vector<Plant*> getPlants() {return listPlant;};

		static PlotManager* instance();
};

#endif



