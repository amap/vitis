/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _heap_h
#define _heap_h
#include <vector>

#define _FIRST 0
//
// This is an implementation of a binary heap taken from R. Sedgewick,
// "Algorithms in C," 3rd Ed., Vol. 1, pp. 368--375.  A few
// modifications to Sedgewick's implementation are noted below.
//
template <typename T>
class heap {
	public:
		typedef typename std::vector<T>::size_type		size_type;
		typedef typename std::vector<T>::iterator		iterator;
		typedef typename std::vector<T>::const_iterator	const_iterator;
		typedef typename std::vector<T>::reverse_iterator reverse_iterator;

	private:
		std::vector<T> a;
		void swap(T & first, T & second) {
			T tmp = first;
			first = second;
			second = tmp;
		}
		// contrary to Sedgewick's implementation, which counts elements
		// starting from 1, we start from 0.  So, in order to avoid
		// confusion and mistakes, we abstract all the position stuff with
		// the following member functions.  The compiler should optimize
		// everything out anyway.
		//
		//static const size_type _FIRST = 0;
		size_type last() const { return a.size() - 1; }
		static size_type left(size_type pos) { return pos*2 + 1; }
		static size_type right(size_type pos) { return (pos + 1)*2; }
		static size_type parent(size_type pos) { return (pos - 1)/2; }

	public:
		bool empty() { return a.empty(); }
		iterator begin() { return a.begin(); }
		iterator end() { return a.end(); }
		reverse_iterator rbegin() { return a.rbegin(); }
		reverse_iterator rend() { return a.rend(); }


		const_iterator begin() const { return a.begin(); }
		const_iterator end() const { return a.end(); }
		void clear() { a.clear(); }
		size_type size() { return a.size();}
		size_type size() const{ return a.size();}
		void resize(size_type s) { a.resize(s);}


		T & at(int i) {
			return a.at(i);
		}

		void erase (int i) {
			iterator it=a.begin();
			a.erase (it+i);
		}

		void insert(const T & x) {
			if (a.empty())
				a.push_back(x);
			else {
				iterator pos;
				int i = a.size()-1;
				pos = a.end();
				if (x < a[i]){
					a.push_back(x);
				}else{
					while (   pos != a.begin()
						   && a[i] < x) {
						pos --;
						i--;
					}
					a.insert (pos, x);
				}
			}
		}

		T pop_first() {
			if (a.empty())
				return 0;
			T res = a.at(a.size()-1);
			a.pop_back();
			return res;
		}

		//void insert(const T & x) {
		//	a.push_back(x);
		//	size_type k = last();
		//	size_type k_parent;
		//	while(k > _FIRST) {
		//		k_parent = parent(k);
		//		if (a[k] < a[k_parent]) {
		//			swap(a[k], a[k_parent]);
		//			k = k_parent;
		//		} else {
		//			return;
		//		}
		//	}
		//}

		//T pop_first() {
		//	// ASSERT _FIRST <= last().  I.e., ! empty()
		//	T res = a[_FIRST];
		//	if (_FIRST == last()) {
		//		a.pop_back();
		//		return res;
		//	}
		//	a[_FIRST] = a[last()];
		//	a.pop_back();
		//	size_type k = _FIRST;
		//	size_type k_next;
		//	for(;;) {
		//		k_next = left(k);
		//		if (k_next > last()) {
		//			break;
		//		}
		//		if (right(k) <= last() && a[right(k)] < a[k_next]) {
		//			k_next = right(k);
		//		}
		//		if (a[k_next] < a[k]) {
		//			swap(a[k], a[k_next]);
		//			k = k_next;
		//		} else {
		//			break;
		//		}
		//	}
		//	return res;
		//}


};


#endif

