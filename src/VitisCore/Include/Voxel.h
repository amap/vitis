/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  Voxel.h
///			\brief Definition of Voxel/VoxelSpace Class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _VOXEL_H
#define _VOXEL_H

#include <iostream>
using namespace std;

#include <vector>

#include "GeomElemCone.h"
#include "Plant.h"

class VoxelData : public PrintableObject
{
//	virtual void copyTo (VoxelData **v);
public :
    virtual VoxelData *duplicate(){return 0;};
    virtual std::string to_string(){return 0;};
    void *data;
};

/// \brief A map which associates a key (string) with a pointer
VITIS_EXPORT typedef std::map<std::string ,VoxelData *> MapVoxelData;
/// \brief A pair which associates a key (string) with a pointer
VITIS_EXPORT typedef std::pair<std::string ,VoxelData *> MapVoxelDataPair;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class  Voxel
///			\brief Descrition of a simple Voxel
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT Voxel
{
public:
	Voxel () {containedSurface=0;};
	~Voxel () {};
//// \brief application dependent data held by the voxel
	MapVoxelData data;
/// \brief vector of GeomElem that intersects the voxel
	vector<GeomElemCone *> content;
	vector<float> part;
	float containedSurface;
	float ContainedSurface(int i, int j, int k);
	float ContainedSurface();
    void addVoxelData (std::string criteria, VoxelData *d );
    VoxelData *getVoxelData (std::string criteria );
    VoxelData *getVoxelData (int ind );
    void copyData (MapVoxelData m);
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class  VoxelSpace
///			\brief Descrition of a simple VoxelSpace singleton \n
///			it is strongly advised to accesss it through the "instance" method.
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT VoxelSpace
{
public:
	/// \brief Construct an empty voxel space
	VoxelSpace () {space = NULL; xSize=ySize=zSize=0;xOrigin=yOrigin=0;voxelSize=-1;nbPlant=0;};
	~VoxelSpace () {reset();};
	/// \brief singleton to be able to share voxel space between multiple plant simulation
	static VoxelSpace* instance();
	/// \brief Empty the voxel space
	void reset ();
	/// \brief Remove everything that belongs to plant from voxels content
	void resetContent (Plant *plant);
	void getOrigin (float *x, float *y){*x=xOrigin; *y=yOrigin;};
	/// \brief adjust current voxelSpace so that it also fits this new one
	void adjust (float x, float y, int is, int js, int ks);
	void adjust (float x, float y, float z, int is, int js, int ks);
	float VoxelSize(){return voxelSize;};
	int ZSize(){return zSize;};
	int YSize(){return ySize;};
	int XSize(){return xSize;};
	float YOrigin(){return yOrigin;};
	float XOrigin(){return xOrigin;};
	float ZOrigin(){return zOrigin;};
	/// \brief Set the voxels size
	bool VoxelSize(float s){voxelSize = s;return true;};
	/// \return The voxel at position i, j, k
	Voxel at(int i, int j, int k);
	/// \return The voxel address at position i, j, k
	Voxel *addrAt(int i, int j, int k);
	/// \brief Find the voxels coordinate (i, j, k) containing world point (x, y, z)
	/// \return True if a voxel was found, False otherwise
	bool findVoxel (float x, float y, float z, int *i, int *j, int *k);
	/// \brief Find the set of voxels joining voxel (x1, y1) to voxel (x2, y2) accorning to Bresenham algorithm
	/// \param *vv the voxel vector containing the set of voxels
	void bresenham2D(int x1, int y1, int x2, int y2, int z, vector<Voxel*> *vv);
	/// \brief Find the set of voxels joining voxel (x1, y1, z1) to voxel (x2, y2, z2) accorning to Bresenham algorithm
	/// \param *vv the voxel vector containing the set of voxels
	void bresenham3D(int x1, int y1, int z1, int x2, int y2, int z2, vector<Voxel*> *vv);
	/// \brief return a random voxel coordinate on a vertical face of the voxel space border
	void randomInitialVoxel(int *ib, int *jb, int *kb);
	/// \brief Remove any occurence of GeomElemCone *e into the content of the voxels of the voxel space, \n
	/// remove also any GeomElemCone belonging to GeomBranc *b
	/// \return A vector containing the list of voxels containing part of *e
	vector<Voxel*> removeElem (GeomElemCone *e, GeomBranc *b);
	int NbPlant(){return nbPlant;};
	void NbPlant(int nb){nbPlant=nb;};
	void displayContent();
	/// \brief Find the coordinates of a voxel
	/// \param *v the voxel
	/// \param *i x coordinate
	/// \param *j y coordinate
	/// \param *k z coordinate
	/// \return true if the voxel was found in the voxel space, false if not
	bool voxelCoordinates (Voxel *v, int *i, int *j, int *k);
	bool voxelCoordinates (Voxel v, int *i, int *j, int *k);

	void output (const std::string& name);
	Voxel ***Space () {return space;};

private:
	/// \brief The voxel space
	Voxel ***space;
	/// \brief Side size of a voxel
	float voxelSize;
	/// \brief Size of the voxel space in x, y and z direction
	int xSize, ySize, zSize;
	/// \brief World insertion position (z=0 assumed)
	float xOrigin, yOrigin, zOrigin=0;
	/// \brief create a voxel space
	void set (float xo, float yo, float zo, int is, int js, int ks);
	/// \brief number of plants in this voxelspace
	int nbPlant;
};
#endif


