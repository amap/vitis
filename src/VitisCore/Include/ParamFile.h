/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _PARAMFILE_H
#define _PARAMFILE_H
#include <fstream>
#include <vector>
#include "File.h"
#include "VitisCoreSignalInterface.h"
#include "VitisObject.h"
using namespace std;
#include "externDll.h"



namespace Vitis
{

class Parameter;
/// \brief A map which associates a key (string) with a parameter
VITIS_EXPORT typedef std::map<std::string , Parameter *> MapParameter;
/// \brief A pair which associates a key (string) with a parameter
VITIS_EXPORT typedef std::pair<std::string , Parameter *> MapParameterPair;

enum ParamType {parametric, single, pstring};
enum messageParam {ValParam};

	/** This message will be sent to any subscriber to messageParam */
	struct paramParameter:public paramMsg /** \brief message for parameter value computing, it is part of the VitisCoreSignalInterface */
	{
		Parameter *p;/**< \brief The Parameter that is to be manipulated */
		VitisObject *o;/**< \brief The Object that requests value */
		float v;/**< \brief The current parameter value */

	public:
		paramParameter (){p=NULL;};
		paramParameter (float vv, Parameter *pp, VitisObject *oo) { v=vv;p=pp;o=oo;};
		void set (float vv, Parameter *pp, VitisObject *oo) { v=vv;p=pp;;o=oo;}
	};

	class ParameterSignal
	{
	protected:
		paramParameter pParameter;
	public:
		float sigGetParameter (float & vv, Parameter *pp, VitisObject *oo, VitisObject *subscriber)
		{
			notifier<messageParam> *t = &notifier<messageParam>::instance();

			if (t->get_nb_subscriber() == 0)
				return vv;

			pParameter.set(vv, pp, oo);

			t->notify(ValParam, &pParameter, subscriber);

			vv=pParameter.v;

			return vv;
		};
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///
	///			\class Parameter
	///			\brief Description of a parameter from reference axis
	///
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class VITIS_EXPORT Parameter : public VitisObject{
	protected :
		std::string name;
		ParamType type;
		ParameterSignal *signal;
	public :
		virtual float value(VitisObject *o=0, VitisObject *subscriber=0) = 0;
		virtual float value(float x, VitisObject *o=0, VitisObject *subscriber=0) = 0;
		virtual std::string toString () = 0;
		virtual void fromString (std::string) = 0;
		virtual std::string Type () = 0;
		void setSignal (ParameterSignal *s){signal = s;};
		void setName (std::string s){name=s;};
		std::string Name(){return name;};
		virtual void copy (Parameter *p) = 0;

	};

	class VITIS_EXPORT StringParameter : public Parameter{
	private :
		std::string val;
	public :
		virtual float value(VitisObject *o=0, VitisObject *subscriber=0) {return 0;};
		virtual float value(float x, VitisObject *o=0, VitisObject *subscriber=0) {return 0;};
		virtual std::string toString ()  {return val;};
		virtual void fromString (std::string s)  {val = s;};
		virtual std::string Type ()  {return std::string("StringParameter");};
		virtual void copy (Parameter *p) {val = ((StringParameter*)p)->val;};
	};

	class VITIS_EXPORT SingleValueParameter : public Parameter{
	private :
		float val;
	public :
		virtual float value(VitisObject *o=0, VitisObject *subscriber=0);
		virtual float value(float x, VitisObject *o=0, VitisObject *subscriber=0) {return value();};
		virtual void set(float x) {val=x;};
		virtual std::string toString () {std::ostringstream str;str<<val;return str.str();};
		virtual void fromString (std::string st){istringstream str(st);str>>val;};
		virtual std::string Type () {return std::string("SingleValueParameter");};
		virtual void copy (Parameter *p) {val = ((SingleValueParameter*)p)->val;};
	};



	class VITIS_EXPORT ParametricParameter : public Parameter{

		private:
			int						nb_X;				/**< \brief number of Main described abciss*/
			vector<float>			X;					/**< \brief list of described main abciss */
			int						interpolation_X;	/**< \brief interpolation rule between main abciss  */
			vector<int>				nb_Y;				/**< \brief number of secondary abciss for each main abciss  */
			vector<vector<float> >	Y;					/**< \brief secondary abciss values */
			vector<vector<float> >	Val;				/**< \brief parameter values */
			vector<int>				interpolation_Y;	/**< \brief interpolation rule between secondary abciss for eac main abciss values  */

			int				currentNb_Y;				/**< \brief current number of described abciss into the abciss table */
			vector<float>	currentYTab;				/**< \brief current abciss table   */
			vector<float>	currentValTab;				/**< \brief current parameter values table  */
			int				currentYIndex;				/**< \brief index of current abciss  */
			int				currentInterpolationY;		/**< \brief current interpolation rule between secondary abciss */

			float		xmin;							/**< \brief current abciss minimum value */
			float		xmax;							/**< \brief current abciss maximum value */
			float		ymin;							/**< \brief current parameter minimum value */
			float		ymax;							/**< \brief current parameter maximum value */

			float		currentX;						/**< \brief current main abciss value  */
			float		currentY;						/**< \brief current secondary abciss value */
			float		currentValue;					/**< \brief current parameter value */

		public:
			/// \brief Default constructor
			ParametricParameter();
			ParametricParameter(float d);

			/// \brief Destructor
			~ParametricParameter();

			void set (float pos, float val, bool insert=true);
			void set (float pos, float subPos, float val, bool insert=true);
			void set (int pos, float val, bool insert=true);
			void set (int pos, int subPos, float val, bool insert=true);
			int CurrentNb_Y(){return currentNb_Y;};
			int Nb_X(){return nb_X;};
			vector<float> getCurrentYTab(){return currentYTab;}
			vector<float>& getCurrentValTab(){return currentValTab;}

			void setCurrentInterpolationY (int v){currentInterpolationY = v;};
			int CurrentInterpolationY (){return currentInterpolationY;};

			virtual std::string toString ();
			std::string Type() {return std::string("ParametricParameter");};
			virtual void fromString (std::string);
			virtual void write (class ParamSet fic, float conv, float shift, float subShift);
			virtual void read (class ParamSet fic, float conv=1.0, float shift=0, float subShift=0);

					/*@{*/
			/** \return the value of the parameter at that position */
			float value (VitisObject *o=0, VitisObject *subscriber=0);

			/** @brief return value of parameter at abciss x */
			float value (float x /**<  current abciss */, VitisObject *o=0, VitisObject *subscriber=0);
			float value (int x /**<  current abciss */, VitisObject *o=0, VitisObject *subscriber=0);

			/** @brief return value of 2D parameter with abciss x,y */
			float value (float x, /**< current abciss */
						float y, /**< current subAbciss */
						VitisObject *o=0, VitisObject *subscriber=0);
			float value (int x, /**< current abciss */
						int y, /**< current subAbciss */
						VitisObject *o=0, VitisObject *subscriber=0);

			/** @brief return surface of 1D parameter between abciss debX and endX */
			float integrate (float debX, float endX, VitisObject *o=0, VitisObject *subscriber=0);
			/** @brief return surface of 2D parameter for abciss x and between abciss debY and endY */
			float integrate (float x, float debY, float endY, VitisObject *o=0, VitisObject *subscriber=0);
		/*@}*/


            double logIntegrate(double _begin, double _end, VitisObject *o=0, VitisObject *subscriber=0);

            virtual void copy (Parameter *p);

	};


	//enum V_OpenMod {V_WRITE, V_READ};

	/////////////////////////////////////////////////////////////////////////////////////////////
	///
	///			\class ParamFile opens a file and gives some method to read and write
	///
	//////////////////////////////////////////////////////////////////////////////////////////////
	class VITIS_EXPORT ParamSet : public CFile
	{

		private:

			fstream  * fic;
			MapParameter mapParameter;


			/// \brief on Parameter manipulation
			ParameterSignal *signal;

			bool valid;

		public:
		    MapParameter getMap() {
                return mapParameter;
		    }

            const std::string formatName (std::string, int ind);

            ParamSet(){
            ;
            };

			/// \brief Constructor
			ParamSet(const std::string& Name);

			/// \brief Destructor
			virtual ~ParamSet(void);

			void open(ios_base::openmode _Mode);
			void open();
			void close();

			//double logIntegrate(RootBud b, double beginPos, double endPos);


			int size();

			int readParameter();

			/*@{*/

			/** @brief read a set of parameter from file */
			MapParameter read ();
			/** @brief write a set of parameter from file */
			void write ();
			/*@}*/
			/*@{*/
			Parameter * getParameter (std::string key);
			Parameter * getParameter (std::string key, int ind);
			int addParameter (std::string key, Parameter * par);
			int removeParameter (std::string key);
			/*@}*/

			fstream *getFic() {return fic;};

			void setParameterSignal (ParameterSignal *s){signal = s;};
			ParameterSignal *parameterSignal (){return signal;};

			bool checkParameter (std::string str);

			void setValid (bool b) {valid=b;};
			bool isValid () {return valid;};
	};

	class VITIS_EXPORT ParamFactory : public VitisObject
	{
	public :
		Parameter * createParameter (std::string type);
		static ParamFactory& instance();
	};

};//end namespace


#endif

