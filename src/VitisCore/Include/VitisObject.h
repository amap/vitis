/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file VitisObject.h
///			\brief Base class Vitis objects.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __VITISOBJECT_H__
#define __VITISOBJECT_H__
#include "externDll.h"
#include <stdlib.h>
#include <string>
#include <sstream>
#include <map>

/// \define denotes that additional data has to be serialized
#define VITISARCHIVABLE 1
/// \define denotes that additional data has to be output into GLDS format
#define VITISGLDSPRINTABLE 2
/// \brief data to be dynamically added to each VitisObject
VITIS_EXPORT typedef struct {
	/// a flag to denote if data is to be serialized (VITISARCHIVABLE) output or output (VITISGLDSPRINTABLE)
	char flag;
	/// the data address
	void *data;
							} AdditionalData;

/// \brief A map which associates a key (string) with a pointer
VITIS_EXPORT typedef std::map<std::string ,AdditionalData *> MapData;
/// \brief A pair which associates a key (string) with a pointer
VITIS_EXPORT typedef std::pair<std::string ,AdditionalData *> MapDataPair;


// Struct to delete a pointer - avalaible for any type
struct Delete
{
   template <class T> void operator ()(T*& p) const
   {
      delete p;
      p = NULL;
   }
};



//Tool for convert a value to a string
	template<typename T>
std::string to_string( const T & Value )
{
	// utiliser un flux de sortie pour cr�er la cha�ne
	std::ostringstream oss;
	// �crire la valeur dans le flux
	oss << Value;
	// renvoyer une string
	return oss.str();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class VitisObject
///			\brief Base class of Vitis objects.
///
///			Base class of Vitis objects. \n
///			It basically offers capability to dynamically attach custom data
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT VitisObject
{


	public:

		/// \brief Default constructor
		VitisObject();

		/// \brief Destructor
		virtual ~VitisObject();

		/// \brief Attach data to this object with a string criteria to recover it.
		///
		/// Attach data to this object with a string criteria to recover it. \n
		/// The criteria must be unique for this object.
		/// \param criteria a string which represents the criteria
		/// \param data a pointer to the data to be stored
		/// \return 0 : data successfuly added
		/// \return 1 : this criteria is already in use
		int addAdditionalData (std::string criteria, void *data);
		/// \brief Attach data to this object with a string criteria to recover it.
		///
		/// Attach data to this object with a string criteria to recover it. \n
		/// The criteria must be unique for this object.
		/// \param criteria a string which represents the criteria
		/// \param data a pointer to the data to be stored
		/// \param flag
		/// \return \li 0 : data successfuly added
		/// \li 1 : this criteria is already in use
		int addAdditionalData (std::string criteria, void *data, char flag);

		/// \brief Return the data address associated to the criteria
		///
		/// \return \li the data address associated to the criteria \e criteria upon succes
		/// \li NULL if this criteria is not in use for this object
		void *getAdditionalData (std::string criteria );
		/// \brief Return the data address associated to the index
		///
		/// \return \li the data address associated to the index \e ind upon succes
		/// \li NULL if this criteria is not in use for this object
		void *getAdditionalData (int ind );
		/// Return the criteria associated to the index ind
		///
		/// \return \li the criteria associated to the index \e ind upon succes
		/// \li 0 if this index is not in use for this object
		std::string getAdditionalDataCriteria (int ind );
		/// \brief Check the printable property associated to data with the criteria
		///
		/// Check the printable property associated to data with the criteria \e criteria upon succes
		/// \return \li true if the to_string method may be invoked for this data
		/// \li false if this object does not offer the to_string method
		bool isAdditionalDataPrintable (std::string criteria );
		/// \brief Check the archivable property associated to data with the criteria
		///
		/// Check the archivable property associated to data with the criteria \e criteria upon succes
		/// \return \li true if the serialize method may be invoked for this data
		/// \li false if this object does not offer the serialize method
		bool isAdditionalDataArchivable (std::string criteria );
		/// \brief Check the printable property associated to data with the index
		///
		/// Check the printable property associated to data with the index \e index upon succes
		/// \return \li true if the to_string method may be invoked for this data
		/// \li false if this object does not offer the to_string method
		bool isAdditionalDataPrintable (int ind );
		/// \brief Check the archivable property associated to data with the index
		///
		/// Check the archivable property associated to data with the index \e ind upon succes
		/// \return \li true if the serialize method may be invoked for this data
		/// \li false if this object does not offer the serialize method
		bool isAdditionalDataArchivable (int ind );

		/// \brief Modify the value of the address asscociated to the criteria
		///
		/// Modify the value of the address asscociated to the crtiria \e criteria
		/// \return \li 0 : data successfuly modified
		/// \li 1 : this criteria is not in use for this object
		int modifyAdditionalData (std::string criteria, void *data);
		/// \brief Modify the value of the address and the flag asscociated to the criteria
		///
		/// Modify the value of the address and the flag asscociated to the crtiria \e criteria
		/// \return \li 0 : data successfuly modified
		/// \li 1 : this criteria is not in use for this object
		int modifyAdditionalData (std::string criteria, void *data, char flag);

		/// \brief Detach data associated to this objetc for this criteria
		///
		/// Detach data associated to this objetc for this criteria
		void removeAdditionalData (std::string criteria);

		/// Return the number of data associated to this object
		///
		/// \return the number of data associated to this object
		int	additionalDataNumber ();

		/// \brief Check if this criteria is associated to this object
		///
		/// Check if the criteria \e criteria is associated to this object
		/// \return \li true if the criteria was found
		/// \li false if the criteria was not found

        bool hasAdditionalData (std::string criteria);

	private :
		/// \brief a map which associates a criteria (string) with a pointer to store additional data
		MapData *additionalData;
		/// @cond 0
		bool hasAdditionalDataProperty (std::string criteria, char prop);
		bool hasAdditionalDataProperty (int ind, char prop);
		/// @endcond

};


#endif
