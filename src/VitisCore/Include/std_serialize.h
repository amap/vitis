/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _STD_SERIALIZE_H
#define _STD_SERIALIZE_H
/////////////////////////////////////////////////////////////
///
///		\file std_serialize.h
///		\brief serialization of collections onto archive
///
/// define operator << and >> from ar with:
///	std::list,  std::vector,    std::pair,
///	std::map,   std::hash_map,  std::set
///
///	require the value types to have << and >> operator themselves




//STL headers
#include <utility>
#include <algorithm>
#include <list>
#include <vector>
#include <map>
//#include <hash_map>
#include <set>
#include <bitset>
#include "heap.h"

#include "Archive.h"

	template <class T>
Archive& operator<<(Archive& ar, heap<T>& v)
{
	ar << (unsigned long)v.size();
	typename heap<T>::iterator I;
	for(I=v.begin(); I!=v.end(); I++)
		ar << *I;
	return ar;
}

	template <class T>
Archive& operator>>(Archive& ar, heap<T>& v)
{
	v.clear();
	unsigned long size; ar >> size;
	v.resize(size);
	typename heap<T>::iterator I = v.begin();
	while(size--)
	{
		ar >> *I;
		I++;
	}
	//ASSERT(I == v.end());
	return ar;
}

namespace std
{
	template <class T>
		Archive& operator<<(Archive& ar, const std::list<T>& lst)
		{
			ar << (unsigned long)lst.size();
			typename std::list<T>::const_iterator I;
			for(I=lst.begin(); I!=lst.end(); I++)
				ar << *I;
			return ar;
		}

	template <class T>
		Archive& operator>>(Archive& ar, std::list<T>& lst)
		{
			lst.clear();
			unsigned long size; ar >> size;
			while(size--)
			{
				T element; ar >> element;
				lst.push_back(element);
			}
			return ar;
		}

	template <class T>
		Archive& operator<<(Archive& ar, const std::vector<T>& v)
		{
			ar << (unsigned long)v.size();
			typename std::vector<T>::const_iterator I;
			for(I=v.begin(); I!=v.end(); I++)
				ar << *I;
			return ar;
		}

	template <class T>
		Archive& operator<<(Archive& ar, std::vector<T>& v)
		{
			ar << (unsigned long)v.size();
			typename std::vector<T>::iterator I;
			for(I=v.begin(); I!=v.end(); I++)
				ar << *I;
			return ar;
		}

	template <class T>
		Archive& operator>>(Archive& ar, std::vector<T>& v)
		{
			v.clear();
			unsigned long size; ar >> size;
			v.resize(size);
			typename std::vector<T>::iterator I;
			I = v.begin();
			while(size--)
			{
				ar >> *I;
				I++;
			}
			//ASSERT(I == v.end());
			return ar;
		}




	template <class T, class U>
		Archive& operator<<(Archive& ar, const std::pair<T,U>& c)
		{
			ar << c.first << c.second;
			return ar;
		}

	template <class T, class U>
		Archive& operator>>(Archive& ar, std::pair<T,U>& c)
		{
			ar >> c.first >> c.second;
			return ar;
		}

	template<class K, class T, class Pr>
		Archive& operator<<(Archive& ar, const std::map<K,T,Pr>& m)
		{
			ar << (unsigned long)m.size();
			typename std::map<K,T,Pr>::const_iterator I;
			for(I=m.begin(); I!=m.end(); I++)
				ar << *I;
			return ar;
		}

	template<class K, class T, class Pr>
		Archive& operator>>(Archive& ar, std::map<K,T,Pr>& m)
		{
			m.clear();
			unsigned long size; ar >> size;
			while(size--)
			{
				std::pair<K,T> val;
				ar >> val;
				m.insert(val);
			}
			return ar;
		}
	/*
		template<class K, class T, class Pr>
		Archive& operator<<(Archive& ar, const std::hash_map<K,T,Pr>& m)
		{
		ar << (unsigned long)m.size();
		std::map<K,T,Pr>::const_iterator I;
		for(I=m.begin(); I!=m.end(); I++)
		ar << *I;
		return ar;
		}

		template<class K, class T, class Pr>
		Archive& operator>>(Archive& ar, std::hash_map<K,T,Pr>& m)
		{
		m.clear();
		unsigned long size; ar >> size;
		while(size--)
		{
		std::pair<K,T> val;
		ar >> val;
		m.insert(val);
		}
		return ar;
		}
		*/
	template<class K, class Pr>
		Archive& operator<<(Archive& ar, const std::set<K,Pr>& s)
		{
			ar << (unsigned long)s.size();

			typename std::set<K, Pr>::const_iterator I;
			for(I=s.begin(); I!=s.end(); I++)
				ar << *I;
			return ar;
		}

	template<class K, class Pr>
		Archive& operator>>(Archive& ar, std::set<K,Pr>& s)
		{
			s.clear();
			unsigned long size; ar >> size;
			while(size--)
			{
				K val; ar >> val;
				s.insert(val);
			}
			return ar;
		}
};

#endif
