/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file ObserverManager.h
///			\brief Definition of ObserverManager.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef VITIS_OBSMANAGER_
#define VITIS_OBSMANAGER_
#include "Plugin.h"
#include "VitisObject.h"
#include <vector>
using std::vector;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class ObserverManager
///			\brief A manager to load the plugins observers
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT ObserverManager:public VitisObject
{





	public:

		/// A vector of plugins observer (CPlugin<Observer>) loaded
		vector< Vitis::CPlugin <VitisObject>  *> * myObs;

		vector<VitisObject *> vecObj;

		/// \brief Default constructor
		ObserverManager();


		/// \brief Destructor
		///
		///	All the observers are detached form the Subjects and deleted.
		~ObserverManager(void);

		/// \brief Load observers plugins.
		///
		/// Open and initialize observers plugins, and attach observers to the Subjects.
		void scanForExternalObservers(class Plant * plt);

};


#endif
