/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  GeomElem.h
///			\brief Definition of GeomElem Class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _GEOMELEM_H
#define _GEOMELEM_H
#include <vector>
#include "Matrix.h"
#include "VitisObject.h"

class GeomBranc;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GeomElem
///			\brief Abstract geometrical primitive for branch component
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT GeomElem:public VitisObject
{

	/// \brief The position of this element on its corresponding topological Branc at lowest decomposition level
	int posInBranc;

	/// \brief The position of this element on its GeomBranc container
	int posInGeomBranc;

	/// \brief The length
	double lng;

	/// \brief A pointer to its GeomBranc container
	GeomBranc * ptrBrc;

	/// \brief the transformation matrix
	///
	/// A Matrix4 is a set of 4X4 floating point values where the first three columns
	/// contains the orientation of the branc along the X,Y,Z world triedron. The last
	/// column contains the position of the translation along the X,Y,Z world triedron.
	Matrix4 	matTrans ;

	/// \brief vector of GeomElem pointers borne by this element
	std::vector<GeomBranc *> geomchild;


	public:

	//! \brief Default constructor
	GeomElem(void);

	//! \brief Destructor
	virtual ~GeomElem(void);

	//! \brief Copy the data of a GeomElem into this
	//! \param The GeomElem to copy
	virtual void copy(GeomElem * g);

	//! \brief Return the pointer to the GeomBranc container
	//! \return Pointer to the GeomBranc container
	inline GeomBranc * getPtrBrc () {return ptrBrc; }

	//! \brief Set the position of this element on its GeomBranc container
	inline void setPosInGeomBranc (int pos) {posInGeomBranc=pos;}

	//! \brief Set the position of this element on its Branc container
	inline void setPosInBranc (int pos) {posInBranc=pos;}

	//! \brief Return the position of this element on its GeomBranc container
	//! \return The position of this element on its GeomBranc container
	inline int getPosInGeomBranc () {return posInGeomBranc;}

	//! \brief Return the position of this element on its Branc container
	//! \return The position of this element on its Branc container
	inline int getPosInBranc () {return posInBranc;}

	//! \brief Set the pointer to its GeomBranc container
	void setPtrBrc (GeomBranc * gb);

	//! \brief Return the reference of the vector of GeomElem pointers borne by this element
	//! \return Reference of the vector of GeomElem pointers borne by this element
	inline std::vector<GeomBranc *> & getGeomChildren() {return geomchild;}


	//! \brief Return the length of the element
	//! \return The length of the element
	inline double getLength() { return lng;}

	//! \brief Set the length of the element
	void setLength(double l, bool sig=false);

	//! \brief Set transformation matrix
	void setMatrix(Matrix4 p);

	//! \brief Return the transformation matrix
	//! \return The transformation matrix
	inline Matrix4 & getMatrix() {return matTrans;}

	//! \brief Return the azimuth (the angle on horizontal plane) of the main direction of this element (in Radian)
	//! \return The azimuth (the angle on horizontal plane) of the main direction of this element (in Radian)
	float getAzimuth();

	//! \brief Return the angle of the main direction with the vertical direction of this element (in Radian)
	//! \return The angle of the main direction with the vertical direction of this element (in Radian)
	float getElevation();


};


#endif
