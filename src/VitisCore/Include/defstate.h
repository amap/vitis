/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _DEFSTATE_H
#define _DEFSTATE_H


#define NEWBUD 0
#define DELBUD 1
#define	OUTGEO 2
#define GROBUD 3
#define EFFBUD 4
#define NEWITN 5
#define NEWGUN 6
#define DELITN 7
#define COMPLEN 8
#define COMPDIA 9
#define DELSUBJ 10
#define DELGEOB 11
#define OUTFORMAT 12
#define NEWGEOMB 13
#define NEWGEOME 14
#define HYDROSTART 15
#define HYDROPROCESS 16
#define NEWAXE 0
#define DELAXE 1
//MODIF SG A d�finir








#endif
