/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _SERIALIZER_VITIS_H
#define _SERIALIZER_VITIS_H

#include "event.h"



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class Serializer
///			\brief Class that manage serialization process.
///
///			Class that manages serialization process. Each Plant has a member that is an instance of this class. \n
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

class VITIS_EXPORT Serializer :
	public VProcess
{

	/// \brief the plant to be serialized
	class Plant * v_plt;

	public:

	Serializer(class Plant * plt);

	virtual ~Serializer(void);

	void setDate (double date);

	//! \brief Specialized function of VProcess , launch the serialization of Plant data
	///
	//! Specialized function of VProcess , launch the serialization of Plant data into or from
	/// a file whose path is built on a configured string and the serialization time value. \n
	/// To run serialization, the simulation model has to record this VProcess into an Action to
	/// be run at the required moment. \n
	//! \see VProcess
	virtual void process_event( EventData * msg);

};

#endif
