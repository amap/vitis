/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  GeomElemCone.h
///			\brief Definition of GeomElemCone Class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _GEOMELEMCONE_H
#define _GEOMELEMCONE_H
#include "GeomElem.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GeomElemCone
///			\brief Cone geometrical primitive for branch components
///
///			In this specialization of GeomElem, the component is assumed to be a geometrical shape
///			that will be recognized through an numerical index value. To be able to simulate diameter
///			decrease along a branch, each component will have a top and a bottom diameter.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT GeomElemCone :
	public GeomElem
{

	/// \brief upper diameter correction for current cone
	float 	topDiam;

	/// \brief down diameter correction for current node
	float 	bottomDiam;

	/// \brief A identifier for the cone symbol
	int numsymb;



	public:

	//! \brief Default constructor
	GeomElemCone(void);

	//! \brief Destructor
	~GeomElemCone(void);

	//! \brief Copy the data of a GeomElem into this
	//! \param The GeomElem to copy
	virtual void copy(GeomElem * g);

	//! \brief Return upper diameter
	//! \return upper diameter
	inline float getTopDiam() {return topDiam;}

	//! \brief Return bottom diameter
	//! \return down diameter
	inline float getBottomDiam() {return bottomDiam;}

	//! \brief Return identifier for the corresponding symbol
	//! \return identifier for the cone symbol
	inline int getSymbole() { return numsymb;}

	//! \brief Set the upper diameter
	/// \param v : the diameter.
	/// \param sig : a diameter computing signal will be raised if true.
	float setTopDiam(float v, bool sig=false);

	//! \brief Set the down diameter
	inline void setBottomDiam(float v) {bottomDiam=v;}

	//! \brief Set the identifier for the cone symbol
	inline void setSymbole(int s) {numsymb=s;}





};


#endif

