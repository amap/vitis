/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file FileArchive.h
///			\brief Definition of FileArchive class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _FILE_ARCHIVE_H
#define _FILE_ARCHIVE_H
#include "Archive.h"
#include <stdio.h>
#include <string>
using namespace std;


namespace Vitis {


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///
	///			\class FileArchive
	///			\brief A class which extends the Archive class to read/write binary data from/to disk files
	///
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class VITIS_EXPORT FileArchive : public Archive
	{

		public:

			//! \brief Default constructor
			FileArchive();

			//! \brief Constructor which opens an archive and sets the version info to default values
			FileArchive( const char* filename, const char* mode );


			//! \brief Constructor which opens an archive and sets the version info
			/*! \param filename : the path to the file
			/*! \param mode must be either "r" for reading, "w" for writing, or "a" for appending (writing)
			  \param fileType should be a short descriptive name for the type of file
			  \note if \b mode is "r", then the file type and version info will be overwritten with the corresponding info from the file
			  */
			FileArchive( const char* filename, const char* mode, const char* fileType,
					unsigned int majorVersion, unsigned int minorVersion );

			//! \brief Default destructor
			virtual ~FileArchive();

			//! \brief Define the file type and version for this archive.
			/*! This info will be written to the file the next time the archive will be opened for saving. */
			inline void setVersionInfo( const char* fileType, unsigned int majorVersion,
					unsigned int minorVersion )
			{
				_fileType = fileType;
				_majorVersion = majorVersion;
				_minorVersion = minorVersion;
			}

			//! \brief Test the version info of this archive
			//!
			//! Test the version info of this archive
			//! \return \c true if the version info of this archive matches the provided info, \c false otherwise
			bool testVersionInfo( const char* fileType, unsigned int majorVersion,
					unsigned int minorVersion );

			/*! \name Version queries
			 *  These functions return the file type and version for this archive. \n They
			 *  are updated each time the archive is opened for loading. Use them to
			 *  determine whether the archive should continue to be loaded, or which version to load.
			 *  See setVersionInfo()
			 */
			//@{
			inline const char* getFileType() const { return _fileType.c_str(); }
			inline unsigned int getMajorVersion() const { return _majorVersion; }
			inline unsigned int getMinorVersion() const { return _minorVersion; }
			//@}

			//! \brief Open the archive for loading (\b mode = \b "r" ) or saving (\b mode = \b "w" or \b "a" ).
			//!
			//! Open the archive for loading
			//! \param filename : the path to the file
			//! \param mode : "r" for reading,   "w" or "a" for saving.
			//! \return \c true if the archive was opened successfully, \c false otherwise.
			bool open( const char* filename, const char* mode );

			//! \brief Test if the archive is open for reading or writing
			//!
			//! Test if the archive is open for reading or writing
			//! \return \c true if the archive is open for reading or writing, \c false otherwise.
			inline bool valid()
			{ return (file!=0); }

			//! \brief Close the archive.
			//!
			//! Close the archive.
			void close();

			//! \brief Return the name of the open file the archive is writing/reading.
			//!
			//! \return the path of the open file taht the archive is writing/reading to, or the empty string if no file is open
			inline const char* getFilename() { return _filename.c_str(); }


		protected:

			//! \brief Implementation override of Archive::onWrite()
			//!
			//! Implementation override of Archive::onWrite()
			//! \param p : data to be written.
			//! \param size : size in bytes of data to be written.
			bool onWrite( const void* p, int size );

			//! \brief Implementation override of Archive::onRead()
			//!
			//! Implementation override of Archive::onRead()
			//! \param p : data to be read.
			//! \param size : size in bytes of data to be read.
			bool onRead( void* p, int size );

			//! \brief Current file handler got from open().
			FILE* file;

			string _fileType;

			string _filename;

			unsigned int _majorVersion, _minorVersion;

	}; /* end class FileArchive */

} /* end namespace Vitis */

#endif
