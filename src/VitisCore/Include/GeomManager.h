/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  GeomManager.h
///			\brief Definition of the manager class of geometry GeomManager and super simplification class GeomSuperSimpl
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _GEOMMANAGER_H
#define _GEOMMANAGER_H
#include "GeomBranc.h"
#include <vector>




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GeomSuperSimpl
///			\brief Class which associates an axe Branc (topology) to his geometrical representations sorted by angular sector
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT GeomSuperSimpl:public VitisObject {

	public:

		/// \brief Pointer to the topological corresponding axe
		class Branc * b;

		/// \brief Array of GeomBranc indices for each vertical angle sector ( number of sector defined by cfg_supersimplif
		///
		///	Each cell of this array corresponds to indices of GeomBranc instance in geomBrancStack, see GeomManager (-1 by default).
		/// \n The indices in this array correspond to the sectors in descending order.
		int * secteurAng ;

		/// \brief Constructor
		/// \param bp Pointer to the corresponding topological axe
		GeomSuperSimpl(class Branc * bp);

		/// \brief Destructor
		virtual ~GeomSuperSimpl();


};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GeomManager
///			\brief Manage the geometrical computing for each axe (Branc). Each plant has a member instance of this class.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT GeomManager:public VitisObject
{
	public:

		/// \brief Constructor
		/// \param plt Pointer to the plant
		GeomManager(class Plant * plt=NULL);

		/// \brief Destructor
		~GeomManager(void);


		/// name Accessors
		/// @4
		/// \return The topological manager
		TopoManager & getTopoManager () ;

		/// \return The stack of GeomBranc
		inline std::vector<GeomBranc *> & getGeomBrancStack(){return geomBrancStack;}

		inline std::vector<GeomSuperSimpl *> & getGeomSimplStack(){return geomStack;}

		inline Plant * getPlant(){return v_plt;}

		/// \return The number of GeomElem instance
		inline int & getElemeGeoNumber () { return elemGeoNumber;}

		/// \return The maximum identifier number of GeomElem
		inline int & getElemeGeoMaxNumber () { return elemGeoMaxNumber;}

		/// \return The number of GeomBranc instance
		inline int & getGeomBrancNumber () { return geoBrNumber;}

		/// \return The minimum coordinates of the plant
		inline Vector3 getMin () {return mino;}

		/// \return The maximum coordinates of the plant
		inline Vector3 getMax () {return maxo;}

		/// \return The translation position vector of the plant
		inline const Vector3 & getPosition () {return position;}

		/// \return Set the position of the plant
		inline void setPosition  (const Vector3 & p) {position.set(p.x(),p.y(),p.z());}

		/// \brief Set the plant pointer
		inline void setPlant(class Plant * plt) {v_plt=plt;}

		/// \brief Compute the angular sector of the first element of \li gb which is the geometry of the axe identified by \li id_br
		/// \param gb The current GeomBranc which call that function to get an instance of a simplfied branch
		/// \param id_br The identifier of the current topological axe
		/// \return An instance of GeomBranc to paste (instead of computing all geometry of gb)
		GeomBranc * getSuperSimplifedGeom(GeomBranc * gb, int id_br);
		/// @}

		/// \brief Compute all the geometry axes of the plant and run geometrical simplification
		void computeTreeGeometry (bool silent=false);

		/// \brief Compute a bounding box according to the trunk height and the order 1 branches
		void computeRawBoundingBox();


		/// \brief Compute a new GeomBranc  for the topological axe \li b
		/// \param b An axe of the hierarchy
		/// \param gbr The geometrical bearer axe (GeomBranc)
		/// \param maxOrder The maximum branching order that will have its geometry computed (trunk is 0).
		void computeGeometryForAxe(class Hierarc * b=NULL, bool silent=false, GeomBranc * gbr=NULL, short maxOrder=32767);

		/// \brief Compute the bounding box of the plant
		void updateBoundingBox(GeomElem * ge);

		/// \brief Update the collection of geometrical representations ( sorted by angular sector) of an axe
		/// \param gb The GeomBranc instance to add for the axe (if there is no instance with the same sector of gb)
		/// \param id_br The identifier of the current topological axe
		void updateGeomSuperSimpl(GeomBranc * gb,int id_br);

		/// \brief Update the counter of GeomElem instance
		/// \param ajout true : increment counter , false : decrement counter
		void updateElemNumber(bool ajout);

		/// \brief Add a new instance of GeomBranc in \li geomBrancStack
		/// \param gb The GeomBranc instance to add
		void addGeomBranc(GeomBranc * gb);

		/// \brief Get the GeomElem instance for a given GeomBranc identifier and a position in this axe
		/// \param idBr The identifier of the Branc
		/// \param idElem The element's position in the geometrical axe
		GeomElem * getGeomElem( int idBr , int idElem );


		/// \brief Delete all plant'geometry except GeomBrancStack
		void clear();
		/// \brief Delete GeomBrancStack
		void clearGeomBrancStack();

		void HasToBeComputed (bool htbc);

	private:
		/// \brief GeomBranc is a friend class
		friend class GeomBranc;

		/// \brief The number of geometrical element (GeomElem) of the current plant
		int elemGeoNumber;

		/// \brief The maximum index of geometrical element (GeomElem) of the current plant (can be > of elemGeoNumber after simplification)
		int elemGeoMaxNumber;

		/// \brief The number of geometrical Axe (GeomBranc) of the current plant
		int geoBrNumber;


		///	\brief The maximum coordinates of the plant
		///
		/// Used to compute bounding box
		Vector3 maxo;

		///	\brief The minimum coordinates of the plant
		///
		/// Used to compute bounding box
		Vector3 mino;

		/// \brief Translation to the current position of the plant into the plantation
		Vector3 position;

		/// \brief stack of geometrical axes
		std::vector<GeomBranc *> geomBrancStack;

		/// \brief stack of all possible geometrical representations for each simplified axe
		///
		/// The indices in this vector are the v_idbranc of corresponding topological Branc
		std::vector<GeomSuperSimpl *> geomStack;

		/// \brief Pointer to the mother plant
		class Plant * v_plt;

        bool hasToBeComputed;


};


#endif
