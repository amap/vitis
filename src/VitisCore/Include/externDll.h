/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#if !defined STATIC_LIB

#ifndef __EXPORTDLL_H__
#define __EXPORTDLL_H__


#if !defined STATIC_LIB
	#if defined WIN32

#pragma warning ( disable : 4251 )

			#ifdef VITIS_EXPORTS
				#define VITIS_EXPORT __declspec(dllexport)
			#else
				#define VITIS_EXPORT __declspec(dllimport)
			#endif


	#else
			#define VITIS_EXPORT
	#endif

#else
		#define VITIS_EXPORT
#endif



#endif

#else
	#define VITIS_EXPORT
#endif

#ifndef WIN32
#define HMODULE void*
#endif
