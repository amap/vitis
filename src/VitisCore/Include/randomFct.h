/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file RandomFct.h
///			\brief Definition of functions to compute probabilty laws
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _RANDOM_H
#define _RANDOM_H
#include <stdlib.h>
#include "defVitis.h"
#include "externDll.h"
#include "Plant.h"





/** This function process a two state markov process
 * \return the new state value */
VITIS_EXPORT long markovSimulation2( Plant * plt, long old_state /**< the previous state value (-1 means no previous state) */,
		float proba_in /**< initial probability for state 0 */,
		float proba1 /**< state 0 to state 1 probability */,
		float proba2 /**< state 1 to state 0 probability */,
		long randomtype/**< old fashioned */);

/** This function process a cumulative law according to binomial law
 * \return the choosen state
 * */
VITIS_EXPORT long  cumulativeLaw( Plant * plt,float *proba /**< probability for each state */,
		long nbstate /**< number of posible states */,
		long randomtype/**< old fashioned */);

/** This function returns the result of a Bernouilli process
 * \return \li 1 : success
 * \li 0 : fail
 * */

VITIS_EXPORT long bernouilli ( Plant * plt, float p /**< probability of Bernouilli law */);

/** This function returns the result of a Binomial (negative) shifted process
 * \return \li number of success for positive law
 * \li number of fail for negative law
 * */
VITIS_EXPORT long binodec ( Plant * plt,
		float n,  /**< test number (n<0 for negative law)*/
		float p, /**< probability for each test */
		long d);  /**< shift */

VITIS_EXPORT long binodecf ( Plant * plt,
		float n,  /**< test number (n<0 for negative law)*/
		float p, /**< probability for each test */
		long d);  /**< shift */



#endif


