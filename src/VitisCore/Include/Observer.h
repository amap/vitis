////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file  Observer.h																					
///			\brief Definition of the Observer class 
///																											
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __OBSERVER_H__
#define __OBSERVER_H__
#include "VitisObject.h"
#include <string>


class Subject;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class Observer																				
///			\brief Describe an observer .
///			\see Subject
///			\warning Pattern Observer
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT Observer:public VitisObject
{
	protected:

		//! \brief An identifier
		short o_id;


	public:
		//! \brief Static counter of instancied Observer
		static  short  o_cptid;

		//! \brief Default constructor
		Observer();


		//! \brief Destructor
		virtual ~Observer(void) {};

		//! \brief This function is called by a Subject observed by this
		/*! \param pSubject The caller subject 
		  \param param A table of void pointer */
		virtual bool update(Subject * pSubject, void ** param);

		//! \return The identifier of the instance
		inline short getID(){ return o_id;}


};


#endif
