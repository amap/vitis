/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  Output.h
///			\brief Definition of the OutputManager and Output class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __OUTPUT_H__
#define __OUTPUT_H__
//#include <vector>
#include "Plugin.h"
#include "event.h"
#include "OutputFormat.h"
#include "Notifier.h"
using std::vector;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class Output
///			\brief Mother class to output simulation results. \n Most of its functions are virtual and must be
///				   implemented by the descendant classes.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT Output : public  VProcess
{

	protected:

		//! \brief Pointer to the output data format
		OutputFormat * outFormat;

		//! \brief Path to the output file
		std::string filename;

		/// \brief Plant to be output
		class Plant * v_plt;

		/// \brief Current number of done output
		int cpt;

		/// \brief Output step
		double step;

		/// \brief Binary writing type (if needed). True stands for BigEndian.
		bool swap;

	public:

		//! \brief Default constructor
		Output();

		//! \brief Destructor
		virtual ~Output(void);

		//! \brief Do the output data computing and writing.
		///
		//! Do the output data computing and writing.
		virtual void output();

		//! \brief Specialized function of VProcess , launch the output data computing and writing.
		///
		//! Specialized function of VProcess , launch the output data computing and writing. Schedule the
		/// next output according to step.
		//! \see VProcess
		virtual void process_event(EventData * msg);

		//! \brief Compute data to be output
		///
		//! Compute data to be output. \n
		/// By default this function does nothing. Each particular descendant output will have to implement this function.
		virtual void computeDataToOutput (){};

		//! \brief Set the current format (see OutputFormat)
		virtual void setOutputFormat (OutputFormat * outFor);

		//! \brief Free output data
		///
		//! Free output data. \n
		/// By default this function does nothing. Each particular descendant output will have to implement this function.
		virtual void freeData (){};

		//! \brief Write output data
		///
		//! Write output data. \n
		/// By default this function does nothing. Each particular descendant output will have to implement this function.
		virtual void writeData (const std::string & ){};

		//! \return Output file path
		const std::string & getFileName();

		//! \brief Set the path to the output file
		void setFileName(const std::string & name);

		//! \brief Set the pointer on the given plant
		void setPlant(class Plant * plt);

		//! \brief Set the output time step
		void setStep(double s);

		//! \brief Set the swap flag
		void setSwap(bool s){swap=s;};

		//! \brief Get the swap flag
		bool Swap(){return swap;};

		class Plant * getPlant(){return v_plt;};

};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class OutputManager
///			\brief Manage the output instanciation
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//class VITIS_EXPORT OutputManager: public VitisObject,public singleton<OutputManager>
class VITIS_EXPORT OutputManager: public VitisObject
{

	/// \brief Helper to use dynamic library
	vector< Vitis::CPlugin<Output> >  s_Library;

	/// \brief Vector of Output instance
	vector<Output *> outpVec;



	public:

	//! \brief Default constructor
	OutputManager();

	//! \brief Destructor
	virtual ~OutputManager(void);

	//! \brief Add a new kind of output
	///
	//! Add a new kind of output to s_Library. \n
	/// Basically the dynamic library to be loaded must contain a CPlugin. This means that it has to
	/// provide a "StartPlugin" function (see CPlugin). This function must return an instance
	/// of an Output descendant class.
	//! \param dllName dll file path where the specialized output class is defined
	void addOutputDevice (const std::string& dllname);

	//! \brief Create a new Output instance
	///
	//! Create a new Output instance. \n
	/// It runs the StartPlugin function of the output that was loaded in indexDevice position and store
	/// the returned Output into outpVec.
	//! \param indexDevice index into s_Library to select the kind of output wanted
	//! \param filename output filename
	//! \param swap little/big endian toggle
	Output * create_output(class Plant * plt, int indexDevice, const std::string filename, bool swap=false);

	//! \brief Schedule an Output instance
	///
	//! Schedule an Output instance. \n
	/// It creates an action that is stored into the plant scheduler action stack.
	//! \param outp Pointer to the output instance to schedule
	//! \param step output step time
	void scheduler_output(Plant * plt, Output * outp, double step);

	//! \brief Create a new Output instance and schedule it
	///
	//! Create a new Output instance and schedule it. \n
	/// It does the sequence of : addOutputDevice, create_output and scheduler_output.
	//! \param dllName dll filename where is defined the specialized output class
	//! \param filename output filename
	//! \param step output step time
	//! \param swap little/big endian toggle
	//! \return The index of output device into the device stack
    int create_and_schedule_output(Plant * plt, const std::string& dllname, const std::string& filename, double step, bool swap=false);

	//! \brief Create a new Output instance and schedule it
	///
	//! Create a new Output instance and schedule it. \n
	/// It does the sequence of : create_output and scheduler_output.
	//! \param indexDevice index into s_Library to select the kind of output wanted
	//! \param filename output filename
	//! \param swap little/big endian toggle
	//! \param step output step time
	void create_and_schedule_output(Plant * plt, int indexDevice, const std::string& filename, double step, bool swap=false);

	//! \return The number of output device ( kind of output)
	int getNumberOfOutputDevice ();

	void clean (Plant *plt);

    static OutputManager* instance();

};





#endif
