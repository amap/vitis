/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file VitisCoreSignalInterface.h
///			\brief Definition of the signal interface provided by VitisCore.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _VITISCORESIGNALINTERFACE_
#define _VITISCORESIGNALINTERFACE_
#include "externDll.h"
#include "Notifier.h"
#include "DecompAxeLevel.h"
#include "GeomManager.h"
#include "GeomBrancCone.h"
#include "GeomElemCone.h"
using namespace Vitis;


//
//
/** \name set of signals definition */
/*\{*/
enum messageVitisPlant {SigPlant };
enum messageVitisPlantDelete {SigPlantDelete };
enum messageVitisDecompAxeLevel {SigDecompAxeLevel, SigDelDecompAxeLevel };
enum messageVitisHierarc {SigHierarc };

enum messageVitisBeginGeomCompute {SigBeginGeomCompute};
enum messageVitisEndGeomCompute {SigEndGeomCompute};
enum messageVitisBeginGeomBrancCone {SigBeginGeomBrancCone};
enum messageVitisEndGeomBrancCone {SigEndGeomBrancCone};
enum messageVitisGeomElemCone {SigGeomElemCone};
enum messageVitisElemDiameter {SigElemDiameter};
enum messageVitisElemLength {SigElemLength};
enum messageVitisElemPosition {SigElemPosition};
enum messageVitisElemGeometry {SigElemGeometry};
enum messageVitisComputingMonitoring {SigTopoComputing, SigGeomComputing};
/*\}*/

/// \name VitisCoreSignalInterface message list
/// @{
/** \brief message for computing monitoring it is part of the VitisCoreSignalInterface */
/** This message will be sent to any subscriber to messageVitisComputingMonitoring */
struct paramComputingMonitoring:public paramMsg
{
	float val; /**< \brief The computing value */

	paramComputingMonitoring (){};
	paramComputingMonitoring (float v) { val = v;};
	void set (float v) { val = v;}
};
/** \brief message for plant creation it is part of the VitisCoreSignalInterface */
/** This message will be sent to any subscriber to messageVitisPlant */
struct paramPlant:public paramMsg
{
	Plant *p; /**< \brief The plant that is to be created */

	paramPlant (){p=NULL;};
	paramPlant (Plant *pp) { p=pp;};
	void set (Plant *pp) { p=pp;}
};
/** \brief message for plant deletion it is part of the VitisCoreSignalInterface */
/** This message will be sent to any subscriber to messageVitisPlantDelete */
struct paramPlantDelete:public paramMsg
{
	Plant *p; /**< \brief The plant that is to be created */

	paramPlantDelete (){p=NULL;};
	paramPlantDelete (Plant *pp) { p=pp;};
	void set (Plant *pp) { p=pp;}
};
/** This message will be sent to any subscriber to messageVitisDecompAxeLevel */
struct paramDecompAxeLevel:public paramMsg /** \brief message for decompAxeLevel creation/deletion it is part of the VitisCoreSignalInterface */
{
	DecompAxeLevel *d;/**< \brief The Decomposition Element that is to be created */

	paramDecompAxeLevel (){d=NULL;};
	paramDecompAxeLevel (DecompAxeLevel *dd) { d=dd;};
	void set (DecompAxeLevel *dd) { d=dd;}
};
/** This message will be sent to any subscriber to messageVitisHierarc */
struct paramHierarc:public paramMsg /** \brief message for Hierarc creation it is part of the VitisCoreSignalInterface */
{
	Hierarc *h;/**< \brief The Hierarchy element that is to be created */

	paramHierarc (){h=NULL;};
	paramHierarc (Hierarc *hh) { h=hh;};
	void set (Hierarc *hh) { h=hh;}
};

/** This message will be sent to any subscriber to messageVitisBegingeomCompute and messageVitisEndGeomCompute */
struct paramGeomCompute:public paramMsg /** \brief message at begining and end of geometry computing it is part of the VitisCoreSignalInterface */
{
	GeomManager *gm;/**< \brief The Geometry manager that will handle geometry computing */

	paramGeomCompute (){gm=NULL;};
	paramGeomCompute (GeomManager *g) { gm=g;};
	void set (GeomManager *g) { gm=g;}
};

/** This message will be sent to any subscriber to messageVitisGeomBrancCone */
struct paramGeomBrancCone:public paramMsg /** \brief message for creation of geometrical branc it is part of the VitisCoreSignalInterface */
{
	GeomBrancCone *gbc;/**< \brief The geometrical branch that is to be created */

	paramGeomBrancCone (){gbc=NULL;};
	paramGeomBrancCone (GeomBrancCone *b) { gbc=b;};
	void set (GeomBrancCone *b) { gbc=b;}
};

/** This message will be sent to any subscriber to messageVitisGeomElemCone */
struct paramGeomElemCone:public paramMsg /** \brief message for creation of geometrical branc element it is part of the VitisCoreSignalInterface */
{
	GeomElemCone *gec;/**< \brief The geometrical element that is to be created */

	paramGeomElemCone (){gec=NULL;};
	paramGeomElemCone (GeomElemCone *e) { gec=e;};
	void set (GeomElemCone *e) { gec=e;}
};

/** This message will be sent to any subscriber to messageVitisElemLength and messageVitisElemDiameter */
struct paramElemGeom:public paramMsg /** \brief message for length and diameter computing it is part of the VitisCoreSignalInterface */
{
	GeomElemCone *gec;/**< \brief The geometrical element whose geometry is computed */
	double v;/**< the current computed geometrical value */

	paramElemGeom (){gec=NULL;};
	paramElemGeom (GeomElemCone *e, float vv) { gec=e;v=vv;};
	void set (GeomElemCone *e, float vv) { gec=e;v=vv;}
};

/** This message will be sent to any subscriber to messageVitisElemPosition it is part of the VitisCoreSignalInterface */
struct paramElemPosition:public paramMsg /** \brief message for position and orientation computing it is part of the VitisCoreSignalInterface */
{
	GeomElemCone *gec;/**< \brief The geometrical element that is to be created */
	Matrix4 p;/**< \brief The current position matrix for this element */

	paramElemPosition (){gec=NULL;};
	paramElemPosition (GeomElemCone *e, Matrix4 pp) { gec=e;p=pp;};
	void set (GeomElemCone *e, Matrix4 pp) { gec=e;p=pp;}
};

/** This message will be sent to any subscriber to messageVitisElemGeometry it is part of the VitisCoreSignalInterface */
struct paramElemGeometry:public paramMsg /** \brief message for position and orientation computing it is part of the VitisCoreSignalInterface */
{
	GeomElemCone *gec;/**< \brief The geometrical element that is to be created */
	Matrix4 p;/**< \brief The current position matrix for this element */

	paramElemGeometry (){gec=NULL;};
	paramElemGeometry (GeomElemCone *e, Matrix4 pp) { gec=e;p=pp;};
	void set (GeomElemCone *e, Matrix4 pp) { gec=e;p=pp;}
};

/// @}

//Astuce car j'avais un probl�me de liaison (�vident apr�s coup) avec les dll ..un template ne peut pas s'exporter,
//donc on d�fini une instanciation de ce template que l'on exporte ...tous les message devront �tre de type typeMsg
/** @name set of notifiers provided by Vitis
* Any object that want to connect to one of these notifier should implement a subclass from the corresponding subscriber
* that calls subscribe() on creation, unsubscribe() on deletion and that implements the virtual on_notify() method
* and then declare an instance of that subclass into its members \n
* For instance : \n
* \code
* struct  Diamzz:public subscriber<messageVitisElemDiameter>
* {
*    Diamzz () { subscribe();};
*    virtual ~Diamzz () {unsubscribe();};
*    void on_notify (const state& st, paramMsg * data=NULL) {;};
* };
* struct MyDiameterPlugin
* {
*    MyDiameterPlugin();
*    Diamzz d;
* };
* \endcode*/

/*@{*/
template class VITIS_EXPORT notifier<messageVitisPlant>;
template class VITIS_EXPORT notifier<messageVitisPlantDelete>;
template class VITIS_EXPORT notifier<messageVitisDecompAxeLevel>;
template class VITIS_EXPORT notifier<messageVitisHierarc>;
template class VITIS_EXPORT notifier<messageVitisBeginGeomCompute>;
template class VITIS_EXPORT notifier<messageVitisEndGeomCompute>;
template class VITIS_EXPORT notifier<messageVitisBeginGeomBrancCone>;
template class VITIS_EXPORT notifier<messageVitisEndGeomBrancCone>;
template class VITIS_EXPORT notifier<messageVitisGeomElemCone>;
template class VITIS_EXPORT notifier<messageVitisElemDiameter>;
template class VITIS_EXPORT notifier<messageVitisElemLength>;
template class VITIS_EXPORT notifier<messageVitisElemPosition>;
template class VITIS_EXPORT notifier<messageVitisElemGeometry>;
template class VITIS_EXPORT notifier<messageVitisComputingMonitoring>;
/*@}*/



/** \brief this structure encapsulates every signals that are notified to subscribers. \n
 * Every plant owns an instance of it */
struct VitisCoreSignalInterface
{
	VitisCoreSignalInterface() {};
	~VitisCoreSignalInterface() {};
/** @name instances of the messages that will be sent (see corresponding structure description)*/
/**\{*/
	/// \brief on plant creation
	paramComputingMonitoring pComputingMonitoring;
	/// \brief on plant creation
	paramPlant pPlant;
	/// \brief on plant deletion
	paramPlantDelete pdPlant;
	/// \brief on DecompAxeLevel creation
	paramDecompAxeLevel pDecompAxeLevel;
	/// \brief on branch creation
	paramHierarc pHierarc;
	/// \brief on geometrical computing begin and end
	paramGeomCompute pGeomCompute;
	/// \brief on geometrical branch creation
	paramGeomBrancCone pGeomBrancCone;
	/// \brief on geometrical branch component creation
	paramGeomElemCone pGeomElemCone;
	/// \brief on geometrical property computing (length, diameter, position)
	paramElemGeom pGeomElem;
	/// \brief on position and orientation computing
	paramElemPosition pElemPosition;
	/// \brief on element geometry computing
	paramElemGeometry pElemGeometry;
/**\}*/

/** \brief Signal that notifies upon plant creation */
	Plant* sigPlant (Plant *pp)
	{
        notifier<messageVitisPlant> *t= &(notifier<messageVitisPlant>::instance());
		if (t->get_nb_subscriber() == 0)
			return pp;

		pPlant.set(pp);

		t->notify(SigPlant, &pPlant, pp);

		pp=pPlant.p;

		return pp;
	};
/** \brief Signal that notifies upon plant deletion */
	Plant* sigPlantDelete (Plant *pp)
	{
        notifier<messageVitisPlantDelete> *t= &(notifier<messageVitisPlantDelete>::instance());

		if (t->get_nb_subscriber() == 0)
			return pp;

		pdPlant.set(pp);

		t->notify(SigPlantDelete, &pdPlant, pp);

		pp=pdPlant.p;

		return pp;
	};
/** \brief Signal that notifies upon topolgy computing progress */
	void sigMonitorTopology (float v)
	{
        notifier<messageVitisComputingMonitoring> *t= &(notifier<messageVitisComputingMonitoring>::instance());

		if (t->get_nb_subscriber() == 0)
			return ;

		pComputingMonitoring.set(v);

		t->notify(SigTopoComputing, &pComputingMonitoring);

		return;
	};
/** \brief Signal that notifies upon geometry computing progress */
	void sigMonitorGeometry (float v)
	{
        notifier<messageVitisComputingMonitoring> *t= &(notifier<messageVitisComputingMonitoring>::instance());

		if (t->get_nb_subscriber() == 0)
			return ;

		pComputingMonitoring.set(v);

		t->notify(SigGeomComputing, &pComputingMonitoring);

		return;
	};
/** \brief Signal that notifies upon topological decomposition element creation */
	DecompAxeLevel* sigDecompAxeLevel (DecompAxeLevel *dd)
	{
        notifier<messageVitisDecompAxeLevel> *t= &(notifier<messageVitisDecompAxeLevel>::instance());

		if (t->get_nb_subscriber() == 0)
			return 0;

		pDecompAxeLevel.set(dd);

		t->notify(SigDecompAxeLevel, &pDecompAxeLevel, dd->getPlant());

		dd=pDecompAxeLevel.d;

		return dd;
	};
/** \brief Signal that notifies upon topological decomposition element deletion */
	DecompAxeLevel* sigDelDecompAxeLevel (DecompAxeLevel *dd)
	{
      notifier<messageVitisDecompAxeLevel> *t= &(notifier<messageVitisDecompAxeLevel>::instance());

		if (t->get_nb_subscriber() == 0)
			return 0;

		pDecompAxeLevel.set(dd);

		t->notify(SigDelDecompAxeLevel, &pDecompAxeLevel, dd->getPlant());

		dd=pDecompAxeLevel.d;

		return dd;
	};
/** \brief Signal that notifies upon hierarchical element creation */
	Hierarc* sigHierarc (Hierarc *hh)
	{
        notifier<messageVitisHierarc> *t= &(notifier<messageVitisHierarc>::instance());

		if (t->get_nb_subscriber() == 0)
			return 0;

		pHierarc.set(hh);

		t->notify(SigHierarc, &pHierarc, hh->getBranc()->getPlant());

		hh=pHierarc.h;

		return hh;
	};
/** \brief Signal that notifies upon geometrical branch creation */
	GeomBrancCone* sigBeginGeomBrancCone (GeomBrancCone *b)
	{
        notifier<messageVitisBeginGeomBrancCone> *t= &(notifier<messageVitisBeginGeomBrancCone>::instance());

		if (t->get_nb_subscriber() == 0)
			return b;

		pGeomBrancCone.set(b);

		t->notify(SigBeginGeomBrancCone, &pGeomBrancCone, b->getBranc()->getPlant());

		b=pGeomBrancCone.gbc;

		return b;
	};

	/** \brief Signal that notifies upon geometrical branch creation */
	GeomBrancCone* sigEndGeomBrancCone (GeomBrancCone *b)
	{
        notifier<messageVitisEndGeomBrancCone> *t= &(notifier<messageVitisEndGeomBrancCone>::instance());

		if (t->get_nb_subscriber() == 0)
			return b;

		pGeomBrancCone.set(b);

		t->notify(SigEndGeomBrancCone, &pGeomBrancCone, b->getBranc()->getPlant());

		b=pGeomBrancCone.gbc;

		return b;
	};

/** \brief Signal that notifies upon geometrical element of a branch creation */
	GeomElemCone* sigGeomElemCone (GeomElemCone *e)
	{
        notifier<messageVitisGeomElemCone> *t= &(notifier<messageVitisGeomElemCone>::instance());

		if (t->get_nb_subscriber() == 0)
			return e;

		pGeomElemCone.set(e);

		t->notify(SigGeomElemCone, &pGeomElemCone, e->getPtrBrc()->getBranc()->getPlant());

		e=pGeomElemCone.gec;

		return e;
	};
/** \brief Signal that notifies upon diameter of a geometrical element computing */
	double sigElemDiameter (GeomElemCone *e, double d)
	{
        notifier<messageVitisElemDiameter> *t= &(notifier<messageVitisElemDiameter>::instance());

		if (t->get_nb_subscriber() == 0)
			return d;

		pGeomElem.set(e, (float)d);

		t->notify(SigElemDiameter, &pGeomElem, e->getPtrBrc()->getBranc()->getPlant());

		d=pGeomElem.v;

		return d;
	};
/** \brief Signal that notifies upon length of a geometrical element computing */
	double sigElemLength (GeomElem *e, double d)
	{
        notifier<messageVitisElemLength> *t= &(notifier<messageVitisElemLength>::instance());

		if (t->get_nb_subscriber() == 0)
			return d;

		pGeomElem.set((GeomElemCone*)e, (float)d);

		t->notify(SigElemLength, &pGeomElem, e->getPtrBrc()->getBranc()->getPlant());

		d=pGeomElem.v;

		return d;
	};
/** \brief Signal that notifies upon position of a geometrical element computing */
	Matrix4 sigElemPosition (GeomElem *e, Matrix4 p)
	{
        notifier<messageVitisElemPosition> *t= &(notifier<messageVitisElemPosition>::instance());

		if (t->get_nb_subscriber() == 0)
			return p;

		pElemPosition.set((GeomElemCone*)e, p);

		t->notify(SigElemPosition, &pElemPosition, e->getPtrBrc()->getBranc()->getPlant());

		p=pElemPosition.p;

		return p;
	};
/** \brief Signal that notifies upon end of geometry element computing */
	Matrix4 sigElemGeometry (GeomElem *e, Matrix4 p)
	{
        notifier<messageVitisElemGeometry> *t= &(notifier<messageVitisElemGeometry>::instance());

		if (t->get_nb_subscriber() == 0)
			return p;

		pElemGeometry.set((GeomElemCone*)e, p);

		t->notify(SigElemGeometry, &pElemGeometry, e->getPtrBrc()->getBranc()->getPlant());

		p=pElemGeometry.p;

		return p;
	};
/** \brief Signal that notifies at begining of a plant geometry computing */
	void sigBeginGeomCompute (GeomManager *g)
	{
        notifier<messageVitisBeginGeomCompute> *t= &(notifier<messageVitisBeginGeomCompute>::instance());

		if (t->get_nb_subscriber() == 0)
			return;

		pGeomCompute.set(g);

		t->notify(SigBeginGeomCompute, &pGeomCompute, g->getPlant());
	}
/** \brief Signal that notifies at end of a plant geometry computing */
	void sigEndGeomCompute (GeomManager *g)
	{
        notifier<messageVitisEndGeomCompute> *t= &(notifier<messageVitisEndGeomCompute>::instance());

		if (t->get_nb_subscriber() == 0)
			return;

		pGeomCompute.set(g);

		t->notify(SigEndGeomCompute, &pGeomCompute, g->getPlant());
	}
};


#endif

