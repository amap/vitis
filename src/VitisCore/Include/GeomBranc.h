/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  GeomBranc.h
///			\brief Definition of GeomBranc Class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _GEOMBRANC_H
#define _GEOMBRANC_H
#include "GeomElem.h"
#include "Topology.h"



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GeomBranc
///			\brief Abstract geometry of an axe
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT GeomBranc:public VitisObject
{



	protected:

		/// \brief	Scale
		float 	scale;

		/// \brief The insertion angle of the axe on his bearer
		float 	insertionAngle;

		/// \brief The geometrical random value
		float 	alea;

		/// \brief An identifier for GeomBranc instances
		int v_idGeomBranc;

		/// \brief A pointer to the axe into the Hierarchy
		Hierarc * hierarc;

		/// \brief A pointer to the mother plant
		class Plant * v_plt;

		/// \brief A pointer to the geometrical bearer axe
		GeomBranc * gbearer;

		/// \brief Position on the geometrical bearer axe
		int posOnBearer;

		/// \brief A vector of pointer to the geometrical axe components
		std::vector<GeomElem *> elemGeo;



	public:


		//! \brief Constructor
		/*! \param brc Pointer to the hierarchical axe
		  \param gbear Pointer to the geometrical bearer axe
		  \param plt Pointer to the mother Plant */
		GeomBranc(Hierarc * brc=NULL, GeomBranc * gbear=NULL, class Plant * plt=NULL);

		//! \brief Destructor
		virtual ~GeomBranc(void);

		//! \brief Abstract initialisation geometry function
		virtual int initGeomtry()=0;

		//! \brief Abstract function to compute branc
		virtual int computeBranc(bool silent=false)=0;

		//! \brief Abstract function to simplify geometry of the branc
		virtual void simplification ()=0;

		//! \brief Accessor to  the corresponding hierarchical axe
		//! \return The hierarchical axe
		inline Hierarc * getAxe() {return hierarc;}

		//! \brief Set the hierarchical axe
		inline void setAxe(Hierarc * h) {hierarc = h;}

		//! \brief Accessor to  the corresponding topological axe
		//! \return The topological corresponding axe
		inline Branc * getBranc() {return hierarc->getBranc();}

		//! \brief Accessor to the geometrical bearer
		//! \return The geometrical bearer
		inline GeomBranc * getGeomBrancBearer() {return gbearer;}

		//! \brief Accessor to the geometrical bearer
		inline void setGeomBrancBearer (GeomBranc * gb) {gbearer=gb;}

		//! \brief Accessor to the geometrical branc identifier
		inline void setNum (int n){v_idGeomBranc=n;}

		//! \brief Accessor to the geometrical branc  identifier
		//! \return "this" identifier
		inline int getNum (){return v_idGeomBranc;}

		//! \brief Accessor to the position on the geometrical bearer
		inline void setPosOnBearer(int pos) {posOnBearer=pos;}

		//! \brief Accessor to the position on the geometrical bearer
		inline int getPosOnBearer() {return posOnBearer;}

		//! \brief Accessor to the stack of geometrical elements
		//! \return The stack of geometrical elements
		inline std::vector<GeomElem *> & getElementsGeo (){ return elemGeo;}

		//! \brief getter to get the scale.
		//! \return scale
		float getScale() const;

        //! \brief setter to get the scale.
        void setScale(int scale);

};

#endif
