/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef __VECTOR_H__
#define __VECTOR_H__

/*!
  \file util_vector.h
  \brief Definition of Vector3, Vector4.
  */
#include "Tuple.h"
#include "ArchivableObject.h"

class Point4f;

using namespace Vitis;


namespace Vitis
{

	/**
	  \class Vector3
	  \brief A 3D vector represented by its cartesian coordinates \c x, \c y
	  and \c z.
	  */

	class VITIS_EXPORT Vector3 : public Tuple3<float>, public ArchivableObject
	{

		public:

			/// The origin vector.
			static const Vector3 ORIGIN;

			/** Constructs a Vector3 with the \e x, \e y and \e z coordinates.
			  \post
			  - \e self is valid. */
			explicit Vector3( const float& x = 0,
					const float& y = 0,
					const float& z = 0 );

			/** Constructs a Vector3 with the 3D array \e v3.
			  \post
			  - \e self is valid. */
			Vector3( const float * v3 );

			/// Destructor
			virtual ~Vector3( );

			const float & operator[](int n) const{ return __T[n]; }

				float & operator[](int n) { return __T[n]; }

			/// set new values.
			void set(const float& x, const float& y, const float& z);

			/// set new values.
			void set(const float * v3);

			/// set new value.
			void set(const Vector3& v);

			/// Returns a const reference to the \c x coordinate of \e self.
			const float& x( ) const ;

			/// Returns a reference to the \c x coordinate of \e self.
			float& x( ) ;

			/// Returns a const reference to the \c y coordinate of \e self.
			const float& y( ) const ;

			/// Returns a reference to the \c y coordinate of \e self.
			float& y( ) ;

			/// Returns a const reference to the \c z coordinate of \e self.
			const float& z( ) const;

			/// Returns a reference to the \c z coordinate of \e self.
			float& z( ) ;


		void add(Vector3 p);

		float distanceSquared(Vector3 p1);

		float distance(Vector3 p1);

		float distanceL1(Vector3 p1);

		float distanceLinf(Vector3 p1);

		void project(Point4f p1) ;

			/// Returns the index of the maximum absolute coordinate
			int getMaxAbsCoord() const;

			/// Returns the index of the minimum absolute coordinate
			int getMinAbsCoord() const;

			/// Returns whether \e self is equal to \e v.
			bool operator==( const Vector3& v ) const;

			/// Returns whether \e self is equal to \e v.
			bool operator!=( const Vector3& v ) const;

			/// Sets \e self to result of the addition of itself and \e v.
			Vector3& operator+=( const Vector3& v );

			/// Sets \e self to result of the substraction of itself and \e v.
			Vector3& operator-=( const Vector3& v );

			/** Sets \e self to result of the multiplication of itself by the
			  scalar \e s. */
			Vector3& operator*=( const float& s );

			/** Sets \e self to result of the division of itself by the scalar \e s.
			  \warning
			  - \e s must be different from 0. */
			Vector3& operator/=( const float& s );

			/// Returns the opposite Vector3 of \e self.
			Vector3 operator-( ) const;

			/// Returns the result of the addition of \e this and \e v.
			Vector3 operator+( const Vector3& v) const ;

			/// Returns the result of the substraction of \e this and \e v.
			Vector3 operator-( const Vector3& v ) const;

			/// Normalizes \e self and returns the norm before.
			float normalize( );

			/// Returns whether \e self is normalized.
			bool isNormalized( ) const;

			/// Returns whether \e self is orthogonal to \e v.
			bool isOrthogonalTo( const Vector3& v ) const;

			/// Returns whether \e self is valid.
			bool isValid( ) const ;

			/// Returns the result of the multiplication of \e v by the scalar \e s.
			VITIS_EXPORT friend  Vector3 operator*( const Vector3& v, const float& s );

			/** Returns the result of the division of \e v by the scalar \e s.
			  \warning
			  - \e s must be different from 0. */
			VITIS_EXPORT friend  Vector3 operator/( const Vector3& v, const float& s );

			/** Returns the direction of \e v.
			  The resulting Vector3 is normalized. */
			VITIS_EXPORT friend  Vector3 direction( const Vector3& v );

			/// Returns the abs value of \e v.
			VITIS_EXPORT friend  Vector3 abs( const Vector3& v );

			/// Returns the norm of \e v.
			VITIS_EXPORT friend  float norm( const Vector3& v );


			/// Returns the L-infinite norm of \e v.
			VITIS_EXPORT friend  float normLinf( const Vector3& v );

			/// Returns the square of the norm of \e v.
			VITIS_EXPORT friend  float normSquared( const Vector3& v );

			static Vector3 axisRotation (Vector3 axe, Vector3 vin, float ang);

			///  Returns the result of the cross product between \e v1 and \e v2.
			VITIS_EXPORT friend  Vector3 cross( const Vector3& v1, const Vector3& v2 );
			VITIS_EXPORT friend  Vector3 operator^( const Vector3& v1, const Vector3& v2 );

			/// Returns the dot product between \e v1 and \e v2.
			VITIS_EXPORT friend  float dot( const Vector3& v1, const Vector3& v2 );
			VITIS_EXPORT friend  float operator*( const Vector3& v1, const Vector3& v2 );

			/// Returns the vector with the maximum values between \e v1 and \e v2.
			VITIS_EXPORT friend  Vector3 Max( const Vector3& v1, const Vector3& v2 );

			/// Returns the vector with the minimum values between \e v1 and \e v2.
			VITIS_EXPORT friend  Vector3 Min( const Vector3& v1, const Vector3& v2 );

			/// Returns the value of the angle between \e v1 and \e v2.
			VITIS_EXPORT friend  float angle( const Vector3& v1, const Vector3& v2 );

			/// Prints \e v to the output stream \e stream.
			VITIS_EXPORT friend  std::ostream& operator<<( std::ostream& stream, const Vector3& v );

			/// Defines an order relation in order to use this class in a sorted container
			VITIS_EXPORT friend  bool operator<(const Vector3& v1, const Vector3& v2);

			void serialize(Archive& ar );




	}; // Vector3

	/**
	  \class Vector4
	  \brief A 4D vector represented by its cartesian coordinates \c x, \c y,
	  \e z and \c w.
	  */

	class VITIS_EXPORT Vector4 : public Tuple4<float>
	{

		public:

			/// The origin vector.
			static const Vector4 ORIGIN;



			/** Constructs a Vector4 with the \e x, \e y, \e z and \e w coordinates.
			  \post
			  - \e self is valid. */
			explicit Vector4( const float& x = 0, const float& y = 0,
					const float& z = 0, const float& w = 0 );

			/** Constructs a Vector4 with the 4D array \e v4.
			  \post
			  - \e self is valid. */
			Vector4( const float * v4 );

			/** Constructs a Vector4 with a Vector3 \e v and \e w.
			  \pre
			  - \e v must be valid.
			  \post
			  - \e self is valid. */
			Vector4( const Vector3& v, const float& w );

			/// Destructor
			virtual ~Vector4( );


			const float  operator[](int n) const{ return __T[n]; }

			float & operator[](int n) { return __T[n]; }

			/// set new values.
			void set(const float& x, const float& y, const float& z , const float& w);

			/// set new values.
			void set(const float * v4);

			/// set new values.
			void set(const Vector3& v, const float& w);

			/// set new value.
			void set(const Vector4& v);

			/// Returns a const reference to the \c x coordinate of \e self.
			const float& x( ) const ;

			/// Returns a reference to the \c x coordinate of \e self.
			float& x( );

			/// Returns a const reference to the \c y coordinate of \e self.
			const float& y( ) const;

			/// Returns a reference to the \c y coordinate of \e self.
			float& y( );

			/// Returns a const reference to the \c z coordinate of \e self.
			const float& z( ) const;

			/// Returns a reference to the \c z coordinate of \e self.
			float& z( );

			/// Returns a const reference to \c w coordinate of \e self.
			const float& w( ) const;

			/// Returns a reference to the \c w coordinate of \e self.
			float& w( );

			/// Returns whether \e self is equal to \e v.
			bool operator==( const Vector4& v ) const;

			/// Returns whether \e self is equal to \e v.
			bool operator!=( const Vector4& v ) const ;

			/// Sets \e self to result of the addition of itself and \e v.
			Vector4& operator+=( const Vector4& v );

			/// Sets \e self to result of the substraction of itself and \e v.
			Vector4& operator-=( const Vector4& v );

			/** Sets \e self to result of the multiplication of itself by the
			  scalar \e s. */
			Vector4& operator*=( const float& s );

			/** Sets \e self to result of the division of itself by the scalar \e s.
			  \warning
			  - \e s must be different from 0. */
			Vector4& operator/=( const float& s );

			/// Returns the opposite Vector4 of \e self.
			Vector4 operator-( ) const ;

			/// Returns the result of the addition of \e this and \e v.
			Vector4 operator+( const Vector4& v) const;

			/// Returns the result of the substraction of \e this and \e v.
			Vector4 operator-( const Vector4& v ) const;

			/// Returns the abs value of \e v.
			VITIS_EXPORT friend  Vector4 abs( const Vector4& v );

			/** Returns the direction of \e v.
			  The resulting Vector4 is normalized. */
			VITIS_EXPORT friend  Vector4 direction( const Vector4& v );

			/// Returns the dot product between \e v1 and \e v2.
			VITIS_EXPORT friend  float dot( const Vector4& v1, const Vector4& v2 );
			VITIS_EXPORT friend  float operator*( const Vector4& v1, const Vector4& v2 );

			/// Returns whether \e self is normalized.
			bool isNormalized( ) const;

			/// Returns whether \e self is orthogonal to \e v.
			bool isOrthogonalTo( const Vector4& v ) const;

			/// Returns whether \e self is valid.
			bool isValid( ) const ;

			/// Normalizes \e self and returns the norm before.
			float normalize( );

			/** Returns the Vector3 corresponding to the projection of \e self.
			  The \c x, \c y and \c z  coordinates are divided by the \c w
			  coordinate. */
			Vector3 project( ) const;

			/// Returns the result of the multiplication of \e v by the scalar \e s.
			VITIS_EXPORT friend Vector4 operator*( const Vector4& v, const float& s );

			/** Returns the result of the division of \e v by the scalar \e s.
			  \warning
			  - \e s must be different from 0. */
			VITIS_EXPORT friend Vector4 operator/( const Vector4& v, const float& s );

			/// Returns the vector with the maximum values between \e v1 and \e v2.
			VITIS_EXPORT friend Vector4 Max( const Vector4& v1, const Vector4& v2 ) ;

			/// Returns the vector with the minimum values between \e v1 and \e v2.
			VITIS_EXPORT friend Vector4 Min( const Vector4& v1, const Vector4& v2 );

			/// Returns the norm of \e v.
			VITIS_EXPORT friend float norm( const Vector4& v );

			/// Returns the L-infinite norm of \e v.
			VITIS_EXPORT friend float normLinf( const Vector4& v );

			/// Returns the square of the norm of \e v.
			VITIS_EXPORT friend float normSquared( const Vector4& v ) ;

			/// Prints \e v to the output stream \e stream.
			VITIS_EXPORT friend std::ostream& operator<<( std::ostream& stream, const Vector4& v );

			/// Defines an order relation in order to use this class in a sorted container
			VITIS_EXPORT friend bool operator<(const Vector4& v1, const Vector4& v2);

	}; // Vector4

}; // end namespace vitis

#endif
