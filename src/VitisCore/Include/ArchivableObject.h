/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file ArchivableObject.h
///			\brief Definition of ArchivableObject and SArchiveMaps classes
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _ARCHIVABLE_OBJECT_H
#define _ARCHIVABLE_OBJECT_H

#include <string>
#include <map>

#include "Archive.h"
#include "defVitis.h"
#include "VitisObject.h"


namespace Vitis {


	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///
	///			\class ArchivableObject
	///			\brief A class which adds binary I/O routines (using class Archive) to the standard Object (VitisObject)interface.
	///			\warning An archivable object must not be muliple inherited !!
	///
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class VITIS_EXPORT ArchivableObject : public VitisObject
	{


		public:

			//! \brief Descendents need to override this to save/load themselves to/from archives.
			/*! Descendent classes should be sure to invoke their parent's
			  version of this function before proceeding. See clas Archive. */
			virtual void serialize( Archive& ar ) = 0;

			//!	\brief Load a pointer
			/*!	\param ar A reference to the serialization archive
			  \param pE A reference to the pointer where the pointer will be loaded
			  \return \li 0 if the pointed ArchivableObject has already been serialized \li 1 otherwise */
			int LoadPtr(Archive& ar, ArchivableObject*& pE);

			//!	\brief Save a pointer
			/*!	\param ar A reference to the serialization archive
			  \param pE The pointer to serialize
			  \return \li 0 if the pointed ArchivableObject has already been serialized \li 1 otherwise */
			int SavePtr(Archive& ar, ArchivableObject* pE);

			//!	\brief Map the pointer to avoid serializing many times the same pointer
			/*!	\param ar A reference to the serialization archive
			  \param pE The pointer serialized */
			void AddPtr(Archive& ar, ArchivableObject* pE);

			//! \brief Convenience operator for saving an ArchivableObject to an archive.
			friend VITIS_EXPORT Archive& operator <<( Archive& ar, ArchivableObject& o );

			//! \brief Convenience operator for loading an ArchivableObject from an archive.
			friend VITIS_EXPORT Archive& operator >>( Archive& ar, ArchivableObject& o );

	};
	/* end class ArchivableObject */

/// \cond 0
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///
	///			\class SArchiveMaps
	///			\brief A serialization map class
	///
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class VITIS_EXPORT SArchiveMaps : public VitisObject
	{

		private:

			//! \brief the referred archive
			Archive* _pAr;

			//! \brief Find this from Archive
			static std::map<Archive*, SArchiveMaps*> _s_armaps;

			//! \brief An ID
			USint _nID;

			//! \brief "already stored" map
			std::map<void *, USint> _storemap;

			//! \brief "already loaded" map (the reverse map. NOTE: maps on save and maps on load act as a relocation table)
			std::map<USint, void *> _loadmap;

		public:

			//! \brief retrieve the ar associated maps
			/*! \param ar A reference the Archive to retrieve
			  \return The SArchiveMaps associated to the archive */
			static SArchiveMaps* FromArchive(Archive& ar);

			//! \brief construt and associate to an Archive
			SArchiveMaps(Archive& ar);

			//! Destructor
			~SArchiveMaps();

			//! \brief Map the loaded pointer
			//! \param pE The loaded pointer to map
			void  MapLoadObject(void * pE);

			//! \brief Map the saved pointer
			//! \param pE The saved pointer to map
			USint MapStoreObject(void * pE);


			//! \brief Saves a pointer
			//! \param pE The pointer to save
			int Save(ArchivableObject * pE)
			{

				if(!pE)
					(*_pAr) << (int)0;	//null pointer flag
				else
				{
					(*_pAr) << (int)1;	//good pointer flag

					USint & m = _storemap[pE]; //gets a tag (will be 0 if not yet mapped)

					if(!m)
					{	// object not yet saved
						//	pE->Serialize(*_pAr); //save the contents and map the objects
						(*_pAr) << MapStoreObject(pE); //save the tag

						return 1;
					}

					(*_pAr) << m; //save the tag
				}
				return 0;
			}

			//! \brief Load a pointer
			//! \param pE The pointer to load
			int Load(ArchivableObject*& rpE)
			{
				int isvalid; (*_pAr) >> isvalid;  //the null flag
				if(!isvalid)
				{
					rpE = NULL;
					return 0;
				}

				//non null pointer
				USint m;

				(*_pAr) >> m; //gets the ID

				rpE = (ArchivableObject*)_loadmap[m]; //sets return value, taken from the map

				if(!rpE) //if 0, we must create a new object
				{
					_nID=m;
					return 1;

					//pES->Serialize(*_pAr); //serialize the object and map the object into maps
					//(*_pAr) >> m;	//gets the real ID

				}


				return 0;

			}
	};

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///
	///			\class PrintableObject
	///			\brief A class which adds string output routines to the standard Object.
	///
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class VITIS_EXPORT PrintableObject
	{
		public:

			//! \brief Descendents need to override this to output themselves to ascii string.
			virtual std::string to_string() = 0;

	};


} /* end namespace Vitis */
/// \endcond

#endif
