/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef __TUPLE_H__
#define __TUPLE_H__
#include "defVitis.h"
#include "VitisObject.h"
#include <iostream>

namespace Vitis
{
	/*!

	  \class Tuple3

	  \brief A 3D tuple of numeric elements of Type T.

*/


	template <class T>
		class Tuple3 : public VitisObject
		{

			public:

				/// A const iterator used to iterate through an Tuple3.
				typedef const T * const_iterator;

				/// An iterator used to iterate through an Tuple3.
				typedef T * iterator;

				/// Constructs an Tuple3 with \e to, \e t1 and \e t2.
				inline explicit Tuple3( const T& t0 = T(),
						const T& t1 = T(),
						const T& t2 = T() ) {
					__T[0] = t0; __T[1] = t1; __T[2] = t2;
				}

				/// Constructs an Tuple3 from the 3 elements tuple \e a3
				inline Tuple3( const T * a3 ) {
					__T[0] = a3[0]; __T[1] = a3[1]; __T[2] = a3[2];
				}

				/// Returns whether \e self is equal to \e t.
				bool operator==( const Tuple3& t ) const {
					return (__T[0] == t.__T[0]) && (__T[1] == t.__T[1]) && (__T[2] == t.__T[2]);
				}

				/// Returns whether \e self contain \e t.
				inline bool contain( const T& t ) const {
					return (__T[0] == t) || (__T[1] == t) || (__T[2] == t);
				}

				/** Returns a const reference to the \e i-th element of \e self.
				  \pre
				  - \e i must belong to the range [0,3). */
				inline const T& getAt( Uchar i ) const {

					return __T[i];
				}

				/** Returns a reference to the \e i-th element of \e self.
				  \pre
				  - \e i must belong to the range [0,3). */
				inline T& getAt( Uchar i ) {

					return __T[i];
				}

				/// Returns a const iterator at the beginning of \e self.
				inline const_iterator getBegin( ) const {
					return __T;
				}

				/// Returns an iterator at the beginning of \e self.
				inline iterator getBegin( ) {
					return __T;
				}

				/// Returns a const iterator at the end of \e self.
				inline const_iterator getEnd( ) const {
					return __T + 3;
				}

				/// Returns an iterator at the end of \e self.
				inline iterator getEnd( ) {
					return __T + 3;
				}

				/// Returns an iterator at the maximum value of \e self.
				const_iterator getMax( ) const {
					return __T[0] < __T[1] ?
						(__T[1] < __T[2] ? __T + 2 : __T + 1) :
							(__T[0] < __T[2] ? __T + 2 : __T);
				}

				/// Returns an iterator at the minimum value of \e self.
				const_iterator getMin( ) const {
					return __T[0] < __T[1] ?
						(__T[0] < __T[2] ? __T : __T + 2) :
							(__T[1] < __T[2] ? __T + 1 : __T + 2);
				}



				/// Prints \e self to the output stream \e stream.
				std::ostream& print( std::ostream& stream,
						char delimiter = ',',
						char begin = '<',
						char end = '>' ) const {
					stream << begin << __T[0];
					stream << delimiter << __T[1];
					stream << delimiter << __T[2];
					return stream << end;
				}

				/** Sets the \e i-th element of \e self with \e t.
				  \pre
				  - \e i must belong to the range [0,3). */
				void setAt( Uchar i, const T& t ) {

					__T[i] = t;
				}

			protected:

				/// The 3D value tuple.
				T __T[3];

		}; // Tuple3

	typedef Tuple3<float> FloatTuple3;

	/*  --------------------------------------------------------------------- */


	/*!

	  \class Tuple4

	  \brief A 4D tuple of numeric elements of Type T.

*/

	template <class T>
		class Tuple4 : public VitisObject
		{

			public:

				/// A const iterator used to iterate through an Tuple4.
				typedef const T * const_iterator;

				/// An iterator used to iterate through an Tuple4.
				typedef T * iterator;

				/// Constructs a Tuple4 with \e to, \e t1, \e t2 and \e t3.
				inline explicit Tuple4( const T& t0 = T(),
						const T& t1 = T(),
						const T& t2 = T(),
						const T& t3 = T() ) {
					__T[0] = t0; __T[1] = t1; __T[2] = t2; __T[3] = t3;
				}

				/// Constructs an Tuple4 from the 4 elements tuple \e a4
				inline Tuple4( const T * a4 ) {
					__T[0] = a4[0]; __T[1] = a4[1]; __T[2] = a4[2]; __T[2] = a4[2];
				}

				/// Returns whether \e self is equal to \e t.
				bool operator==( const Tuple4& t ) const {
					return
						(__T[0] == t.__T[0]) && (__T[1] == t.__T[1]) &&
						(__T[2] == t.__T[2]) && (__T[3] == t.__T[3]);
				}

				/// Returns whether \e self contain \e t.
				inline bool contain( const T& t ) const {
					return (__T[0] == t) || (__T[1] == t) || (__T[2] == t) || (__T[3] == t);
				}

				/** Returns a const reference to the \e i-th element of \e self.
				  \pre
				  - \e i must belong to the range [0,4). */
				inline const T& getAt( Uchar i ) const {

					return __T[i];
				}

				/** Returns a reference to the \e i-th element of \e self.
				  \pre
				  - \e i must belong to the range [0,4). */
				inline T& getAt( Uchar i ) {

					return __T[i];
				}

				/// Returns a const iterator at the beginning of \e self.
				inline const_iterator getBegin( ) const {
					return __T;
				}

				/// Returns an iterator at the beginning of \e self.
				inline iterator getBegin( ) {
					return __T;
				}


				/// Returns a const iterator at the end of \e self.
				inline const_iterator getEnd( ) const {
					return __T + 4;
				}

				/// Returns an iterator at the end of \e self.
				inline iterator getEnd( ) {
					return __T + 4;
				}

				/// Returns an iterator at the maximum value of \e self.
				const_iterator getMax( ) const {
					return __T[0] < __T[1] ?
						(__T[1] < __T[2] ?
						 (__T[2] < __T[3] ? __T + 3 : __T + 2) :
						 (__T[1] < __T[3] ? __T + 3 : __T + 1)) :
							(__T[0] < __T[2] ?
							 (__T[2] < __T[3] ? __T + 3 : __T + 2) :
							 (__T[0] < __T[3] ? __T + 3 : __T));
				}

				/// Returns an iterator at the minimum value of \e self.
				const_iterator getMin( ) const {
					return __T[0] < __T[1] ?
						(__T[0] < __T[2] ?
						 (__T[0] < __T[3] ? __T : __T + 3) :
						 (__T[2] < __T[3] ? __T + 2 : __T + 3)) :
							(__T[1] < __T[2] ?
							 (__T[1] < __T[3] ? __T + 1 : __T + 3) :
							 (__T[2] < __T[3] ? __T + 2 : __T + 3));
				}


				/// Prints \e self to the output stream \e stream.
				std::ostream& print( std::ostream& stream,
						char delimiter = ',',
						char begin = '<',
						char end = '>' ) const {
					stream << begin << __T[0];
					stream << delimiter << __T[1];
					stream << delimiter << __T[2];
					stream << delimiter << __T[3];
					return stream << end;
				}

				/** Sets the \e i-th element of \e self with \e t.
				  \pre
				  - \e i must belong to the range [0,4). */
				void setAt( Uchar i, const T& t ) {
					__T[i] = t;
				}

			protected:

				/// The 4D value tuple.
				T __T[4];

		}; // Tuple4

	typedef Tuple4<float> RealTuple4;

}; //end namespace Vitis

#endif
