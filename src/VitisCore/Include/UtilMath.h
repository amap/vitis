/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef __util_math_h__
#define __util_math_h__

///	 \file util_math.h
///  \brief File that contains some math utility.


#include <cmath>     // Common math function.
#include <algorithm> // For min, max ...
#include "Matrix.h"

//#define max(x,y) x>y?x:y
//#define min(x,y) x>y?y:x

#ifdef WIN32
#include <float.h>
#endif

/* ----------------------------------------------------------------------- */


/// value of a degree in radian
#define GEOM_RAD 0.01745329252
/// value of a radian in degree
#define GEOM_DEG 57.295779513
/// value of 2 pi
#define GEOM_TWO_PI 6.2831853072


/// Geom tolerance value
#define GEOM_TOLERANCE 1e-10

#ifdef M_PI
#  define GEOM_PI M_PI
#else
/// value of pi
#  define GEOM_PI 3.14159265358979323846
/// value of pi
#  define M_PI GEOM_PI
#endif

#ifdef M_PI_2
/// value of pi / 2
#  define GEOM_HALF_PI M_PI_2
#else
/// value of pi / 2
#  define GEOM_HALF_PI 1.5707963286
/// value of pi / 2
#  define M_PI_2 GEOM_HALF_PI
#endif

#define FGEOM_PI (float)GEOM_PI
#define FGEOM_TWO_PI (float)GEOM_TWO_PI
#define FGEOM_HALF_PI (float)GEOM_HALF_PI
#define FM_PI_2 (float)M_PI_2
#define FM_PI (float)M_PI


/// \brief Returns the cube value of \e v.
inline float cb( const float& v ) {
	return v * v * v;
}

/// \brief Returns the cube value of \e v.
inline double cb( const double& v ) {
	return v * v * v;
}

/// \brief Computes the square value of \e v.
inline float sq( const float& v ) {
	return v * v;
}

/// \brief Computes the square value of \e v.
inline double sq( const double& v ) {
	return v * v;
}

//! Performs an in-place reversal of the byte ordering of the \b n bytes at \b ptr.
inline void reverseByteOrder( char* ptr, int n )
{
	char temp;
	int i = 0;
	n--;
	while( i < n )
	{
		temp = ptr[i];
		ptr[i++] = ptr[n];
		ptr[n--] = temp;
	}


}


/// \brief global bending computing
///	@warning BE VERY CAREFUL WHITH THIS FUNCTION THAT I DON'T CONTROL
/// @param young Young modulus
/// @param dirz Vertical direction
/// @param length Length of the branch
/// @param conicity Conicity factor
inline float bending( float young,
		float dirz,
		float length,
		float conicity)
{
	float angle, amin, amax, precision, teta, h, coef, po, omega, somme, increment;
	float seuil;
	long  nbiter;

	/* initialisations */
	if (dirz < -1.)
		dirz = -1.;
	else if (dirz > 1.)
		dirz = 1.;
	teta = (float)acos(dirz);
	precision = 1.0;
	seuil = (float)0.5 * (float)CONV;
	amin = 0.0;
	amax = (float)GEOM_PI - teta;

	/* evaluation */
	h = fabs(length / ((float)1.0 - conicity * length));
	coef = young * h * (float)sqrt(fabs(dirz));
	po = coef * (float)200.0 / (float)GEOM_PI;
	if (teta > 1.553 && teta < 1.588)
		angle = young * young * h * h / (float)2.0;
	else
		angle = (float)sin(teta) * (float)(1.0 - cos(coef)) / cos(coef) / fabs(dirz);

	/* cas de la flexion faible */
	if (   po <= 40.0
			&& angle <= 0.2)
	{
		angle = (float)cos(angle + teta);
		return(angle);
	}

	/* integration */
	while ((amax - amin) > seuil)
	{
		angle = (float)(amax + amin) / (float)2.0;
		omega = 0.0;
		somme = 0.0;
		increment = 1.0;
		nbiter = 0;
		while (   omega < angle
				&& increment != 0.0
				&& nbiter < 500)
		{
			increment = precision * (float)1.414 * young * (float)sqrt(fabs(cos(teta + omega) - cos(teta + angle)));
			omega += increment;
			somme += precision;
			nbiter++;
		}
		if ( somme <= (h - precision))
			amin = angle;
		else
			amax = angle;
	}
	angle = (amin + amax) / 2.0f;

	angle = (float)cos(angle + teta);

	return(angle);
	/* YEAH !!!! */
}





//// \brief Projects vin on plan vn and store it into vout
inline Vector3 projection_plan(Vector3 vn ,Vector3 vin)
{
	Vector3 vaux, vout;

	float v11 ,v12 ,v13 ,v22 ,v23 ,v33;

	v11 = 1.0f - vn[0] * vn[0];
	v12 = vn[0] * vn[1];
	v13 = vn[0] * vn[2];
	v22 = 1.0f - vn[1] * vn[1];
	v23 = vn[1] * vn[2];
	v33 = 1.0f - vn[2] * vn[2];
	vaux[0] = v11 * vin[0] - v12 * vin[1] - v13 * vin[2];
	vaux[1] = -v12 * vin[0] + v22 * vin[1] - v23 * vin[2];
	vaux[2] = -v13 * vin[0] - v23 * vin[1] + v33 * vin[2];
	vout[0] = vaux[0];
	vout[1] = vaux[1];
	vout[2] = vaux[2];
	if (vout[0]*vout[0] + vout[1]*vout[1] + vout[2]*vout[2] < .0001)
	{
		vout[0] = 1;
		vout[1] = 0;
		vout[2] = 0;
	}

	return vout;
}



/// \brief Return the projection of vin to the plan caracterized by his normal vn
inline Vector3 projection_mat(Vector3 vn ,Vector3 vin)
{
	Vector3 vnorm(vn);
	vnorm.normalize();
	return (vin-(vn*(vin*vn)));


}





/// \brief Linear interpolation between two points
/// @param code Truncate outside born1, born2
/// @param born1 First point
/// @param val1  Value for first point
/// @param born2 Second point
/// @param val2  Value for second point
/// @param borne Current value
inline float interpole_lineaire (long code,
		long born1,
		float val1,
		long born2,
		float val2,
		long borne)
{
	long  baux;
	float vaux, coef;

	if (born1 == born2)
		return((val2 > val1) ? val2 : val1);

	if (born2 < born1)
	{
		baux = born1;
		vaux = val1;
		born1 = born2;
		val1  = val2;
		born2 = baux;
		val2  = vaux;
	}

	if (borne < born1)
	{
		if (code)
			return (val2);
		else
			return (0.0);
	}

	if (borne > born2)
	{
		if (code)
			return (val1);
		else
			return (0.0);
	}

	coef = (float)(borne - born1)/(float)(born2 - born1);
	vaux = ((float)1. - coef) * val2 + coef * val1;

	return (vaux);
}







/// \brief A pseudo random function
inline void aleains (double *x,
		double *aleax)
{
	double  aux;

	aux = *x * 997.0 - floor(*x * 997.0);
	*aleax = aux;
}

#ifdef WIN32

/// On win32, redirect finite on _finite.
#define finite _finite

/// On win32, redefine cubic root.
inline double cbrt(double x){
	return pow(x,1/3);
}

#if 0
/// On win32, redefine round double to int.
inline int rint(double x){
	int res = (int)x;
	if(x-res>0.5)res++;
	return res;
}

/// On win32, redefine truncate double to int.
inline int trunc(double x){
	int res = (int)x;
	if(x-res>0.5)res++;
	return res;
}
#endif


#endif

/* ----------------------------------------------------------------------- */

//__util_math_h__
#endif
