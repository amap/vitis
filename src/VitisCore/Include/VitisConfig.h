/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file VitisConfig.h
///			\brief Definition of VitisConfig.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _VITISCONFIG_H
#define _VITISCONFIG_H
#include <vector>
#include "randomMers.h"
#include "Vector.h"
#include "ArchivableObject.h"
using namespace Vitis;


#ifdef WIN32
#include <direct.h>
#define getcwd _getcwd
#endif


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class VitisConfig
///			\brief Description of the configuration of Vitis Framework. An instance of VitisConfig is a member of Plant class
///			It should be seen and used more as a module than a class.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT VitisConfig : public ArchivableObject
{


	public:


		/// \brief Simulation total time
		float simuTotalTime;

		/// \brief Simulation random generator initialization value
		long randSeed;

		/// \brief output time step
		float  step_output;

		/// \brief current output file path
		std::string outputFileName;

		/// \brief current serialization file path
		std::string serialFileName;

		/// \brief current dynamic library file path containing the Simulation Modele
		std::string modname;

		/// \brief topological simplification level
		USint cfg_topoSimp;

		/// \brief threshold on birth date to build simplified Topological classes
		float cfg_birthDateThresholdTopoSimp;

		/// \brief geometrical simplification level
		///
		//! Parameter to the function to simplify the geometry of the axe
		/// Merge successive components of geomElemCone if they are approximatively colinear and not too distant.
		/// Depending on the value of the geometrical simplification parameter
		/// different threshold will be applied.
		/// simplification 0 : no simplification
		/// simplification 1 : direction are closer than 15� and their connection distance is less than 3mm.
		/// simplification 2 : direction are closer than 21� and their connection distance is less than 6mm.
		/// simplification 3 : direction are closer than 25� and their connection distance is less than 1cm.
		USint cfg_geoSimp;

		/// \brief super simplification level
		///
		/// super simplification level
		///
		//! Parameter to find the simplified axe wich can represent this axe and launch the copy.
		/// This simplification takes place only if this value
		/// is greater than 4. In this case the vertical range is divided in cfg_geoSimp equal sectors and only one
		/// geometrical branch will be computed for each topological branch class and for each elevation sector.
		/// This means that every branches belonging to the same topological class and having an insertion angle into
		/// the same sector will have the same geometrical shape.
		USint cfg_supersimplif;

//		// \brief the output of topologicly simplified element for the geometrical output
//		USint cfg_showSimp;

		/// \brief The coordinates of ligth direction (may be usefull for functioning plant simulator)
		Vector3 light;

		/// \brief Number of external module (plugin)
		int	nbExternModules;

		/// \brief contains the time at which simulation was launched (more than zero in case of serialization loading)
		double hotStart;

		/// \brief The parameter files path to initialize external module (plugin)
		std::vector<std::string> ExternModulesParam;

		/// \brief The dynamic library file path that contain the pluginS
		std::vector<std::string> ExternModules;

		/// \brief The random number generator
		TRandomMersenne * randGenerator;

		/// \brief Initialize the random generator
		void initRandomGenerator();

		/// \brief Accessor
		/// @return The random generator
		TRandomMersenne * getRandomGenerator();

		/// \brief Default Constructor
		VitisConfig ();

		/// \brief Destructor
		///	Destroy all the static members of Vitis configuration
		virtual ~VitisConfig();

		/// \brief Save/restore this object in case of serialization
		virtual void serialize(Archive& ar );

		/// \brief Add an external module to the simulator
		//void addExternalModule (std::string module, std::string parameter="");
		void addExternalModule (char *module, char *parameter="");




	private:

		/// \brief convenient tool to compute a valid path name from a directory and a file name
		char * makepath (char *path,char *rep,char *fic);

};

#endif
