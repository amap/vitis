/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file Branc.h
///			\brief Definition of Branc class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _BRANC_H
#define _BRANC_H
#include "DecompAxeLevel.h"
#include "externDll.h"

class Hierarc;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class Branc
///			\brief Describe an axe of the plant
///			The plant is made of a collection of Branc that are topologically organized through the Hierarc
///			Structure.
///			\warning One instance of Branc can be link to many Hierarc instance
///			\see SimplifTopo
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class VITIS_EXPORT Branc:public DecompAxeLevel
{

	std::string critere;


		/// \brief An identifier for each instance
		int		v_idBranc;

		/// \brief Total number of Branc's instance in the plant (usefull if topological simplif)
		long	 totalAxeNumberWithinPlant;

		/// \brief number of substructure Hierarc that refer to that branch
//		long    hierarcPtrsNumber;

		/// \brief current pointer in topological structure
		Hierarc *   currentHierarcPtr;

		///\brief list of pointers in topological structure
		std::vector<Hierarc *>   hierarcPtrs;

		/// \brief Pointer to "mother" plant
		class Plant * v_plt;

        std::string classType = "Axe";

public:

	const std::string &getClassType() const;

	void setClassType(const std::string &classType);


	/// \brief Constructor
		Branc(class Plant * plt);

		/// \brief Copy constructor
		Branc(Branc * b);

		/// \brief Destructor
		virtual ~Branc();

		/// \brief Update the list of Hierarc pointers relied to this axe (call for each topological simplification )
		/// \param hbear The pointer to the new topological axe Hierarc
		virtual void updateTopo (class Hierarc * hierarcPtr);

		/// \return The pointer to the current corresponding topological axe
		inline class Hierarc * getCurrentHierarc(){return currentHierarcPtr;}


		virtual void updatePruning(){};

		/// \brief Serialize this Branc
		///	\param ar A reference to the archive \see Archive
		virtual void serialize(Archive& ar );

		void setCritere(const std::string &c);
		inline const std::string & getCritere() {return critere;};

		/// \brief accessor to get v_idbranc
		int		V_idBranc(){return v_idBranc;};

		/// \brief accessor to get Total number of Branc's instance in the plant
		long	 TotalAxeNumberWithinPlant(){return totalAxeNumberWithinPlant;};
		/// \brief accessor to set Total number of Branc's instance in the plant
		void	 TotalAxeNumberWithinPlant(int n){totalAxeNumberWithinPlant=n;};

		/// \brief accessor to get number of substructure Hierarc that refer to that branch
//		long    HierarcPtrsNumber(){return hierarcPtrsNumber;};
		/// \brief accessor to set number of substructure Hierarc that refer to that branch
//		void    HierarcPtrsNumber(int n){hierarcPtrsNumber=n;};

		/// \brief accessor to get current pointer in topological structure
		Hierarc *   CurrentHierarcPtr(){return currentHierarcPtr;};
		/// \brief accessor to set current pointer in topological structure
		void  CurrentHierarcPtr(Hierarc *h){currentHierarcPtr=h;};

		///\brief accessor to get list of pointers in topological structure
		std::vector<Hierarc *>   HierarcPtrs(){return hierarcPtrs;};
		std::vector<Hierarc *>   *HierarcPtrsByAddr(){return &hierarcPtrs;};

		///\brief Set list of pointers in topological structure
		void   HierarcPtrs(std::vector<Hierarc *> p){hierarcPtrs=p;};

		/// \brief accessor to get current mother plant
		Plant *V_plt() {return v_plt;};
		/// \brief accessor to set current mother plant
		void V_plt(Plant *p) {v_plt=p;};

		virtual Plant* getPlant() {return v_plt;};

};





#endif
