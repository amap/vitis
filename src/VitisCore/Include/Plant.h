/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  Plant.h
///			\brief Definition of Plant class.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __PLANT_H__
#define __PLANT_H__
#include "GeomManager.h"
#include "Topology.h"
#include "VitisConfig.h"
#include "scheduler.h"
#include "PlantBuilder.h"
#include "Serializer.h"
#include "VitisCoreSignalInterface.h"
#include <string>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class Plant
///			\brief Basic describtion for a plant , must be specialized in the simulation model plugin
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// \brief A map which associates a key (string) with a parameter file name
VITIS_EXPORT typedef std::map<std::string , std::string> MapParameterFileName;
/// \brief A pair which associates a key (string) with a parameter file name
VITIS_EXPORT typedef std::pair<std::string , std::string> ParameterFileNamePair;

/// \brief A map which associates a key (string) with a parameter file name
VITIS_EXPORT typedef std::map<std::string , void *> MapParameterData;
/// \brief A pair which associates a key (string) with a parameter file name
VITIS_EXPORT typedef std::pair<std::string , void *> ParameterDataPair;

class VITIS_EXPORT Plant : public VProcess
{


	public:

		/// \brief The plant builder (instancier)
		PlantBuilder * v_pltBuilder;

		/// \brief The configuration data
		VitisConfig * v_config;

		/// \brief the manager of signal interface data
		VitisCoreSignalInterface * v_signal;

		/// \brief the manager of geometry
		GeomManager * v_geomMan;

		/// \brief the manager of topology
		TopoManager * v_topoMan;

		/// \brief the scheduler
		Scheduler * v_actionScheduler;

		/// \brief the serializer manager
		Serializer * v_serial;

		/// \brief The fullname for the parameter file associated to the model's name
		//std::string v_paramFileName;
		MapParameterFileName mapParameterFileName;

		/// \brief The parameter data
		MapParameterData mapParameterData;

		/// \brief The name of the plant
		std::string v_nameTree;

		DecompAxeLevel *theMerde;

	public:

		/// \brief Default constructor
		Plant();

		/// \brief Plant constructor
		/// \param nameT The name of the plant
		/// \param nameP The fullname for the parameter file
		Plant(const std::string & nameT, const std::string & nameP);

		/// \brief Plant constructor
		///
		/// Delete the manager of geometry and topology
		virtual ~Plant(void);

		/// \brief Set the name of the plant
		inline void setName(const std::string & str){ v_nameTree=str;}

		// \brief Set the fullname for the parameter file
//		inline void setParamName(const std::string & str){ v_paramFileName=str;}
		void setParamName(const std::string mod, const std::string & str); 
		
		/// \return the name of the plant
		inline const std::string & getName () { return v_nameTree;}

		/// \return The fullname for the parameter file
//		inline const std::string & getParamName () { return v_paramFileName;}
		std::string & getParamName (std::string mod);
		inline const MapParameterFileName & getParamName () { return mapParameterFileName;}

		inline void setParamData(const std::string mod, void *d){std::string s(mod);mapParameterData.insert(ParameterDataPair(s, d));}
		inline void *getParamData (std::string mod) { if (mapParameterData.find(mod)==mapParameterData.end()) return NULL; return mapParameterData.find(mod)->second;}

		/// \return the manager of geometry
		inline GeomManager & getGeometry () { return *v_geomMan;}

		/// \return the manager of topology
		inline TopoManager & getTopology () { return *v_topoMan;}

		/// \return the configuration data
		inline VitisConfig & getConfigData () { return *v_config;}

		/// \return the plant factory (factory)
		inline PlantBuilder * getPlantFactory () { return v_pltBuilder;}

		/// \return the action scheduler for this plant
//		inline Scheduler * getScheduler () { return v_actionScheduler;}

		/// \return the action scheduler for this plant
		inline VitisCoreSignalInterface & getSignalInterface () { return *v_signal;}

		/// \brief Return TRUE if the plant has finished her organogenesis for the current cycle  , FALSE otherwise
		short hasFinishedCurrentCycleOrgano (int numCycleP);

		/// \brief Set the position of the plant in the plantation
		virtual void setPosition(const Vector3 & pos);

		/// \brief Get the position of the plant in the plantation
		const Vector3 & getPosition();

		/// \brief Set the moment to pause the simualtion
		/// @param cycle The moment (in cycle number) to stop simulation
		void setHotStop(double cycle);

		/// \brief Set the moment to start the simualtion
		/// @param cycle The moment (in cycle number) to start simulation
		void setHotStart(double cycle);


		/// \brief Launch deserialization
		/// @param serialName the serialization filename
		void deserializeFrom(const std::string & serialName);


		/// \brief Set the moment to serialize the plant
		void setSerialization(double time);

		/// \brief Set the random seed
		/// @param seed	A positive value
		void setSeed(USint seed);

		/// \brief method to seed the plant  at the given time
		virtual void seedPlant (float time);

		/// \brief method to run the simulation of the plant
		virtual void run (float timeShift=0, float timeStart=-1, float timeStop=-1);

		/// \brief  init the plant
		virtual void init();

		/// \brief  launch the geometry manager
		virtual void computeGeometry(bool silent=false);

		/// \brief  init the current instance of plant builder
		virtual PlantBuilder * instanciatePlantBuilder()=0;

		/// \brief Serialize a Plant
		/// \param ar A reference to the archive \see Archive
		virtual void serialize(Archive& ar );


		virtual DecompAxeLevel *getDecompAxeLevel (int id);
};


#endif
