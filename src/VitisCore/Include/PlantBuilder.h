/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _PLANT_BUILDER_H_
#define _PLANT_BUILDER_H_
#include "GeomManager.h"
#include "Topology.h"
#include "scheduler.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class PlantBuilder
///			\brief Plant component factory. It is a member of Plant
///
///			Plant component factory. It is a member of Plant. \n
///			It provides with virtual functions to create topological and geometrical branches or branch components. \n
///			Thoose function may be overwritten for model purpose if it intends to use specialized classes of the base one.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

VITIS_EXPORT typedef std::map <std::string, int> LevelOfDecompMap;
VITIS_EXPORT typedef std::pair<std::string , int> LevelOfDecompPair;

class VITIS_EXPORT PlantBuilder
{

	protected:

		/// \brief The mother plant
		class Plant * v_plt;


	public:

		/// \brief Number of decomposition level for an axe
		LevelOfDecompMap modelLevelOfDecomp;

		/// \brief Default constructor
		PlantBuilder(class Plant * plt);

		/// \brief Destructor
		virtual ~PlantBuilder(void);

		/// \brief Create a new geomtrical branch (this method should be specialized to return an instance of a specialized class GeomBranc in the model)
		///
		/// Create a new geomtrical branch (this method should be specialized to return an instance of a specialized class GeomBranc in the model)
		/// @param brc The topological bearer
		/// @param gbr The geometrical bearer
		/// @param gt The geometrical manager
		///	@return An instance of GeomBranc
		virtual GeomBranc * createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr);
		virtual GeomBranc * createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr, char *type) {return createInstanceGeomBranc(brc, gbr);};

		/// \brief Create a new branch (this method should be specialized to return an instance of a specialized class Branc in the model)
		///
		/// Create a new branch (this method should be specialized to return an instance of a specialized class Branc in the model)
		///	@return An instance of Branc
		virtual Branc * createInstanceBranc ();
		virtual Branc * createInstanceBranc (char *type) {return createInstanceBranc();};

		/// \brief Create a new instance of an element of decomposition (this method should be specialized to return an instance of a specialized class DecompAxeLevel in the model)
		///
		/// Create a new instance of an element of decomposition (this method should be specialized to return an instance of a specialized class DecompAxeLevel in the model)
		/// @param level The level of the element to instanciate
		///	@return An instance of DecompAxeLevel
		virtual DecompAxeLevel * createInstanceDecompAxe (USint level);
        virtual DecompAxeLevel * createInstanceDecompAxe (USint level, char *type) {return createInstanceDecompAxe(level);};

		/// \brief Create a new geometrical element (this method should be specialized to return an instance of a specialized class GeomElem in the model)
		///
		/// Create a new geometrical element (this method should be specialized to return an instance of a specialized class GeomElem in the model)
		///	@return An instance of GeomElem
		virtual GeomElem * createInstanceGeomElem ();

		/// \brief Set the level of axe decomposition
		void setLevelOfDecomposition(std::string mod, int level);
		/// \brief get the level of axe decomposition
		int getLevelOfDecomposition(std::string mod);

		/// \brief get the level of axe decomposition
		class Plant * V_plt(){ return v_plt;}

		virtual VProcess *createInstanceBud(int n,...)=0;
};


#endif
