/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include "ObserverManager.h"
#include "Plant.h"
#include "DebugNew.h"

ObserverManager::ObserverManager()
{

	myObs=NULL;

}

ObserverManager::~ObserverManager(void)
{

	if(!vecObj.empty())
	{
		for(unsigned int i=0; i<vecObj.size(); i++)
		{
			delete vecObj[i];
		}
		vecObj.clear();
	}

	if(myObs != NULL)
	{

		for(unsigned int i=0; i<myObs->size(); i++)
		{
			delete myObs->at(i);
		}

		myObs->clear();

		delete myObs;

	}

}



void ObserverManager::scanForExternalObservers(Plant * plt)
{
	int i;
	VitisObject * obs;
	myObs = new vector<Vitis::CPlugin<VitisObject> *> ();

	for (i=0; i<plt->getConfigData().nbExternModules; i++)
	{
		myObs->push_back(new Vitis::CPlugin<VitisObject> ());

		try
		{
			Vitis::CPlugin<VitisObject> *cp = myObs->back();
			myObs->back()->Load(plt->getConfigData().ExternModules[i]);
			if (!plt->getConfigData().ExternModulesParam[i].empty())
				obs=myObs->back()->Start(plt->getConfigData().ExternModulesParam[i], plt);
			else
				obs=myObs->back()->Start(plt);
			vecObj.push_back(obs);

		} catch(Vitis::CException e){
		    std::cout <<e.what()<<std::endl;
            char rep;
            std::cout << "continue simulation (y/n)? "<<std::endl;
            std::cin >> rep;
            if (rep != 'y')
                exit(0);
        };

	}

}

