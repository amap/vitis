/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "VitisThread.h"

////Implementation thread for Windows

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------

#ifdef WIN32
#include <process.h>
#include <windows.h>
#include <windef.h>
#include <tchar.h>

// ----------------------------------------------------------------------------
// constants
// ----------------------------------------------------------------------------

// the possible states of the thread ("=>" shows all possible transitions from
// this state)
enum vtsThreadState
{
	STATE_NEW,          // didn't start execution yet (=> RUNNING)
	STATE_RUNNING,      // thread is running (=> PAUSED, CANCELED)
	STATE_PAUSED,       // thread is temporarily suspended (=> RUNNING)
	STATE_CANCELED,     // thread should terminate a.s.a.p. (=> EXITED)
	STATE_EXITED        // thread is terminating
};

// ----------------------------------------------------------------------------
// this module globals
// ----------------------------------------------------------------------------

// TLS index of the slot where we store the pointer to the current thread
static DWORD gs_tlsThisThread = 0xFFFFFFFF;

// id of the main thread - the one which can call GUI functions without first
// calling vtsMutexGuiEnter()
static DWORD gs_idMainThread = 0;

// if it's FALSE, some secondary thread is holding the GUI lock
static bool gs_bGuiOwnedByMainThread = TRUE;

// critical section which controls access to all GUI functions: any secondary
// thread (i.e. except the main one) must enter this crit section before doing
// any GUI calls
static vtsCriticalSection *gs_critsectGui = NULL;

// critical section which protects gs_nWaitingForGui variable
static vtsCriticalSection *gs_critsectWaitingForGui = NULL;

// number of threads waiting for GUI in vtsMutexGuiEnter()
static size_t gs_nWaitingForGui = 0;

// are we waiting for a thread termination?
static bool gs_waitingForThread = FALSE;

// ============================================================================
// Windows implementation of thread classes
// ============================================================================


void vtsThreadModule::Init()
{
	gs_tlsThisThread=::TlsAlloc();

}

void vtsThreadModule::Exit()
{
	::TlsFree(gs_tlsThisThread);

}

// ----------------------------------------------------------------------------
// vtsMutex implementation
// ----------------------------------------------------------------------------

class vtsMutexInternal
{
	public:
		vtsMutexInternal()
		{
			m_mutex = ::CreateMutex(NULL, FALSE, NULL);
			if ( !m_mutex )
			{
				//vtsLogSysError(_("Can not create mutex"));
			}
		}

		~vtsMutexInternal() { if ( m_mutex ) CloseHandle(m_mutex); }

	public:
		HANDLE m_mutex;
};

vtsMutex::vtsMutex()
{
	m_internal = new vtsMutexInternal;

	m_locked = 0;
}

vtsMutex::~vtsMutex()
{
	if ( m_locked > 0 )
	{
		//vtsLogDebug(_T("Warning: freeing a locked mutex (%d locks)."), m_locked);
	}

	delete m_internal;
}

vtsMutexError vtsMutex::Lock()
{
	DWORD ret;

	ret = WaitForSingleObject(m_internal->m_mutex, INFINITE);
	switch ( ret )
	{
		case WAIT_ABANDONED:
			return vtsMUTEX_BUSY;

		case WAIT_OBJECT_0:
			// ok
			break;

		case WAIT_FAILED:
			//LogSysError(_("Couldn't acquire a mutex lock"));
			return vtsMUTEX_MISC_ERROR;

		case WAIT_TIMEOUT: break;
		default: break;
					//vtsFAIL_MSG(vtsT("impossible return value in vtsMutex::Lock"));
	}

	m_locked++;
	return vtsMUTEX_NO_ERROR;
}

vtsMutexError vtsMutex::TryLock()
{
	DWORD ret;

	ret = WaitForSingleObject(m_internal->m_mutex, 0);
	if (ret == WAIT_TIMEOUT || ret == WAIT_ABANDONED)
		return vtsMUTEX_BUSY;

	m_locked++;
	return vtsMUTEX_NO_ERROR;
}

vtsMutexError vtsMutex::Unlock()
{
	if (m_locked > 0)
		m_locked--;

	BOOL ret = ReleaseMutex(m_internal->m_mutex);
	if ( ret == 0 )
	{
		//vtsLogSysError(_("Couldn't release a mutex"));
		return vtsMUTEX_MISC_ERROR;
	}

	return vtsMUTEX_NO_ERROR;
}

// ----------------------------------------------------------------------------
// vtsCondition implementation
// ----------------------------------------------------------------------------

class vtsConditionInternal
{
	public:
		vtsConditionInternal()
		{
			eventData = ::CreateEvent(
					NULL,   // default secutiry
					FALSE,  // not manual reset
					FALSE,  // nonsignaled initially
					NULL    // nameless eventData
					);
			if ( !eventData )
			{
				// vtsLogSysError(_("Can not create eventData object."));
			}
			waiters = 0;
		}

		bool Wait(DWORD timeout)
		{
			waiters++;

			// FIXME this should be MsgWaitForMultipleObjects() as well probably
			DWORD rc = ::WaitForSingleObject(eventData, timeout);

			waiters--;

			return rc != WAIT_TIMEOUT;
		}

		~vtsConditionInternal()
		{
			if ( eventData )
			{
				if ( !::CloseHandle(eventData) )
				{
					//  vtsLogLastError(vtsT("CloseHandle(eventData)"));
				}
			}
		}

		HANDLE eventData;
		int waiters;
};

vtsCondition::vtsCondition()
{
	m_internal = new vtsConditionInternal;
}

vtsCondition::~vtsCondition()
{
	delete m_internal;
}

void vtsCondition::Wait()
{
	(void)m_internal->Wait(INFINITE);
}

bool vtsCondition::Wait(unsigned long sec,
		unsigned long nsec)
{
	return m_internal->Wait(sec*1000 + nsec/1000000);
}

void vtsCondition::Signal()
{
	// set the eventData to signaled: if a thread is already waiting on it, it will
	// be woken up, otherwise the eventData will remain in the signaled state until
	// someone waits on it. In any case, the system will return it to a non
	// signalled state afterwards. If multiple threads are waiting, only one
	// will be woken up.
	if ( !::SetEvent(m_internal->eventData) )
	{
		//vtsLogLastError(vtsT("SetEvent"));
	}
}

void vtsCondition::Broadcast()
{
	// this works because all these threads are already waiting and so each
	// SetEvent() inside Signal() is really a PulseEvent() because the eventData
	// state is immediately returned to non-signaled
	for ( int i = 0; i < m_internal->waiters; i++ )
	{
		Signal();
	}
}

// ----------------------------------------------------------------------------
// vtsCriticalSection implementation
// ----------------------------------------------------------------------------

vtsCriticalSection::vtsCriticalSection()
{
	//vtsASSERT_MSG( sizeof(CRITICAL_SECTION) <= sizeof(m_buffer),
	//            _T("must increase buffer size in vts/thread.h") );

	::InitializeCriticalSection((CRITICAL_SECTION *)m_buffer);
}

vtsCriticalSection::~vtsCriticalSection()
{
	::DeleteCriticalSection((CRITICAL_SECTION *)m_buffer);
}

void vtsCriticalSection::Enter()
{
	::EnterCriticalSection((CRITICAL_SECTION *)m_buffer);
}

void vtsCriticalSection::Leave()
{
	::LeaveCriticalSection((CRITICAL_SECTION *)m_buffer);
}

// ----------------------------------------------------------------------------
// vtsThread implementation
// ----------------------------------------------------------------------------

// vtsThreadInternal class
// ----------------------

class vtsThreadInternal
{
	public:
		vtsThreadInternal()
		{
			m_hThread = 0;
			m_state = STATE_NEW;
			m_priority = VTSTHREAD_DEFAULT_PRIORITY;
		}

		~vtsThreadInternal()
		{
			Free();
		}

		void Free()
		{
			if ( m_hThread )
			{
				if ( !::CloseHandle(m_hThread) )
				{
					//vtsLogLastError(vtsT("CloseHandle(thread)"));
				}

				m_hThread = 0;
			}
		}

		// create a new (suspended) thread (for the given thread object)
		bool Create(vtsThread *thread);

		// suspend/resume/terminate
		bool Suspend();
		bool Resume();
		void Cancel() { m_state = STATE_CANCELED; }

		// thread state
		void SetState(vtsThreadState state) { m_state = state; }
		vtsThreadState GetState() const { return m_state; }

		// thread priority
		void SetPriority(unsigned int priority);
		unsigned int GetPriority() const { return m_priority; }

		// thread handle and id
		HANDLE GetHandle() const { return m_hThread; }
		DWORD  GetId() const { return m_tid; }

		// thread function
		static DWORD WinThreadStart(vtsThread *thread);

	private:
		HANDLE        m_hThread;    // handle of the thread
		vtsThreadState m_state;      // state, see vtsThreadState enum
		unsigned int  m_priority;   // thread priority in "vts" units
		DWORD         m_tid;        // thread id
};

DWORD vtsThreadInternal::WinThreadStart(vtsThread *thread)
{
	DWORD rc;
	bool wasCancelled;

	// first of all, check whether we hadn't been cancelled already and don't
	// start the user code at all then
	if ( thread->m_internal->GetState() == STATE_EXITED )
	{
		rc = (DWORD)-1;
		wasCancelled = TRUE;
	}
	else // do run thread
	{
		// store the thread object in the TLS
		if ( !::TlsSetValue(gs_tlsThisThread, thread) )
		{
			//            vtsLogSysError(_("Can not start thread: error writing TLS."));

			return (DWORD)-1;
		}

		rc = (DWORD)thread->Entry();

		// enter m_critsect before changing the thread state
		thread->m_critsect.Enter();
		wasCancelled = thread->m_internal->GetState() == STATE_CANCELED;
		thread->m_internal->SetState(STATE_EXITED);
		thread->m_critsect.Leave();
	}

	thread->OnExit();

	// if the thread was cancelled (from Delete()), then its handle is still
	// needed there
	if ( thread->IsDetached() && !wasCancelled )
	{
		// auto delete
		delete thread;
	}
	//else: the joinable threads handle will be closed when Wait() is done

	return rc;
}

void vtsThreadInternal::SetPriority(unsigned int priority)
{
	m_priority = priority;

	// translate vtsWindows priority to the Windows one
	int win_priority;
	if (m_priority <= 20)
		win_priority = THREAD_PRIORITY_LOWEST;
	else if (m_priority <= 40)
		win_priority = THREAD_PRIORITY_BELOW_NORMAL;
	else if (m_priority <= 60)
		win_priority = THREAD_PRIORITY_NORMAL;
	else if (m_priority <= 80)
		win_priority = THREAD_PRIORITY_ABOVE_NORMAL;
	else if (m_priority <= 100)
		win_priority = THREAD_PRIORITY_HIGHEST;
	else
	{
		//vtsFAIL_MSG(vtsT("invalid value of thread priority parameter"));
		win_priority = THREAD_PRIORITY_NORMAL;
	}

	if ( !::SetThreadPriority(m_hThread, win_priority) )
	{
		//vtsLogSysError(_("Can't set thread priority"));
	}
}

bool vtsThreadInternal::Create(vtsThread *thread)
{
	// for compilers which have it, we should use C RTL function for thread
	// creation instead of Win32 API one because otherwise we will have memory
	// leaks if the thread uses C RTL (and most threads do)
	typedef unsigned (__stdcall *RtlThreadStart)(void *);

	m_hThread = (HANDLE)_beginthreadex(NULL, 0,
			(RtlThreadStart)
			vtsThreadInternal::WinThreadStart,
			thread, CREATE_SUSPENDED,
			(unsigned int *)&m_tid);


	if ( m_hThread == NULL )
	{
		//vtsLogSysError(_("Can't create thread"));

		return FALSE;
	}

	if ( m_priority != VTSTHREAD_DEFAULT_PRIORITY )
	{
		SetPriority(m_priority);
	}

	return TRUE;
}

bool vtsThreadInternal::Suspend()
{
	DWORD nSuspendCount = ::SuspendThread(m_hThread);
	if ( nSuspendCount == (DWORD)-1 )
	{
		//vtsLogSysError(_("Can not suspend thread %x"), m_hThread);

		return FALSE;
	}

	m_state = STATE_PAUSED;

	return TRUE;
}

bool vtsThreadInternal::Resume()
{
	DWORD nSuspendCount = ::ResumeThread(m_hThread);
	if ( nSuspendCount == (DWORD)-1 )
	{
		//vtsLogSysError(_("Can not resume thread %x"), m_hThread);

		return FALSE;
	}

	// don't change the state from STATE_EXITED because it's special and means
	// we are going to terminate without running any user code - if we did it,
	// the codei n Delete() wouldn't work
	if ( m_state != STATE_EXITED )
	{
		m_state = STATE_RUNNING;
	}

	return TRUE;
}

// static functions
// ----------------

vtsThread *vtsThread::This()
{
	vtsThread *thread = (vtsThread *)::TlsGetValue(gs_tlsThisThread);

	// be careful, 0 may be a valid return value as well
	if ( !thread && (::GetLastError() != NO_ERROR) )
	{
		//vtsLogSysError(_("Couldn't get the current thread pointer"));

		// return NULL...
	}

	return thread;
}

bool vtsThread::IsMain()
{
	return ::GetCurrentThreadId() == gs_idMainThread;
}




void vtsThread::Sleep(unsigned long milliseconds)
{
	::Sleep(milliseconds);
}

int vtsThread::GetCPUCount()
{
	SYSTEM_INFO si;
	GetSystemInfo(&si);

	return si.dwNumberOfProcessors;
}

bool vtsThread::SetConcurrency(size_t level)
{
	//vtsASSERT_MSG( IsMain(), _T("should only be called from the main thread") );

	// ok only for the default one
	if ( level == 0 )
		return 0;

	// get system affinity mask first
	HANDLE hProcess = ::GetCurrentProcess();
	DWORD dwProcMask, dwSysMask;
	if ( ::GetProcessAffinityMask(hProcess, &dwProcMask, &dwSysMask) == 0 )
	{
		//  vtsLogLastError(_T("GetProcessAffinityMask"));

		return FALSE;
	}

	// how many CPUs have we got?
	if ( dwSysMask == 1 )
	{
		// don't bother with all this complicated stuff - on a single
		// processor system it doesn't make much sense anyhow
		return level == 1;
	}

	// calculate the process mask: it's a bit vector with one bit per
	// processor; we want to schedule the process to run on first level
	// CPUs
	DWORD bit = 1;
	while ( bit )
	{
		if ( dwSysMask & bit )
		{
			// ok, we can set this bit
			dwProcMask |= bit;

			// another process added
			if ( !--level )
			{
				// and that's enough
				break;
			}
		}

		// next bit
		bit <<= 1;
	}

	// could we set all bits?
	if ( level != 0 )
	{
		//vtsLogDebug(_T("bad level %u in vtsThread::SetConcurrency()"), level);

		return FALSE;
	}

	// set it: we can't link to SetProcessAffinityMask() because it doesn't
	// exist in Win9x, use RT binding instead

	typedef BOOL (*SETPROCESSAFFINITYMASK)(HANDLE, DWORD);

	// can use static var because we're always in the main thread here
	static SETPROCESSAFFINITYMASK pfnSetProcessAffinityMask = NULL;

	if ( !pfnSetProcessAffinityMask )
	{
		HMODULE hModKernel = ::LoadLibrary(_T("kernel32"));
		if ( hModKernel )
		{
			pfnSetProcessAffinityMask = (SETPROCESSAFFINITYMASK)
				::GetProcAddress(hModKernel, "SetProcessAffinityMask");
		}

		// we've discovered a MT version of Win9x!
		//vtsASSERT_MSG( pfnSetProcessAffinityMask,
		//            _T("this system has several CPUs but no SetProcessAffinityMask function?") );
	}

	if ( !pfnSetProcessAffinityMask )
	{
		// msg given above - do it only once
		return FALSE;
	}

	if ( pfnSetProcessAffinityMask(hProcess, dwProcMask) == 0 )
	{
		//vtsLogLastError(_T("SetProcessAffinityMask"));

		return FALSE;
	}

	return TRUE;
}

// ctor and dtor
// -------------

vtsThread::vtsThread(vtsThreadKind kind)
{
	m_internal = new vtsThreadInternal();

	m_isDetached = kind == vtsTHREAD_DETACHED;
}

vtsThread::~vtsThread()
{
	delete m_internal;
}

// create/start thread
// -------------------

vtsThreadError vtsThread::Create()
{
	vtsCriticalSectionLocker lock(m_critsect);

	if ( !m_internal->Create(this) )
		return vtsTHREAD_NO_RESOURCE;

	return vtsTHREAD_NO_ERROR;
}

vtsThreadError vtsThread::Run()
{
	vtsCriticalSectionLocker lock(m_critsect);

	if ( m_internal->GetState() != STATE_NEW )
	{
		// actually, it may be almost any state at all, not only STATE_RUNNING
		return vtsTHREAD_RUNNING;
	}

	// the thread has just been created and is still suspended - let it run
	return Resume();
}

// suspend/resume thread
// ---------------------

vtsThreadError vtsThread::Pause()
{
	vtsCriticalSectionLocker lock(m_critsect);

	return m_internal->Suspend() ? vtsTHREAD_NO_ERROR : vtsTHREAD_MISC_ERROR;
}

vtsThreadError vtsThread::Resume()
{
	vtsCriticalSectionLocker lock(m_critsect);

	return m_internal->Resume() ? vtsTHREAD_NO_ERROR : vtsTHREAD_MISC_ERROR;
}

// stopping thread
// ---------------

vtsThread::ExitCode vtsThread::Wait()
{
	// although under Windows we can wait for any thread, it's an error to
	// wait for a detached one in vtsWin API
	//xCHECK_MSG( !IsDetached(), (ExitCode)-1,
	//           _T("can't wait for detached thread") );

	ExitCode rc = (ExitCode)-1;

	(void)Delete(&rc);

	m_internal->Free();

	return rc;
}

vtsThreadError vtsThread::Delete(ExitCode *pRc)
{
	ExitCode rc = 0;

	// Delete() is always safe to call, so consider all possible states

	// we might need to resume the thread, but we might also not need to cancel
	// it if it doesn't run yet
	bool shouldResume = FALSE,
		  shouldCancel = TRUE,
		  isRunning = FALSE;

	// check if the thread already started to run
	{
		vtsCriticalSectionLocker lock(m_critsect);

		if ( m_internal->GetState() == STATE_NEW )
		{
			// WinThreadStart() will see it and terminate immediately, no need
			// to cancel the thread - but we still need to resume it to let it
			// run
			m_internal->SetState(STATE_EXITED);

			Resume();   // it knows about STATE_EXITED special case

			shouldCancel = FALSE;
			isRunning = TRUE;

			// shouldResume is correctly set to FALSE here
		}
		else
		{
			shouldResume = IsPaused();
		}
	}

	// resume the thread if it is paused
	if ( shouldResume )
		Resume();

	HANDLE hThread = m_internal->GetHandle();

	// does is still run?
	if ( isRunning || IsRunning() )
	{
		if ( IsMain() )
		{
			// set flag for vtsIsWaitingForThread()
			gs_waitingForThread = TRUE;


		}

		// ask the thread to terminate
		if ( shouldCancel )
		{
			vtsCriticalSectionLocker lock(m_critsect);

			m_internal->Cancel();
		}

		// simply wait for the thread to terminate
		//
		// OTOH, even console apps create windows (in vtsExecute, for WinSock
		// &c), so may be use MsgWaitForMultipleObject() too here?
		if ( WaitForSingleObject(hThread, INFINITE) != WAIT_OBJECT_0 )
		{
			// vtsFAIL_MSG(vtsT("unexpected result of WaitForSingleObject"));
		}

		if ( IsMain() )
		{
			gs_waitingForThread = FALSE;
		}
	}

	if ( !::GetExitCodeThread(hThread, (LPDWORD)&rc) )
	{
		//  vtsLogLastError(vtsT("GetExitCodeThread"));

		rc = (ExitCode)-1;
	}

	if ( IsDetached() )
	{
		// if the thread exits normally, this is done in WinThreadStart, but in
		// this case it would have been too early because
		// MsgWaitForMultipleObject() would fail if the thread handle was
		// closed while we were waiting on it, so we must do it here
		delete this;
	}

	// vtsASSERT_MSG( (DWORD)rc != STILL_ACTIVE,
	//               vtsT("thread must be already terminated.") );

	if ( pRc )
		*pRc = rc;

	return rc == (ExitCode)-1 ? vtsTHREAD_MISC_ERROR : vtsTHREAD_NO_ERROR;
}

vtsThreadError vtsThread::Kill()
{
	if ( !IsRunning() )
		return vtsTHREAD_NOT_RUNNING;

	if ( !::TerminateThread(m_internal->GetHandle(), (DWORD)-1) )
	{
		//vtsLogSysError(_("Couldn't terminate thread"));

		return vtsTHREAD_MISC_ERROR;
	}

	m_internal->Free();

	if ( IsDetached() )
	{
		delete this;
	}

	return vtsTHREAD_NO_ERROR;
}

void vtsThread::Exit(ExitCode status)
{
	m_internal->Free();

	if ( IsDetached() )
	{
		delete this;
	}

	_endthreadex((unsigned)status);


	//    vtsFAIL_MSG(vtsT("Couldn't return from ExitThread()!"));
}

// priority setting
// ----------------

void vtsThread::SetPriority(unsigned int prio)
{
	vtsCriticalSectionLocker lock(m_critsect);

	m_internal->SetPriority(prio);
}

unsigned int vtsThread::GetPriority() const
{
	vtsCriticalSectionLocker lock((vtsCriticalSection &)m_critsect); // const_cast

	return m_internal->GetPriority();
}

unsigned long vtsThread::GetId() const
{
	vtsCriticalSectionLocker lock((vtsCriticalSection &)m_critsect); // const_cast

	return (unsigned long)m_internal->GetId();
}

bool vtsThread::IsRunning() const
{
	vtsCriticalSectionLocker lock((vtsCriticalSection &)m_critsect); // const_cast

	return m_internal->GetState() == STATE_RUNNING;
}

bool vtsThread::IsAlive() const
{
	vtsCriticalSectionLocker lock((vtsCriticalSection &)m_critsect); // const_cast

	return (m_internal->GetState() == STATE_RUNNING) ||
		(m_internal->GetState() == STATE_PAUSED);
}

bool vtsThread::IsPaused() const
{
	vtsCriticalSectionLocker lock((vtsCriticalSection &)m_critsect); // const_cast

	return m_internal->GetState() == STATE_PAUSED;
}

bool vtsThread::TestDestroy()
{
	vtsCriticalSectionLocker lock((vtsCriticalSection &)m_critsect); // const_cast

	return m_internal->GetState() == STATE_CANCELED;
}
#else
class vtsMutexInternal
{
	public:
		pthread_mutex_t m_mutex;
};

vtsMutex::vtsMutex()
{
	m_internal = new vtsMutexInternal;

	pthread_mutex_init(&(m_internal->m_mutex),
			(pthread_mutexattr_t*) NULL );
	m_locked = 0;
}

vtsMutex::~vtsMutex()
{
	//     if (m_locked > 0)
	//         vtsLogDebug(vtsT("Freeing a locked mutex (%d locks)"), m_locked);

	pthread_mutex_destroy( &(m_internal->m_mutex) );
	delete m_internal;
}

vtsMutexError vtsMutex::Lock()
{
	int err = pthread_mutex_lock( &(m_internal->m_mutex) );
	if (err == EDEADLK)
	{
		//        vtsLogDebug(vtsT("Locking this mutex would lead to deadlock!"));

		return vtsMUTEX_DEAD_LOCK;
	}

	m_locked++;

	return vtsMUTEX_NO_ERROR;
}

vtsMutexError vtsMutex::Unlock()
{
	if (m_locked > 0)
	{
		m_locked--;
	}
	else
	{
		//        vtsLogDebug(vtsT("Unlocking not locked mutex."));

		return vtsMUTEX_UNLOCKED;
	}

	pthread_mutex_unlock( &(m_internal->m_mutex) );

	return vtsMUTEX_NO_ERROR;
}

#endif


