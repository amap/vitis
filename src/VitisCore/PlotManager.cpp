/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "PlotManager.h"
#include <math.h>
#include <vector>
#include "std_serialize.h"
#include "DebugNew.h"
#include "Exceptions.h"
#include <exception>
#include <iomanip>

using namespace std;

PlotManager::PlotManager()
{
	for (vector<Plant *>::iterator it=listPlant.begin(); it!=listPlant.end(); it++)
	{
        listPlant.erase(it);
	}
}

PlotManager::~PlotManager()
{
	listPlant.clear();
}



bool PlotManager::add(Plant *plt )
{
	listPlant.push_back(plt);
	return true;
}

void PlotManager::remove(Plant *plt )
{
	for (vector<Plant *>::iterator it=listPlant.begin(); it!=listPlant.end(); it++)
	{
		if (*it == plt)
		{
			listPlant.erase(it);
			break;
		}
	}
}

void PlotManager::serialize(Archive& ar )
{

    Plant *p;
    ArchivableObject * arVp;
	if( ar.isWriting() )
	{
        ar <<listPlant.size();
        for (vector<Plant*>::iterator it=listPlant.begin(); it!=listPlant.end(); it++)
        {
            p = *it;
            this->SavePtr(ar, p);
        }
	}
	else
	{
        int nb;
        ar>>nb;
		for (int i=0; i<nb; i++)
		{
		    this->LoadPtr(ar, arVp);
		    p = (Plant*)arVp;
            add(p);
		}
	}

}

bool PlotManager::checkCoordinates (Plant *plt)
{
    Plant *p;
    char rep;
    for (vector<Plant *>::iterator it=listPlant.begin(); it!=listPlant.end(); it++)
    {
        p = *it;
        if (   p != plt
            && p->getGeometry().getPosition().x() == plt->getGeometry().getPosition().x()
            && p->getGeometry().getPosition().y() == plt->getGeometry().getPosition().y())
        {
            cout << "plant "<< p->getName()<<" and plant "<<plt->getName()<< " have same coordinates "<<p->getGeometry().getPosition().x()<<" "<<p->getGeometry().getPosition().y()<<endl;
            cout << "continue simulation (y/n)? "<<endl;
            cin >> rep;
            if (rep != 'y')
                exit(0);
        }
    }
    return true;
}

PlotManager* PlotManager::instance()
{
	static PlotManager *p_=NULL;
	if (p_ == NULL)
		p_ = new PlotManager();
	return p_;
}
