/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "Matrix.h"
#include "UtilMath.h"

#include <iostream>


using namespace std;


namespace Vitis {
    /*  --------------------------------------------------------------------- */

#define GEOM_DET2(m0, m1, m2, m3) \
    (m0 * m3 - m1 * m2)

#define GEOM_DET3(m0, m1, m2, m3, m4, m5, m6, m7, m8) \
    m0 * GEOM_DET2(m4,m5,m7,m8) - \
    m3 * GEOM_DET2(m1,m2,m7,m8) + \
    m6 * GEOM_DET2(m1,m2,m4,m5)


    /*  --------------------------------------------------------------------- */

    const Matrix4 Matrix4::IDENTITY;

    /*  --------------------------------------------------------------------- */

    Matrix4::Matrix4(const float &m0, const float &m1,
                     const float &m2, const float &m3,
                     const float &m4, const float &m5,
                     const float &m6, const float &m7,
                     const float &m8, const float &m9,
                     const float &m10, const float &m11,
                     const float &m12, const float &m13,
                     const float &m14, const float &m15) {
        set(m0, m1, m2, m3, m4, m5, m6, m7, m8, m9, m10, m11, m12, m13, m14, m15);
        length = 1;
    }

    Matrix4::Matrix4(const float *matrix) {
        set(matrix[0], matrix[1], matrix[2], matrix[3],
            matrix[4], matrix[5], matrix[6], matrix[7],
            matrix[8], matrix[9], matrix[10], matrix[11],
            matrix[12], matrix[13], matrix[14], matrix[15]);
        length = ((Matrix4*)matrix)->getLength();
    }

    Matrix4::Matrix4(const Vector4 &v0,
                     const Vector4 &v1,
                     const Vector4 &v2,
                     const Vector4 &v3) {
        set(v0.x(), v1.x(), v2.x(), v3.x(),
            v0.y(), v1.y(), v2.y(), v3.y(),
            v0.z(), v1.z(), v2.z(), v3.z(),
            v0.w(), v1.w(), v2.w(), v3.w());
        length = 1;
    }


    Matrix4::~Matrix4() {
    }

    const float &Matrix4::operator()(Uchar i, Uchar j) const {
        //GEOM_ASSERT((i < 4) && (j < 4));
        return (__M[i * 4 + j]);
    }

    float &Matrix4::operator()(Uchar i, Uchar j) {
        //GEOM_ASSERT((i < 4) && (j < 4));
        return (__M[i * 4 + j]);
    }

    const float *Matrix4::getData() const {
        //GEOM_ASSERT(isValid());
        return __M;
    }

    float *Matrix4::getData() {
        //GEOM_ASSERT(isValid());
        return __M;
    }


    Vector3 Matrix4::getMainVector() const {
        return Vector3(__M[2], __M[6], __M[10]);
    }

    Vector3 Matrix4::getNormalVector() const {
        return Vector3(__M[1], __M[5], __M[9]);
    }

    Vector3 Matrix4::getSecondaryVector() const {
        return Vector3(__M[0], __M[4], __M[8]);
    }


    Vector3 Matrix4::getTranslation() const {
        return Vector3(__M[3], __M[7], __M[11]);
    }


    void Matrix4::setPlace(int i, float val){
        __M[i] = val;
    }

    float Matrix4::getPlace(int i) {
        return  __M[i];
    }

    void Matrix4::setTranslation(float x, float y, float z) {
        __M[3] = x;
        __M[7] = y;
        __M[11] = z;

    }

    void Matrix4::setSecondaryVector(float x, float y, float z) {
        __M[0] = x;
        __M[4] = y;
        __M[8] = z;

    }

    void Matrix4::setNormalVector(float x, float y, float z) {
        __M[1] = x;
        __M[5] = y;
        __M[9] = z;

    }

    void Matrix4::setMainVector(float x, float y, float z) {
        __M[2] = x;
        __M[6] = y;
        __M[10] = z;
    }


    Vector4 Matrix4::getColumn(Uchar i) const {
        ///GEOM_ASSERT(i < 4);
        return Vector4(__M[i], __M[4 + i], __M[8 + i], __M[12 + i]);
    }

    Vector4 Matrix4::getDiagonal() const {
        //GEOM_ASSERT(isValid());
        return Vector4(__M[0], __M[5], __M[10], __M[15]);
    }

    Vector4 Matrix4::getRow(int i) const {
        // GEOM_ASSERT(i < 4);
        int _offset = i * 4;
        return Vector4(__M[_offset], __M[_offset + 1], __M[_offset + 2], __M[_offset + 3]);
    }

    void Matrix4::set(const float &m0, const float &m1, const float &m2, const float &m3,
                      const float &m4, const float &m5, const float &m6, const float &m7,
                      const float &m8, const float &m9, const float &m10, const float &m11,
                      const float &m12, const float &m13, const float &m14, const float &m15) {

        __M[0] = m0;
        __M[1] = m1;
        __M[2] = m2;
        __M[3] = m3;
        __M[4] = m4;
        __M[5] = m5;
        __M[6] = m6;
        __M[7] = m7;
        __M[8] = m8;
        __M[9] = m9;
        __M[10] = m10;
        __M[11] = m11;
        __M[12] = m12;
        __M[13] = m13;
        __M[14] = m14;
        __M[15] = m15;
    }

    /*  --------------------------------------------------------------------- */

    bool Matrix4::operator==(const Matrix4 &m) const {
        return
                (fabs(__M[0] - m.__M[0]) < GEOM_TOLERANCE) &&
                (fabs(__M[1] - m.__M[1]) < GEOM_TOLERANCE) &&
                (fabs(__M[2] - m.__M[2]) < GEOM_TOLERANCE) &&
                (fabs(__M[3] - m.__M[3]) < GEOM_TOLERANCE) &&
                (fabs(__M[4] - m.__M[4]) < GEOM_TOLERANCE) &&
                (fabs(__M[5] - m.__M[5]) < GEOM_TOLERANCE) &&
                (fabs(__M[6] - m.__M[6]) < GEOM_TOLERANCE) &&
                (fabs(__M[7] - m.__M[7]) < GEOM_TOLERANCE) &&
                (fabs(__M[8] - m.__M[8]) < GEOM_TOLERANCE) &&
                (fabs(__M[9] - m.__M[9]) < GEOM_TOLERANCE) &&
                (fabs(__M[10] - m.__M[10]) < GEOM_TOLERANCE) &&
                (fabs(__M[11] - m.__M[11]) < GEOM_TOLERANCE) &&
                (fabs(__M[12] - m.__M[12]) < GEOM_TOLERANCE) &&
                (fabs(__M[13] - m.__M[13]) < GEOM_TOLERANCE) &&
                (fabs(__M[14] - m.__M[14]) < GEOM_TOLERANCE) &&
                (fabs(__M[15] - m.__M[15]) < GEOM_TOLERANCE);
    }

    bool Matrix4::operator!=(const Matrix4 &m) const {
        return !(operator==(m));
    }

    Matrix4 &Matrix4::operator+=(const Matrix4 &m) {
        __M[0] += m.__M[0];
        __M[1] += m.__M[1];
        __M[2] += m.__M[2];
        __M[3] += m.__M[3];
        __M[4] += m.__M[4];
        __M[5] += m.__M[5];
        __M[6] += m.__M[6];
        __M[7] += m.__M[7];
        __M[8] += m.__M[8];
        __M[9] += m.__M[9];
        __M[10] += m.__M[10];
        __M[11] += m.__M[11];
        __M[12] += m.__M[12];
        __M[13] += m.__M[13];
        __M[14] += m.__M[14];
        __M[15] += m.__M[15];
        return *this;
    }

    Matrix4 &Matrix4::operator-=(const Matrix4 &m) {
        __M[0] -= m.__M[0];
        __M[1] -= m.__M[1];
        __M[2] -= m.__M[2];
        __M[3] -= m.__M[3];
        __M[4] -= m.__M[4];
        __M[5] -= m.__M[5];
        __M[6] -= m.__M[6];
        __M[7] -= m.__M[7];
        __M[8] -= m.__M[8];
        __M[9] -= m.__M[9];
        __M[10] -= m.__M[10];
        __M[11] -= m.__M[11];
        __M[12] -= m.__M[12];
        __M[13] -= m.__M[13];
        __M[14] -= m.__M[14];
        __M[15] -= m.__M[15];
        return *this;
    }

    Matrix4 &Matrix4::operator*=(const Matrix4 &m) {
        *this = *this * m;
        return *this;
    }

    Matrix4 &Matrix4::operator*=(const float &s) {
        __M[0] *= s;
        __M[1] *= s;
        __M[2] *= s;
        __M[3] *= s;
        __M[4] *= s;
        __M[5] *= s;
        __M[6] *= s;
        __M[7] *= s;
        __M[8] *= s;
        __M[9] *= s;
        __M[10] *= s;
        __M[11] *= s;
        __M[12] *= s;
        __M[13] *= s;
        __M[14] *= s;
        __M[15] *= s;
        return *this;
    }

    Matrix4 &Matrix4::operator/=(const float &s) {
        //GEOM_ASSERT(fabs(s) > GEOM_TOLERANCE);
        __M[0] /= s;
        __M[1] /= s;
        __M[2] /= s;
        __M[3] /= s;
        __M[4] /= s;
        __M[5] /= s;
        __M[6] /= s;
        __M[7] /= s;
        __M[8] /= s;
        __M[9] /= s;
        __M[10] /= s;
        __M[11] /= s;
        __M[12] /= s;
        __M[13] /= s;
        __M[14] /= s;
        __M[15] /= s;
        return *this;
    }

    Matrix4 Matrix4::operator-() const {
        return Matrix4(-__M[0], -__M[1], -__M[2], -__M[3],
                       -__M[4], -__M[5], -__M[6], -__M[7],
                       -__M[8], -__M[9], -__M[10], -__M[11],
                       -__M[12], -__M[13], -__M[14], -__M[15]);
    }

    Matrix4 Matrix4::operator+(const Matrix4 &m) const {
        return Matrix4(*this) += m;
    }

    Matrix4 Matrix4::operator-(const Matrix4 &m) const {
        return Matrix4(*this) -= m;
    }

    Matrix4 Matrix4::operator*(const Matrix4 &m) const {
        return Matrix4(m.__M[0] * __M[0] + m.__M[4] * __M[1] +
                       m.__M[8] * __M[2] + m.__M[12] * __M[3],
                       m.__M[1] * __M[0] + m.__M[5] * __M[1] +
                       m.__M[9] * __M[2] + m.__M[13] * __M[3],
                       m.__M[2] * __M[0] + m.__M[6] * __M[1] +
                       m.__M[10] * __M[2] + m.__M[14] * __M[3],
                       m.__M[3] * __M[0] + m.__M[7] * __M[1] +
                       m.__M[11] * __M[2] + m.__M[15] * __M[3],

                       m.__M[0] * __M[4] + m.__M[4] * __M[5] +
                       m.__M[8] * __M[6] + m.__M[12] * __M[7],
                       m.__M[1] * __M[4] + m.__M[5] * __M[5] +
                       m.__M[9] * __M[6] + m.__M[13] * __M[7],
                       m.__M[2] * __M[4] + m.__M[6] * __M[5] +
                       m.__M[10] * __M[6] + m.__M[14] * __M[7],
                       m.__M[3] * __M[4] + m.__M[7] * __M[5] +
                       m.__M[11] * __M[6] + m.__M[15] * __M[7],

                       m.__M[0] * __M[8] + m.__M[4] * __M[9] +
                       m.__M[8] * __M[10] + m.__M[12] * __M[11],
                       m.__M[1] * __M[8] + m.__M[5] * __M[9] +
                       m.__M[9] * __M[10] + m.__M[13] * __M[11],
                       m.__M[2] * __M[8] + m.__M[6] * __M[9] +
                       m.__M[10] * __M[10] + m.__M[14] * __M[11],
                       m.__M[3] * __M[8] + m.__M[7] * __M[9] +
                       m.__M[11] * __M[10] + m.__M[15] * __M[11],

                       m.__M[0] * __M[12] + m.__M[4] * __M[13] +
                       m.__M[8] * __M[14] + m.__M[12] * __M[15],
                       m.__M[1] * __M[12] + m.__M[5] * __M[13] +
                       m.__M[9] * __M[14] + m.__M[13] * __M[15],
                       m.__M[2] * __M[12] + m.__M[6] * __M[13] +
                       m.__M[10] * __M[14] + m.__M[14] * __M[15],
                       m.__M[3] * __M[12] + m.__M[7] * __M[13] +
                       m.__M[11] * __M[14] + m.__M[15] * __M[15]);
    }

    bool Matrix4::isOrthogonal() const {
        return
                getColumn(0).isOrthogonalTo(getColumn(1)) &&
                getColumn(0).isOrthogonalTo(getColumn(2)) &&
                getColumn(0).isOrthogonalTo(getColumn(3)) &&
                getColumn(1).isOrthogonalTo(getColumn(2)) &&
                getColumn(1).isOrthogonalTo(getColumn(3)) &&
                getColumn(2).isOrthogonalTo(getColumn(3));
    }

    bool Matrix4::isSingular() const {
        //GEOM_ASSERT(isValid());
        return fabs(det(*this)) < GEOM_TOLERANCE;
    }

    bool Matrix4::isValid() const {
        return
                finite(__M[0]) && finite(__M[1]) &&
                finite(__M[2]) && finite(__M[3]) &&
                finite(__M[4]) && finite(__M[5]) &&
                finite(__M[6]) && finite(__M[7]) &&
                finite(__M[8]) && finite(__M[9]) &&
                finite(__M[10]) && finite(__M[11]) &&
                finite(__M[12]) && finite(__M[13]) &&
                finite(__M[14]) && finite(__M[15]);
    }


    /*  --------------------------------------------------------------------- */

    Vector4 operator*(const Matrix4 &m, const Vector4 &v) {
        return Vector4(m.__M[0] * v.x() + m.__M[1] * v.y() +
                       m.__M[2] * v.z() + m.__M[3] * v.w(),
                       m.__M[4] * v.x() + m.__M[5] * v.y() +
                       m.__M[6] * v.z() + m.__M[7] * v.w(),
                       m.__M[8] * v.x() + m.__M[9] * v.y() +
                       m.__M[10] * v.z() + m.__M[11] * v.w(),
                       m.__M[12] * v.x() + m.__M[13] * v.y() +
                       m.__M[14] * v.z() + m.__M[15] * v.w());
    }

    Vector3 operator*(const Matrix4 &m, const Vector3 &v) {
        register double h = m(3, 0) * v.x()
                            + m(3, 1) * v.y()
                            + m(3, 2) * v.z()
                            + m(3, 3);
        h = 1. / h;
        register float x = v.x(), y = v.y(), z = v.z();
        return
                Vector3((m(0, 0) * x + m(0, 1) * y + m(0, 2) * z + m(0, 3)) * (float) h,
                        (m(1, 0) * x + m(1, 1) * y + m(1, 2) * z + m(1, 3)) * (float) h,
                        (m(2, 0) * x + m(2, 1) * y + m(2, 2) * z + m(2, 3)) * (float) h);
    }

    Matrix4 operator*(const Matrix4 &m, const float &s) {
        return Matrix4(m) *= s;
    }

    Matrix4 operator/(const Matrix4 &m, const float &s) {
        //GEOM_ASSERT(fabs(s) > GEOM_TOLERANCE);
        return Matrix4(m) /= s;
    }

    Matrix4 adjoint(const Matrix4 &m) {
        //GEOM_ASSERT(m.isValid());
        return Matrix4(GEOM_DET3(m.__M[5], m.__M[6], m.__M[7],
                                 m.__M[9], m.__M[10], m.__M[11],
                                 m.__M[13], m.__M[14], m.__M[15]),
                       -GEOM_DET3(m.__M[1], m.__M[2], m.__M[3],
                                  m.__M[9], m.__M[10], m.__M[11],
                                  m.__M[13], m.__M[14], m.__M[15]),
                       GEOM_DET3(m.__M[1], m.__M[2], m.__M[3],
                                 m.__M[5], m.__M[6], m.__M[7],
                                 m.__M[13], m.__M[14], m.__M[15]),
                       -GEOM_DET3(m.__M[1], m.__M[2], m.__M[3],
                                  m.__M[5], m.__M[6], m.__M[7],
                                  m.__M[9], m.__M[10], m.__M[11]),
                       GEOM_DET3(m.__M[4], m.__M[6], m.__M[7],
                                 m.__M[8], m.__M[10], m.__M[11],
                                 m.__M[12], m.__M[14], m.__M[15]),
                       -GEOM_DET3(m.__M[0], m.__M[2], m.__M[3],
                                  m.__M[8], m.__M[10], m.__M[11],
                                  m.__M[12], m.__M[14], m.__M[15]),
                       GEOM_DET3(m.__M[0], m.__M[2], m.__M[3],
                                 m.__M[4], m.__M[6], m.__M[7],
                                 m.__M[12], m.__M[14], m.__M[15]),
                       -GEOM_DET3(m.__M[0], m.__M[2], m.__M[3],
                                  m.__M[4], m.__M[6], m.__M[7],
                                  m.__M[8], m.__M[10], m.__M[11]),
                       GEOM_DET3(m.__M[4], m.__M[5], m.__M[7],
                                 m.__M[8], m.__M[9], m.__M[11],
                                 m.__M[12], m.__M[13], m.__M[15]),
                       -GEOM_DET3(m.__M[0], m.__M[1], m.__M[3],
                                  m.__M[8], m.__M[9], m.__M[11],
                                  m.__M[12], m.__M[13], m.__M[15]),
                       GEOM_DET3(m.__M[0], m.__M[1], m.__M[3],
                                 m.__M[4], m.__M[5], m.__M[7],
                                 m.__M[12], m.__M[13], m.__M[15]),
                       -GEOM_DET3(m.__M[0], m.__M[1], m.__M[3],
                                  m.__M[4], m.__M[5], m.__M[7],
                                  m.__M[8], m.__M[9], m.__M[11]),
                       GEOM_DET3(m.__M[4], m.__M[5], m.__M[6],
                                 m.__M[8], m.__M[9], m.__M[10],
                                 m.__M[12], m.__M[13], m.__M[14]),
                       -GEOM_DET3(m.__M[0], m.__M[1], m.__M[2],
                                  m.__M[8], m.__M[9], m.__M[10],
                                  m.__M[12], m.__M[13], m.__M[14]),
                       GEOM_DET3(m.__M[0], m.__M[1], m.__M[2],
                                 m.__M[4], m.__M[5], m.__M[6],
                                 m.__M[12], m.__M[13], m.__M[14]),
                       -GEOM_DET3(m.__M[0], m.__M[1], m.__M[2],
                                  m.__M[4], m.__M[5], m.__M[6],
                                  m.__M[8], m.__M[9], m.__M[10]));
    }

    float det(const Matrix4 &m) {
        //GEOM_ASSERT(m.isValid());
        return (m.__M[0] * GEOM_DET3(m.__M[5], m.__M[6], m.__M[7],
                                     m.__M[9], m.__M[10], m.__M[11],
                                     m.__M[13], m.__M[14], m.__M[15]) -
                m.__M[1] * GEOM_DET3(m.__M[4], m.__M[6], m.__M[7],
                                     m.__M[8], m.__M[10], m.__M[11],
                                     m.__M[12], m.__M[14], m.__M[15]) +
                m.__M[2] * GEOM_DET3(m.__M[4], m.__M[5], m.__M[7],
                                     m.__M[8], m.__M[9], m.__M[11],
                                     m.__M[12], m.__M[13], m.__M[15]) -
                m.__M[3] * GEOM_DET3(m.__M[4], m.__M[5], m.__M[6],
                                     m.__M[8], m.__M[9], m.__M[10],
                                     m.__M[12], m.__M[13], m.__M[14]));
    }

    Matrix4 inverse(const Matrix4 &m) {
        //GEOM_ASSERT(m.isValid());
        Matrix4 _adjoint = adjoint(m);
        float _det = (m.__M[0] * _adjoint.__M[0] +
                      m.__M[1] * _adjoint.__M[4] +
                      m.__M[2] * _adjoint.__M[8] +
                      m.__M[3] * _adjoint.__M[12]);
        return _adjoint / _det;
    }

    float trace(const Matrix4 &m) {
        //GEOM_ASSERT(m.isValid());
        return m.__M[0] + m.__M[5] + m.__M[10] + m.__M[15];
    }

    Matrix4 transpose(const Matrix4 &m) {
        //GEOM_ASSERT(m.isValid());
        return Matrix4(m.__M[0], m.__M[4], m.__M[8], m.__M[12],
                       m.__M[1], m.__M[5], m.__M[9], m.__M[13],
                       m.__M[2], m.__M[6], m.__M[10], m.__M[14],
                       m.__M[3], m.__M[7], m.__M[11], m.__M[15]);
    }

    ostream &operator<<(ostream &stream, const Matrix4 &m) {
        stream << "[[" << m.__M[0] << "," << m.__M[1] << "," << m.__M[2] << "," << m.__M[3] << "]," << endl;
        stream << "[" << m.__M[4] << "," << m.__M[5] << "," << m.__M[6] << "," << m.__M[7] << "]," << endl;
        stream << "[" << m.__M[8] << "," << m.__M[9] << "," << m.__M[10] << "," << m.__M[11] << "]," << endl;
        return stream << "[" << m.__M[12] << "," << m.__M[13] << "," << m.__M[14] << "," << m.__M[15] << "]]";
    }

    /*  --------------------------------------------------------------------- */





    /*  --------------------------------------------------------------------- */

    Matrix4 Matrix4::scaling(const Vector3 &s) {

        return Matrix4(s.x(), 0, 0, 0,
                       0, s.y(), 0, 0,
                       0, 0, s.z(), 0,
                       0, 0, 0, 1);
    }

    /*inline*/ Matrix4 Matrix4::axisRotation(const Vector3 &axis, const float &angle) {
        register float _c = cos(angle);
        register float _s = sin(angle);
        register float _t = 1 - _c;
        register float x = axis.x();
        register float y = axis.y();
        register float z = axis.z();

        float _sx = _s * x;
        float _sy = _s * y;
        float _sz = _s * z;
        float _txx = _t * x * x;
        float _txy = _t * x * y;
        float _txz = _t * x * z;
        float _tyy = _t * y * y;
        float _tyz = _t * y * z;
        float _tzz = _t * z * z;

        return Matrix4(_txx + _c, _txy - _sz, _txz + _sy, 0,
                       _txy + _sz, _tyy + _c, _tyz - _sx, 0,
                       _txz - _sy, _tyz + _sx, _tzz + _c, 0,
                       0, 0, 0, 1);
    }


    /*inline*/ Matrix4 Matrix4::eulerRotationZYX(const Vector3 &angle) {

        float _CZ = cos(angle.z());
        float _SZ = sin(angle.z());
        float _CY = cos(angle.y());
        float _SY = sin(angle.y());
        float _CX = cos(angle.x());
        float _SX = sin(angle.x());
        float _CXSY = _CX * _SY;
        float _SXSY = _SX * _SY;

        return Matrix4(_CX * _CY, _CXSY * _SZ - _SX * _CZ, _CXSY * _CZ + _SX * _SZ, 0,
                       _SX * _CY, _CX * _CZ + _SXSY * _SZ, _SXSY * _CZ - _CX * _SZ, 0,
                       -_SY, _CY * _SZ, _CY * _CZ, 0,
                       0, 0, 0, 1);
    }

    Matrix4 Matrix4::eulerRotationXYZ(const Vector3 &angle) {
        //  GEOM_ASSERT(angle.isValid());
        float _CX = cos(angle.x());
        float _SX = sin(angle.x());
        float _CY = cos(angle.y());
        float _SY = sin(angle.y());
        float _CZ = cos(angle.z());
        float _SZ = sin(angle.z());
        float _CXSZ = _CX * _SZ;
        float _SXSY = _SX * _SY;

        return Matrix4(_CY * _CZ, -_CXSZ * _CY + _SXSY, _SX * _CY * _SZ + _CX * _SY, 0,
                       _SZ, _CZ * _CX, -_CZ * _SX, 0,
                       -_SY * _CZ, _CXSZ * _SY + _SX * _CY, -_SXSY * _SZ + _CX * _CY, 0,
                       0, 0, 0, 1);
    }


    Matrix4 Matrix4::translation(const Vector3 &v) {

        return Matrix4(1, 0, 0, v.x(),
                       0, 1, 0, v.y(),
                       0, 0, 1, v.z(),
                       0, 0, 0, 1);
    }

    float Matrix4::getAzimuth() {

        if (fabs(__M[2]) < 0.000001) {
            if (__M[2] < 0)
                return -(float) GEOM_HALF_PI;
            else
                return (float) GEOM_HALF_PI;
        }

        float tang = fabs(atan(__M[6] / __M[2]));

        if (__M[2] < 0)
            tang = (float) GEOM_PI - tang;

        if (__M[6] < 0)
            tang = (float) GEOM_TWO_PI - tang;


        return tang;
    }

    void Matrix4::serialize(Archive &ar) {
        if (ar.isWriting()) {
            ar << __M[0] << __M[1] << __M[2] << __M[3];
            ar << __M[4] << __M[5] << __M[6] << __M[7];
            ar << __M[8] << __M[9] << __M[10] << __M[11];
            ar << __M[12] << __M[13] << __M[14] << __M[15];
        } else {
            ar >> __M[0] >> __M[1] >> __M[2] >> __M[3];
            ar >> __M[4] >> __M[5] >> __M[6] >> __M[7];
            ar >> __M[8] >> __M[9] >> __M[10] >> __M[11];
            ar >> __M[12] >> __M[13] >> __M[14] >> __M[15];
        }

    }

    Vector4 Matrix4::getDirection(bool scaled) {
        Vector4 direction(getColumn(2));
        direction.normalize();
        if (scaled && length != NAN) {
            direction *= length;
        }
        return direction;
    }

    void Matrix4::transform(Vector3 &normal) {
        double x = __M[0] * normal.x() + __M[1] * normal.y() + __M[2] * normal.z();
        double y = __M[4] * normal.x() + __M[5] * normal.y() + __M[6] * normal.z();
        normal[2] = __M[8] * normal.x() + __M[9] * normal.y() + __M[10] * normal.z();
        normal[0] = static_cast<float>(x);
        normal[1] = static_cast<float>(y);
    }

    void Matrix4::setIdentity() {
        __M[0] = 1.0;
        __M[1] = 0.0;
        __M[2] = 0.0;
        __M[3] = 0.0;
        __M[4] = 0.0;
        __M[5] = 1.0;
        __M[6] = 0.0;
        __M[7] = 0.0;
        __M[8] = 0.0;
        __M[9] = 0.0;
        __M[10] = 1.0;
        __M[11] = 0.0;
        __M[12] = 0.0;
        __M[13] = 0.0;
        __M[14] = 0.0;
        __M[15] = 1.0;
    }

    double Matrix4::getLength() const {
        return length;
    }

    void Matrix4::setLength(double length) {
        Matrix4::length = length;
    }

    Vector4 Matrix4::getTopPosition() {
        Vector4 direction = getDirection(true);
        Vector4 topPosition (__M[3], __M[7], __M[11], __M[15]);
        topPosition += direction;

        return topPosition;
    }

    Vector4 Matrix4::getSecondaryDirection() {
        Vector4 direction (__M[0], __M[4], __M[8], __M[12]);
        direction.normalize();
        return direction;
    }




};//end namespace
/////////////////////////////////////////////////////////////////////////////
