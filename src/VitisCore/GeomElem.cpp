/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "GeomElem.h"
#include "UtilMath.h"
#include "Plant.h"
//#include "DebugNew.h"
#include "VitisCoreSignalInterface.h"

GeomElem::GeomElem(void):VitisObject()
{


	//	id=0;
	ptrBrc = NULL;
	posInGeomBranc=-1;
	posInBranc=-1;

	lng = 1;


}


GeomElem::~GeomElem(void)
{


}


void GeomElem::copy(GeomElem * g)
{

	//	id=g->id;

	posInGeomBranc=g->posInGeomBranc;
	posInBranc=g->posInBranc;

	lng=g->getLength();

	matTrans=g->getMatrix();



}


float GeomElem::getAzimuth()
{

	float tang=fabs(atan(matTrans(1,2)/matTrans(0,2)));

	if(matTrans(0,2)<0)
		tang=(float)GEOM_PI-tang;

	if(matTrans(1,2)<0)
		tang=(float)GEOM_TWO_PI-tang;


	return tang;

}

float GeomElem::getElevation()
{
	return angle(matTrans.getMainVector(),Vector3(0,0,1));

}

void GeomElem::setLength(double l, bool sig)
{
	if (   sig
	    && ptrBrc != NULL)
		l = getPtrBrc()->getBranc()->getPlant()->getSignalInterface().sigElemLength(this, l);

	lng=l;
}

void GeomElem::setMatrix(Matrix4 p)
{
	if(ptrBrc != NULL)
		p = getPtrBrc()->getBranc()->getPlant()->getSignalInterface().sigElemPosition(this, p);

	matTrans=p;
}

void GeomElem::setPtrBrc (GeomBranc * gb)
{
	ptrBrc=gb;

	if(ptrBrc != NULL)
		ptrBrc->getBranc()->getPlant()->getSignalInterface().sigGeomElemCone((GeomElemCone*)this);

}

