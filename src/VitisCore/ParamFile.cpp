/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <string.h>
#include <math.h>
#include "ParamFile.h"
#include "Exceptions.h"
#include <iostream>
#include <cmath>

using namespace Vitis;

namespace Vitis
{

	ParametricParameter::ParametricParameter(float d) : ParametricParameter()
	{
        set (0, d);
	}

	ParametricParameter::ParametricParameter()
	{
		nb_X=0;

		currentYIndex=0;

		currentNb_Y=0;

		interpolation_X=0;

		currentInterpolationY=0;

		xmin=0;

		xmax=0;

		ymin=0;

		ymax=0;

		currentX=1.E32;

		currentY=1.E32;

		currentValue=0;

		signal = 0;

	}

	ParametricParameter::~ParametricParameter()
	{

	}

	std::string ParametricParameter::toString ()
	{
		std::ostringstream str;

		str<<interpolation_X<<"\t"; /* first dimension interpolation functoin */
		str<<nb_X<<"\t"; /* number of control points in the first dimension */

		for (int i=0; i<nb_X; i++)
		{
			str<<X[i]<<"\t"; /* index into the first dimension */

			str<<interpolation_Y[i]<<"\t"<<nb_Y[i]<<"\t"; /* interpolation and nb ctrl points on 2nd dimension */

			for (int j=0; j<nb_Y[i]; j++)
			{
				str<<Y[i][j]<<"\t"<<Val[i][j]<<"\t";
			}
		}

		currentX = currentY = 1.E32;

		// return string
		return str.str();
	}

	void ParametricParameter::fromString (std::string st)
	{
		xmin = ymin = 1e32;
		xmax = ymax = -1e32;

		istringstream str(st);

		str>>interpolation_X; /* first dimension interpolation functoin */
		str>>nb_X; /* number of control points in the first dimension */

		nb_Y.resize(nb_X);
		interpolation_Y.resize(nb_X);
		Y.resize(nb_X);
		X.resize(nb_X);
		Val.resize(nb_X);

		for (int i=0; i<nb_X; i++)
		{
			str>>X[i];
			if (xmin > X[i])
				xmin = X[i];
			if (xmax < X[i])
				xmax = X[i];

			str>>interpolation_Y[i]>>nb_Y[i]; /* interpolation and nb ctrl points on 2nd dimension */
			Y[i].resize(nb_Y[i]);
			Val[i].resize(nb_Y[i]);

			for (int j=0; j<nb_Y[i]; j++)
			{
				str>>Y[i][j]>>Val[i][j];
				if (ymin > Y[i][j])
					ymin = Y[i][j];
				if (ymax < Y[i][j])
					ymax = Y[i][j];

			}
		}

		currentNb_Y = nb_Y[0];
		currentYIndex = 0;
		currentYTab = Y[0];
		currentValTab = Val[0];
		currentX = X[0];
		currentY = Y[0][0];
		currentValue = Val[0][0];
	}

	void ParametricParameter::read (ParamSet pfic, float conv, float shift, float subShift)
	{
		float val,val2,tmp;
		long  i, j;
		char lig[256];
		std::fstream *fic = pfic.getFic();


		Assert(fic->is_open());

		*fic>>interpolation_X; /* first dimension interpolation functoin */
		*fic>>nb_X; /* number of control points in the first dimension */

		nb_Y.resize(nb_X);
		interpolation_Y.resize(nb_X);
		Y.resize(nb_X);
		X.resize(nb_X);
		Val.resize(nb_X);

		for (i=0; i<nb_X; i++)
		{
			*fic>>val>>tmp; /* index into the first dimension */

			X[i] = (int)val-shift;

			*fic>>interpolation_Y[i]>>nb_Y[i]; /* interpolation and nb ctrl points on 2nd dimension */

			for (j=0; j<nb_Y[i]; j++)
			{
				*fic>>val>>val2;

				Val[i].push_back (val2*conv);

				Y[i].push_back ((int)val-subShift);
			}
		}

		currentX = currentY = 1.E32;

		fic->getline(lig,255);
	}

	void ParametricParameter::write (ParamSet pfic, float conv, float shift, float subShift)
	{
	}

	void ParametricParameter::set (int x, float val, bool insert)
	{
		set (1., (float)x, val, insert);
	}

	void ParametricParameter::set (float x, float val, bool insert)
	{
		int ind;

		for (ind=0; ind<currentNb_Y; ind++)
			if (currentYTab[ind] >= x)
				break;

		if (   ind < currentNb_Y
		    && currentYTab[ind] == x)
		{
			currentValTab[ind] = val;
			currentY = x;
			currentValue =  val;
		}
		else if (insert)
		{
			currentYTab.resize(currentNb_Y+1);
			currentValTab.resize(currentNb_Y+1);

			if (currentNb_Y > 0)
			{
				for (int i=currentNb_Y-1; i>=ind; i--)
				{
					currentYTab[i+1] = currentYTab[i];
					currentValTab[i+1] = currentValTab[i];
				}
			}
			currentYTab[ind] = x;
			currentValTab[ind] = val;

			currentNb_Y++;
			currentY = x;
			currentValue =  val;
		}
	}

	void ParametricParameter::set (int pos, int subPos, float val, bool insert)
	{
		set ((float)pos, (float)subPos, val, insert);
	}


	void ParametricParameter::set (float x, float y, float val, bool insert)
	{
		int ind;

		for (ind=0; ind<nb_X; ind++)
			if (X[ind] >= x)
				break;

		if (	!insert
			&& (   ind >= nb_X
			    || X[ind] != x))
			return;

		if (   insert
			&& (   ind >= nb_X
			    || X[ind] != x))
		{
			Val.resize(nb_X+1);
			nb_Y.resize(nb_X+1);
			interpolation_Y.resize(nb_X+1);
			Y.resize(nb_X+1);
			X.resize(nb_X+1);
			for (int i=nb_X-1; i>=ind; i--)
			{
				Val[i+1] = Val[i];
				nb_Y[i+1] = nb_Y[i];
				Y[i+1] = Y[i];
				Val[i+1] = Val[i];
			}
			X[ind] = x;
			interpolation_Y[ind] = 0;
			nb_Y[ind] = 1;
			Y[ind].resize(1);
			Y[ind][0] = y;
			Val[ind].resize(1);
			Val[ind][0] = val;
			nb_X ++;
		}

		currentYIndex = ind;
		currentYTab= Y[ind];
		currentValTab = Val[ind];
		currentNb_Y = nb_Y[ind];
		currentInterpolationY = 0;
		currentY = 1.E32;
		currentX = x;
		currentY = y;
		currentValue = val;

		set(y, val, insert);
	}


float SingleValueParameter::value (VitisObject *o, VitisObject *subscriber)
{
	if (signal)
        val = signal->sigGetParameter (val, this, o, subscriber);

    return val;
}

float ParametricParameter::value (VitisObject *o, VitisObject *subscriber)
{
	float val = currentValue;

	if (signal)
        val = signal->sigGetParameter (val, this, o, subscriber);

	return val;
}

/* get the value of a parameter inside a parametric 2d curve */
float ParametricParameter::value (int x, VitisObject *o, VitisObject *subscriber)
{
	return value ((float) x, o, subscriber);
}

float ParametricParameter::value (float x, VitisObject *o, VitisObject *subscriber)
{
	int		i;
	float         val;

	/* if we already seeked for that value last time directly return it */
	if (x == currentY)
	{
		val = currentValue;
		if (signal)
            val = signal->sigGetParameter(val, this, o, subscriber);
		return (val);
	}
	/* if x < smaller x axis value */
	else if (x <= currentYTab[0])
	{
		currentValue = currentValTab[0];
		currentYIndex = 0;
	}
	/* if x > greater x axis value */
	else if (x >= currentYTab[currentNb_Y-1])
	{
		currentValue = currentValTab[currentNb_Y-1];
		currentYIndex = currentNb_Y-1;
	}
	/* get the right value */
	else
	{
		for (i=1; i<currentNb_Y; i++)
			if (currentYTab[i] >= x)
				break;
		if (currentInterpolationY == 0)
		{
			if (currentYTab[i] == x)
			{
				currentValue = currentValTab[i];
				currentYIndex = i;
			}
			else
			{
				currentValue = currentValTab[i-1];
				currentYIndex = i-1;
			}
		}
		else
		{
			currentValue = currentValTab[i-1] + (currentValTab[i]-currentValTab[i-1]) * (x-currentYTab[i-1]) / (currentYTab[i]-currentYTab[i-1]);
			if (currentYTab[i] == x)
				currentYIndex = i;
			else
				currentYIndex = i-1;
		}
	}

	currentY = x;
	val = currentValue;

	if (signal)
        val = signal->sigGetParameter (val, this, o, subscriber);

	/* return the value */
	return val;
}



/* get the value of a parameter inside a parametric 3d curve */
float ParametricParameter::value (int x, /* x axis value */
		int y, /* y axis value */
		VitisObject *o,
		VitisObject *subscriber)
{
	return value ((float)x, (float)y, o, subscriber);
}

float ParametricParameter::value (float x, /* x axis value */
		float y, /* y axis value */
		VitisObject *o,
		VitisObject *subscriber)
{
	int		i;
	float 	val1, val2, val;


	if (   currentX == x
		&& currentY == y)
	{
		val = currentValue;
        if (signal)
            val = signal->sigGetParameter (val, this, o, subscriber);
		return (val);
	}
	else if (   currentX == x
			&& interpolation_X == 0)
	{
		currentValue = value (y);
		val = currentValue;
        if (signal)
            val = signal->sigGetParameter (val, this, o, subscriber);
		return (val);
	}
	else if (x <= X[0])
	{
		currentYIndex = 0;
		currentYTab= Y[0];
		currentValTab = Val[0];
		currentNb_Y = nb_Y[0];
		currentInterpolationY = interpolation_Y[0];
		currentY = 1.E32;
		currentValue = value (y);
	}
	else if (x >= X[nb_X-1])
	{
		currentYIndex = nb_X-1;
		currentYTab= Y[nb_X-1];
		currentValTab = Val[nb_X-1];

		currentNb_Y = nb_Y[nb_X-1];
		currentInterpolationY = interpolation_Y[nb_X-1];
		currentY = 1.E32;
		currentValue = value (y);
	}
	else
	{
		for (i=1; i<nb_X; i++)
			if (X[i] >= x)
				break;
		if (X[i] == x)

		{
			currentYIndex = i;
			currentYTab= Y[i];
			currentValTab = Val[i];
			currentNb_Y = nb_Y[i];
			currentInterpolationY = interpolation_Y[i];
			currentY = 1.E32;
			currentValue = value (y);
		}
		else if (interpolation_X == 0)
		{
			currentYIndex = i-1;
			currentYTab= Y[i-1];
			currentValTab = Val[i-1];
			currentNb_Y = nb_Y[i-1];
			currentInterpolationY = interpolation_Y[i-1];
			currentY = 1.E32;
			currentValue = value (y);
		}
		else
		{
			currentYIndex = i-1;
			currentYTab= Y[i-1];
			currentValTab = Val[i-1];
			currentNb_Y = nb_Y[i-1];
			currentInterpolationY = interpolation_Y[i-1];
			currentY = 1.E32;
			val1 = value (y);
			currentYIndex = i;
			currentYTab= Y[i];
			currentValTab = Val[i];
			currentNb_Y = nb_Y[i];
			currentInterpolationY = interpolation_Y[i];
			currentY = 1.E32;
			val2 = value (y);
			currentValue = val1 + (val2-val1) * (x-X[i-1]) / (X[i]-X[i-1]);
		}
	}

	currentX = x;
	currentY = y;

	val = currentValue;

	if (signal)
        val = signal->sigGetParameter (val, this, o, subscriber);

	return (val);
}

float ParametricParameter::integrate (float debY, float endY, VitisObject *o, VitisObject *subscriber)
{
	float s = 0;
	float prevVal = value (debY, o, subscriber);
	int i = currentYIndex;
	float prevY = debY;

	while (   i < currentNb_Y-1
		   && currentYTab[i+1] < endY)
	{
		s += prevVal * (currentYTab[i+1] - prevY);
		if (currentInterpolationY == 1)
		{
			s += fabs (prevVal - currentValTab[currentYIndex+1]) * (currentYTab[currentYIndex+1] - prevY) / 2.;
		}
		prevY = i+1;
		prevVal = currentYTab[i+1];
	}

	s += prevVal * (endY - prevY);
	if (currentInterpolationY == 1)
	{
		s += fabs (prevVal - value(endY, o, subscriber)) * (endY - prevY) / 2.;
	}

	return s;
}

float ParametricParameter::integrate (float x, float debY, float endY, VitisObject *o, VitisObject *subscriber)
{
	value (x, debY, o, subscriber);
	return integrate (debY, endY, o, subscriber);
}

double ParametricParameter::logIntegrate(double _begin, double _end, VitisObject *o, VitisObject *subscriber){

    int counter = 0;

    if(counter >= CurrentNb_Y()){
        return 0;
    }

    double s = 0;
    double xBegin, xEnd;
    double yBegin, yEnd;

    xBegin = currentYTab[counter++];

    if(_begin < xBegin){
        yBegin = value(static_cast<float>(xBegin), o, subscriber);
        if(_end <= xBegin){
            return (_end - _begin)/yBegin;
        }
        s = (xBegin - _begin) / yBegin;
        if(counter < CurrentNb_Y()){
            xEnd = currentYTab[counter++];
        }else {
            xEnd = xBegin;
        }
        yEnd = value(static_cast<float>(xEnd), o, subscriber);
    } else {
        // find the segment containing begin
        xEnd = xBegin;
        while( counter < CurrentNb_Y() && xEnd <= _begin){
            xBegin = xEnd;
            xEnd = currentYTab[counter++];
        }
        yBegin = value(static_cast<float>(xBegin), o, subscriber);
        yEnd = value(static_cast<float>(xEnd), o, subscriber);

        if(xEnd <= _begin){
            return (_end - _begin)/yEnd;
        }
        if(_end < xEnd){
            if( std::abs(yEnd - yBegin) > 0.000001 ){
                double a = (yEnd - yBegin)/(xEnd - xBegin);
                double b = yBegin - a * xBegin;
                s = (1.0 / a) * std::log( (a * _end + b) / (a * _begin + b) );
            }else{
                s = (_end - _begin) / yEnd;
            }
        }else{
            if (std::abs(yEnd - yBegin) > 0.000001){
					double a = ( yEnd - yBegin ) / ( xEnd - xBegin );
					double b = yBegin - a * xBegin;
					s = (1.0 / a) * std::log ( yEnd/(a * _begin + b) );
				}else{
					s = (xEnd - _begin)/yEnd;
				}
        }


    }

    xBegin = xEnd;
    yBegin = yEnd;

    while(counter < CurrentNb_Y() && xEnd < _end){

        xEnd = currentYTab[counter++];
        yEnd = value(static_cast<float>(xEnd), o, subscriber);

        if(xEnd < _end){
            if(std::abs( yEnd - yBegin ) > 0.000001){
                    s+= (xEnd - xBegin) / (yEnd - yBegin) * std::log( yEnd / yBegin );
            }else{
                    s+= (xEnd - xBegin) / yEnd;
            }
        }else {
            if( std::abs(yEnd - yBegin) > 0.000001 ){
                double a = (yEnd - yBegin) / (xEnd - xBegin);
                double b = yBegin - a * xBegin;
                s+= (1.0 / a) * std::log( (a * _end + b) / yBegin );
            }else{
                s+= (_end - xBegin) / yEnd;
            }
        }

        xBegin = xEnd;
        yBegin = yEnd;

    }

    if( xEnd < _end){
        s += (_end - xEnd) / yEnd;
    }

    return s;
}

void ParametricParameter::copy (Parameter *pp)
{
    ParametricParameter *p = (ParametricParameter*)pp;

    nb_X = p->nb_X;				/**< \brief number of Main described abciss*/
	X = p->X;					/**< \brief list of described main abciss */
	interpolation_X = p->interpolation_X;	/**< \brief interpolation rule between main abciss  */
	nb_Y = p->nb_Y;				/**< \brief number of secondary abciss for each main abciss  */
	Y = p->Y;					/**< \brief secondary abciss values */
	Val = p->Val;				/**< \brief parameter values */
	interpolation_Y = p->interpolation_Y;	/**< \brief interpolation rule between secondary abciss for eac main abciss values  */

	currentNb_Y = p->currentNb_Y;				/**< \brief current number of described abciss into the abciss table */
	currentYTab = p->currentYTab;				/**< \brief current abciss table   */
	currentValTab = p->currentValTab;				/**< \brief current parameter values table  */
	currentYIndex = p->currentYIndex;				/**< \brief index of current abciss  */
	currentInterpolationY = p->currentInterpolationY;		/**< \brief current interpolation rule between secondary abciss */

	xmin = p->xmin;							/**< \brief current abciss minimum value */
	xmax = p->xmax;							/**< \brief current abciss maximum value */
	ymin = p->ymin;							/**< \brief current parameter minimum value */
	ymax = p->ymax;							/**< \brief current parameter maximum value */

	currentX = p->currentX;						/**< \brief current main abciss value  */
	currentY = p->currentY;						/**< \brief current secondary abciss value */
	currentValue = p->currentValue;					/**< \brief current parameter value */
}

	ParamSet::ParamSet(const std::string& Name):CFile(Name)
	{
		open();

        signal = 0;
		if (!fic)
			return;

		if (this->size() == -1)
			return;

		signal = new ParameterSignal;

		read();

		close ();

	}

	ParamSet::~ParamSet(void)
	{

		close ();

		if (signal)
            delete signal;
	}


	void ParamSet::open()
	{
		fic = new fstream (m_Name.c_str(), ios::in);
	}

	void ParamSet::close()
	{
        if (fic)
        {
            if (fic->is_open())
                fic->close();
            delete fic;
            fic = NULL;
        }
	}

	void ParamSet::open(ios_base::openmode _Mode)
	{
		char str[256];
		strcpy (str, m_Name.c_str());
		fic= new fstream (m_Name.c_str(), _Mode);
	}

	int ParamSet::size()
	{

		// sauvegarder la position courante
		long pos = fic->tellg();
		// se placer en fin de fichier
		fic->seekg( 0 , std::ios_base::end );
		// r�cup�rer la nouvelle position = la taille du fichier
		long size = fic->tellg() ;
		// restaurer la position initiale du fichier
		fic->seekg( pos,  std::ios_base::beg ) ;

		return size ;

	}

	MapParameter ParamSet::read()
	{
		MapParameter m;

		open();

		while (readParameter());

		close();

		return m;
	}

	int ParamSet::readParameter()
	{
		std::string s, name, type;
		char str[2056];

		while (!fic->eof())
		{
			*fic >> s;
			if (   s.empty()
				|| s.at(0) == '#')
			{
				fic->getline(str, 2056);
				continue;
			}

			break;
		}
		if (fic->eof())
			return 0;

		name = s;
		*fic >> type;

		fic->getline(str, 2056);

		Parameter *param = ParamFactory::instance().createParameter(type);
		if (param == 0)
			return 0;

		param->fromString (std::string(str));
		param->setSignal (signal);
		param->setName (name);

		this->addParameter (name, param);

		return 1;
	}

	void ParamSet::write()
	{
		fic = 0;

		open(ios::trunc|ios::out);

		if (!fic)
			return;

		MapParameter::const_iterator p;
		for (p=mapParameter.begin(); p!=mapParameter.end(); p++)
		{
			*fic << p->first << '\t' << p->second->Type() << '\t' << p->second->toString() << endl;
		}

		close();
	}

	Parameter * ParamSet::getParameter(std::string key, int ind)
	{
		return getParameter (formatName (key, ind));
	}

	Parameter * ParamSet::getParameter(std::string key)
	{

		if (mapParameter.empty())
			return NULL;

		MapParameter::const_iterator p;

		p=mapParameter.find(key);

		if (p != mapParameter.end())
			return (Parameter *)p->second;
		else
			return NULL;
	}

	int ParamSet::addParameter(std::string key, Parameter *p)
	{
		if (!mapParameter.empty())
		{
			MapParameter::const_iterator p=mapParameter.find(key);

			if (p != mapParameter.end())
				return 1;
		}

		p->setSignal(signal);
		mapParameter.insert(MapParameterPair(key, p));

		return 0;
	}

	int ParamSet::removeParameter(std::string key)
	{
		if (!mapParameter.empty())
			mapParameter.erase(key);

		return 0;
	}

	Parameter * ParamFactory::createParameter (std::string type)
	{
		Parameter *p=0;
		if (type == "SingleValueParameter")
			p = new SingleValueParameter;
		else if (type == "ParametricParameter")
			p = new ParametricParameter;
        else if (type == "StringParameter")
            p = new StringParameter;
		return p;
	}

	ParamFactory& ParamFactory::instance()
	{
		static ParamFactory p_;
		return p_;
	}

	const std::string ParamSet::formatName(std::string key, int ind)
	{
		std::ostringstream oss;
		oss<<key<<ind;
		return oss.str();
	}

	bool ParamSet::checkParameter (std::string str)
	{
		bool ret = true;
		if (!getParameter(str))
		{
			ret = false;
			cerr << "error loading parameter : "<<str<<"\n";
		}
		else
			getParameter(str)->setSignal(signal);

		return ret;
	}




}; //end namespace
