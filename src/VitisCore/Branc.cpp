/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

// Branc.cpp: implementation of the Branc class.
//
//////////////////////////////////////////////////////////////////////

#include "randomFct.h"
#include "Topology.h"
#include "Branc.h"
#include "defVitis.h"
#include <iostream>
#include <math.h>
#include "DebugNew.h"

//using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Branc::Branc(Plant * plt):DecompAxeLevel(plt)
{
	v_idBranc = plt->getTopology().nbBrancBorne;

	v_plt=plt;

//	hierarcPtrsNumber=0;

	totalAxeNumberWithinPlant = 0;

}


//Constructeur par copie
Branc::Branc(Branc * b):DecompAxeLevel()
{

	currentHierarcPtr=b->currentHierarcPtr;

//	hierarcPtrsNumber=b->hierarcPtrsNumber;

	hierarcPtrs = b->hierarcPtrs;

	totalAxeNumberWithinPlant=b->totalAxeNumberWithinPlant;

	v_plt=b->v_plt;

}



Branc::~Branc()
{

	getPlant()->getTopology().removeInstanceBrancSimplified(this, critere);

}

void Branc::setCritere(const std::string &c)
{
    critere.assign(c);
}

void Branc::updateTopo (Hierarc * hierarcPtr)
{

	hierarcPtrs.push_back(hierarcPtr);

//	hierarcPtrsNumber++;

	if(hierarcPtrs.size()>1)
		hierarcPtr->simplified=1;
	else
		currentHierarcPtr=hierarcPtr;

	Hierarc * p=hierarcPtr->getBearer();

	if (p == currentHierarcPtr)
		this->totalAxeNumberWithinPlant = 1;
	else
		this->totalAxeNumberWithinPlant += p->getBranc()->totalAxeNumberWithinPlant;


}




void Branc::serialize(Archive& ar )
{

	if( ar.isWriting() )
	{
		ar <<v_idBranc<< totalAxeNumberWithinPlant/*<< hierarcPtrsNumber*/;


	}
	else
	{
		ar >>v_idBranc>> totalAxeNumberWithinPlant/*>> hierarcPtrsNumber*/;
		hierarcPtrs.resize(1);
	}

	DecompAxeLevel::serialize(ar);

}

const std::string &Branc::getClassType() const {
	return classType;
}

void Branc::setClassType(const std::string &classType) {
	Branc::classType = classType;
}

