/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "FileArchive.h"
#include "Plant.h"
using namespace Vitis;


FileArchive :: FileArchive() : Archive()
{
  file = NULL;
  setVersionInfo( "", 0, 0 );
  _filename = "";
}

FileArchive :: FileArchive( const char* filename, const char* mode ) : Archive()
{
  file = NULL;
  setVersionInfo( "", 0, 0 );
  _filename = "";
  open( filename, mode );
}



FileArchive :: FileArchive( const char* filename, const char* mode, const char* fileType, unsigned int majorVersion, unsigned int minorVersion ) : Archive()
{
  file = NULL;
  setVersionInfo( fileType, majorVersion, minorVersion );
  _filename = "";
  open( filename, mode );
}

FileArchive :: ~FileArchive()
{
  close();
}

bool FileArchive :: testVersionInfo( const char* fileType, unsigned int majorVersion,
				     unsigned int minorVersion )
{
  return( ( string(fileType) == _fileType ) &&
	  ( majorVersion == _majorVersion ) &&
	  ( minorVersion == _minorVersion ) );
}

bool FileArchive :: open( const char* filename, const char* mode )
{
  _filename = "";

  if( mode == NULL )
    {
      //ErrLog::error( 0, "FileArchive::open", "while opening '%s': mode is NULL.\n", filename );
      return false;
    }

  if( mode[0] == 'w' )
    mode = "wb";
  else if( mode[0] == 'r' )
    mode = "rb";
  else if( mode[0] == 'a' )
    mode = "ab";
  else
    {
      //ErrLog::error( 0, "FileArchive::open", "while opening '%s': mode is '%c' (should be 'w', 'wb', 'r' or 'rb').\n", filename, mode[0] );
      return false;
    }

#ifdef WIN32
  fopen_s( &file, filename, mode );
#else
  file = fopen(filename, mode );
#endif
  if( !file )
    {
      //ErrLog::error( 0, "FileArchive::open", "unable to open '%s' for mode '%s'.\n", filename, mode );
      return false;
    }

  if( mode[0] == 'r' )
    {
      // read the file type and version info
      enableReadMode();
      *this >> _fileType >> _majorVersion >> _minorVersion;
    }
  else
    {
      // write the file type and version info
      enableWriteMode();
      *this << _fileType << _majorVersion << _minorVersion;
    }

  _filename = filename;

  return true;
}

void FileArchive :: close()
{
  _filename = "";
  if( file )
    fclose( file );
  file = NULL;
}

bool FileArchive :: onWrite( const void* p, int size )
{
  if( file )
    return( fwrite( p, size, 1, file ) == 1 );
  else
    return false;
}

bool FileArchive :: onRead( void* p, int size )
{
  if( file )
    return( fread( p, size, 1, file ) == 1 );
  else
    return false;
}
