/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "Plant.h"
#include "GeomElemCone.h"
//#include "DebugNew.h"
#include "VitisCoreSignalInterface.h"

#include <math.h>

GeomElemCone::GeomElemCone(void)
{
	topDiam=1; // 	upper diameter correction for current node

	bottomDiam=1;

}

GeomElemCone::~GeomElemCone(void)
{
}



void GeomElemCone::copy(GeomElem * g)
{
	GeomElem::copy(g);
	GeomElemCone * gc=(GeomElemCone *)g;
	topDiam=gc->getTopDiam();
	bottomDiam=gc->getBottomDiam();
	numsymb=gc->getSymbole();



}

float GeomElemCone::setTopDiam(float v, bool sig)
{
	if (sig)
		v = (float)getPtrBrc()->getBranc()->getPlant()->getSignalInterface().sigElemDiameter(this, v);

	topDiam=v;

	return v;
}
