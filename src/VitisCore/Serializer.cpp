/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "Serializer.h"
#include "VitisConfig.h"
#include "scheduler.h"
#include "FileArchive.h"
#include "Plant.h"

#include <string>

Serializer::Serializer(Plant * plt)
{
	v_plt=plt;
}

Serializer::~Serializer(void)
{
}

void Serializer::setDate (double date)
{
	Scheduler::instance()->create_process(this, NULL, date, 0);
}

void Serializer::process_event(EventData * )
{


	char * fileHot= new char [100];

#ifdef WIN32
	sprintf_s(fileHot,100,"%s%f.serial",v_plt->getConfigData().serialFileName.c_str(),Scheduler::instance()->getTopClock());
#else
	sprintf(fileHot,"%s%f.serial",v_plt->getConfigData().serialFileName.c_str(),Scheduler::instance()->getTopClock());
#endif

	FileArchive * ar=new FileArchive( fileHot, "wb" );

	delete [] fileHot;

	SArchiveMaps * sar=new SArchiveMaps (*ar);

	v_plt->SavePtr(*ar,(ArchivableObject *)v_plt);

	*ar<<*v_plt;

	/*if(Scheduler::instance().getTopClock()+1<=Scheduler::instance().getHotStop())
	  Scheduler::instance().self_signal_event(NULL, (double)Scheduler::instance().getTopClock()+1, 0);	*/

	delete ar;
	delete sar;


}
