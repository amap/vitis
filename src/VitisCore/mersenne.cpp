/************************** MERSENNE.CPP ******************** AgF 2001-10-18 *
 *  Random Number generator 'Mersenne Twister'                                *
 *                                                                            *
 *  This random number generator is described in the article by               *
 *  M. Matsumoto & T. Nishimura, in:                                          *
 *  ACM Transactions on Modeling and Computer Simulation,                     *
 *  vol. 8, no. 1, 1998, pp. 3-30.                                            *
 *  Details on the initialization scheme can be found at                      *
 *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html                  *
 *                                                                            *
 *  Experts consider this an excellent random number generator.               *
 *                                                                            *
 *  � 2001 - 2004 A. Fog.                                                     *
 *  GNU General Public License www.gnu.org/copyleft/gpl.html                  *
 *****************************************************************************/
#include <math.h>
#include <assert.h>
#include <stdio.h>
#include "randomMers.h"
#include "UtilMath.h"


namespace Vitis
{

	void TRandomMersenne::RandomInit(uint32 seed)
	{
        theSeed = seed;
		RandomInit (seed, 0, NBCOMPTRAND);
	}
	void TRandomMersenne::RandomInit(uint32 seed, int cptFrom, int cptTo)
	{
		// re-seed generator
		int i;


		for(i=cptFrom; i<cptTo; i++)
		{
            mt[i][0]= seed+i;
			for (mti=1; mti < MERS_N; mti++) {
				mt[i][mti] = (1812433253UL * (mt[i][mti-1] ^ (mt[i][mti-1] >> 30)) + mti);}
			saveIdx[i]=0;
			toBeReinitialized[i] = false;
		}
		currentIdx=0;

		// detect computer architecture
		union {double f; uint32 i[2];} convert;
		convert.f = 1.0;
		// Note: Old versions of the Gnu g++ compiler may make an error here,
		// compile with the option  -fenum-int-equiv  to fix the problem
		if (convert.i[1] == 0x3FF00000) Architecture = LITTLEENDIANV;
		else if (convert.i[0] == 0x3FF00000) Architecture = BIGENDIANV;
		else Architecture = NONIEEEV;
	}



	uint32 TRandomMersenne::BRandom()
	{
		uint32 y;

		if (mti >= MERS_N)
		{
			// generate MERS_N words at one time
			const uint32 LOWER_MASK = (1LU << MERS_R) - 1; // lower MERS_R bits
			const uint32 UPPER_MASK = -1L  << MERS_R;      // upper (32 - MERS_R) bits
			static const uint32 mag01[2] = {0, MERS_A};

			int kk;
			for (kk=0; kk < MERS_N-MERS_M; kk++)
			{
				y = (mt[currentIdx][kk] & UPPER_MASK) | (mt[currentIdx][kk+1] & LOWER_MASK);
				mt[currentIdx][kk] = mt[currentIdx][kk+MERS_M] ^ (y >> 1) ^ mag01[y & 1];
			}

			for (; kk < MERS_N-1; kk++)
			{
				y = (mt[currentIdx][kk] & UPPER_MASK) | (mt[currentIdx][kk+1] & LOWER_MASK);
				mt[currentIdx][kk] = mt[currentIdx][kk+(MERS_M-MERS_N)] ^ (y >> 1) ^ mag01[y & 1];
			}

			y = (mt[currentIdx][MERS_N-1] & UPPER_MASK) | (mt[currentIdx][0] & LOWER_MASK);
			mt[currentIdx][MERS_N-1] = mt[currentIdx][MERS_M-1] ^ (y >> 1) ^ mag01[y & 1];
			mti = 0;
			toBeReinitialized[currentIdx] = true;
		}

		y = mt[currentIdx][mti++];

		// Tempering (May be omitted):
		y ^=  y >> MERS_U;
		y ^= (y << MERS_S) & MERS_B;
		y ^= (y << MERS_T) & MERS_C;
		y ^=  y >> MERS_L;
		return y;
	}


	double TRandomMersenne::Random()
	{
		// output random float number in the interval 0 <= x < 1
		union {double f; uint32 i[2];} convert;
		uint32 r = BRandom();
		// get 32 random bits
		// The fastest way to convert random bits to floating point is as follows:
		// Set the binary exponent of a floating point number to 1+bias and set
		// the mantissa to random bits. This will give a random number in the
		// interval [1,2). Then subtract 1.0 to get a random number in the interval
		// [0,1). This procedure requires that we know how floating point numbers
		// are stored. The storing method is tested in function RandomInit and saved
		// in the variable Architecture. The following switch statement can be
		// omitted if the architecture is known. (A PC running Windows or Linux uses
		// LITTLEENDIAN architecture):

		//return 0.0;
		switch (Architecture)
		{
			case LITTLEENDIANV:
				convert.i[0] =  r << 20;
				convert.i[1] = (r >> 12) | 0x3FF00000;
				return convert.f - 1.0;
			case BIGENDIANV:
				convert.i[1] =  r << 20;
				convert.i[0] = (r >> 12) | 0x3FF00000;
				return convert.f - 1.0;
			case NONIEEEV: default:
				;}
				// This somewhat slower method works for all architectures, including
				// non-IEEE floating point representation:
				return (double)r * (1./((double)(uint32)(-1L)+1.));

	}


	int TRandomMersenne::IRandom(int min, int max)
	{
		// output random integer in the interval min <= x <= max
		int r;
		r = int((max - min + 1) * Random()) + min; // multiply interval with random and truncate
		if (r > max) r = max;
		if (max < min) return 0x80000000;
		return r;
	}



	void TRandomMersenne::initState (int idx)
	{
		saveIdx[idx]=0;
		if (idx == currentIdx)
			mti=0;
        if (toBeReinitialized[idx])
        {
            mt[idx][0]= theSeed+idx;
			for (int j=1; j < MERS_N; j++) {
				mt[idx][j] = (1812433253UL * (mt[idx][j-1] ^ (mt[idx][j-1] >> 30)) + j);}
			saveIdx[idx]=0;
			toBeReinitialized[idx] = false;
        }
	}

	int TRandomMersenne::setState (int idx)
	{
		int savIdx = currentIdx;

		saveIdx[currentIdx]=mti;
		mti=saveIdx[idx];
		currentIdx=idx;

		return savIdx;
	}

	int TRandomMersenne::getCounter ()
	{
		return mti;
	}

	void TRandomMersenne::setCounter (int c)
	{
		mti = c;
	}

	double TRandomMersenne::normalNoise (double stdDev)
	{
		double r1 = Random();
		if (r1 == 0)
			r1 = 0.00000000000001;
		double r2 = Random();
		double v = sqrt(-2. * log(r1)) * cos(2.*M_PI*r2);
		return (stdDev * v);
	}

	double TRandomMersenne::nextGaussian ()
	{
        double v1, v2, s;
        do {
            v1 = 2 * Random() - 1; // between -1 and 1
            v2 = 2 * Random() - 1; // between -1 and 1
            s = v1 * v1 + v2 * v2;
        } while (s >= 1 || s == 0);
        double multiplier = sqrt(-2 * log(s)/s);
        return v1 * multiplier;
	}

	void TRandomMersenne::serialize(Archive& ar )
	{


		if( ar.isWriting() )
		{

			ar <<mti<<currentIdx;

			for(int i=0; i<NBCOMPTRAND; i++)
				ar<<saveIdx[i];


		}
		else
		{
			ar >>mti>>currentIdx;

			for(int i=0; i<NBCOMPTRAND; i++)
				ar>>saveIdx[i];

		}

	}

};
