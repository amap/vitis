/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

// Tree.cpp: implementation of the Tree class.
//
//////////////////////////////////////////////////////////////////////
#include "Plant.h"
#include "Topology.h"
#include <algorithm>
#include <iostream>
#include <fstream>
#include "Branc.h"
#include "randomFct.h"
#include "DebugNew.h"
#include "VitisCoreSignalInterface.h"




//structure de graphe repr�sentant la topologie de la plante
Hierarc::Hierarc ()
{
	simplified=0;

	borneNumber = 0;

	brancPtr=NULL;

	indexIntoVerticille = 0;

	verticilleBranchNumber = 0; /* taille du verticille */


	bearerPtr=NULL;

	indexOnBearerAtLowestLevel = 0;

	ordre = 0;

	getBranc()->getPlant()->getSignalInterface().sigHierarc(this);

    getBranc()->getPlant()->getGeometry().HasToBeComputed(true);
}


Hierarc::Hierarc (Hierarc * bPtr, Branc * b, char numVertic, USint nbrVertic)
{
	simplified=0;

	borneNumber = 0;

	brancPtr=b;

	indexIntoVerticille = numVertic;

	verticilleBranchNumber = nbrVertic; /* taille du verticille */


	if (bPtr != NULL)
	{
		bearerPtr=bPtr;

		int level = bPtr->getBranc()->getPlant()->getPlantFactory()->getLevelOfDecomposition(bPtr->getBranc()->getClassType());
		indexOnBearerAtLowestLevel = bPtr->getBranc()->getCurrentElementIndexOfSubLevel(level);

		ordre = bPtr->ordre + 1;

		bearerPtr->addBorne(this, indexOnBearerAtLowestLevel);

	}
	else
	{
		bearerPtr=NULL;

		indexOnBearerAtLowestLevel = 0;

		ordre = 0;

	}

	b->updateTopo(this);

	getBranc()->getPlant()->getSignalInterface().sigHierarc(this);

    getBranc()->getPlant()->getGeometry().HasToBeComputed(true);
};

Hierarc::~Hierarc()
{
    getBranc()->getPlant()->getGeometry().HasToBeComputed(true);
	while(bornePtr.size()!=0)
	{
		delete bornePtr.back();
		bornePtr.pop_back();

	}
}

void Hierarc::copyBorneFrom(Hierarc * hierarcPtr)
{
	borneNumber=hierarcPtr->borneNumber;
	bornePtr.resize(borneNumber);
	bornePosition.resize(borneNumber);

	for (int i=0; i<borneNumber; i++)
	{
		bornePtr[i]=hierarcPtr->bornePtr[i];
		bornePosition[i]=hierarcPtr->bornePosition[i];
	}
    getBranc()->getPlant()->getGeometry().HasToBeComputed(true);

}

void Hierarc::deleteBorne (Hierarc * hierarcPtr)
{
	long i;



	for (i=0; i<borneNumber; i++)
	{
		if (bornePtr[i] == hierarcPtr)
		{
			bornePtr.erase(bornePtr.begin()+i);
			bornePosition.erase(bornePosition.begin()+i);
			break;
		}
	}


	borneNumber --;

    getBranc()->getPlant()->getGeometry().HasToBeComputed(true);

}


void Hierarc::addBorne(Hierarc * h, long pos)
{

	bornePtr.push_back(h);
	bornePosition.push_back((unsigned short)pos);
	borneNumber++;

    getBranc()->getPlant()->getGeometry().HasToBeComputed(true);
}



void Hierarc::printTree(std::ofstream & treestr, std::string ecart)
{
	int i;

	sortPosHierarc();


	treestr<<ecart<<"Branch";

	treestr<<" "<<brancPtr->V_idBranc()<<" borne by "<<getBearer()->getBranc()->V_idBranc();

	treestr<<" NbSons : "<<borneNumber;

	if(simplified)
		treestr<<" NbSons simp: "<<getNbBorne();

	std::string ecartNext=ecart;
	for (i=0; i<getNbBorne(); i++)
	{
		ecartNext+=" ";
		treestr<<std::endl;
		getBorne(i)->CurrentHierarcPtr()->printTree(treestr, ecartNext);

	}
	treestr<<std::endl;
	treestr<<ecart<<" Fin branche "<<brancPtr->V_idBranc();


}



Hierarc * Hierarc::getBearer ()
{

	if (this->bearerPtr == NULL)
		return this;

	return bearerPtr;

}



Branc * Hierarc::getBorne (int next)
{

	Hierarc *   ind;

	Hierarc *   child;

	if (this->brancPtr == NULL)
		return NULL;

//    if (this->brancPtr->HierarcPtrsNumber() == 0)
//        return NULL;

	//Dans le cas o� il y a eu simplification topo , les fils sont indiqu� dans le premier �l�ment du HierarcPtrs()
	ind = *(this->brancPtr->HierarcPtrs().begin());;

	if (ind->getNbBorne() <= next)
		return NULL;


	child=ind->bornePtr[next];


	return child->getBranc();

}



Branc *Hierarc::getBranc ()
{
	Branc *b;


	b = this->brancPtr;

	b->CurrentHierarcPtr(this);

	return b;
}

/* sort borne branches indices according to position along the bearer */
void Hierarc::sortPosHierarc ()
{
	int i, j;
	Hierarc * hsav;
	int possav;

	for (i=0; i<this->borneNumber; i++)
	{
		for (j=0; j<i; j++)
		{
			if (this->bornePosition[j] > this->bornePosition[i])
			{
				possav = this->bornePosition[i];
				hsav =this->bornePtr[i];
				this->bornePosition[i] = this->bornePosition[j];
				this->bornePtr[i] = this->bornePtr[j];
				this->bornePosition[j] = possav;
				this->bornePtr[j] = hsav;
				i = j;
				break;
			}
		}
	}
}

void Hierarc::sortTree()
{
	sortPosHierarc();

	for (int i=0; i<borneNumber; i++)
	{
		bornePtr[i]->sortTree();
	}
}



short Hierarc::getNbBorne ()
{

	Hierarc * ind;

	if (this->brancPtr == NULL)
		return 0;

	ind = *(this->brancPtr->HierarcPtrs().begin());

	if(ind == 0){
        return 0;
	}

	return ind->bornePtr.size();
	return ind->borneNumber;
}

void Hierarc::serialize(Archive& ar )
{
	int i;

	if( ar.isWriting() )
	{

		ar <<indexOnBearerAtLowestLevel<< verticilleBranchNumber<< indexIntoVerticille;
		ar <<simplified<< ordre;

		//le pointeur a d�j� �t� sauver dans le topoManager
		this->SavePtr(ar,brancPtr);
		/*if()
		{	this->SavePtr(ar,(ArchivableObject *)brancPtr->getPlant());
		ar<<*brancPtr;
		}*/

		ar <<borneNumber;

		for(i=0; i<borneNumber; i++)
		{
			ar << *(bornePtr[i]);
			ar << bornePosition[i];
		}


	}
	else
	{
		ar >>indexOnBearerAtLowestLevel>> verticilleBranchNumber>> indexIntoVerticille;

		ar >>simplified>> ordre;

		ArchivableObject * arBr;
		this->LoadPtr(ar,arBr);
		brancPtr=(Branc *)arBr;

		/*ArchivableObject * arBr;
		if(this->LoadPtr(ar,arBr))
		{
		Plant * plt;

		this->LoadPtr(ar,(ArchivableObject *&)plt);

		brancPtr=plt->getPlantFactory()->createInstanceBranc();

		//on enregistre le pointeur ke lon vient de cr�er
		this->AddPtr(ar,brancPtr);

		ar >> *brancPtr;

		}else brancPtr=(Branc *)arBr;*/

		if(!this->simplified)
		{
			brancPtr->HierarcPtrs()[0]=this;
			brancPtr->CurrentHierarcPtr(this);
		}
		else
			brancPtr->HierarcPtrs().push_back(this);


		ar >> borneNumber;

		bornePtr.clear();
		bornePosition.clear();
		if(borneNumber)
		{
			bornePtr.resize(borneNumber);
			bornePosition.resize(borneNumber);
			for(i=0; i<borneNumber; i++)
			{	bornePtr[i]=new Hierarc();
			bornePtr[i]->bearerPtr=this;
			ar >> *(bornePtr[i]);
			ar >> bornePosition[i];
			}
		}


	}

}

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

TopoManager::TopoManager(Plant * plt)
{

	trunc=NULL;

	v_plt=plt;

	nbBranc=0;

	nbBrancBorne=0;

	currentId = 2;

}

TopoManager::~TopoManager()
{
	if(   nbBranc > 0
		&& trunc != NULL)
		removeBranc(trunc->getBranc(),2);

	delete simpTopo;

	simpTopo = 0;
}



void TopoManager::fork(Branc * b, Hierarc * bearerH, char numVertic, USint nbrVertic )
{

	Hierarc * hierarcPtr=new Hierarc (bearerH, b, numVertic, nbrVertic);

	if(bearerH==NULL)
		trunc=hierarcPtr;

	if(!hierarcPtr->simplified)
	{
		nbBranc++;
		nbBrancBorne++;
	}

	b->getPlant()->getSignalInterface().sigDecompAxeLevel(b);

}



void TopoManager::removeBranc(Branc * b , int total)
{

	int i;
	Hierarc * bearer;

	bearer=b->CurrentHierarcPtr()->bearerPtr;
	std::vector<Hierarc *> ptr = b->HierarcPtrs();
	Hierarc *savHier = b->CurrentHierarcPtr();

	if(total==2)
	{	while (savHier->getNbBorne() > 0)
	{
		removeBranc (savHier->getBorne(0), total);
	}
	}
	else
	{
		/* free everything that is borned */
		while (savHier->borneNumber > 0)
		{

			//Branc * tmp=b->CurrentHierarcPtr()->getBorne(0);
			if(v_plt->getConfigData().cfg_topoSimp != NO_SIMP)
				removeBranc (savHier->getBorne(0), 0);
			else
				removeBranc (savHier->getBorne(0), 1);


		}
	}


	/* every instance of that bud should be removed from hierarchy */
	if (total)
	{
		nbBranc--;

		b->TotalAxeNumberWithinPlant(0);

		for (i=b->HierarcPtrs().size()-1; i>=0; i--)
		{
			bearer=ptr[i]->bearerPtr;


			if(bearer != NULL)
				bearer->deleteBorne (ptr[i]);

			if(total!=2)
				ptr[i]->getBranc()->updatePruning();

			// JFB 07/2019 cagouille destruction et nombre de branches
//            savHier = ptr[i];
//			vector<Hierarc *>::iterator f=std::find(ptr.begin(), ptr.end(), savHier);
//			ptr.erase(f);
            delete (ptr[i]);
		}
		delete b;

	}	/* only that index should be removed from hierarchy *//* cad qu'il y a simplif topo*/
	else
	{
		/* JFB 1/11/02 take care about total number of axes */
		b->TotalAxeNumberWithinPlant(b->TotalAxeNumberWithinPlant() - b->CurrentHierarcPtr()->getBearer()->getBranc()->TotalAxeNumberWithinPlant());

		if(bearer != NULL)
			bearer->deleteBorne (b->CurrentHierarcPtr());


        vector<Hierarc *>::iterator f=std::find(ptr.begin(), ptr.end(), savHier);
        ptr.erase(f);
        delete savHier;

		if(total!=2)
			b->updatePruning();

		if(   ptr.size() > 1
		   && savHier == *(ptr.begin()))
        {
            ptr[1]->copyBorneFrom(savHier);
            ptr[1]->simplified=0;
 		}

        b->CurrentHierarcPtr(*(ptr.begin()));
        b->HierarcPtrs(ptr);
        nbBranc--;
//		b->HierarcPtrsNumber(b->HierarcPtrsNumber()-1);

	}

}


void TopoManager::emptyBranc(Branc * b)
{

	Hierarc *savHier = b->CurrentHierarcPtr();

	// remove any borne branch
	while (savHier->getNbBorne() > 0)
		removeBranc (savHier->getBorne(0), 2);

	// empty the decomposition of b
	while (DecompAxeLevel *elem = b->getCurrentElementOfSubLevel())
		b->removeSubElementAtLevel(elem);
}


Branc * TopoManager::seekTree(forestSeekMode mode)
{
	Hierarc * hNext;
	short i;


	switch (mode)
	{
	case RESET:
		{

			while(!pileSeek.empty())
			{
				pileSeek.pop();
				//					std::cout<<"Error seek"<<std::endl;
			}

			if(trunc==NULL) break;


//			for(i=0; i<trunc->borneNumber; i++)
			for(i=trunc->borneNumber-1; i>=0; i--)
			{
				if(!trunc->bornePtr[i]->simplified)
					pileSeek.push(trunc->bornePtr[i]);

			}
			return trunc->getBranc();
			break;
		}
	case NEXT:
		{

		/* get the next borne */
		if(pileSeek.empty()) {
			break;
		}


		hNext=pileSeek.front();



//		for(i=0; i<hNext->borneNumber; i++)
		for(i=hNext->borneNumber-1; i>=0; i--)
		{

			if(!hNext->bornePtr[i]->simplified)
				pileSeek.push(hNext->bornePtr[i]);

		}


		pileSeek.pop();
		return hNext->getBranc();
		break;

		}

	}
	return NULL;

}



void TopoManager::removeInstanceBrancSimplified (Branc *b, const std::string & c)
{
	if(v_plt->getConfigData().cfg_topoSimp==NO_SIMP)
		return ;
	simpTopo->removeSimplifiedBranc(b, c);
	return;
}

Branc * TopoManager::getInstanceBrancSimplified (const std::string & c)
{
	if(v_plt->getConfigData().cfg_topoSimp==NO_SIMP)
		return NULL;
	return simpTopo->getSimplifiedBranc(c);
}


void TopoManager::updateSimplifier (Branc * b, const std::string & c)
{
	if(v_plt->getConfigData().cfg_topoSimp!=NO_SIMP)
		simpTopo->updateListe(b, c);
}


long TopoManager::ComputeNbHierarc()
{
	nbHierarc = ComputeNbHierarc (trunc->brancPtr);

	return nbHierarc;
}

long TopoManager::ComputeNbHierarc(Branc *b)
{
	if (!b->getElementNumberOfSubLevel())
		return 0;

	long nb = 1;
	Hierarc *h = b->getCurrentHierarc();

	for (short i=0; i<h->borneNumber; i++)
	{
		if (!h->bornePtr[i]->simplified)
			nb += ComputeNbHierarc (h->bornePtr[i]->getBranc());
	}

	nb *= b->HierarcPtrs().size();

	return nb;
}

Hierarc* TopoManager::getTrunc (const std::string type)
{
    if (trunc->getBranc()->getClassType() == type)
        return trunc;

    for (int i=0; i<trunc->getNbBorne(); i++)
    {
        if (trunc->getBorne(i)->getClassType() == type)
            return trunc->bornePtr[i];
    }

    return NULL;
}


void TopoManager::serialize(Archive& ar )
{


	if( ar.isWriting() )
	{
		ar <<nbBranc<<nbBrancBorne;

		Branc * b = seekTree (TopoManager::RESET);

		while (b!= NULL)
		{
			this->SavePtr(ar,b);
			ar <<*b ;
			b =seekTree (TopoManager::NEXT);
		}

		ar<< *trunc; // <<*trunc enregistre toute la structure topo




	}
	else
	{
		ar >>nbBranc>>nbBrancBorne;

		//Ici on charge toutes les branches
		Branc * b;
		for(int i=0; i<nbBranc; i++)
		{
			ArchivableObject * arBr;
			this->LoadPtr(ar,arBr);

			b=v_plt->getPlantFactory()->createInstanceBranc();
			this->AddPtr(ar,b);
			ar >>*b;

		}

		trunc =new Hierarc();
		ar>> *trunc;


	}

}
