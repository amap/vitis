/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "scheduler.h"
#include "Plant.h"
#include <math.h>
#include <vector>
#include "std_serialize.h"
#include "DebugNew.h"
#include "Exceptions.h"
#include <exception>
#include <iomanip>

//#include <omp.h>

using namespace std;

Scheduler::Scheduler(Plant * plt)
{
	top_clock=0;

	current_priority=0;

	current_process=-1;

	v_plt=plt;

	hotStop=0;

	next_top_clock=0;


}

Scheduler::~Scheduler()
{
	clear();

}


void Scheduler::stop_process() throw()
{

	agenda.insert(Action(top_clock,current_priority,A_Stop,current_process,NULL));

}

void Scheduler::delete_process() throw()
{

	agenda.insert(Action(top_clock,current_priority,A_Delete,current_process,NULL));

}

void Scheduler::delete_process(ProcessId pid) throw()
{

	agenda.insert(Action(top_clock,current_priority,A_Delete,pid,NULL));

}

int Scheduler::stop_process(ProcessId pid) throw()
{
	if (processes[pid].terminated)
		return -1;
	agenda.insert(Action(top_clock,current_priority,A_Stop,pid,NULL));
	return 0;
}

int Scheduler::create_process(VProcess * pro, EventData * e, Time d, Priority p )
{
	processes.push_back(PDescr(pro));

	ProcessId newpid = (ProcessId)(processes.size() - 1);

	pro->process_id=newpid;

	agenda.insert(Action(d,p,A_Event,newpid,e));

	return pro->process_id;
}

void Scheduler::signal_event(ProcessId  pid, EventData * e, Time d, Priority p) throw()
{
	if(top_clock<=d)
		agenda.insert(Action(d,p,A_Event,pid,e));
}

void Scheduler::self_signal_event(EventData * e, Time d, Priority p) throw()
{
	if(top_clock<=d)
		agenda.insert(Action(d,p,A_Event,current_process,e));
}

void Scheduler::displayAgenda()
{
	for (A_table_t::iterator i=agenda.begin(); i!=agenda.end(); i++)
	{
		cout << "pid "<<i->pid<<"  time "<<i->time<<"  priority "<<i->priority<<endl;
	}
	cout<<endl;
}
int Scheduler::getActionIndex (ProcessId pid, Time d, Priority p, EventData * e)
{
	for (unsigned int i=0; i<agenda.size(); i++)
	{
		if (agenda.at(i).pid == pid)
		{
			if (d == -1)
				return (i);

			if (agenda.at(i).time == d)
			{
				if (agenda.at(i).priority == -1)
					return (i);

				if (agenda.at(i).priority == p)
				{
				if (   agenda.at(i).eventData == NULL
				    || agenda.at(i).eventData == e)
					return (i);
				}
			}

		}
	}
	return -1;
}


Action & Scheduler::getAction (ProcessId pid, Time d, Priority p, EventData * e)
{

	int i = getActionIndex (pid, d, p, e);

	if (i >= 0)
		return (agenda.at(i));
	else
	{
		throw Vitis::CException("no such action");
	}
}

void Scheduler::changeAction (int actionId, ProcessId pid, Time d, Priority p, EventData * e)
{
	agenda.erase (actionId);
	agenda.insert (Action (d, p, A_Event, pid, e));
}

void Scheduler::run_simulation()
{
	static float prev_clock = 0;

//#pragma omp parallel
	while (!agenda.empty()) {

		//next_top_clock = agenda.begin()->time; new event heap management
		next_top_clock = (agenda.end()-1)->time;

		//si pause il ne faut pas d�piler l'�v�nement
		if (next_top_clock>hotStop+0.000001)
			break;

		Action action = agenda.pop_first();

		top_clock=action.time;

		if (!agenda.empty())
			//next_top_clock = agenda.begin()->time;  new event heap management
			next_top_clock = (agenda.end()-1)->time;
		else
			next_top_clock = 0;

		if (top_clock > prev_clock + .1)
		{
			prev_clock = (float)((int)(top_clock *10.)) / 10.;
			//cout << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b" << setiosflags( ios::left ) << setw(4) << "age " << setw(16) << prev_clock;
			v_plt->getSignalInterface().sigMonitorTopology (prev_clock);
		}

		current_process = action.pid;

		current_priority = action.priority;

		PDescr & pd = processes[current_process];

		// right now I don't check if current_process is indeed a
		// valid process.  Keep in mind that this is the heart of the
		// simulator main loop, therefore efficiency is crucial.
		// Perhaps I should check.  This is somehow a design choice.
		//
		if (!pd.terminated && pd.process)
		{
			pd.available_at=top_clock;

			switch (action.type) {
				case A_Event:
					pd.process->process_event(action.eventData);
					break;
				case A_Init:
					pd.process->init();
					break;
				case A_Stop:
					pd.terminated=true;
					break;
				case A_Delete:
					clear(pd);
					break;
				default: break;
			}

		}
//cout << omp_get_num_threads()<<endl;
	}//end while

	prev_clock = 0;
}

void Scheduler::clear(PDescr pd){

	// find this element
	PsTable::iterator it;
	int processId;

	for (processId=0,it=processes.begin(); it!=processes.end(); it++,processId++)
	{
		if (   it->process == pd.process
			&& it->available_at == pd.available_at
			&& it->terminated == pd.terminated)
			break;
	}

	if (it != processes.end())
	{
		it->terminated = true;
		//// remove process from the processes vector
		//processes.erase(it);
		//delete pd.process;

		//// remove process from agenda
		//bool found;
		//do
		//{
		//	found = false;
		//	int i;
		//	A_table_t::iterator a;
		//	for(a=agenda.begin(),i=0; a!=agenda.end(); ++a,i++)
		//	{
		//		if (a->pid == processId)
		//		{
		//			agenda.erase(i);
		//			found = true;
		//			break;
		//		}
		//	}
		//}while (found);

		//// update agenda process id
		//for(A_table_t::iterator a = agenda.begin(); a != agenda.end(); ++a)
		//{
		//	if (a->pid > processId)
		//		a->pid --;
		//}
	}
}

bool Scheduler::clear(VProcess *v){

	// find this element
	PsTable::iterator it;
	bool found = false;

	for (it=processes.begin(); it!=processes.end(); it++)
	{
		if (it->process == v) {
            it->terminated = true;
            found = true;
        }
	}
    return found;
}

void Scheduler::clear() throw() {

	top_clock = 0;
	current_process = 0;

	processes.clear();

	for(A_table_t::iterator a = agenda.begin(); a != agenda.end(); ++a) {
		const EventData * e = (*a).eventData;
		if (e != NULL )
			delete(e);
	}
	agenda.clear();
}

double Scheduler::getTopClock()
{
	return top_clock;
}

double Scheduler::getNextTopClock()
{
	return next_top_clock;
}

double Scheduler::getHotStop()
{
	return hotStop;
}


int Scheduler::getTopPriority()
{
	return current_priority;
}


void Scheduler::setHotStop(double hstp)
{
	this->hotStop=hstp;
}

void Scheduler::serialize(Archive& ar )
{


	if( ar.isWriting() )
	{
		ar <<top_clock<<next_top_clock<<agenda << processes;


	}
	else
	{		agenda.clear();
		ar >>top_clock>>next_top_clock>>agenda >> processes;


	}

}

Scheduler* Scheduler::instance(){
	static Scheduler *p_=NULL;
	if (p_ == NULL)
		p_ = new Scheduler(NULL);
	return p_;
}
