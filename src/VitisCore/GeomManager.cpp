/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <fstream>
#include "GeomManager.h"
#include "Plant.h"
#include "Branc.h"
#include "randomFct.h"
#include "defVitis.h"
#include "VitisConfig.h"
#include "UtilMath.h"
#include "VitisCoreSignalInterface.h"
#include <ios>
#include <iomanip>

//#include "DebugNew.h"
//#define GEOM_PI 3.14159265358979323846

using namespace std;

GeomSuperSimpl::GeomSuperSimpl(Branc *bp)
{

	b=bp;

	if(bp->getPlant()->getConfigData().cfg_supersimplif>0)
	{

		secteurAng=new int [bp->getPlant()->getConfigData().cfg_supersimplif];

		for(int i=0; i<bp->getPlant()->getConfigData().cfg_supersimplif ; i++)
		{
			secteurAng[i]=-1;
		}
	}
	else
		secteurAng=NULL;


}

GeomSuperSimpl::~GeomSuperSimpl()
{
	if(secteurAng)
		delete [] secteurAng;

}




GeomManager::GeomManager(Plant * t):VitisObject()
{
	elemGeoNumber=0;

	elemGeoMaxNumber=0;

	v_plt=t;

	geoBrNumber=0;

    hasToBeComputed = true;
}

GeomManager::~GeomManager(void)
{

	if(!geomStack.empty())
		this->clear();

	this->clearGeomBrancStack();

}


void GeomManager::clearGeomBrancStack()
{
	while(!geomBrancStack.empty())
	{
		delete geomBrancStack.back();
		geomBrancStack.pop_back();
	}
    hasToBeComputed = true;
}

void GeomManager::clear()
{

	while(!geomStack.empty())
	{
		delete geomStack.back();
		geomStack.pop_back();
	}

	elemGeoNumber=0;;
	elemGeoMaxNumber=0;

	geoBrNumber=0;
    hasToBeComputed = true;
}

void GeomManager::computeTreeGeometry (bool silent)
{
	if (v_plt->getTopology().nbBranc == 0)
		return;

	if (v_plt->getTopology().getTrunc() == 0)
		return;

    if (!hasToBeComputed)
        return;

//	std::cout << std::endl << "computing tree geometry"<<std::endl;
	getPlant()->getSignalInterface().sigBeginGeomCompute(this);

	maxo.set(0,0,0);
	mino.set(0,0,0);

	if(v_plt->getConfigData().cfg_supersimplif)
	{
		geomStack.resize(v_plt->getTopology().nbBrancBorne,NULL);
	}

	v_plt->getTopology().getTrunc()->sortTree();

	//v_plt->getConfigData().getRandomGenerator()->RandomInit(v_plt->getConfigData().randSeed, 2, NBCOMPTRAND);

	v_plt->getTopology().ComputeNbHierarc();

	v_plt->getGeometry().clearGeomBrancStack();

	computeGeometryForAxe(v_plt->getTopology().getTrunc(), silent);

/*
	if(v_plt->getConfigData().cfg_geoSimp)
	{
		int s = geomBrancStack.size();
		for (int i=0; i<s; i++)
		{
			geomBrancStack[i]->simplification();
		}
	}
*/

    hasToBeComputed = false;

    v_plt->getConfigData().getRandomGenerator()->Random();

	getPlant()->getSignalInterface().sigEndGeomCompute(this);


//std::cout << std::endl << "current geomelem number " << elemGeoNumber <<"   max "<< elemGeoMaxNumber  << std::endl;

}


void GeomManager::addGeomBranc(GeomBranc * gb)
{
	geomBrancStack.push_back(gb);

	gb->setNum(geoBrNumber);//set indice in geomBrancStack

	geoBrNumber++;

}


void GeomManager::computeRawBoundingBox()
{

	maxo.set(0,0,0);
	mino.set(0,0,0);

	if(v_plt->getConfigData().cfg_supersimplif)
	{
		geomStack.resize(v_plt->getTopology().nbBrancBorne,NULL);
	}

	computeGeometryForAxe(v_plt->getTopology().getTrunc(), false, NULL, 1);


}

void GeomManager::computeGeometryForAxe(Hierarc * b, bool silent, GeomBranc * gbr, short max_order)
{

	GeomBranc * geobr;
	int res;
	std::vector<GeomBranc *>::iterator it;
	static int nbBranchProcessed=0;
	static float prevPercent = 0;
	float percent;


	if (gbr == 0)
	{
		nbBranchProcessed = 0;
		prevPercent = 0;
	}

	//Sort position of current branc's borne axes

	if (   b->ordre <= max_order
		&& b->getBranc()->getElementNumberOfSubLevel())
	{
		geobr=v_plt->getPlantFactory()->createInstanceGeomBranc(b, gbr);

		addGeomBranc(geobr);

		if(v_plt->getConfigData().cfg_supersimplif)
		{
			if(geomStack[b->getBranc()->V_idBranc()]==NULL)
				geomStack[b->getBranc()->V_idBranc()]=new GeomSuperSimpl(b->getBranc());
		}


		res=geobr->computeBranc(silent);

		if(res>0)
		{

			if(v_plt->getConfigData().cfg_supersimplif)
				updateGeomSuperSimpl(geobr, b->getBranc()->V_idBranc());

			nbBranchProcessed ++;
			percent = (float)nbBranchProcessed / v_plt->getTopology().NbHierarc() * 100;
			if (percent > prevPercent + .1)
			{
				prevPercent = (float)((int)(percent *10.)) / 10.;
				if (!silent)
					//cout << "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b" << setiosflags( ios::left )<< setw(9) << "geometry " << setw(5)<< prevPercent <<  setw(1) << "%";
				v_plt->getSignalInterface().sigMonitorGeometry (prevPercent);
			}

		}
		else if(res<0)
		{
			//cette branche est vide ( donc aucune cr�ation de branche)
			delete geomBrancStack.back();
			geomBrancStack.pop_back();
			geoBrNumber--;

		}
	}



}

TopoManager & GeomManager::getTopoManager ()
{
	return v_plt->getTopology();
}

GeomElem * GeomManager::getGeomElem( int idBr , int idElem )
{
	return geomBrancStack[idBr]->getElementsGeo().at(idElem);
}


void GeomManager::updateBoundingBox(GeomElem * ge)
{
	int i;
	Vector3 extreme;
	Matrix4 triedron;


	triedron=ge->getMatrix();
	extreme = triedron.getTranslation() + triedron.getMainVector()*(float)ge->getLength();

	/* updates bounding box */
	for (i=0; i<3; i++)
	{

		if (extreme[i] > maxo[i])
			maxo[i] = extreme[i];

		if (extreme[i] < mino[i])
			mino[i] = extreme[i];

	}



}


void GeomManager::updateGeomSuperSimpl(GeomBranc * gb,int id_br)
{
	int * sectAng = geomStack[id_br]->secteurAng;

	if(v_plt->getConfigData().cfg_supersimplif==0)
		return;

	if(gb->getElementsGeo().size()==0)
		return;

	//Get insertion angle of first GeomElem
	float insert = gb->getElementsGeo().front()->getElevation();

	//Compute the angle sector
	int i=(int)(insert/(GEOM_PI/(float)v_plt->getConfigData().cfg_supersimplif));

	if(i>=v_plt->getConfigData().cfg_supersimplif)
		i=v_plt->getConfigData().cfg_supersimplif-1;

	//Ajouter possibilt� de mettre plusieurs branche appartenant aux m�me secteur angulaire?
	if(sectAng[i] == -1)
		sectAng[i] = gb->getNum();


}

GeomBranc * GeomManager::getSuperSimplifedGeom(GeomBranc * gb, int id_br)
{
	if(v_plt->getConfigData().cfg_supersimplif==0)
        return NULL;

	GeomElem * ge=gb->getElementsGeo().front();

	int * sectAng = geomStack[id_br]->secteurAng;

	float insert = ge->getElevation();

	int theta=(int)(insert/(GEOM_PI/(float)v_plt->getConfigData().cfg_supersimplif));

	if(theta>=v_plt->getConfigData().cfg_supersimplif)
        theta=v_plt->getConfigData().cfg_supersimplif-1;

	if(sectAng[theta] != -1)
		return geomBrancStack[sectAng[theta]];
	else
        return NULL;


}

void GeomManager::updateElemNumber(bool ajout)
{
	if(ajout)
	{
		elemGeoNumber++;

		elemGeoMaxNumber++;
	}
	else
	{
		elemGeoNumber--;
	}


}

void GeomManager::HasToBeComputed (bool htbc)
{
    hasToBeComputed = htbc;
}






