/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file Plugin.h
///			\brief Definition of the template class CPlugin
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef PLUGIN_H
#define PLUGIN_H

#ifndef WIN32
#include <dlfcn.h>
#include <dirent.h>
#define HMODULE void*
#endif
#ifdef WIN32
#include <stdio.h>
#include <windows.h>
#endif
#include <Exceptions.h>


namespace Vitis
{

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class CPlugin
///			\brief Manage loading and initializing of plugin through dynamic libraries
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    template <class T>
    class CPlugin
    {
    public :


		/// \brief Default Constructor
        CPlugin();

        /// \brief Destructor
        ~CPlugin();


		/// \brief Load the dynamic library
		/*! This function opens the file containing the library and looks for a "StartPlugin" function into it.
		 Upon success, the address that is returned will be stored into the Function and FunctionP members.
		 The plugin should provide something like :\n
			\code
		 extern "C" MyPluginObject * StartPlugin (const std::string& myParameterFile)
		 \endcode
		 or
			\code
		 extern "C" MyPluginObject * StartPlugin ()
		 \endcode
		*/
		void Load(const std::string& Filename);

		/// \brief initialize the dynamic library by runing the "StartPlugin" function
		/// \return A pointer on the object that was returned with "StartPlugin" function
        T* Start(const void *p);
 		/// \brief initialize the dynamic library  by runing the "StartPlugin" function with custom parameter file
		/// \return A pointer on the object that was returned with "StartPlugin" function
       T* Start(const std::string& Filename, const void *p);

    private :


        typedef T* (*PtrFunc)(const void *);
        typedef T* (*PtrFuncP)(const std::string&, const void * );

         /// \brief Dynamic Library handle
         HMODULE m_Library;

		 /// \brief Pointer to the "StartPlugin" function that was collected with the Load function
		 PtrFunc Function;
		 /// \brief Pointer to the "StartPlugin" function that was collected with the Load function
		 PtrFuncP FunctionP;
    };

    #include "Plugin.inl"

} // namespace Vitis


#endif // PLUGIN_H
