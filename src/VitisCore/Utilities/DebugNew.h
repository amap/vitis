/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file DebugNew.h
///			\brief Specialize the operator/functions of allocation ans desallocation of memory to find the leaks
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef WIN32
#ifdef _DEBUGM

#ifndef DEBUGNEW_H
#define DEBUGNEW_H

//==========================================================
// En-t�tes
//==========================================================
#include "MemoryManager.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\brief Specialize the operator new
///			\param Size Size to allocate
///			\param File Source File
///			\param Line Line in source file
///			\return pointer to the allocate zone
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline void* operator new(size_t Size, const char* File, int Line)
{
    return Vitis::CMemoryManager::Instance().Allocate(Size, File, Line, false);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\brief Specialize the operator new[]
///			\param Size Size to allocate
///			\param File Source File
///			\param Line Line in source file
///			\return pointer to the allocate zone
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline void* operator new[](size_t Size, const char* File, int Line)
{
    return Vitis::CMemoryManager::Instance().Allocate(Size, File, Line, true);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\brief Specialize the operator new[]
///			\param Ptr pointer to the zone to free
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline void operator delete(void* Ptr)
{
    Vitis::CMemoryManager::Instance().Free(Ptr, false);
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\brief Specialize the operator delete
///			\param Ptr  Pointer to the zone to free
///			\param File Source File
///			\param Line Line in source file
///			\return pointer to the allocate zone
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline void operator delete(void* Ptr, const char* File, int Line)
{
    Vitis::CMemoryManager::Instance().NextDelete(File, Line);
    Vitis::CMemoryManager::Instance().Free(Ptr, false);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\brief Specialize the operator delete[]
///			\param Ptr  Pointer to the zone to free
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline void operator delete[](void* Ptr)
{
    Vitis::CMemoryManager::Instance().Free(Ptr, true);
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\brief Specialize the operator delete
///			\param Ptr  Pointer to the zone to free
///			\param File Source File
///			\param Line Line in source file
///			\return pointer to the allocate zone
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
inline void operator delete[](void* Ptr, const char* File, int Line)
{
    Vitis::CMemoryManager::Instance().NextDelete(File, Line);
    Vitis::CMemoryManager::Instance().Free(Ptr, true);
}

#endif // DEBUGNEW_H


/// \brief Definition of macros to automatisate the tracking
#ifndef new
    #define new    new(__FILE__, __LINE__)
    #define delete Vitis::CMemoryManager::Instance().NextDelete(__FILE__, __LINE__), delete
#endif


#endif // _DEBUG
#endif

