/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file DebugNew.h
///			\brief Definition of CMemoryManager
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef MEMORYMANAGER_H
#define MEMORYMANAGER_H

#include <stdlib.h>
#include "externDll.h"
#include "File.h"
#include "VitisThread.h"
#include <fstream>
#include <map>
#include <stack>
#include <string>


namespace Vitis
{

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///
	///			\class CMemoryManager
	///			\brief manage the memory - detect the memory leaks
	///
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class VITIS_EXPORT CMemoryManager
	{
		public :

			static vtsCriticalSection m_c_lock;

			//----------------------------------------------------------
			// Renvoie l'instance de la classe
			//----------------------------------------------------------
			static CMemoryManager& Instance();

			//----------------------------------------------------------
			// Ajoute une allocation m�moire
			//----------------------------------------------------------
			void* Allocate(size_t Size, const CFile& File, int Line, bool Array);

			//----------------------------------------------------------
			// Retire une allocation m�moire
			//----------------------------------------------------------
			void Free(void* Ptr, bool Array);

			//----------------------------------------------------------
			// Sauvegarde les infos sur la d�sallocation courante
			//----------------------------------------------------------
			void NextDelete(const CFile& File, int Line);

			//----------------------------------------------------------
			// Destructeur
			//----------------------------------------------------------
			~CMemoryManager();

		private :

			//----------------------------------------------------------
			// Constructeur par d�faut
			//----------------------------------------------------------
			CMemoryManager();


			//----------------------------------------------------------
			// Inscrit le rapport sur les fuites de m�moire
			//----------------------------------------------------------
			void ReportLeaks();

			//----------------------------------------------------------
			// Types
			//----------------------------------------------------------
			struct TBlock
			{
				size_t Size;  // Taille allou�e
				CFile       File;  // Fichier contenant l'allocation
				int         Line;  // Ligne de l'allocation
				bool        Array; // Est-ce un objet ou un tableau ?
			};
			typedef std::map<void*, TBlock> TBlockMap;

			//----------------------------------------------------------
			// Donn�es membres
			//----------------------------------------------------------
			std::ofstream      m_File;        // Fichier de sortie
			TBlockMap          m_Blocks;      // Blocs de m�moire allou�s
			std::stack<TBlock> m_DeleteStack; // Pile dont le sommet contient la ligne et le fichier de la prochaine d�sallocation


	};

} // namespace Vitis

#endif // MEMORYMANAGER_H
