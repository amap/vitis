/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//==========================================================
// Exception.h
//
// Exceptions d�finies par le moteur
//
//==========================================================

#ifndef EXCEPTION_H
#define EXCEPTION_H

//==========================================================
// En-t�tes
//==========================================================
#include <exception>
#include <string>
#include "externDll.h"


namespace Vitis
{
	//==========================================================
	// Classe de base pour les exceptions
	//==========================================================
	class VITIS_EXPORT  CException : public std::exception
	{
		public :

			//----------------------------------------------------------
			// Constructeur
			//----------------------------------------------------------
			CException(const std::string& Message = "");

			//----------------------------------------------------------
			// Destructeur
			//----------------------------------------------------------
			virtual ~CException() throw();

			//----------------------------------------------------------
			// Renvoie le message associ� � l'exception
			//----------------------------------------------------------
			virtual const char* what() const throw();

		protected :

			//----------------------------------------------------------
			// Donn�es membres
			//----------------------------------------------------------
			std::string m_Message; // Message associ� � l'exception
	};


	//==========================================================
	// Exception lanc�e si une condition n'est pas v�rifi�e
	//==========================================================
	struct VITIS_EXPORT CAssertException : public CException
	{
		CAssertException(const std::string& File, int Line, const std::string& Message);
	};
#ifdef _DEBUG
#   define Assert(condition) if (!(condition)) throw CAssertException(__FILE__, __LINE__, "Condition non satisfaite\n\n" #condition)
#else
	inline void DoNothing(bool) {}
#   define Assert(condition) DoNothing(!(condition))
#endif


	//==========================================================
	// Anomalie d'allocation m�moire
	//==========================================================
	struct VITIS_EXPORT CBadDelete : public CException
	{
		CBadDelete(const void* Ptr, const std::string& File, int Line, bool NewArray);
	};


	//==========================================================
	// Exception lanc�e lors d'erreur de chargement de fichiers
	//==========================================================
	struct VITIS_EXPORT CLoadingFailed : public CException
	{
		CLoadingFailed(const std::string& File, const std::string& Message);
	};


	//==========================================================
	// Exception lanc�e lors de saturations de m�moire
	//==========================================================
	struct VITIS_EXPORT COutOfMemory : public CException
	{
		COutOfMemory(const std::string& Message);
	};

} // namespace Vitis


#endif // EXCEPTION_H
