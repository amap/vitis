/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//==========================================================
// File.h
//
// D�finition de la classe CFile
//
//==========================================================

#ifndef FILE_H
#define FILE_H

//==========================================================
// En-t�tes
//==========================================================
#include <string>
#include "externDll.h"


namespace Vitis
{
	//==========================================================
	// Classe facilitant la manipulation des fichiers
	//==========================================================
	class VITIS_EXPORT CFile
	{
		public :

			//----------------------------------------------------------
			// Constructeur � partir d'un std::string
			//----------------------------------------------------------
			CFile(const std::string& Name = "unknown");

			//----------------------------------------------------------
			// Constructeur � partir d'un const char*
			//----------------------------------------------------------
			CFile(const char* Name);

			//----------------------------------------------------------
			// Indique si le fichier existe ou non
			//----------------------------------------------------------
			bool Exists() const;

			//----------------------------------------------------------
			// Renvoie le nom du fichier avec son chemin complet
			//----------------------------------------------------------
			const std::string& Fullname() const;

			//----------------------------------------------------------
			// Renvoie le nom du fichier sans son chemin
			//----------------------------------------------------------
			std::string Filename() const;

			//----------------------------------------------------------
			// Renvoie le nom du fichier sans extension ni chemin
			//----------------------------------------------------------
			std::string ShortFilename() const;

			//----------------------------------------------------------
			// Renvoie l'extension du fichier
			//----------------------------------------------------------
			std::string Extension() const;

			void setName (const std::string name);

		protected :

			//----------------------------------------------------------
			// Donn�es membres
			//----------------------------------------------------------
			std::string m_Name; // Chemin complet du fichier
	};

} // namespace Vitis


#endif // FILE_H
