/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file DebugNewOff.h
///			\brief Cancel the new and delete macro
///
///			Need when DebugNew.h is included in a header -> Include DebugNewOff.h at end of the header
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef WIN32
#ifdef _DEBUGM

#ifndef DEBUGNEWOFF_H
#define DEBUGNEWOFF_H

#undef new
#undef delete

#endif // DEBUGNEWOFF_H
#endif
#endif

