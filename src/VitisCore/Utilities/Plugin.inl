/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef WIN32
#define GetProcAddress dlsym
#define FreeLibrary dlclose
#endif


////////////////////////////////////////////////////////////
// default constructor
//
////////////////////////////////////////////////////////////
template <class T>
inline CPlugin<T>::CPlugin() :
m_Library(NULL)
{

}


////////////////////////////////////////////////////////////
// Destruct0r
//
////////////////////////////////////////////////////////////
template <class T>
inline CPlugin<T>::~CPlugin()
{
    if (m_Library)
        FreeLibrary(m_Library);
}


////////////////////////////////////////////////////////////
/// \template CPlugin
/// \brief Load DLL and get pointer to the object
///
/// [in] Filename : Path to the DLL
///
/// [retval] Pointer to the created object
//
////////////////////////////////////////////////////////////
template <class T>
inline void CPlugin<T>::Load(const std::string& Filename)
{
    // Chargement de la biblioth�que dynamique
#ifdef WIN32
    m_Library = LoadLibrary(Filename.c_str());
#else
    m_Library = dlopen(Filename.c_str(), RTLD_NOW);
#endif
    if (!m_Library)
	{
		Function = 0;
#ifdef WIN32
        throw CLoadingFailed(Filename, "error opening plugin file");
#else
        throw CLoadingFailed(Filename, dlerror());
#endif
	}

    // R�cup�ration de la fonction
    Function = (PtrFunc)(GetProcAddress(m_Library, "StartPlugin"));
    if (!Function)
        throw CLoadingFailed(Filename, "unable to find 'StartPlugin' function into library");
	FunctionP = (PtrFuncP)Function;

}


template <class T>
inline T* CPlugin<T>::Start(const void *p)
{
	return Function(p);
}
template <class T>
inline T* CPlugin<T>::Start(const std::string& Filename, const void *p)
{
	return FunctionP(Filename, p);
}
