/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "GeomBranc.h"
#include "Plant.h"
#include "VitisConfig.h"
#include "UtilMath.h"

//#include "DebugNew.h"



GeomBranc::GeomBranc(Hierarc * brc, GeomBranc * gbear, Plant * plt):VitisObject()
{


	hierarc=brc;

	v_plt=plt;

	if (gbear==NULL)
		this->gbearer = this;
	else
		this->gbearer = gbear;

	posOnBearer=0;

//	v_plt->getConfigData().getRandomGenerator()->initState((b->ordre+2)%NBCOMPTRAND);

//	v_plt->getConfigData().getRandomGenerator()->initState((b->ordre+2-(int)floor((double)(b->ordre+2)/NBCOMPTRAND)*NBCOMPTRAND));
//	v_plt->getConfigData().getRandomGenerator()->setState((b->ordre+2-(int)floor((double)(b->ordre+2)/NBCOMPTRAND)*NBCOMPTRAND));

	v_idGeomBranc = 0;

	if (gbear)
	{
        if (brc->getBranc()->getClassType() == gbear->getBranc()->getClassType())
            insertionAngle = (float)M_PI / 3;
        else
            insertionAngle = (float)M_PI;
    }
	else
		insertionAngle = 0;
}



GeomBranc::~GeomBranc(void)
{
	for (int i=0; i<elemGeo.size(); i++)
	{
		delete 	elemGeo[i];
	}
}

float GeomBranc::getScale() const {
    return scale;
}

void GeomBranc::setScale(int scale) {
	GeomBranc::scale = scale;
}







