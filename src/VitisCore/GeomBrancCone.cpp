/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "GeomBrancCone.h"
#include "Plant.h"
#include "UtilMath.h"
#include "randomFct.h"
#include "VitisCoreSignalInterface.h"
#include "iostream"




GeomBrancCone::GeomBrancCone(Hierarc * brc, GeomBranc * gbear, Plant * plt):GeomBranc(brc,gbear,plt)
{
	geoBrClone=NULL;

	//Modif sg 23.07.07
	if(gbear != NULL)
		posOnBearer=(int)(gbear->getElementsGeo().size()-1);

	//Modif sg 23.07.07
	plt->getSignalInterface().sigBeginGeomBrancCone(this);

}



GeomBrancCone::~GeomBrancCone(void)
{

	geoBrClone=NULL;

}

int GeomBrancCone::initGeomtry()
{

	//Variable destin� � sauvegarder la matrice de l'�l�ment porteur
	Matrix4   referenceTriedron;

	Vector3 transla;

	GeomElem *ge, *gprev;

	float angphy, angins;

	Hierarc * p = hierarc->getBearer();

	ge=v_plt->getPlantFactory()->createInstanceGeomElem();

	elemGeo.push_back(ge);

	//GeomElment of internode bearer
	if (hierarc == p)
	{
		gprev=ge;

	}
	else
	{	//le dernier �l�ment du tableau d'entrenoeuds correspond � l'entrenoeud porteur de cette branche
		//Modif sg 23.07.07
		gprev=gbearer->getElementsGeo().at(posOnBearer);
		referenceTriedron = gprev->getMatrix();

	}

	ge->setMatrix(referenceTriedron);

	Matrix4 & currentTriedron = ge->getMatrix();

	transla=currentTriedron.getTranslation();

	currentTriedron.setTranslation(0,0,0);

	referenceTriedron.setTranslation(0,0,0);

	angphy=computeInsertAnglePhy();

	angins=computeInsertAngle();

	currentTriedron=Matrix4::axisRotation(referenceTriedron.getMainVector(),angphy)*Matrix4::axisRotation(referenceTriedron.getNormalVector(),-angins)*currentTriedron;

	computeBending (ge);

	currentTriedron.setTranslation(transla.x(), transla.y(), transla.z());

	/* G.XII        Nouvelle origine                                        */
	if (hierarc != p)
	{
		currentTriedron = computeInsertTranslation(currentTriedron,gprev)*currentTriedron;

	}




	this->currentTrans=ge->getMatrix();

	//Update the Geom Bearer
	if(hierarc!=p)
		gprev->getGeomChildren().push_back(this);

	if(computeSuperSimplif())
		return 0;

	ge->setPtrBrc(this);

	return 1;

}

float GeomBrancCone::computeInsertAnglePhy()
{

//	return (float)M_PI * gbearer->getBranc()->getCurrentElementIndexOfSubLevel(v_plt->getPlantFactory()->levelOfDecomp);

}

float GeomBrancCone::computeInsertAngle()
{
	return insertionAngle;

}

Matrix4 GeomBrancCone::computeInsertTranslation(Matrix4 & currentTriedron, GeomElem * gprev)
{
	Vector3 ajout;
	Matrix4 & referenceTriedron=gprev->getMatrix();
	/* adjustment of initial position according to bearers diameter */

	{
		float cf;
		ajout=projection_mat(referenceTriedron.getMainVector(), currentTriedron.getMainVector());
		ajout.normalize();
		cf = ((GeomElemCone *)gprev)->getTopDiam()*(float)0.4;
		ajout *= cf;
	}


	return Matrix4::translation(Vector3(ajout[0],ajout[1],ajout[2]))*Matrix4::translation(referenceTriedron.getMainVector()*(float)gprev->getLength());

}



int GeomBrancCone::computeSuperSimplif()
{


	if(v_plt->getConfigData().cfg_supersimplif)
	{

		geoBrClone=v_plt->getGeometry().getSuperSimplifedGeom(this, hierarc->getBranc()->V_idBranc());

		if(geoBrClone!=NULL)
		{

            computeCurrentTrans (elemGeo[0]->getMatrix());

			delete elemGeo.back();

			elemGeo.pop_back();

			return 1;

		}
	}

	return 0;
}

void GeomBrancCone::computeCurrentTrans (Matrix4 transf)
{
    Vector3 transAttach=transf.getTranslation();

    Vector3 transAttachbearer=this->getGeomBrancBearer()->getElementsGeo().at(0)->getMatrix().getTranslation();

    Vector3 trans = transAttach-transAttachbearer;

    float angr=((GeomElemCone *)geoBrClone->getElementsGeo().at(0))->getAzimuth()-transf.getAzimuth();

    currentTrans=Matrix4::translation(trans)*Matrix4::axisRotation(Vector3(0,0,-1),angr);

}



int GeomBrancCone::computeBranc(bool silent)
{
	return computeBranc (ASCENDING, silent);
}

int GeomBrancCone::computeBranc(BrancSeekMode bsm, bool silent)
{
	long  j;
	Hierarc  *bearer;
	Branc *bNext;
	int pos;
	bool first;

	if(hierarc == NULL) return -1;

	bearer=hierarc->getBearer();

	/* resets buds datas */

	hierarc->getBranc()->startPosit();


	int level = v_plt->getPlantFactory()->getLevelOfDecomposition(hierarc->getBranc()->getClassType());
	if(!hierarc->getBranc()->positOnFirstElementOfSubLevel(level))
	{
		hierarc->getBranc()->endPosit();
		return -1;
	}


/*
	int savCounter = v_plt->getConfigData().getRandomGenerator()->getCounter();
	int savIdx = v_plt->getConfigData().getRandomGenerator()->setState(b->ordre+2+getPosOnBearer()%3);

	v_plt->getConfigData().getRandomGenerator()->initState(b->ordre+2+getPosOnBearer()%3);
*/

	/* scan each node of the branch */
	do
	{
		pos=hierarc->getBranc()->getCurrentElementIndexOfSubLevel(level);

		/* first node of the branch, geometry initialisation */
//		if (pos==0)
		first = false;
		if (elemGeo.size() == 0)
		{
			if(!initGeomtry ()) {

				hierarc->getBranc()->endPosit();
				//v_plt->getSignalInterface().sigEndGeomBrancCone(this);
/*
				v_plt->getConfigData().getRandomGenerator()->setState(savIdx);
				v_plt->getConfigData().getRandomGenerator()->setCounter(savCounter);
*/
				return 0;//Il y a ultra simplification topo
			}
			first = true;
		}

//		v_plt->getConfigData().getRandomGenerator()->setState((b->ordre+2)%NBCOMPTRAND);

		computeEntn(pos, first);

		if (elemGeo.size() == 0)
			if(hierarc!=bearer)
				gbearer->getElementsGeo().at(posOnBearer)->getGeomChildren().pop_back();


		/* check what is borne and recursively go down the topology */
		if (hierarc != NULL)
		{

			if (bsm == ASCENDING)
			{

				for (j=0; j<hierarc->getNbBorne(); j++)
				{
					/* get the next borne */

					bNext = hierarc->getBorne(j);
//					if (!bNext)
//                        continue;

					/* if lateral branch is not borne by that node */
					if (bNext->CurrentHierarcPtr()->indexOnBearerAtLowestLevel != pos)
					{
						/* the next branch is much far down the bearer, stop loop */
						if (bNext->CurrentHierarcPtr()->indexOnBearerAtLowestLevel > pos)
							break;
						/* continue to the next borne */
						else
							continue;
					}

					if(bNext->getCurrentElementIndexOfSubLevel(v_plt->getPlantFactory()->getLevelOfDecomposition(bNext->getClassType()))!=-1)
					{
						v_plt->getGeometry().computeGeometryForAxe(bNext->CurrentHierarcPtr(),silent,this);
					}
				}
			}
			else
			{
				for (j=hierarc->getNbBorne()-1; j>=0; j--)
				{
					/* get the next borne */

					bNext = hierarc->getBorne(j);

					/* if lateral branch is not borne by that node */
					if (bNext->CurrentHierarcPtr()->indexOnBearerAtLowestLevel != pos)
					{
						/* the next branch is much far down the bearer, stop loop */
						if (bNext->CurrentHierarcPtr()->indexOnBearerAtLowestLevel < pos)
							break;
						/* continue to the next borne */
						else
							continue;
					}

					if(bNext->getCurrentElementIndexOfSubLevel(level)!=-1)
						v_plt->getGeometry().computeGeometryForAxe(bNext->CurrentHierarcPtr(),silent,this);
				}


			}//fin for
		}//fin if


		/* update topology */
		updatePhyllotaxy();


	}while(hierarc->getBranc()->positOnNextElementOfSubLevel(level));



	//v_plt->getConfigData().getRandomGenerator()->setState(0);

	hierarc->getBranc()->endPosit();

	if(v_plt->getConfigData().cfg_supersimplif)
	{
		if(!elemGeo.empty())
		{
			Vector3 transAttach=elemGeo[0]->getMatrix().getTranslation();

			Vector3 transAttachbearer=this->getGeomBrancBearer()->getElementsGeo().at(0)->getMatrix().getTranslation();

			Vector3 trans = transAttach-transAttachbearer;

			currentTrans=Matrix4::translation(trans);
		}
	}

	//v_plt->getSignalInterface().sigEndGeomBrancCone(this);

	if(v_plt->getConfigData().cfg_geoSimp)
		simplification();

/*
	v_plt->getConfigData().getRandomGenerator()->setState(savIdx);
	v_plt->getConfigData().getRandomGenerator()->setCounter(savCounter);
*/

	if(elemGeo.empty())
		return -1;

	return 1;
}



/* computes current geometry */
void GeomBrancCone::computeEntn(int pos, bool first)
{
	GeomElemCone * gelm;

	GeomElemCone  *gprev;


	float   d;
	float   bottomDiam, topDiam;




	if (first)
	{
		gelm=(GeomElemCone *)elemGeo.front();
		gprev=gelm;
	}
	else
	{
		gprev=(GeomElemCone *)elemGeo[elemGeo.size()-1];
		gelm=(GeomElemCone *)v_plt->getPlantFactory()->createInstanceGeomElem();

		gelm->copy(gprev);

		elemGeo.push_back(gelm);

		//Update the Geom Bearer
		gelm->setPtrBrc(this);
	}

if (pos != elemGeo.size() - 1)
	gelm->setPosInGeomBranc((elemGeo.size() - 1));
else
	gelm->setPosInGeomBranc((elemGeo.size() - 1));
	gelm->setPosInBranc(pos);




	/* compute bending */
	flexionAtNode();

	currentTrans.getMainVector().normalize();

	currentTrans.getNormalVector().normalize();

	currentTrans.getSecondaryVector().normalize();

	gelm->setMatrix(this->currentTrans);



	/* initial triedron (previous one that will be
	 * transformed accordind to phyllotaxy and bending)
	 * */
	Matrix4 &  currentTriedron = gelm->getMatrix();

	Matrix4 & previousTriedron=gprev->getMatrix();

	/* compute length */
	/* TODO if external computation flag is set for length
	 * then overwrite the standard procedure */


	d = computeLength ();


	if(d<0.0001)
	{
		if(this->hierarc->getNbBorne()==0)
		{
			delete elemGeo.back();
			elemGeo.pop_back();
			return;
		}

	}

	gelm->setLength(d, true);


	bottomDiam  = 1.0;

	topDiam = 1.0;

	topDiam=computeTopDiam();

	topDiam = gelm->setTopDiam(topDiam, true);

	//Translation along the Principal vector of the bearer
	if(pos>0)
	{
		currentTriedron=Matrix4::translation(previousTriedron.getMainVector()*(float)gprev->getLength())*currentTriedron;

		this->currentTrans=currentTriedron;

		bottomDiam=computeBottomDiam();
	}
	else
	{
		bottomDiam=topDiam;
	}

	gelm->setBottomDiam(bottomDiam);

	gelm->setSymbole(findSymbole());

	this->v_plt->getGeometry().updateElemNumber(true);

	this->currentTrans = this->getBranc()->getPlant()->getSignalInterface().sigElemGeometry(gelm, this->currentTrans);
    gelm->setMatrix(this->currentTrans);

	v_plt->getGeometry().updateBoundingBox(gelm);

}



/* compute current length */
float GeomBrancCone::computeLength ()
{
	return 1;
}


float GeomBrancCone::computeTopDiam()
{ //phy age
	float topDiam;

	topDiam = 1.0;

	return topDiam;

}

float GeomBrancCone::computeBottomDiam()
{
	float bottomDiam;
	GeomElemCone * gelm;

//JFB Cleaning 28/04/08
	if (elemGeo.size() <= 1)
		gelm=(GeomElemCone *)elemGeo.back();
	else
		gelm=(GeomElemCone *)elemGeo.at(elemGeo.size()-2);

	bottomDiam = gelm->getTopDiam();

	return bottomDiam;
}






void GeomBrancCone::simplification ()
{

	Vector3 positionTop;

	Vector3 positionBottom;

	Vector3 tmpVec, rotVec, transla, vectmp;

	unsigned int i,j;

	float scalarDir;

	float dif, dirThreshold, distThreshold;

	GeomElemCone * gelmprec, * gelm;

	i=1;

	if(!v_plt->getConfigData().cfg_geoSimp)
		return ;
	else if (v_plt->getConfigData().cfg_geoSimp >= 3)
	{
		dirThreshold = .90;
		distThreshold = 1.;
	}
	else if (v_plt->getConfigData().cfg_geoSimp >= 2)
	{
		dirThreshold = .93;
		distThreshold = .6;
	}
	else
	{
		dirThreshold = .96;
		distThreshold = .3;
	}

	while(i<elemGeo.size())
	{

		gelm=(GeomElemCone *)elemGeo[i];

		gelmprec=(GeomElemCone *)elemGeo[i-1];



		Matrix4 & currentTriedron = gelm->getMatrix();

		Matrix4 & precTriedron = gelmprec->getMatrix();


		dif=0;
		/* computes distance between top of current level and bottom of organ */
		positionTop=currentTriedron.getTranslation();

		positionBottom=precTriedron.getMainVector()*(float)(gelmprec->getLength())+precTriedron.getTranslation();

		dif += fabs(positionTop[0]-positionBottom[0]);
		dif += fabs(positionTop[1]-positionBottom[1]);
		dif += fabs(positionTop[2]-positionBottom[2]);


		/* computes direction shift between current level and organ */

		scalarDir = currentTriedron.getMainVector()*precTriedron.getMainVector();


		if (gelm->getSymbole() != gelmprec->getSymbole())
			i++;
		else if (!customSimplification(gelmprec, gelm))
			i++;
		else if (  (   scalarDir > dirThreshold
			        && dif < distThreshold
			        && gelm->getGeomChildren().empty())
				|| (scalarDir >= 0.9999))
		{
			/* concatenate current level and organ */

			gelmprec->setTopDiam(gelm->getTopDiam());

			if(scalarDir<0.999)
			{

				tmpVec=(precTriedron.getMainVector()*(float)gelmprec->getLength())+(currentTriedron.getMainVector()*(float)gelm->getLength());

				vectmp=(precTriedron.getMainVector()*(float)gelmprec->getLength());

				gelmprec->setLength(norm(tmpVec));

				vectmp.normalize();

				tmpVec.normalize();

				rotVec=vectmp^tmpVec;

				rotVec.normalize();

				transla=precTriedron.getTranslation();

				precTriedron=Matrix4::translation(-transla)*precTriedron;

				precTriedron=Matrix4::axisRotation(rotVec, angle(vectmp,tmpVec))*precTriedron;

				precTriedron=Matrix4::translation(transla)*precTriedron;

			}
			else
			{
				gelmprec->setLength(gelmprec->getLength()+gelm->getLength());
			}



			elemGeo.erase(elemGeo.begin()+i);


			for(j=0; j<gelm->getGeomChildren().size(); j++)
			{


				gelm->getGeomChildren().at(j)->setPosOnBearer(i);

				gelmprec->getGeomChildren().push_back(gelm->getGeomChildren().at(j));
			}

			this->v_plt->getGeometry().updateElemNumber(false);

			gelmprec->setPosInGeomBranc(i-1);
			gelmprec->setPosInBranc(gelm->getPosInBranc());

			delete gelm;
		}
		else
			i++;



	}//end while



}




void GeomBrancCone::copyFrom(GeomBranc * geoBr,  Matrix4  Mtransf )
{
	unsigned int i,j;

	Matrix4  transf = Mtransf;

	GeomElemCone * get;

	GeomElemCone * gprev;

	GeomBranc * geob ;

	std::vector<GeomElem *>& elemGeoToClone = geoBr->getElementsGeo();

	//geomelem bearer
	gprev=(GeomElemCone *)gbearer->getElementsGeo().back();

    for(i=0; i<elemGeoToClone.size(); i++)
	{
		//On est sur le premier entrenoeud

		get=(GeomElemCone *)v_plt->getPlantFactory()->createInstanceGeomElem();

		get->copy(elemGeoToClone[i]);

		this->v_plt->getGeometry().updateElemNumber(true);

		get->setPtrBrc(this);

		elemGeo.push_back(get);

		pasteElem(get, transf);

//		transf=Matrix4::translation(get->getMatrix().getMainVector()*get->getLength())*transf;

		for(j=0; j<elemGeoToClone[i]->getGeomChildren().size(); j++)
		{
			geob=elemGeoToClone[i]->getGeomChildren().at(j);

			if(geob!=geoBr)
			{

				GeomBranc * geomChild = v_plt->getPlantFactory()->createInstanceGeomBranc(geob->getAxe(), this);

				v_plt->getGeometry().addGeomBranc(geomChild);

                ((GeomBrancCone*)geomChild)->setGeomBrancClone(NULL);
                ((GeomBrancCone*)geomChild)->copyFrom(geob, Mtransf);

				get->getGeomChildren().push_back(geomChild);
			}


		}

		gprev=get;
	}

}


void GeomBrancCone::pasteElem(GeomElem * ge, Matrix4  trans)
{

	Vector3 transla;

	Matrix4 & currentTriedron=ge->getMatrix();

//	transla=currentTriedron.getTranslation();

//	currentTriedron= trans*Matrix4::translation(-transla)*currentTriedron;
	currentTriedron= trans*currentTriedron;

}


GeomElem GeomBrancCone::getTranslationForCurvilinearDistance (double curvilinearDistance) {
	double cumul_curv_distance = 0;
	GeomElem current_elem;
	double delta_lenght=0;
    bool found = false;
    double current_internode_length = 0;
    int i = 0;


	//Retrieve internode number
	int entn_number = elemGeo.size();



	//Traverse all the internodes list
	for (i=0; i<elemGeo.size(); i++) {

		//Sum internode length
        current_internode_length = (elemGeo.at(i))->getLength();
		cumul_curv_distance += current_internode_length;

        //Main TEST
		if(cumul_curv_distance >= curvilinearDistance) { //Cumul exceed the place where to put the foliole / spine
            found = true;
			break;
        }
	}







    //Routing
    if(entn_number>0) {
        //There is entn on the left rachis

        if (found) {
            //Bearer internode found

            //Retrieve internode on which we will put the element (spine / petiole)
            current_elem.copy(elemGeo.at(i));

            //Compute the !!BACKWARD!! offset inside the internode (the Amapsim engine put the foliole / spine at the end of the internode, SO WE HAVE TO MOVE BACKWARD!!!)
            delta_lenght =  cumul_curv_distance - curvilinearDistance;
        } else {
            //End of internode list reached => no internode can hold the foliole / spine, it is to far away

            //Should have never happened
            current_elem.copy(elemGeo.at(i-1));
            delta_lenght =  0;
			//std::cerr << "EJ30004";
        }
    } else {
        //No entn on the left rachis!!

        //Should have never happened
		//std::cerr << "EJ30003";
    }







	//Retrieve the internode Matrix
	Matrix4 current_elem_trans (current_elem.getMatrix());

	//Compute the translation vector
	Vector3 vec_translate (current_elem_trans.getMainVector()*(-delta_lenght)); //delta_length is minus preceded to move BACKWARD!!!

	//
	current_elem_trans = Matrix4::translation(vec_translate)*current_elem_trans;

	//Update rightside internode
	current_elem.setMatrix(current_elem_trans);

	return current_elem;
}


