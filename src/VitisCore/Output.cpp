/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/


#include <stdio.h>
#include "Output.h"
#include "Plant.h"
#include "scheduler.h"
#include <sstream>
#include "DebugNew.h"


Output::Output():VProcess()
{
	cpt = 0;
	swap = false;
}

Output::~Output(void)
{
	Scheduler::instance()->stop_process(this->process_id);
}

void Output::setOutputFormat (OutputFormat * outFor)
{
	outFormat = outFor;
	swap = false;

}

void Output::output( )
{
	int savCounter = v_plt->getConfigData().getRandomGenerator()->getCounter();
	int savIdx = v_plt->getConfigData().getRandomGenerator()->setState(2);// 0 and 1 counter are dedicated to topology

	computeDataToOutput();

	std::ostringstream oss;

	oss <<filename<<"_";

	char prev = oss.fill ('0');
	oss.width (3);

	oss<<Scheduler::instance()->getTopClock();

	writeData(oss.str());

	freeData();

	cpt++;

	v_plt->getConfigData().getRandomGenerator()->setState(savIdx);
	v_plt->getConfigData().getRandomGenerator()->setCounter(savCounter);
}



void Output::process_event(EventData * )
{
	output ();

//	if(Scheduler::instance()->getTopClock()+step<=Scheduler::instance()->getHotStop()+0.000001)
	if(Scheduler::instance()->getTopClock()+step<=v_plt->getConfigData().simuTotalTime+0.000001)
		Scheduler::instance()->self_signal_event(NULL,(double)Scheduler::instance()->getTopClock()+step,1);


}

const std::string & Output::getFileName()
{
	return filename;
}

void Output::setFileName(const std::string & name)
{
	filename=name;
}

void Output::setPlant(Plant * plt)
{
	v_plt=plt;
}

void Output::setStep(double s)
{
	step=s;
}



OutputManager::OutputManager()
{
	s_Library.reserve(100);
}

OutputManager::~OutputManager(void)
{
	while(!outpVec.empty())
	{
		delete outpVec.back();
		outpVec.pop_back();

	}

}



void OutputManager::addOutputDevice (const std::string& dllname)
{
	s_Library.push_back(Vitis::CPlugin<Output>());

	s_Library.back().Load(dllname);
}

int OutputManager::create_and_schedule_output(Plant * plt, const std::string& dllname, const std::string& filename, double step, bool swap)
{
	addOutputDevice (dllname);
	printf ("schedule %s\n", dllname.c_str());
	create_and_schedule_output (plt, s_Library.size()-1, filename, step, swap);

	return s_Library.size()-1;
}

Output * OutputManager::create_output(Plant * plt, int indexDevice, const std::string filename, bool swap)
{
	Output * o;
	o=s_Library[indexDevice].Start(NULL);

	o->setFileName(filename);

	o->setPlant(plt);

	o->setSwap (swap);

	outpVec.push_back(o);

	return o;

}

void OutputManager::scheduler_output(Plant * plt,Output * outp, double step)
{

	outp->setStep(step);

	Scheduler::instance()->create_process(outp,NULL,step,1);

}

void OutputManager::create_and_schedule_output(Plant * plt, int indexDevice, const std::string& filename, double step, bool swap)
{
	Output * o;
	o=s_Library[indexDevice].Start(NULL);

	o->setFileName(filename);

	o->setPlant(plt);

	o->setStep(step);

	o->setSwap (swap);

	outpVec.push_back(o);

//	Scheduler::instance()->create_process(o,NULL,step+.000000001,1);
	Scheduler::instance()->create_process(o,NULL,step,1);
}


int OutputManager::getNumberOfOutputDevice ()
{
	return s_Library.size();

}


void OutputManager::clean (Plant *plt)
{
    Output *o;
    bool trouve=true;
    vector<Output *>::iterator it;
    while (trouve)
    {
        trouve = false;
        for (it=outpVec.begin(); it !=outpVec.end(); it++)
        {
            o = *it;
            if (o->getPlant() == plt)
            {
                trouve = true;
                delete o;
                break;
            }
        }
        if (trouve)
            outpVec.erase(it);
    }
}

OutputManager* OutputManager::instance()
{
	static OutputManager *p_=NULL;
	if (p_ == NULL)
		p_ = new OutputManager();
	return p_;
}

long OutputFormat::swapDatal (long v)
{
	long i = v;
	char a, *vv=(char*)&i;


	a = vv[0];
	vv[0] = vv[3];
	vv[3] = a;

	a = vv[2];
	vv[2] = vv[1];
	vv[1] = a;

//	i = (long)swapData (i);
	return (i);
}
float OutputFormat::swapDataf (float v)
{
	float f = v;
	f = swapData (f);
	return (f);
}
float OutputFormat::swapData (float v)
{
	char a, *vv=(char*)&v;


	a = vv[0];
	vv[0] = vv[3];
	vv[3] = a;

	a = vv[2];
	vv[2] = vv[1];
	vv[1] = a;

	return v;
}






