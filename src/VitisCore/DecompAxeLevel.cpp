/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

// GrowthUnit.cpp: implementation of the GrowthUnit class.
//
//////////////////////////////////////////////////////////////////////
#include "DecompAxeLevel.h"
#include "Plant.h"
#include <string>
#include "std_serialize.h"
#include "DebugNew.h"
#include "VitisCoreSignalInterface.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

DecompAxeLevel::DecompAxeLevel(USint level)
{
	currentIndex=-1;
	currentLevel=level;
	sizeElement=0;
	currentGoto=-1;
	parent = 0;
	// give a unique id to this
	id = getPlant()->getTopology().createId();

}

DecompAxeLevel::DecompAxeLevel(Plant *p, USint level)
{
	currentIndex=-1;
	currentLevel=level;
	sizeElement=0;
	currentGoto=-1;
	parent = 0;
	// give a unique id to this
	id = p->getTopology().createId();
}


DecompAxeLevel::DecompAxeLevel(DecompAxeLevel * groc)
{
	currentIndex=groc->currentIndex;
	id = groc->getId();
}

DecompAxeLevel::~DecompAxeLevel()
{
	int i;

	if (   !this->getPlant()
		|| this->getPlant()->getSignalInterface().sigDelDecompAxeLevel(this) == 0)
	{
		for(i=0; i<sizeElement; i++)
		{
			delete ss[i];
		}
	}
}

void DecompAxeLevel::startPosit()
{
	if (   currentGoto<0
        || dposit.size() == 0)
	{
        dposit.push_back(currentIndex);
    }
	else
	{
		dposit.push_back(dposit[currentGoto]);
    }

	currentGoto++;

	for(int i=0; i<sizeElement; i++)
	{
		ss[i]->startPosit();
	}
}


void DecompAxeLevel::endPosit()
{
if (dposit.size() <= 0)
{
    return;
}

	for(int i=0; i<sizeElement; i++)
	{
		ss[i]->endPosit();
	}

	dposit.pop_back();

	currentGoto--;
}

Plant * DecompAxeLevel::getPlant ()
{
	Branc *b = (Branc*)this->getParentOfLevel(0);
	if (b)
		return b->V_plt();
	else
		return 0;
}

USint DecompAxeLevel::positOnFirstElementOfSubLevel(int level)
{

	if(level==-1)
		level=currentLevel+1;

	if(!sizeElement)
		return 0;

	dposit[currentGoto]=0;

	if(currentLevel==level-1)
		return 1;

	while(!ss[dposit[currentGoto]]->positOnFirstElementOfSubLevel(level))
	{
		if(dposit[currentGoto]+1 >= sizeElement)
			return 0;
		dposit[currentGoto]=dposit[currentGoto]+1;

	}
	return 1;

}

USint DecompAxeLevel::positOnLastElementOfSubLevel(int level)
{
	if(level==-1)
		level=currentLevel+1;

	if(!sizeElement)
		return 0;

	dposit[currentGoto]=(int)sizeElement-1;

	if(currentLevel==level-1)
		return 1;

	while(!ss[dposit[currentGoto]]->positOnLastElementOfSubLevel(level))
	{
		if(dposit[currentGoto]-1 < 0)
			return 0;
		dposit[currentGoto]=dposit[currentGoto]-1;

	}
	return 1;
}


USint DecompAxeLevel::positOnNextElementOfSubLevel(int level)
{

	if(level==-1)
		level=currentLevel+1;

	if(!sizeElement)
		return 0;

	if(currentLevel==level-1)
	{
		dposit[currentGoto]=dposit[currentGoto]+1;

		if(dposit[currentGoto]>=sizeElement)
		{	//this is the end of this axe level
			dposit[currentGoto]=(int)sizeElement-1;
			return 0;
		}

	}
	else
	{
		//try to posit on next element within sublevel
		while(!ss[dposit[currentGoto]]->positOnNextElementOfSubLevel(level))
		{
			//if don't posit

			//try to posit on next DecompElement within this level
			dposit[currentGoto]=dposit[currentGoto]+1;

			if(dposit[currentGoto]>=sizeElement)
			{	//this is the end of this axe level
				dposit[currentGoto]=(int)sizeElement-1;
				return 0;
			}
			else
			{
				//try to posit on first element within sublevel
				if(ss[dposit[currentGoto]]->positOnFirstElementOfSubLevel(level))
					return 1;
				//sinon on repart pour se positionner sur le prochain

			}

		}
	}

	return 1;
}

USint DecompAxeLevel::positOnPreviousElementOfSubLevel(int level)
{

	if(level==-1)
		level=currentLevel+1;

	if(!sizeElement)
		return 0;

	if(currentLevel==level-1)
	{
		dposit[currentGoto]=dposit[currentGoto]-1;

		if(dposit[currentGoto]==-1)
		{	//this is the end of this axe level
			dposit[currentGoto]=0;
			return 0;
		}

	}
	else
	{
		//try to posit on previous element within sublevel
		while(dposit[currentGoto]>=0 && !ss[dposit[currentGoto]]->positOnPreviousElementOfSubLevel(level))
		{
			//if can't posit

			//try to posit on previous DecompElement within this level
			dposit[currentGoto]=dposit[currentGoto]-1;

			if(dposit[currentGoto]==-1)
			{	//this is the start of this axe level
				dposit[currentGoto]=0;
				return 0;
			}
			else
			{
				//try to posit on last element within sublevel
				if(ss[dposit[currentGoto]]->positOnLastElementOfSubLevel(level))
					return 1;

			}
		}
	}

	return 1;
}




USint DecompAxeLevel::positOnElementAtSubLevel(int pos, int level)
{
	if(level==-1)
		level=currentLevel+1;

	if(!positOnFirstElementOfSubLevel(level))
		return 0;

	for (int i=0; i<pos; i++)
	{
		if(!positOnNextElementOfSubLevel(level))
			return 0;
	}
	return 1;

}

DecompAxeLevel * DecompAxeLevel::getParentOfLevel (int level)
{
	DecompAxeLevel *ret=this;

	if (   level > 0
		&& level > currentLevel)
		level = currentLevel-1;

	for (int i=currentLevel; i>level; i--)
	{
		ret = ret->parent;
		if (   !ret
			|| !ret->parent)
			break;
	}

	return ret;
}


void DecompAxeLevel::addSubElementAtLevel(DecompAxeLevel * declevl, int _index)
{

	if(declevl->currentLevel-1==currentLevel)
		addElement(declevl, _index);
	else
	{	if(!sizeElement)
		{//return error code ..can't access this level
			return;
		}
		else
			ss[CurrentIndex()]->addSubElementAtLevel(declevl, _index);
	}

}

int DecompAxeLevel::removeSubElementAtLevel(DecompAxeLevel * declevl)
{

	if(declevl->currentLevel-1==currentLevel)
	{
		if (removeElement(declevl) == -1)
		{
		}
		return 1;
	}
	else
	{	if(!sizeElement)
		{//return error code ..can't access this level
			return 0;
		}
		else
			return ss[CurrentIndex()]->removeSubElementAtLevel(declevl);
	}

}


void DecompAxeLevel::addElement (DecompAxeLevel * declevel, int _index)
{
	//// give a unique id to declevel
	//declevel->setId (getPlant()->getTopology().createId());

	if(currentLevel>=declevel->getLevel())
	{
		//Envoyer code erreur ...un level x ne peut emcapsuler ke des �l�ment d'un nivo inf�rieur
		return;
	}
	declevel->parent = this;
	if(_index && _index < ss.size()){
        ss.insert(ss.begin() + _index , declevel);
	}
	else{
        ss.push_back(declevel);
        currentIndex++;
	}
	sizeElement++;

	this->getPlant()->getSignalInterface().sigDecompAxeLevel(declevel);

    this->getPlant()->getGeometry().HasToBeComputed(true);

}



int DecompAxeLevel::removeElement (DecompAxeLevel * declevel)
{

	if(currentLevel>=declevel->getLevel())
	{
		//Envoyer code erreur ...un level x ne peut emcapsuler ke des �l�ment d'un nivo inf�rieur
		return 0;
	}

    this->getPlant()->getGeometry().HasToBeComputed(true);

	// find this element
	vector<DecompAxeLevel*>::iterator it;

	for (it=ss.begin(); it!=ss.end(); it++)
	{
		if (*it == declevel)
			break;
	}

	if (it != ss.end())
	{
		// remove every sub elements
		while (!declevel->empty())
			declevel->removeSubElementAtLevel (declevel->ss.front());

		// remove current elment
		ss.erase (it);
		currentIndex=0;
		sizeElement--;

		delete declevel;

		if (sizeElement == 0)
			return -1;
		else
			return 1;
	}
	else
		return 0;

}



bool DecompAxeLevel::empty()
{
	if(!sizeElement)
		return true;
	return false;
}




DecompAxeLevel * DecompAxeLevel::getCurrentElementOfSubLevel (int level)
{
	int indexLoc;

	if(!sizeElement)
		return NULL;

	if(level==-1)
		level=currentLevel+1;

	if(currentGoto>=0)
		indexLoc=dposit[currentGoto];
	else
		indexLoc=currentIndex;

	if (indexLoc >= sizeElement)
		indexLoc = sizeElement -1;


	if(currentLevel==level-1)
	{
		return ss[indexLoc];
	}
	else
	{
		return ss[indexLoc]->getCurrentElementOfSubLevel(level);
	}

}



int DecompAxeLevel::getCurrentElementIndexOfSubLevel(int level)
{
	int levelElemIndex=-1;
	int delevel;

	if(!sizeElement)
		return -1;

	//check if we are in positionning mode
	if(currentGoto>=0)
		delevel=dposit[currentGoto];
	else
		delevel=currentIndex;

	if(level==-1)
		level=currentLevel+1;

	if(currentLevel==level-1) return delevel;

	for(int i=0; i<=delevel; i++)
	{
		levelElemIndex += ss[i]->getCurrentElementIndexOfSubLevel(level)+1;
	}

	return levelElemIndex;
}

std::string DecompAxeLevel::getName()
{
	return std::string ("topo_LEVEL"+currentLevel);
}

int DecompAxeLevel::getElementNumberOfSubLevel(int level)
{

	int elemNumber=0;
	int i;

	if(level==-1)
		level=currentLevel+1;

	if(currentLevel==level-1)
		return (USint)sizeElement;

	for(i=0; i<sizeElement; i++)
	{
		elemNumber = elemNumber + (int)ss[i]->getElementNumberOfSubLevel(level);
	}

	return elemNumber;
}





void DecompAxeLevel::serialize(Archive& ar )
{

	if( ar.isWriting() )
	{
		ar <<currentLevel<< sizeElement<< currentGoto;
		ar <<currentIndex<< dposit;
		std::vector<DecompAxeLevel *>::const_iterator I;
		ar << (unsigned long)ss.size();
		for(I=ss.begin(); I!=ss.end(); I++)
			ar << (*(*I));


	}
	else
	{
		ar >>currentLevel>> sizeElement>> currentGoto;
		ar >>currentIndex>> dposit;

		ss.clear();

		unsigned long size; ar >> size;
		if(size)
		{
			Plant *plt = this->getPlant();
			ss.resize(size);
			std::vector<DecompAxeLevel *>::iterator I = ss.begin();
			while(size--)
			{
				*I= plt->getPlantFactory()->createInstanceDecompAxe(currentLevel+1);
				ar >> (*(*I));
				I++;
			}
		}
	}

}

DecompAxeLevel *DecompAxeLevel::getDecompAxeLevel (int id)
{
	DecompAxeLevel *d;

	if (this->getId() == id)
		return this;

	int level = getLevel()+1;
	startPosit();

	if(!positOnFirstElementOfSubLevel(level))
	{
		endPosit();
		return NULL;
	}

	do
	{
		if ((d = this->getCurrentElementOfSubLevel(level)->getDecompAxeLevel(id)) != NULL)
		{
			endPosit();
			return d;
		}
	} while(positOnNextElementOfSubLevel(level));


	endPosit();

	return NULL;

}



