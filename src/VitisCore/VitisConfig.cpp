/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "VitisConfig.h"
#include "std_serialize.h"


VitisConfig::VitisConfig(void)
{

	simuTotalTime=0;
	nbExternModules=0;
	step_output=0;
	cfg_supersimplif=0;
	cfg_topoSimp=3;
	cfg_birthDateThresholdTopoSimp = 0;
//	cfg_geometrie=1;
//	cfg_showSimp=0;
	randSeed=0;
	outputFileName="defaultOutput";
	serialFileName="defaultSerial";
	randGenerator=NULL;
	hotStart=0;
	nbExternModules=0;

	light.set(0,0,100);

}

VitisConfig::~VitisConfig(void)
{
	delete randGenerator;

	if(!ExternModules.empty())
	{
		ExternModules.erase(ExternModules.begin());
		ExternModulesParam.erase(ExternModulesParam.begin());
	}

}



#ifdef WIN32
char * VitisConfig::makepath (char *path,char *rep,char *fic)
{
	if (   *fic == '\\'
			|| strncmp(fic,"..",2) == 0
			|| *(fic+1) == ':' ) /* fic est deja un chemin d'acces */
	{
		strcpy_s(path,256,fic);
	}
	else if (!rep || *rep == '\000')	       /* Pas de repertoire */
	{
		strcpy_s(path,256,fic);
	}
	else
	{
		sprintf_s(path, 256, "%s\\%s", rep, fic); /* Path = concat(Rep,'/',Fic) */
	}
	return path;
}
#else
char *VitisConfig::makepath (char *path,char *rep,char *fic)
{
	char temp[256];

	if (*fic == ':' || *fic == '/' || strncmp(fic,"..",2) == 0) /* fic est deja un chemin d'acces */
		strcpy(path,fic);

	else if (*rep == '\000')	       /* Pas de repertoire */
		strcpy(path,fic);

	else
	{
		sprintf(temp, "%s/%s", rep, fic);      /* Path = concat(Rep,'/',Fic) */
		strcpy(path, temp);
	}

	return(path);
}
#endif





void VitisConfig::initRandomGenerator()
{
    if(randGenerator!=NULL)
        delete randGenerator;
	randGenerator =  new TRandomMersenne (randSeed);
}

TRandomMersenne * VitisConfig::getRandomGenerator()
{ return randGenerator;}


void VitisConfig::serialize(Archive& ar )
{
	if( ar.isWriting() )
	{
		ar <<simuTotalTime<< randSeed<< nbExternModules<< step_output;
		ar << modname<< cfg_topoSimp<< cfg_geoSimp;
		ar <<light<< ExternModulesParam<< ExternModules;
		ar <<*randGenerator;
	}
	else
	{
		initRandomGenerator();
		ar >>simuTotalTime>> randSeed>> nbExternModules>> step_output;
		ar >>modname>> cfg_topoSimp>> cfg_geoSimp;
		ar >>light>> ExternModulesParam>> ExternModules;
		ar >>*randGenerator;
	}
}


void VitisConfig::addExternalModule (char *module, char *pmodule)
//void VitisConfig::addExternalModule (std::string module, std::string pmodule)
{
	nbExternModules ++;
	char *mm = strdup(module);
	char *pm = strdup(pmodule);
	ExternModules.push_back (mm);
	ExternModulesParam.push_back (pm);
}

