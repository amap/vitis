/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "PlantBuilder.h"
#include "Plant.h"
#include "GeomBrancCone.h"
#include "GeomElemCone.h"

PlantBuilder::PlantBuilder(Plant * plt)
{
	v_plt=plt;
}

PlantBuilder::~PlantBuilder(void)
{
}



GeomBranc * PlantBuilder::createInstanceGeomBranc(Hierarc * brc, GeomBranc * gbr)
{
	return new GeomBrancCone(brc,gbr,v_plt);
}

Branc * PlantBuilder::createInstanceBranc ()
{
	return new Branc(v_plt);
}

DecompAxeLevel * PlantBuilder::createInstanceDecompAxe (USint level)
{
	return new DecompAxeLevel (v_plt, level);
}

GeomElem * PlantBuilder::createInstanceGeomElem ()
{
	return new GeomElemCone ();
}

void PlantBuilder::setLevelOfDecomposition(std::string mod, int level)
{
    LevelOfDecompMap::iterator it = modelLevelOfDecomp.begin();
    modelLevelOfDecomp.insert(it, LevelOfDecompPair(mod, level));
}

int PlantBuilder::getLevelOfDecomposition(std::string mod)
{
    if (modelLevelOfDecomp.find(mod) == modelLevelOfDecomp.end())
        return 0;
    return modelLevelOfDecomp.find(mod)->second;
}
