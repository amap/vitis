/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include "Plugin.h"
#include "Plant.h"
#include "Serializer.h"
#include "FileArchive.h"
#include "DebugNew.h"
#include "ObserverManager.h"
#include "Voxel.h"
#include "PlotManager.h"
#include "Output.h"


Plant::Plant()
{
	v_nameTree="default";
//	v_paramFileName="default";
	v_pltBuilder=NULL;

	v_config=new VitisConfig();

	v_topoMan = new TopoManager(this);
	v_geomMan= new GeomManager(this);
//	v_actionScheduler = new Scheduler(this);
	v_actionScheduler = Scheduler::instance();
	v_actionScheduler->setPlant(this);
	v_serial = new Serializer(this);

	v_signal = new VitisCoreSignalInterface();

	v_signal->sigPlant (this);
	PlotManager::instance()->add(this);

	theMerde = 0;
}


Plant::Plant(const std::string & nameT, const std::string & nameP)
{

	v_nameTree=nameT;
//	v_paramFileName=nameP;

	v_pltBuilder=NULL;

	v_config=new VitisConfig();

	v_topoMan = new TopoManager(this);
	v_geomMan= new GeomManager(this);
//	v_actionScheduler = new Scheduler(this);
	v_actionScheduler = Scheduler::instance();
	v_actionScheduler->setPlant(this);
	v_serial = new Serializer(this);

	v_signal = new VitisCoreSignalInterface();

	PlotManager::instance()->add(this);
//	v_signal->sigPlant (this);

	theMerde = 0;
}

Plant::~Plant(void)
{
   v_signal->sigPlantDelete (this);

	// delete any output linked to this plant
	PlotManager::instance()->remove(this);
	OutputManager::instance()->clean(this);
    VoxelSpace::instance()->resetContent (this);

	if(v_topoMan != NULL)
		delete v_topoMan;

	if(v_geomMan != NULL)
		delete v_geomMan;

	//if(v_actionScheduler != NULL)
	//	delete v_actionScheduler;

	if(v_config != NULL)
		delete v_config;

	if(v_serial != NULL)
		delete v_serial;

	if(v_pltBuilder != NULL)
		delete v_pltBuilder;

	delete v_signal;


}

void Plant::setSeed(USint seed)
{
	getConfigData().randSeed=seed;
}

void Plant::setHotStart(double cycle)
{

	getConfigData().hotStart=cycle;

}

void Plant::setHotStop(double cycle)
{

	if (Scheduler::instance()->getHotStop() < cycle)
		Scheduler::instance()->setHotStop(cycle);

}

short Plant::hasFinishedCurrentCycleOrgano (int numCycleP)
{

	int numCycle=(int)(Scheduler::instance()->getTopClock());
	int numCycleNext=(int)(Scheduler::instance()->getNextTopClock());

	return (numCycle==numCycleP) && (numCycleNext>numCycle);

}

void Plant::setPosition(const Vector3 & pos)
{
	v_geomMan->setPosition(pos);
}

const Vector3 & Plant::getPosition()
{
	return v_geomMan->getPosition();
}


//PlantBuilder * Plant::instanciatePlantBuilder()
//{
//	return new PlantBuilder(this);
//}

void Plant::seedPlant (float shift)
{
}

void Plant::init()
{	/*add init function*/
	v_pltBuilder=instanciatePlantBuilder();

}

void Plant::computeGeometry(bool silent)
{
	/* launch geom computing */
	v_geomMan->computeTreeGeometry(silent);

}


void Plant::deserializeFrom(const std::string & serialName)
{

	FileArchive ar( serialName.c_str(), "rb" );

	SArchiveMaps sar (ar);

	ArchivableObject * arObj;
	LoadPtr(ar,arObj);	//ici on recupere juste l'id du ptr, avt de charge l'objet
	AddPtr(ar,(ArchivableObject *)this);
	ar>>*this;

}

void Plant::run (float timeShift, float timeStart, float timeStop)
{

	//ObserverManager omgr;
	//omgr.scanForExternalObservers(this);

	if(timeStop != -1)
		Scheduler::instance()->setHotStop(timeStop);

	if(timeStart != -1)
		getConfigData().hotStart=timeStart;

	//si on demarre la simulation d�s l'�tat de graine
	if(!getConfigData().hotStart)
	{
		getSignalInterface().sigPlant(this);

		getConfigData().initRandomGenerator();
		seedPlant(timeShift);

	}
	//std::cout<<"Run"<<std::endl;

	//getScheduler().run_simulation();
}

void Plant::setSerialization(double time)
{

	v_serial->setDate(time);

}

void Plant::serialize(Archive& ar )
{


	if( ar.isWriting() )
	{
		ar <<*v_config;
//		ar <<v_nameTree<<v_paramFileName<<*v_topoMan;
		ar <<*v_actionScheduler;

	}
	else
	{
		ar >>*v_config;
//		ar >>v_nameTree>>v_paramFileName>>*v_topoMan;
		ar >>*v_actionScheduler;
	}

}

void Plant::setParamName (const std::string mod, const std::string & str)
{
	mapParameterFileName.insert(ParameterFileNamePair(mod, str));
}

std::string & Plant::getParamName (std::string mod)
{
	if (mapParameterFileName.find (mod) != mapParameterFileName.end())
	{
		return mapParameterFileName.find (mod)->second;
	}
}

DecompAxeLevel *Plant::getDecompAxeLevel (int id)
{
	DecompAxeLevel *d;

	Branc *b = (Branc*)getTopology().seekTree(TopoManager::RESET);
	while (b)
	{
		if ((d = b->getDecompAxeLevel (id)) != NULL)
			return d;

		b = (Branc*)getTopology().seekTree(TopoManager::NEXT);
	}
	return NULL;

}

