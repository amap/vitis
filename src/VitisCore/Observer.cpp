#include "Observer.h"
#include <iostream>


short  Observer::o_cptid=0;


Observer::Observer()
{
	o_id=o_cptid;

	o_cptid++;

}



bool Observer::update(Subject * , void **)
{
	std::cout<<"Observer "<<o_id<<" doesn't provide an update method !"<<std::endl;
	return false;
}
