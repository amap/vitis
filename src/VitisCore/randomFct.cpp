/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "randomFct.h"
#include "VitisConfig.h"
#include <math.h>


/**< random number lists */
/* 2 state markov automaton */
long markovSimulation2(Plant * plt, long old_state , /* previous state */
		float proba_in , /* initial proba state 1 */
		float proba1 ,   /* state 1 to 2 proba */
		float proba2,    /* state 2 to 1 proba */
		long ) /* old fashion */

{
	register long i = 0;
	float limit , cumul[2];


	/* construction fonction de repartition */

	switch (old_state) {
		case 0 :
			cumul[0] = proba1;
			break;
		case 1 :
		case 2 :
			cumul[0] = (float)1. - proba2;
			break;
		default :
			cumul[0] = proba_in;
			break;
	}

	if (cumul[0] <= 0.0)
		return (1);
	else if (cumul[0] >= 1.)
		return (0);

	cumul[1] = 1.;

	/* simulation */

	limit = (float)plt->getConfigData().getRandomGenerator()->Random();

	while (cumul[i] <= limit) {
		i++;
	}

	return(i);
}

/* general markov automaton whith n state slightly adjusted to 3 stste */
long  cumulativeLaw(Plant * plt, float *proba , /* transition probabilities */
		long nbstate,    /* number of state */
		long ) /* old fashioned */

{
	register long i = 0;
	float limit , cumul[10];


	/* construction fonction de repartition */

	cumul[0] = *proba;
	for (i=1; i< nbstate; i++)
		cumul[i] = cumul[i-1] + *(proba+i);

	/* some test on the 3 first values to avoid heavy
	 * computing in simple cases
	 * */
	if (cumul[0] == 1.)
		return (0);
	else if (   cumul[0] == 0.
			&& cumul[1] == 1.)
		return (1);
	else if (   cumul[0] == 0.
			&& cumul[1] == 0.
			&& cumul[2] == 1.)
		return (2);

	/* simulation */
	limit =(float)plt->getConfigData().getRandomGenerator()->Random();

	i = 0;
	while (cumul[i] <= limit) {
		i++;
	}

	return(i);
}

/* simple Bernouilli law */
long bernouilli (Plant * plt, float p) /* proba */
{
	float t;

	if (p <= 0.0)
		return (0);
	else if (p >= 1.0)
		return (1);

	t = (float)plt->getConfigData().getRandomGenerator()->Random();

	if (t>=p)
		return(0);
	else
		return(1);
}

/* shift negative binomial law simulation */
long binodec (Plant * plt,
		float n,  /* test number if n<0 then negative law */
		float p, /* probability */
		long d)  /* shift */
{
	long succes, t, i;

	succes = t = 0;
	/* positive law, counts the number of success */
	if (n>=0)
	{
		if (n - (int)n >= .5)
			n = (int)n +1;
		else
			n = (int)n;
		for (i=0; i<n; i++)
			if (bernouilli(plt,p))
				t++;
	}
	/* negative law, counts number of failure until number of success has been reached */
	else
	{
		return binodecf (plt, -n, p, d);
/*
		if (n < 0)
			n = -n;

		if (p == 0)
			t = -1;
		else do
		{
			if (bernouilli(plt,p))
				succes++;
			t++;
		} while (succes<n);
		t -= succes;
*/
	}

	return (t+d);
}

long binodecf (Plant *plt,
		float parameter,  /* test number if n<0 then negative law */
              float probability, /* probability */
              long inf_bound)  /* shift */
{
  register int i;
  double failure = 1. - probability , success = probability , log_failure ,
         set , subset , scale , term , pcumul;
  double cumul_threshold;


    // valeurs de probabilite nulle avant la borne inferieure

	 i = inf_bound;

    subset = parameter - 1.;
    set = subset;

	 cumul_threshold = (float)plt->getConfigData().getRandomGenerator()->Random();

    // cas calcul direct

    if (sqrt(parameter) / success < 500) {

      // calcul de la probabilite de la borne inferieure

      term = pow(success , (double)parameter);
      pcumul = term;

      // calcul des probabilites des valeurs suivantes

      while ((pcumul < cumul_threshold) && (i < 5000))
		{
        set++;
        scale = set / (set - subset);
        term *= scale * failure;
        pcumul = pcumul + term;
        i++;
      }
    }

    // cas calcul en log

    else {

      // calcul de la probabilite de la borne inferieure

      term = parameter * log(success);
      pcumul = exp(term);

      // calcul des probabilites des valeurs suivantes

      log_failure = log(failure);

      while ((pcumul < cumul_threshold) && (i < 5000))
		{
        set++;
        scale = set / (set - subset);
        term += log(scale) + log_failure;
        pcumul = pcumul + term;
        i++;
      }
    }

	 return i;
}


