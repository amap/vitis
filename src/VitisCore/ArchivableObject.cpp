/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "ArchivableObject.h"

using namespace Vitis;


Archive& Vitis :: operator <<( Archive& ar, ArchivableObject& o )
{
	if( !ar.isWriting() )
	{
		//o.error( 0, "<<", "Archive is not open for writing.\n" );
		return ar;
	}
	o.serialize( ar );

	if (o.additionalDataNumber())
	{
		for (int i=0; i < o.additionalDataNumber(); i++)
		{
			if (o.isAdditionalDataArchivable(i))
			{
				ArchivableObject *a = (ArchivableObject*)o.getAdditionalData(i);
				a->serialize(ar);
			}
		}
	}

	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, ArchivableObject& o )
{
	if( ar.isWriting() )
	{
		// o.error( 0, ">>", "Archive is not open for reading.\n" );
		return ar;
	}
	o.serialize( ar );

	if (o.additionalDataNumber())
	{
		for (int i=0; i < o.additionalDataNumber(); i++)
		{
			if (o.isAdditionalDataArchivable(i))
			{
				ArchivableObject *a = (ArchivableObject*)o.getAdditionalData(i);
				a->serialize(ar);
			}
		}
	}

	return ar;
}


//return 1 if the ptr need to be save (he never be saved before)
int ArchivableObject::SavePtr(Archive& ar, ArchivableObject* pE)  //saves a pointer
{
	SArchiveMaps* pM = SArchiveMaps::FromArchive(ar);
	return pM->Save(pE);
}


//return 1 if the ptr need to be Add
int ArchivableObject::LoadPtr(Archive& ar, ArchivableObject*& pE)   //loads a pointer
{
	SArchiveMaps* pM = SArchiveMaps::FromArchive(ar);
	return pM->Load(pE);
}


void ArchivableObject::AddPtr(Archive& ar, ArchivableObject* pE)   //add map pointer
{
	SArchiveMaps* pM = SArchiveMaps::FromArchive(ar);
	pM->MapLoadObject(pE);

}


//initiator
std::map<Archive*, SArchiveMaps*> SArchiveMaps::_s_armaps;


SArchiveMaps::SArchiveMaps(Archive& ar)
{
	//put in the map
	_pAr = &ar;
	_s_armaps[&ar] = this;
	_nID = 0;

}

SArchiveMaps::~SArchiveMaps()
{
	//unmap
	_s_armaps.erase(_pAr);
}


SArchiveMaps* SArchiveMaps::FromArchive(Archive& ar)
{
	std::map<Archive*, SArchiveMaps*>::iterator i;
	i = _s_armaps.find(&ar);
	if(i == _s_armaps.end())
		return NULL;
	return i->second;
}


USint SArchiveMaps::MapStoreObject(void * pE)
{

	++_nID;
	_storemap[pE] = _nID;

	return _nID;
}

void SArchiveMaps::MapLoadObject(void * pE)
{

	_loadmap[_nID] = pE;

}




