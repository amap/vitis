/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "Archive.h"
#include "defVitis.h"
#include "UtilMath.h"
#include "Matrix.h"
#include <stdio.h>

using namespace Vitis;
using namespace std;


Archive :: Archive()
{
	_bigEndian = isMachineBigEndian();
	_saving = false;

	int s = (int)sizeof(char);
	if( s != 1 )
		printf( "Critical Error in Archive::Archive: sizeof(char) is not 1. Change Archive.cpp and recompile.\n" );
	s = (int)sizeof(short);
	if( s != 2 )
		printf( "Critical Error in Archive::Archive: sizeof(short) is not 2. Change Archive.cpp and recompile.\n" );
	s = (int)sizeof(int);
	if( s != 4 )
		printf( "Critical Error in Archive::Archive: sizeof(int) is not 4. Change Archive.cpp and recompile.\n" );
	s = (int)sizeof(float);
	if( s != 4 )
		printf( "Critical Error in Archive::Archive: sizeof(float) is not 4. Change Archive.cpp and recompile.\n" );
}

Archive :: ~Archive()
{
}

bool Archive :: write( const void* p, int size )
{
	if( !_saving )
	{
		//error( 0, "write", "Archive not open for writing.\n" );
		_error = true;
	}
	else
		_error = onWrite( p, size );
	return _error;
}

bool Archive :: read( void* p, int size )
{
	if( _saving )
	{
		//error( 0, "write", "Archive not open for reading.\n" );
		_error = true;
	}
	else
		_error = onRead( p, size );
	return _error;
}

/////////////////////////////////////////////////////
//    OUTPUT
/////////////////////////////////////////////////////

Archive& Vitis :: operator <<( Archive& ar, char c )
{
	ar.write( &c, sizeof(char) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, signed char c )
{
	ar.write( &c, sizeof(char) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, unsigned char c )
{
	ar.write( &c, sizeof(char) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, short n )
{
	if( !ar.isBigEndian() )
		n = reverse_short( n );
	ar.write( &n, sizeof(short) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, int n )
{
	if( !ar.isBigEndian() )
		n = reverse_int( n );
	ar.write( &n, sizeof(int) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, long n )
{
	if( !ar.isBigEndian() )
		reverseByteOrder( (char*)&n, sizeof(long) );
	ar.write( &n, sizeof(long) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, unsigned short n )
{
	if( !ar.isBigEndian() )
		n = reverse_short( n );
	ar.write( &n, sizeof(short) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, unsigned int n )
{
	if( !ar.isBigEndian() )
		n = reverse_int( n );
	ar.write( &n, sizeof(int) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, unsigned long n )
{
	if( !ar.isBigEndian() )
		reverseByteOrder( (char*)&n, sizeof(long) );
	ar.write( &n, sizeof(long) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, float f )
{
	if( !ar.isBigEndian() )
		reverseByteOrder( (char*)&f, sizeof(float) );
	ar.write( &f, sizeof(float) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, double f )
{
	if( !ar.isBigEndian() )
		reverseByteOrder( (char*)&f, sizeof(double) );
	ar.write( &f, sizeof(double) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, bool n )
{
	unsigned char val = n;
	ar.write( &val, sizeof(unsigned char) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, const void* p )
{
	if( !ar.isBigEndian() )
		reverseByteOrder( (char*)&p, sizeof(void*) );
	ar.write( (&p), sizeof(void*) );
	return ar;
}

Archive& Vitis :: operator <<( Archive& ar, string s )
{
	unsigned int n = s.length() + 1;
	ar << n;
	ar.write( s.c_str(), n );
	return ar;
}

/////////////////////////////////////////////////////
//    INPUT
/////////////////////////////////////////////////////

Archive& Vitis :: operator >>( Archive& ar, char& c )
{
	ar.read( &c, sizeof(char) );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, signed char& c )
{
	ar.read( &c, sizeof(char) );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, unsigned char& c )
{
	ar.read( &c, sizeof(char) );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, short& n )
{
	ar.read( &n, sizeof(short) );
	if( !ar.isBigEndian() )
		n = reverse_short( n );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, int& n )
{
	ar.read( &n, sizeof(int) );
	if( !ar.isBigEndian() )
		n = reverse_int( n );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, long& n )
{
	ar.read( &n, sizeof(long) );
	if( !ar.isBigEndian() )
		reverseByteOrder( (char*)&n, sizeof(long) );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, unsigned short& n )
{
	ar.read( &n, sizeof(short) );
	if( !ar.isBigEndian() )
		n = reverse_short( n );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, unsigned int& n )
{
	ar.read( &n, sizeof(int) );
	if( !ar.isBigEndian() )
		n = reverse_int( n );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, unsigned long& n )
{
	ar.read( &n, sizeof(long) );
	if( !ar.isBigEndian() )
		reverseByteOrder( (char*)&n, sizeof(long) );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, float& f )
{
	ar.read( &f, sizeof(float) );
	if( !ar.isBigEndian() )
		reverseByteOrder( (char*)&f, sizeof(float) );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, double& f )
{
	ar.read( &f, sizeof(double) );
	if( !ar.isBigEndian() )
		reverseByteOrder( (char*)&f, sizeof(double) );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, bool& n )
{
	unsigned char val;
	ar.read( &val, sizeof(unsigned char) );
	n = (bool)val;
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, void* &p )
{
	ar.read( (&p), sizeof(void*) );
	if( !ar.isBigEndian() )
		reverseByteOrder( (char*)&p, sizeof(void*) );
	return ar;
}

Archive& Vitis :: operator >>( Archive& ar, string& s )
{
	unsigned int n;
	ar >> n;
	char* buf = (char*)malloc( n );
	if( buf )
	{
		ar.read( buf, n );
		s = buf;
		free( buf );
	}
	return ar;
}
