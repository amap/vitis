/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "Vector.h"
#include "UtilMath.h"

#include <iostream>

#include "Geometry/Point4f.h"

using namespace std;



namespace Vitis
{

#define __X __T[0]
#define __Y __T[1]
#define __Z __T[2]
#define __W __T[3]


	/*  --------------------------------------------------------------------- */

	const Vector3 Vector3::ORIGIN;


	/*  --------------------------------------------------------------------- */

	Vector3::Vector3( const float& x,
			const float& y,
			const float& z ) :
		Tuple3<float>(x,y,z) {
			//GEOM_ASSERT(isValid());
		}

	Vector3::Vector3( const float * v3 ) :
		Tuple3<float>(v3) {
			//GEOM_ASSERT(isValid());
		}




	void
		Vector3::set( const float& x,
				const float& y,
				const float& z )  {
			__X = x;
			__Y = y;
			__Z = z;
			// GEOM_ASSERT(isValid());
		}

	void
		Vector3::set( const float * v3 ) {
			__X = v3[0];
			__Y = v3[1];
			__Z = v3[2];
			//GEOM_ASSERT(isValid());
		}


	void
		Vector3::set( const Vector3& v )  {
			__X = v.__X;
			__Y = v.__Y;
			__Z = v.__Z;
			//GEOM_ASSERT(isValid());
		}

	Vector3::~Vector3( ) {
	}

	const float&
		Vector3::x( ) const {
			return __X;
		}

	float&
		Vector3::x( ) {
			return __X;
		}

	const float&
		Vector3::y( ) const {
			return __Y;
		}

	float&
		Vector3::y( ) {
			return __Y;
		}

	const float&
		Vector3::z( ) const {
			return __Z;
		}

	float&
		Vector3::z( ) {
			return __Z;
		}

	/*  --------------------------------------------------------------------- */

	bool
		Vector3::operator==( const Vector3& v ) const {
			return normLinf(*this - v) < GEOM_TOLERANCE;
		}

	bool
		Vector3::operator!=( const Vector3& v ) const {
			return normLinf(*this - v) >= GEOM_TOLERANCE;
		}

	Vector3&
		Vector3::operator+=( const Vector3& v ) {
			__X += v.__X; __Y += v.__Y; __Z += v.__Z;
			return *this;
		}
	Vector3&
		Vector3::operator-=( const Vector3& v ) {
			__X -= v.__X; __Y -= v.__Y; __Z -= v.__Z;
			return *this;
		}
	Vector3&
		Vector3::operator*=( const float& s ) {
			__X *= s; __Y *= s; __Z *= s;
			return *this;
		}

	Vector3&
		Vector3::operator/=( const float& s ) {
			//GEOM_ASSERT(fabs(s) > GEOM_TOLERANCE);
			__X /= s; __Y /= s; __Z /= s;
			return *this;
		}

	Vector3
		Vector3::operator-( ) const {
			return Vector3(-__X, -__Y, -__Z);
		}

	Vector3 Vector3::operator+( const Vector3& v) const {
		return Vector3(*this) += v;
	}

	Vector3 Vector3::operator-( const Vector3& v ) const {
		return Vector3(*this) -= v;
	}

	bool
		Vector3::isNormalized( ) const {
			return fabs(normSquared(*this) - 1) < EPSILON;
		}

	bool
		Vector3::isOrthogonalTo( const Vector3& v ) const {
			return fabs(dot(*this,v)) < EPSILON;
		}

	bool
		Vector3::isValid( ) const {
			return finite(__X) && finite(__Y) && finite(__Z);
		}

	float
		Vector3::normalize( )  {
			float _norm = norm(*this);
			if (_norm > GEOM_TOLERANCE)
				*this /= _norm;
			return _norm;
		}


	float Vector3::distanceSquared(Vector3 p1) {
		float dx = __X - p1.x();
		float dy = __Y - p1.y();
		float dz = __Z - p1.z();
		return dx * dx + dy * dy + dz * dz;
	}

	float Vector3::distance(Vector3 p1) {
		float dx = __X - p1.x();
		float dy = __Y - p1.y();
		float dz = __Z - p1.z();
		return (float)std::sqrt((double)(dx * dx + dy * dy + dz * dz));
	}

	float Vector3::distanceL1(Vector3 p1) {
		return std::abs(__X - p1.x()) + std::abs(__Y - p1.y()) + std::abs(__Z - p1.z());
	}

	float Vector3::distanceLinf(Vector3 p1) {
		float tmp = std::max(std::abs(__X - p1.x()), std::abs(__Y - p1.y()));
		return std::max(tmp, std::abs(__Z - p1.z()));
	}

	void Vector3::project( Point4f p1) {
		float oneOw = 1.0F / p1.w;
		__X = p1.x * oneOw;
		__Y = p1.y * oneOw;
		__Z = p1.z * oneOw;
	}

	void Vector3::add(Vector3 p) {
		__X += p.x();
		__Y += p.y();
		__Z += p.z();
	}

	int
		Vector3::getMaxAbsCoord() const
		{
			if( fabs(__X) > fabs(__Y) )
				return ( fabs(__X) > fabs(__Z) )  ? 0 : 2;
			else
				return ( fabs(__Y) > fabs(__Z) ) ? 1 : 2;
		}

	int
		Vector3::getMinAbsCoord() const
		{
			if( fabs(__X) < fabs(__Y) )
				return ( fabs(__X) < fabs(__Z) )  ? 0 : 2;
			else
				return ( fabs(__Y) < fabs(__Z) ) ? 1 : 2;
		}

	Vector3 Vector3::axisRotation (Vector3 axe, Vector3 vin, float ang)
	{
		Vector3 vaux;

		float cosa, cosf, sina;
		float ax11, ax12, ax13, ax22, ax23, ax33, ax1, ax2, ax3;

		cosa = cos(ang);
		cosf = (float)1.0 - cosa;
		sina = sin(ang);
		ax11 = axe.x() * axe.x() * cosf;
		ax12 = axe.x() * axe.y() * cosf;
		ax13 = axe.x() * axe.z() * cosf;
		ax22 = axe.y() * axe.y() * cosf;
		ax23 = axe.y() * axe.z() * cosf;
		ax33 = axe.z() * axe.z() * cosf;
		ax1 = axe.x() * sina;
		ax2 = axe.y() * sina;
		ax3 = axe.z() * sina;
		vaux.x() = (ax11 + cosa) * vin.x() + (ax12 - ax3) * vin.y() + (ax13 + ax2) * vin.z();
		vaux.y() = (ax12 + ax3) * vin.x() + (ax22 + cosa) * vin.y() + (ax23 - ax1) * vin.z();
		vaux.z() = (ax13 - ax2) * vin.x() + (ax23 + ax1) * vin.y() + (ax33  + cosa) * vin.z();

		return vaux;
	}

	void Vector3::serialize(Archive& ar )
	{
		if( ar.isWriting() )
		{
			ar <<__X<< __Y<< __Z;

		}
		else
		{
			ar >>__X>> __Y>> __Z;
		}


	}

	/*  --------------------------------------------------------------------- */

	Vector3 operator*( const Vector3& v, const float& s ) {
		return Vector3(v) *= s;
	}

	Vector3 operator/( const Vector3& v, const float& s ) {
		// GEOM_ASSERT(fabs(s) > GEOM_TOLERANCE);
		return Vector3(v) /= s;
	}

	ostream& operator<<( ostream& stream, const Vector3& v ) {
		return stream << "<" << v.__X << "," << v.__Y << "," << v.__Z << ">";
	}

	bool operator<(const Vector3& v1, const Vector3& v2)
	{
		if(v1.__X < v2.__X)
		{
			return true;
		}
		if(v1.__X > v2.__X)
		{
			return false;
		}
		if(v1.__Y < v2.__Y)
		{
			return true;
		}
		if(v1.__Y > v2.__Y)
		{
			return false;
		}
		return v1.__Z < v2.__Z;
	}

	Vector3 abs( const Vector3& v ) {
		return Vector3(fabs(v.__X),fabs(v.__Y),fabs(v.__Z));
	}

	Vector3 cross( const Vector3& v1, const Vector3& v2 ) {
		return Vector3(v1.__Y * v2.__Z - v1.__Z * v2.__Y,
				v1.__Z * v2.__X - v1.__X * v2.__Z,
				v1.__X * v2.__Y - v1.__Y * v2.__X);
	}

	Vector3 operator^( const Vector3& v1, const Vector3& v2 ) {
		return Vector3(v1.__Y * v2.__Z - v1.__Z * v2.__Y,
				v1.__Z * v2.__X - v1.__X * v2.__Z,
				v1.__X * v2.__Y - v1.__Y * v2.__X);
	}

	Vector3 direction( const Vector3& v ) {
		return v / norm(v);
	}

	float dot( const Vector3& v1, const Vector3& v2 ) {
		return (v1.__X * v2.__X + v1.__Y * v2.__Y + v1.__Z * v2.__Z);
	}

	float operator*( const Vector3& v1, const Vector3& v2 ) {
		return (v1.__X * v2.__X + v1.__Y * v2.__Y + v1.__Z * v2.__Z);
	}

	Vector3 Max( const Vector3& v1, const Vector3& v2 ) {
		return Vector3(max(v1.__X,v2.__X),
				max(v1.__Y,v2.__Y),
				max(v1.__Z,v2.__Z));
	}

	Vector3 Min( const Vector3& v1, const Vector3& v2 ) {
		return Vector3(min(v1.__X,v2.__X),
				min(v1.__Y,v2.__Y),
				min(v1.__Z,v2.__Z));
	}

	float norm( const Vector3& v ) {
		return sqrt(normSquared(v));
	}


	float normLinf( const Vector3& v ) {
		return *(abs(v).getMax());
	}

	float normSquared( const Vector3& v ) {
		return dot(v,v);
	}



	float angle( const Vector3& v1 , const Vector3& v2 ) {
		float n = norm(v1)*norm(v2);
		if(n < EPSILON)
			return 0;
		float s = dot(v1,v2);
		if (s/n > 1)
			s = n;
		if (s/n < -1)
			s = -n;
		return acos(s/n);
	}




	/*  --------------------------------------------------------------------- */

	const Vector4 Vector4::ORIGIN;


	/*  --------------------------------------------------------------------- */

	Vector4::Vector4( const float& x , const float& y ,
			const float& z , const float& w ) :
		Tuple4<float>(x,y,z,w) {

		}

	Vector4::Vector4( const float * v4 ) :
		Tuple4<float>(v4) {

		}

	Vector4::Vector4( const Vector3& v, const float& w ) :
		Tuple4<float>(v.x(),v.y(),v.z(),w) {

		}

	Vector4::~Vector4( ) {
	}

	void
		Vector4::set( const float& x , const float& y ,
				const float& z , const float& w ) {
			__X = x;
			__Y = y;
			__Z = z;
			__W = w;

		}

	void
		Vector4::set( const float * v4 ) {
			__X = v4[0];
			__Y = v4[1];
			__Z = v4[2];
			__W = v4[3];

		}

	void
		Vector4::set( const Vector3& v, const float& w ) {

			__X = v.x();
			__Y = v.y();
			__Z = v.z();
			__W = w;

		}

	void
		Vector4::set( const Vector4& v) {

			__X = v.__X;
			__Y = v.__Y;
			__Z = v.__Z;
			__W = v.__W;

		}

	const float& Vector4::x( ) const {
		return __X;
	}

	float& Vector4::x( ) {
		return __X;
	}

	const float& Vector4::y( ) const {
		return __Y;
	}

	float& Vector4::y( ) {
		return __Y;
	}

	const float& Vector4::z( ) const {
		return __Z;
	}

	float& Vector4::z( ) {
		return __Z;
	}

	const float& Vector4::w( ) const {
		return __W;
	}

	float& Vector4::w( ) {
		return __W;
	}

	/*  --------------------------------------------------------------------- */

	bool Vector4::operator==( const Vector4& v ) const {
		return normLinf(*this - v) < GEOM_TOLERANCE;
	}

	bool Vector4::operator!=( const Vector4& v ) const {
		return normLinf(*this - v) >= GEOM_TOLERANCE;
	}

	Vector4& Vector4::operator+=( const Vector4& v ) {
		__X += v.__X; __Y += v.__Y; __Z += v.__Z; __W += v.__W;
		return *this;
	}

	Vector4& Vector4::operator-=( const Vector4& v ) {
		__X -= v.__X; __Y -= v.__Y; __Z -= v.__Z; __W -= v.__W;
		return *this;
	}

	Vector4& Vector4::operator*=( const float& s ) {
		__X *= s; __Y *= s; __Z *= s; __W *= s;
		return *this;
	}

	Vector4& Vector4::operator/=( const float& s ) {

		__X /= s; __Y /= s; __Z /= s; __W /= s;
		return *this;
	}

	Vector4 Vector4::operator-( ) const {
		return Vector4(-__X, -__Y, -__Z,-__W);
	}

	Vector4 Vector4::operator+( const Vector4& v) const {
		return Vector4(*this) += v;
	}

	Vector4 Vector4::operator-( const Vector4& v ) const {
		return Vector4(*this) -= v;
	}

	bool Vector4::isNormalized( ) const {
		return fabs(normSquared(*this) - 1) < EPSILON;
	}

	bool Vector4::isOrthogonalTo( const Vector4& v ) const {
		return fabs(dot(*this,v)) < EPSILON;
	}

	bool Vector4::isValid( ) const {
		return
			finite(__X) && finite(__Y) &&
			finite(__Z) && finite(__W);
	}

	float Vector4::normalize( ) {
		float _norm = norm(*this);
		if (_norm > GEOM_TOLERANCE) *this /= _norm;
		return _norm;
	}

	Vector3 Vector4::project( ) const {
		if(__W==0)return Vector3(__X , __Y , __Z );
		return Vector3(__X / __W, __Y / __W, __Z / __W);
	}

	/*  --------------------------------------------------------------------- */

	Vector4 operator*( const Vector4& v, const float& s ) {
		return Vector4(v) *= s;
	}

	Vector4 operator/( const Vector4& v, const float& s ) {

		return Vector4(v) /= s;
	}

	ostream& operator<<( ostream& stream, const Vector4& v ) {
		return stream << "<" << v.__X << "," << v.__Y << "," << v.__Z << "," << v.__W << ">";
	}

	bool operator<(const Vector4& v1, const Vector4& v2)
	{
		if(v1.__X < v2.__X)
		{
			return true;
		}
		if(v1.__X > v2.__X)
		{
			return false;
		}
		if(v1.__Y < v2.__Y)
		{
			return true;
		}
		if(v1.__Y > v2.__Y)
		{
			return false;
		}
		if(v1.__Z < v2.__Z)
		{
			return true;
		}
		if(v1.__Z > v2.__Z)
		{
			return false;
		}
		return v1.__W < v2.__W;
	}

	Vector4 abs( const Vector4& v ) {
		return Vector4(fabs(v.__X),fabs(v.__Y),
				fabs(v.__Z),fabs(v.__W));
	}

	Vector4 direction( const Vector4& v ) {
		return v / norm(v);
	}

	float dot( const Vector4& v1, const Vector4& v2 ) {
		return
			v1.__X * v2.__X + v1.__Y * v2.__Y +
			v1.__Z * v2.__Z + v1.__W * v2.__W;
	}

	float operator*( const Vector4& v1, const Vector4& v2 ) {
		return
			v1.__X * v2.__X + v1.__Y * v2.__Y +
			v1.__Z * v2.__Z + v1.__W * v2.__W;
	}

	Vector4 Max( const Vector4& v1, const Vector4& v2 ) {
		return Vector4(max(v1.__X,v2.__X),max(v1.__Y,v2.__Y),
				max(v1.__Z,v2.__Z),max(v1.__W,v2.__W));
	}

	Vector4 Min( const Vector4& v1, const Vector4& v2 ) {
		return Vector4(min(v1.__X,v2.__X),min(v1.__Y,v2.__Y),
				min(v1.__Z,v2.__Z),min(v1.__W,v2.__W));
	}

	float norm( const Vector4& v ) {
		return sqrt(normSquared(v));
	}



	float normLinf( const Vector4& v ) {
		return *(abs(v).getMax());
	}

	float normSquared( const Vector4& v ) {
		return dot(v,v);
	}

}; //end namespace

