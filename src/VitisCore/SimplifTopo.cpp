/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "SimplifTopo.h"
#include "Plant.h"
#include <iostream>
#include "DebugNew.h"


/////////////////////////////
//Construction Liste
//////////////////////////////
ListeBr::ListeBr(Branc * b)
{
	brancPtr.push_back(b);
}

ListeBr::~ListeBr()
{
	//TODO si on ve simplifier des branches ki ont d�j� �t� prun�s fo garder une copie ds la map
	/*std::vector<Branc *>::iterator it;

	  for(it=brancPtr.begin(); it!=brancPtr.end(); it++)
	  {
	  delete *it;
	  }
	  */
}


SimplifTopo::SimplifTopo(Plant * plt)
{
	listeCrit.clear();
	v_plt=plt;
}

SimplifTopo::~SimplifTopo(void)
{
	MapCrit::iterator it;

	for(it=listeCrit.begin(); it!=listeCrit.end(); it++)
	{
		delete it->second;
	}

}


void SimplifTopo::removeSimplifiedBranc(Branc *b, const std::string & critere)
{
	if (critere.empty()) return;

	MapCrit::const_iterator p;

	p=listeCrit.find(critere);

	if (p!=listeCrit.end())
	{
		for (unsigned int i=0; i<p->second->brancPtr.size(); i++)
		{
			if (p->second->brancPtr[i] == b)
				p->second->brancPtr[i] = NULL;
		}
	}
}

Branc * SimplifTopo::getSimplifiedBranc(const std::string & critere, int indexInCriteria)
{

	Branc * b;

	if(v_plt->getConfigData().cfg_topoSimp==NO_SIMP)
		return NULL;


	MapCrit::const_iterator p;

	p=listeCrit.find(critere);


	if(p!=listeCrit.end())
	{

		if((unsigned)indexInCriteria>=p->second->brancPtr.size())
			return NULL;


		b= p->second->brancPtr[indexInCriteria];

		// there is already a branc for that index -> set up hierarchy and return the bud
		return b;
	}


	return NULL;


}

void SimplifTopo::updateListe(Branc * b,const std::string & critere)
{

	MapCrit::const_iterator p;
	p=listeCrit.find(critere);
	if(p!=listeCrit.end())
		p->second->brancPtr.push_back(b);
	else
		listeCrit[critere]=new ListeBr(b);


}

