/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <cstring>
#include <stdio.h>
#include <math.h>
#include <fstream>
#include "Voxel.h"

void Voxel::addVoxelData (std::string criteria, VoxelData *d)
{
	if (!data.empty())
	{
		MapVoxelData::const_iterator p=data.find(criteria);

		if (p == data.end())
		{
			data.insert(MapVoxelDataPair(criteria, d));
		}
	}
	else
	{
		data.insert(MapVoxelDataPair(criteria, d));
	}
}

VoxelData *Voxel::getVoxelData (int ind )
{
	if (   data.empty()
		|| data.size() <= ind)
		return NULL;

	MapVoxelData::const_iterator p;

	int i;
	for (p=data.begin(),i=0; i<ind; p++,i++);

	return p->second;
}

VoxelData *Voxel::getVoxelData (std::string criteria )
{
	if (data.empty())
		return NULL;

	MapVoxelData::const_iterator p;

	p=data.find(criteria);

	if (p != data.end())
		return p->second;
	else
		return NULL;
}

void Voxel::copyData (MapVoxelData m)
{
	if (m.empty())
        return;

	MapVoxelData::const_iterator p;
	MapVoxelDataPair mp;
	VoxelData *vp;

	for (p=m.begin(); p!=m.end(); p++)
    {
        mp = *p;
        vp = (VoxelData *)mp.second;
        addVoxelData(mp.first, vp->duplicate());
    }
}

float Voxel::ContainedSurface()
{
    int i, j, k;
    VoxelSpace::instance()->voxelCoordinates(this, &i, &j, &k);
    return ContainedSurface(i,j,k);
}

float Voxel::ContainedSurface(int i, int j, int k)
{
    if (containedSurface != 0)
        return containedSurface;

    if (content.size() == 0)
    {
        containedSurface = 0;
        return 0;
    }

    float surface = 0, length, diam;
 	vector<GeomElemCone*>::iterator it;
 	vector<float>::iterator itp;
 	float voxelSize = VoxelSpace::instance()->VoxelSize();

    for (it=content.begin(),itp=part.begin(); it!=content.end(); it++)
    {
        GeomElemCone *g = *it;

        if (   !part.empty()
            && itp != part.end())
		{
            length = g->getLength() * *itp;
			itp++;
		}
        else
            length = g->getLength();

        diam = g->getBottomDiam();
        if (diam > voxelSize)
            diam = voxelSize;

        surface += length * diam / 2;
    }
    containedSurface = surface;
    return surface;
}
void VoxelSpace::reset()
{
	if (space)
	{
//		for (int i=0; i<xSize; i++)
//		{
//			if (space[i])
//			{
//				for (int j=0; j<ySize; j++)
//				{
//					if (space[i][j])
//					{
//						for (int k=0; k<zSize; k++)
//						{
//							if (space[i][j][k].data != 0)
//								delete space[i][j][k].data;
//						}
//						delete []space[i][j];
//					}
//				}
//				delete []space[i];
//			}
//		}
		for (int i=0; i<zSize; i++)
		{
			if (space[i])
			{
				for (int j=0; j<xSize; j++)
				{
					if (space[i][j])
					{
						delete []space[i][j];
					}
				}
				delete []space[i];
			}
		}
		delete []space;
	}
	space = NULL;
	xSize = ySize= zSize = 0;
	nbPlant = 0;
}

void VoxelSpace::adjust (float xo, float yo, int is, int js, int ks)
{
    adjust (xo, yo, 0, is, js, ks);
}

void VoxelSpace::adjust (float xo, float yo, float zo, int is, int js, int ks)
{
	// new space
	if (space == NULL)
	{
		set (xo, yo, zo, is, js, ks);
		return;
	}

    if (   xo == xOrigin
        && yo == yOrigin
        && zo == zOrigin
        && is == xSize
        && js == ySize
        && ks == zSize)
        return;

	// space has to be adjusted

	Voxel ***spaceSav = space;
	float xOriginSav = xOrigin;
	float yOriginSav = yOrigin;
	float zOriginSav = zOrigin;
	float xend = xOrigin + xSize*voxelSize;
	float yend = yOrigin + ySize*voxelSize;
	float zend = zOrigin + zSize*voxelSize;
	int xSizeSav = xSize;
	int ySizeSav = ySize;
	int zSizeSav = zSize;

	// Compute the size of the merged space and create
	if (xOrigin > xo)
		xOrigin = xo;
	if (yOrigin > yo)
		yOrigin = yo;
	if (zOrigin > zo)
		zOrigin = zo;


	if (xend < xo + is*voxelSize)
		xend = xo + is*voxelSize;
	if (yend < yo + js*voxelSize)
		yend = yo + js*voxelSize;
	if (zend < zo + ks*voxelSize)
		zend = zo + ks*voxelSize;

	xSize = ceil (xend / voxelSize /*+ 0.5*/) - floor (xOrigin / voxelSize /*- 0.5*/);
	ySize = ceil (yend / voxelSize /*+ 0.5*/) - floor (yOrigin / voxelSize /*- 0.5*/);
	zSize = ceil (zend / voxelSize /*+ 0.5*/) - floor (zOrigin / voxelSize /*- 0.5*/);

	set (xOrigin, yOrigin, zOrigin, xSize, ySize, zSize);

	// Copy the content of the previous space
	int isav, jsav, ksav;
	findVoxel (xOriginSav+voxelSize/2, yOriginSav+voxelSize/2, zOriginSav+voxelSize/2, &isav, &jsav, &ksav);
	for (int k=0; k<zSizeSav; k++)
	for (int i=0; i<xSizeSav; i++)
	for (int j=0; j<ySizeSav; j++)
	{
		space[k+ksav][i+isav][j+jsav].content = spaceSav[k][i][j].content;
		space[k+ksav][i+isav][j+jsav].part = spaceSav[k][i][j].part;
		space[k+ksav][i+isav][j+jsav].copyData (spaceSav[k][i][j].data);
		space[k+ksav][i+isav][j+jsav].containedSurface = spaceSav[k][i][j].containedSurface;
	}

	// delete the previous space
	for (int i=0; i<zSizeSav; i++)
	{
		if (spaceSav[i])
		{
			for (int j=0; j<xSizeSav; j++)
			{
				if (spaceSav[i][j])
                {
					delete []spaceSav[i][j];
					spaceSav[i][j] = 0;
                }
			}
			delete []spaceSav[i];
			spaceSav[i] = 0;
		}
	}
	delete []spaceSav;
	spaceSav = 0;

}

void VoxelSpace::set (float xo, float yo, float zo, int is, int js, int ks)
{
	xOrigin = xo;
	yOrigin = yo;
	zOrigin = zo;

	xSize = is;
	ySize = js;
	zSize = ks;

//	space = (Voxel***) new Voxel**[xSize];
//	for (int i=0; i<xSize; i++)
//	{
//		space[i] = (Voxel**) new Voxel*[ySize];
//		for (int j=0; j<ySize; j++)
//		{
//			space[i][j] = (Voxel*) new Voxel[zSize];
//		}
//	}
	space = (Voxel***) new Voxel**[zSize];
	for (int i=0; i<zSize; i++)
	{
		space[i] = (Voxel**) new Voxel*[xSize];
		for (int j=0; j<xSize; j++)
		{
			space[i][j] = (Voxel*) new Voxel[ySize];
		}
	}
}

bool VoxelSpace::findVoxel (float x, float y, float z, int *i, int *j, int *k)
{
	*i = *j = *k = -1;
	*i = (x - xOrigin) / voxelSize;
	*j = (y - yOrigin) / voxelSize;
	*k = (z - zOrigin) / voxelSize;
//	for (int ii=0; ii<xSize; ii++)
//	{
//		if (x < xOrigin + ii*voxelSize)
//			continue;
//		else if (x <= xOrigin + (ii+1)*voxelSize)
//		{
//			for (int jj=0; jj<ySize; jj++)
//			{
//				if (y < yOrigin + jj*voxelSize)
//					continue;
//				else if (y <= yOrigin + (jj+1)*voxelSize)
//				{
//					for (int kk=0; kk<zSize; kk++)
//					{
//						if (z < kk*voxelSize)
//							continue;
//						if (z <= (kk+1)*voxelSize)
//						{
//							*k = kk;
//							break;
//						}
//					}
//					*j = jj;
//					break;
//				}
//			}
//			*i = ii;
//			break;
//		}
//	}

	if (   *i < 0
        || *i >= xSize
	    || *j < 0
        || *j >= ySize
	    || *k < 0
        || *k >= zSize
	    )
	{
//		if (*i == -1)
//			cout << endl<< x << " out of bound(x)" << endl;
//		if (*j == -1)
//			cout << endl<< y << " out of bound(y)" << endl;
//		if (*k == -1)
//			cout << endl<< z << " out of bound(z)" << endl;
		return false;
	}
	else
		return true;
}

// pompe sur http://roguebasin.roguelikedevelopment.org/index.php?title=Bresenham%27s_Line_Algorithm
void VoxelSpace::bresenham2D (int x1, int y1, int x2, int y2, int z, vector<Voxel*> *vv)
{
//cout << "de ("<<x1<<","<<y1<<") a ("<<x2<<","<<y2<<")"<<endl;
    int delta_x(x2 - x1);
    // if x1 == x2, then it does not matter what we set here
    signed char const ix((delta_x > 0) - (delta_x < 0));
    delta_x = abs(delta_x) << 1;

    int delta_y(y2 - y1);
    // if y1 == y2, then it does not matter what we set here
    signed char const iy((delta_y > 0) - (delta_y < 0));
    delta_y = abs(delta_y) << 1;

//cout << x1 <<" "<< y1  << endl;
    vv->push_back (&space[z][x1][y1]);

    if (delta_x >= delta_y)
    {
        // error may go below zero
        int error(delta_y - (delta_x >> 1));

        while (x1 != x2)
        {
            if ((error >= 0) && (error || (ix > 0)))
            {
                error -= delta_x;
                y1 += iy;
            }
            // else do nothing

            error += delta_y;
            x1 += ix;

//cout << x1 <<" "<< y1  << endl;
            vv->push_back (&space[z][x1][y1]);

        }
    }
    else
    {
        // error may go below zero
        int error(delta_x - (delta_y >> 1));

        while (y1 != y2)
        {
            if ((error >= 0) && (error || (iy > 0)))
            {
                error -= delta_y;
                x1 += ix;
            }
            // else do nothing

            error += delta_x;
            y1 += iy;

//cout << x1 <<" "<< y1  << endl;
            vv->push_back (&space[z][x1][y1]);

        }
    }
}

// pompe sur http://www.luberth.com/plotter/line3d.c.txt.html
/* find maximum of a and b */
#define MAXVAL(a,b) (((a)>(b))?(a):(b))

/* absolute value of a */
#define ABS(a) (((a)<0) ? -(a) : (a))

/* take sign of a, either -1, 0, or 1 */
#define ZSGN(a) (((a)<0) ? -1 : (a)>0 ? 1 : 0)

void VoxelSpace::bresenham3D (int x1, int y1, int z1, int x2, int y2, int z2, vector<Voxel*> *vv)
{
    int xd, yd, zd;
    int x, y, z;
    int ax, ay, az;
    int sx, sy, sz;
    int dx, dy, dz;

    dx = x2 - x1;
    dy = y2 - y1;
    dz = z2 - z1;

    ax = ABS(dx) << 1;
    ay = ABS(dy) << 1;
    az = ABS(dz) << 1;

    sx = ZSGN(dx);
    sy = ZSGN(dy);
    sz = ZSGN(dz);

    x = x1;
    y = y1;
    z = z1;

//cout << "de ("<<x1<<","<<y1<<","<<z1<<") a ("<<x2<<","<<y2<<","<<z2<<")"<<endl;
    if (ax >= MAXVAL(ay, az))            /* x dominant */
    {
        yd = ay - (ax >> 1);
        zd = az - (ax >> 1);
        for (;;)
        {
//cout << x <<" "<< y<<" " << z << endl;
	    if (x>=0 && y>=0 && z>=0)
            vv->push_back(&space[z][x][y]);
            if (x == x2)
            {
                return;
            }

            if (yd >= 0)
            {
                y += sy;
                yd -= ax;
            }

            if (zd >= 0)
            {
                z += sz;
                zd -= ax;
            }

            x += sx;
            yd += ay;
            zd += az;
        }
    }
    else if (ay >= MAXVAL(ax, az))            /* y dominant */
    {
        xd = ax - (ay >> 1);
        zd = az - (ay >> 1);
        for (;;)
        {
//cout << x <<" "<< y<<" " << z << endl;
	    if (x>=0 && y>=0 && z>=0)
            vv->push_back(&space[z][x][y]);
            if (y == y2)
            {
                return;
            }

            if (xd >= 0)
            {
                x += sx;
                xd -= ay;
            }

            if (zd >= 0)
            {
                z += sz;
                zd -= ay;
            }

            y += sy;
            xd += ax;
            zd += az;
        }
    }
    else if (az >= MAXVAL(ax, ay))            /* z dominant */
    {
        xd = ax - (az >> 1);
        yd = ay - (az >> 1);
        for (;;)
        {
//cout << x <<" "<< y<<" " << z << endl;
	    if (x>=0 && y>=0 && z>=0)
            vv->push_back(&space[z][x][y]);
            if (z == z2)
            {
                return;
            }

            if (xd >= 0)
            {
                x += sx;
                xd -= az;
            }

            if (yd >= 0)
            {
                y += sy;
                yd -= az;
            }

            z += sz;
            xd += ax;
            yd += ay;
        }
    }
}

void VoxelSpace::randomInitialVoxel(int *ib, int *jb, int *kb)
{
	// random height in the interesting part
	*kb = rand() % (zSize);

	// determine
	int i = rand() % (2*(xSize+ySize));
	if (i < xSize)
	{
		*ib = i;
		*jb = 0;
	}
	else if (i < xSize+ySize)
	{
		*ib = 0;
		*jb = i - xSize;
	}
	else if (i < 2*xSize+ySize)
	{
		*ib = i - (xSize+ySize);
		*jb = ySize-1;
	}
	else
	{
		*ib = xSize - 1;
		*jb = i - (2*xSize+ySize);
	}
//cout << "de "<<*ib<<" "<<*jb<<" "<<*kb<<endl;
}

vector<Voxel*> VoxelSpace::removeElem (GeomElemCone *e, GeomBranc *b)
{
	vector<GeomElemCone*>::iterator it;
	vector<float>::iterator itp;
	vector<Voxel*> v;
	for (int i=0; i<zSize; i++)
	for (int j=0; j<xSize; j++)
	for (int k=0; k<ySize; k++)
	{
		bool trouve=true;
		while (trouve)
		{
		trouve = false;
		for (it=space[i][j][k].content.begin(),itp=space[i][j][k].part.begin(); it!=space[i][j][k].content.end(); it++)
		{
			GeomElemCone *g = *it;
			if (e == g)
			{
				space[i][j][k].content.erase(it);
				if (   !space[i][j][k].part.empty()
                    && itp != space[i][j][k].part.end())
				{
                    space[i][j][k].part.erase(itp);
					itp++;
				}
				space[i][j][k].containedSurface = 0;
				v.push_back (&space[i][j][k]);
				trouve = true;
				break;
			}
			else if (g->getPtrBrc() == b)
			{
				space[i][j][k].content.erase(it);
				if (   !space[i][j][k].part.empty()
                    && itp != space[i][j][k].part.end())
				{
                    space[i][j][k].part.erase(itp);
					itp++;
				}
				space[i][j][k].containedSurface = 0;
				v.push_back (&space[i][j][k]);
				trouve = true;
				break;
			}
		}
		}
	}
	return v;
}

void VoxelSpace::resetContent (Plant *plant)
{
	vector<GeomElemCone*>::iterator it;
	vector<float>::iterator itp;
	for (int i=0; i<zSize; i++)
	for (int j=0; j<xSize; j++)
	for (int k=0; k<ySize; k++)
	{
		bool trouve=true;
		while (trouve)
		{
		trouve = false;
			for (it=space[i][j][k].content.begin(),itp=space[i][j][k].part.begin(); it!=space[i][j][k].content.end(); it++)
			{
				GeomElemCone *g = *it;
				if (g->getPtrBrc()->getBranc()->getPlant() == plant)
				{
					space[i][j][k].content.erase(it);
                    if (   !space[i][j][k].part.empty()
                        && itp != space[i][j][k].part.end())
                        space[i][j][k].part.erase(itp);
					space[i][j][k].containedSurface = 0;
					trouve = true;
					break;
				}
				if (!space[i][j][k].part.empty())
					itp++;
			}
		}
	}
	nbPlant --;
	if (nbPlant == 0)
		reset();
}

VoxelSpace* VoxelSpace::instance(){
	static VoxelSpace *s_=NULL;
	if (s_ == NULL)
		s_ = new VoxelSpace();
	return s_;
}

Voxel VoxelSpace::at(int i, int j, int k)
{
    if (   i < 0
        || i >= xSize
        || j < 0
        || j >= ySize
        || k < 0
        || k >= zSize)
        return space[0][0][0];

    return space[k][i][j];
}

Voxel *VoxelSpace::addrAt(int i, int j, int k)
{
    if (   i < 0
        || i >= xSize
        || j < 0
        || j >= ySize
        || k < 0
        || k >= zSize)
        return NULL;

    return &space[k][i][j];
}

bool VoxelSpace::voxelCoordinates (Voxel v, int *i, int *j, int *k)
{
    return voxelCoordinates(&v, i, j, k);
}

bool VoxelSpace::voxelCoordinates (Voxel *v, int *i, int *j, int *k)
{
	for (*k=0; *k<zSize; (*k)++)
	for (*i=0; *i<xSize; (*i)++)
	for (*j=0; *j<ySize; (*j)++)
	{
		if (&space[*k][*i][*j] == v)
		{
			return true;
		}
	}
	return false;
}

void VoxelSpace::displayContent()
{
	vector<GeomElemCone*>::iterator it;
	for (int k=0; k<zSize; k++)
	for (int i=0; i<xSize; i++)
	for (int j=0; j<ySize; j++)
	{
		if (space[k][i][j].content.size() == 0)
			continue;

//		cout << i<<" "<<j<<" "<<k;
		for (it=space[k][i][j].content.begin(); it!=space[k][i][j].content.end(); it++)
		{
			GeomElemCone *g = *it;
//			cout << endl<<g->getPtrBrc()->getBranc()->getPlant()->getName()<<"(de "<<g->getMatrix().getTranslation().x()+g->getPtrBrc()->getBranc()->getPlant()->getPosition()[0]<<","<<g->getMatrix().getTranslation().y()+g->getPtrBrc()->getBranc()->getPlant()->getPosition()[1]<<","<<g->getMatrix().getTranslation().z()<<" a "<<g->getMatrix().getTranslation().x()+g->getMatrix().getMainVector().x()*g->getLength()+g->getPtrBrc()->getBranc()->getPlant()->getPosition()[0]<<","<<g->getMatrix().getTranslation().y()+g->getMatrix().getMainVector().y()*g->getLength()+g->getPtrBrc()->getBranc()->getPlant()->getPosition()[1]<<","<<g->getMatrix().getTranslation().z()+g->getMatrix().getMainVector().z()*g->getLength()<<")";
			g->getPtrBrc()->getBranc()->getPlant()->getName();
			g->getMatrix().getTranslation().x()+g->getPtrBrc()->getBranc()->getPlant()->getPosition()[0];
		}
//		cout<<endl;
	}
}

void VoxelSpace::output(const string& f)
{
    ofstream fic (f.c_str());
    float l;

	if (!fic.good())
	{
		perror (f.c_str());
		return;
	}

    cout << "output voxelSpace to " << f << endl;

	fic << "XOrig\tYOrig\tZOrig\tVoxelSize\tXsize\tYSize\tZSize" << endl;
	fic << xOrigin << "\t" << yOrigin << "\t" << zOrigin << "\t" << VoxelSize() << "\t" << XSize() << "\t" << YSize() << "\t" << ZSize() << endl;
//	fic << "k\taverage" << endl;
//	for (int k=0; k<ZSize(); k++)
//    {
//        l = 0;
//        for (int i=0; i<XSize(); i++)
//        for (int j=0; j<YSize(); j++)
//        {
//            Voxel *v = addrAt(i,j,k);
////            float lf = atof((const char*)v->getData()->to_string().c_str());
//            if (v->getAdditionalData ("light"))
//            {
//                LightData *light = (lightData *)v->getAdditionalData("light");
//                l += light->val;
//            }
//        }
//        l /= XSize()*YSize();
//        fic << k << "\t" << l << endl;
//    }

	fic << "k\ti\tj\tz\tx\ty\tsurf";
	fic << endl;

	for (int k=0; k<ZSize(); k++)
	for (int i=0; i<XSize(); i++)
	for (int j=0; j<ZSize(); j++)
    {
        Voxel *v = addrAt(i,j,k);
        if (v)
        {
            fic << i << "\t" << j << "\t" << k << "\t" << xOrigin+i*VoxelSize() << "\t" << yOrigin+j*VoxelSize() << "\t" << zOrigin+k*VoxelSize();
            fic << "\t" << v->ContainedSurface();
    //        fic << "\t" << v->data->to_string();
    //        if (v->getData() != 0)
    //            fic << "\t" << v->getData()->to_string();
            fic << endl;
        }
    }
    fic.close();

}

