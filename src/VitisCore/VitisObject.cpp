/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "VitisObject.h"

VitisObject::VitisObject()
{
	additionalData = 0;
}

VitisObject::~VitisObject()
{
	if (additionalData != 0)
		delete additionalData;
}

int VitisObject::addAdditionalData (std::string criteria, void *data)
{
	return addAdditionalData (criteria, data, 0);
}

int VitisObject::addAdditionalData (std::string criteria, void *data, char flag)
{
	if (additionalData == 0)
		additionalData = new MapData;

	if (!additionalData->empty())
	{
		MapData::const_iterator p=additionalData->find(criteria);

		if (p != additionalData->end())
			return 1;
		else
		{
			AdditionalData *adata = new AdditionalData;
			adata->flag = flag;
			adata->data = data;
			additionalData->insert(MapDataPair(criteria, adata));
		}
	}
	else
	{
		AdditionalData *adata = new AdditionalData;
		adata->flag = flag;
		adata->data = data;
		additionalData->insert(MapDataPair(criteria, adata));
	}

	return 0;
}

void VitisObject::removeAdditionalData (std::string criteria)
{
	if (	additionalData != 0
		&& !additionalData->empty())
		additionalData->erase(criteria);
}

void *VitisObject::getAdditionalData (std::string criteria )
{
	if (   additionalData == 0
		|| additionalData->empty())
		return NULL;

	MapData::const_iterator p;

	p=additionalData->find(criteria);

	if (p != additionalData->end())
		return p->second->data;
	else
		return NULL;
}

void *VitisObject::getAdditionalData (int ind )
{
	if (   additionalData == 0
		|| additionalData->empty()
		|| additionalData->size() <= ind)
		return NULL;

	MapData::const_iterator p;

	int i;
	for (p=additionalData->begin(),i=0; i<ind; p++,i++);

	return p->second->data;
}

std::string VitisObject::getAdditionalDataCriteria (int ind )
{
	if (   additionalData == 0
		|| additionalData->empty()
		|| additionalData->size() <= ind)
		return 0;

	MapData::const_iterator p;

	int i=0;
	for (p=additionalData->begin(); i<ind; p++,i++);

	return p->first;
}

int VitisObject::modifyAdditionalData (std::string criteria, void *data)
{
	if (   additionalData == 0
		|| additionalData->empty())
		return 1;

	MapData::iterator p;

	p=additionalData->find(criteria);

	if (p != additionalData->end())
		p->second->data = data;
	else
		return 1;

	return 0;
}

int VitisObject::modifyAdditionalData (std::string criteria, void *data, char flag)
{
	if (   additionalData == 0
		|| additionalData->empty())
		return 1;

	MapData::iterator p;

	p=additionalData->find(criteria);

	if (p != additionalData->end())
	{
		p->second->data = data;
		p->second->flag = flag;
	}
	else
		return 1;

	return 0;
}

int VitisObject::additionalDataNumber ()
{
	if (!additionalData)
		return 0;

	return additionalData->size();
}

bool VitisObject::isAdditionalDataPrintable (std::string criteria )
{
	return (hasAdditionalDataProperty (criteria, VITISGLDSPRINTABLE));
}

bool VitisObject::isAdditionalDataArchivable (std::string criteria )
{
	return (hasAdditionalDataProperty (criteria, VITISARCHIVABLE));
}

bool VitisObject::isAdditionalDataPrintable (int ind )
{
	return (hasAdditionalDataProperty (ind, VITISGLDSPRINTABLE));
}

bool VitisObject::isAdditionalDataArchivable (int ind )
{
	return (hasAdditionalDataProperty (ind, VITISARCHIVABLE));
}

bool VitisObject::hasAdditionalDataProperty (std::string criteria, char prop)
{
	if (   additionalData == 0
		|| additionalData->empty())
		return NULL;

	MapData::const_iterator p;

	p=additionalData->find(criteria);

	if (   p != additionalData->end()
		&& p->second->flag & prop)
		return true;
	else
		return false;
}
bool VitisObject::hasAdditionalDataProperty (int ind, char prop)
{
	if (   additionalData == 0
		|| additionalData->empty()
		|| additionalData->size() <= ind)
		return NULL;

	MapData::const_iterator p;

	int i;
	for (p=additionalData->begin(),i=0; i<ind; p++,i++);

	if (p->second->flag & prop)
		return true;
	else
		return false;
}
bool VitisObject::hasAdditionalData (std::string criteria)
{
	if (   additionalData == 0
		|| additionalData->empty())
		return NULL;

	MapData::const_iterator p;

	p=additionalData->find(criteria);

	if (p != additionalData->end())
		return true;
	else
		return false;
}




