#include "Output.h"
#include "PlantPIPE.h"


void read_arguments(long  nbarg, char **arguments, Plant & plt);

int main ( int argc, char **argv)
{
    PlantPIPE plt;
	//	read_arguments(argc, argv, plt);
	read_arguments(argc, argv, plt);

	plt.run();

	int i;
	cin >> i;
Sleep(10000);

	return 1;
}

void usage(void)
{
}

void read_arguments(long  nbarg, char **arguments, Plant & plt)
{
	bool trouve;
	long  i;
	float f;
	char buffer[256];
	float step_geometry=0;
	static OutputManager outpManl;
	static OutputManager outpMang;
	static OutputManager outpManm;
	Output * outpl=NULL;
	Output * outpg=NULL;
	Output * outpm=NULL;


	if(nbarg<=1)
	{
		usage();
	}
	else
	{
#ifndef WIN32
		outpManl.addOutputDevice("libOutputMtgAmapsim.0.0.0");
		outpMang.addOutputDevice("libOutputLineTreeAmapsim.0.0.0");
#else
		outpManl.addOutputDevice("OutputMtgPipe.dll");
		outpMang.addOutputDevice("OutputLineTreeAmapsim.dll");
#endif

		do
		{


			trouve=0;




			if (strncmp (arguments[nbarg-1], "-o", 2) == 0)
			{
				strcpy (buffer, &arguments[nbarg-1][2]);
				nbarg --;
				trouve = true;
				outpl=outpManl.create_output(&plt,0,to_string(buffer));
				outpg=outpMang.create_output(&plt,0,to_string(buffer));
			}





			if (strncmp (arguments[nbarg-1], "-g", 2) == 0)
			{
				if (sscanf (&arguments[nbarg-1][2], "%f", &f) > 0)
				{
					step_geometry = f;

				}
				nbarg --;
				trouve = 1;
			}



			if (strncmp (arguments[nbarg-1], "-h", 2) == 0)
			{
				usage ();
			}



		} while (trouve);



		if (nbarg >= 2)
		{
			std::string acro (arguments[1]);
			plt.setName(acro);
			plt.setParamName(acro);
			plt.setHotStop(atof(arguments[2]));
			nbarg -= 2;

			if (nbarg > 1)
			{
				plt.setSeed(atoi(arguments[3]));
			}
		}
		else
		{
			usage ();

		}


		if(!step_geometry)
			step_geometry=plt.getScheduler().getHotStop();
		if(outpl)
		{
			outpManl.scheduler_output(&plt,outpl,step_geometry);
		}
		if(outpg)
		{
			outpMang.scheduler_output(&plt,outpg,step_geometry);
		}

		plt.getConfigData().cfg_supersimplif=0;
		plt.getConfigData().cfg_geoSimp=0;
		plt.setHotStart(0);

		plt.init();
	}//end else

}



