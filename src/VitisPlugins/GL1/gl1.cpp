/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WINDOWS
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "externGL1.h"
#include "gl1.h"
#include "ParamFile.h"
#include "Notifier.h"
//#include "Bud.h"


/////////////////////////////////////////////////////
//
// This method get called for each new topology creation
//
// It stores UT global parameters
//
/////////////////////////////////////////////////////
void DecompPlugin::on_notify (const state& s, paramMsg * data)
{
    int category;

	if (!glplugin_ptr->getParam()->isValid())
		return;


    //Cast msg parameters
    paramDecompAxeLevel *p = (paramDecompAxeLevel *)data;

    //Retrieve branch
    DecompAxeLevel *I = p->d;

	if (s == SigDelDecompAxeLevel)
	{
		LeafData *d;
		FruitData *f;
		EntnData *e;
		if ((d = (LeafData*)I->getAdditionalData("leafVolume")))
		{
			delete d;
			I->removeAdditionalData("leafVolume");
		}
		else if ((f = (FruitData*)I->getAdditionalData("fruitVolume")))
		{
			delete f;
			I->removeAdditionalData("fruitVolume");
		}
		if ((e = (EntnData*)I->getAdditionalData("woodVolume")))
		{
			delete e;
			I->removeAdditionalData("woodVolume");
		}
#ifdef GL3-4
		if ((d = (double*)I->getAdditionalData("budVolume")))
		{
			delete d;
			I->removeAdditionalData("budVolume");
		}
#endif
		return;
	}

    //Swith on axe object level (0 stand for branch, etc..)
	int decompLevel = ((Branc*)I->getParentOfLevel(0))->getPlant()->getPlantFactory()->getLevelOfDecomposition(((Branc*)I->getParentOfLevel(0))->getClassType());
    if (I->getLevel() == decompLevel)
	{
        //Retrieve initial phyAge of the branch (at birth of)
		Branc *b = (Branc*)I->getParentOfLevel(0);
		OrganKind kind = glplugin_ptr->kindOf (b);

		if (   kind == BLADEORGAN
			|| kind == PETIOLEORGAN)
		{
			LeafData *volumeL = new LeafData;
			volumeL->v[0] = volumeL->v[1] = 0;
			I->addAdditionalData ("leafVolume", volumeL, VITISGLDSPRINTABLE);
		}
		else if (kind == FRUITORGAN)
		{
			FruitData *volumeF = new FruitData;
			volumeF->v = 0;
			I->addAdditionalData ("fruitVolume", volumeF, VITISGLDSPRINTABLE);
		}
		else
		{
			EntnData *volumeE = new EntnData;
			volumeE->nbval = 1;
			volumeE->volume.resize(1);
			volumeE->volume[0] = 0;
			volumeE->nbleaves = 0; /* number of leaves above that element */
			volumeE->lng = 0; /* length of this element */
			volumeE->surface = 0; /* externalsurface for this element */
			I->addAdditionalData ("woodVolume", volumeE, VITISGLDSPRINTABLE);
		}
    }
#ifdef GL3-4
	else if (   I->getLevel() == 0
			 && kind != BLADEORGAN
			 && kind != PETIOLEORGAN
			 && kind != FRUITORGAN)
	{
		double *volumeB = new double;
		*volumeB = 0;
		I->addAdditionalData ("budVolume", volumeB);
	}
#endif
}

/////////////////////////////////////////////////////
//
// This methods get called in geometry context, when <Length> parameter
// is retrieved.
//
// We can override AmapSim normal behavior here, or just let
// the <Length> parameter as is.
//
/////////////////////////////////////////////////////
void LengthPlugin::on_notify (const state& , paramMsg * data)
{

	if (!glplugin_ptr->getParam()->isValid())
		return;

	//Cast message parameters
    paramElemGeom * p = (paramElemGeom*)data;

    //Retrieve branch
    Branc *b = p->gec->getPtrBrc()->getBranc();

    //Retrieve PA (Retrieve TU/GU from branch then retrieve PA)
    int category = glplugin_ptr->getCategory (b);

	ParamSet * vp_plt_h = glplugin_ptr->getParam();


	b->startPosit();
	DecompAxeLevel *GU = b->getCurrentElementOfSubLevel(glplugin_ptr->getGuLevel());
	int decompLevel = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	GU->positOnFirstElementOfSubLevel(decompLevel);
	DecompAxeLevel *entn = GU->getCurrentElementOfSubLevel(decompLevel);
	b->endPosit();

	OrganKind kind = glplugin_ptr->kindOf (b);
	if (kind == BLADEORGAN)
	{
		LeafData *volume = (LeafData*)entn->getAdditionalData("leafVolume");
		if (!volume)
			return;
		p->v = (float)sqrt (volume->v[BLADE]/vp_plt_h->getParameter("BladeThickness")->value());
	}
	else if (kind == PETIOLEORGAN)
	{
		LeafData *volume = (LeafData*)entn->getAdditionalData("leafVolume");
		if (!volume)
			return;
		double vol = volume->v[PETIOLE] / GU->getElementNumberOfSubLevel(decompLevel);

		p->v = (float)glplugin_ptr->length ( vol,
				vp_plt_h->getParameter("ShapeFactorA")->value(category),
				vp_plt_h->getParameter("ShapeFactorB")->value(category));
	}
	else if (kind == FRUITORGAN)
	{
		/* assume it is a sphere */

		FruitData *volume = (FruitData*)entn->getAdditionalData("fruitVolume");
		if (!volume)
			return;
		p->v = (float)2. * pow (3. * volume->v / M_PI /4., 1./3.);
	}
	else if (kind != EMPTY) /* it is wood */
	{
		/* compute length according to pith volume */

		EntnData *e = (EntnData*)entn->getAdditionalData("woodVolume");
		if (!e)
			return;
		p->v = e->lng;
	}
}


double GLPlugin::length (double volume, float ShapeA, float ShapeB)
{
	double l;

	l = pow (volume, 1.+ShapeB) * ShapeA;
	l = sqrt (l);

	return l;
}


/////////////////////////////////////////////////////
//
//
// This methods get called in geometry context, when <Diameter> parameter
// is retrieved.
//
/////////////////////////////////////////////////////
void DiameterPlugin::on_notify (const state& , paramMsg * data)
{
 	if (!glplugin_ptr->getParam()->isValid())
		return;

   paramElemGeom * p = (paramElemGeom*)data; //Retrieve msg params

    Branc *b = p->gec->getPtrBrc()->getBranc(); //Retrieve current branch

    int category = (int)glplugin_ptr->getCategory (b); //Retrieve current branch physiological age

	OrganKind kind = glplugin_ptr->kindOf (b);

	ParamSet * vp_plt_h = glplugin_ptr->getParam();

	b->startPosit();
	DecompAxeLevel *GU = b->getCurrentElementOfSubLevel(glplugin_ptr->getGuLevel());
	int decompLevel = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	GU->positOnFirstElementOfSubLevel(decompLevel);
	DecompAxeLevel *entn = GU->getCurrentElementOfSubLevel(decompLevel);
	b->endPosit();

	if (kind == BLADEORGAN)
	{
		LeafData *volume = (LeafData*)entn->getAdditionalData("leafVolume");
		if (!volume)
			return;
		p->v = (float)sqrt (volume->v[BLADE]/vp_plt_h->getParameter("BladeThickness")->value()) ;
	}
	else if (kind == PETIOLEORGAN)
	{

		//TODO Verif utilit� du posit
		LeafData *volume = (LeafData*)entn->getAdditionalData("leafVolume");
		if (!volume)
			return;
		double vol = volume->v[PETIOLE] / GU->getElementNumberOfSubLevel(decompLevel);


		p->v = (float)diameter ( vol,
			vp_plt_h->getParameter("ShapeFactorA")->value(category),
				vp_plt_h->getParameter("ShapeFactorB")->value(category));
	}
	else if (kind == FRUITORGAN)
	{
		/* assume it is a sphere */

		FruitData *volume = (FruitData*)entn->getAdditionalData("fruitVolume");
		if (!volume)
			return;
		p->v = (float)2. * pow (3. * volume->v / M_PI /4., 1./3.);
	}
	else if (kind != EMPTY) /* it is wood */
	{
		/* compute length according to pith volume */

		EntnData *e = (EntnData*)entn->getAdditionalData("woodVolume");
		if (!e)
			return;

		/* compute total volume */
		double v = 0;
		for (int i=0; i<e->nbval; i++)
			v += e->volume[i];
		/* assume it is a circular cylinder */
		p->v = (float)sqrt (4. * v / M_PI / e->lng);
	}

}

double DiameterPlugin::diameter (double volume, float ShapeA, float ShapeB)
{
	double d, s;

	s = pow (volume, 1.-ShapeB) / ShapeA;
	s = sqrt (s);
	/* assume it is a circular cylinder */
	d = sqrt (4. * s / M_PI);

	return d;
}




/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{

   paramPlant * d = (paramPlant*)data; //Retrieve msg params

	glplugin_ptr->setPlant (d->p);

	if (glplugin_ptr->initData())
		return;

	Scheduler::instance()->create_process(glplugin_ptr,NULL,1,102);
	Scheduler::instance()->create_process(glplugin_ptr,(EventData*)d->p,Scheduler::instance()->getHotStop(),101);
}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{
	glplugin_ptr->setPlant (NULL);
}



/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
GLPlugin::GLPlugin(const std::string& f, void *ppp) {

	plant_ptr = (Plant *)pp;

    //child -> parent binding PTR
	l = new LengthPlugin (ppp);
    l->glplugin_ptr = this;
	diam = new DiameterPlugin (ppp);
    diam->glplugin_ptr = this;
	p = new PlantPlugin (ppp);
    p->glplugin_ptr = this;
	pp = new PlantDeletePlugin (ppp);
    pp->glplugin_ptr = this;
	d = new DecompPlugin (ppp);
    d->glplugin_ptr = this;

	paramFile = f;
}


int GLPlugin::initData()
{

	totalMatter=0;
	glMaxCategory=0;
	pithSink=NULL;
	layerSink=NULL;
	petioleSink=NULL;
	bladeSink=NULL;
	fruitSink=NULL;
	param_H=NULL;
	waterConsumption=0;
	totalResistivity=0;
	totalSurface=0;
	lastMatterTime=0;
	currentDemand = -1;

	cumultotalOrganMatter=0;
	cumultotalBladeMatter=0;
	cumultotalPetioleMatter=0;
 	cumultotalPithMatter=0;
	cumultotalLayerMatter=0;
	cumultotalFruitMatter=0;
	totalOrganMatter=0;
	totalBladeMatter=0;
	totalPetioleMatter=0;
	totalPithMatter=0;
	totalLayerMatter=0;
	totalFruitMatter=0;

	setGuLevel();

	param_H = new ParamSet(paramFile);

	if (!param_H->Exists())
		return 1;

	param_H->setParameterSignal (new GLParameterSignal);

	if (!checkParameterSet())
		return 1;

#ifdef GL3-4
	a = createGL4ParameterPlugin();
	a->glplugin_ptr = this;
#endif

	glMaxCategory = MaxCategory();

	pithSink = normalizeExpansion (glMaxCategory, param_H->getParameter("GrowthTime"), param_H->getParameter("PithSinkAgingN"), param_H->getParameter("PithSinkAgingP"));

//	layerSink = normalizeExpansion (glMaxCategory, param_H->getParameter("GrowthTime"), param_H->getParameter("LayerSinkAgingN"), param_H->getParameter("LayerSinkAgingP"));

	petioleSink = normalizeExpansion (glMaxCategory, param_H->getParameter("GrowthTime"), param_H->getParameter("PetioleSinkAgingN"), param_H->getParameter("PetioleSinkAgingP"));

	bladeSink = normalizeExpansion (glMaxCategory, param_H->getParameter("GrowthTime"), param_H->getParameter("BladeSinkAgingN"), param_H->getParameter("BladeSinkAgingP"));

	fruitSink = normalizeExpansion (glMaxCategory, param_H->getParameter("GrowthTime"), param_H->getParameter("FruitSinkAgingN"), param_H->getParameter("FruitSinkAgingP"));

#ifdef GL3-4
	budSink = normalizeExpansion (glMaxCategory, param_H->getGrowthTime(), param_H->getBudSinkAgingN(), param_H->getBudSinkAgingP());
#endif

	cout<<"\ttotal\tblade\tpetiole\tpith\tlayer\t(nb)\tfruit\t"<<endl;;

	return 0;
}

double ** GLPlugin::normalizeExpansion (int phySz, Parameter *GrowthTime, Parameter *AgingN, Parameter *AgingP)
{
	int i,j,t;
	double **exp, s, x, n, p;

	exp = new double * [phySz];
	for (i=0; i<phySz; i++)
	{
		s = 0;
		t = GrowthTime->value(i);
		exp[i] = new double [t];
		for (j=0,x=0; j<t; j++,x+=1.)
		{
			n = AgingN->value(j);
			p = AgingP->value(j);
			exp[i][j] = pow ((x+.5)/t,(1+n)*p-1.) * pow (1.-(x+.5)/t, (1.+n)*(1.-p)-1.) / t;
			s += exp[i][j];
		}
		for (j=0; j<t; j++)
			exp[i][j] /= s;
	}
	return exp;
}



void GLPlugin::process_event(EventData *msg)
{

	if (msg != NULL)
	{
		outTarget();
		return;
	}

	float top_extern;

	Scheduler *s = Scheduler::instance();


	computeMatterCurrentTime();

	/* sets the next external eventData */
	top_extern = s->getTopClock();

		/* computes demand and split matter between the existing organs */
	currentDemand = computeDemand (top_extern);

	computeVolume (top_extern, currentDemand);

#ifdef OLDGL
	/* computes resistivity for the next cycle */
	totalResistivity=computeResistivity (top_extern);
#else
	/* computes functioning surface for the next cycle */
	totalSurface=computeSurface (top_extern);
#endif

	if (s->getTopClock()+1 <= s->getHotStop())
	{
		s->signal_event(this->getPid(),(EventData*)msg,s->getTopClock()+1,102);
	}
}

double GLPlugin::computeDemand (float top)
{
	double localDemand = 0, demand;
	int organ, i, j, k, category, gTime, topAge, cpt=0;
	Branc *b;


	adjustUpperLeavesNumber(top);

	localDemand = 0;
	b = plant_ptr->getTopology().seekTree(TopoManager::RESET);
	Scheduler *s = Scheduler::instance();

	int decompLevel = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	while (b != NULL)
	{
		demand = 0;
		organ = 0;
		b->startPosit();
		if(b->positOnFirstElementOfSubLevel(guLevel))
		{
			do
			{
				DecompAxeLevel *U = b->getCurrentElementOfSubLevel(guLevel);
				if(!U->positOnFirstElementOfSubLevel(decompLevel))
					continue;

				OrganKind kind = kindOf (b);
				if (kind == EMPTY)
					continue;

				DecompAxeLevel *entn = b->getCurrentElementOfSubLevel(decompLevel);
				category = getCategory (b);
				int subcategory = category > MaxCategory()-1 ? MaxCategory()-1 : category;
				gTime = param_H->getParameter("GrowthTime")->value(category);
				/* is this a blade ? */
				if (kind == BLADEORGAN)
				{
					organ = 1;
					if (entn->getAdditionalData("leafVolume"))
					{
						topAge = top - getTime (entn);
						if (topAge < gTime)
						{
							demand += bladeSink[subcategory][topAge]*param_H->getParameter("BladeMasterSink")->value(category);
//							adjustUpperLeavesNumber(b);
						}
					}
				}
				/* is this a petiole ? */
				else if (kind == PETIOLEORGAN)
				{
					organ = 1;
					if (entn->getAdditionalData("leafVolume"))
					{
						topAge = top - getTime (entn);
						if (topAge < gTime)
						{
							demand += petioleSink[subcategory][topAge]*param_H->getParameter("PetioleMasterSink")->value(category);
						}
					}
				}
				/* is this a fruit ? */
				else if (kind == FRUITORGAN)
				{
					organ = 1;
					if (entn->getAdditionalData("fruitVolume"))
					{
						topAge = top - getTime (entn) - 1;
						if (   topAge < gTime
							&& topAge >= 0)
						{
							demand += fruitSink[subcategory][topAge]*param_H->getParameter("FruitMasterSink")->value(category);
						}
					}
				}

				if (   !organ
					&& entn->getAdditionalData("woodVolume"))
				{
					/* add layer sink if axe is older than one and bears functioning leaves */
					if (   s->getTopClock()> 1
						&& (category = HasLayer(b)) != -1)
					{
						b->startPosit();
						b->positOnFirstElementOfSubLevel(guLevel);
						DecompAxeLevel *GU;
						for (int i=0; i<b->getElementNumberOfSubLevel(guLevel)-1; i++)
						{
							GU = b->getCurrentElementOfSubLevel(guLevel);
							if (GU->positOnFirstElementOfSubLevel (decompLevel))
							do
							{
								entn = GU->getCurrentElementOfSubLevel (decompLevel);
								EntnData *e = (EntnData*)entn->getAdditionalData("woodVolume");
								float lambda;
								if (param_H->getParameter("LayerRepartitionFactor"))
									lambda = param_H->getParameter("LayerRepartitionFactor")->value();
								else
									lambda = 1;
								// demand is proportional to external surface of the internode -> ring width is constant
								//demand += param_H->getParameter("LayerMasterSink")->value(category)*e->surface * (1-lambda + lambda*e->nbleaves);
								//presslerLayerDemand += param_H->getParameter("LayerMasterSink")->value(category)*e->surface * e->nbleaves;
								//poolLayerDemand += param_H->getParameter("LayerMasterSink")->value(category)*e->surface;

								// demand is proportional to length of the internode -> ring surface is constant
								demand += param_H->getParameter("LayerMasterSink")->value(category)*e->lng * (1-lambda + lambda*e->nbleaves);
								presslerLayerDemand += param_H->getParameter("LayerMasterSink")->value(category)*e->lng * e->nbleaves;
								poolLayerDemand += param_H->getParameter("LayerMasterSink")->value(category)*e->lng;

							}while (GU->positOnNextElementOfSubLevel(decompLevel));

							b->positOnNextElementOfSubLevel(guLevel);
						}

						b->endPosit();
					}

					/* go to last Gu for the pith */
					b->startPosit();
					DecompAxeLevel *GU;

#ifdef GL3-4
					b->positOnLastElementOfSubLevel(guLevel);
					GU = b->getCurrentElementOfSubLevel(guLevel);
					category = getCategory (b);
					int subcategory = category > MaxCategory()-1 ? MaxCategory()-1 : category;
					while (   GU->getElementNumberOfSubLevel (decompLevel) == 0
						&& b->positOnPreviousElementOfSubLevel (guLevel))
						GU = b->getCurrentElementOfSubLevel(guLevel);
					GU->positOnFirstElementOfSubLevel (decompLevel);
					if ((entn=b->getCurrentElementOfSubLevel(decompLevel)))
						topAge = top - getTime (entn);
					else
						topAge = top;
					// add apical bud demand
					demand += budSink[subcategory][topAge]*param_H->getParameter("BudMasterSink")->value(category);
#endif

					b->positOnLastElementOfSubLevel(guLevel);
					GU = b->getCurrentElementOfSubLevel(guLevel);
					GU->positOnFirstElementOfSubLevel (decompLevel);
					category = getCategory (b);
					int subcategory = category > MaxCategory()-1 ? MaxCategory()-1 : category;

					if (GU->getElementNumberOfSubLevel (decompLevel) > 0)
					{
						/* add pith sink(s) I assume that growingTime is given by the last Gu */
						gTime = param_H->getParameter("GrowthTime")->value(category);

						entn=b->getCurrentElementOfSubLevel(decompLevel);
						/* computes the age of last Gu (for terminated axes) */
						topAge = top-getTime (entn);

						/* add sink from end to bottom of branch */
						for (k=0; k<gTime-topAge && k < b->getElementNumberOfSubLevel(guLevel); k++)
						{
							int icategory = getCategory (b);
							int subicategory = icategory > MaxCategory()-1 ? MaxCategory()-1 : icategory;
							demand += pithSink[subicategory][k+topAge]*param_H->getParameter("PithMasterSink")->value(icategory);
							b->positOnPreviousElementOfSubLevel(guLevel);
						}
					}
					b->endPosit();
					/* no need to seek all the axe, we are only interested into the layer and the pith */
					break;
				}
			}while(b->positOnNextElementOfSubLevel(guLevel));


			/* multiply by the number of instances of that organ */
			//demand *= b->hierarcPtrsNumber;
			demand *= b->TotalAxeNumberWithinPlant();
			presslerLayerDemand *= b->TotalAxeNumberWithinPlant();
			poolLayerDemand *= b->TotalAxeNumberWithinPlant();

			localDemand += demand;

		}//endif
		b->endPosit();


		cpt+=b->TotalAxeNumberWithinPlant();

		b = plant_ptr->getTopology().seekTree(TopoManager::NEXT);

	}

	return localDemand;
}

void GLPlugin::adjustUpperLeavesNumber (float top)
{
	Branc *b = plant_ptr->getTopology().seekTree(TopoManager::RESET);
	Scheduler *s = Scheduler::instance();
	int category;
	float gTime, topAge;

	int decompLevel = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());

	while (b != NULL)
	{
		b->startPosit();
		if(b->positOnFirstElementOfSubLevel(guLevel))
		{
			do
			{
				DecompAxeLevel *U = b->getCurrentElementOfSubLevel(guLevel);
				if(!U->positOnFirstElementOfSubLevel(decompLevel))
					continue;

				OrganKind kind = kindOf (b);
				if (kind == EMPTY)
					continue;

				/* is this a blade ? */
				if (kind == BLADEORGAN)
				{
					DecompAxeLevel *entn = b->getCurrentElementOfSubLevel(decompLevel);
					if (entn->getAdditionalData("leafVolume"))
					{
						category = getCategory (b);
						gTime = param_H->getParameter("GrowthTime")->value(category);
						topAge = top - getTime (entn);
						if (topAge < gTime)
							adjustUpperLeavesNumber(b);
					}
				}
			}while(b->positOnNextElementOfSubLevel(guLevel));

		}//endif
		b->endPosit();

		b = plant_ptr->getTopology().seekTree(TopoManager::NEXT);

	}
}

void GLPlugin::adjustUpperLeavesNumber (Branc *b)
{
	Branc *bearer = b->CurrentHierarcPtr()->getBearer()->getBranc();
	int increment;

	bearer->startPosit();
	/* on the leafs bearer, tag every branch nodes */
	int decompLevel = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	if (kindOf (b) == BLADEORGAN)
	{
		bearer->positOnLastElementOfSubLevel(decompLevel);
		if (b->getCurrentElementOfSubLevel(decompLevel)->getAdditionalData("leafVolume"))
			increment = 1;
		else
			increment = 0;
	}
	else
	{
		bearer->positOnElementAtSubLevel(b->getCurrentHierarc()->indexOnBearerAtLowestLevel,decompLevel);
		increment = 1;
	}

	do
	{
		EntnData *e=(EntnData*)bearer->getCurrentElementOfSubLevel(decompLevel)->getAdditionalData("woodVolume");
		if (!e)
			continue;
		e->nbleaves += increment;
	} while (bearer->positOnPreviousElementOfSubLevel(decompLevel));

	if (bearer != bearer->CurrentHierarcPtr()->getBearer()->getBranc())
		adjustUpperLeavesNumber (bearer);

	bearer->endPosit();
}

double GLPlugin::computeResistivity (float top)
{
	double leafResistivity, resistance;

	int  category, fTime, topAge;

	Branc *b;

	DecompAxeLevel *GU, *entn;

	/* get the global resistivity of the leaf of the forest */
	leafResistivity = 0;

	b = plant_ptr->getTopology().seekTree (TopoManager::RESET);

	int decompLevel = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	while (b != NULL)
	{
		b->startPosit();

		if(b->positOnFirstElementOfSubLevel(guLevel))
		{
			do
			{
				GU = b->getCurrentElementOfSubLevel(guLevel);
				resistance=0;

				if(!GU->positOnFirstElementOfSubLevel(decompLevel))
					continue;

				OrganKind kind = kindOf (b);
				if (kind == EMPTY)
					continue;

				category = getCategory (b);
				fTime =  param_H->getParameter("FunctioningTime")->value(category);

				entn = GU->getCurrentElementOfSubLevel(decompLevel);

				topAge = top-getTime(entn);


				/* is this a blade ? */
				if (   kind == BLADEORGAN
					&& topAge < fTime)
				{
					LeafData *volume = (LeafData*)entn->getAdditionalData("leafVolume");
					if (volume)
						resistance += param_H->getParameter("BladeResistivity")->value(category)*param_H->getParameter("BladeThickness")->value()/volume->v[BLADE];
				}
				/* is this a petiole ? */
				if (   kind == PETIOLEORGAN
					&& topAge < fTime)
				{
					LeafData *volume = (LeafData*)entn->getAdditionalData("leafVolume");
					if (volume)
						resistance += param_H->getParameter("PetioleResistance")->value(category);
				}


				if (resistance > 0)
				{
					leafResistivity += (double)b->TotalAxeNumberWithinPlant() / resistance;
				}


			}
			while(b->positOnNextElementOfSubLevel(guLevel));
		}//end if

		b->endPosit();


		b =plant_ptr->getTopology().seekTree (TopoManager::NEXT);
	}//end while

	return leafResistivity;

}


double GLPlugin::computeSurface (float top)
{
	double surface;

	int  category, fTime, topAge;

	Branc *b;

	DecompAxeLevel *GU, *entn;

	/* get the global resistivity of the leaf of the forest */
	surface = 0;
//	totalResistance = 0;

	b = plant_ptr->getTopology().seekTree (TopoManager::RESET);

	int decompLevel = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	while (b != NULL)
	{
		b->startPosit();

		if(b->positOnFirstElementOfSubLevel(guLevel))
		{

			do
			{
				GU = b->getCurrentElementOfSubLevel(guLevel);

				if(!GU->positOnFirstElementOfSubLevel(decompLevel))
					continue;

				OrganKind kind = kindOf (b);
				if (kind == EMPTY)
					continue;

				entn = GU->getCurrentElementOfSubLevel(decompLevel);

				category = getCategory (b);
				fTime =  param_H->getParameter("FunctioningTime")->value(category);
				topAge = top-getTime (entn);


				/* is this a blade ? */
				if (   kind == BLADEORGAN
					&& topAge < fTime)
				{
					LeafData *volume = (LeafData*)entn->getAdditionalData("leafVolume");
					if (volume)
					{
						surface += volume->v[BLADE]/param_H->getParameter("BladeThickness")->value()*(double)b->TotalAxeNumberWithinPlant();
//						totalResistance += param_H->getParameter("BladeResistivity")->value(category)*param_H->getParameter("BladeThickness")->value()*param_H->getParameter("BladeThickness")->value()/volume->v[BLADE]*(double)b->TotalAxeNumberWithinPlant();
					}
				}

				/* reset seen leaves number for wood */
				else if (   kind != BLADEORGAN
						 && kind != PETIOLEORGAN
						 && kind != FRUITORGAN)
				{
					for (int i=0; i<GU->getElementNumberOfSubLevel(decompLevel); i++)
					{
						entn = GU->getCurrentElementOfSubLevel(decompLevel);
						EntnData *e = (EntnData*)entn->getAdditionalData("woodVolume");
						if (e)
						{
							e->nbleaves = 0;
							float vol = 0;
							for (int i=0; i<e->nbval; i++)
								vol += e->volume[i];
							if (e->lng > 0)
								e->surface = 2 * sqrt (M_PI * vol * e->lng);
							else
								e->surface = 0;
						}
						GU->positOnNextElementOfSubLevel(decompLevel);
					}
				}
			}
			while(b->positOnNextElementOfSubLevel(guLevel));
		}//end if

		b->endPosit();

		b = plant_ptr->getTopology().seekTree (TopoManager::NEXT);
	}//end while

	return surface;
}


void GLPlugin::computeVolume (float top, double totalDemand)
{
	double layerMatter=0, organMatter=0, unitMatter=0, pithVolume=0;
	int i, j, k, category, organ, first, seenLeafNumber, totalSeenLeafNumber;
	int topAge, gTime, inLayer;
	Branc *b;
	DecompAxeLevel *GU, *entn;

	double totalPithInLayerVol, pithInLayerVol;
	double nbEntnInLayer, totalNbEntnInLayer;

	int totalNbLayers = 0;

	totalOrganMatter=0; totalBladeMatter=0; totalPetioleMatter=0; totalPithMatter=0; totalLayerMatter=0; totalFruitMatter=0;
	/* compute the base matter per demand unit */
	if (totalDemand == 0)
		unitMatter = 0;
	else
		unitMatter = currentMatter / totalDemand;

	//printf ("split %lf matter for global %lf demand\n",totalMatter,totalDemand);
	totalSeenLeafNumber = 0;

	b = plant_ptr->getTopology().seekTree(TopoManager::RESET);

	if(b == NULL)
		return;

	totalPithInLayerVol=0.0;
	totalSeenLeafNumber = 0;
	double totalPresslerLayerDemand = 0, presslerLayerDemand;
	double totalPoolLayerDemand = 0, poolLayerDemand;

	totalNbEntnInLayer=0;

	int decompLevel = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	do
	{
		organ = 0;
		first = 1;
		seenLeafNumber = 0;
		pithInLayerVol = 0;
		nbEntnInLayer=0;
		presslerLayerDemand = 0;
		poolLayerDemand = 0;

		b->startPosit();
		if(b->positOnFirstElementOfSubLevel(guLevel))
		{
			do
			{
				GU = b->getCurrentElementOfSubLevel(guLevel);

				if(!GU->positOnFirstElementOfSubLevel(decompLevel))
					continue;

				OrganKind kind = kindOf (b);
				if (kind == EMPTY)
					continue;

				entn = GU->getCurrentElementOfSubLevel(decompLevel);

				category = getCategory (b);
				int subcategory = category > MaxCategory()-1 ? MaxCategory()-1 : category;
				gTime = param_H->getParameter("GrowthTime")->value(category);
				/* is this a blade ? */
				if (kind == BLADEORGAN)
				{
					organ = 1;
					topAge = top-getTime(entn);
					if (topAge < gTime)
					{
						organMatter = bladeSink[subcategory][topAge]*param_H->getParameter("BladeMasterSink")->value(category) * unitMatter;
						LeafData *volume = (LeafData*)entn->getAdditionalData("leafVolume");
						if (volume)
						{
							volume->v[BLADE] += organMatter;

							if (top > 0)
							{
								totalBladeMatter += organMatter*b->TotalAxeNumberWithinPlant();
								totalOrganMatter += organMatter*b->TotalAxeNumberWithinPlant();
							}
						}
					}
				}
				/* is this a petiole ? */
				if (kind == PETIOLEORGAN)
				{
					organ = 1;
					topAge = top-getTime(entn);
					if (topAge < gTime)
					{
						organMatter = petioleSink[subcategory][topAge]*param_H->getParameter("PetioleMasterSink")->value(category) * unitMatter;
						LeafData *volume = (LeafData*)entn->getAdditionalData("leafVolume");
						if (volume)
						{
							volume->v[PETIOLE] += organMatter;
							if (top > 0)
							{
								totalPetioleMatter += organMatter*b->TotalAxeNumberWithinPlant();
								totalOrganMatter += organMatter*b->TotalAxeNumberWithinPlant();
							}
						}
					}
				}
				/* is this a fruit ? */
				if (kind == FRUITORGAN)
				{
					organ = 1;
					topAge = top-getTime(entn)-1;
					if (   topAge < gTime
						&& topAge >= 0)
					{
						organMatter = fruitSink[subcategory][topAge]*param_H->getParameter("FruitMasterSink")->value(category) * unitMatter;
						FruitData *volume = (FruitData*)entn->getAdditionalData("fruitVolume");
						if (volume)
						{
							volume->v += organMatter;
							if (top > 0)
							{
								totalFruitMatter += organMatter*b->TotalAxeNumberWithinPlant();
								totalOrganMatter += organMatter*b->TotalAxeNumberWithinPlant();
							}
						}
					}
				}

				if (   !organ
					&& entn->getAdditionalData("woodVolume"))
				{
					int category;
					if (first)
					{
						first = 0;

						/* add matter to the stack for layers if there is more than 1 Gu */
						if (   Scheduler::instance()->getTopClock() > 1.
							&& (category = HasLayer(b) ) != -1)
						{
							totalNbLayers ++;
							/* go to last Gu */

							float lambda;
							if (param_H->getParameter("LayerRepartitionFactor"))
								lambda = param_H->getParameter("LayerRepartitionFactor")->value();
							else
								lambda = 1;
							b->startPosit();
							b->positOnFirstElementOfSubLevel(guLevel);
							for (int i=0; i<b->getElementNumberOfSubLevel(guLevel)-1; i++)
							{
								GU = b->getCurrentElementOfSubLevel(guLevel);
								if (GU->positOnFirstElementOfSubLevel (decompLevel))
								do
								{
									entn = GU->getCurrentElementOfSubLevel (decompLevel);
									EntnData *e = (EntnData*)entn->getAdditionalData("woodVolume");
									// demand is proportional to external surface of the internode -> ring width is constant
									//double demand = param_H->getParameter("LayerMasterSink")->value(category) * e->surface * (1-lambda + lambda*e->nbleaves);
									// demand is proportional to length of the internode -> ring surface is constant
									double demand = param_H->getParameter("LayerMasterSink")->value(category) * e->lng * (1-lambda + lambda*e->nbleaves);
									layerMatter += demand * unitMatter;

									/* adjust size of layer table */
									e->nbval ++;
									e->volume.push_back(demand * unitMatter);
									entn->modifyAdditionalData("woodVolume", e);

									if (top > 0)
									{
										totalLayerMatter += layerMatter * b->TotalAxeNumberWithinPlant();
										totalOrganMatter += layerMatter * b->TotalAxeNumberWithinPlant();
									}
								}while (GU->positOnNextElementOfSubLevel(decompLevel));
								b->positOnNextElementOfSubLevel(guLevel);
							}

							b->endPosit();
						}

						/* go to last Gu for the pith matter */

						b->startPosit();
#ifdef GL3-4
						//b->positOnLastGuWithinBranc();
						//category = getCategory (b);
						//b->positOnFirstEntnWithinGu();
						//while (   b->getCurrentEntnNumberWithinGu() == 0
						//	   && b->positOnPreviousGuWithinBranc());
						//if (b->getCurrentEntn())
						//	topAge = top-b->getCurrentEntn()->instant;
						//else
						//	topAge = top;

						b->positOnLastElementOfSubLevel(guLevel);
						category = getCategory (b);
						int subcategory = category > MaxCategory()-1 ? MaxCategory()-1 : category;
						GU = b->getCurrentElementOfSubLevel(guLevel);
						while (   GU->getElementNumberOfSubLevel (decompLevel) == 0
							&& b->positOnPreviousElementOfSubLevel (guLevel))
							GU = b->getCurrentElementOfSubLevel(guLevel);
						GU->positOnFirstElementOfSubLevel (decompLevel);
						if ((entn=b->getCurrentElementOfSubLevel(decompLevel)))
							topAge = top - getTime (entn);
						else
							topAge = top;

						double budVolumeIncrease = budSink[subcategory][topAge]*param_H->getParameter("BudMasterSink")->value(category) * unitMatter;
						double *budVolume = (double*)b->getAdditionalData("budVolume");
#endif
						b->positOnLastElementOfSubLevel(guLevel);
						GU = b->getCurrentElementOfSubLevel(guLevel);
						if (GU->getElementNumberOfSubLevel (decompLevel) > 0)
						{
							category = getCategory (b);
							int subcategory = category > MaxCategory()-1 ? MaxCategory()-1 : category;
#ifdef GL3-4
							double *budVolume = (double*)b->getAdditionalData("budVolume");
#endif

							gTime = param_H->getParameter("GrowthTime")->value(category);
							GU->positOnFirstElementOfSubLevel(decompLevel);
							/* computes the age of last Gu (for terminated axes) */
							entn = GU->getCurrentElementOfSubLevel(decompLevel);
							topAge = top-getTime (entn);


							/* compute pith entn number */
							long pithEntnNumber = 0;
							for (k=0; k<gTime-topAge && k < b->getElementNumberOfSubLevel(guLevel); k++)
							{
								GU = b->getCurrentElementOfSubLevel(guLevel);
								pithEntnNumber += GU->getElementNumberOfSubLevel(decompLevel);
								b->positOnPreviousElementOfSubLevel(guLevel);
							}
							/* compute matter from end to bottom of branch */
							b->positOnLastElementOfSubLevel(guLevel);
							for (k=0; k<gTime-topAge && k < b->getElementNumberOfSubLevel(guLevel); k++)
							{
								int icategory = getCategory (b);
								int subicategory = icategory > MaxCategory()-1 ? MaxCategory()-1 : icategory;
								organMatter = pithSink[subicategory][k+topAge]*param_H->getParameter("PithMasterSink")->value(icategory) * unitMatter;
								GU->positOnFirstElementOfSubLevel(decompLevel);
								entn = GU->getCurrentElementOfSubLevel(decompLevel);
								EntnData *e = (EntnData*)entn->getAdditionalData("woodVolume");
								if (e)
								{
									e->volume[PITH] += organMatter/GU->getElementNumberOfSubLevel(decompLevel);
#ifdef GL3-4
									e->volume[PITH] += budVolume[BUD]/pithEntnNumber;
#endif
									if (top > 0)
									{
										totalPithMatter += organMatter*b->TotalAxeNumberWithinPlant();
										totalOrganMatter += organMatter*b->TotalAxeNumberWithinPlant();
									}
								}
								b->positOnPreviousElementOfSubLevel(guLevel);
							}

#ifdef GL3-4
							budVolume[BUD] = 0;
#endif
						}

#ifdef GL3-4
						budVolume[BUD] += budVolumeIncrease;
#endif

						b->endPosit();
					}

					GU = b->getCurrentElementOfSubLevel(guLevel);
					GU->positOnFirstElementOfSubLevel(decompLevel);

					/* check if the Gu will get a new layer */
					if (b->getCurrentElementIndexOfSubLevel(guLevel) < b->getElementNumberOfSubLevel(guLevel)-1)
						inLayer = 1;
					else
						inLayer = 0;

					k=0;
					/* sum the total number of functioning seen leaves and update pith volume for next entn */
					do
					{
						entn = GU->getCurrentElementOfSubLevel(decompLevel);
						EntnData *e = (EntnData*)entn->getAdditionalData("woodVolume");
						if (!e)
							continue;

						/* avoid the last apperaring GU */
						if (k == 0)
							pithVolume = e->volume[PITH];
						else
							e->volume[PITH] = pithVolume;

						int category = getCategory (b);
						e->lng = length (e->volume[PITH],param_H->getParameter("ShapeFactorA")->value(category),param_H->getParameter("ShapeFactorB")->value(category));

						/* avoid the last apperaring GU */
						if (inLayer)
						{
							seenLeafNumber += e->nbleaves;
							nbEntnInLayer++;
							pithInLayerVol+=e->volume[PITH];
						}

						k++;

					}while(GU->positOnNextElementOfSubLevel(decompLevel));
				}
			}while(b->positOnNextElementOfSubLevel(guLevel));
		}//end posit

		b->endPosit();


		totalSeenLeafNumber += seenLeafNumber * b->TotalAxeNumberWithinPlant();
		totalPithInLayerVol += pithInLayerVol * b->TotalAxeNumberWithinPlant();
		totalNbEntnInLayer += nbEntnInLayer * b->TotalAxeNumberWithinPlant();

		//((BudHYDROSANSLUM *)b->getBud())->totalPithInLayerVol=totalPithInLayerVol;



	}while((b=plant_ptr->getTopology().seekTree(TopoManager::NEXT))!=NULL);

#if 0
	/* loop on the branches to split total layer matter according to nbUpperLeaves
	 * and add a new ring to each node
	 * */
	if (totalSeenLeafNumber > 0)
		unitMatter = totalLayerMatter / totalSeenLeafNumber;
	else
		unitMatter = 0;



	double averagePithMatterVolume = totalPithInLayerVol/(double)totalNbEntnInLayer;
	int nbEntn;
	b = plant_ptr->getTopology().seekTree(TopoManager::RESET);

	do
	{

		/* is this not a branch ? */
		b->startPosit();
		if(b->positOnFirstElementOfSubLevel(guLevel))
		{
			OrganKind kind = kindOf (b);
			if (   kind == BLADEORGAN
				|| kind == PETIOLEORGAN
				|| kind == FRUITORGAN
				|| kind == EMPTY
				|| !b->getCurrentElementOfSubLevel(decompLevel)->getAdditionalData("woodVolume"))
			{
				b->endPosit();
				continue;
			}

			/* split layer matter according to the number of seen leaves */
			do
			{

				GU = b->getCurrentElementOfSubLevel(guLevel);

				if(!GU->positOnFirstElementOfSubLevel(decompLevel))
					continue;

				if (b->getCurrentElementIndexOfSubLevel(guLevel) == b->getElementNumberOfSubLevel(guLevel)-1)
					continue;

				nbEntn = GU->getElementNumberOfSubLevel(decompLevel);
				do
				{
					entn = GU->getCurrentElementOfSubLevel(decompLevel);

					EntnData *e = (EntnData*)entn->getAdditionalData("woodVolume");
					if (!e)
						continue;
					organMatter = e->nbleaves * unitMatter * (e->volume[PITH] / averagePithMatterVolume);

					/* adjust size of layer table */
					e->nbval ++;
					e->volume.push_back(organMatter);
					entn->modifyAdditionalData("woodVolume", e);
				}while(GU->positOnNextElementOfSubLevel(decompLevel));
			}while(b->positOnNextElementOfSubLevel(guLevel));
		}//end if posit
		b->endPosit();


	}while((b=plant_ptr->getTopology().seekTree(TopoManager::NEXT))!=NULL);
#endif

	cumultotalOrganMatter+=totalOrganMatter;

	cumultotalBladeMatter+=totalBladeMatter;

	cumultotalPetioleMatter+=totalPetioleMatter;

	cumultotalPithMatter+=totalPithMatter;

	cumultotalLayerMatter+=totalLayerMatter;

	cumultotalFruitMatter+=totalFruitMatter;

	if (top > 0)
	{
		cout.setf(ios_base::fixed, ios_base::floatfield);
		cout.precision(3);

		cout<<endl<<"top "<<top<<endl;
		cout<<"\t"<<totalOrganMatter<<"\t"<<totalBladeMatter<<"\t"<<totalPetioleMatter<< "\t"<<totalPithMatter<<"\t"<<totalLayerMatter<<"\t("<<totalNbLayers<<")"<<"\t"<<totalFruitMatter<<endl;;
		cout<<"cumul\t"<<cumultotalOrganMatter<<"\t"<<cumultotalBladeMatter<<"\t"<<cumultotalPetioleMatter<< "\t"<<cumultotalPithMatter<<"\t"<<cumultotalLayerMatter<<"\t\t"<<cumultotalFruitMatter<<endl;;
		if (totalDemand > 0)
			cout <<"source sink ratio : "<<currentMatter / totalDemand<<endl;
		else
			cout<<"no Demand\n"<<endl;

		cout.unsetf(ios_base::floatfield);
		cout.precision(6);
	}
}







void GLPlugin::computeMatterCurrentTime ()
{
	double timeFraction=Scheduler::instance()->getTopClock()-lastMatterTime; /// Fraction cycle to compute

	currentMatter=0;
	Scheduler *s = Scheduler::instance();

	/* first add the matter coming from seed */
	if (s->getTopClock() <= param_H->getParameter("SeedTime")->value())
		currentMatter +=  param_H->getParameter("SeedVolume")->value() /  param_H->getParameter("SeedTime")->value();

	// if the matter computing has been already done for this time , don't compute again
	if(timeFraction != 0 )
	{
		//BAD
#ifdef OLDGL
		waterConsumption=param_H->getClimateParam() * totalResistivity * timeFraction;
#else
		double *p_climate = (double *)plant_ptr->getAdditionalData("Climate");
		if (p_climate)
		{
			((SingleValueParameter*)param_H->getParameter("ClimateParam"))->set(*p_climate);
		}
		waterConsumption = param_H->getParameter("ClimateParam")->value() * timeFraction;
		waterConsumption *= param_H->getParameter("PetioleResistance")->value((float)0)*10000.;
//		waterConsumption /= totalResistance;
		waterConsumption /= param_H->getParameter("BladeResistivity")->value((float)0);
		waterConsumption *= (1 - exp (-totalSurface/param_H->getParameter("PetioleResistance")->value((float)0)/10000.));
#endif
		currentMatter+=waterConsumption;

		lastMatterTime=s->getTopClock();
	}

	totalMatter += currentMatter;
}

int GLPlugin::HasLayer (Branc *b)
{
	Branc *borne, *branc;
	Hierarc *cur;
	int i, nbBorne, exist;

	/* an axe bears a layer if it consists of more than one GU and bears functioning leaves
	 * or if it bears only axes consisting in one GU bearing functioning leaves
	 * */
	/* seeks for the last GU of the axe */

	int category = -1;
	branc = b;
	branc->startPosit();
	branc->positOnLastElementOfSubLevel (guLevel);
	category = getCategory (branc);
	branc->endPosit();

	if (   branc->getElementNumberOfSubLevel(guLevel) > 1
		&& seekForFunctioningLeaves (b))
		return category;

	/* seeks for borne functioning leaves */
	cur = branc->CurrentHierarcPtr();
	nbBorne = cur->getNbBorne ();
	exist = 0;
	for (i=0; i<nbBorne; i++)
	{
		borne = cur->getBorne (i);
		borne->startPosit();
		borne->positOnFirstElementOfSubLevel(b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType()));
		OrganKind kind = kindOf (borne);
		borne->endPosit();

		/* if it is not wood, continue */
		if (kind != WOODORGAN)
			continue;

		//if (   borne->getElementNumberOfSubLevel(guLevel) == 1
		//	&& seekForFunctioningLeaves (borne))
		//	return -1;
		if (   borne->getElementNumberOfSubLevel(guLevel) >= 1
			&& seekForFunctioningLeaves (borne))
		{
			exist = 1;
			break;
		}
	}
	if (exist)
		return  category;
	else
		return -1;
}

int GLPlugin::seekForFunctioningLeaves (Branc *b)
{
	Branc *borne, *branc;
	Hierarc *cur;
	int i, nbBorne, fTime;
	float topAge;
	int decompLevel = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());

	branc = b;
	/* seeks for borne functioning leaves */
	cur = branc->CurrentHierarcPtr();
	nbBorne = cur->getNbBorne ();
	for (i=0; i<nbBorne; i++)
	{
		borne = cur->getBorne (i);
		if (   borne->getElementNumberOfSubLevel(branc->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType())) > 0
			&& kindOf (borne) == BLADEORGAN)
		{
			fTime = getParam()->getParameter("FunctioningTime")->value(getCategory (borne), borne);

			borne->startPosit();
			borne->positOnFirstElementOfSubLevel(decompLevel);
			/* compute age of the blade */
			topAge = Scheduler::instance()->getTopClock()- getTime(borne->getCurrentElementOfSubLevel(decompLevel));
			borne->endPosit();

			if (topAge < fTime)
				return 1;
		}
	}

	return 0;
}

/** \brief Signal that notifies upon parameter access */
float GLParameterSignal::sigGetParameter (float & vv, Parameter *pp, VitisObject *oo)
{
    notifier<messageGLParameter> *t= &(notifier<messageGLParameter>::instance());

	if (t->get_nb_subscriber() == 0)
		return vv;

	pParameter.set(vv,pp,oo);

	t->notify(SigGetParameter, &pParameter, oo);

	vv=pParameter.v;

	return vv;
}

bool GLPlugin::checkParameterSet()
{
	bool ret = true;
	ret &= param_H->checkParameter("BladeThickness");
	ret &= param_H->checkParameter("ShapeFactorA");
	ret &= param_H->checkParameter("ShapeFactorB");
	ret &= param_H->checkParameter("GrowthTime");
	ret &= param_H->checkParameter("PithSinkAgingN");
	ret &= param_H->checkParameter("PithSinkAgingP");
	ret &= param_H->checkParameter("BladeSinkAgingN");
	ret &= param_H->checkParameter("BladeSinkAgingP");
	ret &= param_H->checkParameter("PetioleSinkAgingN");
	ret &= param_H->checkParameter("PetioleSinkAgingP");
	ret &= param_H->checkParameter("FruitSinkAgingN");
	ret &= param_H->checkParameter("FruitSinkAgingP");
#ifdef GL3-4
	ret &= param_H->checkParameter("BudSinkAgingN");
	ret &= param_H->checkParameter("BudSinkAgingP");
	ret &= param_H->checkParameter("BudMasterSink");
#endif
	ret &= param_H->checkParameter("BladeMasterSink");
	ret &= param_H->checkParameter("PetioleMasterSink");
	ret &= param_H->checkParameter("PithMasterSink");
	ret &= param_H->checkParameter("FruitMasterSink");
	ret &= param_H->checkParameter("LayerMasterSink");
	ret &= param_H->checkParameter("FunctioningTime");
	ret &= param_H->checkParameter("BladeResistivity");
	ret &= param_H->checkParameter("PetioleResistance");
	ret &= param_H->checkParameter("ClimateParam");
	ret &= param_H->checkParameter("SeedTime");
	ret &= param_H->checkParameter("SeedVolume");

	param_H->checkParameter("LayerRepartitionFactor");

	param_H->setValid(ret);

	return ret;
}


