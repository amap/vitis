/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "ParamFile.h"
#include "externGL1.h"

#include <vector>

class GLPlugin;


/** @name some constant definition
 * */
/*@{*/
#define PITH 0
#define FRUIT 0
#define PETIOLE 0
#define BLADE 1
#define BUD 0

enum OrganKind {WOODORGAN, BLADEORGAN, PETIOLEORGAN, FRUITORGAN, BUDORGAN, ROOTORGAN, EMPTY};

/*@}*/



class EntnData : public PrintableObject
{
public :
	int nbval;
	std::vector<double> volume;
	int nbleaves;
	float lng, surface;
	std::string to_string ()
	{
		std::ostringstream oss;
		// �crire la valeur dans le flux
		oss << nbval ;
		for (int i=0; i<volume.size(); i++)
		{
			oss << " " << volume[i];
		}
		oss << " " << nbleaves << " " << surface;
		// renvoyer une string
		return oss.str();
	};
};

class LeafData : public PrintableObject
{
public :
	double v[2];
	std::string to_string ()
	{
		std::ostringstream oss;
		// �crire la valeur dans le flux
		oss << v[0] << " " << v[1] ;
		// renvoyer une string
		return oss.str();
	};
};
class FruitData : public PrintableObject
{
public :
	double v;
	std::string to_string ()
	{
		std::ostringstream oss;
		// �crire la valeur dans le flux
		oss << v ;
		// renvoyer une string
		return oss.str();
	};
};



struct GL1_EXPORT LengthPlugin:public subscriber<messageVitisElemLength> {
	GLPlugin * glplugin_ptr;

	LengthPlugin (void *p) { subscribe(p);};
	virtual ~LengthPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
	double length (double volume, float ShapeA, float ShapeB);
};


struct GL1_EXPORT DiameterPlugin:public subscriber<messageVitisElemDiameter> {
	GLPlugin * glplugin_ptr;

	DiameterPlugin (void *p) { subscribe(p);};
	virtual ~DiameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
	double diameter (double volume, float ShapeA, float ShapeB);
};

struct GL1_EXPORT DecompPlugin:public subscriber<messageVitisDecompAxeLevel> {
	GLPlugin * glplugin_ptr;

	DecompPlugin (void *p) {subscribe(p);};
	virtual ~DecompPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

//struct GL4ParameterPlugin:public subscriber<messageAxeRef> {
//	GLPlugin * glplugin_ptr;
//
//	GL4ParameterPlugin () {subscribe();};
//	virtual ~GL4ParameterPlugin () {unsubscribe();};
//	void on_notify (const state& st, paramMsg * data=NULL);
//};

struct GL1_EXPORT PlantPlugin:public subscriber<messageVitisPlant> {
	GLPlugin * glplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct GL1_EXPORT PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	GLPlugin * glplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

enum messageGLParameter {SigGetParameter};

// specialized signal class for GL
class GL1_EXPORT GLParameterSignal:public ParameterSignal
{
public:
	float sigGetParameter (float & vv, Parameter *pp, VitisObject *oo);
};


class GL1_EXPORT GLPlugin : public VProcess
{
public:
	GLPlugin(const std::string& f, void *p);
	virtual ~GLPlugin(){;};
	void reset();
	Plant *getPlant(){return plant_ptr;};
	void setPlant(Plant *p){plant_ptr=p;};
	void process_event(EventData *msg);
	ParamSet* getParam(){return param_H;};

	int initData();

	double length (double volume, float ShapeA, float ShapeB);

	double Demand() {return currentDemand;};
	double Matter() {return currentMatter;};

	virtual OrganKind kindOf (Branc *b) = 0;
	virtual int getCategory (Branc *b) = 0;
	int getGuLevel () {return guLevel;};
	virtual void setGuLevel () = 0;
//	virtual GL4ParameterPlugin *createGL4ParameterPlugin () = 0;

	virtual bool checkParameterSet ();

protected:

	PlantPlugin *p;
	PlantDeletePlugin *pp;
	DecompPlugin *d;
	DiameterPlugin *diam;
	LengthPlugin *l;
//	GL4ParameterPlugin *a;

	int guLevel;

	Plant * plant_ptr;

	string paramFile;

	double **pithSink;

	double **budSink;

	double **layerSink;

	double **petioleSink;

	double **bladeSink;

	double **fruitSink;

	double **normalizeExpansion (int phySz, Parameter *GrowthTime, Parameter *AgingN, Parameter *AgingP);

	 double totalMatter;
	 double currentMatter;
	 double currentDemand;
	 double cumultotalOrganMatter;
	 double cumultotalBladeMatter;
	 double cumultotalPetioleMatter;
	 double cumultotalPithMatter;
	 double cumultotalLayerMatter;
	 double cumultotalFruitMatter;
	 double totalOrganMatter;
	 double totalBladeMatter;
	 double totalPetioleMatter;
	 double totalPithMatter;
	 double totalLayerMatter;
	 double totalFruitMatter;
	 double Climate;
     float glMaxCategory;

	 /// \brief Total blade surface of the plant for the cycle
	 double	totalSurface;

	 /// \brief Total resisitivity of the plant for the cycle( sum of resisitivity of each leaf)
	 double	totalResistivity;
//	 double	totalResistance; // old fashioned GL with silly petiole resistance and ridiculous blades resistivity

	 /// \brief Layer demand computed according to Pressler or global pool theory
	 double	presslerLayerDemand;
	 double	poolLayerDemand;

	 /// \brief Last date of water consumption computing
	 double lastMatterTime;

	 /// \brief Water consumption since the last consumption computing
	 double waterConsumption;

	 /// \brief Pointer to the hydro parameters manager
	 ParamSet * param_H;

	void computeMatterCurrentTime ();

	virtual int MaxCategory () = 0;
	virtual int HasLayer (Branc *b);
	virtual int seekForFunctioningLeaves (Branc *b);
	virtual float getTime (DecompAxeLevel *d) = 0;
	double computeDemand (float top);
	void computeVolume (float top, double totalDemand);
	double computeResistivity (float top);
	double computeSurface (float top);

	void adjustUpperLeavesNumber (Branc *b);
	void adjustUpperLeavesNumber (float top);

	virtual void outTarget ()=0;

};

