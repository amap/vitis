/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  LineTreeFormat.h
///			\brief Definition of the LineTreeFormat class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _OBJFORMAT_H
#define _OBJFORMAT_H
#include "OutputFormat.h"
#include "externOutputObj.h"
#include "bfstream.h"
#include "Symbole.h"
using namespace Vitis;
#include <vector>


struct IdParentChild
{
	int idParent;

	int * idChild;

	int nbChild;

	IdParentChild() {idParent=0; idChild=NULL; nbChild=0;}

	~IdParentChild()
	{
		delete [] idChild;
	}



};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class ObjFormat
///			\brief Write data into .obj wavefront format
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class OUTPUTOBJ_EXPORT ObjFormat:public OutputFormat
{
	protected:

	/// \brief obj filestream
	ofstream   __objstream;

	/// \brief geometrical symbols vector
	std::vector<int> tabSymb;

	std::vector<IdParentChild> vectId;

	Uint cptID;

	/// \brief Current "port�e"
	int currentPortee;

	int nbsymb;

	Symbole *symb;


	public:

	/// \brief Constructor
	/// \param finame base filename (path + short filename - extension)
	ObjFormat(const std::string &finame);

	/// \brief Destructor
	virtual ~ObjFormat(void);

	bool printHeader(const std::string& headerf){return true;};

	/// \brief Print the vobj into line-tree and arc
	virtual int printData(VitisObject * vobj);

	/// \brief Print the vobj into line-tree and arc
	virtual int initFormat(VitisObject * vobj);

	/// \brief Print a linetree record
	virtual void printRecord(class GeomElemCone * ge, int elemId);

	/// \brief Print tree data in recursive order
	virtual int  printDataInOrder(VitisObject * vobj, int idBearer);

	virtual int printSubStructure(class GeomBranc * gb);

	virtual int loadSymbols(std::string paramName);

	void createRootSymbol();

};



#endif

