int find_path (char *search, char *path)
{
   FILE *fic;
   char line[256];
 
   path[0] = '.';
   path[1] = 0;
#ifdef WIN32
   if (fopen_s (&fic, "vitis.cfg","r") != 0)
   {
      if (fopen_s (&fic, "C:\\Documents and Settings\\All Users\\vitis.cfg","r") != 0)
         return 0;
   }
#else
   if ((fic = fopen (".cfg","r")) == NULL)
   {
      if ((fic = fopen ("~/.cfg","r")) == NULL)
         return 0;
   }
#endif
   
   while (fgets (line, 255, fic) != NULL)
   {
      if (strncmp (line, search, strlen(search)) == 0)
      {
         path[0] = 0;
#ifdef WIN32
         sscanf_s (line, "%*s = %s", path, 255);
#else
         sscanf (line, "%*s = %s", path);
#endif
         if (strlen(path) == 0)
#ifdef WIN32
            strcpy_s (path, 256, ".");
#else
            strcpy (path, ".");
#endif
         break;
      }
   }
 
   fclose (fic);

#ifndef WIN32
   if (path[0] != '/')
   {
      char str1[256];
#ifdef WIN32
      _getcwd (str1, 255);
#else
      getcwd (str1, 255);
#endif
      strcat (str1, "/");
      strcat (str1, path);
      strcpy (path, str1);
   }
#endif

 
   return 1;
}


void prepareName (char *out, char *in)
{
	/* check if file name has a path inside */
	int i;
	for (i=(int)strlen(in); i>=0; i--)
	{
#ifdef WIN32
		if (   in[i] == '\\'
			|| in[i] == '/')
#else
		if (in[i] == '/')
#endif
			break;
	}

	if (i < 0)
	{
		find_path ("LIGNE", out);
#ifdef WIN32
		strcat_s(out, 256, "\\");
		strcat_s(out,256, in);
#else
		strcat(out, "/");
		strcat(out, in);
#endif
	}
	else
#ifdef WIN32
		strcpy_s (out, 256, in);
#else
		strcpy (out, in);
#endif

}

