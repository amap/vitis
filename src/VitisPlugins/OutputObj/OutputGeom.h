/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file OutputGeom.h
///			\brief geometrical output.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __OUTPUTGEOMOBJ_H__
#define __OUTPUTGEOMOBJ_H__
#include "Output.h"
#include "GeomManager.h"
#include "externOutputObj.h"




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class OutputGeom
///			\brief Geometrical output class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class OUTPUTOBJ_EXPORT OutputGeom: public Output
{

	public:

		/// \brief Default constructor
		OutputGeom();

		/// \brief Destructor
		virtual ~OutputGeom(void);

		/// \brief Compute geometrical data to be output (lauch geommanager)
		virtual void computeDataToOutput ();


		/// \see Output
		virtual void freeData ();

		/// \see Output
		virtual void writeData (const std::string & finame);

};

#if !defined STATIC_LIB
#ifdef WIN32
extern "C" __declspec(dllexport)  Output * StartPlugin ();
#else
extern "C" Output * StartPlugin ();
#endif
#endif

#endif

