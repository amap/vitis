/***************************************************************************
this file is part of AMAPsim software
                             -------------------
    begin                : 1995
    copyright            : (C) 2002 by jef
    email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#include <stdio.h>
#include <string.h>

#ifdef WIN32
	#include <direct.h>
#endif

#include "common.h"


long swapl (long &i)
{
    unsigned char a[8];
	unsigned long j;

    a[7] = i >> 56;
    a[6] = (i & 0xffffffffffffff) >> 48;
    a[5] = (i & 0xffffffffffff) >> 40;
    a[4] = (i & 0xffffffffff) >> 32;
    a[3] = (i & 0xffffffff) >> 24;
    a[2] = (i & 0xffffff) >> 16;
    a[1] = (i & 0xffff) >> 8;
    a[0] = (i & 0xff);
    
	 i = 0;
	 j = a[7];
	 i += j;
	 j = a[6];
	 j <<= 8;
	 i += j;
	 j = a[5];
	 j <<= 16;
	 i += j;
	 j = a[4];
	 j <<= 24;
	 i += j;
	 j = a[3];
	 j <<= 32;
	 i += j;
	 j = a[2];
	 j <<= 40;
	 i += j;
	 j = a[1];
	 j <<= 48;
	 i += j;
	 j = a[0];
	 j <<= 56;
	 i += j;

	 return i;
}

int swap (int &i)
{
    unsigned char a[4];
	unsigned int j;

	a[3] = i >> 24;
    a[2] = (i & 0xffffff) >> 16;
    a[1] = (i & 0xffff) >> 8;
    a[0] = (i & 0xff);
    
	 i = 0;
	 j = a[3];
	 i += j;
	 j = a[2];
	 j <<= 8;
	 i += j;
	 j = a[1];
	 j <<= 16;
	 i += j;
	 j = a[0];
	 j <<= 24;
	 i += j;

	 return i;
}

int binreadswi (FILE *f, int &i)
{
	 int nb;

    nb = binread (f, i);
	i = swap (i);
//    i = a[0] * 0xffffff + a[1] * 0xffff + a[2] * 0xff + a[3];

	 return nb;
}
int binreadswl (FILE *f, long &i)
{
	 int nb;

    nb = binread (f, i);
	if (sizeof(long) == 4)
	{
		int j = i;
		i = swap(j);
	}
	else
		i = swapl (i);
//    i = a[0] * 0xffffff + a[1] * 0xffff + a[2] * 0xff + a[3];

	 return nb;
}
int binreadsws (FILE *f, unsigned short &i)
{
    unsigned char a[2];
	 unsigned int j;
	 int nb;

    nb = binread (f, i);
    a[1] = (i & 0xffff) >> 8;
    a[0] = (i & 0xff);
    
//    i = a[0] * 0xff + a[1];
	 i = 0;
	 j = a[1];
	 i += j;
	 j = a[0];
	 j <<= 8;
	 i += j;

	 return nb;
}
int binreadswt (FILE *f, float &i, int s)
{
    char a[4],b;
	int nb;
	int j;
	float *x;

	nb = binreadt(f,i,s);
	x = &i;
	for (j=0; j<s; j++)
	{
		memcpy (a, &x[j], 4);
		b = a[3];
		a[3] = a[0];
		a[0] = b;
		b = a[2];
		a[2] = a[1];
		a[1] = b;
		memcpy (&x[j], a, 4);
	}

	return nb;
}

int binreadswf (FILE *f, float &i)
{
    char a[8],b[8];
    int nb;
    int s = sizeof(float);
	 
    nb = binread (f, i);
    memcpy (a, &i, s);
    for (int j=0; j<s; j++)
    {
        b[s-j-1] = a[j];
    }
    memcpy (&i, b, s);

	 return nb;
}

int touille (FILE *f, float &i)
{

    char a[8],b[8];
    int nb;
    int s = sizeof(float);
	 
    nb = binread (f, i);
    memcpy (a, &i, s);
    for (int j=0; j<s; j++)
    {
        b[s-j-1] = a[j];
    }
    memcpy (&i, b, s);

	 return nb;
}


