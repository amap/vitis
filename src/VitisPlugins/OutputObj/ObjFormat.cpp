/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#include <windows.h>
#endif
#include "ObjFormat.h"
#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "GeomBrancCone.h"
#include "UtilMath.h"
#include "Bud.h"
#include "Plant.h"
#include "DebugNew.h"




ObjFormat::ObjFormat(const std::string &finame):OutputFormat(finame),__objstream((finame+".obj").c_str())
{
	currentPortee=0;
	cptID=0;
	symb = NULL;
	nbsymb = 0;
}

ObjFormat::~ObjFormat(void)
{
	__objstream.close();
}

int ObjFormat::initFormat(VitisObject * vobj)
{
	GeomManager * gb = (GeomManager *) vobj;

	Plant *p = (Plant *)gb->getPlant();
	MapParameterFileName lmap = p->getParamName();
	if (lmap.find(string("AMAPSimMod")) != lmap.end())
	{
        if (!loadSymbols (gb->getPlant()->getParamName(string("AMAPSimMod"))))
            return 0;
    }
    else if (lmap.find(string("DigRMod")) != lmap.end())
    {
        createRootSymbol ();
    }

//	vectId.resize(gb->getElemeGeoMaxNumber());

	return 1;

}



//raw writing
int ObjFormat::printData(VitisObject * vobj)
{
	GeomBranc * gb = (GeomBranc *) vobj;
	Plant * plt= gb->getBranc()->getPlant();

	if (gb->getBranc()->getClassType() == "AMAPSimMod")
	{
        __objstream << "#AmapSim simulation of "<< plt->getParamName(string("AMAPSimMod"));
	}
	else if (gb->getBranc()->getClassType() == "DigRMod")
	{
        __objstream << "#DigR simulation of "<< plt->getParamName(string("DigRMod"));
	}
	else
	{
        __objstream << "#void simulation "<<gb->getBranc()->getClassType();
	}
    __objstream << " at age " << Scheduler::instance()->getTopClock();
    __objstream << " with seed " << plt->getConfigData().randSeed;
	__objstream << std::endl<<std::endl;;

	if(plt->getConfigData().cfg_supersimplif)
        printSubStructure (gb);
	else
		printDataInOrder(gb, 0);

	return 1;

}

int ObjFormat::printSubStructure(GeomBranc * gb)
{
	unsigned int i, j;


	Matrix4 bearTransla;

	GeomBrancCone * curGeomBrancCone, *clone;

	currentPortee=0;
    __objstream << "#axe index " << gb->getBranc()->getId()<<std::endl;

	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
        GeomElemCone *cone = (GeomElemCone *)gb->getElementsGeo().at(i);

        if (i == 0)
            bearTransla = Matrix4::translation(cone->getMatrix().getTranslation());

		printRecord(cone, 0 );

		for(j=0; j<cone->getGeomChildren().size(); j++)
		{

			curGeomBrancCone=(GeomBrancCone*)cone->getGeomChildren().at(j);

			if((clone = (GeomBrancCone*)curGeomBrancCone->getGeomBrancClone()) != 0)
			{
				Matrix4 & matTrans=curGeomBrancCone->getCurrentTransform();
                Vector3 transla = clone->getElementsGeo().at(0)->getMatrix().getTranslation();

                curGeomBrancCone->copyFrom(clone, bearTransla*matTrans*Matrix4::translation(-transla));
			}

            printSubStructure(curGeomBrancCone);

		}

		currentPortee++;

	}

	return 1;
}


//Order writing
int ObjFormat::printDataInOrder(VitisObject * vobj, int idParent)
{

	unsigned int i,j,nbChild;
	GeomBranc * gb = (GeomBranc *) vobj;
	int curId;

	curId = idParent;

	//position dans la branche
	currentPortee=0;

    __objstream << "#axe index " << gb->getBranc()->getId()<<std::endl;
	//Pour chaque �l�ment g�om�trique
	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
		//Si on est pas sur le premier neoud de l'axe, on indique l'id du noeud porteur sur l'axe courant
//		vectId[cptID].idParent=curId;


		curId=cptID;


		//Si on est pas sur le premier noeud de la plante, on maj les relations p�res-fils
		//nbChild = 0;
		//if(curId != 0)
		//{
		//	int idBearer=vectId[curId].idParent;

		//	vectId[idBearer].idChild[vectId[idBearer].nbChild]=curId;

		//	vectId[idBearer].nbChild++;

		//}
		//nbChild=(unsigned int)gb->getElementsGeo().at(i)->getGeomChildren().size();
		//if (i < gb->getElementsGeo().size()-1)
		//	nbChild ++;

		//if (nbChild > 0)
		//	vectId[curId].idChild = new int[nbChild];

		currentPortee=i;
		printRecord((GeomElemCone *)gb->getElementsGeo().at(i),curId);

		cptID++;

		//Pour chaque �l�ment port� par celui courant
		for(j=0; j<gb->getElementsGeo().at(i)->getGeomChildren().size(); j++)
			//for (j=0; j<gb->getAxe().getNbBorne(); j++)
		{

			//Ici on indique l'id du parent ( ici l'id courant) � l'�l�ment qui vient d'�tre cr��
            if (gb->getElementsGeo().at(i)->getGeomChildren().at(j)->getElementsGeo().size() > 0)
                printDataInOrder(gb->getElementsGeo().at(i)->getGeomChildren().at(j), curId);
		}
	}

	return 1;

}



void ObjFormat::printRecord(GeomElemCone * ge, int )
{
    int numBr, i;

	numBr=ge->getPtrBrc()->getNum();

	for (i=0; i<tabSymb.size(); i++)
		if (tabSymb[i] == ge->getSymbole())
			break;
	if (i == tabSymb.size())
		tabSymb.push_back(ge->getSymbole());

	Branc *b=(Branc*)(ge->getPtrBrc()->getBranc());


	Matrix4  __matrix= ge->getMatrix();

	__matrix=Matrix4::translation(ge->getPtrBrc()->getBranc()->getPlant()->getGeometry().getPosition())*__matrix;

	Vector3 dp, dssb ,ds, t;

	dssb=__matrix.getNormalVector();
	ds=__matrix.getSecondaryVector();
	dp=__matrix.getMainVector()*(float)ge->getLength();
	t=__matrix.getTranslation();

	float mat[12];
	mat[0] = (float) dp[1];
	mat[1] = (float) dssb[1];
	mat[2] = (float) ds[1];
	mat[3] = (float) t[1];
	mat[4] = (float) -dp[0];
	mat[5] = (float) -dssb[0];
	mat[6] = (float) -ds[0];
	mat[7] = (float) -t[0];
	mat[8] = (float) dp[2];
	mat[9] = (float) dssb[2];
	mat[10] = (float) ds[2];
	mat[11] = (float) t[2];

	int indSymb;
	if (b->getClassType() == "AMAPSimMod")
	{
        if (ge->getSymbole() >= nbsymb)
        {
            int i = ge->getSymbole();
            Symbole *nsymb = new Symbole[i+1];
            for (int j=0; j<nbsymb; j++)
            {
                nsymb[j] = symb[j];
            }

            delete [] symb;
            symb = nsymb;
            nbsymb = i+1;
        }
        if (i = ge->getSymbole() < 0)
            return;

		indSymb = ge->getSymbole();
    }
    else
        indSymb = 0;

	Symbole *s = &symb[indSymb];

	s->CalculerPoints ( (float)ge->getBottomDiam(), (float)ge->getTopDiam(), (float*)mat);


	if (b->getClassType() == "AMAPSimMod")
	{
        BrancAMAP *ba = (BrancAMAP*)b;
        ba->startPosit();
        ba->positOnEntnWithinBrancAt(ge->getPosInBranc());
        float age = Scheduler::instance()->getTopClock() - ba->getCurrentEntn()->instant;
        __objstream << "#symbol index " << ge->getSymbole();
        __objstream << "   phyAge " << ba->getCurrentGu()->phyAgeGu;
        __objstream << "   age " << age;
        __objstream << std::endl;
        ba->endPosit();
	}
	else if (b->getClassType() == "DigRMod")
	{
        __objstream << "#segment "<<std::endl;
	}

	for (i=0; i<s->NbFaces(); i++)
	{
		Face *face = s->getFaceByAdress(i);
		for (int j=0; j<face->NbPts(); j++)
		{
			int ind = face->getPointByAdress(j)->getIndex();
			Pcoord *p = s->getCoordTravByAdress(ind);
			__objstream << "v " << p->px << " " << p->py << " " << p->pz << std::endl;
		}
		__objstream << "f ";
		for (int j=0; j<face->NbPts(); j++)
		{
			__objstream << j-face->NbPts() << " ";
		}
		__objstream << std::endl;
	}
}

void prepareName (char *out, char *in, char *path)
{
	/* check if file name has a path inside */
	int i;
	for (i=strlen(in); i>=0; i--)
	{
#ifdef WIN32
		if (   in[i] == '\\'
			|| in[i] == '/')
#else
		if (in[i] == '/')
#endif
			break;
	}

	if (i < 0)
	{
		strcpy (out, path);
#ifdef WIN32
		if (   out[strlen(out)-1] != '\\'
			&& out[strlen(out)-1] != '/')
		strcat(out,"\\");
#else
		strcat(out,"/");
#endif
		strcat(out,in);
	}
	else
		strcpy (out, in);

}

void ObjFormat::createRootSymbol()
{
    int i;
    nbsymb = 1;
	if (symb)
		delete [] symb;
	symb = new Symbole[1];
	symb->NbFaces(5);
	symb->NbPts(12);
	symb->Faces(new Face[symb->NbFaces()]);
    for (i=0; i<symb->NbFaces(); i++)
    {
        symb->Faces()[i].NbPts(4);
        symb->Faces()[i].Dim(1);
        symb->Faces()[i].Points(new Point[symb->Faces()[i].NbPts()]);
        for (int j=0; j<symb->Faces()[i].NbPts(); j++)
        {
            symb->Faces()[i].getPointByAdress(j)->Col(0);
            symb->Faces()[i].getPointByAdress(j)->Att(0);
       }
    }
    symb->Faces()[0].getPointByAdress(0)->Index(0);
    symb->Faces()[0].getPointByAdress(1)->Index(1);
    symb->Faces()[0].getPointByAdress(2)->Index(2);
    symb->Faces()[0].getPointByAdress(3)->Index(3);
    symb->Faces()[1].getPointByAdress(0)->Index(1);
    symb->Faces()[1].getPointByAdress(1)->Index(4);
    symb->Faces()[1].getPointByAdress(2)->Index(5);
    symb->Faces()[1].getPointByAdress(3)->Index(2);
    symb->Faces()[2].getPointByAdress(0)->Index(4);
    symb->Faces()[2].getPointByAdress(1)->Index(6);
    symb->Faces()[2].getPointByAdress(2)->Index(7);
    symb->Faces()[2].getPointByAdress(3)->Index(5);
    symb->Faces()[3].getPointByAdress(0)->Index(6);
    symb->Faces()[3].getPointByAdress(1)->Index(8);
    symb->Faces()[3].getPointByAdress(2)->Index(9);
    symb->Faces()[3].getPointByAdress(3)->Index(7);
    symb->Faces()[4].getPointByAdress(0)->Index(8);
    symb->Faces()[4].getPointByAdress(1)->Index(10);
    symb->Faces()[4].getPointByAdress(2)->Index(11);
    symb->Faces()[4].getPointByAdress(3)->Index(9);

    symb->Pts(new Pcoord[symb->NbPts()]);
    symb->Pts()[0].setPcoord (0, 0, 0.309, 0, 0.9511, 0.309, 1, 0, 0);
    symb->Pts()[1].setPcoord (0, 0, 1, 0, 0, 1, 0.5, 0, 0);
    symb->Pts()[2].setPcoord (0, 0, 1, 1, 0, 1, 0.5, 1, 0);
    symb->Pts()[3].setPcoord (0, 0.9511, 0.309, 1, 0.9511, 0.309, 1, 1, 0);
    symb->Pts()[4].setPcoord (0, -0.9511, 0.309, 0, -0.9511, 0.309, 0, 0, 0);
    symb->Pts()[5].setPcoord (0, -0.9511, 0.309, 1, -0.9511, 0.309, 0, 1, 0);
    symb->Pts()[6].setPcoord (0, -0.5878, -0.809, 0, -0.5878, -0.809, 0.190989, 0, 0);
    symb->Pts()[7].setPcoord (0, -0.5878, -0.809, 1, -0.5878, -0.809, 0.190989, 1, 0);
    symb->Pts()[8].setPcoord (0, 0.5878, -0.809, 0, 0.5878, -0.809, 0.809011, 0, 0);
    symb->Pts()[9].setPcoord (0, 0.5878, -0.809, 1, 0.5878, -0.809, 0.809011, 1, 0);
    symb->Pts()[10].setPcoord (0, 0.9511, 0.309, 0, 0.9511, 0.309, 1, 0, 0);
    symb->Pts()[11].setPcoord (0, 0.9511, 0.309, 1, 0.9511, 0.309, 1, 1, 0);
}

int ObjFormat::loadSymbols(std::string paramName)
{
	FILE      *f, *fca;
	char      line[256], name[256], fullname[256];
	int       i, ind, maxind;
	std::string symbDir, fullName;

	std::string baseName, s;
	baseName = paramName;
	std::string::size_type Pos = paramName.find_last_of(".");

	if (Pos != std::string::npos)
		baseName = paramName.substr(0, Pos);
	else
		baseName = paramName;

	Pos = baseName.length() - 1;
	while (   (   baseName[Pos] >= '0'
		       && baseName[Pos] <= '9')
		   || baseName[Pos] == '_')
		Pos--;
	baseName = baseName.substr (0, Pos+1);
	std::string::size_type P1 = baseName.find_last_of("/");
	if (P1 == std::string::npos)
		P1 = 0;
	else
		P1 ++;
	std::string::size_type P2 = baseName.find_last_of("\\");
	if (P2 == std::string::npos)
		P2 = 0;
	else
		P2 ++;
	if (P1 > P2)
		Pos = P1;
	else
		Pos = P2;
	while (   baseName[Pos] >= '0'
		   && baseName[Pos] <= '9')
		baseName.erase(Pos, 1);

	s = baseName;
	s.append(".dta");
	if ((f = fopen(s.c_str(),"r")) == NULL)
	{
		perror (s.c_str());
		return(0);
	}

	s = baseName;
	s.append(".fca");
	if ((fca = fopen(s.c_str(),"r")) == NULL)
	{
		perror (s.c_str());
		return(0);
	}
	while (fgets (line, 255, fca) != NULL)
	{
		if (strncmp (line, "SYMBOLES", 8) == 0)
		{
			symbDir[0] = 0;
			i = 8;
			while (   line[i+1] != 0
			       && line[i] != '=')
				i++;
			while (   line[i+1] != 0
				   && line[i] != ' ')
				i++;
			symbDir.assign(&line[i+1]);
			if (symbDir[0] == ' ')
			{
				Pos = paramName.find_first_of(" ");
				symbDir = symbDir.substr(0, Pos-1);
			}
			symbDir = symbDir.substr(0, symbDir.length()-1);
			break;
		}
	}
	fclose (fca);

	fgets (line, 254, f);
	sscanf(line, "%d", &nbsymb);

	maxind = 0;
	for (i=0; i<nbsymb; i++)
	{
		fgets (line, 254, f);
		sscanf(line, "%*s %d", &ind);
		if (maxind < ind+1)
			maxind = ind+1;
	}

	if (symb)
		delete [] symb;
	symb = new Symbole[maxind];
	rewind (f);

	fgets (line, 254, f);
	sscanf(line, "%d", &nbsymb);

	for (i=0; i<nbsymb; i++)
	{
		fgets (line, 254, f);
		sscanf(line, "%*s %d %s", &ind, name);

		prepareName (fullname, name, (char *)symbDir.c_str());
		strcat (fullname, ".smb");
		symb[ind].read((char*)fullname, ind);
	}
	fclose (f);
	nbsymb = maxind;
	return 1;

}




