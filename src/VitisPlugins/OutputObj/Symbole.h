/***************************************************************************
this file is part of AMAPsim software
                             -------------------
    begin                : 1995
    copyright            : (C) 2002 by jef
    email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _SYMBOLE_H_
#define _SYMBOLE_H_

#include <stdio.h>
#include <math.h>

#include "common.h"

using namespace std;

class Point
{
	public :
		Point (void){col = 0;index=0;att=0;};
		int getCol (void){return col;};
		int getIndex (void){return index;};
		unsigned short getAtt (void){return att;};
		void Col (int c){col = c;};
		void Index (int i){index = i;};
		void Att (unsigned short a){att = a;};
		void read(FILE *f, int ){binreadswi(f,col);
			binreadswi(f,index); if (index < 0) index = -index;
			binreadsws(f,att);
		};
		Point &operator=(const Point &p)
		{
			col = p.col;
			index = p.index;
			att = p.att;
			return *this;
		};
	private :
		int    col;
		int    index;
		unsigned short   att;
};

class Face
{
	public :
		Face (void){nbpts=0;type = 0;dim=0;points=NULL;};
		~Face(void){delete [] (points);};
		int NbPts (void){return nbpts;};
		unsigned short getType(void){return type;};
		unsigned short getDim(void){return dim;};
		Point getPoint (int i){return points[i];};
		Point *getPointByAdress (int i){return &points[i];};
		void NbPts (int n){nbpts = n;}
		void Type(short t){type= t;};
		void Dim(unsigned short d){dim= d;};
		void Points(Point *p){points= p;};
		void read(FILE *f, int swap)
		{
			int i;
			binreadswi(f,nbpts);
			binreadsws(f,type);
			binreadsws(f,dim);
			points = new Point[nbpts];
			for (i=0; i<nbpts; i++)
				points[i].read (f, swap);
		};
		Face &operator=(const Face &f)
		{
			nbpts = f.nbpts;
			type = f.type;
			dim = f.dim;
			points = new Point[nbpts];
			for (int i=0; i<nbpts; i++)
			{
				points[i] = f.points[i];
			};
			return *this;
		}

	private :
		int     nbpts;
		unsigned short   type;
		unsigned short   dim;
		Point   *points;
};

class Pcoord
{
	public :
		Pcoord (){;};
		Pcoord &operator=(const Pcoord &p)
		{
			nx = p.nx;
			ny = p.ny;
			nz = p.nz;
			px = p.px;
			py = p.py;
			pz = p.pz;
			u = p.u;
			v = p.v;
			d = p.d;
			return *this;
		};
		Pcoord (float Nx, float Ny, float Nz, float Px, float Py, float Pz, float U, float V, float D)
		{ nx = Nx;
			ny = Ny;
			nz = Nz;
			px = Px;
			py = Py;
			pz = Pz;
			u = U;
			v = V;
			d = D;
		};
		void setPcoord (float Nx, float Ny, float Nz, float Px, float Py, float Pz, float U, float V, float D)
		{
			nx = Nx;
			ny = Ny;
			nz = Nz;
			px = Px;
			py = Py;
			pz = Pz;
			u = U;
			v = V;
			d = D;
		};
		void getCoord (float *Nx, float *Ny, float *Nz, float *Px, float *Py, float *Pz, float *U, float *V, float *D)
		{*Nx = nx;
			*Ny = ny;
			*Nz = nz;
			*Px = px;
			*Py = py;
			*Pz = pz;
			*U = u;
			*V = v;
			*D = d;
		};
		void read (FILE *f, int swap)
		{
			binreadswf(f,nx);
			binreadswf(f,ny);
			binreadswf(f,nz);
			binreadswf(f,px);
			binreadswf(f,py);
			binreadswf(f,pz);
			binreadswf(f,u);
			binreadswf(f,v);
			binreadswf(f,d);
		}
		float   nx;
		float   ny;
		float   nz;
		float   px;
		float   py;
		float   pz;
		float   u;
		float   v;
		float   d;
};

class Symbole
{
	public :
		Symbole (void){index = 0;nbfaces=0;nbtot=0;face=NULL;pts=NULL;name[0]=0;};
		Symbole (const Symbole &s)
		{
			strcpy (name, s.name);
			index = s.index;
			nbfaces	= s.nbfaces;
			nbtot	= s.nbtot;
			face = new Face[nbfaces];
			for (int i=0; i<nbfaces; i++)
				face[i] = s.face[i];
			pts = new Pcoord[nbtot];
			for (int i=0; i<nbtot; i++)
				pts[i] = s.pts[i];
			ptstrav	= s.ptstrav;
			for (int i=0; i<3; i++)
				color[i] = s.color[i];
			material= s.material;
		};
		~Symbole (void)
		{
			if (face != NULL)
				delete [] face;
			face = 0;
			if (pts != NULL)
				delete [] pts;
			pts = 0;
		}
		int getIndex(void){return index;};
		void setIndex(int i){index=i;};
		int NbFaces(void){return nbfaces;};
		int NbPts(void){return nbtot;};
		Face getFace(int i){return face[i];};
		Face *Faces(){return face;};
		Face *getFaceByAdress(int i){return &face[i];};
		Pcoord getCoord(int i){return pts[i];};
		Pcoord *getCoordByAdress(int i){return &pts[i];};
		Pcoord *Pts(){return pts;};
		Pcoord *getCoordTravByAdress(int i){return &ptstrav[i];};
		char *getName (){return (char*)name;};
		void setColor (float *c){color[0]=c[0];color[1]=c[1];color[2]=c[2];};
		void getColor (float *c){c[0]=color[0];c[1]=color[1];c[2]=color[2];};
		int getMaterial (void){return material;};
		void NbFaces (int n){nbfaces=n;};
		void NbPts (int n){nbtot=n;};
		void Faces (Face *f){face = f;};
		void Pts (Pcoord *p){pts = p;};
		void read (char *n, int ind)
		{
			FILE *f;
			char fullName[LGMAXFIC];
			int i, swap=1;

			strcpy (name, n);
			index = ind;
			strcpy (fullName, n);
			if ((f=fopen(fullName,"rb")) == NULL)
			{
				perror (fullName);
				return;
			}
			binreadswi(f,nbfaces);
			if (nbfaces < 0)
				nbfaces = -nbfaces;
			binreadswi(f,nbtot);
			if (nbtot < 0)
				nbtot = -nbtot;
			face = new Face[nbfaces];
			for (i=0; i<nbfaces; i++)
				face[i].read(f, swap);
			pts = new Pcoord[nbtot];
			ptstrav = new Pcoord[nbtot];
			for (i=0; i<nbtot; i++)
				pts[i].read(f, swap);

			fclose (f);
		};
		void CalculerPoints (float var1, float var2, float *mat)
		{
			float *ptrmat,deltavar,dilat,c0,c1,c2;
			float norm,pnorm[3],pface[3];
			int i,j;
			Pcoord *pt,*pttrav;

			deltavar = var2 - var1;
			for (i=0; i<nbtot; i++)
			{
				pt = getCoordByAdress(i);
				pttrav = getCoordTravByAdress(i);

				ptrmat = mat;
				for (j=0; j<3; j++)
				{
					pnorm[j]  = *ptrmat++ * pt->nx;
					pnorm[j] += *ptrmat++ * pt->ny;
					pnorm[j] += *ptrmat++ * pt->nz;
					ptrmat++;
				}
				c0 = pt->px;
				dilat = c0 * deltavar + var1;
				c1 = pt->py * dilat;
				c2 = pt->pz * dilat;
				ptrmat = mat;
				for (j=0; j<3; j++)
				{
					pface[j]  = *ptrmat++ * c0;
					pface[j] += *ptrmat++ * c1;
					pface[j] += *ptrmat++ * c2;
					pface[j] += *ptrmat++;
				}
				norm = sqrt (pnorm[0]*pnorm[0] + pnorm[1]*pnorm[1] + pnorm[2]*pnorm[2]);
				if (norm > 0)
				{
					pnorm[0] /= norm;
					pnorm[1] /= norm;
					pnorm[2] /= norm;
				}
				pttrav->nx = pnorm[0];
				pttrav->ny = pnorm[2];
				pttrav->nz = -pnorm[1];
				pttrav->px = pface[0] / 100.;
				pttrav->py = pface[2] / 100.;
				pttrav->pz = -pface[1] / 100.;
				pttrav->nx = pnorm[1];
				pttrav->ny = pnorm[2];
				pttrav->nz = -pnorm[0];
				pttrav->px = pface[1] / 100.;
				pttrav->py = pface[2] / 100.;
				pttrav->pz = -pface[0] / 100.;
			}
		};
	Symbole& operator=(const Symbole& s)
	{
		strcpy (name, s.name);
		index = s.index;
		nbfaces	= s.nbfaces;
		nbtot	= s.nbtot;
		face	= s.face;
		pts	= s.pts;
		ptstrav	= s.ptstrav;
		for (int i=0; i<3; i++)
			color[i] = s.color[i];
		material= s.material;

		return *this;
	}

	private :
		char name[255];
		int    index;
		int    nbfaces;
		int    nbtot;
		Face   *face;
		Pcoord *pts;
		Pcoord *ptstrav;
		float  color[3];
		int    material;
};

#endif

