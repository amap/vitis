/***************************************************************************
this file is part of AMAPsim software
                             -------------------
    begin                : 1995
    copyright            : (C) 2002 by jef
    email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdio.h>
#ifdef WIN32
	#include <windows.h>
#else
	#include <unistd.h>
#endif
#include <string.h>

#ifndef LGMAXFIC
#define LGMAXFIC 255
#endif

#define binreadt(f,n,t) fread(&n,sizeof(n),t,f)
#define binread(f,n) fread(&n,sizeof(n),1,f)
int swap (int &i);
long swapl (long &i);
int find_path (char *, char *);
int binreadswi (FILE *f, int &i);
int binreadswl (FILE *f, long &i);
int touille (FILE *f, float &i);
int binreadswf (FILE *f, float &i);
int binreadswt (FILE *f, float &i, int nb);
int binreadsws (FILE *f, unsigned short &i);

#endif
