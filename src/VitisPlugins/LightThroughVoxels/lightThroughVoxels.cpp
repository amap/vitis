/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WINDOWS
#include <conio.h>
#endif
#include <iostream>
#include <iomanip>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "VitisCoreSignalInterface.h"
#include "externlightThroughVoxels.h"
#include "lightThroughVoxels.h"



/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
lightThroughVoxelsPlugin::lightThroughVoxelsPlugin(const std::string& f, void *pp)
{
    if (initData(f))
    {
//        maskReady = false;
// 		Scheduler::instance()->signal_event(this->getPid(),NULL,1,3);
 		Scheduler::instance()->create_process(this,NULL,1,102);
    }

}

lightThroughVoxelsPlugin::lightThroughVoxelsPlugin(void *pp)
{
    Scheduler::instance()->create_process(this,NULL,1,102);
}

lightThroughVoxelsPlugin::~lightThroughVoxelsPlugin()
{
//    if (maskReady)
//    {
//        int n = 2*sizeCornerMask;
//        for (int i=0; i<n; i++)
//            delete cornerMask[i];
//
//        n = 2*sizeCenterMask+1;
//        for (int i=0; i<n; i++)
//            delete centerMask[i];
//
//        delete centerMask;
//        delete cornerMask;
//    }

}

bool lightThroughVoxelsPlugin::initData(const std::string& f)
{

//	ParamSet * param_H = new ParamSet(f);
//
//	if (!param_H->Exists())
//	{
//        cout << "unable to open "<<f<<" parameter file"<<endl;
//		return false;
//	}
//
//	if (param_H->getParameter("perceptionDistance"))
//	{
//		perceptionDistance = param_H->getParameter("perceptionDistance")->value();
//	}
//	else
//	{
//        cout << "unable to find parameter perceptionDistance in "<<f<<" parameter file"<<endl;
//        return false;
//	}

	return true;
}


void lightThroughVoxelsPlugin::process_event(EventData *)
{

	Scheduler *s = Scheduler::instance();
	float top_extern = s->getTopClock();

//    if (!maskReady)
//    {
        voxelSpace = VoxelSpace::instance();
//        computeMask();
//    }

cout << endl << "computing light through voxels at time " << top_extern << endl;

    updateLightThroughVoxels();

	if (s->getTopClock()+1 <= s->getHotStop())
	{
		s->signal_event(this->getPid(),NULL,s->getTopClock()+1,102);
	}
}

void lightThroughVoxelsPlugin::updateLightThroughVoxels()
{
//    int ii, jj;
//    float upperOpacity;
//    float surface;
//
//    for (int k=voxelSpace->ZSize()-1; k>=0; k--)
//    for (int i=0; i<voxelSpace->XSize(); i++)
//    for (int j=0; j<voxelSpace->YSize(); j++)
//    {
//        LightData *d = (LightData*)voxelSpace->at(i,j,k).getData();
//        if (d == NULL)
//        {
//            d = new LightData;
//            voxelSpace->addrAt(i,j,k)->setData(d);
//        }
//        // computing with point densities
//        d->opacity = 1./(1.+voxelSpace->at(i,j,k).content.size()*1000 / voxelVolume);
//        // compting with half the surface
//        surface = voxelSpace->at(i,j,k).containedSurface();
//        d->opacity = 1./(1. + surface*10000 / voxelVolume);
//
//        upperOpacity = 0;
//        for (int t=0; t<maskDepth; t++)
//        {
//            for (int m=-maskDepth; m<maskDepth; m++)
//            for (int n=-maskDepth; n<maskDepth; n++)
//            {
//                if (k+t >= voxelSpace->ZSize()-1)
//                {
//                    upperOpacity += Mask[t][m+maskDepth][n+maskDepth];
//                    continue;
//                }
//
//                if (i+m < 0)
//                {
//                    ii = voxelSpace->XSize() + i+m;
//                }
//                else if (i+m >= voxelSpace->XSize())
//                {
//                    ii = -voxelSpace->XSize() + i+m;
//                }
//                else
//                {
//                    ii = i+m;
//                }
//
//                if (j+n < 0)
//                {
//                    jj = voxelSpace->YSize() + j+n;
//                }
//                else if (j+n >= voxelSpace->YSize())
//                {
//                    jj = -voxelSpace->YSize() + j+n;
//                }
//                else
//                {
//                    jj = j+n;
//                }
//                LightData *dd = (LightData*)(voxelSpace->at(ii,jj,k+t+1).getData());
//                upperOpacity += Mask[t][m+maskDepth][n+maskDepth] * dd->opacity;
//            }
//        }
//        d->opacity *= upperOpacity;
//   }
//

    int imin, imax, jmin, jmax;
    float **upperLayer;
    float surface, coef, voxelSurface=voxelSpace->VoxelSize()*voxelSpace->VoxelSize();
    LightData *dd;
    Voxel ***space = voxelSpace->Space();

    upperLayer = (float**) new float*[voxelSpace->XSize()];
    for (int i=0; i<voxelSpace->XSize(); i++)
    {
        upperLayer[i] = new float[voxelSpace->YSize()];
        for (int j=0; j<voxelSpace->YSize(); j++)
        {
            upperLayer[i][j] = 1;
        }
    }

    for (int k=voxelSpace->ZSize()-1; k>=0; k--)
    {
//cout << "layer "<<k<< " integration"<<endl;
        // light in a voxel is the average upperLight
        for (int i=0; i<voxelSpace->XSize(); i++)
        for (int j=0; j<voxelSpace->YSize(); j++)
        {
            LightData *d = (LightData*)space[k][i][j].getVoxelData("light");
            if (d == NULL)
            {
                d = new LightData;
//                voxelSpace->addrAt(i,j,k)->setData(d);
                space[k][i][j].addVoxelData("light", d);
            }

            if (i-1 < 0)
            {
                imin = voxelSpace->XSize() - 1;
            }
            else
                imin = i - 1;

            if (i+1 >= voxelSpace->XSize())
            {
                imax = 0;
            }
            else
            {
                imax = i+1;
            }

            if (j-1 < 0)
            {
                jmin = voxelSpace->YSize() - 1;
            }
            else
                jmin = j - 1;

            if (j+1 >= voxelSpace->YSize())
            {
                jmax = 0;
            }
            else
            {
                jmax = j+1;
            }
            d->light = 0;
//            float maxLum = 0;
            d->light += upperLayer[imin][j];
//            if (maxLum < upperLayer[imin][j])
//                maxLum = upperLayer[imin][j];
            d->light += upperLayer[imin][jmin];
//            if (maxLum < upperLayer[imin][jmin])
//                maxLum = upperLayer[imin][jmin];
            d->light += upperLayer[imin][jmax];
//            if (maxLum < upperLayer[imin][jmax])
//                maxLum = upperLayer[imin][jmax];
            d->light += upperLayer[i][j];
//            if (maxLum < upperLayer[i][j])
//                maxLum = upperLayer[i][j];
            d->light += upperLayer[i][jmin];
//            if (maxLum < upperLayer[i][jmin])
//                maxLum = upperLayer[i][jmin];
            d->light += upperLayer[i][jmax];
//            if (maxLum < upperLayer[i][jmax])
//                maxLum = upperLayer[i][jmax];
            d->light += upperLayer[imax][j];
//            if (maxLum < upperLayer[imax][j])
//                maxLum = upperLayer[imax][j];
            d->light += upperLayer[imax][jmin];
//            if (maxLum < upperLayer[imax][jmin])
//                maxLum = upperLayer[imax][jmin];
            d->light += upperLayer[imax][jmax];
//            if (maxLum < upperLayer[imax][jmax])
//                maxLum = upperLayer[imax][jmax];
//            d->light += 2*maxLum;
            d->light /= 9;

//            d->light = upperLayer[imin][j];
//            if (d->light < upperLayer[imin][jmin])
//                d->light = upperLayer[imin][jmin];
//            if (d->light < upperLayer[imin][jmax])
//                d->light = upperLayer[imin][jmax];
//            if (d->light < upperLayer[i][j])
//                d->light = upperLayer[i][j];
//            if (d->light < upperLayer[i][jmin])
//                d->light = upperLayer[i][jmin];
//            if (d->light < upperLayer[i][jmax])
//                d->light = upperLayer[i][jmax];
//            if (d->light < upperLayer[imax][j])
//                d->light = upperLayer[imax][j];
//            if (d->light < upperLayer[imax][jmin])
//                d->light = upperLayer[imax][jmin];
//            if (d->light < upperLayer[imax][jmax])
//                d->light = upperLayer[imax][jmax];
//if (d->light > 0.5 && d->light < 1)
//{
//cout<<i<<" "<<j<<" "<<k<<" "<<d->light<<endl;
//}
        }

//cout << "layer "<<k<< " maj"<<endl;
        for (int i=0; i<voxelSpace->XSize(); i++)
{
//cout << k<<" i "<<i<<endl;
        for (int j=0; j<voxelSpace->YSize(); j++)
        {
//            surface = voxelSpace->addrAt(i,j,k)->ContainedSurface();
            surface = space[k][i][j].ContainedSurface();
            coef = voxelSurface / (voxelSurface + surface);
            upperLayer[i][j] *= coef*coef*coef*coef;
        }
}

    }

    for (int i=0; i<voxelSpace->XSize(); i++)
    {
        delete upperLayer[i];
    }
    delete upperLayer;

    if (Scheduler::instance()->getTopClock() == Scheduler::instance()->getHotStop())
    {
        std::stringstream sout;
        sout << "voxelSpace" << Scheduler::instance()->getTopClock()<< ".vxl";
        std::string fname (sout.str());
        voxelSpace->output(fname);
    }
//    int prec = cout.precision();
//    int width = cout.width();
//    cout.precision(2);
//    cout.width(4);
//    for (int k=voxelSpace->ZSize()-1; k>=0; k--)
//    {
//        cout << "k: "<< k << endl;
//        for (int i=0; i<voxelSpace->XSize(); i++)
//        {
//            cout << " i: "<< i << " (";
//            for (int j=0; j<voxelSpace->YSize(); j++)
//            {
//                LightData *dd = (LightData*)(voxelSpace->at(i,j,k).getData());
//                dd->light = expf (-dd->opacity*0.5);
//                dd->light = dd->opacity;
//                cout << ((LightData*)(voxelSpace->at(i,j,k).getData()))->light<< "["<< ((LightData*)(voxelSpace->at(i,j,k).getData()))->opacity<<"]"<<" ";
//            }
//            cout << ")"<<endl;
//        }
//    }
//    cout.width(width);
//    cout.precision(prec);
//        cout << "space "<<voxelSpace->XSize()<< " "<<voxelSpace->YSize()<<endl;
//        cout << " voxelsize "<< voxelSpace->VoxelSize() <<" Volume "<<voxelVolume<< endl;

}

//void lightThroughVoxelsPlugin::computeMask()
//{
//    maskReady = true;
//    int beginZone, endZone, nbVoxelsInMask=0;
//
//    voxelVolume = voxelSpace->VoxelSize() * voxelSpace->VoxelSize() * voxelSpace->VoxelSize();
//    computeDepthMask();
//
//    maskWidth = 1 + 2 * maskDepth;
//    Mask = (float***) new float**[maskDepth];
//    for (int i=0; i<maskDepth; i++)
//    {
//        Mask[i] = (float**)new float*[maskWidth];
//        beginZone = i+1;
//        endZone = maskWidth - (i+1);
//        for (int j=0; j<maskWidth; j++)
//        {
//            Mask[i][j] = (float*) new float[maskWidth];
//            for (int k=0; k<maskWidth; k++)
//            {
//                if (   k < beginZone
//                    || j < beginZone
//                    || k >= endZone
//                    || j >= endZone)
//                {
//                    Mask[i][j][k] = 0;
//                }
//                else
//                {
//                    nbVoxelsInMask ++;
//                    Mask[i][j][k] = 1;
//                }
//            }
//        }
//    }
//    for (int i=0; i<maskDepth; i++)
//    {
//        for (int j=0; j<maskWidth; j++)
//        {
//            for (int k=0; k<maskWidth; k++)
//            {
//                Mask[i][j][k] /= nbVoxelsInMask;
//            }
//        }
//    }
//}
//
//int lightThroughVoxelsPlugin::computeDepthMask()
//{
//    float v = perceptionDistance;
//    if (v > voxelSpace->VoxelSize()*voxelSpace->ZSize())
//        v = voxelSpace->VoxelSize()*voxelSpace->ZSize();
//    v /= voxelSpace->VoxelSize();
//    maskDepth = ceil(v);
//    return maskDepth;
//}

//extern "C" LIGHTTHROUGHVOXELS_EXPORT  lightThroughVoxelsPlugin * StartPlugin (const std::string& f, void *p)
//{
//	std::cout << "loading lightThroughVoxelsPlugin module with parameter " << f << std::endl;
//
//	return new lightThroughVoxelsPlugin (f, p) ;
//}





