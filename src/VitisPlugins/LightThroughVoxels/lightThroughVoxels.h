/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef LIGHTTHROUGHVOXELS
#define LIGHTTHROUGHVOXELS

using namespace std;

#include "scheduler.h"
#include "Voxel.h"
#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "externlightThroughVoxels.h"
#include "ParamFile.h"

#include <vector>

class LightData;

class LightData : public VoxelData
{
    public :
    float light;
    virtual std::string to_string(){
        std::ostringstream oss;
		// écrire la valeur dans le flux
		oss << light;
		// renvoyer une string
		return oss.str();
    };
    virtual VoxelData *duplicate (){
        LightData *v = new LightData;
        v->light = light;
        return v;
    };
};

class LIGHTTHROUGHVOXELS_EXPORT lightThroughVoxelsPlugin : public VProcess
{
public:
	lightThroughVoxelsPlugin(const std::string& f, void *p);
	lightThroughVoxelsPlugin(void *p);
	lightThroughVoxelsPlugin() {voxelSpace = VoxelSpace::instance();};
	virtual ~lightThroughVoxelsPlugin();
	void reset();
	void process_event(EventData *);

	bool initData(const std::string& f);
	void updateLightThroughVoxels();

protected:
	VoxelSpace *voxelSpace;
    float voxelVolume;

// 	float angle;
// 	float perceptionDistance;

//    int maskDepth;
//    int maskWidth;
//    float ***Mask;
//	void computeMask();
//	int computeDepthMask();
//	bool maskReady;

};

#if !defined STATIC_LIB
extern "C" LIGHTTHROUGHVOXELS_EXPORT  lightThroughVoxelsPlugin * StartPlugin (void *p)
{
	std::cout << "loading lightThroughVoxelsPlugin module" << std::endl;

	return new lightThroughVoxelsPlugin (p) ;
}
#endif



#endif

