/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _RAWGLDSFORMAT_H
#define _RAWGLDSFORMAT_H
#include "bfstream.h"
#include "OutputFormat.h"
//#include "libglds.h"
//using namespace glds;
using namespace std;

class RawGldsFormat : public OutputFormat
{

	ofstream stream;
	string baseName;

	//Document doc;

	//Node currentNode;

	/// \brief Current "port�e"
	int currentPortee;

	/// \brief Number of elements already written
	int currentNbElem;

	int maxDecompLevel;

	int id;

	public:

	RawGldsFormat(const std::string &finame);

	virtual ~RawGldsFormat(void);

	virtual int initFormat(VitisObject * vobj);

	virtual int printData(VitisObject *);

	//int printAxe(class GeomBranc * gb, Node & curNod);
	//void printAdditionalData (VitisObject * o, Node & curNod);

	//int printDataInOrder(class GeomBranc * vobj, Node & curNod);

	int printAxe(class GeomBranc * gb, string decal);
	void printAdditionalData (VitisObject * o, string decal);

	int printDataInOrder(class GeomBranc * vobj, string decal);


	virtual bool printHeader (const std::string &headerf);

	//virtual void printRecord(class GeomElemCone * ge, Node & ligcur);
	virtual void printRecord(class GeomElemCone * ge, string decal);

	void printSubStruct(class GeomManager & gtr);
};

#endif

