/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "RawGldsFormat.h"
#include <algorithm>
#include <sstream>
#include <vector>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "GeomBranc.h"
#include "Plant.h"
#include "UtilMath.h"
#include "Plant.h"
#include "DebugNew.h"


RawGldsFormat::RawGldsFormat(const std::string &finame):OutputFormat(finame+".opf"),stream((finame+".opf").c_str())
{
	currentPortee=0;
	currentNbElem=0;
	id = 1;
	baseName = finame;
	while (   baseName.at(baseName.length()-1) == '_'
		   || (   baseName.at(baseName.length()-1) >= '0'
		       && baseName.at(baseName.length()-1) <= '9'))
		baseName.resize(baseName.length()-1);

}

RawGldsFormat::~RawGldsFormat(void)
{



}


int RawGldsFormat::initFormat(VitisObject * vobj)
{
	GeomManager * gb = (GeomManager *) vobj;
	Branc *b = gb->getPlant()->getTopology().getTrunc()->getBranc();

	maxDecompLevel = gb->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());

	//Node root = doc.createRootNode("gtds");

	//Text hlig=root.addChild("headerlig");
	//hlig.setValue("noHeader");

//	hlig=LineTreeFormat::computeKey(this->filename);

	//currentNode= root;

	stream << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" << endl;
	stream << "<opf>" << endl;
	stream << "\t<headerlig>noHeader</headerlig>" << endl;

	string name;
	ifstream f;
	f.open((baseName+".headeropf").c_str());
	if (f.is_open())
	{
		char str[512];
		while (!f.eof())
		{
			f.getline (str, 512);
			stream << str << endl;
		}
		f.close();
	}


	return 1;
}

void RawGldsFormat::printSubStruct(GeomManager & gtr)
{

	int i,indice;
	std::vector<GeomBranc *> & vecGeomBranc = gtr.getGeomBrancStack();
	std::vector<GeomSuperSimpl *> & vecGeomSSimp= gtr.getGeomSimplStack();
	std::vector<GeomSuperSimpl *>::iterator it_ssimp;

	Plant * plt= gtr.getPlant();

	//Node nsstruct=currentNode.addChild("topo_substructure");

	for(it_ssimp=vecGeomSSimp.begin(); it_ssimp != vecGeomSSimp.end(); ++it_ssimp)
	{

		if((*it_ssimp)!= NULL)
		{
			if((*it_ssimp)->secteurAng!=NULL)
			{
				for(i=0; i<plt->getConfigData().cfg_supersimplif ; i++)
				{
					indice=(*it_ssimp)->secteurAng[i];
					if(indice!=-1)
					{
						stream << "\t\t\t<sstruct ref\"" << indice << " class=\"sstruct\">" << endl;

						printAxe(vecGeomBranc[indice], string("\t\t\t\t"));

						stream << "\t\t\t</sstruct>" << endl;
					}
				}
			}
		}
	}
}


int RawGldsFormat::printData(VitisObject * vobj)
{

	GeomBranc * gb = (GeomBranc *) vobj;

	Plant * plt= gb->getBranc()->getPlant();

	stream << "\t<Plant class=\"plant\" nb_axes=\"" << plt->getTopology().NbHierarc() << "\">" << endl;

	if(plt->getConfigData().cfg_supersimplif)
	{
		printSubStruct(plt->getGeometry());
	}
	else
	{
		printDataInOrder(gb, string("\t\t"));
	}

	stream << "\t</Plant>" << endl;
	stream << "</opf>" << endl;
	//doc.writeToFile(filename);

	return 1;

}

//void RawGldsFormat::printAdditionalData (VitisObject *o, Node & curNod)
void RawGldsFormat::printAdditionalData (VitisObject *o, string decal)
{
	for (int i=0; i<o->additionalDataNumber(); i++)
	{
		if (o->isAdditionalDataPrintable (i))
		{
			PrintableObject *p=(PrintableObject*)o->getAdditionalData(i);

			stream << decal << "\t<" << o->getAdditionalDataCriteria(i) << ">" << endl;
			stream << decal << "\t\t" << p->to_string() << endl;
			stream << decal << "\t</" << o->getAdditionalDataCriteria(i) << ">" << endl;

			//Text t=curNod.addChild(o->getAdditionalDataCriteria(i));
			//t.setValue (p->to_string());
		}
	}
}

//int RawGldsFormat::printAxe(GeomBranc * gb, Node & curNod)
int RawGldsFormat::printAxe(GeomBranc * gb, string decal)
{
	int i, j, k;

	Vector3 dp, dssb ,ds, t;

	//Node axefils;

	GeomBrancCone * curGeomCone;

	//Node axe =curNod, curNode=curNod;
	//Node nu[100], ne;

	string decalLevel[100];
	bool closeLevel[100];
	decalLevel[0] = decal+"\t";
	for (i=1; i<maxDecompLevel; i++)
		decalLevel[i] = decalLevel[i-1]+"\t";

	currentPortee=0;

	//printAdditionalData (gb, axe);
	//printAdditionalData (gb->getBranc(), axe);
	printAdditionalData (gb, decal);
	printAdditionalData (gb->getBranc(), decal);

	Branc *b = gb->getBranc();
	DecompAxeLevel *d = b;
	b->startPosit();
	b->positOnFirstElementOfSubLevel(maxDecompLevel);

	//nu[0] = axe;

	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
		for (j=b->getCurrentElementIndexOfSubLevel(maxDecompLevel); j<=((GeomElemCone *)gb->getElementsGeo().at(i))->getPosInBranc(); j++)
		{
			for (k=1; k<maxDecompLevel-1; k++)
			{
				d = b->getCurrentElementOfSubLevel(k);
				if (d->getCurrentElementIndexOfSubLevel(maxDecompLevel) == 0)
				{
					std::string str;
					str = "topo_LEVEL";
					str.append (to_string(k));

					stream << decal << decalLevel[k] << "<" << str;

					str = "LEVEL";
					str.append (to_string(k));

					stream << decal << decalLevel[k] << " class=\"" << d->getName() << "\" id=\"" << id++ << "\">" << endl;

					stream << decalLevel[k] << "\t<index>" << d->getParentOfLevel(k-1)->getCurrentElementIndexOfSubLevel(k) << "</index>" << endl;
					printAdditionalData (d, decalLevel[k]+"\t");
				}
			}

			stream << decal << decalLevel[k] << "\t<topo_LEVEL" << maxDecompLevel-1 << " id=\"" << id++ << "\" class=\"" << b->getCurrentElementOfSubLevel(maxDecompLevel)->getName() << "\">" << endl;
			stream << decalLevel[k] << "\t<index>" << d->getCurrentElementIndexOfSubLevel(maxDecompLevel) << "</index>" << endl;
			printAdditionalData (b->getCurrentElementOfSubLevel(maxDecompLevel), decalLevel[k]+"\t");

			for(k=0; k<gb->getElementsGeo().at(i)->getGeomChildren().size(); k++)
			{
				curGeomCone = (GeomBrancCone*)gb->getElementsGeo().at(i)->getGeomChildren().at(k);
				if (curGeomCone->getAxe()->indexOnBearerAtLowestLevel == b->getCurrentElementIndexOfSubLevel(maxDecompLevel))
				{
					int ind;
					if(curGeomCone->getGeomBrancClone())
						ind = curGeomCone->getGeomBrancClone()->getNum();
					else
						ind = curGeomCone->getNum();

					//axefils.setAttribute("refto",to_string(ind));

					stream << decal << decalLevel[k] << "<\t\tclone refto=\"" << ind << "\">" << endl;
					Matrix4 & matTrans=curGeomCone->getCurrentTransform();

					dssb=matTrans.getNormalVector();
					ds=matTrans.getSecondaryVector();
					dp=matTrans.getMainVector();
					t=matTrans.getTranslation();

			// We create the coordinat matrix
					std::vector<std::vector<float> > tempMat;
					tempMat.resize(3);
			// first line of the matrix
					tempMat[0].resize(4);
					tempMat[0][0] = dp[0];
					tempMat[0][1] = dssb[0];
					tempMat[0][2] = ds[0];
					tempMat[0][3] = t[0];
			// second line of the matrix
					tempMat[1].resize(4);
					tempMat[1][0] = dp[1];
					tempMat[1][1] = dssb[1];
					tempMat[1][2] = ds[1];
					tempMat[1][3] = t[1];
			// third line of the matrix
					tempMat[2].resize(4);
					tempMat[2][0] = dp[2];
					tempMat[2][1] = dssb[2];
					tempMat[2][2] = ds[2];
					tempMat[2][3] = t[2];

					//Matrix<float> l_mat = axefils.addChild("mat");
					//l_mat.setValue(tempMat);

					stream << decal << decalLevel[k] << "\t\t\t<mat>" << endl;
					stream << decal << decalLevel[k] << "\t\t\t\t" << dp[0] << " " << dssb[0] << " " << ds[0] << " " << t[0] << endl;
					stream << decal << decalLevel[k] << "\t\t\t\t" << dp[1] << " " << dssb[1] << " " << ds[1] << " " << t[1] << endl;
					stream << decal << decalLevel[k] << "\t\t\t\t" << dp[2] << " " << dssb[2] << " " << ds[2] << " " << t[2] << endl;
					stream << decal << decalLevel[k] << "\t\t\t</mat>" << endl;
					stream << decal << decalLevel[k] << "<\t\t/clone>" << endl;
				}
			}

			for (k=maxDecompLevel-2; k>0; k--)
			{
				if (b->getCurrentElementOfSubLevel(k)->getElementNumberOfSubLevel(maxDecompLevel) == b->getCurrentElementOfSubLevel(k)->getCurrentElementIndexOfSubLevel(maxDecompLevel)+1)
					closeLevel[k] = true;
				else
					closeLevel[k] = false;
			}

			if (j < ((GeomElemCone *)gb->getElementsGeo().at(i))->getPosInBranc())
			{
				stream << decal << decalLevel[maxDecompLevel-1] << "\t</topo_LEVEL" << maxDecompLevel-1 << ">" << endl;
				for (k=maxDecompLevel-2; k>0; k--)
				{
//					if (b->getCurrentElementOfSubLevel(k)->getElementNumberOfSubLevel(maxDecompLevel) == b->getCurrentElementOfSubLevel(k)->getCurrentElementIndexOfSubLevel(maxDecompLevel)+1)
					if (closeLevel[k])
					{
						std::string str;
						str = "topo_LEVEL";
						str.append (to_string(k));

						stream << decal << decalLevel[k] << "</" << str << ">" << endl;
					}
				}
			}

			b->positOnNextElementOfSubLevel(maxDecompLevel);
		}

		printRecord((GeomElemCone *)gb->getElementsGeo().at(i), decal+decalLevel[maxDecompLevel-1]);

		stream << decal << decalLevel[maxDecompLevel-1] << "\t</topo_LEVEL" << maxDecompLevel-1 << ">" << endl;

		for (k=maxDecompLevel-2; k>0; k--)
		{
//			if (b->getCurrentElementOfSubLevel(k)->getElementNumberOfSubLevel(maxDecompLevel) == b->getCurrentElementOfSubLevel(k)->getCurrentElementIndexOfSubLevel(maxDecompLevel))
			if (closeLevel[k])
			{
				std::string str;
				str = "topo_LEVEL";
				str.append (to_string(k));

				stream << decal << decalLevel[k] << "</" << str << ">" << endl;
			}
		}

		currentNbElem++;

		currentPortee++;

	}


	b->endPosit();

	return 1;
}


int RawGldsFormat::printDataInOrder(GeomBranc * gb, string decal)
{
	int i, j, k;

	Plant *p = gb->getBranc()->getPlant();

	string decalLevel[100];
	bool closeLevel[100];
	decalLevel[0] = decal+"\t";
	for (i=1; i<maxDecompLevel; i++)
		decalLevel[i] = decalLevel[i-1]+"\t";

	stream << decal << "<topo_axe id=\"" << id++ << " class=\"" << gb->getBranc()->getName() << "\">" << endl;

	printAdditionalData (gb, decal);

	currentPortee=0;

	gb->getBranc()->startPosit();
	gb->getBranc()->positOnFirstElementOfSubLevel(maxDecompLevel);

	Branc *b = gb->getBranc();
	printAdditionalData (b, decal);
	DecompAxeLevel *d = b;

	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
		int pos = ((GeomElemCone *)gb->getElementsGeo().at(i))->getPosInBranc();
		for (j=b->getCurrentElementIndexOfSubLevel(maxDecompLevel); j<=pos; j++)
		{
			for (k=1; k<maxDecompLevel-1; k++)
			{
				d = b->getCurrentElementOfSubLevel(k);
				if (d->getCurrentElementIndexOfSubLevel(maxDecompLevel) == 0)
				{
					std::string str;
					str = "topo_LEVEL";
					str.append (to_string(k));

					stream << decal << decalLevel[k] << "<" << str;

					str = "LEVEL";
					str.append (to_string(k));

					stream << " class=\"" << d->getName() << "\" id=\"" << id++ << "\">" << endl;

					stream << decalLevel[k] << "\t<index>" << d->getParentOfLevel(k-1)->getCurrentElementIndexOfSubLevel(k) << "</index>" << endl;
					printAdditionalData (d, decalLevel[k]+"\t");
				}
			}

			stream << decal << decalLevel[k] << "\t<topo_LEVEL" << maxDecompLevel-1 << " id=\"" << id++;
			stream << "\" class=\"" << d->getCurrentElementOfSubLevel(maxDecompLevel)->getName() << "\">" << endl;
			stream << decalLevel[k] << "\t<index>" << d->getCurrentElementIndexOfSubLevel(maxDecompLevel) << "</index>" << endl;
			printAdditionalData (b->getCurrentElementOfSubLevel(maxDecompLevel), decalLevel[k]+"\t");

			for(k=0; k<gb->getElementsGeo().at(i)->getGeomChildren().size(); k++)
			{
				GeomBranc *gbp = gb->getElementsGeo().at(i)->getGeomChildren().at(k);
				if (gbp->getAxe()->indexOnBearerAtLowestLevel == b->getCurrentElementIndexOfSubLevel(maxDecompLevel))
					printDataInOrder(gbp, decal+decalLevel[maxDecompLevel-1]+"\t");
			}

			for (k=maxDecompLevel-2; k>0; k--)
			{
				if (b->getCurrentElementOfSubLevel(k)->getElementNumberOfSubLevel(maxDecompLevel) == b->getCurrentElementOfSubLevel(k)->getCurrentElementIndexOfSubLevel(maxDecompLevel)+1)
					closeLevel[k] = true;
				else
					closeLevel[k] = false;
			}

			if (j < pos)
			{
				stream << decal << decalLevel[k] << "\t</topo_LEVEL" << maxDecompLevel-1 << ">" << endl;
				for (k=maxDecompLevel-2; k>0; k--)
				{
//					if (b->getCurrentElementOfSubLevel(k)->getElementNumberOfSubLevel(maxDecompLevel) == b->getCurrentElementOfSubLevel(k)->getCurrentElementIndexOfSubLevel(maxDecompLevel)+1)
					if (closeLevel[k])
					{
						std::string str;
						str = "topo_LEVEL";
						str.append (to_string(k));

						stream << decal << decalLevel[k] << "</" << str << ">" << endl;
					}
				}
			}

			b->positOnNextElementOfSubLevel(maxDecompLevel);
		}

		printRecord((GeomElemCone *)gb->getElementsGeo().at(i), decal+decalLevel[maxDecompLevel-1]+"\t");

		stream << decal << decalLevel[k] << "\t</topo_LEVEL" << maxDecompLevel-1 << ">" << endl;
		for (k=maxDecompLevel-2; k>0; k--)
		{
			if (closeLevel[k])
			{
				std::string str;
				str = "topo_LEVEL";
				str.append (to_string(k));

				stream << decal << decalLevel[k] << "</" << str << ">" << endl;
			}
		}

		currentNbElem++;

		currentPortee++;

	}

	stream << decal << "</topo_axe>" << endl;

	gb->getBranc()->endPosit();

	return 1;


}

bool RawGldsFormat::printHeader (const std::string &headerf)
{

	return true;

}


//void RawGldsFormat::printRecord(GeomElemCone * ge, Node & lig)
void RawGldsFormat::printRecord(GeomElemCone * ge, string decal)
{
	int numBr;


	numBr=ge->getPtrBrc()->getNum();

	Matrix4  __matrix= ge->getMatrix();

	//__matrix=Matrix4::translation(ge->getPtrBrc()->getBranc()->getPlant()->getGeometry().getPosition())*__matrix;

	Vector3 dp, dssb ,ds, t;

	dssb=__matrix.getNormalVector();
	ds=__matrix.getSecondaryVector();
	dp=__matrix.getMainVector()*ge->getLength();
	t=__matrix.getTranslation();

	//LIG DATA
	// we start a lig node

	stream << decal << "<geom_elm class=\"Mesh\" id=\"" << currentNbElem << "\">" << endl;

	stream << decal << "\t<ShapeIndex>" << ge->getSymbole() << "</ShapeIndex>" << endl;

	// We create the coordinat matrix
	std::vector<std::vector<float> > tempMat;
	tempMat.resize(3);
	// first line of the matrix
	tempMat[0].resize(4);
	tempMat[0][0] = dp[0];
	tempMat[0][1] = dssb[0];
	tempMat[0][2] = ds[0];
	tempMat[0][3] = t[0];
	// second line of the matrix
	tempMat[1].resize(4);
	tempMat[1][0] = dp[1];
	tempMat[1][1] = dssb[1];
	tempMat[1][2] = ds[1];
	tempMat[1][3] = t[1];
	// third line of the matrix
	tempMat[2].resize(4);
	tempMat[2][0] = dp[2];
	tempMat[2][1] = dssb[2];
	tempMat[2][2] = ds[2];
	tempMat[2][3] = t[2];

	stream << decal << "\t<mat>" << endl;
	stream << decal << "\t\t" << dp[0] << " " << dssb[0] << " " << ds[0] << " " << t[0] << endl;
	stream << decal << "\t\t" << dp[1] << " " << dssb[1] << " " << ds[1] << " " << t[1] << endl;
	stream << decal << "\t\t" << dp[2] << " " << dssb[2] << " " << ds[2] << " " << t[2] << endl;
	stream << decal << "\t</mat>" << endl;
	stream << decal << "\t<dUp>" << ge->getTopDiam() << "</dUp>" << endl;
	stream << decal << "\t<dDwn>" << ge->getBottomDiam() << "</dDwn>" << endl;
	printAdditionalData (ge, decal);

	stream << decal << "</geom_elm>" << endl;

	//Matrix<float> l_mat = lig.addChild("mat");
	//l_mat.setValue(tempMat);

	//Number<float> l_haut = lig.addChild("dUp");
	//l_haut = ge->getTopDiam();

	//Number<float> l_bas = lig.addChild("dDwn");
	//l_bas = ge->getBottomDiam();

	//printAdditionalData (ge, lig);
}



