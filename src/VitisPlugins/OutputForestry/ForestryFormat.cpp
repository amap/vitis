/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#include <windows.h>
#endif
#include "ForestryFormat.h"
#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "Plant.h"
#include "OutputForestry.h"



ForestryFormat::ForestryFormat(const std::string &finame):OutputFormat(finame),__profstream(finame.c_str())
{
	currentPortee=0;
	cptID=0;
}

ForestryFormat::~ForestryFormat(void)
{
	__profstream.close();
}

bool ForestryFormat::printHeader(const std::string &headerf)
{
	return true;
}

bool ForestryFormat::printHeader(Plant *plt)
{
	std::string *s = (std::string*)plt->getAdditionalData("diff");

	if (s)
	{
		plt->addAdditionalData("outputDiffusionFicName", (void*)outputName.c_str());

		__profstream << "#Tree_profile_for "<<plt->getParamName(std::string("AMAPSimMod"))<<" computed_at "<<Scheduler::instance()->getTopClock()<<" whith "<<*s<<" as_diffusion_law"<<std::endl;
	}
	else
		__profstream << "#Tree_profile_for "<<plt->getParamName(std::string("AMAPSimMod"))<<" computed_at "<<Scheduler::instance()->getTopClock()<<std::endl;

	__profstream << "#for each annual shoot : the number of growth cycles, the length, the number of borne branches" << std::endl;
	__profstream << "#for each branch inside the annual shoot : the base diameter, the number and surface of borne leaves" << std::endl;

	return true;
}


int ForestryFormat::initFormat(VitisObject * vobj)
{
	printHeader((Plant*)vobj);

	return 1;

}




//raw writing
int ForestryFormat::printData(VitisObject * vobj)
{

	OutputForestry *o = (OutputForestry *)vobj;
	__profstream << "Total_height\t" << o->LngTrunk() << "\tdbh\t" << o->Dbh() << "\tfirst_branch_height\t" << o->FirstBranchHeight() <<std::endl;
	PA pa;
	__profstream << "Annual_shoot\tlength\tnb_cycles\tnb_branches\tBranch\tbase_diameter\tnb_borne_leaves\tleaves_surface" << std::endl;
	for (int i=0; i<o->Vpa().size(); i++)
	{
		__profstream << i << "\t" << o->Vpa().at(i).lng << "\t" << o->Vpa().at(i).nbCycles << "\t" << o->Vpa().at(i).nbBranch << std::endl;
		for (int j=0; j<o->Vpa().at(i).vbrch.size(); j++)
		{
			Brch *brch = o->Vpa().at(i).vbrch.at(j);
			__profstream << "\t\t\t\t" << j << "\t" << brch->diam;
			__profstream << "\t" << brch->nbLeaves;
			__profstream << "\t" << brch->leavesSurface;
			__profstream << std::endl;
		}

	}

	return 1;

}


