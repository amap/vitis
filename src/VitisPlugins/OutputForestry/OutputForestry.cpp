/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <string.h>

#include "externOutputForestry.h"
#include "OutputForestry.h"
#include "Plant.h"
#include "ForestryFormat.h"
#include "DebugNew.h"
#include "defAMAP.h"
#include "BrancAMAP.h"
#include "PlantAMAP.h"
#include "Bud.h"
#include "GrowthUnitAMAP.h"

#include "config.inl"

//Noter que DP = Z
//DS = X
//ECHEL [0] = echel pour dp -> Z


OutputForestry::OutputForestry():Output()
{
	lngTrunk = 0;
	dbh = -1;
	firstBranchHeight = -1;
	vpa.clear();
}

OutputForestry::~OutputForestry(void)
{
}



void OutputForestry::freeData ()
{
	v_plt->getGeometry().clear();
	vpa.clear();
}

void OutputForestry::computeDataToOutput ()
{

	int geoSimp = v_plt->getConfigData().cfg_geoSimp;
	v_plt->getConfigData().cfg_geoSimp = 0;
	v_plt->computeGeometry();
	v_plt->getConfigData().cfg_geoSimp = geoSimp;

	BrancAMAP *trunk = (BrancAMAP*)v_plt->getTopology().getTrunc()->getBranc();
	GeomBranc *geomTrunk = (GeomBranc *)v_plt->getGeometry().getGeomBrancStack()[0];
	GrowthUnitAMAP *GU;
	PA *pa;
	Brch *brch;

	trunk->startPosit();
	trunk->positOnFirstEntnWithinBranc();

	vpa.resize(trunk->getGuNumber());
	for (int i=0; i<vpa.size(); i++)
	{
		vpa.at(i).lng = 0;
		vpa.at(i).nbBranch = 0;
		vpa.at(i).nbCycles = 0;
		vpa.at(i).vbrch.clear();
	}

	std::vector <GeomElem *> geom;
	geom = geomTrunk->getElementsGeo();
	lngTrunk = 0;

	for (int i=0; i<geom.size(); i++)
	{
		GeomElemCone *g = (GeomElemCone *)geom.at(i);

		vpa.at(trunk->getCurrentGuIndex()).nbCycles = trunk->getCurrentGu()->getElementNumberOfSubLevel(GROWTHCYCLE);

		float lng = g->getLength();
		vpa.at(trunk->getCurrentGuIndex()).lng += lng;
		lngTrunk += lng;
		if (   lngTrunk > 130
			&& dbh == -1)
			dbh = g->getBottomDiam();

		for (int j=0; j<g->getGeomChildren().size(); j++)
		{
			BrancAMAP *borne = (BrancAMAP *)g->getGeomChildren().at(j)->getBranc();
			if (borne->nature != SimpleImmediateOrgan)
			{
				if (g->getGeomChildren().at(j)->getElementsGeo().size() > 0)
				{
					vpa.at(trunk->getCurrentGuIndex()).nbBranch ++;

					brch = new Brch;

					brch->diam = ((GeomElemCone*)g->getGeomChildren().at(j)->getElementsGeo().at(0))->getBottomDiam();
					brch->nbLeaves = 0;
					brch->leavesSurface = 0;
					computeBranchValues ((GeomBranc*)g->getGeomChildren().at(j), brch);

					vpa.at(trunk->getCurrentGuIndex()).vbrch.push_back (brch);

					if (firstBranchHeight == -1)
						firstBranchHeight = lngTrunk;
				}
			}
		}

		trunk->positOnNextEntnWithinBranc();

	}


	trunk->endPosit();
}

void OutputForestry::writeData (const std::string &finame)
{

	int i;
	char iname[256], oname[256];
	strcpy (iname, finame.data());
	prepareName (oname, iname);

	std::stringstream sout;
	sout << oname;
	sout<< "_"<<v_plt->getConfigData().randSeed;
	std::string fname (sout.str());
//	const std::string fname (oname);

	outFormat= new ForestryFormat(fname+".dap");

	outFormat->initFormat(v_plt);

	std::cout<<"Output tree data for forestry comparison"<<std::endl;

	outFormat->printData(this);

	delete outFormat;

}

void OutputForestry::computeBranchValues (GeomBranc *g, Brch *brch)
{
	BrancAMAP *b = (BrancAMAP *)g->getBranc();

	if (b->nature == SimpleImmediateOrgan)
	{
		GeomElemCone *e = (GeomElemCone *)g->getElementsGeo().at(0);
		brch->nbLeaves ++;
		brch->leavesSurface += e->getLength() * e->getBottomDiam();
		return;
	}

	for (int i=0; i<g->getElementsGeo().size(); i++)
		for (int j=0; j<g->getElementsGeo().at(i)->getGeomChildren().size(); j++)
			computeBranchValues ((GeomBranc *)g->getElementsGeo().at(i)->getGeomChildren().at(j), brch);
}



#if !defined STATIC_LIB
extern "C" OUTPUTFORESTRY_EXPORT Output * StartPlugin ()
{
	return new OutputForestry();
}
#endif


