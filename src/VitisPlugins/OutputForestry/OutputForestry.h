/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file OutputProfile.h
///			\brief geometrical output.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __OUTPUTFORESTRY_H__
#define __OUTPUTFORESTRY_H__
#include "Output.h"
#include "GeomManager.h"
#include "externOutputForestry.h"

struct Brch {
	float diam;
	int nbLeaves;
	float leavesSurface;
};
struct PA {
	int nbBranch;
	int nbCycles;
	float lng;
	vector <Brch*> vbrch;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class OutputForestry
///			\brief Forestry dedicated output class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class OUTPUTFORESTRY_EXPORT OutputForestry: public Output
{
	private:
		float lngTrunk;
		float dbh;
		float firstBranchHeight;
		vector <PA> vpa;

	public:

		/// \brief Default constructor
		OutputForestry();

		/// \brief Destructor
		virtual ~OutputForestry(void);

		/// \brief Compute geometrical data to be output (lauch geommanager)
		virtual void computeDataToOutput ();


		/// \see Output
		virtual void freeData ();

		/// \see Output
		virtual void writeData (const std::string & finame);

		void computeBranchValues (GeomBranc *g, Brch *brch);
		float LngTrunk () {return lngTrunk;};
		float Dbh () {return dbh;};
		float FirstBranchHeight(){return firstBranchHeight;};

		vector <PA> Vpa () {return vpa;};

};

#if !defined STATIC_LIB
#ifdef WIN32
extern "C" __declspec(dllexport)  Output * StartPlugin ();
#else
extern "C" Output * StartPlugin ();
#endif
#endif

#endif

