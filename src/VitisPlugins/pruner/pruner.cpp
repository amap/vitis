/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "Plant.h"
#include "ObserverManager.h"
#include "PlotManager.h"

#ifdef WIN32
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "externPruner.h"
#include "pruner.h"



/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{

    paramPlant * d = (paramPlant*)data; //Retrieve msg params

	Prunerplugin_ptr->setPlant (d->p);

    Scheduler::instance()->create_process(Prunerplugin_ptr,NULL,Prunerplugin_ptr->PruningTime(),103);
}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{
	Prunerplugin_ptr->setPlant (NULL);
}

void PrunerPlugin::process_event(EventData *msg)
{
    if (plant == NULL)
        return;

    float nextTime;

	Scheduler *s = Scheduler::instance();

    prune(PruningHeight());

    pruningTimeIndex++;
    if (   pruningTimeIndex < pruningTimeNumber
        && pruningTime[pruningTimeIndex][0] <= s->getTopClock()+1 < s->getHotStop())
    {
    	s->signal_event(this->getPid(),NULL,PruningTime(),103);
	}
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
PrunerPlugin::PrunerPlugin(const std::string& f, const void *plant) {

    if (   !initPrunerParam (f)
        || pruningTimeNumber == 0)
    {
		cout << "Failed reading pruner parameter file " << f << endl;
		cout << "File sould look like this :" << endl<<endl;
		cout << "# WARNING this is a comment"<<endl;
		cout << "# pruning time 10, pruning height 5m"<<endl;
		cout << "10 5"<<endl;
		cout << "# pruning time 25, pruning height 8,5m"<<endl;
		cout << "25 8.5"<<endl;
		cout << "# removing at time 40"<<endl;
		cout << "40 -1"<<endl;

 		cout << endl<< "**** Failed reading light parameter file " << f << " ****" << endl;
		return;
	}
	this->plant = (Plant*)plant;

    //child -> parent binding PTR
	p = new PlantPlugin (this->plant);
    p->Prunerplugin_ptr = this;
    //child -> parent binding PTR
	pp = new PlantDeletePlugin (this->plant);
    pp->Prunerplugin_ptr = this;

}

bool PrunerPlugin::initPrunerParam (const std::string& f) {
    std::string s;
    char str[2056];
	fstream * fic= new fstream(f.c_str());
	float date, height;
	int pos;

	if (!fic->is_open())
		return false;

    pruningTimeIndex = 0;
    pruningTimeNumber = 0;

    while (!fic->eof())
    {
        while (!fic->eof())
        {
            pos = fic->tellg();
            *fic >> s;
            if (   s.empty()
                || s.at(0) == '#')
            {
                fic->getline(str, 2056);
                continue;
            }
            break;
        }
        if (fic->eof())
            break;

        fic->seekg(pos);
        *fic >> date;
        *fic >> height;
        fic->getline(str, 2056);

        pruningTime[pruningTimeNumber][0] = date;
        pruningTime[pruningTimeNumber][1] = height;
        pruningTimeNumber ++;
    }

	return true;
}

void PrunerPlugin::prune (float height)
{
    plant->computeGeometry();

    Branc *b;
    FloatData *L;
	bool trouve = true;
	Hierarc *h;
	vector<Hierarc *> listHierarc;

	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	for (vector<GeomBranc *>::iterator gb=brcs.begin(); gb!=brcs.end(); gb++)
	{
		GeomBranc *g = *gb;
		b = g->getBranc();
		if (b == NULL)
            continue;
        if (g->getAxe()->ordre == 1)
        {
            if (g->getElementsGeo ().at(0)->getMatrix().getTranslation().z() <= PruningHeight())
            {
                L = new FloatData;
                L->val = TOBEPRUNED;
                b->addAdditionalData("pruned", L, VITISGLDSPRINTABLE);
            }
        }
	}


	// create the list of hierarcs to be deleted
	while (trouve)
	{
		trouve = false;
        brcs = plant->getGeometry().getGeomBrancStack();
        for (vector<GeomBranc *>::iterator gb=brcs.begin(); gb!=brcs.end(); gb++)
		{
			GeomBranc *g = *gb;

			L=(FloatData*)g->getBranc()->getAdditionalData("pruned");
			if (L == 0)
				continue;

			if (L->val != TOBEPRUNED)
				continue;

			L->val = PRUNED;
			brcs.erase(gb);
			h = g->getAxe();
			listHierarc.push_back(h);
			trouve = true;
			break;
		}
	}

	// delete the hierarcs
	// in case of topological simplification, removing a part of a simplified branc makes it different from
	// the other previous siblings. So it has to be put out of the simplified hierarc list.
	//
	cout << "pruning " << listHierarc.size() << " branches at time "<< Scheduler::instance()->getTopClock() << endl;
	for (vector<Hierarc *>::iterator it=listHierarc.begin(); it!=listHierarc.end(); it++)
	{
		h = *it;
		b = h->getBranc();
        plant->getTopology().removeBranc(b, 0);
	}

    plant->getGeometry().clear();

}



