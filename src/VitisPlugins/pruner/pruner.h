/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "externPruner.h"


#include <vector>

class PrunerPlugin;

#define TOBEPRUNED 1
#define PRUNED 0

struct FloatData : public PrintableObject
{
public :
	float val;
	std::string to_string ()
	{
		std::ostringstream oss;
		// écrire la valeur dans le flux
		oss << val ;
		// renvoyer une string
		return oss.str();
	};
};


struct PRUNER_EXPORT PlantPlugin:public subscriber<messageVitisPlant> {
	PrunerPlugin * Prunerplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};
struct PRUNER_EXPORT PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	PrunerPlugin * Prunerplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};


class PRUNER_EXPORT PrunerPlugin : public VProcess
{
public :
	PrunerPlugin (const std::string& f, const void *p);
	virtual ~PrunerPlugin(){;};
	void setPlant (Plant  *p) {plant = p;};
	Plant *getPlant () {return plant;};
	void process_event(EventData *msg);

	float startTime;

	float PruningTime(){return pruningTime[pruningTimeIndex][0];};
	float PruningHeight(){return pruningTime[pruningTimeIndex][1];};
protected :
    bool initPrunerParam (const std::string& f);
	void prune (float height);

	Plant *plant;
	PlantPlugin *p;
	PlantDeletePlugin *pp;

	int pruningTimeIndex;
	int pruningTimeNumber;
	float pruningTime[20][2];
};

extern "C" PRUNER_EXPORT  PrunerPlugin * StartPlugin (const std::string& f, const void *p)
{
	std::cout << "loading PrunerPlugin module with parameter " << f << std::endl;

	return new PrunerPlugin (f, p) ;
}
