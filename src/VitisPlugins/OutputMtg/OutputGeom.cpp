/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include "OutputGeom.h"
#include "Plant.h"
#include "MtgFormat.h"
#include "DebugNew.h"



//Noter que DP = Z
//DS = X
//ECHEL [0] = echel pour dp -> Z


OutputGeom::OutputGeom():Output()
{
}

OutputGeom::~OutputGeom(void)
{
}



void OutputGeom::freeData ()
{
	v_plt->getGeometry().clear();
}

void OutputGeom::computeDataToOutput ()
{
	if (v_plt->getTopology().nbBranc == 0)
		return;

	if (v_plt->getGeometry().getGeomBrancNumber() == 0)
		v_plt->computeGeometry();
}

void OutputGeom::writeData (const std::string &finame)
{

	outFormat= new mtgFormat(finame+".mtg");

//	outFormat->initFormat(&(v_plt->getGeometry()));
	outFormat->initFormat(&(v_plt->getTopology()));

	std::cout<<"Output mtg"<<std::endl;


	outFormat->printData(v_plt->getGeometry().getGeomBrancStack().at(0));
//	outFormat->printData(v_plt->getTopology().getTrunc());


	delete outFormat;
}




#if !defined STATIC_LIB
extern "C" Output * StartPlugin ()
{
	return new OutputGeom();
}
#endif


