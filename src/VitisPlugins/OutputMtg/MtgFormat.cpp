/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <time.h>
#include "MtgFormat.h"
#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "GeomBrancCone.h"
#include "UtilMath.h"
#include "Plant.h"
#include "DebugNew.h"

using namespace std;

mtgFormat::mtgFormat(const std::string &finame):OutputFormat(finame),__mtgstream(finame.c_str())
{
	indexBranc = 0;
}

mtgFormat::~mtgFormat(void)
{

	__mtgstream.close();

}


int mtgFormat::initFormat(VitisObject * vobj)
{
	TopoManager * tpmgr = (TopoManager *) vobj;
	Branc *b = tpmgr->seekTree(TopoManager::RESET);

	if (b == 0)
		return 0;

	Plant * plt= b->getPlant();
	maxOrder = 0;
	maxShiftOrder = 0;

	decomp[0] = 2;
	for (int i=1; i<100; i++)
		decomp[i] = 0;
// scan for decomposition sketch of the plant
	while (b)
	{
		b->startPosit();

		scanDecomp (b, decomp+1);

		b->endPosit();

		adjustMaxOrder (b);

		b = tpmgr->seekTree(TopoManager::NEXT);
	}

	for (int i=0; i<10; i++)
		cout <<decomp[i]<<" ";
	cout << endl;

	printHeader (plt);

	return 1;
}

void mtgFormat::adjustMaxOrder (Branc *b)
{
	if (maxOrder <= b->getCurrentHierarc()->ordre)
		maxOrder = b->getCurrentHierarc()->ordre+1;
	if (maxShiftOrder <= b->getCurrentHierarc()->ordre - b->getCurrentHierarc()->getBearer()->ordre)
		maxShiftOrder = b->getCurrentHierarc()->ordre - b->getCurrentHierarc()->getBearer()->ordre;

}

int mtgFormat::scanDecomp (DecompAxeLevel *d, int *decomp)
{

	if (d->getElementNumberOfSubLevel() > *decomp)
		*decomp = d->getElementNumberOfSubLevel();

	if (d->empty())
		return 0;

	d->positOnFirstElementOfSubLevel();
	do
	{
		scanDecomp (d->getCurrentElementOfSubLevel(), decomp+1);
	}
	while (d->positOnNextElementOfSubLevel());

	return 1;
}

int mtgFormat::printData(VitisObject * vobj)
{

	GeomBranc *g = (GeomBranc*)vobj;
	Hierarc *h = g->getAxe();
	Plant * plt= g->getBranc()->getPlant();

	__mtgstream << "/P1"<<endl;

	h->sortTree();
    if(plt->getConfigData().cfg_supersimplif)
        printSubStructure (g, g->getBranc(),0,0);
	else
        printDataInOrder(g,g->getBranc(),0,0);

	return 1;

}

void mtgFormat::shift (int s)
{
	for (int i=0; i<s; i++)
		__mtgstream << "\t";
}

int mtgFormat::printDataInOrder(GeomBranc *g, DecompAxeLevel *d, int index, int ramifOrder)
{
	int i;
	Hierarc *h = g->getAxe();
	GeomBranc *borne;

	if (skipObject (g, d))
		return 1;

	if (   (   decomp[d->getLevel()] > 1
			  || d->getLevel() == nbDecomp - 1)
	    && !skipLevel (d->getLevel()))
	{
		shift (ramifOrder);

		if (d->getLevel() == 0)
		{
			if (indexBranc == 0)
				__mtgstream <<"^/"<<getDecompLetterAtLevel(d->getLevel())<<++indexBranc;
			else
				__mtgstream <<"+"<<getDecompLetterAtLevel(d->getLevel())<<++indexBranc;
		}
		else if (index == 0)
			__mtgstream <<"^/"<<getDecompLetterAtLevel(d->getLevel())<<index+1;
		else
			__mtgstream <<"^<"<<getDecompLetterAtLevel(d->getLevel())<<index+1;

		shift (maxOrder+maxShiftOrder-ramifOrder);

		printFeatureData (g, d);

		__mtgstream << endl;
	}

	if (d->getLevel() == 0)
		d->startPosit();

	// at lowest level seek for borne branch
	if (d->getLevel() == nbDecomp-1)
	{
		int pos =h->getBranc()->getCurrentElementIndexOfSubLevel(d->getLevel());

		for(unsigned int i=0; i<g->getElementsGeo().size(); i++)
		{
			for(unsigned int j=0; j<g->getElementsGeo().at(i)->getGeomChildren().size(); j++)
			{
				borne = g->getElementsGeo().at(i)->getGeomChildren().at(j);
				if (   borne->getAxe()->indexOnBearerAtLowestLevel == pos
					 && borne->getElementsGeo().size() > 0)
					printDataInOrder(g->getElementsGeo().at(i)->getGeomChildren().at(j), borne->getBranc(), 0, ramifOrder+1);
			}
		}
	}

	if (d->positOnFirstElementOfSubLevel())
	{
		do
		{
			// find the relevant upper decomposition level
			i = d->getLevel();
			while (   decomp[i] == 1
			       || skipLevel(i))
				i--;

			if (i > 0)
				printDataInOrder (g, d->getCurrentElementOfSubLevel(), h->getBranc()->getCurrentElementOfSubLevel(i)->getCurrentElementIndexOfSubLevel(d->getLevel()+1), ramifOrder);
			else
				printDataInOrder (g, d->getCurrentElementOfSubLevel(), h->getBranc()->getCurrentElementIndexOfSubLevel(d->getLevel()+1), ramifOrder);
		} while ( d->positOnNextElementOfSubLevel());
	}

	if (d->getLevel() == 0)
		d->endPosit();


	return 1;
}

int mtgFormat::printSubStructure(GeomBranc *g, DecompAxeLevel *d, int index, int ramifOrder)
{
	int i;
	Hierarc *h = g->getAxe();
	GeomBrancCone *borne, *clone;

	if (skipObject (g, d))
		return 1;

	if (   (   decomp[d->getLevel()] > 1
			  || d->getLevel() == nbDecomp - 1)
	    && !skipLevel (d->getLevel()))
	{
		shift (ramifOrder);

		if (d->getLevel() == 0)
		{
			if (indexBranc == 0)
				__mtgstream <<"^/"<<getDecompLetterAtLevel(d->getLevel())<<++indexBranc;
			else
				__mtgstream <<"+"<<getDecompLetterAtLevel(d->getLevel())<<++indexBranc;
		}
		else if (index == 0)
			__mtgstream <<"^/"<<getDecompLetterAtLevel(d->getLevel())<<index+1;
		else
			__mtgstream <<"^<"<<getDecompLetterAtLevel(d->getLevel())<<index+1;

		shift (maxOrder+maxShiftOrder-ramifOrder);

		printFeatureData (g, d);

		__mtgstream << endl;
	}

	if (d->getLevel() == 0)
		d->startPosit();

	// at lowest level seek for borne branch
	if (d->getLevel() == nbDecomp-1)
	{
        Matrix4 bearTransla;
		int pos =h->getBranc()->getCurrentElementIndexOfSubLevel(d->getLevel());

		for(unsigned int i=0; i<g->getElementsGeo().size(); i++)
		{
            GeomElemCone *cone = (GeomElemCone *)g->getElementsGeo().at(i);
            if (i == 0)
                bearTransla = Matrix4::translation(cone->getMatrix().getTranslation());

			for(unsigned int j=0; j<cone->getGeomChildren().size(); j++)
			{
				borne = (GeomBrancCone*)cone->getGeomChildren().at(j);
				if (   borne->getAxe()->indexOnBearerAtLowestLevel == pos
                    && borne->getElementsGeo().size() > 0)
                {
                    if((clone = (GeomBrancCone*)borne->getGeomBrancClone()) != 0)
                    {
                        Matrix4 & matTrans=borne->getCurrentTransform();
                        Vector3 transla = clone->getElementsGeo().at(0)->getMatrix().getTranslation();

                        borne->copyFrom(clone, bearTransla*matTrans*Matrix4::translation(-transla));
                    }

					printSubStructure(borne, borne->getBranc(), 0, ramifOrder+1);
                }
			}
		}
	}

	if (d->positOnFirstElementOfSubLevel())
	{
		do
		{
			// find the relevant upper decomposition level
			i = d->getLevel();
			while (   decomp[i] == 1
			       || skipLevel(i))
				i--;

			if (i > 0)
				printDataInOrder (g, d->getCurrentElementOfSubLevel(), h->getBranc()->getCurrentElementOfSubLevel(i)->getCurrentElementIndexOfSubLevel(d->getLevel()+1), ramifOrder);
			else
				printDataInOrder (g, d->getCurrentElementOfSubLevel(), h->getBranc()->getCurrentElementIndexOfSubLevel(d->getLevel()+1), ramifOrder);
		} while ( d->positOnNextElementOfSubLevel());
	}

	if (d->getLevel() == 0)
		d->endPosit();


	return 1;
}


bool mtgFormat::printHeader(const std::string& )
{
	return true;
}

std::string mtgFormat::formatHeader (Plant *)
{
	return NULL;
}

std::string mtgFormat::formatHeaderFeature (Plant *)
{
	return NULL;
}

bool mtgFormat::printHeaderFeature(Plant *)
{
	return true;
}

void mtgFormat::printFeatureDescription (Plant *)
{
}

void mtgFormat::printFeatureData (GeomBranc *g, VitisObject *o)
{
	unsigned int i;
	DecompAxeLevel *d = (DecompAxeLevel *)o;

	if (d->getLevel() == nbDecomp-1)
	{
		int pos = g->getBranc()->getCurrentElementIndexOfSubLevel(d->getLevel());
		for(i=0; i<g->getElementsGeo().size(); i++)
		{
			GeomElemCone *e = (GeomElemCone*)g->getElementsGeo().at(i);
			if (e->getPosInBranc() == pos)
			{
				Vector3 v = e->getMatrix().getMainVector();
				v *= (float)e->getLength();
				v += e->getMatrix().getTranslation();
				__mtgstream <<v.x()<<"\t"<<v.y()<<"\t"<<v.z()<<"\t"<<e->getTopDiam()<<"\t";
				break;
			}
		}
		if (i == g->getElementsGeo().size())
			__mtgstream <<"\t\t\t\t";
	}
	else if (d->getLevel() == 0)
	{
		GeomElemCone *e = (GeomElemCone*)g->getElementsGeo().at(0);
		Vector3 v(e->getMatrix().getTranslation());
		__mtgstream <<v.x()<<"\t"<<v.y()<<"\t"<<v.z()<<"\t"<<e->getBottomDiam()<<"\t";
	}
	else
		__mtgstream <<"\t\t\t\t";

	printFeatureData (o);
}

void mtgFormat::printFeatureData (VitisObject *)
{
//	__mtgstream << "#no feature data";
}

char mtgFormat::getDecompLetterAtLevel (int level)
{
	char c='B';
	return c+(char)level;
}

bool mtgFormat::skipLevel (int )
{
	return false;
}

bool mtgFormat::skipObject (GeomBranc *, DecompAxeLevel *d)
{
    Branc *b = (Branc*)d->getParentOfLevel(0);
    int level = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	if (   d->getLevel() != level
		&& d->getElementNumberOfSubLevel(level) == 0)
		return true;
	else
		return false;
}

bool mtgFormat::printHeader(Plant *plt)
{
	char str[256];
   time_t t = time (NULL);
	MapParameterFileName lmap = plt->getParamName();
#ifdef WIN32
   struct tm _tm;
   localtime_s (&_tm, &t);
   asctime_s (str, 256, &_tm);
   __mtgstream <<"#"<< str <<endl;
#else
   __mtgstream <<"#"<< asctime(localtime(&t)) <<endl;
#endif
   __mtgstream <<"#file "<< plt->getParamName(string("AMAPSimMod")) <<endl;
	if (lmap.find(string("AMAPSimMod")) != lmap.end())
        __mtgstream <<"#file "<< plt->getParamName(string("AMAPSimMod")) <<endl;
    else if (lmap.find(string("DigRMod")) != lmap.end())
        __mtgstream <<"#file "<< plt->getParamName(string("DigRMod")) <<endl;
    else if (!lmap.empty())
    {
        MapParameterFileName::iterator p = lmap.begin();
        __mtgstream <<"#file "<< p->second <<endl;
    }
	__mtgstream <<"#age "<< Scheduler::instance()->getTopClock() <<endl;
	__mtgstream << "#seed "<< plt->getConfigData().randSeed <<endl<<endl<<endl;

   __mtgstream <<endl<<endl<<"CODE:	FORM-A	"<<endl<<endl<<endl;


   __mtgstream <<"CLASSES:	"<<endl;
   __mtgstream <<"SYMBOL	SCALE	DECOMPOSITION	INDEXATION	DEFINITION"<<endl;
   __mtgstream <<"$	0	FREE	FREE	IMPLICIT"<<endl;
   __mtgstream <<"P 	1 	CONNECTED	FREE	EXPLICIT"<<endl;
   __mtgstream <<"B 	2 	LINEAR	FREE	EXPLICIT"<<endl;

	nbDecomp=0;
	while (decomp[++nbDecomp] > 0);

	cout << nbDecomp << endl;

   int i=0, niveau = 3;
	char c;
	for (i=1; i<nbDecomp-1; i++)
	{
		if (	 decomp[i] > 1
			 && !skipLevel(i)	)
		{
   		__mtgstream <<getDecompLetterAtLevel(i)<<" 	"<<niveau++<<" 	LINEAR	FREE	EXPLICIT"<<endl;
		}
	}
  	__mtgstream <<getDecompLetterAtLevel(i)<<" 	"<<niveau++<<" 	LINEAR	FREE	EXPLICIT"<<endl;

   __mtgstream << endl << "DESCRIPTION :		"<<endl;
   __mtgstream << "LEFT	RIGHT	RELTYPE	MAX	"<<endl;
   __mtgstream << "B	B	+	? 	"<<endl;
	i=0;
	for (i=1; i<nbDecomp-1; i++)
	{
		c=getDecompLetterAtLevel (i);
		if (	 decomp[i] > 1
			 && !skipLevel(i)	)
		{
   		__mtgstream << c << "	" << c <<"	+	? 	"<<endl;
   		__mtgstream << c << "	" << c <<"	<	1	"<<endl;
		}
	}
	c=getDecompLetterAtLevel (i);
  	__mtgstream << c << "	" << c <<"	+	? 	"<<endl;
  	__mtgstream << c << "	" << c <<"	<	1	"<<endl;

   __mtgstream << endl;

	__mtgstream << "FEATURES:			"<<endl;
	__mtgstream << "NAME	TYPE		"<<endl<<endl;
	__mtgstream << "XX	REAL		"<<endl;
	__mtgstream << "YY	REAL		"<<endl;
	__mtgstream << "ZZ	REAL		"<<endl;
	__mtgstream << "Dia	REAL		"<<endl;

	printHeaderFeature (plt);

   __mtgstream << endl << endl << "MTG:			"<<endl<<endl;

   __mtgstream << "ENTITY-CODE";
   shift (maxOrder+maxShiftOrder);

	__mtgstream << "XX	YY	ZZ	Dia	";

	printFeatureDescription (plt);

   __mtgstream << endl;

	return true;

}


