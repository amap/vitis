/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _MTGFORMAT_H
#define _MTGFORMAT_H
#include "externOutputMtg.h"
#include "Plant.h"
#include "OutputFormat.h"
#include "bfstream.h"
using namespace Vitis;

class OUTPUTMTG_EXPORT mtgFormat : public OutputFormat
{

	protected:
	std::ofstream   __mtgstream;

	int maxOrder, maxShiftOrder;
	int nbDecomp;
	int indexBranc;
	int decomp[100];

	public:

	mtgFormat(const std::string &finame);

	virtual ~mtgFormat(void);

	virtual int initFormat(VitisObject * vobj);
	virtual void adjustMaxOrder (Branc *b);

	virtual int printData(VitisObject *);

	int printDataInOrder(GeomBranc *g, DecompAxeLevel *d, int index, int ramifOrder);
	int printSubStructure(GeomBranc *g, DecompAxeLevel *d, int index, int ramifOrder);

	std::string formatHeader (Plant *plt);
	virtual bool printHeader (const std::string &headerf);
	virtual bool printHeader (Plant *plt);

	virtual bool printHeaderFeature (Plant *plt);
	virtual std::string formatHeaderFeature (Plant *plt);
	virtual void printFeatureDescription (Plant *plt);
	virtual void printFeatureData (GeomBranc *g, VitisObject *);
	virtual void printFeatureData (VitisObject *);

	virtual char getDecompLetterAtLevel (int level);
	virtual bool skipLevel (int level);
	virtual bool skipObject (GeomBranc *g, DecompAxeLevel *d);
	void shift (int s);

	int scanDecomp (class DecompAxeLevel *d, int *decomp);

};

#endif

