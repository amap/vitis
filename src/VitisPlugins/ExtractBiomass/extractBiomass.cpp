/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WINDOWS
#include <conio.h>
#endif
#include <iostream>
#include <iomanip>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "VitisCoreSignalInterface.h"
#include "GeomBrancAMAP.h"
#include "externextractBiomass.h"
#include "extractBiomass.h"

#include "Root.h"


void PlantPlugin::on_notify (const state& , paramMsg * data)
{
	paramPlant * d = (paramPlant*)data; //Retrieve msg params

	extractBiomassplugin_ptr->setPlant (d->p);

	// to avoid complexity due to simplication
	d->p->getConfigData().cfg_topoSimp = 0;
	d->p->getConfigData().cfg_geoSimp = 0;

	BiomassExtractEventData *e = new BiomassExtractEventData(2);
	Scheduler::instance()->create_process(extractBiomassplugin_ptr,e,NULL,103);
}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{
	paramPlant * d = (paramPlant*)data; //Retrieve msg params

	extractBiomassplugin_ptr->deletePlant (d->p);
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
extractBiomassPlugin::extractBiomassPlugin(const std::string& f, const void *plant)
{
    if (initData(f))
    {
        p = new PlantPlugin ((Plant*)plant);
        p->extractBiomassplugin_ptr = this;
        pp = new PlantDeletePlugin ((Plant*)plant);
        pp->extractBiomassplugin_ptr = this;
 		// Scheduler::instance()->create_process(this,NULL,1,102);
    }

}

extractBiomassPlugin::extractBiomassPlugin(void *pp)
{
    cout << "no parameter file provided" <<endl;
    cout << "aborting biomass extraction"<<endl;
    cout << "parameter should look like :"<<endl<<endl;

    cout << "voxelSize SingleValueParameter 20"<<endl;
    cout << "leafPhyAgeMin SingleValueParameter 601"<<endl;
    cout << "leafPhyAgeMax SingleValueParameter 601"<<endl;
    cout << "SLD SingleValueParameter 1"<<endl;
    cout << "woodDensity SingleValueParameter 1"<<endl<<endl;
    cout << "absorbingRootType SingleValueParameter 2"<<endl<<endl;
    cout << "ExtractStep SingleValueParameter 1"<<endl<<endl;
}

extractBiomassPlugin::~extractBiomassPlugin()
{

}

bool extractBiomassPlugin::initData(const std::string& f)
{

	ParamSet * param_H = new ParamSet(f);

	if (!param_H->Exists())
	{
        cout << "unable to open "<<f<<" parameter file"<<endl;
		return false;
	}

    cout << "opening "<<f<<" parameter file"<<endl;
	if (param_H->getParameter("voxelSize"))
	{
        if (dynamic_cast<ParametricParameter *>(param_H->getParameter("voxelSize")) == NULL)
        {
            voxelSize = param_H->getParameter("voxelSize")->value();
            voxelSizeList.set(1, voxelSize);
            cout << "voxelSize : "<< voxelSize<<endl;
        }
        else
        {
            voxelSizeList.copy (param_H->getParameter("voxelSize"));
            cout << "voxelSize : "<< voxelSizeList.toString() <<endl;
        }
	}
	else
	{
        cout << "unable to find parameter voxelSize in "<<f<<" parameter file. Setting default value to 20"<<endl;
        voxelSize = 20;
        voxelSizeList.set(1, voxelSize);
	}

	if (param_H->getParameter("woodDensity"))
	{
		woodDensity = param_H->getParameter("woodDensity")->value();
		cout << "woodDensity : "<< woodDensity<<endl;
	}
	else
	{
        cout << "unable to find parameter woodDensity in "<<f<<" parameter file. Setting default value to 1"<<endl;
        woodDensity = 1;
	}

	if (param_H->getParameter("SLD"))
	{
		SLD = param_H->getParameter("SLD")->value();
		cout << "SLD : "<< SLD<<endl;
	}
	else
	{
        cout << "unable to find parameter SLD in "<<f<<" parameter file. Setting default value to 1"<<endl;
        SLD = 1;
	}

	if (param_H->getParameter("ExtractStep"))
	{
		extractStep = param_H->getParameter("ExtractStep")->value();
		cout << "ExtractStep : "<< extractStep<<endl;
	}
	else
	{
        cout << "unable to find parameter ExtractStep in "<<f<<" parameter file. Setting default value to 1"<<endl;
        extractStep = 1;
	}

	if (param_H->getParameter("leafPhyAgeMin"))
	{
		leafPhyAgeMin = param_H->getParameter("leafPhyAgeMin")->value();
		cout << "leafPhyAgeMin : "<< leafPhyAgeMin<<endl;
	}
	else
	{
        cout << "unable to find parameter leafPhyAgeMin in "<<f<<" parameter file."<<endl;
        return false;
	}
	if (param_H->getParameter("leafPhyAgeMax"))
	{
		leafPhyAgeMax = param_H->getParameter("leafPhyAgeMax")->value();
		cout << "leafPhyAgeMax : "<< leafPhyAgeMax<<endl;
	}
	else
	{
        cout << "unable to find parameter leafPhyAgeMax in "<<f<<" parameter file."<<endl;
        return false;
	}
	if (param_H->getParameter("absorbingRootType"))
	{
		absorbingRootType = param_H->getParameter("absorbingRootType")->value();
		cout << "absorbingRootType : "<< absorbingRootType<<endl;
	}
	else
	{
        cout << "unable to find parameter absorbingRootType in "<<f<<" parameter file. Stting default value to 2"<<endl;
        absorbingRootType = 2;
	}



	return true;
}


void extractBiomassPlugin::process_event(EventData *)
{

	Scheduler *s = Scheduler::instance();
	float top_extern = s->getTopClock();

    voxelSpace = VoxelSpace::instance();

    voxelSize = voxelSizeList.value((int)top_extern, 0);
    voxelSpace->reset();
cout << "top "<< (int)top_extern <<" voxelsize "<< voxelSize << endl;
    voxelSpace->VoxelSize(voxelSize);
    voxelSpace->reset();

cout << endl << "computing biomass through voxels at time " << top_extern;

	maxBiomassWoodBranch = 0;
	maxBiomassWoodRoot = 0;
	maxBiomassLeaf = 0;
	maxBiomassAbsRoot = 0;

    updateBiomassThroughVoxels();

cout << "       maxbranch " << maxBiomassWoodBranch << " maxroot " << maxBiomassWoodRoot << " maxleaf " << maxBiomassLeaf << " maxfine " << maxBiomassAbsRoot << endl;

	if (s->getTopClock()+extractStep <= s->getHotStop())
	{
		s->signal_event(this->getPid(),NULL,s->getTopClock()+extractStep,102);
	}

    // store max values for normalisation purpose
    float maxabs, maxwood;
    fstream ific ("maxvalues.csv", std::fstream::in);

    if (!ific.good())
    {
        maxabs = maxwood = 0;
    }
    else
    {
        ific >> maxabs >> maxwood;
        ific.close();
    }

    if (maxwood < maxBiomassWoodBranch)
        maxwood = maxBiomassWoodBranch;
    if (maxwood < maxBiomassWoodRoot)
        maxwood = maxBiomassWoodRoot;
    if (maxabs < maxBiomassLeaf)
        maxabs = maxBiomassLeaf;
    if (maxabs < maxBiomassAbsRoot)
        maxabs = maxBiomassAbsRoot;

    ofstream ofic ("maxvalues.csv", std::fstream::out | std::fstream::trunc);
    if (!ofic.good())
    {
        perror ("updating maxvalues.csv");
        return;
    }

    ofic << maxabs << " " << maxwood;
    ofic.close();
}

void extractBiomassPlugin::updateBiomassThroughVoxels()
{
    Plant *plant;
//    float voxelSize = voxelSpace->VoxelSize();
cout << endl << "updating with size " << voxelSpace->VoxelSize() << endl;

    for (vector<Plant*>::iterator it=plants.begin(); it!=plants.end(); it++)
    {
        plant = *it;
        plant->computeGeometry();
//cout << "posx "<<pos[0]<<" posy "<<pos[1]<<endl;
//cout << "minx "<<min[0]<<" miny "<<min[1]<<endl;
//cout << "xOrig "<<xOrig<<" yOrig "<<yOrig<<endl;
//cout << "sizex "<<x<<" sizey "<<y<<endl;
        // compute bounding box dimensions
        Vector3 min = plant->getGeometry().getMin();
        Vector3 max = plant->getGeometry().getMax();
        Vector3 pos = plant->getPosition();

        int x = ceil (max[0] / voxelSize + 0.5) - floor (min[0] / voxelSize - 0.5);
        int y = ceil (max[1] / voxelSize + 0.5) - floor (min[1] / voxelSize - 0.5);
        int z = ceil (max[2] / voxelSize + 0.5) - floor (min[2] / voxelSize - 0.5);
        int indXOrigin = floor ((pos[0]+min[0]) / voxelSize - 0.5);
        int indYOrigin = floor ((pos[1]+min[1]) / voxelSize - 0.5);
        int indZOrigin = floor ((pos[2]+min[2]) / voxelSize - 0.5);
        float xOrig = indXOrigin * voxelSize;
        float yOrig = indYOrigin * voxelSize;
        float zOrig = indZOrigin * voxelSize;

    //cout << "posx "<<pos[0]<<" posy "<<pos[1]<<endl;
//        float h = max[2]-min[2];
//        int z = ceil (h / voxelSize);

        voxelSpace->adjust (xOrig, yOrig, zOrig, x, y, z);

        int ib, jb, kb;
        int ie, je, ke;
        GeomElemCone *e;
        vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
        vector<Voxel*> vv;

        for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
        {
            GeomBrancAMAP *g = (GeomBrancAMAP*)*b;

            vector<GeomElem *> ges = g->getElementsGeo ();
            for (vector<GeomElem *>::iterator gj=ges.begin(); gj!=ges.end(); gj++)
            {
                vv.clear();
                e = (GeomElemCone*)*gj;
                if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+pos[0], e->getMatrix().getTranslation().y()+pos[1], e->getMatrix().getTranslation().z()+pos[2], &ib, &jb, &kb))
                {
                    if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()+pos[0],
                                               e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()+pos[1],
                                               e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength()+pos[2],
                                               &ie, &je, &ke))
                    {
                        // if it is caulinear
                        if (ib!=ie || jb!=je || kb!=ke)
                        {
                            voxelSpace->bresenham3D (ib, jb, kb, ie, je, ke, &vv);
                        }
                        else
                            vv.push_back(voxelSpace->addrAt(ib, jb, kb));

                        updateBiomass (e, vv);
                   }
                }
            }
        }
	}
}

void extractBiomassPlugin::updateBiomass (GeomElemCone *e, vector<Voxel*> vv)
{
    int phyAge;
    if (e->getPtrBrc()->getBranc()->getClassType() == "AMAPSimMod")
    {
        BrancAMAP *b = (BrancAMAP*)e->getPtrBrc()->getBranc();
        phyAge = b->phyAgeInit;
        for (vector<Voxel*>::iterator it=vv.begin(); it!=vv.end(); it++)
        {
            Voxel *v = *it;
            v->content.push_back(e);
            if (v->getVoxelData("biomass") == 0)
            {
                v->addVoxelData("biomass", new BiomassData());
            }
            BiomassData *br = (BiomassData*)v->getVoxelData("biomass");
            if (   phyAge >= leafPhyAgeMin
                && phyAge <= leafPhyAgeMax)
            {
                br->biomassAbs += SLD * e->getLength() * e->getBottomDiam() / vv.size();
                if (maxBiomassLeaf < br->biomassAbs)
                    maxBiomassLeaf = br->biomassAbs;
            }
            else
            {
                br->biomassWood += woodDensity * e->getLength() * M_PI*((e->getBottomDiam()+e->getTopDiam()) / 2)*((e->getBottomDiam()+e->getTopDiam()) / 2) / vv.size();
                if (maxBiomassWoodBranch < br->biomassAbs)
                    maxBiomassWoodBranch = br->biomassAbs;
            }
        }
    }
    else
    {
        Root *r = (Root*)e->getPtrBrc()->getBranc();
        for (vector<Voxel*>::iterator it=vv.begin(); it!=vv.end(); it++)
        {
            Voxel *v = *it;
            v->content.push_back(e);
            if (v->getVoxelData("biomass") == 0)
            {
                v->addVoxelData("biomass", new BiomassData());
            }
            BiomassData *br = (BiomassData*)v->getVoxelData("biomass");
            if (r->Type() == absorbingRootType)
            {
                br->biomassAbs += 100 * woodDensity * e->getLength() * M_PI*((e->getBottomDiam()+e->getTopDiam()) / 2)*((e->getBottomDiam()+e->getTopDiam()) / 2) / vv.size();
                if (maxBiomassAbsRoot < br->biomassAbs)
                    maxBiomassAbsRoot = br->biomassAbs;
            }
            else
            {
                br->biomassWood += 100 * woodDensity * e->getLength() * M_PI*((e->getBottomDiam()+e->getTopDiam()) / 2)*((e->getBottomDiam()+e->getTopDiam()) / 2) / vv.size();
                if (maxBiomassWoodRoot < br->biomassAbs)
                    maxBiomassWoodRoot = br->biomassAbs;
            }
        }
    }
}

void extractBiomassPlugin::setPlant (Plant *p)
{
    plants.push_back(p);
}

void extractBiomassPlugin::deletePlant (Plant *p)
{
    for (vector<Plant*>::iterator it=plants.begin(); it!=plants.end(); it++)
    {
        if (*it == p)
        {
            plants.erase(it);
            break;
        }
    }
}



