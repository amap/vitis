/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef EXTRACTBIOMASS
#define EXTRACTBIOMASS

using namespace std;

#include "Plant.h"
#include "scheduler.h"
#include "Voxel.h"
#include "Notifier.h"
#include "ParamFile.h"
#include "VitisCoreSignalInterface.h"
//#include "AmapSimModSignalInterface.h"
#include "externextractBiomass.h"
#include "ParamFile.h"

#include <vector>

class extractBiomassPlugin;
class BiomassData;

struct BiomassExtractEventData: public EventData
{
public:
	BiomassExtractEventData (int action){this->action = action;};
	int Action (){return action;};
	void setAction (int a){action = a;};
private:
	int action;
};

class BiomassData : public VoxelData
{
    public :
    float biomassAbs;
    float biomassWood;
    BiomassData () {biomassAbs=0;biomassWood=0;};
    virtual std::string to_string(){
        std::ostringstream oss;
		// écrire la valeur dans le flux
		oss << biomassAbs << " " << biomassWood;
		// renvoyer une string
		return oss.str();
    };
    virtual VoxelData *duplicate (){
        BiomassData *v = new BiomassData;
        v->biomassAbs = biomassAbs;
        v->biomassWood = biomassWood;
        return v;
    };
};

struct PlantPlugin:public subscriber<messageVitisPlant> {
	extractBiomassPlugin * extractBiomassplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	extractBiomassPlugin * extractBiomassplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

class EXTRACTBIOMASS_EXPORT extractBiomassPlugin : public VProcess
{
public:
	extractBiomassPlugin(const std::string& f, const void *p);
	extractBiomassPlugin(void *p);
	extractBiomassPlugin() {voxelSpace = VoxelSpace::instance();};
	virtual ~extractBiomassPlugin();
	void reset();
	void process_event(EventData *);
	void setPlant (Plant *p);
	void deletePlant (Plant *p);

	bool initData(const std::string& f);
	void updateBiomassThroughVoxels();
	void updateBiomass (GeomElemCone *e, vector<Voxel*> vv);

protected:

	VoxelSpace *voxelSpace;
    float voxelVolume;
    float voxelSize;
    ParametricParameter voxelSizeList;
    float woodDensity, SLD;
    float extractStep;
    int leafPhyAgeMin, leafPhyAgeMax;
    int absorbingRootType;
    vector<Plant*> plants;

    PlantPlugin *p;
	PlantDeletePlugin *pp;

	float maxBiomassWoodBranch;
	float maxBiomassWoodRoot;
	float maxBiomassLeaf;
	float maxBiomassAbsRoot;
};

#if !defined STATIC_LIB
extern "C" EXTRACTBIOMASS_EXPORT  extractBiomassPlugin * StartPlugin (const std::string& f, const void *p)
{
	std::cout << "loading AmapSim extractBiomassPlugin module with parameter " << f << std::endl;

	return new extractBiomassPlugin (f, p) ;
};
#endif



#endif

