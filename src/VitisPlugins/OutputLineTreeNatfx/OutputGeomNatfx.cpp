/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "OutputGeomNatfx.h"
#include "Plant.h"
#include "LineTreeFormatNatfx.h"
#include "DebugNew.h"



//Noter que DP = Z
//DS = X
//ECHEL [0] = echel pour dp -> Z


OutputGeomNatfx::OutputGeomNatfx():OutputGeom()
{

}

void OutputGeomNatfx::writeData (const std::string &finame)
{

	int i;

	std::stringstream sout;
	sout << finame;
	sout<< "_"<<v_plt->getConfigData().randSeed;
	std::string fname (sout.str());

	std::cout<<"Output Geometry linetree natfx to : "<<fname<<std::endl;

	outFormat= new LineTreeFormatNatfx(fname);
	outFormat->initFormat(&(v_plt->getGeometry()));

	outFormat->printData(v_plt->getGeometry().getGeomBrancStack().at(0));

	delete outFormat;

}




#if !defined STATIC_LIB
extern "C" Output * StartPlugin ()
{
	return new OutputGeomNatfx();
}
#endif


