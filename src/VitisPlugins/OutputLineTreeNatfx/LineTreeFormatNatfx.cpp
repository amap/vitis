/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#include <windows.h>
#else
#include <string.h>
#endif
#include "LineTreeFormatNatfx.h"
#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "GeomBrancCone.h"
#include "UtilMath.h"
#include "Bud.h"
#include "PlantAMAP.h"
#include "DebugNew.h"
#include <assert.h>

extern "C"
{
#include "fcrypt0.c"
}





LineTreeFormatNatfx::LineTreeFormatNatfx(const std::string &finame):LineTreeFormat(finame),__arcstream(finame+".arc")
{
}

LineTreeFormatNatfx::~LineTreeFormatNatfx(void)
{
	__ligstream.getStream().close();
	__arcstream.getStream().close();

}

std::string LineTreeFormatNatfx::computeKey(const std::string & filename)
{
	char bufg[128];
	char   client[16], scode[16], cle[32];
	unsigned char sel[16];
	int    clg, poscle;
	float  maxx;
	char parametre[256];
	unsigned int i;
	long pos[20]={12,56,46,1,65,75,4,19,51,54,31,26,30,25,49,67,52,15,17,71};

	strncpy(parametre, filename.c_str(),256);

	sel[0] = 'J';
	sprintf (client, "%hd", 5467);
	clg = 5467 + 83837;
	for (i=0; i<strlen(parametre); i++)
		clg += *(parametre+i) - 49;
	maxx = 1.0F / clg;
	sel[1] = 'F';
	for (i=0; i<80; i++)
	{
		maxx = (float)(maxx * 73.0 - (int)(maxx * 73.0));
		bufg[i] = (char)((int)(maxx * 92.0) + 33);
	}
	bufg[80] = 0;
	clg = (int)strlen(parametre) - 4;
	sprintf (scode, "%s%s",client,&parametre[clg]);
	sel[2] = 0;
	strncpy (cle, (char *)mycrypt (scode,(unsigned char*)sel), 32);  // sel = JF

	poscle = 0;
	for (i=2; i<strlen(cle); i++)
		poscle += cle[i];

	for (i=0; i<strlen(cle)-2; i++)
		bufg[(poscle+pos[i])%80] = cle[i+2];

	return std::string(bufg);
}

int LineTreeFormatNatfx::initFormat(VitisObject * vobj)
{
	GeomManager * gb = (GeomManager *) vobj;

	vectId.resize(gb->getElemeGeoMaxNumber());

	PlantAMAP *p = (PlantAMAP*)gb->getPlant();

	printHeader(computeKey(p->getParamName(string("AMAPSimMod"))));

	headerArc((GeomManager *) vobj);
	return 1;

}




//Order writing
int LineTreeFormatNatfx::printDataInOrder(VitisObject * vobj, int idParent)
{

	int i,j,nbChild;
	GeomBranc * gb = (GeomBranc *) vobj;
	int curId;

	curId = idParent;

	//position dans la branche
	currentPortee=0;



	//Pour chaque �l�ment g�om�trique
	for (i=0; i<(int)gb->getElementsGeo().size(); i++)
	{
		//Si on est pas sur le premier neoud de l'axe, on indique l'id du noeud porteur sur l'axe courant
		vectId[cptID].idParent=curId;


		curId=cptID;

		bool bIsWood = true;
		GeomElemCone *ge = (GeomElemCone *)gb->getElementsGeo().at(i);
		if (!ge)
		{
			assert(0 && "Could not retrieve GeomElemCone from current branch");
			continue;
		}
		if (((BrancAMAP *)ge->getPtrBrc()->getAxe()->getBranc())->nature == SimpleImmediateOrgan)
			bIsWood = false;


		//Si on est pas sur le premier noeud de la plante, on maj les relations p�res-fils
		nbChild = 0;
		if (curId != 0 && i == 0)
		{
			int idBearer=vectId[curId].idParent;

			vectId[idBearer].idChild[vectId[idBearer].nbChild]=curId;

			vectId[idBearer].nbChild++;

		}
		nbChild = (int)gb->getElementsGeo().at(i)->getGeomChildren().size();
		if (i < (int)gb->getElementsGeo().size()-1)
			nbChild ++;

		if (nbChild > 0)
			vectId[curId].idChild = new int[nbChild];

		// compute portee differently with wood or organs
		if (bIsWood == true)
		{
			currentPortee = ge->getPosInGeomBranc();
		}
		else
		{
			// for an organ we set portee to the value of the bearer
			currentPortee = ge->getPtrBrc()->getPosOnBearer();
		}

		//if (   ge->getLength() > 0
		//	&& ge->getBottomDiam() > 0
		//	&& ge->getTopDiam() > 0)
		if (ge->getLength() >= 0.0001)
		{
			printRecord(ge,curId);
			cptID++;
		}

		//Pour chaque �l�ment port� par celui courant
		for (j=0; j<(int)gb->getElementsGeo().at(i)->getGeomChildren().size(); j++)
		{

			//On lance la r�cursivit� sur un axe port�

			//Ici on indique l'id du parent ( ici l'id courant) � l'�l�ment qui vient d'�tre cr��
			if (gb->getElementsGeo().at(i)->getGeomChildren().at(j)->getElementsGeo().size() > 0)
				printDataInOrder(gb->getElementsGeo().at(i)->getGeomChildren().at(j), curId);
		}

		if (ge->getLength() >= 0.0001)
		{
			// now that children have been processed, add the next element as a child of the current one
			if (i < (int)gb->getElementsGeo().size()-1)
			{
				vectId[curId].nbChild++;
				vectId[curId].idChild[nbChild-1] = cptID;
			}

			// output hierarchy only when all son branches have been processed
			printElemArc(ge,curId);
		}
	}

	return 1;

}



void LineTreeFormatNatfx::printRecord(GeomElemCone * ge, int elemId)
{
    int numBr;

	if(((BrancAMAP *)ge->getPtrBrc()->getAxe()->getBranc())->nature==SimpleImmediateOrgan)
		numBr=-1;
	else
		numBr=ge->getPtrBrc()->getNum();

	int i;
	for (i=0; i<(int)tabSymb.size(); i++)
		if (tabSymb[i] == ge->getSymbole())
			break;
	if (i == (int)tabSymb.size())
		tabSymb.push_back(ge->getSymbole());

	i = ge->getSymbole();
	__ligstream << ge->getSymbole() << 0 << numBr <<currentPortee;

	Matrix4  __matrix= ge->getMatrix();

	__matrix=Matrix4::translation(ge->getPtrBrc()->getBranc()->getPlant()->getGeometry().getPosition())*__matrix;

	Vector3 dp, dssb ,ds, t;



	dssb=__matrix.getNormalVector();
	ds=__matrix.getSecondaryVector();
	dp=__matrix.getMainVector()*(float)ge->getLength();
	t=__matrix.getTranslation();

	__ligstream << (float) dp[1]<< (float) dssb[1]<< (float) ds[1]<< (float) t[1];
	__ligstream << (float) -dp[0]<< (float) -dssb[0]<< (float) -ds[0]<< (float) -t[0];
	__ligstream << (float) dp[2]<< (float) dssb[2]<< (float) ds[2]<< (float) t[2];

	__ligstream << (float)ge->getBottomDiam()<< (float)ge->getTopDiam();

	__ligstream << elemId;

}




void LineTreeFormatNatfx::headerArc(GeomManager * geomt)
{
	__arcstream<<geomt->getElemeGeoNumber();
}

void LineTreeFormatNatfx::printElemArc(GeomElemCone * ge, int elemId)
{

	unsigned short i;
	int numBr;


	if(((BrancAMAP *)ge->getPtrBrc()->getAxe()->getBranc())->nature == SimpleImmediateOrgan)
		numBr=-1;
	else
		numBr=ge->getPtrBrc()->getNum();


	Vector3 orig= ge->getMatrix().getTranslation();

	unsigned short nb_children = (unsigned short)vectId[elemId].nbChild;;
	__arcstream<<elemId<<vectId[elemId].idParent<<nb_children<<(float)orig[0]<<(float)orig[1]<<(float)orig[2]<<(float)1.0<<(float)ge->getBottomDiam()<<(float)ge->getTopDiam()<<(short)ge->getSymbole()<<numBr<<currentPortee;

	for(i=0; i<nb_children; i++)
	{
		unsigned int id_son = (unsigned int)vectId[elemId].idChild[i];
		__arcstream<<id_son;
	}


}

void LineTreeFormatNatfx::printInf(GeomManager * geomt)
{

	unsigned int i;
	std::string finame=filename+".inf";
	std::ofstream inf(finame.c_str());

	Vector3 _max=geomt->getMax();
	Vector3 _min=geomt->getMin();

	PlantAMAP *p = (PlantAMAP *)geomt->getPlant();
	MapParameterFileName lmap = p->getParamName();
	if (lmap.find(string("AMAPSimMod")) != lmap.end())
        inf << "File : " << p->getParamName(string("AMAPSimMod")) << std::endl;
    else if (lmap.find(string("DigRMod")) != lmap.end())
        inf << "File : " << p->getParamName(string("DigRMod")) << std::endl;
    else if (!lmap.empty())
    {
        MapParameterFileName::iterator p = lmap.begin();
        inf << "File : " << p->second << std::endl;
    }
	inf << "Age : "<<10<<"  "<< tabSymb.size()<<"  pattern(s) number of branches 1"  << std::endl;
	inf << "Random_seed 0 Simplification 0 " << std::endl;

	inf.precision(4);

	inf << _max[0]<< " " << _max[1]<< " " << _max[2]<< std::endl;
	inf << _min[0] << " " << _min[1] << " " << _min[2] << std::endl;

	for (i = 0; i <tabSymb.size(); i++)
	{

		if (tabSymb[i] > 0)
			inf << "entre-noeud  "<<tabSymb[i]<<"  nentn105  e" << std::endl;
		else
			inf << "feuille  "<<-tabSymb[i]<<"  feui104  f" << std::endl;


	}

	inf.close();

}


