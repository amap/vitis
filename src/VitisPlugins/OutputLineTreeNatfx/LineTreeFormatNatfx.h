/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  LineTreeFormat.h
///			\brief Definition of the LineTreeFormat class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _LINETREEFORMATNATFX_H
#define _LINETREEFORMATNATFX_H
#include "LineTreeFormat.h"
#include "bfstream.h"
using namespace Vitis;
#include <vector>


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class LineTreeFormat
///			\brief Write data into .lig , .arc, .inf format
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class LineTreeFormatNatfx:public LineTreeFormat
{

		/// \brief arc filestream
		beofstream	 __arcstream;



	public:

	/// \brief Constructor
	/// \param finame base filename (path + short filename - extension)
	LineTreeFormatNatfx(const std::string &finame);

	/// \brief Destructor
	virtual ~LineTreeFormatNatfx(void);

	std::string computeKey(const std::string & filename);
	/// \brief Print the vobj into line-tree and arc
	virtual int initFormat(VitisObject * vobj);

	/// \brief Print a linetree record
	void printRecord(class GeomElemCone * ge, int elemId);

	/// \brief Print the .arc header
	void headerArc(class GeomManager * geomt) ;

	/// \brief Print a arc record
	void printElemArc(class GeomElemCone * ge, int elemId) ;

	/// \brief Print tree data in recursive order
	int  printDataInOrder(VitisObject * vobj, int idBearer);

	void printInf(GeomManager * geomt);

};



#endif

