/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "VitisCoreSignalInterface.h"

struct  Diamzz:public subscriber<messageVitisElemDiameter>
{
	Diamzz (void *p) { subscribe(p);};
	virtual ~Diamzz () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};
struct  Lengthzz:public subscriber<messageVitisElemLength>
{
	Lengthzz (void *p) { subscribe(p);};
	virtual ~Lengthzz () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};
struct  Geom:public subscriber<messageVitisBeginGeomCompute>
{
public:
	Geom (void *p);
	virtual ~Geom ();
	void on_notify (const state& st, paramMsg * data=NULL);
	void init (const std::string& f, void *p);
	void init (void *p);
	void diffusion (GeomBrancCone *f);
	void diffusion (BrancAMAP *f);
	float valdiff (float pos);

private:
	float prevTop;
	int nbvaldiff;
	float corrdiff;
	float *xdiff,*ydiff;
};
struct  Decomp:public subscriber<messageVitisDecompAxeLevel>
{

	Decomp (void *p) {subscribe(p);};
	virtual ~Decomp () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct Diffusion
{
	Diffusion(void *p);
	Diffusion(const std::string& f, void *p);
	virtual ~Diffusion(){;};
	Decomp *d;
	Diamzz *diam;
	Geom *geom;
};
extern "C" DIFF_EXPORT  Diffusion * StartPlugin (const std::string& f, void *p)
{
	std::cout << "loading Diffusion module" << std::endl;
	return new Diffusion (f, p) ;
}
