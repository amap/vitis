/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//#include <windows.h>
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "Bud.h"
#include "BrancAMAP.h"
#include "InterNodeAMAP.h"
#include "externDiff.h"
#include "diff.h"

void Lengthzz::on_notify (const state& , paramMsg * data)
{
	paramElemGeom * p = (paramElemGeom*)data;

    int level = p->gec->getPtrBrc()->getBranc()->getPlant()->getPlantFactory()->getLevelOfDecomposition(p->gec->getPtrBrc()->getBranc()->getClassType());
	DecompAxeLevel *d=p->gec->getPtrBrc()->getBranc()->getCurrentElementOfSubLevel(level);
	if (d)
	{
		float *v = (float*)d->getAdditionalData("length");
		if (v && *v==0)
		{
			*v = (float)p->v;
			d->modifyAdditionalData("length", v);
		}
	}
}
void Diamzz::on_notify (const state& , paramMsg * data)
{
	paramElemGeom * p = (paramElemGeom*)data;

    int level = p->gec->getPtrBrc()->getBranc()->getPlant()->getPlantFactory()->getLevelOfDecomposition(p->gec->getPtrBrc()->getBranc()->getClassType());
	DecompAxeLevel *d=p->gec->getPtrBrc()->getBranc()->getCurrentElementOfSubLevel(level);
	if (d)
	{
		float *v = (float*)d->getAdditionalData("volume");
		if (v)
		{
			float *l = (float*)d->getAdditionalData("length");

			p->v = sqrt (4 * *v / M_PI / *l);
		}
	}
}
Geom::Geom (void *p)
{
	prevTop = 0;
	nbvaldiff = -1;
	xdiff = NULL;
	ydiff = NULL;
	subscribe(p);
}
Geom::~Geom ()
{
	unsubscribe();
	if (xdiff)
	{
		delete [] xdiff;
		xdiff = NULL;
	}
	if (ydiff)
	{
		delete [] ydiff;
		ydiff = NULL;
	}
}

void Geom::init (void *p)
{
	cout <<"Diffusion initialisation with default values"<<endl;
	corrdiff = 1;
	nbvaldiff = 2;
	xdiff = new float[nbvaldiff];
	ydiff = new float[nbvaldiff];
	for (int i=0; i<nbvaldiff; i++)
	{
		xdiff[i] = (float)i;
		ydiff[i] = 1;
	}
}
void Geom::init (const std::string& f, void *p)
{
	int i, nbval;
	float xmin, xmax, prop, surf;

	cout <<"initialisation diffusion "<<f<<endl;

	ifstream fic (f.c_str());

	if (!fic)
	{
		nbvaldiff = -1;
		perror (f.c_str());
		return;
	}
	try
	{
		fic >> corrdiff;
		fic >> nbval;
		if (   !fic
			|| nbval <= 0)
		{
			cout << "Error while reading diffusion file "<< f.c_str() << " (wrong format)"  << endl;
			fic.close();
			nbvaldiff = -1;
			return ;
		}
		if (nbval == 1)
			nbvaldiff = 2;
		else
			nbvaldiff = nbval;

		xdiff = new float[nbvaldiff];
		ydiff = new float[nbvaldiff];
		for (i=0; i<nbval; i++)
		{
			fic >> xdiff[i] >>ydiff[i];
		}
		if (!fic)
		{
			cout << "Error while reading diffusion function in file "<< f.c_str() << endl;
			fic.close();
			nbvaldiff = -1;
			return ;
		}
	}
	catch (std::exception)
	{
		cout << "Error while reading diffusion file "<< f.c_str() << endl;
		fic.close();
		nbvaldiff = -1;
		return ;
	};

	fic.close();

	if (nbval == 1)
	{
		xdiff[1] = xdiff[0]+1;
		ydiff[1] = ydiff[0];
	}
	for (i=0; i<nbvaldiff; i++)
	{
		if (   i > 0
			&& xdiff[i] <= xdiff[i-1])
		{
			printf ("wrong value order in diffusion table in file %s (line %d)\n", f.c_str(), i+1);
			nbvaldiff = -1;
			return ;
		}
		if (ydiff[i] < 0)
		{
			printf ("negative value in diffusion table in file %s (line %d)\n", f.c_str(), i+1);
			nbvaldiff = -1;
			return ;
		}
	}

/* normaliser les valeurs */
	xmin = xdiff[0];
	xmax = xdiff[nbvaldiff-1];
	prop = xmax - xmin;
	if (prop == 0)
		prop = 1;
	for (i=0; i<nbvaldiff; i++)
		xdiff[i] = (xdiff[i] - xmin) / prop;

	surf = 0;
	for (i=1; i<nbvaldiff; i++)
		surf += (float)((xdiff[i] - xdiff[i-1]) * (ydiff[i] + ydiff[i-1]) / 2.);

	if (surf == 0)
		surf = 1;

	for (i=0; i<nbvaldiff; i++)
		ydiff[i] /= surf;
}
void Geom::on_notify (const state& , paramMsg * data)
{
	paramGeomCompute * p = (paramGeomCompute*)data;

	if (prevTop == Scheduler::instance()->getTopClock())
		return;

	float length, initialLength, initialDiameter, w=0;
	BrancAMAP *b;
	std::vector<GeomBranc*> gbs = p->gm->getGeomBrancStack();

// compute internode length
	b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::RESET);
    int level = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	while (b)
	{
		if (b->nature == SimpleImmediateOrgan)
		{
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}

		b->startPosit();
		if(!b->positOnFirstElementOfSubLevel(level))
		{
			b->endPosit();
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}

		do
		{
			AxisReference *axisref = b->getBud()->axisref;
			int testNumb=b->getTestNumberWithinGu();

			float instant = (float)b->getBud()->endInstant;

			int nbtest = b->getTestNumberWithinBranch();

			while (instant < Scheduler::instance()->getTopClock() - EPSILON)
			{
				nbtest = nbtest +(int)testNumb;
				instant += (float)(1. / b->getBud()->getGrowth()->rythme / axisref->val1DParameter (rtrythme,b->getBud()->getPhyAge(), b));
			}
			b->computeInitialLengthDiameter (&initialLength, &initialDiameter);

			length = b->computeLength (initialLength, nbtest);
			InterNodeAMAP *entn = b->getCurrentEntn();
			float *v = (float*)entn->getAdditionalData("length");
			if (v && *v==0)
				*v = length;
			entn->modifyAdditionalData("length", v);
			int *n = (int*)entn->getAdditionalData ("ring number");
			vector<float> *rv = (vector<float> *)entn->getAdditionalData ("ring volume");
			*n += 1;
			rv->push_back (w);

			entn->modifyAdditionalData ("ring number", n);
			entn->modifyAdditionalData ("ring volume", rv);

		} while(b->positOnNextElementOfSubLevel(level));

		b->endPosit();
		b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
	}

// compute diffusion
	b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::RESET);
	while (b)
	{
		b->startPosit();

		if(!b->positOnFirstElementOfSubLevel(level))
		{
			b->endPosit();
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}
		if (b->nature == SimpleImmediateOrgan)
			diffusion (b);

		b->endPosit();

		b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
	}

	DecompAxeLevel *entn;
	float lng = 0, *l, *v, dbh=0;
	b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::RESET);
	b->startPosit();
	if(b->positOnFirstElementOfSubLevel(level))
	{
		do
		{
			entn = b->getCurrentElementOfSubLevel(level);

			if ((l = (float*)entn->getAdditionalData("length")) != NULL)
				lng += *l;
			if (   lng >= 130
			    && dbh == 0)
			{
				float *v = (float*)entn->getAdditionalData("volume");
				if (v)
				{
					dbh = sqrt (4 * *v / M_PI / *l);
				}
			}
		} while (b->positOnNextElementOfSubLevel(level));
	}
	b->endPosit();
	cout << "height = "<<lng<< " dbh = "<<dbh<<endl;

	prevTop = (float)Scheduler::instance()->getTopClock();
}
void Geom::diffusion (GeomBrancCone *)
{
}

void Geom::diffusion (BrancAMAP *br)
{
// compute length from end of bearer branch to trunk base
	float lng = 0, *l;
	float length, diameter, initialLength, initialDiameter;
	Hierarc *hBearer = br->getCurrentHierarc();
	BrancAMAP *b = br;
	int level = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	DecompAxeLevel *entn;
	bool begin = true;
	int posit;

	do
	{
		posit = hBearer->indexOnBearerAtLowestLevel;
		hBearer = hBearer->getBearer();
		b = (BrancAMAP*)hBearer->getBranc();
		b->startPosit();

		if (begin)
		{
			begin = false;
			b->positOnLastElementOfSubLevel(level);
		}
		else
		{
			b->positOnElementAtSubLevel (posit, level);
		}

		do
		{
			entn = b->getCurrentElementOfSubLevel(level);

			if ((l = (float*)entn->getAdditionalData("length")) != NULL)
				lng += *l;
		}
		while (b->positOnPreviousElementOfSubLevel(level));

		b->endPosit();
	}
	while (hBearer != hBearer->getBearer());

	if (lng == 0)
		return;

	AxisReference *axisref = br->getBud()->axisref;
	int testNumb=br->getTestNumberWithinGu();

	float instant = (float)br->getBud()->endInstant;

	int nbtest = br->getTestNumberWithinBranch();

	while (instant < Scheduler::instance()->getTopClock() - EPSILON)
	{
		nbtest = nbtest + (int)testNumb;
		instant += (float)(1. / br->getBud()->getGrowth()->rythme / axisref->val1DParameter (rtrythme,br->getBud()->getPhyAge(), br));
	}

	br->computeInitialLengthDiameter (&initialLength, &initialDiameter);
	length = br->computeLength (initialLength, nbtest);
	diameter = br->computeDiam (initialDiameter, nbtest);

	float qtt = (float)(corrdiff * length * diameter * (Scheduler::instance()->getTopClock()- prevTop));
	float curvAbciss = 0;

	hBearer = br->getCurrentHierarc();
	begin = true;
	do
	{
		posit = hBearer->indexOnBearerAtLowestLevel;
		hBearer = hBearer->getBearer();
		b = (BrancAMAP*)hBearer->getBranc();
		b->startPosit();

		if (begin)
		{
			begin = false;
			b->positOnLastElementOfSubLevel(level);
		}
		else
		{
			b->positOnElementAtSubLevel (posit, level);
		}

		do
		{
			entn = b->getCurrentElementOfSubLevel(level);

			int *n = (int*)entn->getAdditionalData ("ring number");
			float *v = (float*)entn->getAdditionalData ("volume");
			float length;
			vector<float> *rv = (vector<float> *)entn->getAdditionalData ("ring volume");

			if ((l = (float*)entn->getAdditionalData("length")) != NULL)
				length = *l;
			float w = qtt * length / lng * valdiff (curvAbciss/lng);
			*v += w;
			(*rv)[*n - 1] += w;
			entn->modifyAdditionalData ("volume", v);
			entn->modifyAdditionalData ("ring volume", rv);
			curvAbciss += length;
		}
		while (b->positOnPreviousElementOfSubLevel(level));

		b->endPosit();
	}
	while (hBearer != hBearer->getBearer());

}

float Geom::valdiff (float pos)
{
	int i;

	for (i=0; i<nbvaldiff-1; i++)
		if (pos < xdiff[i+1])
			break;
	float val = ydiff[i] + (ydiff[i+1]-ydiff[i]) * (pos - xdiff[i]) / (xdiff[i+1] - xdiff[i]);
	return val;
}

void Decomp::on_notify (const state& , paramMsg * data)
{
	paramDecompAxeLevel *p = (paramDecompAxeLevel *)data;

	int level = p->d->getPlant()->getPlantFactory()->getLevelOfDecomposition(((BrancAMAP *)p->d->getParentOfLevel(0))->getClassType());

	if (p->d->getLevel()== level)
	{
		BrancAMAP *b = (BrancAMAP *)p->d->getParentOfLevel(0);
		if (b)
		{
			if (b->nature != SimpleImmediateOrgan)
			{
				int *i = new int(0);
				float *l = new float(0);
				float *d = new float(0);
				p->d->addAdditionalData ("ring number", i);
				if (p->d->getAdditionalData ("length") == 0)
					p->d->addAdditionalData ("length", l);
				p->d->addAdditionalData ("volume", d);
				p->d->addAdditionalData ("ring volume", new vector<float>);
			}
		}
	}
};



Diffusion::Diffusion(void *p)
{
	diam = new Diamzz (p);
	d = new Decomp (p);
	geom = new Geom (p);
	geom->init(p);
}
Diffusion::Diffusion(const std::string& f, void *p)
{
	diam = new Diamzz (p);
	d = new Decomp (p);
	geom = new Geom (p);
	geom->init(f, p);
}
