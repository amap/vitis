#ifndef __EXPORTDIFF_H__
#define __EXPORTDIFF_H__


#pragma warning ( disable : 4251 )

#if !defined STATIC_LIB
	#if defined WIN32

			#ifdef DIFF_EXPORTS
				#define DIFF_EXPORT __declspec(dllexport)
			#else
				#define DIFF_EXPORT __declspec(dllimport)
			#endif

	#else
		#define DIFF_EXPORT
	#endif

#else
		#define DIFF_EXPORT
#endif



#endif
