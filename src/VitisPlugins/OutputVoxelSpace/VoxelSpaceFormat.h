/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  ForestryFormat.h
///			\brief Definition of the ForestryFormat class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _VOXELSPACEFORMAT_H
#define _VOXELSPACEFORMAT_H
#include "OutputFormat.h"
#include "Voxel.h"
#include "Plant.h"
#include "externOutputVoxelSpace.h"
#include "bfstream.h"
using namespace Vitis;
#include <vector>


struct IdParentChild
{
	int idParent;

	int * idChild;

	int nbChild;

	IdParentChild() {idParent=0; idChild=NULL; nbChild=0;}

	~IdParentChild()
	{
		delete [] idChild;
	}



};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class ProfileFormat
///			\brief Write data into .lig , .arc, .inf format
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class OUTPUTVOXELSPACE_EXPORT VoxelSpaceFormat:public OutputFormat
{
	protected:
	/// \brief Profile filestream
	std::ofstream   __vxlstream;

	/// \brief Profile file name
	std::string outputName;

	/// \brief geometrical symbols vector
	std::vector<int> tabSymb;

	std::vector<IdParentChild> vectId;

	Uint cptID;

	/// \brief Current "port�e"
	int currentPortee;




	public:

	/// \brief Constructor
	/// \param finame base filename (path + short filename - extension)
	VoxelSpaceFormat(const std::string &finame);

	/// \brief Destructor
	virtual ~VoxelSpaceFormat(void);

	/// \brief Print the vobj into line-tree and arc
	virtual int printData(VitisObject * vobj);

	/// \brief Print the vobj into line-tree and arc
	virtual int initFormat(VitisObject * vobj);

	virtual bool printHeader (const std::string &headerf);
	virtual bool printHeader (Plant *plt);

};



#endif

