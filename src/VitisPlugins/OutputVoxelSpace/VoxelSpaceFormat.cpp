/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#include <windows.h>
#endif
#include "VoxelSpaceFormat.h"
#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "Plant.h"
#include "OutputVoxelSpace.h"



VoxelSpaceFormat::VoxelSpaceFormat(const std::string &finame):OutputFormat(finame),__vxlstream(finame.c_str())
{
	currentPortee=0;
	cptID=0;
}

VoxelSpaceFormat::~VoxelSpaceFormat(void)
{
	__vxlstream.close();
}

bool VoxelSpaceFormat::printHeader(const std::string &headerf)
{
	return true;
}

bool VoxelSpaceFormat::printHeader(Plant *plt)
{
	return true;
}


int VoxelSpaceFormat::initFormat(VitisObject * vobj)
{
	return 1;
}




//raw writing
int VoxelSpaceFormat::printData(VitisObject * vobj)
{
    VoxelSpace *voxelSpace;
    Voxel *v;

	MapVoxelData::const_iterator p;
	VoxelData *vd;


    voxelSpace = VoxelSpace::instance();

    __vxlstream << "VoxelSpace" <<std::endl;
    __vxlstream << "VoxelSize:" << voxelSpace->VoxelSize() << std::endl;
    __vxlstream << "VoxelSpaceSize x:"<<voxelSpace->XSize()<<" y:"<<voxelSpace->YSize()<<" z:"<<voxelSpace->ZSize()<<std::endl<<std::endl;
    __vxlstream << "VoxelSpaceOrogin x:"<<voxelSpace->XOrigin()<<" y:"<<voxelSpace->YOrigin()<<" z:"<<voxelSpace->ZOrigin()<<std::endl<<std::endl;

    v = voxelSpace->addrAt(0,0,0);
//    __vxlstream << "x\ty\tz\tsurface";
    __vxlstream << "x\ty\tz";
//	for (p=v->data.begin(); p!=v->data.end(); p++){
//        __vxlstream << "\t"<<p->first;
//	}
    __vxlstream<<std::endl;
    float voxelSize = VoxelSpace::instance()->VoxelSize();
    int XSize = VoxelSpace::instance()->XSize();
    int YSize = VoxelSpace::instance()->YSize();
    int ZSize = VoxelSpace::instance()->ZSize();

    // Z up direction
    for (int k=0; k<ZSize; k++)
    for (int i=0; i<XSize; i++)
    for (int j=0; j<YSize; j++) {
         v = voxelSpace->addrAt(i,j,k);
        if (v){
            __vxlstream << voxelSpace->XOrigin()+i*voxelSize << "\t" << voxelSpace->YOrigin()+j*voxelSize << "\t" << voxelSpace->ZOrigin()+k*voxelSize;
//            __vxlstream << "\t(surf) " << v->ContainedSurface();
            for (p=v->data.begin(); p!=v->data.end(); p++){
                vd = (VoxelData*)p->second;
                __vxlstream << "\t(" << p->first << ") "<< vd->to_string();
            }
            __vxlstream << endl;
        }
   }

/*
	OutputVoxelSpace *o = (OutputVoxelSpace *)vobj;
	__profstream << "Total_height\t" << o->LngTrunk() << "\tdbh\t" << o->Dbh() << "\tfirst_branch_height\t" << o->FirstBranchHeight() <<std::endl;
	PA pa;
	__profstream << "Annual_shoot\tlength\tnb_cycles\tnb_branches\tBranch\tbase_diameter\tnb_borne_leaves\tleaves_surface" << std::endl;
	for (int i=0; i<o->Vpa().size(); i++)
	{
		__profstream << i << "\t" << o->Vpa().at(i).lng << "\t" << o->Vpa().at(i).nbCycles << "\t" << o->Vpa().at(i).nbBranch << std::endl;
		for (int j=0; j<o->Vpa().at(i).vbrch.size(); j++)
		{
			Brch *brch = o->Vpa().at(i).vbrch.at(j);
			__profstream << "\t\t\t\t" << j << "\t" << brch->diam;
			__profstream << "\t" << brch->nbLeaves;
			__profstream << "\t" << brch->leavesSurface;
			__profstream << std::endl;
		}

	}
*/
	return 1;

}


