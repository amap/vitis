/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef WIN32
#include <unistd.h>
#endif
#include <stdio.h>
#include <string.h>

#include "externOutputVoxelSpace.h"
#include "OutputVoxelSpace.h"
#include "Plant.h"
#include "VoxelSpaceFormat.h"
#include "DebugNew.h"
#include "defAMAP.h"
#include "BrancAMAP.h"
#include "PlantAMAP.h"
#include "Bud.h"
#include "GrowthUnitAMAP.h"

#include "config.inl"

//Noter que DP = Z
//DS = X
//ECHEL [0] = echel pour dp -> Z


OutputVoxelSpace::OutputVoxelSpace():Output()
{
	lngTrunk = 0;
	dbh = -1;
	firstBranchHeight = -1;
	vpa.clear();
}

OutputVoxelSpace::~OutputVoxelSpace(void)
{
}



void OutputVoxelSpace::freeData ()
{
	v_plt->getGeometry().clear();
	vpa.clear();
}

void OutputVoxelSpace::computeDataToOutput ()
{
}

void OutputVoxelSpace::writeData (const std::string &finame)
{

	int i;
	char iname[256], oname[256];
	strcpy (iname, finame.data());
	prepareName (oname, iname);

	std::stringstream sout;
	sout << oname;
	sout<< "_"<<v_plt->getConfigData().randSeed;
	std::string fname (sout.str());
//	const std::string fname (oname);

	outFormat= new VoxelSpaceFormat(fname+".vxl");

	outFormat->initFormat(v_plt);

	std::cout<<"Output voxel data to "<< fname << ".vxl" <<std::endl;

	outFormat->printData(this);

	delete outFormat;

}

void OutputVoxelSpace::computeBranchValues (GeomBranc *g, Brch *brch)
{
}



#if !defined STATIC_LIB
extern "C" OUTPUTVOXELSPACE_EXPORT Output * StartPlugin ()
{
	return new OutputVoxelSpace();
}
#endif


