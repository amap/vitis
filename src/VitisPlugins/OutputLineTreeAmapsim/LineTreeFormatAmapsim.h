/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  LineTreeFormat.h
///			\brief Definition of the LineTreeFormat class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _LINETREEFORMATAMAPSIM_H
#define _LINETREEFORMATAMAPSIM_H
#include "LineTreeFormat.h"
#include "bfstream.h"
using namespace Vitis;
#include <vector>


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class LineTreeFormatAmapsim
///			\brief Write data into .lig , .brc, .inf format
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class LineTreeFormatAmapsim : public LineTreeFormat
{

		/// \brief brc filestream
		beofstream	 __brcstream;


	public:

	/// \brief Constructor
	/// \param finame base filename (path + short filename - extension)
	LineTreeFormatAmapsim(const std::string &finame);

	/// \brief Destructor
	virtual ~LineTreeFormatAmapsim(void);

	std::string computeKey(const std::string & filename);
	virtual int initFormat (VitisObject * vobj);

	/// \brief Print a linetree record
	virtual void printRecord(class GeomElemCone * ge, int elemId);

	/// \brief Print tree data in recursive order
	int  printDataInOrder(VitisObject * vobj, int idBearer);

	/// \brief Print a brc record
	void outBrc (GeomBranc *gb);

	virtual int printSubStructure(GeomBranc * gb);

	void printInf(GeomManager * geomt);

private:
	int nbBranc;

};



#endif

