/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include "OutputGeomAmapsim.h"
#include "Plant.h"
#include "LineTreeFormatAmapsim.h"
#include "DebugNew.h"

#include "config.inl"

//Noter que DP = Z
//DS = X
//ECHEL [0] = echel pour dp -> Z


OutputGeomAmapsim::OutputGeomAmapsim():OutputGeom()
{

}

void OutputGeomAmapsim::writeData (const std::string &finame)
{

	char iname[256], oname[256];

	std::stringstream sout;
	sout << finame;
	sout<< "_"<<v_plt->getConfigData().randSeed;
	std::string fname (sout.str());

	std::cout<<"Output Geometry linetree Amapsim to "<<fname<<" ("<<v_plt->getGeometry().getGeomBrancStack().size()<<" axes)"<<std::endl;

#ifdef WIN32
	strcpy_s (iname, 256, fname.data());
#else
	strcpy (iname, fname.data());
#endif
	prepareName (oname, iname);

	fname.assign(oname);

	outFormat= new LineTreeFormatAmapsim(fname);
	outFormat->setSwap (swap);
	outFormat->initFormat(&(v_plt->getGeometry()));

	if (v_plt->getGeometry().getGeomBrancStack().size() > 0)
		outFormat->printData(v_plt->getGeometry().getGeomBrancStack().at(0));
	else
		std::cout << "empty geometry"<<std::endl;

	std::cout << "end geometry output"<<std::endl;

	delete outFormat;

}




#if !defined STATIC_LIB
extern "C" Output * StartPlugin ()
{
	return new OutputGeomAmapsim();
}
#endif


