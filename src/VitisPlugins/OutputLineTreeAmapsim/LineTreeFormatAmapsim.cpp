/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#include <windows.h>
#else
#include <string.h>
#endif
#include "LineTreeFormatAmapsim.h"
#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "GeomBrancCone.h"
#include "UtilMath.h"
#include "Bud.h"
#include "PlantAMAP.h"
//#include "DebugNew.h"


extern "C"
{
#include "fcrypt0.c"
}




LineTreeFormatAmapsim::LineTreeFormatAmapsim(const std::string &finame):LineTreeFormat(finame),__brcstream(finame+".brc")
{
cout << "creating " << finame << endl;
	nbBranc=0;
}

LineTreeFormatAmapsim::~LineTreeFormatAmapsim(void)
{
	__ligstream.getStream().close();

	__brcstream.getStream().seekp(0,ios_base::beg);
	__brcstream << nbBranc;
	__brcstream.getStream().close();
}

std::string LineTreeFormatAmapsim::computeKey(const std::string & filename)
{
	char bufg[128];
	char   client[16], scode[16], cle[32], tmp[256];
	unsigned char sel[16];
	int    clg, poscle;
	float  maxx;
	char parametre[256];
	unsigned int i;
	long pos[20]={12,56,46,1,65,75,4,19,51,54,31,26,30,25,49,67,52,15,17,71};

	strncpy(parametre, filename.c_str(),256);

	sel[0] = 'J';
	sprintf (client, "%hd", 5467);
	clg = 5467 + 83837;
	for (i=0; i<strlen(parametre); i++)
		clg += *(parametre+i) - 49;
	maxx = 1.0F / clg;
	sel[1] = 'F';
	for (i=0; i<80; i++)
	{
		maxx = (float)(maxx * 73.0 - (int)(maxx * 73.0));
		bufg[i] = (char)((int)(maxx * 92.0) + 33);
	}
	bufg[80] = 0;
	clg = (int)strlen(parametre) - 4;
	sprintf (scode, "%s%s",client,&parametre[clg]);
	sel[2] = 0;
	strncpy (cle, (char *)mycrypt (scode,(unsigned char *)sel), 32);  // sel = JF

	poscle = 0;
	for (i=2; i<strlen(cle); i++)
		poscle += cle[i];

	for (i=0; i<strlen(cle)-2; i++)
		bufg[(poscle+pos[i])%80] = cle[i+2];


	return std::string(bufg);
}

int LineTreeFormatAmapsim::initFormat (VitisObject * vobj)
{
	GeomManager * gb = (GeomManager *) vobj;

	vectId.resize(gb->getElemeGeoMaxNumber());

	PlantAMAP *p = (PlantAMAP*)gb->getPlant();

	printHeader(computeKey(p->getParamName(string("AMAPSimMod"))));

	__brcstream << nbBranc;

	return 0;
}

//Order writing
int LineTreeFormatAmapsim::printDataInOrder(VitisObject * vobj, int idParent)
{

	unsigned int i,j;
	GeomBranc * gb = (GeomBranc *) vobj;
	int curId;

	curId = idParent;

	//position dans la branche
	currentPortee=0;

    outBrc (gb);
	//SORTIE BRC

	//Pour chaque �l�ment g�om�trique
	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
		currentPortee=i;
		printRecord((GeomElemCone *)gb->getElementsGeo().at(i),curId);

		cptID++;

		//Pour chaque �l�ment port� par celui courant
		for(j=0; j<gb->getElementsGeo().at(i)->getGeomChildren().size(); j++)
			//for (j=0; j<gb->getAxe().getNbBorne(); j++)
		{

			//On lance la r�cursivit� sur un axe port�

			//Ici on indique l'id du parent ( ici l'id courant) � l'�l�ment qui vient d'�tre cr��
            if (gb->getElementsGeo().at(i)->getGeomChildren().at(j)->getElementsGeo().size() > 0)
                printDataInOrder(gb->getElementsGeo().at(i)->getGeomChildren().at(j), curId);
		}
	}

	return 1;

}



void LineTreeFormatAmapsim::printRecord(GeomElemCone * ge, int)
{
    int numBr, i, symb;

	numBr=ge->getPtrBrc()->getNum();

	symb = ge->getSymbole();
	if (((BrancAMAP*)ge->getPtrBrc()->getBranc())->nature == SimpleImmediateOrgan)
		symb = -symb;
	for (i=0; i<tabSymb.size(); i++)
		if (tabSymb[i] == symb)
			break;
	if (i == tabSymb.size())
		tabSymb.push_back(symb);

	BrancAMAP *b=(BrancAMAP*)(ge->getPtrBrc()->getBranc());
	b->startPosit();
	b->positOnElementAtSubLevel((int)ge->getPosInBranc(), b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType()));
	if (!swap)
	{
		__ligstream << long(ge->getSymbole()) << long(1) << long(b->getCurrentGu()->phyAgeGu - 1.) <<long(currentPortee);
	}
	else
	{
		__ligstream << swapDatal(long(ge->getSymbole())) << swapDatal(long(1)) << swapDatal(numBr) <<swapDatal(long(currentPortee));
	}
		b->endPosit();


	Matrix4  __matrix= ge->getMatrix();

	__matrix=Matrix4::translation(ge->getPtrBrc()->getBranc()->getPlant()->getGeometry().getPosition())*__matrix;

	Vector3 dp, dssb ,ds, t;
	double lg=ge->getLength();


	dssb=__matrix.getNormalVector();
	ds=__matrix.getSecondaryVector();
	dp=__matrix.getMainVector()*(float)ge->getLength();
	t=__matrix.getTranslation();

/*
	__ligstream << (float) dp[1]<< (float) dssb[1]<< (float) ds[1]<< (float) t[1];
	__ligstream << (float) -dp[0]<< (float) -dssb[0]<< (float) -ds[0]<< (float) -t[0];
	__ligstream << (float) dp[2]<< (float) dssb[2]<< (float) ds[2]<< (float) t[2];

	__ligstream << (float)ge->getBottomDiam()<< (float)ge->getTopDiam();

	__ligstream << (long)numBr;
*/
	if (!swap)
	{
	__ligstream << (float) dp[1]<< (float) dssb[1]<< (float) ds[1]<< (float) t[1];
	__ligstream << (float) -dp[0]<< (float) -dssb[0]<< (float) -ds[0]<< (float) -t[0];
	__ligstream << (float) dp[2]<< (float) dssb[2]<< (float) ds[2]<< (float) t[2];

	__ligstream << (float)ge->getBottomDiam()<< (float)ge->getTopDiam();

	__ligstream << (long)numBr;
	}
	else
	{
	__ligstream << swapDataf((float) dp[1])<< swapDataf((float) dssb[1])<< swapDataf((float) ds[1])<< swapDataf((float) t[1]);
	__ligstream << swapDataf((float) -dp[0])<< swapDataf((float) -dssb[0])<< swapDataf((float) -ds[0])<< swapDataf((float) -t[0]);
	__ligstream << swapDataf((float) dp[2])<< swapDataf((float) dssb[2])<< swapDataf((float) ds[2])<< swapDataf((float) t[2]);

	__ligstream << swapDataf((float)ge->getBottomDiam())<< swapDataf((float)ge->getTopDiam());

	__ligstream << swapDatal((long)numBr);
	}


}

void LineTreeFormatAmapsim::outBrc (GeomBranc *gb)
{
	unsigned int i,j;
 	unsigned short nbBorne;
	GeomBrancCone *curGeomBrancCone, *clone;

	if (gb->getElementsGeo().size() > 0)
	{
		if (nbBranc <= gb->getNum())
			nbBranc = gb->getNum()+1;

		__brcstream<<gb->getNum();

		__brcstream<<gb->getGeomBrancBearer()->getNum();
		__brcstream<<gb->getPosOnBearer()+1;

		nbBorne = 0;
		for(i=0; i<gb->getElementsGeo().size(); i++)
		{
			for(j=0; j<gb->getElementsGeo().at(i)->getGeomChildren().size(); j++)
			{
				curGeomBrancCone=(GeomBrancCone*)gb->getElementsGeo().at(i)->getGeomChildren().at(j);
				if (curGeomBrancCone->getElementsGeo().size() > 0)
					nbBorne++;
				else if(   (clone = (GeomBrancCone*)curGeomBrancCone->getGeomBrancClone()) != 0
					    && clone->getElementsGeo().size() > 0)
					nbBorne++;
			}
		}

		__brcstream<<nbBorne;
		for(i=0; i<gb->getElementsGeo().size(); i++)
		{
			for(j=0; j<gb->getElementsGeo().at(i)->getGeomChildren().size(); j++)
			{
				curGeomBrancCone=(GeomBrancCone*)gb->getElementsGeo().at(i)->getGeomChildren().at(j);
				if (   curGeomBrancCone->getElementsGeo().size() > 0
					|| (   (clone = (GeomBrancCone*)curGeomBrancCone->getGeomBrancClone()) != 0
					    && clone->getElementsGeo().size() > 0))
				{
					long num=gb->getElementsGeo().at(i)->getGeomChildren().at(j)->getNum();
					__brcstream<<num;
					__brcstream.getStream().flush();
				}
			}
		}
	}
}

int LineTreeFormatAmapsim::printSubStructure(GeomBranc * gb)
{
	int i = gb->getNum();

    outBrc (gb);
    LineTreeFormat::printSubStructure(gb);

	return 0;
}

void LineTreeFormatAmapsim::printInf(GeomManager * geomt)
{

	unsigned int i;
	std::string finame=filename+".inf";
	std::ofstream inf(finame.c_str());

	Vector3 _max=geomt->getMax();
	Vector3 _min=geomt->getMin();

	PlantAMAP *p = (PlantAMAP *)geomt->getPlant();
	MapParameterFileName lmap = p->getParamName();
	if (lmap.find(string("AMAPSimMod")) != lmap.end())
        inf << "File : " << p->getParamName(string("AMAPSimMod")) << std::endl;
    else if (lmap.find(string("DigRMod")) != lmap.end())
        inf << "File : " << p->getParamName(string("DigRMod")) << std::endl;
    else if (!lmap.empty())
    {
        MapParameterFileName::iterator p = lmap.begin();
        inf << "File : " << p->second << std::endl;
    }
	inf << "Age : "<<10<<"  "<< tabSymb.size()<<"  pattern(s) number of branches 1"  << std::endl;
	inf << "Random_seed 0 Simplification 0 " << std::endl;

	inf.precision(4);

	inf << _max[0]<< " " << _max[1]<< " " << _max[2]<< std::endl;
	inf << _min[0] << " " << _min[1] << " " << _min[2] << std::endl;

	for (i = 0; i <tabSymb.size(); i++)
	{

		if (tabSymb[i] > 0)
			inf << "entre-noeud  "<<tabSymb[i]<<"  nentn105  e" << std::endl;
		else
			inf << "feuille  "<<-tabSymb[i]<<"  feui104  f" << std::endl;


	}

	inf.close();

}


