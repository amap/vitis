/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#include <windows.h>
#endif
#include "profileFormat.h"
#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "GeomBrancCone.h"
#include "UtilMath.h"
#include "Bud.h"
#include "Plant.h"
#include "DebugNew.h"
#include "BrancAMAP.h"



ProfileFormat::ProfileFormat(const std::string &finame):OutputFormat(finame),__profstream(finame.c_str())
{
	currentPortee=0;
	cptID=0;
}

ProfileFormat::~ProfileFormat(void)
{
	__profstream.close();
}

bool ProfileFormat::printHeader(const std::string &headerf)
{
	return true;
}

bool ProfileFormat::printHeader(Plant *plt)
{
	string *s = (string*)plt->getAdditionalData("diff");

	plt->addAdditionalData("outputDiffusionFicName", (void*)outputName.c_str());


	__profstream << "#branch profile for "<<plt->getParamName(string("AMAPSimMod"))<<" computed at "<<Scheduler::instance()->getTopClock()<<" whith "<<*s<<" as diffusion law"<<endl;
	__profstream << plt->getGeometry().getGeomBrancNumber() << " branches" << endl;
	__profstream << "#for each branch :" << endl;
	__profstream << "#a global branch information whith : Index Order BearerIndex BearerLngPosition UtNumber InitialDiam BearerUtPosition incl azim" << endl;
	__profstream << "#for each gu : Lng Leavesnumber Ringnumber Phy1 Phy2 ..." << endl;

	return true;
}


int ProfileFormat::initFormat(VitisObject * vobj)
{
	printHeader((Plant*)vobj);

	return 1;

}




//raw writing
int ProfileFormat::printData(VitisObject * vobj)
{

	GeomBranc * gb = (GeomBranc *) vobj;
	Plant * plt= gb->getBranc()->getPlant();

	if(plt->getConfigData().cfg_supersimplif)
        printSubStructure (gb);
	else
		printDataInOrder(gb, 0);

	return 1;

}

int ProfileFormat::printSubStructure(GeomBranc * gb)
{
	int i, j;

	Matrix4 bearTransla;

	GeomBrancCone * curGeomBrancCone, *clone;

	currentPortee=0;

	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
        GeomElemCone *cone = (GeomElemCone *)gb->getElementsGeo().at(i);

        if (i == 0)
            bearTransla = Matrix4::translation(cone->getMatrix().getTranslation());

		printRecord(cone, 0 );

		for(j=0; j<cone->getGeomChildren().size(); j++)
		{

			curGeomBrancCone=(GeomBrancCone*)cone->getGeomChildren().at(j);

			if((clone = (GeomBrancCone*)curGeomBrancCone->getGeomBrancClone()))
			{
				Matrix4 & matTrans=curGeomBrancCone->getCurrentTransform();
                Vector3 transla = clone->getElementsGeo().at(0)->getMatrix().getTranslation();

                curGeomBrancCone->copyFrom(clone, bearTransla*matTrans*Matrix4::translation(-transla));
			}

            printSubStructure(curGeomBrancCone);

		}

		currentPortee++;

	}

	return 1;
}


//Order writing
int ProfileFormat::printDataInOrder(VitisObject * vobj, int idParent)
{

	int i,j,nbChild;
	unsigned short nbBorne;
	GeomBranc * gb = (GeomBranc *) vobj;
	int curId;

	BrancAMAP *b=(BrancAMAP*)(gb->getBranc());

	if (b->nature == 2)
		return 0;

	curId = idParent;

	b->getElementNumberOfSubLevel (GROWTHUNIT);

	Plant * plt= gb->getBranc()->getPlant();
	int level = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());

	//position dans la branche
	currentPortee=0;

	//Pour chaque �l�ment g�om�trique
	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
		//Si on est pas sur le premier neoud de l'axe, on indique l'id du noeud porteur sur l'axe courant
		vectId[cptID].idParent=curId;


		curId=cptID;



		//Si on est pas sur le premier noeud de la plante, on maj les relations p�res-fils
		nbChild = 0;
		if(curId != 0)
		{
			int idBearer=vectId[curId].idParent;

			vectId[idBearer].idChild[vectId[idBearer].nbChild]=curId;

			vectId[idBearer].nbChild++;

		}
		nbChild=gb->getElementsGeo().at(i)->getGeomChildren().size();
		if (i < gb->getElementsGeo().size()-1)
			nbChild ++;

		if (nbChild > 0)
			vectId[curId].idChild = new int[nbChild];

		currentPortee=i;
		printRecord((GeomElemCone *)gb->getElementsGeo().at(i),curId);

		cptID++;

		//Pour chaque �l�ment port� par celui courant
		for(j=0; j<gb->getElementsGeo().at(i)->getGeomChildren().size(); j++)
			//for (j=0; j<gb->getAxe().getNbBorne(); j++)
		{

			//On lance la r�cursivit� sur un axe port�

			//Ici on indique l'id du parent ( ici l'id courant) � l'�l�ment qui vient d'�tre cr��
            if (gb->getElementsGeo().at(i)->getGeomChildren().at(j)->getElementsGeo().size() > 0)
                printDataInOrder(gb->getElementsGeo().at(i)->getGeomChildren().at(j), curId);
		}
	}

	return 1;

}



void ProfileFormat::printRecord(GeomElemCone * ge, int elemId)
{
    int numBr;

	numBr=ge->getPtrBrc()->getNum();

	tabSymb.push_back(ge->getSymbole());

	Branc *b=(Branc*)(ge->getPtrBrc()->getBranc());

	b->startPosit();
	b->positOnElementAtSubLevel(ge->getPosInBranc(), b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType()));
//	__ligstream << long(ge->getSymbole()) << long(1) << long(1) <<long(currentPortee);
	b->endPosit();

	Matrix4  __matrix= ge->getMatrix();

	__matrix=Matrix4::translation(ge->getPtrBrc()->getBranc()->getPlant()->getGeometry().getPosition())*__matrix;

	Vector3 dp, dssb ,ds, t;



	dssb=__matrix.getNormalVector();
	ds=__matrix.getSecondaryVector();
	dp=__matrix.getMainVector()*ge->getLength();
	t=__matrix.getTranslation();

//	__ligstream << (float) dp[1]<< (float) dssb[1]<< (float) ds[1]<< (float) t[1];
//	__ligstream << (float) -dp[0]<< (float) -dssb[0]<< (float) -ds[0]<< (float) -t[0];
//	__ligstream << (float) dp[2]<< (float) dssb[2]<< (float) ds[2]<< (float) t[2];

//	__ligstream << (float)ge->getBottomDiam()<< (float)ge->getTopDiam();

//	__ligstream << (long)numBr;


}



