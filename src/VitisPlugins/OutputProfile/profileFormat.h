/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  ProfileFormat.h
///			\brief Definition of the ProfileFormat class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _PROFILEFORMAT_H
#define _PROFILEFORMAT_H
#include "OutputFormat.h"
#include "Plant.h"
#include "externOutputProfile.h"
#include "bfstream.h"
using namespace Vitis;
#include <vector>


struct IdParentChild
{
	int idParent;

	int * idChild;

	int nbChild;

	IdParentChild() {idParent=0; idChild=NULL; nbChild=0;}

	~IdParentChild()
	{
		delete [] idChild;
	}



};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class ProfileFormat
///			\brief Write data into .lig , .arc, .inf format
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class OUTPUTPROFILE_EXPORT ProfileFormat:public OutputFormat
{
	protected:
	/// \brief Profile filestream
	std::ofstream   __profstream;

	/// \brief Profile file name
	std::string outputName;

	/// \brief geometrical symbols vector
	std::vector<int> tabSymb;

	std::vector<IdParentChild> vectId;

	Uint cptID;

	/// \brief Current "port�e"
	int currentPortee;




	public:

	/// \brief Constructor
	/// \param finame base filename (path + short filename - extension)
	ProfileFormat(const std::string &finame);

	/// \brief Destructor
	virtual ~ProfileFormat(void);

	/// \brief Print the vobj into line-tree and arc
	virtual int printData(VitisObject * vobj);

	/// \brief Print the vobj into line-tree and arc
	virtual int initFormat(VitisObject * vobj);

	virtual bool printHeader (const std::string &headerf);
	virtual bool printHeader (Plant *plt);

	/// \brief Print a Profile record
	virtual void printRecord(class GeomElemCone * ge, int elemId);

	/// \brief Print tree data in recursive order
	virtual int  printDataInOrder(VitisObject * vobj, int idBearer);

	virtual int printSubStructure(class GeomBranc * gb);

};



#endif

