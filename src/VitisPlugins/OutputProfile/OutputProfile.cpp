/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include "externOutputProfile.h"
#include "OutputProfile.h"
#include "Plant.h"
#include "profileFormat.h"
#include "DebugNew.h"

#include "config.inl"

//Noter que DP = Z
//DS = X
//ECHEL [0] = echel pour dp -> Z


OutputProfile::OutputProfile():Output()
{

}

OutputProfile::~OutputProfile(void)
{
}



void OutputProfile::freeData ()
{
	v_plt->getGeometry().clear();
}

void OutputProfile::computeDataToOutput ()
{
	v_plt->computeGeometry();
}

void OutputProfile::writeData (const std::string &finame)
{

	int i;
	char iname[256], oname[256];
	strcpy (iname, finame.data());
	prepareName (oname, iname);

	const std::string fname (oname);

	outFormat= new ProfileFormat(fname+".prof");

	outFormat->initFormat(v_plt);

	std::cout<<"Output tree profile"<<std::endl;

	outFormat->printData(v_plt->getGeometry().getGeomBrancStack().at(0));

	delete outFormat;

}




#if !defined STATIC_LIB
extern "C" OUTPUTPROFILE_EXPORT Output * StartPlugin ()
{
	return new OutputProfile();
}
#endif


