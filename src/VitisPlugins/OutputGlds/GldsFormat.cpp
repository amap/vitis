/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "GldsFormat.h"
#include <algorithm>
#include <sstream>
#ifndef WIN32
#include <stdio.h>
#include <string.h>
#endif
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "AxisReference.h"
//#include "Root.h"
#include "GeomBrancAMAP.h"
#include "CycleAMAP.h"
#include "PlantAMAP.h"
#include "UtilMath.h"
#include "Bud.h"
#include "Plant.h"
#include "DebugNew.h"
#include "Root.h"
#include "GeomRoot.h"





GldsFormat::GldsFormat(const std::string &finame):OutputFormat(finame+".opf"),stream((finame+".opf").c_str())
{
	currentPortee=0;
	currentNbElem=0;
	baseName = finame;
	while (   baseName.at(baseName.length()-1) == '_'
		   || (   baseName.at(baseName.length()-1) >= '0'
		       && baseName.at(baseName.length()-1) <= '9'))
		baseName.resize(baseName.length()-1);

}

GldsFormat::~GldsFormat(void)
{

	for (it = symbolList.begin(); it != symbolList.end(); it++)
	{
		Symbol *s = *it;
		delete (s);
	}
}


int GldsFormat::initFormat(VitisObject * vobj)
{
	GeomManager * gb = (GeomManager *) vobj;

	//Node root = doc.createRootNode("gtds");

	//Text hlig=root.addChild("headerlig");
	//hlig.setValue("noHeader");

//	hlig=LineTreeFormat::computeKey(this->filename);

	//currentNode= root;

	geomId = 2;

	stream << "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>" << endl;
	stream << "<opf version=\"2.0\" src=\"AmapSim\">" << endl;
	stream << "\t<headerlig>noHeader</headerlig>" << endl;

	string name;
	char cname[512];
	MapParameterFileName lmap = gb->getPlant()->getParamName();
	if (lmap.find(string("AMAPSimMod")) != lmap.end())
	{
        gb->getPlant()->getParamName(string("AMAPSimMod")).copy (cname, gb->getPlant()->getParamName(string("AMAPSimMod")).size());
        cname[gb->getPlant()->getParamName(string("AMAPSimMod")).size()] = 0;
        cname[strlen(cname)-4] = 0;
        while (   (   cname[strlen(cname)-1] >= '0'
                   && cname[strlen(cname)-1] <= '9')
               || cname[strlen(cname)-1] == '_')
           cname[strlen(cname)-1] = 0;

        int pos = strlen(cname)-1;
        while (   pos >= 0
               && cname[pos] != '\\'
               && cname[pos] != '/'
               && cname[pos] != ':')
           pos --;
        if (cname[pos+1] == '1')
        {
            for (int i=pos+1; i<strlen(cname); i++)
                cname[i] = cname[i+1];
        }
        strcat (cname, ".headeropf");
        name.assign(cname);

        ifstream f;
    //	f.open((baseName+".headeropf").c_str(), ios_base::out);
        f.open(name.c_str(), ios_base::out);
        if (f.is_open())
        {
            char str[512];
            int index;
            while (!f.eof())
            {
                f.getline (str, 512);
                string line = string(str);
                size_t pos;
                if ((pos=line.find ("<shape Id=\"")) != string::npos)
                {
                    pos = line.find ("\"");
                    size_t endPos = line.rfind ("\"");
                    string sstr = line.substr (pos+1, endPos-pos-1);
                    index = atoi (sstr.c_str());
                }
                if ((pos=line.find ("<name>")) != string::npos)
                {
                    pos = line.find (">");
                    size_t endPos = line.rfind ("<");
                    string sstr = line.substr (pos+1, endPos-pos-1);
                    Symbol *s = new Symbol;
                    s->index = index;
                    s->name = sstr;
                    symbolList.push_back(s);
                }
                stream << str << endl;
            }
            f.close();
        }
    }
	return 1;
}

void GldsFormat::printSubStruct(GeomManager & gtr)
{

	int i,indice;
	std::vector<GeomBranc *> & vecGeomBranc = gtr.getGeomBrancStack();
	std::vector<GeomSuperSimpl *> & vecGeomSSimp= gtr.getGeomSimplStack();
	std::vector<GeomSuperSimpl *>::iterator it_ssimp;

	Plant * plt= gtr.getPlant();

//	Node nsstruct=currentNode.addChild("topo_substructure");

	stream << "\t\t<substructure>" << endl;

	for(it_ssimp=vecGeomSSimp.begin(); it_ssimp != vecGeomSSimp.end(); ++it_ssimp)
	{

		if((*it_ssimp)!= NULL)
		{
			if((*it_ssimp)->secteurAng!=NULL)
			{
				for(i=0; i<plt->getConfigData().cfg_supersimplif ; i++)
				{
					indice=(*it_ssimp)->secteurAng[i];
					if(indice!=-1)
					{

						stream << "\t\t\t<sstruct ref\"" << indice << " class=\"sstruct\">" << endl;

						printAxe(vecGeomBranc[indice], string("\t\t\t\t"));

						stream << "\t\t\t</sstruct>" << endl;
					}
				}
			}
		}
	}

	stream << "\t\t</substructure>" << endl;

}


int GldsFormat::printData(VitisObject * vobj)
{

	GeomBranc * gb = (GeomBranc *) vobj;

	Plant * plt= gb->getBranc()->getPlant();



	if (gb->getBranc()->getClassType() == "DigRMod"){

        Root * r = (Root*) gb->getBranc();

        //changing the scale in order to manipulate the decomp and follow order.

        stream << "\t<topology  id=\"1\" class=\"Individu\" nb_axes=\""
               << plt->getTopology().NbHierarc()
               << "\" parameter_file=\"" << plt->getParamName(string("DigRMod"))
               << "\" scale=\""<< gb->getScale() << "\">" << endl;


        stream << "\t<age>"<<  Scheduler::instance()->getHotStop()  <<"</age>\n";

        (gb->getElementsGeo().size());


        printRecordForRoot((GeomElemCone *)gb->getElementsGeo().at(0),"\t",false);

       // printAdditionalData(plt, string("\t"));

        printDataInOrder(gb, string("\t\t\t"));

        stream << "\t</topology>" << endl;

	}
    else
    {
        stream << "\t<topology  id=\"1\" class=\"plant\" nb_axes=\""
               << plt->getTopology().NbHierarc() << "\" parameter_file=\""
               << plt->getParamName(string("AMAPSimMod"))
               << " \" scale=\"" << 1 << "\">" << endl;

        printAdditionalData(plt, string("\t"));

        if(plt->getConfigData().cfg_supersimplif)
        {
            printSubStruct(plt->getGeometry());
        }
        else
        {
            printDataInOrder(gb, string("\t\t"));
        }

        stream << "\t</topology>" << endl;
	}
	return 1;

}

void GldsFormat::closeFormat ()
{
	stream << "</opf>" << endl;
}

void GldsFormat::printAdditionalData (VitisObject *o, string decal)
{
	for (int i=0; i<o->additionalDataNumber(); i++)
	{
		if (o->isAdditionalDataPrintable (i))
		{
			PrintableObject *p=(PrintableObject*)o->getAdditionalData(i);

			stream << decal << "\t<" << o->getAdditionalDataCriteria(i) << ">";
			stream << p->to_string();
			stream << "</" << o->getAdditionalDataCriteria(i) << ">" << endl;

			//Text t=curNod.addChild(o->getAdditionalDataCriteria(i));
			//t.setValue (p->to_string());
		}
	}
}

//int GldsFormat::printAxe(GeomBranc * gb, Node & curNod, string decal)
int GldsFormat::printAxe(GeomBranc * gb, string decal)
{
	int i, j, k;
	int indexWithinGu, indexWithinCycle, EntnNumberWithinGu, EntnNumberWithinCycle;

	Vector3 dp, dssb ,ds, t;

	//Node axefils;

	GeomBrancCone * curGeomCone;

	GrowthUnitAMAP *u;
	CycleAMAP *e;


	string decalUT, decalCycle;

	currentPortee=0;

	printAdditionalData (gb, decal);
	printAdditionalData (gb->getBranc(), decal);

	BrancAMAP *b = (BrancAMAP*)gb->getBranc();
	DecompAxeLevel *d = b;
	b->startPosit();
	b->positOnFirstElementOfSubLevel(INTERNODE);

	int firstGu=0,firstCycle=0,firstZone=0,firstEntn=0;

	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
		for (j=b->getCurrentEntnIndexWithinBranch(); j<=((GeomElemCone *)gb->getElementsGeo().at(i))->getPosInBranc(); j++)
		{
			if (b->getCurrentEntnIndexWithinGu() == 0)
			{
				u = b->getCurrentGu();
				if (u->getElementNumberOfSubLevel(INTERNODE) > 1)
				{
					d = u;

					decalUT = string ("\t");
					if (firstGu==0)
					{
//						stream << decal << "\t<decomp id=\"" << u->getId() << "\" class=\"" << u->getName() << "\">" << endl;
						stream << decal << "\t<decomp id=\"" << getId() << "\" class=\"" << u->getName() << " \" scale=\""<< GROWTHUNIT +2 << "\">" << endl;
						firstGu = 1;
					}
					else
					{
//						stream << decal << "\t<follow id=\"" << u->getId() << "\" class=\"" << u->getName() << "\">" << endl;
						stream << decal << "\t<follow id=\"" << getId() << "\" class=\"" << u->getName() << " \" scale=\""<< GROWTHUNIT +2 << "\">" << endl;
						firstGu = 2;
					}
					firstCycle=0;
					firstZone=0;
					firstEntn=0;
//					stream << decal << "\t<topo_GU id=\"" << u->CurrentIndex() << "\" class=\"" << u->getName() << "\">" << endl;
					stream << decal << "\t<topoId>" << u->getId() << "</topoId>" << endl;

					stream << decal << "\t\t<phyAge>" << u->phyAgeGu << "</phyAge>" << endl;
					stream << decal << "\t\t<index>" << u->CurrentIndex() << "</index>" << endl;

					printAdditionalData (u, decal+"\t");
				}
			}
			if (   b->getCurrentEntnIndexWithinCycle() == 0
				&& u->getElementNumberOfSubLevel(GROWTHCYCLE) > 1)
			{
				e = (CycleAMAP*)b->getCurrentElementOfSubLevel(GROWTHCYCLE);
				if (e->getElementNumberOfSubLevel(INTERNODE) > 0)
				{
					d = e;

					decalCycle = string ("\t");
					if (firstCycle==0)
					{
//						stream << decal << "\t\t<decomp id=\"" << e->getId() << "\" class=\"" << e->getName() << "\">" << endl;
						stream << decal << "\t\t<decomp id=\"" << getId() << "\" class=\"" << e->getName() << " \" scale=\""<< GROWTHCYCLE +2 << "\">" << endl;
						firstCycle = 1;
					}
					else
					{
//						stream << decal << "\t\t<follow id=\"" << e->getId() << "\" class=\"" << e->getName() << "\">" << endl;
						stream << decal << "\t\t<follow id=\"" << getId() << "\" class=\"" << e->getName() << " \" scale=\""<< GROWTHCYCLE +2 << "\">" << endl;
						firstCycle = 2;
					}
					firstZone=0;
					firstEntn=0;
//					stream << decal << "\t\t<topo_Cycle id=\"" << b->getCurrentCycleIndexWithinGu() << "\" class=\"" << e->getName() << "\">" << endl;
					stream << decal << "\t<topoId>" << e->getId() << "</topoId>" << endl;

					stream << decal << decalUT << "\t\t<index>" << b->getCurrentCycleIndexWithinGu() << "</index>" << endl;
					printAdditionalData (e, decal+decalUT+"\t");
				}
			}

			if (firstEntn == 0)
			{
//				stream << decal << decalUT << decalCycle << "\t<decomp id=\"" << d->getCurrentElementOfSubLevel(INTERNODE)->getId();
				stream << decal << decalUT << decalCycle << "\t<decomp id=\"" << getId();
				firstEntn=1;
			}
			else
			{
//				stream << decal << decalUT << decalCycle << "\t<follow id=\"" << d->getCurrentElementOfSubLevel(INTERNODE)->getId();
				stream << decal << decalUT << decalCycle << "\t<follow id=\"" << getId();
				firstEntn=2;
			}
//			stream << decal << decalUT << decalCycle << "\t<topo_entn id=\"" << d->getCurrentElementIndexOfSubLevel(INTERNODE);
			int nsb = ((GeomElemCone *)gb->getElementsGeo().at(i))->getSymbole();

			for (it = symbolList.begin(); it != symbolList.end(); it++)
			{
				Symbol *s = *it;
				if (s->index == ((GeomElemCone *)gb->getElementsGeo().at(i))->getSymbole())
				{
					break;
				}
			}
			if (it != symbolList.end())
			{
				Symbol *s = *it;
				stream << "\" class=\"" << s->name << "_"  << s->index << " \" scale=\""<< INTERNODE +2 << "\">" << endl;
			}
			else
				stream << "\" class=\"symbol_"  << ((GeomElemCone *)gb->getElementsGeo().at(i))->getSymbole() << " \" scale=\""<< INTERNODE +2 << "\">" << endl;

			stream << decal << decalUT << decalCycle << "\t<topoId>" << d->getCurrentElementOfSubLevel(INTERNODE)->getId() << "</topoId>" << endl;
			stream << decal << decalUT << decalCycle << "\t\t<index>" << d->getCurrentElementIndexOfSubLevel(INTERNODE) << "</index>" << endl;
			printAdditionalData (b->getCurrentEntn(), decal+decalUT+decalCycle+"\t");

			for(k=0; k<gb->getElementsGeo().at(i)->getGeomChildren().size(); k++)
			{
				curGeomCone = (GeomBrancCone*)gb->getElementsGeo().at(i)->getGeomChildren().at(k);
				if (curGeomCone->getAxe()->indexOnBearerAtLowestLevel == b->getCurrentEntnIndexWithinBranch())
				{
					int ind;
					if(curGeomCone->getGeomBrancClone())
						ind = curGeomCone->getGeomBrancClone()->getNum();
					else
						ind = curGeomCone->getNum();

					stream << decal << decalUT << decalCycle << "\t\t<clone refto=\"" << ind << ">\"" << endl;

					Matrix4 & matTrans=curGeomCone->getCurrentTransform();

					dssb=matTrans.getNormalVector();
					ds=matTrans.getSecondaryVector();
					dp=matTrans.getMainVector();
					t=matTrans.getTranslation();

			// We create the coordinat matrix
					std::vector<std::vector<float> > tempMat;
					tempMat.resize(3);
			// first line of the matrix
					tempMat[0].resize(4);
					tempMat[0][0] = dp[0];
					tempMat[0][1] = dssb[0];
					tempMat[0][2] = ds[0];
					tempMat[0][3] = t[0];
			// second line of the matrix
					tempMat[1].resize(4);
					tempMat[1][0] = dp[1];
					tempMat[1][1] = dssb[1];
					tempMat[1][2] = ds[1];
					tempMat[1][3] = t[1];
			// third line of the matrix
					tempMat[2].resize(4);
					tempMat[2][0] = dp[2];
					tempMat[2][1] = dssb[2];
					tempMat[2][2] = ds[2];
					tempMat[2][3] = t[2];

					stream << decal << decalUT << decalCycle << "\t\t\t<mat>" << endl;
					stream << decal << decalUT << decalCycle << "\t\t\t\t" << dp[0] << " " << dssb[0] << " " << ds[0] << " " << t[0] << endl;
					stream << decal << decalUT << decalCycle << "\t\t\t\t" << dp[1] << " " << dssb[1] << " " << ds[1] << " " << t[1] << endl;
					stream << decal << decalUT << decalCycle << "\t\t\t\t" << dp[2] << " " << dssb[2] << " " << ds[2] << " " << t[2] << endl;
					stream << decal << decalUT << decalCycle << "\t\t\t</mat>" << endl;
					stream << decal << decalUT << decalCycle << "\t\t</clone>";

				}
			}

			if (u->getElementNumberOfSubLevel(INTERNODE) > 1)
			{
				indexWithinGu = b->getCurrentEntnIndexWithinGu()+1;
				EntnNumberWithinGu = u->getElementNumberOfSubLevel(INTERNODE);
			}
			if (u->getElementNumberOfSubLevel(GROWTHCYCLE) > 1)
			{
				indexWithinCycle = b->getCurrentEntnIndexWithinCycle()+1;
				EntnNumberWithinCycle = e->getElementNumberOfSubLevel(INTERNODE);
			}

			b->positOnNextEntnWithinBranc ();

			if (j < ((GeomElemCone *)gb->getElementsGeo().at(i))->getPosInBranc())
			{
				if (firstEntn == 1)
				{
					stream << decal << decalUT << decalCycle << "\t</decomp>" << endl;
					firstEntn = 2;
				}
				else
				{
					stream << decal << decalUT << decalCycle << "\t</follow>" << endl;
				}
//				stream << decal << decalUT << decalCycle << "\t</topo_entn>" << endl;
				if (   u->getElementNumberOfSubLevel(GROWTHCYCLE) > 1
					&& indexWithinCycle == EntnNumberWithinCycle)
				{
					decalCycle = string ("");
					if (firstCycle == 1)
					{
						stream << decal << decalUT << "\t</decomp>" << endl;
						firstCycle = 2;
					}
					else
					{
						stream << decal << decalUT << "\t</follow>" << endl;
					}
//					stream << decal << decalUT << "\t</topo_Cycle>" << endl;
				}
				if (   u->getElementNumberOfSubLevel(INTERNODE) > 1
					&& indexWithinGu == EntnNumberWithinGu)
				{
					decalUT = string ("");
					if (firstGu == 1)
					{
						stream << decal << "\t</decomp>" << endl;
						firstGu = 2;
					}
					else
					{
						stream << decal << "\t</follow>" << endl;
					}
//					stream << decal << "\t</topo_GU>" << endl;
				}
			}
		}

		printRecord((GeomElemCone *)gb->getElementsGeo().at(i), decal+"\t\t");

		if (firstEntn == 1)
		{
			stream << decal << decalUT << decalCycle << "\t</decomp>" << endl;
			firstEntn = 2;
		}
		else
		{
			stream << decal << decalUT << decalCycle << "\t</follow>" << endl;
		}
//		stream << decal << decalUT << decalCycle << "\t</topo_entn>" << endl;

		if (   u->getElementNumberOfSubLevel(GROWTHCYCLE) > 1
			&& indexWithinCycle == EntnNumberWithinCycle)
		{
			decalCycle = string ("");
			if (firstCycle == 1)
			{
				stream << decal << decalUT << "\t</decomp>" << endl;
				firstCycle = 2;
			}
			else
			{
				stream << decal << decalUT << "\t</follow>" << endl;
			}
//			stream << decal << decalUT << "\t</topo_Cycle>" << endl;
		}
		if (   u->getElementNumberOfSubLevel(INTERNODE) > 1
			&& indexWithinGu == EntnNumberWithinGu)
		{
			decalUT = string ("");
			if (firstGu == 1)
			{
				stream << decal << "\t</decomp>" << endl;
				firstGu = 2;
			}
			else
			{
				stream << decal << "\t</follow>" << endl;
			}
//			stream << decal << "\t</topo_GU>" << endl;
		}

		currentNbElem++;

		currentPortee++;

	}
	b->endPosit();

	return 1;
}

void GldsFormat::printRecordForRoot(GeomElemCone * ge, string decal, bool shape){

Matrix4  __matrix= ge->getMatrix();

	Vector3 dp, dssb ,ds, t;

	dssb=__matrix.getNormalVector();
	ds=__matrix.getSecondaryVector();
	dp=__matrix.getMainVector()*ge->getLength();
	t=__matrix.getTranslation();

	//LIG DATA
	// we start a lig node

    // index=\"" << currentNbElem should be included in decomp and follow.
	stream << decal << "<geometry class=\"Mesh\">" << endl;

	if(shape)
	stream << decal << "\t<shapeIndex>" << ge->getSymbole() << "</shapeIndex>" << endl;

	// We create the coordinat matrix
	std::vector<std::vector<float> > tempMat;
	tempMat.resize(3);
	// first line of the matrix
	tempMat[0].resize(4);
	tempMat[0][0] = dp[0];
	tempMat[0][1] = dssb[0];
	tempMat[0][2] = ds[0];
	tempMat[0][3] = t[0];
	// second line of the matrix
	tempMat[1].resize(4);
	tempMat[1][0] = dp[1];
	tempMat[1][1] = dssb[1];
	tempMat[1][2] = ds[1];
	tempMat[1][3] = t[1];
	// third line of the matrix
	tempMat[2].resize(4);
	tempMat[2][0] = dp[2];
	tempMat[2][1] = dssb[2];
	tempMat[2][2] = ds[2];
	tempMat[2][3] = t[2];


	stream << decal << "\t<mat>" << endl;
	stream << decal << "\t\t" << dp[0] << " " << dssb[0] << " " << ds[0] << " " << t[0] << endl;
	stream << decal << "\t\t" << dp[1] << " " << dssb[1] << " " << ds[1] << " " << t[1] << endl;
	stream << decal << "\t\t" << dp[2] << " " << dssb[2] << " " << ds[2] << " " << t[2] << endl;
	stream << decal << "\t</mat>" << endl;

	stream << decal << "\t<dUp>" << ge->getTopDiam() << "</dUp>" << endl;

	stream << decal << "\t<dDwn>" << ge->getBottomDiam() << "</dDwn>" << endl;

	printAdditionalData (ge, decal);

	stream << decal << "</geometry>" << endl;

}

void GldsFormat::printDataInOrderRec(GeomBranc * gb, string decal){

    Root * r = (Root *) gb->getBranc();
    RootBud * rbud = ((Root *) gb->getBranc())->theBud;




    std::string name_t;
    std::string name_t2;


    std::string result;          // string which will contain the result

    ostringstream convert;   // stream used for the conversion

    convert << (r->Type() + 1);      // insert the textual representation of 'Number' in the characters in the stream

    result = convert.str(); // set 'Result' to the contents of the stream

    name_t = result + "_Type";


    bool follow = false;


    for(int i=0; i<gb->getElementsGeo().size(); i++)
    {

        if(gb->getScale() > 1){
                stream << decal << "<follow class=\""<< "s"<< name_t
                 <<"\" id =\""<< r->getId()+1+i <<"\" scale=\""<< gb->getScale() <<"\">\n";
                follow = true;
        }else{
                gb->setScale(gb->getScale()+2);
                stream << decal << "<decomp class=\""<< "s"<< name_t
                 <<"\" id =\""<< r->getId()+1+i <<"\" scale=\""<< gb->getScale() <<"\">\n";
                follow = false;
        }

        stream << decal+"\t" << "<birthTime>" << r->theBud->birthTime << "</birthTime> " << endl;

        stream << decal+"\t<type>" << r->Type() << "</type> " << endl;

        for(int j = 0 ; j < gb->getElementsGeo().at(i)->getGeomChildren().size() ; j++){

            Root * rr = (Root *) gb->getElementsGeo().at(i)->getGeomChildren().at(j)->getBranc();
            ostringstream convert;
            convert << (rr->Type() + 1);      // insert the textual representation of 'Number' in the characters in the stream

            std::string result = convert.str(); // set 'Result' to the contents of the stream

            name_t2 = result + "_Type";

            stream << decal+"\t" << "<branch class=\""<< name_t2 <<"\" >" << endl;
            printRecordForRoot((GeomElemCone *)gb->getElementsGeo().at(i),decal+"\t",false);
            printDataInOrderRec( gb->getElementsGeo().at(i)->getGeomChildren().at(j) , decal+"\t\t");
            stream  << "</branch> " << endl;
        }

        printRecordForRoot((GeomElemCone *)gb->getElementsGeo().at(i), decal+"\t",true);


        if(follow){
            stream << decal << "</follow>\n";
        }else{
            stream << decal << "</decomp>\n";
        }

    }
}


int GldsFormat::printDataInOrder(GeomBranc * gb, string decal)
{

	int i, j, k;
	string decalUT, decalCycle;

    if ( gb->getBranc()->getClassType() == "DigRMod")
    {

        Root * r = (Root *) gb->getBranc();
        RootBud * rbud = ((Root *) gb->getBranc())->theBud;


        std::string name_t = "1_Type";

        stream << "\t\t<branch class=\""<< name_t
        << "\" scale=\""
        << gb->getScale() + 1 <<"\" id=\""<< r->getId() <<"\">\n";

        stream << "\t\t<Name>"<< name_t <<"</Name>\n";

        //TODO: here should the segment be calculeated

        printAdditionalData (r, decal);
        printAdditionalData (rbud, decal);

        stream << "\t\t<birthTime>" << r->theBud->birthTime << "</birthTime> " << endl;

        stream << "\t\t<type>" << r->Type() << "</type> " << endl;

        printRecordForRoot((GeomElemCone *)gb->getElementsGeo().at(0),"\t\t",false);

        printDataInOrderRec(gb, decal);


        stream << "\t\t</branch>" << endl;

    }
    else
    {
// case of aerial part
        int indexWithinGu, indexWithinCycle, EntnNumberWithinGu, EntnNumberWithinCycle;

        BrancAMAP * braTemp = (BrancAMAP*)gb->getBranc();

        float temp = (braTemp)->theBud->birthTime;
        stream << decal << "<branch id=\"" << getId() << "\" class=\"";
        stream << ((BrancAMAP*)gb->getBranc())->getName() << "\" birthTime=\"";
        stream << temp << " \" scale=\"";
        stream << AXE  +2 << "\">" << endl;

    //	stream << decal << "<topo_axe class=\"" << ((BrancAMAP*)gb->getBranc())->getName() << "\">" << endl;
        stream << decal << "<birthTime>" << ((BrancAMAP*)gb->getBranc())->getBud()->birthTime << "</birthTime> " << endl;
        stream << decal << "<topoId>" << ((BrancAMAP*)gb->getBranc())->getId() << "</topoId> " << endl;
        GrowthUnitAMAP *u;
        CycleAMAP *e;

        printAdditionalData (gb, decal);

        currentPortee=0;

        gb->getBranc()->startPosit();
        gb->getBranc()->positOnFirstElementOfSubLevel(INTERNODE);

        BrancAMAP *b = (BrancAMAP*)gb->getBranc();
        //printAdditionalData (b, axe, decal);
        printAdditionalData (b, decal);
        DecompAxeLevel *d = b;
        bool GUMarkUp=false, cycleMarkUp=false;
        int firstGu=0,firstCycle=0,firstZone=0,firstEntn=0;


        for (j=0; j<b->getCurrentEntnNumberWithinBranch(); j++)
        {


            if (b->getCurrentEntnIndexWithinGu() == 0)
            {
                u = b->getCurrentGu();
                if (u->getElementNumberOfSubLevel(INTERNODE) > 1)
                {
                    decalUT = string ("\t");
                    GUMarkUp = true;
                }
            }
            if (   b->getCurrentEntnIndexWithinCycle() == 0
                && u->getElementNumberOfSubLevel(GROWTHCYCLE) > 1)
            {
                    decalCycle = string ("\t");
                    cycleMarkUp = true;
            }

            b->positOnNextEntnWithinBranc ();
        }

        gb->getBranc()->positOnFirstElementOfSubLevel(INTERNODE);


        for(i=0; i<gb->getElementsGeo().size(); i++)
        {
            int pos = ((GeomElemCone *)gb->getElementsGeo().at(i))->getPosInBranc();
            for (j=b->getCurrentEntnIndexWithinBranch(); j<=pos; j++)
            {
                if (   b->getCurrentEntnIndexWithinGu() == 0
                    && (   GUMarkUp
                        || cycleMarkUp))
                {
                    u = b->getCurrentGu();
                    d = u;

                    if (firstGu == 0)
                    {
    //					stream << decal << "\t<decomp id=\"" << u->getId() << "\" class=\"" << u->getName() << "\">" << endl;
                        stream << decal << "\t<decomp id=\"" << getId() << "\" class=\"" << u->getName() << " \" scale=\""<< GROWTHUNIT +2 << "\">" << endl;
                        firstGu = 1;
                    }
                    else
                    {
    //					stream << decal << "\t<follow id=\"" << u->getId() << "\" class=\"" << u->getName() << "\">" << endl;
                        stream << decal << "\t<follow id=\"" << getId() << "\" class=\"" << u->getName() << " \" scale=\""<< GROWTHUNIT +2 << "\">" << endl;
                        firstGu = 2;
                    }
                    firstCycle = 0;
                    firstZone = 0;
                    firstEntn = 0;
                    stream << decal << "<topoId>" << u->getId() << "</topoId> " << endl;
    //				stream << decal << "\t<topo_GU id=\"" << b->getCurrentGuIndex() << "\" class=\"" << u->getName() << "\">" << endl;

                    stream << decal << "\t\t<phyAge>" << u->phyAgeGu << "</phyAge>" << endl;

                    stream << decal << "\t\t<index>" << b->getCurrentGuIndex() << "</index>" << endl;
                    printAdditionalData (u, decal+"\t");
                }
                if (   b->getCurrentEntnIndexWithinCycle() == 0
                    && cycleMarkUp)
                {
                    e = (CycleAMAP*)b->getCurrentElementOfSubLevel(GROWTHCYCLE);
                    d = e;

                    if (firstCycle == 0)
                    {
    //					stream << decal << decalCycle << "\t<decomp id=\"" << e->getId() << "\" class=\"" << e->getName() << "\">" << endl;
                        stream << decal << decalCycle << "\t<decomp id=\"" << getId() << "\" class=\"" << e->getName() << " \" scale=\""<< GROWTHCYCLE +2 << "\">" << endl;
                        firstCycle = 1;
                    }
                    else
                    {
    //					stream << decal << decalCycle << "\t<follow id=\"" << e->getId() << "\" class=\"" << e->getName() << "\">" << endl;
                        stream << decal << decalCycle << "\t<follow id=\"" << getId() << "\" class=\"" << e->getName() << " \" scale=\""<< GROWTHCYCLE +2 << "\">" << endl;
                        firstCycle = 2;
                    }
                    firstZone = 0;
                    firstEntn = 0;
    //				stream << decal << decalCycle << "\t<topo_Cycle id=\"" << b->getCurrentCycleIndexWithinGu() << "\" class=\"" << e->getName() << "\">" << endl;

                    stream << decal << decalUT << "<topoId>" << e->getId() << "</topoId> " << endl;
                    stream << decal << decalUT << "\t<index>" << b->getCurrentCycleIndexWithinGu() << "</index>" << endl;
                    printAdditionalData (e, decal+decalUT+"\t");
                }

                if (firstEntn == 0)
                {
    //				stream << decal << decalUT << decalCycle << "\t<decomp id=\"" << d->getCurrentElementOfSubLevel(INTERNODE)->getId();
                    stream << decal << decalUT << decalCycle << "\t<decomp id=\"" << getId();
                    firstEntn = 1;
                }
                else
                {
    //				stream << decal << decalUT << decalCycle << "\t<follow id=\"" << d->getCurrentElementOfSubLevel(INTERNODE)->getId();
                    stream << decal << decalUT << decalCycle << "\t<follow id=\"" << getId();
                    firstEntn = 2;
                }
    //			stream << decal << decalUT << decalCycle << "\t<topo_entn id=\"" << d->getCurrentElementIndexOfSubLevel(INTERNODE);
    //			stream << "\" class=\"symbol_" << ((GeomElemCone *)gb->getElementsGeo().at(i))->getSymbole() << "\">" << endl;
                int nsb = ((GeomElemCone *)gb->getElementsGeo().at(i))->getSymbole();
                for (it = symbolList.begin(); it != symbolList.end(); it++)
                {
                    Symbol *s = *it;
                    if (s->index == ((GeomElemCone *)gb->getElementsGeo().at(i))->getSymbole())
                    {
                        break;
                    }
                }
                if (it != symbolList.end())
                {
                    Symbol *s = *it;
                    stream << "\" class=\"" << s->name << "_"  << s->index << " \" scale=\""<< INTERNODE +2 << "\">" << endl;
                }
                else
                    stream << "\" class=\"symbol_"  << ((GeomElemCone *)gb->getElementsGeo().at(i))->getSymbole() << " \" scale=\""<< INTERNODE +2 << "\">" << endl;

                stream << decal << decalUT << decalCycle << "<topoId>" << d->getCurrentElementOfSubLevel(INTERNODE)->getId() << "</topoId> " << endl;
                stream << decal << decalUT << decalCycle << "\t\t<index>" << d->getCurrentElementIndexOfSubLevel(INTERNODE) << "</index>" << endl;
                printAdditionalData (b->getCurrentEntn(), decal+decalUT+decalCycle+"\t");

                for(k=0; k<gb->getElementsGeo().at(i)->getGeomChildren().size(); k++)
                {
                    GeomBranc *gbp = gb->getElementsGeo().at(i)->getGeomChildren().at(k);
                    if (gbp->getAxe()->indexOnBearerAtLowestLevel == b->getCurrentEntnIndexWithinBranch())
                        printDataInOrder(gbp, decal+decalUT+decalCycle+"\t");
                }

                if (   cycleMarkUp
                    || GUMarkUp)
                {
                    indexWithinGu = b->getCurrentEntnIndexWithinGu()+1;
                    EntnNumberWithinGu = u->getElementNumberOfSubLevel(INTERNODE);
                }
                if (cycleMarkUp)
                {
                    indexWithinCycle = b->getCurrentEntnIndexWithinCycle()+1;
                    EntnNumberWithinCycle = e->getElementNumberOfSubLevel(INTERNODE);
                }

                if (j < pos)
                {
                    if (firstEntn == 1)
                    {
                        stream << decal << decalUT << decalCycle << "\t</decomp>" << endl;
                        firstEntn = 2;
                    }
                    else
                    {
                        stream << decal << decalUT << decalCycle << "\t</follow>" << endl;
                    }
    //				stream << decal << decalUT << decalCycle << "\t</topo_entn>" << endl;
                    if (   cycleMarkUp
                        && indexWithinCycle == EntnNumberWithinCycle)
                    {
                        if (firstCycle == 1)
                        {
                            stream << decal << decalUT << "\t</decomp>" << endl;
                            firstCycle = 2;
                        }
                        else
                        {
                            stream << decal << decalUT << "\t</follow>" << endl;
                        }
    //					stream << decal << decalUT << "\t</topo_Cycle>" << endl;
                    }
                    if (  (   GUMarkUp
                           || cycleMarkUp)
                        && indexWithinGu == EntnNumberWithinGu)
                    {
                        if (firstGu == 1)
                        {
                            stream << decal << "\t</decomp>" << endl;
                            firstGu = 2;
                        }
                        else
                        {
                            stream << decal << "\t</follow>" << endl;
                        }
    //					stream << decal << "\t</topo_GU>" << endl;
                    }
                }

                b->positOnNextEntnWithinBranc ();

            }

            printRecord((GeomElemCone *)gb->getElementsGeo().at(i), decal+decalUT+decalCycle+"\t");

            if (firstEntn == 1)
            {
                stream << decal << decalUT << decalCycle << "\t</decomp>" << endl;
                firstEntn = 2;
            }
            else
            {
                stream << decal << decalUT << decalCycle << "\t</follow>" << endl;
            }
    //		stream << decal << decalUT << decalCycle << "\t</topo_entn>" << endl;
            if (   cycleMarkUp
                && indexWithinCycle == EntnNumberWithinCycle)
            {
                if (firstCycle == 1)
                {
                    stream << decal << decalUT << "\t</decomp>" << endl;
                    firstCycle = 2;
                }
                else
                {
                    stream << decal << decalUT << "\t</follow>" << endl;
                }
    //			stream << decal << decalUT << "\t</topo_Cycle>" << endl;
            }
            if (   (   GUMarkUp
                    || cycleMarkUp)
                && indexWithinGu == EntnNumberWithinGu)
            {
                if (firstGu == 1)
                {
                    stream << decal << "\t</decomp>" << endl;
                    firstGu = 2;
                }
                else
                {
                    stream << decal << "\t</follow>" << endl;
                }
    //			stream << decal << "\t</topo_GU>" << endl;
            }

            currentNbElem++;

            currentPortee++;

        }

        gb->getBranc()->endPosit();

        stream << decal << "</branch>" << endl;
    }
//	stream << decal << "</topo_axe>" << endl;
	return 1;


}

bool GldsFormat::printHeader (const std::string &headerf)
{

	return true;

}


void GldsFormat::printRecord(GeomElemCone * ge, string decal)
{

	Matrix4  __matrix= ge->getMatrix();

	Vector3 dp, dssb ,ds, t;

	dssb=__matrix.getNormalVector();
	ds=__matrix.getSecondaryVector();
	dp=__matrix.getMainVector()*ge->getLength();
	t=__matrix.getTranslation();

	//LIG DATA
	// we start a lig node


	stream << decal << "<geometry class=\"Mesh\" index=\"" << currentNbElem << "\">" << endl;

	stream << decal << "\t<shapeIndex>" << ge->getSymbole() << "</shapeIndex>" << endl;

	// We create the coordinat matrix
	std::vector<std::vector<float> > tempMat;
	tempMat.resize(3);
	// first line of the matrix
	tempMat[0].resize(4);
	tempMat[0][0] = dp[0];
	tempMat[0][1] = dssb[0];
	tempMat[0][2] = ds[0];
	tempMat[0][3] = t[0];
	// second line of the matrix
	tempMat[1].resize(4);
	tempMat[1][0] = dp[1];
	tempMat[1][1] = dssb[1];
	tempMat[1][2] = ds[1];
	tempMat[1][3] = t[1];
	// third line of the matrix
	tempMat[2].resize(4);
	tempMat[2][0] = dp[2];
	tempMat[2][1] = dssb[2];
	tempMat[2][2] = ds[2];
	tempMat[2][3] = t[2];


	stream << decal << "\t<mat>" << endl;
	stream << decal << "\t\t" << dp[0] << " " << dssb[0] << " " << ds[0] << " " << t[0] << endl;
	stream << decal << "\t\t" << dp[1] << " " << dssb[1] << " " << ds[1] << " " << t[1] << endl;
	stream << decal << "\t\t" << dp[2] << " " << dssb[2] << " " << ds[2] << " " << t[2] << endl;
	stream << decal << "\t</mat>" << endl;

	stream << decal << "\t<dUp>" << ge->getTopDiam() << "</dUp>" << endl;

	stream << decal << "\t<dDwn>" << ge->getBottomDiam() << "</dDwn>" << endl;

	printAdditionalData (ge, decal);

	stream << decal << "</geometry>" << endl;
}



