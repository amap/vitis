/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _GLDSFORMAT_H
#define _GLDSFORMAT_H
#include <vector>
#include "bfstream.h"
#include "OutputFormat.h"
#include "externOutputOPF.h"
//#include "libglds.h"
//using namespace glds;
using namespace std;

class OUTPUTOPF_EXPORT GldsFormat : public OutputFormat
{

	ofstream stream;
	std::string baseName;

//	Document doc;

//	Node currentNode;

	/// \brief Current "port�e"
	int currentPortee;

	/// \brief Number of elements already written
	int currentNbElem;

	struct Symbol {int index; string name;} ;
	vector <Symbol*> symbolList;
	vector <Symbol*>::iterator it;

	int geomId;

	public:

	GldsFormat(const std::string &finame);

	virtual ~GldsFormat(void);

	virtual int initFormat(VitisObject * vobj);

	virtual int printData(VitisObject *);

	//int printAxe(class GeomBranc * gb, Node & curNod, string decal);
	//void printAdditionalData (VitisObject * o, Node & curNod, string decal);

	//int printDataInOrder(class GeomBranc * vobj, Node & curNod, string decal);

	int printAxe(class GeomBranc * gb, string decal);
	void printAdditionalData (VitisObject * o, string decal);

	int printDataInOrder(class GeomBranc * vobj, string decal);
	void printDataInOrderRec(GeomBranc * gb, string decal);

	virtual bool printHeader (const std::string &headerf);

//	virtual void printRecord(class GeomElemCone * ge, Node & ligcur, string decal);
	virtual void printRecord(class GeomElemCone * ge, string decal);

    virtual void printRecordForRoot(GeomElemCone * ge, string decal, bool shape);

	void printSubStruct(class GeomManager & gtr);

	void closeFormat();

	int getId(){
		return geomId++;
	}
};

#endif

