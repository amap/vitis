/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include "OutputGeom.h"
#include "Plant.h"
#include "GldsFormat.h"
#include "DebugNew.h"



//Noter que DP = Z
//DS = X
//ECHEL [0] = echel pour dp -> Z


OutputGeom::OutputGeom():Output()
{
}

OutputGeom::~OutputGeom(void)
{
}



void OutputGeom::freeData ()
{
	v_plt->getGeometry().clear();
}

void OutputGeom::computeDataToOutput ()
{
	v_plt->computeGeometry();
}

void OutputGeom::writeData (const std::string &finame)
{
	int i;

	std::stringstream sout;
	sout << finame;
	sout<< "_"<<v_plt->getConfigData().randSeed;
	std::string fname (sout.str());

	outFormat = new GldsFormat(fname);

	outFormat->initFormat(&(v_plt->getGeometry()));

	std::cout<<"Output Geometry opf to : "  << fname << ".opf" << std::endl;

	if (v_plt->getGeometry().getGeomBrancStack().size())
	{
		outFormat->printData(v_plt->getGeometry().getGeomBrancStack().at(0));
	}
	else
	{
		std::cout<<"WARNING : -------> Empty Geometry"<<std::endl;
	}

	((GldsFormat*)outFormat)->closeFormat();
	std::cout<<"end Output"<< std::endl;

	delete outFormat;
}




#if !defined STATIC_LIB
extern "C" Output * StartPlugin ()
{
	return new OutputGeom();
}
#endif


