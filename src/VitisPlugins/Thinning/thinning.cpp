/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WINDOWS
#include <conio.h>
#endif
#include <iostream>
#include <iomanip>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "VitisCoreSignalInterface.h"
#include "Plant.h"
#include "PlotManager.h"
#include "externThinning.h"
#include "thinning.h"



/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
thinningPlugin::thinningPlugin(const std::string& f, void *pp)
{
    if (initData(f))
    {
        event = sylviculture.begin();
        thinningEvent e = *event;
 		Scheduler::instance()->create_process(this,NULL,e.date,105);
    }

}

thinningPlugin::~thinningPlugin()
{

}

bool thinningPlugin::initData(const std::string& f)
{

	cout <<"initialisation thinning "<<f<<endl;

	ifstream fic (f.c_str());
	thinningEvent e, ei;
    vector<thinningEvent>::iterator it;
    char line[256];

	if (!fic)
	{
		perror (f.c_str());
		return false;
	}

    while (fic.getline(line, 255))
    {
        try
        {

            if (line[0] == '#')
                continue;
            if (sscanf (line, "%f %f", &e.date, &e.rate) < 2)
                continue;

            for (it = sylviculture.begin(); it != sylviculture.end(); it++)
            {
                ei = *it;
                if (ei.date > e.date)
                {
                    break;
                }
            }
            sylviculture.insert(it, e);
        }
        catch (std::exception)
        {
            cout << "Error while reading thinning file "<< f.c_str() << endl;
            fic.close();
            return false;
        };
        if (!fic)
            break;
    }

	fic.close();

	return true;
}


void thinningPlugin::process_event(EventData *)
{

	Scheduler *s = Scheduler::instance();
	float top_extern = s->getTopClock();
	float nextdate;

cout << "thinning at time " << top_extern << endl;

    thinning();

	if (event != sylviculture.end())
	{
        event++;
        thinningEvent e = *event;
        if (e.date <= s->getHotStop())
            s->signal_event(this->getPid(),NULL,e.date,105);
	}
}

void thinningPlugin::thinning()
{
    thinningEvent e = *event;

    // compute number of plants to be thinned
    PlotManager *pm = PlotManager::instance();
    vector<Plant*> plants = pm->getPlants();
    Plant *p;
    GeomBrancCone *b;
    GeomElemCone *g;
    float lng, dbhmin;
    float *dbh = new float[plants.size()];
    float *db = new float[plants.size()];
    int i=0, idbmin;
    int nbUnder130 = 0;

    int nbToThin = plants.size() * e.rate / 100;
    if (nbToThin > plants.size())
        nbToThin = plants.size();


    // compute plants dbh and base diameter
    i = 0;
    for (vector<Plant *>::iterator it=plants.begin(); it!=plants.end(); it++)
    {
        p = *it;
        if (p->getGeometry().getGeomBrancStack().size() == 0)
            p->computeGeometry(true);

        b = (GeomBrancCone*)p->getGeometry().getGeomBrancStack().at(0);
        b->computeBranc(true);
        vector<GeomElem *> ge = b->getElementsGeo ();
        lng = 0;
        dbh[i] = 1e34;
        db[i] = 1e34;
        for (vector<GeomElem*>::iterator itg=ge.begin(); itg!=ge.end(); itg++)
        {
            g = (GeomElemCone*)*itg;
            if (db[i] >1e33)
                db[i] = g->getBottomDiam();

            lng += g->getLength();
            if (lng >= 130)
            {
                dbh[i] = g->getTopDiam() + (g->getBottomDiam()-g->getTopDiam()) * (lng-130) / g->getLength();
                break;
            }
        }
        if (dbh[i] > 1e33)
        {
            nbUnder130 ++;
        }
        else
        {
            db[i] = 1e34;
        }
        i++;
     }

    // choose the plants to be thinned accordind to diameter and distance
    for (int nb=0; nb<nbToThin; nb++)
    {
        if (nbUnder130 > 0)
        {
            dbhmin = 1e34;
            idbmin = -1;
            for (i=0; i<plants.size(); i++)
            {
                if (db[i] < dbhmin)
                {
                    dbhmin = db[i];
                    idbmin = i;
                }
            }
            db[idbmin] = 1e34;
            nbUnder130 --;
        }
        else
        {
            dbhmin = 1e34;
            idbmin = -1;
            for (i=0; i<plants.size(); i++)
            {
                if (dbh[i] < dbhmin)
                {
                    dbhmin = dbh[i];
                    idbmin = i;
                }
            }
            dbh[idbmin] = 1e34;
        }
// remove selected plant
cout << "remove plant " <<plants.at(idbmin)->getName() <<" at pos " << plants.at(idbmin)->getPosition().x() << " " << plants.at(idbmin)->getPosition().y() << " with diameter " << dbhmin << endl;
        delete plants.at(idbmin);
    }


    delete dbh;

}


extern "C" THINNING_EXPORT  thinningPlugin * StartPlugin (const std::string& f, void *p)
{
	std::cout << "loading thinningPlugin module with parameter " << f << std::endl;

	return new thinningPlugin (f, p) ;
}





