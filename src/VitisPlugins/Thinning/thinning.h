/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "scheduler.h"
#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "externThinning.h"

#include <vector>


struct thinningEvent
{
    float date;
	float rate;
};

class THINNING_EXPORT thinningPlugin : public VProcess
{
public:
	thinningPlugin(const std::string& f, void *p);
	virtual ~thinningPlugin();
	void process_event(EventData *);

	bool initData(const std::string& f);
	void thinning();

protected:
	vector<thinningEvent> sylviculture;
	vector<thinningEvent>::iterator event;
};

