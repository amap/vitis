/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//#include <windows.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "Bud.h"
#include "BrancAMAP.h"
#include "GeomBrancAMAP.h"
#include "externPipemodel.h"
#include "pipemodel.h"
#include "ParamFile.h"

void Lengthzz::on_notify (const state& , paramMsg * data)
{
	paramElemGeom * p = (paramElemGeom*)data;

	int level = p->gec->getPtrBrc()->getBranc()->getPlant()->getPlantFactory()->getLevelOfDecomposition(p->gec->getPtrBrc()->getBranc()->getClassType());
	DecompAxeLevel *d=p->gec->getPtrBrc()->getBranc()->getCurrentElementOfSubLevel(level);
	if (d)
	{
		float *v = (float*)d->getAdditionalData("length");
		if (v && *v==0)
		{
			*v = (float)p->v;
			d->modifyAdditionalData("length", v);
		}
	}
}
void Diamzz::on_notify (const state& , paramMsg * data)
{
	paramElemGeom * p = (paramElemGeom*)data;

	int level = p->gec->getPtrBrc()->getBranc()->getPlant()->getPlantFactory()->getLevelOfDecomposition(p->gec->getPtrBrc()->getBranc()->getClassType());
	DecompAxeLevel *d=p->gec->getPtrBrc()->getBranc()->getCurrentElementOfSubLevel(level);
	float *v, *l, *f;

	if (d)
	{
		v = (float*)d->getAdditionalData("volume");
		if (v)
		{
			l = (float*)d->getAdditionalData("length");

//			float test = sqrt (4 * *v / M_PI / *l);
//			float ss = M_PI * test * test / 4.;
            if (*l > 0)
                p->v = sqrt (4 * *v / M_PI / *l);
			f = (float*)d->getAdditionalData ("fc");
            if (f != NULL)
            {
                p->v *= *f;
                d->removeAdditionalData("fc");
            }
		}
	}
}
Geom::Geom (void *p)
{
	prevTop = 0;
	subscribe(p);
	effectiveRingSlope = 0;
}
Geom::~Geom ()
{
	unsubscribe();
}

bool Geom::init (void *p)
{
	cout <<"PipeModel initialisation with default values"<<endl;
	effectiveRingSlope = 1;
	minEffectiveRingNumber = 0;
	corrDiff = 1;
	optionBiomass = true;

	return true;
}

bool Geom::init (const std::string& f, void *p)
{
	cout <<"initialisation pipeModel "<<f<<endl;
	optionBiomass = true;

	ParamSet * param_H = new ParamSet(f);
	if (!param_H->Exists())
	{
        perror(f.c_str());
		return false;
    }
	if (param_H->getParameter("optionBiomass"))
		optionBiomass = param_H->getParameter("optionBiomass")->value();

	if (param_H->getParameter("forkConductiveCoeff"))
		forkConductiveCoeff = param_H->getParameter("forkConductiveCoeff")->value();
	else
	{
		cout << "Failed finding forkConductiveCoeff parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("LeafConductiveCoeff"))
		corrDiff = param_H->getParameter("LeafConductiveCoeff")->value();
	else
	{
		cout << "Failed finding LeafConductiveCoeff parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("minEffectiveRingNumber"))
		minEffectiveRingNumber = param_H->getParameter("minEffectiveRingNumber")->value();
	else
	{
		cout << "Failed finding minEffectiveRingNumber parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("effectiveRingSlope"))
		effectiveRingSlope = param_H->getParameter("effectiveRingSlope")->value();
	else
	{
		cout << "Failed finding effectiveRingSlope parameter in file " << f << endl;
		return false;
	}
cout << "optionBiomass " <<optionBiomass << " forkConductiveCoeff "<< forkConductiveCoeff << " leafConductiveCoeff "<< corrDiff<<" minEffectiveRingNumber "<<minEffectiveRingNumber<<" effctiveRingSlope "<<effectiveRingSlope <<endl;

    plant = (Plant*)p;

    return true;

}

void Geom::on_notify (const state& , paramMsg * data)
{
	paramGeomCompute * p = (paramGeomCompute*)data;

	if (prevTop == Scheduler::instance()->getTopClock())
		return;

    float *f;
    if ((f=(float*)plant->getAdditionalData ("addedWoodVolume")) == NULL)
    {
        f = new float;
        plant->addAdditionalData("addedWoodVolume", f);
    }
//cout << "begin compute"<<endl;
    *f = 0;

	float length, initialLength, initialDiameter, w=0, prod;
	BrancAMAP *b, *bearer;
	std::vector<GeomBranc*> gbs = p->gm->getGeomBrancStack();
	vector<Hierarc *> *vh;
	Hierarc *h;

// reset shoot leaf surface and upper conductive surface
    float *v, *s, *us, volume;
	b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::RESET);
	int level = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	while (b)
	{
		if (b->nature == SimpleImmediateOrgan)
		{
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}

		if ((v = (float*)b->getAdditionalData("leaf surface")) == NULL)
		{
		    v = new float;
		    b->addAdditionalData("leaf surface", v);
		}

		*v = 0;
// add default initial surface
        b->computeInitialLengthDiameter (&initialLength, &initialDiameter);
        *v += (initialDiameter / 2) * (initialDiameter / 2) * M_PI;

		b->startPosit();
		if(b->positOnLastElementOfSubLevel(level))
		{
            do
            {
                InterNodeAMAP *entn = b->getCurrentEntn();

                if ((s = (float*)entn->getAdditionalData("us")) != NULL)
                    *s = 0;

            }while(b->positOnPreviousElementOfSubLevel(level));
		}
		b->endPosit();

        b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
 	}

// summarize shoot leaf surface
	b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::RESET);
	while (b)
	{
		if (b->nature != SimpleImmediateOrgan)
		{
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}
		prod = computeProduction(b);

		// in case of topological simplification 1, there may be more than one instance of this leaf
		vh = b->HierarcPtrsByAddr();
		for (vector<Hierarc *>::iterator it=vh->begin(); it<vh->end(); it++)
		{
		    h = *it;
            bearer = (BrancAMAP*)h->getBearer()->getBranc();
			v = (float*)bearer->getAdditionalData("leaf surface");
			*v += prod;
		}

       b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
	}
// compute internode length and update conductive surface
	b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::RESET);
	while (b)
	{
		if (b->nature == SimpleImmediateOrgan)
		{
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}

		b->startPosit();
		if(!b->positOnLastElementOfSubLevel(level))
		{
			b->endPosit();
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}

		do
		{
			AxisReference *axisref = b->getBud()->axisref;
			int testNumb=b->getTestNumberWithinGu();

			float instant = (float)b->getBud()->endInstant;

			int nbtest = b->getTestNumberWithinBranch();

			while (instant < Scheduler::instance()->getTopClock() - EPSILON)
			{
				nbtest = nbtest +(int)testNumb;
				instant += (float)(1. / b->getBud()->getGrowth()->rythme / axisref->val1DParameter (rtrythme,b->getBud()->getPhyAge(), b));
			}
			b->computeInitialLengthDiameter (&initialLength, &initialDiameter);

			length = b->computeLength (initialLength, nbtest);
			InterNodeAMAP *entn = b->getCurrentEntn();
			float *v = (float*)entn->getAdditionalData("length");
			if (v && *v==0)
				*v = length;
			entn->modifyAdditionalData("length", v);
			int *n = (int*)entn->getAdditionalData ("ring number");
			vector<float> *rv = (vector<float> *)entn->getAdditionalData ("ring surface");

            //compute effective ring number according to age of the shoot
            int nbEffectiveRings;
            if (*n < minEffectiveRingNumber)
                nbEffectiveRings = *n;
            else
                nbEffectiveRings = *n - (*n - minEffectiveRingNumber) * effectiveRingSlope - 1;

            // adjust remaining conductive surface
            if ((s = (float*)entn->getAdditionalData("cs")) == NULL)
            {
                s = new float;
                entn->addAdditionalData ("cs", s);
            }

            *s= 0;
            for (int i=0; i<nbEffectiveRings; i++)
            {
                *s += rv->at(*n-i-1);
            }

            // update upper conductive surface on lower entn
            InterNodeAMAP *previous = NULL;
            b->startPosit();
            if (b->positOnPreviousElementOfSubLevel(level))
            {
                 previous = b->getCurrentEntn();
            }
            else if (b != b->getBearer())
            {
                BrancAMAP *bearer = b->getBearer();
                bearer->startPosit();
                bearer->positOnElementAtSubLevel (b->getCurrentHierarc()->indexOnBearerAtLowestLevel, level);
                previous = bearer->getCurrentEntn();
                bearer->endPosit();
            }
            b->endPosit();

            if (   previous
                && (us = (float*)previous->getAdditionalData("us")) != NULL)
                *us += *s;

 			*n += 1;
			rv->push_back (0);

			entn->modifyAdditionalData ("ring number", n);
			entn->modifyAdditionalData ("ring surface", rv);

		} while(b->positOnPreviousElementOfSubLevel(level));

		b->endPosit();
		b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
	}

// compute diffusion
	b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::RESET);
	while (b)
	{
		if (b->nature == SimpleImmediateOrgan)
		{
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}

		b->startPosit();

		if(!b->positOnFirstElementOfSubLevel(level))
		{
			b->endPosit();
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}

		diffusion (b);

		b->endPosit();

		b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
	}

// compute wood volume
	volume=0;
    DecompAxeLevel *entn;
	b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::RESET);
	while (b)
	{
		if (b->nature == SimpleImmediateOrgan)
		{
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}

		b->startPosit();

		if(!b->positOnFirstElementOfSubLevel(level))
		{
			b->endPosit();
			b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
			continue;
		}

        if(b->positOnFirstElementOfSubLevel(level))
        {
            do
            {
                entn = b->getCurrentElementOfSubLevel(level);

                float *v = (float*)entn->getAdditionalData("volume");
                if (v)
                {
                    volume += *v;
                }
             } while (b->positOnNextElementOfSubLevel(level));
        }

		b->endPosit();

		b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::NEXT);
	}

	float lng = 0, *l, dbh=0;
	b = (BrancAMAP*)p->gm->getTopoManager().seekTree(TopoManager::RESET);
	b->startPosit();
	if(b->positOnFirstElementOfSubLevel(level))
	{
		do
		{
			entn = b->getCurrentElementOfSubLevel(level);

			if ((l = (float*)entn->getAdditionalData("length")) != NULL)
				lng += *l;
			if (   lng >= 130
			    && dbh == 0)
			{
				float *v = (float*)entn->getAdditionalData("volume");
				if (v)
				{
					dbh = sqrt (4 * *v / M_PI / *l);
                    float* f = (float*)entn->getAdditionalData ("fc");
                    if (f != NULL)
                    {
                        dbh *= *f;
                    }
				}
			}
		} while (b->positOnNextElementOfSubLevel(level));
	}
	b->endPosit();

    size_t pos = b->getPlant()->getName().rfind("/");
    string name = b->getPlant()->getName().substr(pos+1);
	cout << "height = "<<lng<< " dbh = "<<dbh<< " woodvolume = "<<volume/1000000<<" plant = "<<name<<" age = "<<(float)Scheduler::instance()->getTopClock()<<endl;

	prevTop = (float)Scheduler::instance()->getTopClock();
}
void Geom::diffusion (GeomBrancCone *)
{
}

float Geom::computeProduction (BrancAMAP *br)
{
    if (br->getCurrentEntnNumberWithinBranch() == 0)
        return 0;

	AxisReference *axisref = br->getBud()->axisref;
	int testNumb=br->getTestNumberWithinGu();

	float instant = (float)br->getBud()->endInstant;

	int nbtest = br->getTestNumberWithinBranch();

	while (instant < Scheduler::instance()->getTopClock() - EPSILON)
	{
		nbtest = nbtest + (int)testNumb;
		instant += (float)(1. / br->getBud()->getGrowth()->rythme / axisref->val1DParameter (rtrythme,br->getBud()->getPhyAge(), br));
	}

    float initialLength, initialDiameter;
	br->computeInitialLengthDiameter (&initialLength, &initialDiameter);
	float length = br->computeLength (initialLength, nbtest);
	float diameter = br->computeDiam (initialDiameter, nbtest);

	float qtt = (float)(corrDiff * length * diameter * (Scheduler::instance()->getTopClock()- prevTop));

    return qtt;
}

void Geom::diffusion (BrancAMAP *br)
{
	float lng = 0, *l, *cs, *us, incrementOnUpperSurf;
	float length, diameter, initialLength, initialDiameter;
	Hierarc *hBearer = br->getCurrentHierarc();
	BrancAMAP *b = br;
	int level = b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType());
	DecompAxeLevel *entn, *nentn;
	bool begin = true;
	int posit;

    float pipeSurf = 0;
    if ((l=(float*)br->getAdditionalData("leaf surface")) != NULL)
        pipeSurf = *l;

    if (optionBiomass)
    {
// compute length from end branch to trunk base
        do
        {

            b = (BrancAMAP*)hBearer->getBranc();
            b->startPosit();

            if (begin)
            {
                begin = false;
                b->positOnLastElementOfSubLevel(level);
            }
            else
            {
                b->positOnElementAtSubLevel (posit, level);
            }

            do
            {
                entn = b->getCurrentElementOfSubLevel(level);
                if ((l = (float*)entn->getAdditionalData("length")) != NULL)
                    lng += *l;
            }
            while (b->positOnPreviousElementOfSubLevel(level));

            posit = hBearer->indexOnBearerAtLowestLevel;
            b->endPosit();

            if (hBearer == hBearer->getBearer())
                break;

            hBearer = hBearer->getBearer();
        }
        while (1);

        if (lng == 0)
            return;

// adjust pipe surface according to path length
        pipeSurf /= lng;
    }

	hBearer = br->getCurrentHierarc();
	begin = true;
	incrementOnUpperSurf = 0;

    bool sameBranch = true;
	do
	{

		b = (BrancAMAP*)hBearer->getBranc();
		b->startPosit();

		if (begin)
		{
			begin = false;
			b->positOnLastElementOfSubLevel(level);
		}
		else
		{
			b->positOnElementAtSubLevel (posit, level);
		}

		do
		{
			entn = b->getCurrentElementOfSubLevel(level);

            // if entn bears a branch -> apply conductive coef
            if (   forkConductiveCoeff != 1
				&& !sameBranch)

            {
                if (entn->getAdditionalData ("fc") == NULL)
                {
                    float *f = new float;
                    *f = forkConductiveCoeff;
                    entn->addAdditionalData("fc", f);
                }
            }

            // update data to store upper conductive surface on the next entn
            if ((us = (float*)entn->getAdditionalData("us")) == NULL)
            {
                us = new float;
                entn->addAdditionalData("us", us);
                *us= 0;
            }
            *us += incrementOnUpperSurf;
            float upperSurf = *us;

			float length;
			if ((l = (float*)entn->getAdditionalData("length")) != NULL)
				length = *l;
            else
                continue;

			int *n = (int*)entn->getAdditionalData ("ring number");
			if ((cs=(float*)entn->getAdditionalData ("cs")) == NULL)
			{
                cs = new float;
                entn->addAdditionalData("cs", cs);
			    *cs = 0;
			}
			float conductiveSurf = *cs;

// compute conservative surface along the path
            if (conductiveSurf == 0)
                incrementOnUpperSurf = pipeSurf;
            else if (conductiveSurf < upperSurf)
            {
                if (conductiveSurf + incrementOnUpperSurf > upperSurf)
                    incrementOnUpperSurf = upperSurf - conductiveSurf;
            }
            else
                incrementOnUpperSurf = 0;

            *cs += incrementOnUpperSurf;


			vector<float> *rv = (vector<float> *)entn->getAdditionalData ("ring surface");

			float w = incrementOnUpperSurf * length;

            float *f = (float*)plant->getAdditionalData("addedWoodVolume");
            *f += w;
			float *v = (float*)entn->getAdditionalData ("volume");
			*v += w;
if (*v == 0)
cout << "branc " << b->getId() << " entn "<< b->getCurrentEntnIndexWithinBranch() <<" upper surf "<< *us << " increment "<< incrementOnUpperSurf<<" volume "<<*v<<" lng "<<lng<<endl;
			(*rv)[*n - 1] += incrementOnUpperSurf;
			entn->modifyAdditionalData ("volume", v);
			entn->modifyAdditionalData ("ring surface", rv);
		}
		while (b->positOnPreviousElementOfSubLevel(level));

		posit = hBearer->indexOnBearerAtLowestLevel;
		b->endPosit();

        if (hBearer == hBearer->getBearer())
            break;

		hBearer = hBearer->getBearer();
//		currentForkConductiveCoeff *= forkConductiveCoeff;
//		incrementOnUpperSurf *= forkConductiveCoeff;
		sameBranch = false;
	}
	while (1);

}

bool Geom::hasGreenBranch (BrancAMAP *b)
{
    BrancAMAP *borne;
    Hierarc *h = b->CurrentHierarcPtr();
    int pos = b->getCurrentEntnIndexWithinBranch();
    for (int i=0; i<h->getNbBorne(); i++)
    {
        borne = (BrancAMAP*)h->getBorne(i);
        int ppos = borne->getCurrentHierarc()->indexOnBearerAtLowestLevel;
        int nb = borne->getCurrentEntnNumberWithinBranch();
        if (   borne->nature != SimpleImmediateOrgan
            && ppos == pos
            && nb > 0
            && hasLeaves (borne))
            return true;
    }

    return false;
}

bool Geom::hasLeaves (BrancAMAP *b)
{
    BrancAMAP *borne;
    Hierarc *h = b->CurrentHierarcPtr();
    for (int i=0; i<h->getNbBorne(); i++)
    {
        borne = (BrancAMAP*)h->getBorne(i);
        if (borne->nature == SimpleImmediateOrgan)
            return true;
    }

    return false;
}

void Decomp::on_notify (const state& , paramMsg * data)
{
	paramDecompAxeLevel *p = (paramDecompAxeLevel *)data;

	int level = p->d->getPlant()->getPlantFactory()->getLevelOfDecomposition(((BrancAMAP *)p->d->getParentOfLevel(0))->getClassType());

	if (p->d->getLevel()== level)
	{
		BrancAMAP *b = (BrancAMAP *)p->d->getParentOfLevel(0);
		if (b)
		{
			if (b->nature != SimpleImmediateOrgan)
			{
				int *i = new int(0);
				float *l = new float(0);
				float *d = new float(0);
				p->d->addAdditionalData ("ring number", i);
				if (p->d->getAdditionalData ("length") == 0)
					p->d->addAdditionalData ("length", l);
				p->d->addAdditionalData ("volume", d);
				p->d->addAdditionalData ("ring surface", new vector<float>);
				if ((l=(float*)p->d->getAdditionalData ("us")) != 0)
                    *l = 0;
				if ((l=(float*)p->d->getAdditionalData ("cs")) != 0)
                    *l = 0;
			}
		}
	}
};



PipeModel::PipeModel(void *p)
{
	diam = new Diamzz (p);
	d = new Decomp (p);
	geom = new Geom (p);
	geom->init(p);
}
PipeModel::PipeModel(const std::string& f, void *p)
{
	geom = new Geom (p);
	if (!geom->init(f, p))
	{
        cout << "ERROR initializing pipe model plugin" << endl;
        return;
	}
	diam = new Diamzz (p);
	d = new Decomp (p);
}

