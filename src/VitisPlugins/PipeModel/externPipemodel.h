#ifndef __EXPORTPIPE_H__
#define __EXPORTPIPE_H__


#pragma warning ( disable : 4251 )

#if !defined STATIC_LIB
	#if defined WIN32

			#ifdef PIPE_EXPORTS
				#define PIPE_EXPORT __declspec(dllexport)
			#else
				#define PIPE_EXPORT __declspec(dllimport)
			#endif

	#else
		#define PIPE_EXPORT
	#endif

#else
		#define PIPE_EXPORT
#endif



#endif
