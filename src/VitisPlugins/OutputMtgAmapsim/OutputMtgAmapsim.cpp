/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include "OutputMtgAmapsim.h"
#include "Plant.h"
#include "MtgAmapsimFormat.h"
#include "DebugNew.h"

OutputGeomAmapsim::OutputGeomAmapsim():OutputGeom()
{
}

OutputGeomAmapsim::~OutputGeomAmapsim(void)
{
}

void OutputGeomAmapsim::writeData (const std::string &finame)
{
	std::string out;
	std::stringstream sout;
	sout << finame;
	sout<< "_"<<v_plt->getConfigData().randSeed<<".mtg";
	out = sout.str();
	outFormat= new mtgAmapsimFormat(out);

	if (outFormat->initFormat(&(v_plt->getTopology())))
	{
		std::cout<<"Output mtg amapsim to "<<out<<std::endl;


		outFormat->printData(v_plt->getGeometry().getGeomBrancStack().at(0));
	}
	else
		std::cout<<"empty plant"<<std::endl;


	delete outFormat;
}




#if !defined STATIC_LIB
extern "C" Output * StartPlugin ()
{
	return new OutputGeomAmapsim();
}
#endif


