/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "GeomBrancAMAP.h"
#include "UtilMath.h"
#include "Bud.h"
#include "Plant.h"
#include "DebugNew.h"
#include "MtgAmapsimFormat.h"

mtgAmapsimFormat::mtgAmapsimFormat(const std::string &finame):mtgFormat(finame)
{
	withRings = false;
	maxRingNumber = 0;
}

std::string mtgAmapsimFormat::formatHeaderFeature (Plant *)
{
	return NULL;
}

bool mtgAmapsimFormat::printHeaderFeature(Plant *plt)
{
	__mtgstream << "STEP	INT		"<<endl;
	__mtgstream << "DEATH	INT		#0 vivant en croissance, 1 vivant en fin d'axe, 2 death"<<endl;
	__mtgstream << "PHYORG1	INT		"<<endl;
	__mtgstream << "LNGORG1	REAL		"<<endl;
	__mtgstream << "WIDTHORG1	REAL		"<<endl;
	__mtgstream << "PHYORG2	INT		"<<endl;
	__mtgstream << "LNGORG2	REAL		"<<endl;
	__mtgstream << "WIDTHORG2	REAL		"<<endl;
	__mtgstream << "PHYORG3	INT		"<<endl;
	__mtgstream << "LNGORG3	REAL		"<<endl;
	__mtgstream << "WIDTHORG3	REAL		"<<endl;
	__mtgstream << "PHYORG4	INT		"<<endl;
	__mtgstream << "LNGORG4	REAL		"<<endl;
	__mtgstream << "WIDTHORG4	REAL		"<<endl;

	BrancAMAP *b = (BrancAMAP*)plt->getTopology().seekTree(TopoManager::RESET);
	b->startPosit();
	b->positOnFirstEntnWithinBranc();
	InterNodeAMAP *e = b->getCurrentEntn();
	int *n;
	if ((n=(int*)e->getAdditionalData("ring number")) != NULL)
	{
		withRings = true;
		maxRingNumber = *n;
		for (int i=0; i<*n; i++)
			__mtgstream << "RINGWIDTH"<<i+1<<"	REAL		"<<endl;
	}
	b->endPosit();
	return true;
}

void mtgAmapsimFormat::printFeatureDescription (Plant *)
{
	__mtgstream << "STEP\tDEATH\tPHYORG1\tLNGORG1\tWIDTHORG1\tPHYORG2\tLNGORG2\tWIDTHORG2\tPHYORG3\tLNGORG3\tWIDTHORG3\tPHYORG4\tLNGORG4\tWIDTHORG4\t";
	if (withRings)
	{
		for (int i=0; i<maxRingNumber; i++)
			__mtgstream << "RINGWIDTH"<<i+1<<"\t";
	}
}

void mtgAmapsimFormat::printFeatureData (GeomBranc *g, VitisObject *o)
{
	mtgFormat::printFeatureData (g, o);
	DecompAxeLevel *d = (DecompAxeLevel *)o;
	if (d->getLevel() == AXE)
	{
		BrancAMAP *b = (BrancAMAP *)o;
		__mtgstream << "\t" << (int)b->getBud()->death << "\t#branch";
	}
	else if (d->getLevel() == GROWTHUNIT)
	{
		GrowthUnitAMAP *u = (GrowthUnitAMAP *)o;
		if (!withRings)
			__mtgstream << (float)u->phyAgeGu <<"\t\t#growthunit";
		else
		{
			__mtgstream << (float)u->phyAgeGu <<"\t\t\t\t\t\t\t\t\t\t";
			InterNodeAMAP *e = (InterNodeAMAP *)u->getCurrentElementOfSubLevel(INTERNODE);
			int *n;
			if ((n=(int*)e->getAdditionalData("ring number")) != NULL)
			{
				float rprev = 0, w;
				float *l = (float*)e->getAdditionalData("length");
				vector<float> *rv = (vector<float> *)e->getAdditionalData ("ring volume");
				for (int i=0; i<*n; i++)
				{
					w = (float)sqrt (((*rv)[i] + M_PI*rprev*rprev* *l)/ *l / M_PI) - rprev;
					__mtgstream << w << "\t";
					rprev += w;
				}
			}
		}

	}
	else if (d->getLevel() == INTERNODE)
	{
		__mtgstream << "\t\t";
		int pos =g->getAxe()->getBranc()->getCurrentElementIndexOfSubLevel(INTERNODE);

		for(unsigned int i=0; i<g->getElementsGeo().size(); i++)
		{
			for(unsigned int j=0; j<g->getElementsGeo().at(i)->getGeomChildren().size(); j++)
			{
				GeomBranc *borne = g->getElementsGeo().at(i)->getGeomChildren().at(j);
				if (   borne->getAxe()->indexOnBearerAtLowestLevel == pos
					 && borne->getElementsGeo().size() > 0)
				{
					BrancAMAP *b = (BrancAMAP*)borne->getBranc();
					if (b->nature == 2)
					{
						GeomElemCone *e = (GeomElemCone*)borne->getElementsGeo().at(0);
						__mtgstream << b->phyAgeInit << "\t" << e->getLength() << "\t" << e->getBottomDiam()<< "\t" ;
					}
				}
			}
		}
	}
}

void mtgAmapsimFormat::printFeatureData (VitisObject *)
{
}

void mtgAmapsimFormat::adjustMaxOrder (Branc *br)
{
	BrancAMAP *b = (BrancAMAP *)br;

	if (   b->nature != SimpleImmediateOrgan
		&& b->getCurrentEntnNumberWithinBranch() > 0)
	{
		if (maxOrder <= b->getCurrentHierarc()->ordre)
			maxOrder = b->getCurrentHierarc()->ordre+1;
		if (maxShiftOrder < b->getCurrentHierarc()->ordre - b->getCurrentHierarc()->getBearer()->ordre)
			maxShiftOrder = b->getCurrentHierarc()->ordre - b->getCurrentHierarc()->getBearer()->ordre;
	}

}


char mtgAmapsimFormat::getDecompLetterAtLevel (int level)
{
	char c;
	switch (level)
	{
	case 0:
	c = 'B';
	break;
	case 1:
	c = 'U';
	break;
	case 2:
	c = 'C';
	break;
	case 3:
	c = 'Z';
	break;
	case 4:
	c = 'T';
	break;
	case 5:
	c = 'E';
	break;
	default:
		c = 'Q';
		break;
	}
	return c;
}

bool mtgAmapsimFormat::skipLevel (int level)
{
	if (level == 4)
		return true;
	return false;
}

bool mtgAmapsimFormat::skipObject (GeomBranc *g, DecompAxeLevel *d)
{
	if (mtgFormat::skipObject (g, d))
		return true;

	if (d->getLevel() == AXE)
	{
		BrancAMAP *b = (BrancAMAP *)d;
		if (b->nature == SimpleImmediateOrgan)
			return true;
	}

	return false;
}
