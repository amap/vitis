/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _MTGAMAPSIMFORMAT_H
#define _MTGAMAPSIMFORMAT_H
#include "externOutputMtgAmapSim.h"
#include "MtgFormat.h"


class OUTPUTMTGAMAPSIM_EXPORT mtgAmapsimFormat : public mtgFormat
{

	public:

	mtgAmapsimFormat(const std::string &finame);

// 	virtual ~mtgAmapsimFormat(void);

	virtual void adjustMaxOrder (Branc *b);
	virtual bool printHeaderFeature (Plant *plt);
	virtual std::string formatHeaderFeature (Plant *plt);
	virtual void printFeatureDescription (Plant *plt);
	virtual void printFeatureData (GeomBranc *h, VitisObject *);
	virtual void printFeatureData (VitisObject *);

	virtual char getDecompLetterAtLevel (int level);
	bool skipLevel (int level);
	bool skipObject (GeomBranc *g, DecompAxeLevel *d);

	bool withRings;
	int maxRingNumber;

};

#endif
