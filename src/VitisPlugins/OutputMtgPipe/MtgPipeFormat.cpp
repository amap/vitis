/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "UtilMath.h"
#include "Plant.h"
#include "DebugNew.h"
#include "MtgPipeFormat.h"
#include "paramPipe.h"


mtgPipeFormat::mtgPipeFormat(const std::string &finame):mtgAmapsimFormat(finame)
{
}


int mtgPipeFormat::initFormat(VitisObject * vobj)
{
	mtgFormat::initFormat(vobj);
	return 0;
}

bool mtgPipeFormat::printHeader(Plant *plt)
{
	__mtgstream <<"#Pipe Model Specific parameters value"<<std::endl;
//   __mtgstream <<"#"<< asctime(localtime(&t)) <<endl;

	maxOrder = 6;

   mtgAmapsimFormat::printHeader (plt);

   return true;
}
