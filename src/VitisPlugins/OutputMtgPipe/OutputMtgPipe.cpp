/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include "OutputMtgPipe.h"
#include "Plant.h"
#include "MtgPipeFormat.h"
#include "DebugNew.h"

OutputGeomPipe::OutputGeomPipe():OutputGeomAmapsim()
{
}

OutputGeomPipe::~OutputGeomPipe(void)
{
}


void OutputGeomPipe::writeData (const std::string &finame)
{

	outFormat= new mtgPipeFormat(finame+".mtg");

	outFormat->initFormat(&(v_plt->getTopology()));

	std::cout<<"Output mtg pipe"<<std::endl;


	outFormat->printData(v_plt->getGeometry().getGeomBrancStack().at(0));


	delete outFormat;
}




#if !defined STATIC_LIB
extern "C" Output * StartPlugin ()
{
	return new OutputGeomPipe();
}
#endif


