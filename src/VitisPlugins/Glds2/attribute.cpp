/*!
  attribute.cpp

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#include "attribute.h"
#include <libxml++/libxml++.h>

using namespace glds;

// return the value of the attribute
std::string Attribute::getValue() const
{
	const xmlpp::Attribute* att = dynamic_cast<const xmlpp::Attribute*>(xmlppNode);
	// todo exception
	return att->get_value();
}

// Set the value of the TextNode
void Attribute::setValue(const std::string& value)
{
	xmlpp::Attribute* att = dynamic_cast<xmlpp::Attribute*>(xmlppNode);
	// todo exception
	att->set_value(value);
}

Attribute& Attribute::operator = (const std::string& value)
{
	setValue(value);
	return *this;
}

//! the ostream << operator
std::ostream& glds::operator << (std::ostream& o, const Attribute& a)
{
	return o << a.getValue();
}


