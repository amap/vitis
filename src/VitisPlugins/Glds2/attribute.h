/*!
  attribute.h

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#ifndef __GLDS_ATTRIBUTE_H
#define __GLDS_ATTRIBUTE_H

#include "namespace.h"
#include "Node.h"
#include <string>

GLDS_NAMESPACE_OPEN

class GLDS_EXPORT Attribute : public Node
{
 public:
  //! Constructor
  Attribute(xmlpp::libNode* xml_node = NULL) : Node(xml_node) {}
  Attribute(const Node& rhs) : Node(rhs) {}
  
  //! return the value of the attribute
  std::string getValue() const;

  //! Set the value of the TextNode
  void setValue(const std::string& value);

  operator std::string() const {return getValue();}
  Attribute& operator = (const std::string& value);

};

//! the ostream << operator
std::ostream& operator << (std::ostream& o, const glds::Attribute& a);


GLDS_NAMESPACE_CLOSE

#endif // _GLDS_ATTRIBUTE


