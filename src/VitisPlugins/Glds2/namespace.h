/*!
  namespace.h

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/

#ifndef __GLDS_NAMESPACE_H
#define __GLDS_NAMESPACE_H
#include "externGlds.h"

/* open namespace */
#define GLDS_NAMESPACE_OPEN namespace glds { 

/* close namespace */
#define GLDS_NAMESPACE_CLOSE } // namespace glds 


#endif // __GLDS_NAMESPACE_H
