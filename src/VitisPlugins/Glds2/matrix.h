/*!
  matrix.h

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#ifndef __GLDS_MATRIX_H
#define __GLDS_MATRIX_H

#include "namespace.h"
#include "text.h"
#include "exception.h"
#include <sstream>
#include <iterator>
#include <algorithm>

GLDS_NAMESPACE_OPEN

/*!
  @brief A usefull class for numeric matrix representation.
  
  @param T The type of number : can be int, float, double, etc...
*/
template<class T>
class Matrix : public Node
{
 public:
  //! Constructor
  Matrix(const Node& node) : Node(node) {}

  //! set the value
  void setValue(const std::vector<std::vector<T> >& value);
  //! set the value from an array of size (i,j)
  void setValue(const T* value, unsigned int i, unsigned int j);

  //! get the value
  std::vector<std::vector<T> > getValue() const;
  //! fill the array with the value
  void fill(T* a) const;
};


//! set the value
template <class T>
void Matrix<T>::setValue(const std::vector<std::vector<T> >& value)
{
  // the matrix is of the form :
  // n11 n12 n13 ..
  // n21 n22 n23 ..
  // ..

  std::ostringstream stream;
  for (unsigned int i = 0; i < value.size(); ++i) {
    std::copy(value[i].begin(), value[i].end(), std::ostream_iterator<T>(stream, " "));
    stream << std::endl;
  }

  Text t(*this);
  t = stream.str();
}


// set the value from an array of size (i,j)
template<class T>
void Matrix<T>::setValue(const T* value, unsigned int i, unsigned int j)
{
  std::vector< std::vector<T> > temp;
  temp.resize(i);
  for (unsigned int ii = 0; ii < i; ++ii) {
    temp[ii].resize(j);
    for (unsigned int jj = 0; jj < j; ++jj) {
      temp[ii][jj] = value[j*ii+jj];
    }
  }
  setValue(temp);
}


// get the value
template<class T>
std::vector<std::vector<T> > Matrix<T>::getValue() const
{
  // we get the string
  std::string s = Text(*this).getValue();
  std::istringstream stream(s);

  std::vector<std::vector<T> > ret;
  
  while(stream) {
    // we read line by line
    std::string srow;
    getline(stream, srow);

    // we store the line into a vector
    std::istringstream streamrow(srow);
    std::vector<T> row;
    std::copy(std::istream_iterator<T>(streamrow), std::istream_iterator<T>(), std::back_inserter(row));
    // now we put the vector in the matrix
    // sometime the row can be empty, it's when we got an empty line
    if (!row.empty())
      ret.push_back(row);
  }
  return ret;
}

// fill the array with the value
template<class T>
void Matrix<T>::fill(T* a) const
{
  typedef typename std::vector<std::vector<T> >::const_iterator Iterator1;
  typedef typename std::vector<T>::const_iterator Iterator2;

  std::vector<std::vector<T> > temp = getValue();
  
  unsigned int i = 0;
  for (Iterator1 iter1 = temp.begin(); iter1 != temp.end(); ++iter1)
    for (Iterator2 iter2 = iter1->begin(); iter2 != iter1->end(); ++iter2)
      a[i++] = *iter2;
}



GLDS_NAMESPACE_CLOSE

#endif // __GLDS_MATRIX_H


