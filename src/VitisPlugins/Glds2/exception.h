/*
  exception.h

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#ifndef __GLDS_EXCEPTION_H
#define __GLDS_EXCEPTION_H

#include "namespace.h"
#include <exception>
#include <string>

GLDS_NAMESPACE_OPEN

class Node;

/*!
  \brief The glds exception class.

  This class is the main class for all the exceptions that
  will be raised by the glds librairie. 
*/
class GLDS_EXPORT Exception : public std::exception
{
 public:
  Exception(const std::string& mess) throw() : _mess(mess) {}
  virtual ~Exception() throw() {}
  virtual const char* what() const throw() {return _mess.c_str();}
 protected:
  std::string _mess;
};

/*!
  @brief Exception raised when a node'problem occurs
*/
class NodeException : public Exception
{
 public:
  NodeException(const Node* node, const std::string& mess) throw() : Exception(mess), _node(node)   { }
  virtual ~NodeException() throw() {}
  //! returns the exception error message
  virtual const char* what() const throw();
 protected:
  const Node* _node;
};

/*!
  @brief Exception raised when a node is empty
*/
class GLDS_EXPORT EmptyNodeException : public Exception
{
 public:
  EmptyNodeException(const Node* node) throw() : Exception("empty node")   { }
  virtual ~EmptyNodeException() throw() {}
};


GLDS_NAMESPACE_CLOSE

#endif // __GLDS_EXCEPTION_H

