/*!
  document.h

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#ifndef __GLDS_DOCUMENT_H
#define __GLDS_DOCUMENT_H

#include "namespace.h"
#include "Node.h"
#include <string>
#include <ostream>

namespace xmlpp
{
  class Document;
}


GLDS_NAMESPACE_OPEN

/*! \class Document
  \brief Contains all the node of a data structure.

  Every glds::Node must belong to a glds::Document.
*/
class GLDS_EXPORT Document
{
  friend std::ostream& operator << (std::ostream& o, const Document& doc);

 public:

  //! constructor
  Document();
  //! second constructor
  Document(xmlpp::Document* doc);
  //! assignement to an xmlpp::Document
  Document& operator = (xmlpp::Document* doc);

  // destructor
  virtual ~Document();

  //! return the root node of the document
  Node getRootNode() const;

  //! create the root node
  Node createRootNode(const std::string& name);

  //! write to file
  void writeToFile(const std::string& filename) const;

  //! write to file and compress it
  void writeToFileCompressed(const std::string& filename) const;

 protected:
  //! the xmlpp::document
  xmlpp::Document* _xmlDoc;
  
  // set to true if the xml_document has been created here.
  bool _created;

  //! copy constructor forbidden
  Document(const Document& rhs);
  //! assignement forbidden
  const Document& operator = (const Document& rhs);
};

//! print to stream operator
std::ostream& operator << (std::ostream& o, const Document& doc);

GLDS_NAMESPACE_CLOSE

#endif //__GLDS_DOCUMENT_H

