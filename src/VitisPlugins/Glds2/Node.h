/*!
  node.h

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#ifndef __GLDS_NODE_H
#define __GLDS_NODE_H

#include "namespace.h"

#include <string>
#include <vector>
#include <sstream>

// post declaration
namespace xmlpp
{
  class libNode;
}

GLDS_NAMESPACE_OPEN

class Node;

typedef std::vector<Node> NodeList;

/*!
  \class Node
  \brief The main class for all the green lab specialised data structures.

  Node instances store a reference on a xmlpp Node,
  and are aimed to offer convenient methods to access this Node.
*/
class GLDS_EXPORT Node
{
 public:
  //! Constructor
  Node(xmlpp::libNode* xml_node = NULL) : xmlppNode(xml_node) {}
  Node(const Node& rhs) : xmlppNode(rhs.xmlppNode) {}
  
  //! Return false if the Node was created from a NULL xml++ node
  operator bool() const {return (xmlppNode != NULL);}

  
  //! return the name of the node
  std::string getName() const;
  //! set the name of the node
  void setName(const std::string& name);
  
  //! set an attribute called id
  void setId(const std::string& id) {setAttribute("id", id);}

  /*!
    Obtain the list of child nodes
    including the base node's child nodes
  */
  NodeList getChildren(const std::string& name = std::string()) const;
  

  /*!
    Obtain the first child of a given name,
  */
  Node getChild(const std::string& name) const;

  /*!
    Add a new child to the node.
    \return The new child.
  */
  Node addChild(const std::string& name);

  /*!
    Remove a child from the node.
  */
  void removeChild(Node node);

  /*! Return the Xpath of this node */
  std::string getPath() const;

  /*!
    find nodes from an xpath expression
    \param xpath The xpath expression
  */
  NodeList find(const std::string& xpath) const;

  //! returns the first descendants of a given name
  NodeList getFirstDescendants(const std::string& name) const;
  

  /*! returns the attribute of a given name
    To use the attribute you will have then to cast it into an Attribute instance.
  */
  Node getAttribute(const std::string& name) const;

  /*! Set the value of the attribute with this name
    \return The new Attribute (to be casted to an attribute)
  */
 /* template<class T>*/
  Node setAttribute (const std::string& name, const std::string& value);
    //Node setAttribute(const std::string& name, T value);

  Node getParent() const ;

 protected:

  //! Each glds refers to an xml++ Node
  xmlpp::libNode* xmlppNode;
};


/*
template<class T>
Node Node::setAttribute(const std::string& name, T value)
{
  std::stringstream sstream;
  sstream << value;
  return setAttribute<const std::string&>(name, sstream.str());
}
*/


GLDS_NAMESPACE_CLOSE

#endif // __GLDS_NODE_H
