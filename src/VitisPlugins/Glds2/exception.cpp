/*
	exception.cpp

	This file is part of the greenlab data structure librairie (glds).
	This file is covered by the GNU General Public Licence,
	which should be included with libxml++ as the file COPYING.

*/

#include "exception.h"
#include "Node.h"


using namespace glds;

const char* NodeException::what() const throw()
{
	std::string ret = "node \"" + _node->getName() + "\" : " + Exception::what();
	return ret.c_str(); 
}
