/*!
  ector.h

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#ifndef __GLDS_VECTOR_H
#define __GLDS_VECTOR_H

#include "namespace.h"
#include "text.h"
#include "exception.h"
#include <sstream>
#include <iterator>
#include <algorithm>

GLDS_NAMESPACE_OPEN

/*!
  @brief A usefull class for numeric vector representation.
  
  @param T The type of number : can be int, float, double, etc...
*/
template<class T>
class Vector : public Node
{
 public:
  //! Constructor
  Vector(const Node& node = Node(NULL)) : Node(node) {}

  //! set the value
  void setValue(const std::vector<T>& value);
  //! set the value from an array of size i
  void setValue(const T* value, unsigned int i);

  //! get the value
  std::vector<T> getValue() const;
  //! fill the array with the value
  void fill(T* a) const;
};


// set the value
template <class T>
void Vector<T>::setValue(const std::vector<T>& value)
{
  std::ostringstream stream;
  std::copy(value.begin(), value.end(), std::ostream_iterator<T>(stream, " "));
  Text t(*this);
  t.setValue(stream.str());
}


// set the value from an array of size i
template <class T>
void Vector<T>::setValue(const T* value, unsigned int i)
{
  std::vector<T> temp(i);
  for (unsigned int n=0; n < i; ++n) {
    temp[n] = value[n];
  } 
  setValue(temp);
}



// get the value
template<class T>
std::vector<T> Vector<T>::getValue() const
{
  // we get the string
  std::string s = Text(*this).getValue();
  std::istringstream stream(s);
  
  std::vector<T> ret;
  std::copy(std::istream_iterator<T>(stream), std::istream_iterator<T>(), std::back_inserter(ret));
  return ret;
}

// fill the array with the value
template<class T>
void Vector<T>::fill(T* a) const
{
  std::vector<T> temp = getValue();
  for(int i = 0; i < temp.size(); ++i) {
    a[i] = temp[i];
  }
}


GLDS_NAMESPACE_CLOSE

#endif // __GLDS_VECTOR_H


