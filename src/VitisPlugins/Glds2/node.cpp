/*
	node.cpp

	This file is part of the greenlab data structure librairie (glds).
	This file is covered by the GNU General Public Licence,
	which should be included with libxml++ as the file COPYING.

*/

extern "C" {
	struct _xmlNode;
};

#include "Node.h"
#include "exception.h"
#include <libxml++/libxml++.h>
#include "attribute.h"


//#define DEBUG_NODE

#ifdef DEBUG_NODE
#include <iostream>
#define PRINT(x) std::cout << "Node debug : " << x << std::endl;
#else
#define PRINT(x)
#endif

using namespace glds;

static NodeList xmlToGlds(const xmlpp::libNode::NodeList& list)
{
	PRINT("xml_to_glds");

	// \TODO change the algo
	NodeList ret;
	for(xmlpp::libNode::NodeList::const_iterator iter = list.begin(); iter != list.end(); ++iter) {
		ret.push_back(Node(*iter));
	}  
	return ret;
}

static NodeList xmlToGlds(const xmlpp::NodeSet& set)
{
	PRINT("xmlToGlds");

	NodeList ret;
	for(xmlpp::NodeSet::const_iterator iter = set.begin(); iter != set.end(); ++iter) {
		ret.push_back(Node(*iter));
	}  
	// \TODO change the algo
	return ret;
}


//! return the name of the node
std::string Node::getName() const
{
	if (!*this) throw EmptyNodeException(this);
	return xmlppNode->get_name();
}

//! set the name of the node
void Node::setName(const std::string& name)
{
	if (!*this) throw EmptyNodeException(this);
	xmlppNode->set_name(name);
}


// Obtain the list of child nodes
// including the basenode's child nodes
NodeList Node::getChildren(const std::string& name) const
{  
	if (!*this) throw EmptyNodeException(this);
	PRINT(getName() << ".getChildren(" << name << ")");

	xmlpp::libNode::NodeList children = xmlppNode->get_children(name);

	return xmlToGlds(children);
}


// Obtain the first child of a given name,
Node Node::getChild(const std::string& name) const
{
	if (!*this) throw EmptyNodeException(this);
	PRINT(getName() << ".getChild(" << name << ")");

	NodeList children = getChildren(name);
	return (children.empty())? Node(NULL) : children.front();
}

// Add a new child to the node.
Node Node::addChild(const std::string& name)
{
	if (!*this) throw EmptyNodeException(this);
	PRINT(getName() << ".addChild(" << name << ")");

	return xmlppNode->add_child(name);
}

// Remove a child from the node.
void Node::removeChild(Node node)
{
	if (!*this) throw EmptyNodeException(this);
	PRINT(getName() << ".removeChild(" << node.getName() << ")");

	xmlppNode->remove_child(node.xmlppNode);
}

// Return the Xpath of this node
std::string Node::getPath() const
{
	if (!*this) throw EmptyNodeException(this);

	return xmlppNode->get_path();
}



// find nodes from an xpath expression
NodeList Node::find(const std::string& xpath) const
{

	if (!*this) throw EmptyNodeException(this);
	PRINT(getName() << ".find(" << xpath << ")");


	xmlpp::NodeSet children = xmlppNode->find(xpath);


	return xmlToGlds(children);

}


// returns the first descendants of a given name
NodeList Node::getFirstDescendants(const std::string& name) const
{
	if (!*this) throw EmptyNodeException(this);
	PRINT(getName() << ".getFirstDescendants(" << name << ")");

	NodeList ret;

	// we test all the children
	NodeList children = getChildren();
	for (NodeList::const_iterator iter = children.begin(); iter != children.end(); ++iter) {
		// if the children is of the good type then we add it
		if (iter->getName() == name) {
			ret.push_back(*iter);
		}
		else {
			// if not, then we try its children
			NodeList childRet = iter->getFirstDescendants(name);
			ret.insert(ret.end(), childRet.begin(), childRet.end());
		}
	}

	return ret;
}

// returns the attribute of a given name
Node Node::getAttribute(const std::string& name) const
{
	if (!*this) throw EmptyNodeException(this);
	PRINT(getName() << ".getAttribute(" << name << ")");

	const xmlpp::Element* element = dynamic_cast<const xmlpp::Element*>(xmlppNode);
	if (!element) return NULL;

	Node ret =  element->get_attribute(name);
	if (ret) return ret;


	return NULL;
}


// Set the value of the attribute with this name
/*template<>*/
Node Node::setAttribute(const std::string& name, const std::string& value)
{
	if (!*this) throw EmptyNodeException(this);
	PRINT(getName() << ".setAttribute(" << name << ", " << value << ")");

	xmlpp::Element* element = dynamic_cast<xmlpp::Element*>(xmlppNode);
	if (!element) throw NodeException(this, "not an element");

	return element->set_attribute(name, value);
}

Node Node::getParent() const
{
	xmlpp::libNode * parent=xmlppNode->get_parent();
	return Node(parent);
}

