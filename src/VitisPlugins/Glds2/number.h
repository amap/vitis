/*!
  number.h

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#ifndef __GLDS_NUMBER_H
#define __GLDS_NUMBER_H

#include "namespace.h"
#include "text.h"
#include "exception.h"
#include <sstream>

GLDS_NAMESPACE_OPEN

/*!
  \brief Offer a conveniant way to access xml++ TextNode as a number of type T

  T can be int, float, double...
*/
template<class T>
class Number : public Node
{
 public:
  //! Constructor
  Number(const Node& node = Node(NULL)) : Node(node) {}
  
  //! return the value of the TextNode as a number
  T getValue() const;
  //! Set the value of the TextNode
  void setValue(const T& value);

  operator T() const {return  getValue();}
  Number<T>& operator = (const T& value);

};


//! return the value of the TextNode as a number
template <class T>
T Number<T>::getValue() const
{
  // first we read the node as a text one
  const Text text(*this);
  // now we return the value as a number
  std::string value = text.getValue();
  T ret;
  std::istringstream(value) >> ret;
  return ret;
}


//! Set the value of the TextNode
template <class T>
void Number<T>::setValue(const T& value)
{
  // first we transform the value into a string
  std::stringstream sstream;
  sstream << value;

  // we will consider the node as a text one
  Text text(*this);
  text.setValue(sstream.str());
}


template<class T>
Number<T>& Number<T>::operator = (const T& value)
{
  setValue(value);
  return *this;
}

//! the ostream << operator
template<class T>
std::ostream& operator << (std::ostream& o, const glds::Number<T>& n)
{
  return o << n.getValue();
}


GLDS_NAMESPACE_CLOSE



#endif // __GLDS_NUMBER_H


