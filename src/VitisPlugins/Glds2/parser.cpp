/*!
  parser.cpp

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/

#include "parser.h"
#include <libxml++/libxml++.h>
#include "gzstream.h"

using namespace glds;

// constructor
Parser::Parser() : _xmlParser(new xmlpp::DomParser()), _doc(NULL)
{
}


//! destructor
Parser::~Parser()
{
	delete _xmlParser;
}

// parse a file
void Parser::parseFile(const std::string& filename)
{
	_xmlParser->parse_file(filename);
	_doc = _xmlParser->get_document();
}

// parse a compressed file
void Parser::parseFileCompressed(const std::string& filename)
{
	igzstream stream(filename.c_str());
	_xmlParser->parse_stream(stream);
	_doc = _xmlParser->get_document();
}


Document* Parser::getDocument()
{
	return &_doc;
}
