#ifndef __EXPORTGLDS_H__
#define __EXPORTGLDS_H__
	
#pragma warning ( disable : 4251 )

#if !defined STATIC_LIB_GLDS
	#if defined WIN32 

			#ifdef GLDS_EXPORTS
				#define GLDS_EXPORT __declspec(dllexport)
			#else
				#define GLDS_EXPORT __declspec(dllimport)
			#endif
			
	#else
		#define GLDS_EXPORT
	#endif

#else 
		#define GLDS_EXPORT
#endif



#endif
