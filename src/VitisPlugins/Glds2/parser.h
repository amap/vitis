/*!
  parser.h

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#ifndef __GLDS_PARSER_H
#define __GLDS_PARSER_H

#include "namespace.h"
#include "document.h"
#include <string>

//post declaration
namespace xmlpp
{
  class DomParser;
}

GLDS_NAMESPACE_OPEN

/*! \class Parser
   \brief Used to parse file or stream into a glds::Document
*/
class GLDS_EXPORT Parser
{
 public:
  //! constructor
  Parser();
  //! destructor
  virtual ~Parser();

  //! parse a file
  void parseFile(const std::string& filename);
  //! parse a compressed file
  void parseFileCompressed(const std::string& filename);

  //! return the document previously parsed
  Document* getDocument();

 protected:
  //! the xml_document
  xmlpp::DomParser* _xmlParser;
  //! the glds document referencing the xml one
  Document _doc;
  

};


GLDS_NAMESPACE_CLOSE

#endif // __GLDS_PARSER_H
