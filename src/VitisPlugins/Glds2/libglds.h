/*
  libglds.h

  Guillaume Chereau 2004
  
  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/

/*!
  Green Lab Data Structure librairie

  GLDS is a librairie aimed to be used for data representation of plants and forest.
  It uses the xml++ librairie and add specific classes to it in order to simplify the acces to standard datas (such as vector, plants etc...)

  A manual of GLDS is inclued in the distribution. If you don't know GLDS, you should start there first.

*/

#ifndef __GLDS_LIBGLDS_H
#define __GLDS_LIBGLDS_H

#include "namespace.h"
#include "Node.h"
#include "attribute.h"
#include "exception.h"
#include "text.h"
#include "number.h"
#include "vector.h"
#include "matrix.h"
#include "document.h"
#include "parser.h"

GLDS_NAMESPACE_OPEN


GLDS_NAMESPACE_CLOSE

#endif // __GLDS_LIBGLDS_H
