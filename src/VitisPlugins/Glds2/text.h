/*!
  text.h

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#ifndef __GLDS_TEXT_H
#define __GLDS_TEXT_H

#include "namespace.h"
#include "Node.h"
#include <string>
#include <iostream>

GLDS_NAMESPACE_OPEN

/*!
  @brief Offer a conveniant way to access xml++ element
  that contains text

  Glds::Text refers to an element that looks like :
  <element_name>text_value</element_name>
*/
class GLDS_EXPORT Text : public Node
{
 public:
  //! Constructor
  Text(const Node& node = Node(NULL)) : Node(node) {}

  //! Return the value of the TextNode
  std::string getValue() const;
  //! Set the value of the TextNode
  void setValue(const std::string& value);

  operator std::string() const {return getValue();}
  Text& operator = (const std::string& value);
};

//! the ostream << operator
std::ostream& operator << (std::ostream& o, const glds::Text& t);


GLDS_NAMESPACE_CLOSE


#endif // __GLDS_TEXT_H


