/*!
  document.cpp

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/

#include "document.h"
#include <libxml++/libxml++.h>
#include "gzstream.h"

using namespace glds;

// constructor
Document::Document() : _xmlDoc(new xmlpp::Document), _created(true)
{
};

// constructor
Document::Document(xmlpp::Document* doc) : _xmlDoc(doc), _created(false)
{
};

// assignement to an xmlpp::Document
Document& Document::operator = (xmlpp::Document* doc)
{
	if (_created) delete _xmlDoc;
	_xmlDoc = doc;
	_created = false;
	return *this;
}



// destructor
Document::~Document()
{
	if (_created) delete _xmlDoc;
}




//! return the root node of the document
Node Document::getRootNode() const
{
	return _xmlDoc->get_root_node(); 
}

//! create the root node
Node Document::createRootNode(const std::string& name)
{
	return _xmlDoc->create_root_node(name);
}

//! write to file
void Document::writeToFile(const std::string& filename) const
{
	_xmlDoc->write_to_file(filename, "UTF-8");
}

//! write to file and compress it
void Document::writeToFileCompressed(const std::string& filename) const
{
	ogzstream stream(filename.c_str());
	_xmlDoc->write_to_stream(stream, "UTF-8");


}

//! print to stream operator
std::ostream& glds::operator << (std::ostream& o, const glds::Document& doc)
{
	doc._xmlDoc->write_to_stream_formatted(o, "UTF-8");
	return o;
}
