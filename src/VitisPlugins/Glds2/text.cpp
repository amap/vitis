/*!
  text.cpp

  This file is part of the greenlab data structure librairie (glds).
  This file is covered by the GNU General Public Licence,
  which should be included with libxml++ as the file COPYING.

*/


#include "text.h"
#include "exception.h"
#include <libxml++/libxml++.h>

using namespace glds;

// Return the value of the TextNode
std::string Text::getValue() const
{
	const xmlpp::Element* element = dynamic_cast<const xmlpp::Element*>(xmlppNode);
	if (!element) throw NodeException(this, "is not an element");

	const xmlpp::TextNode* text = element->get_child_text();
	if (!text) return std::string();

	return text->get_content();
}

// Set the value of the TextNode
void Text::setValue(const std::string& value)
{
	xmlpp::Element* element = dynamic_cast<xmlpp::Element*>(xmlppNode);
	if (!element) throw NodeException(this, "is not an element");

	element->set_child_text(value);
}

Text& Text::operator = (const std::string& value)
{
	setValue(value);
	return *this;
}


// the ostream << operator
std::ostream& glds::operator << (std::ostream& o, const Text& t)
{
	return o << t.getValue();
}
