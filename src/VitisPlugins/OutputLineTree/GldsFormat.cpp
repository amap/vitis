#include "GldsFormat.h"
#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "GeomBrancAMAP.h"
#include "UtilMath.h"
#include "Bud.h"
#include "Plant.h"
#include "LineTreeFormat.h"
#include "DebugNew.h"


GldsFormat::GldsFormat(const std::string &finame):OutputFormat(finame+".xml")
{
	currentPortee=0;
	currentNbElem=0;
}

GldsFormat::~GldsFormat(void)
{



}


int GldsFormat::initFormat(VitisObject * vobj)
{
	GeomManager * gb = (GeomManager *) vobj;

	Node root = doc.createRootNode("glds");

	Text hlig=root.addChild("headerlig");  
	hlig=LineTreeFormat::computeKey(this->filename);

	currentNode= root;


	return 1;
}

void GldsFormat::printSubStruct(GeomManager & gtr)
{
	int i,indice;
	std::vector<GeomBranc *> & vecGeomBranc = gtr.getGeomBrancStack();
	std::vector<GeomSuperSimpl *> & vecGeomSSimp= gtr.getGeomSimplStack();
	std::vector<GeomSuperSimpl *>::iterator it_ssimp;

	Plant * plt= gtr.getPlant();

	Node nsstruct=currentNode.addChild("substructure");

	for(it_ssimp=vecGeomSSimp.begin(); it_ssimp != vecGeomSSimp.end(); ++it_ssimp)
	{

		if((*it_ssimp)!= NULL)
		{
			if((*it_ssimp)->secteurAng!=NULL)
			{
				for(i=0; i<plt->getConfigData().cfg_supersimplif ; i++)
				{
					indice=(*it_ssimp)->secteurAng[i];
					if(indice!=-1)
					{
						Node strct=nsstruct.addChild("sstruct");
						strct.setAttribute("ref",to_string(indice));
						printAxe(vecGeomBranc[indice],strct);
					}
				}
			}
		}
	}



}


int GldsFormat::printData(VitisObject * vobj)
{

	long i;
	GeomBranc * gb = (GeomBranc *) vobj;

	Plant * plt= gb->getBranc()->getPlant();

	if(plt->getConfigData().cfg_supersimplif)
		printSubStruct(plt->getGeometry());
	else
	{
		currentNode=currentNode.addChild("Plant");
		printDataInOrder(gb,currentNode);
	}

	doc.writeToFile(filename);

	return 1;

}


int GldsFormat::printAxe(GeomBranc * gb, Node & curNod)
{
	int i, j;	

	Vector3 dp, dssb ,ds, t;

	Node axefils;

	GeomBrancCone * curGeomCone;

	Node axe =curNod;

	currentPortee=0;

	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
		Node curlig=axe.addChild("elm");		

		printRecord((GeomElemCone *)gb->getElementsGeo().at(i),curlig);

		currentNbElem++;

		for(j=0; j<gb->getElementsGeo().at(i)->getGeomChildren().size(); j++)
		{				
			axefils=curlig.addChild("clone");

			curGeomCone=(GeomBrancCone *)gb->getElementsGeo().at(i)->getGeomChildren().at(j);

			if(curGeomCone->getGeomBrancClone())
			{
				axefils.setAttribute("refto",to_string(curGeomCone->getGeomBrancClone()->getNum()));					
			}
			else
				axefils.setAttribute("refto",to_string(curGeomCone->getNum()));

			Matrix4 & matTrans=curGeomCone->getCurrentTransform();				  	

			dssb=matTrans.getNormalVector();
			ds=matTrans.getSecondaryVector();
			dp=matTrans.getMainVector();
			t=matTrans.getTranslation();

			// We create the coordinat matrix
			std::vector<std::vector<float> > tempMat;
			tempMat.resize(3);
			// first line of the matrix
			tempMat[0].resize(4);
			tempMat[0][0] = dp[0];
			tempMat[0][1] = dssb[0];
			tempMat[0][2] = ds[0];
			tempMat[0][3] = t[0];
			// second line of the matrix
			tempMat[1].resize(4);
			tempMat[1][0] = dp[1];
			tempMat[1][1] = dssb[1];
			tempMat[1][2] = ds[1];
			tempMat[1][3] = t[1];
			// third line of the matrix
			tempMat[2].resize(4);
			tempMat[2][0] = dp[2];
			tempMat[2][1] = dssb[2];
			tempMat[2][2] = ds[2];
			tempMat[2][3] = t[2];

			Matrix<float> l_mat = axefils.addChild("mat");
			l_mat.setValue(tempMat);

		}

		currentPortee++;

	}

	return 1;
}


int GldsFormat::printDataInOrder(GeomBranc * gb, Node & curNod)
{
	int i, j;	

	Node axe = curNod.addChild("axe");

	currentPortee=0;

	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
		Node curlig=axe.addChild("elm");		

		printRecord((GeomElemCone *)gb->getElementsGeo().at(i),curlig);

		currentNbElem++;

		for(j=0; j<gb->getElementsGeo().at(i)->getGeomChildren().size(); j++)
		{				
			printDataInOrder(gb->getElementsGeo().at(i)->getGeomChildren().at(j),curlig);
		}

		currentPortee++;

	}


	return 1;


}

bool GldsFormat::printHeader (const std::string &headerf)
{

	return true;

}


void GldsFormat::printRecord(GeomElemCone * ge, Node & lig)
{
	int numBr;


	if(((BrancAMAP *)ge->getPtrBrc()->getAxe()->getBranc())->nature==SimpleImmediateOrgan)
		numBr=-1;
	else numBr=ge->getPtrBrc()->getNum();

	Matrix4  __matrix= ge->getMatrix();   

	//__matrix=Matrix4::translation(ge->getPtrBrc()->getBranc()->getPlant()->getGeometry().getPosition())*__matrix;

	Vector3 dp, dssb ,ds, t;	  	

	dssb=__matrix.getNormalVector();
	ds=__matrix.getSecondaryVector();
	dp=__matrix.getMainVector()*ge->getLength();
	t=__matrix.getTranslation();

	//LIG DATA
	// we start a lig node

	lig.setId(to_string(currentNbElem));

	lig.setAttribute("pa",to_string(((GeomBrancAMAP *)ge->getPtrBrc())->phyAgeCur));

	Number<int> l_nsb = lig.addChild("nsb");
	l_nsb = ge->getSymbole();

	Number<int> l_detail = lig.addChild("dtl");
	l_detail = 1;

	Number<int> l_age = lig.addChild("nat");
	l_age = numBr;

	Number<int> l_prt = lig.addChild("prt");
	l_prt = currentPortee;

	// We create the coordinat matrix
	std::vector<std::vector<float> > tempMat;
	tempMat.resize(3);
	// first line of the matrix
	tempMat[0].resize(4);
	tempMat[0][0] = dp[0];
	tempMat[0][1] = dssb[0];
	tempMat[0][2] = ds[0];
	tempMat[0][3] = t[0];
	// second line of the matrix
	tempMat[1].resize(4);
	tempMat[1][0] = dp[1];
	tempMat[1][1] = dssb[1];
	tempMat[1][2] = ds[1];
	tempMat[1][3] = t[1];
	// third line of the matrix
	tempMat[2].resize(4);
	tempMat[2][0] = dp[2];
	tempMat[2][1] = dssb[2];
	tempMat[2][2] = ds[2];
	tempMat[2][3] = t[2];

	Matrix<float> l_mat = lig.addChild("mat");
	l_mat.setValue(tempMat);

	Number<float> l_haut = lig.addChild("dUp");
	l_haut = ge->getVarHaut();

	Number<float> l_bas = lig.addChild("dDwn");
	l_bas = ge->getVarBas();

}



