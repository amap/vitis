#ifndef _GLDSFORMAT_H
#define _GLDSFORMAT_H
#include "OutputFormat.h"
#include "libglds.h"
using namespace glds;

class GldsFormat : public OutputFormat
{

	Document doc;

	Node currentNode;

	/// \brief Current "port�e"
	int currentPortee;

	/// \brief Number of elements already written
	int currentNbElem;

	public:

	GldsFormat(const std::string &finame);

	virtual ~GldsFormat(void);

	virtual int initFormat(VitisObject * vobj);

	virtual int printData(VitisObject *);

	int printAxe(class GeomBranc * gb, Node & curNod);

	int printDataInOrder(class GeomBranc * vobj, Node & curNod);

	virtual bool printHeader (const std::string &headerf);

	virtual void printRecord(class GeomElemCone * ge, Node & ligcur);

	void printSubStruct(class GeomManager & gtr);
};

#endif

