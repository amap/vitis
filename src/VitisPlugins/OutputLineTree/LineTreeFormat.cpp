/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#include <windows.h>
#endif
#include "LineTreeFormat.h"
#include <algorithm>
#include "GeomManager.h"
#include "VitisConfig.h"
#include "GeomElemCone.h"
#include "GeomBrancCone.h"
#include "UtilMath.h"
#include "Bud.h"
#include "Plant.h"
#include "DebugNew.h"


#ifdef WIN32
extern "C"
{
#include "fcrypt0.c"
}
#endif



std::string LineTreeFormat::PROTECTION = "!&@V3@aFFv5</p*kRE=fCUSB9-;/Q[RS9@nq=9PL22^RE[%<pTIsMvS6mUoL8?RJmQWrM%a.Sj@rl8cO";




LineTreeFormat::LineTreeFormat(const std::string &finame):OutputFormat(finame),__ligstream(finame+".lig")
{
	currentPortee=0;
	cptID=0;
}

LineTreeFormat::~LineTreeFormat(void)
{
	__ligstream.getStream().close();
}

#if 0
std::string LineTreeFormat::computeKey(std::string & filename)
{
	return std::string();
}
#endif


bool LineTreeFormat::printHeader(const std::string& headerf)
{
	if(headerf.empty()){
		__ligstream << PROTECTION;
		return true;
	}
	else if(headerf.size() == 80){
		__ligstream << headerf;
		return true;
	}
	else
		__ligstream << PROTECTION;
	return false;
}


int LineTreeFormat::initFormat(VitisObject * vobj)
{
	GeomManager * gb = (GeomManager *) vobj;

	vectId.resize(gb->getElemeGeoMaxNumber());

	std::string str;
	printHeader(str);

//	printInf(gb);

	return 1;

}




//raw writing
int LineTreeFormat::printData(VitisObject * vobj)
{

	GeomBranc * gb = (GeomBranc *) vobj;
	Plant * plt= gb->getBranc()->getPlant();

	if(plt->getConfigData().cfg_supersimplif)
        printSubStructure (gb);
	else
		printDataInOrder(gb, 0);

	printInf(&gb->getBranc()->getPlant()->getGeometry());

	return 1;

}

int LineTreeFormat::printSubStructure(GeomBranc * gb)
{
	unsigned int i, j;

	Matrix4 bearTransla;

	GeomBrancCone * curGeomBrancCone, *clone;

	currentPortee=0;

	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
        GeomElemCone *cone = (GeomElemCone *)gb->getElementsGeo().at(i);

        if (i == 0)
            bearTransla = Matrix4::translation(cone->getMatrix().getTranslation());

		printRecord(cone, 0 );

		for(j=0; j<cone->getGeomChildren().size(); j++)
		{

			curGeomBrancCone=(GeomBrancCone*)cone->getGeomChildren().at(j);

			if((clone = (GeomBrancCone*)curGeomBrancCone->getGeomBrancClone()) != 0)
			{
				Matrix4 & matTrans=curGeomBrancCone->getCurrentTransform();
                Vector3 transla = clone->getElementsGeo().at(0)->getMatrix().getTranslation();

                curGeomBrancCone->copyFrom(clone, bearTransla*matTrans*Matrix4::translation(-transla));
			}

            printSubStructure(curGeomBrancCone);

		}

		currentPortee++;

	}

	return 1;
}


//Order writing
int LineTreeFormat::printDataInOrder(VitisObject * vobj, int idParent)
{

	unsigned int i,j,nbChild;
	GeomBranc * gb = (GeomBranc *) vobj;
	int curId;

	curId = idParent;

	//position dans la branche
	currentPortee=0;

	//Pour chaque �l�ment g�om�trique
	for(i=0; i<gb->getElementsGeo().size(); i++)
	{
		//Si on est pas sur le premier neoud de l'axe, on indique l'id du noeud porteur sur l'axe courant
		vectId[cptID].idParent=curId;


		curId=cptID;



		//Si on est pas sur le premier noeud de la plante, on maj les relations p�res-fils
		nbChild = 0;
		if(curId != 0)
		{
			int idBearer=vectId[curId].idParent;

			vectId[idBearer].idChild[vectId[idBearer].nbChild]=curId;

			vectId[idBearer].nbChild++;

		}
		nbChild=(unsigned int)gb->getElementsGeo().at(i)->getGeomChildren().size();
		if (i < gb->getElementsGeo().size()-1)
			nbChild ++;

		if (nbChild > 0)
			vectId[curId].idChild = new int[nbChild];

		currentPortee=i;
		printRecord((GeomElemCone *)gb->getElementsGeo().at(i),curId);

		cptID++;

		//Pour chaque �l�ment port� par celui courant
		for(j=0; j<gb->getElementsGeo().at(i)->getGeomChildren().size(); j++)
			//for (j=0; j<gb->getAxe().getNbBorne(); j++)
		{

			//On lance la r�cursivit� sur un axe port�

			//Ici on indique l'id du parent ( ici l'id courant) � l'�l�ment qui vient d'�tre cr��
            if (gb->getElementsGeo().at(i)->getGeomChildren().at(j)->getElementsGeo().size() > 0)
                printDataInOrder(gb->getElementsGeo().at(i)->getGeomChildren().at(j), curId);
		}
	}

	return 1;

}



void LineTreeFormat::printRecord(GeomElemCone * ge, int )
{
    int numBr, i;

	numBr=ge->getPtrBrc()->getNum();

	for (i=0; i<tabSymb.size(); i++)
		if (tabSymb[i] == ge->getSymbole())
			break;
	if (i == tabSymb.size())
		tabSymb.push_back(ge->getSymbole());


	Branc *b=(Branc*)(ge->getPtrBrc()->getBranc());

	b->startPosit();
	b->positOnElementAtSubLevel((int)ge->getPosInBranc(), b->getPlant()->getPlantFactory()->getLevelOfDecomposition(b->getClassType()));
	if (!swap)
	{
		__ligstream << long(ge->getSymbole()) << long(1) << numBr <<long(currentPortee);
	}
	else
	{
		__ligstream << swapDatal(long(ge->getSymbole())) << swapDatal(long(1)) << swapDatal(numBr) <<swapDatal(long(currentPortee));
	}
	b->endPosit();

	Matrix4  __matrix= ge->getMatrix();

	__matrix=Matrix4::translation(ge->getPtrBrc()->getBranc()->getPlant()->getGeometry().getPosition())*__matrix;

	Vector3 dp, dssb ,ds, t;



	dssb=__matrix.getNormalVector();
	ds=__matrix.getSecondaryVector();
	dp=__matrix.getMainVector()*(float)ge->getLength();
	t=__matrix.getTranslation();

	if (!swap)
	{
	__ligstream << (float) dp[1]<< (float) dssb[1]<< (float) ds[1]<< (float) t[1];
	__ligstream << (float) -dp[0]<< (float) -dssb[0]<< (float) -ds[0]<< (float) -t[0];
	__ligstream << (float) dp[2]<< (float) dssb[2]<< (float) ds[2]<< (float) t[2];

	__ligstream << (float)ge->getBottomDiam()<< (float)ge->getTopDiam();

	__ligstream << (long)numBr;
	}
	else
	{
	__ligstream << swapDataf((float) dp[1])<< swapDataf((float) dssb[1])<< swapDataf((float) ds[1])<< swapDataf((float) t[1]);
	__ligstream << swapDataf((float) -dp[0])<< swapDataf((float) -dssb[0])<< swapDataf((float) -ds[0])<< swapDataf((float) -t[0]);
	__ligstream << swapDataf((float) dp[2])<< swapDataf((float) dssb[2])<< swapDataf((float) ds[2])<< swapDataf((float) t[2]);

	__ligstream << swapDataf((float)ge->getBottomDiam())<< swapDataf((float)ge->getTopDiam());

	__ligstream << swapDatal((long)numBr);
	}

}



void LineTreeFormat::printInf(GeomManager * geomt)
{

	unsigned int i;
	std::string finame=filename+".inf";
	std::ofstream inf(finame.c_str());

	Vector3 _max=geomt->getMax();
	Vector3 _min=geomt->getMin();

	inf << "File : 1mod_rauh" << std::endl;
	inf << "Age : "<<10<<"  "<< tabSymb.size()<<"  pattern(s) number of branches 1"  << std::endl;
	inf << "Random_seed 0 Simplification 0 " << std::endl;

	inf.precision(4);

	inf << _max[0]<< " " << _max[1]<< " " << _max[2]<< std::endl;
	inf << _min[0] << " " << _min[1] << " " << _min[2] << std::endl;

	for (i = 0; i <tabSymb.size(); i++)
	{

		if (tabSymb[i] > 0)
			inf << "entre-noeud  "<<tabSymb[i]<<"  nentn105  e" << std::endl;
		else
			inf << "feuille  "<<-tabSymb[i]<<"  feui104  f" << std::endl;


	}

	inf.close();

}



