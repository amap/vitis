/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  LineTreeFormat.h
///			\brief Definition of the LineTreeFormat class
///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _LINETREEFORMAT_H
#define _LINETREEFORMAT_H
#include "OutputFormat.h"
#include "externOutputLineTree.h"
#include "bfstream.h"
using namespace Vitis;
#include <vector>


struct IdParentChild
{
	int idParent;

	int * idChild;

	int nbChild;

	IdParentChild() {idParent=0; idChild=NULL; nbChild=0;}

	~IdParentChild()
	{
		delete [] idChild;
	}



};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class LineTreeFormat
///			\brief Write data into .lig , .arc, .inf format
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class OUTPUTLINETREE_EXPORT LineTreeFormat:public OutputFormat
{
	protected:
	/// \brief Default linetree file header ( for linux = no protection)
	static std::string PROTECTION;

	/// \brief linetree filestream
	beofstream   __ligstream;

	/// \brief geometrical symbols vector
	std::vector<int> tabSymb;

	std::vector<IdParentChild> vectId;

	Uint cptID;

	/// \brief Current "port�e"
	int currentPortee;




	public:

	/// \brief Constructor
	/// \param finame base filename (path + short filename - extension)
	LineTreeFormat(const std::string &finame);

	/// \brief Destructor
	virtual ~LineTreeFormat(void);

	/// \brief Print the vobj into line-tree and arc
	virtual int printData(VitisObject * vobj);

	/// \brief Print the vobj into line-tree and arc
	virtual int initFormat(VitisObject * vobj);

	virtual bool printHeader (const std::string &headerf);

	/// \brief Compute protection key for .lig
	static std::string computeKey(std::string & filename);

	/// \brief Print a linetree record
	virtual void printRecord(class GeomElemCone * ge, int elemId);

	/// \brief Print a the information file (.inf)
	virtual void printInf(class GeomManager *) ;

	/// \brief Print tree data in recursive order
	virtual int  printDataInOrder(VitisObject * vobj, int idBearer);

	virtual int printSubStructure(class GeomBranc * gb);

};



#endif

