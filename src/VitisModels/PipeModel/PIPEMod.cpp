#include "PIPEMod.h"
#include "BrancPIPE.h"
#include "GeomBrancPIPE.h"
#include "InterNodePIPE.h"
#include "PlantPIPE.h"
#include "DebugNew.h"

PIPEMod::PIPEMod(Plant * plt):AmapSimModele(plt)
{
}

PIPEMod::~PIPEMod(void)
{
}


GeomBranc * PIPEMod::createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr)
{
	return new GeomBrancPIPE(brc,gbr,v_plt);

}

DecompAxeLevel * PIPEMod::createInstanceDecompAxe (USint level)
{
	switch(level)
	{
		case INTERNODE : 
			return new InterNodePIPE (v_plt); 
		break;
		case AXE :
			return new BrancPIPE (v_plt); 
		break;
	}
	return AmapSimModele::createInstanceDecompAxe(level);
}

#if 0
Bud * PIPEMod::createInstanceBud (class BrancAMAP * br, char guRam, double timeC, long phyAgeBeg, double ryt)
{

	return new BudPIPE(br,guRam,timeC,phyAgeBeg, ryt);

}

GeomElem * PIPEMod::createInstanceGeomElem ()
{
	return new GeomElemPIPE ();
}

#endif



