// BrancPIPE.cpp: implementation of the BrancPIPE class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include "InterNodePIPE.h"
#include "BrancPIPE.h"
#include "AxisReference.h"
#include "PlantAMAP.h"
#include "Bud.h"


BrancPIPE::BrancPIPE(Plant * plt):BrancAMAP(plt)
{
}

BrancPIPE::BrancPIPE(BrancPIPE * b):BrancAMAP(b)
{
}

BrancPIPE::~BrancPIPE()
{
}

float BrancPIPE::updateDiameter()
{	
	long  i, j, nbleaves;
	bool terminal, borneBranc;
	float previousSection, borneSection, lngPrev = 0;;
	int level, localInverseOrder=1, maxInverseOrder, nbBorneBranc;;
	int pos=0; 
	BrancPIPE *borne;
	InterNodePIPE *entn, *borneEntn;
	Hierarc *h=getCurrentHierarc();

	nbleaves = 0;
	terminal = true;
	previousSection = 0;

	/* resets buds datas */
	startPosit();

	level = V_plt()->getPlantFactory()->getLevelOfDecomposition();
	if(!positOnFirstElementOfSubLevel(level))
	{	
		endPosit();
		return -1;
	}

	int nbUC, indexUC;

	nbUC = 0;
	indexUC = -1;
	lngPrev = 0;
	do
	{
		entn = (InterNodePIPE*)getCurrentElementOfSubLevel(level);
		if (indexUC != getCurrentGuIndex())
		{
			nbUC ++;
			indexUC = getCurrentGuIndex();
		}
		lngPrev += computeLength();
		entn->lngSegment = lngPrev;
		entn->nbUC = nbUC;

		for (j=0; j<h->getNbBorne(); j++)
		{
			borne = (BrancPIPE*)h->getBorne(j);
			if (   borne->CurrentHierarcPtr()->indexOnBearerAtLowestLevel == pos
				&& borne->nature != 2
				&& borne->getCurrentEntnNumberWithinBranch() > 0)
			{
				lngPrev = 0;
				nbUC = 0;
				indexUC = -1;
				break;
			}
		}

	} while(positOnNextElementOfSubLevel(level));
	lngPrev = entn->lngSegment;

	positOnLastElementOfSubLevel(level);
	/* scan each node of the branch */
	do
	{
		entn = (InterNodePIPE*)getCurrentElementOfSubLevel(level);
		pos = getCurrentElementIndexOfSubLevel(level);
		borneSection = 0;

		borneBranc = false;
		nbBorneBranc = 0;
		maxInverseOrder = 0;
		for (j=0; j<h->getNbBorne(); j++)
		{
			borne = (BrancPIPE*)h->getBorne(j);
			if (borne->CurrentHierarcPtr()->indexOnBearerAtLowestLevel == pos)
			{
				/* if it is a leaf */
				if (borne->nature == 2)
					nbleaves ++;
				else if (borne->getCurrentEntnNumberWithinBranch() > 0)
				{
					terminal = false;
					borneBranc = true;
					nbBorneBranc ++;

					float section = borne->updateDiameter();
					borneSection += section;
					entn->updateBorneDiameter(borne->V_idBranc(), section, borne->theBud->death);

					borne->startPosit();
					borne->positOnFirstElementOfSubLevel(level);
					borneEntn = (InterNodePIPE*)(borne->getCurrentEntn());
					if (maxInverseOrder < borneEntn->getInverseOrder())
						maxInverseOrder = borneEntn->getInverseOrder();
					borne->endPosit();
				}
			}
		}

		if (borneBranc)
		{
			localInverseOrder ++;
			lngPrev = 0;
			nbUC = entn->nbUC;
			lngPrev = entn->lngSegment;
		}
		else
		{
			entn->lngSegment = lngPrev;
			entn->nbUC = nbUC;
		}

		if (localInverseOrder < maxInverseOrder)
			localInverseOrder = maxInverseOrder;

		entn->set (nbleaves, terminal, localInverseOrder);

		entn->computeDiameter (h->ordre, previousSection, borneSection, nbBorneBranc, entn->inverseOrder, entn->lngSegment);

		previousSection = entn->section();

	}while(positOnPreviousElementOfSubLevel(level));

	endPosit();

	return previousSection;
}

/* compute current length */
float BrancPIPE::computeLength ()
{
	float l, indexFromEnd, aux, scale;
	long  currentPosForLng;
	long phyAge, posElem, pos;

    AxisReference *axisref = ((PlantAMAP *)getPlant())->getAxisReference();

	/* position along the branch from theoritical end */
	indexFromEnd = getTestNumberWithinBranch() - getTestIndexWithinBranch()-1;

	if (this == getBearer())
	{
		l = (*axisref)[gllnginit].ymin;
	}
	else
	{
		phyAge = getCurrentGu()->phyAgeGu - 1;
		l = axisref->val1DParameter (lnginit, phyAge, this);
	}

	phyAge = getCurrentGu()->phyAgeGu - 1;
	/* computes toplogical position */
	currentPosForLng = calcPosElem (
			(long)axisref->val1DParameter (rgsensgeom, phyAge, this), 
			(long)axisref->val1DParameter (rtbasecompt, phyAge, this), 
			(long)axisref->val1DParameter (rttypetop, phyAge, this));


	/* length adjustment according to nodes age */
	/* max length is reached */
	if (indexFromEnd >= axisref->val1DParameter (renbtestlng, phyAge, this) + axisref->val1DParameter (relaglng, phyAge, this))
	{
		l *= axisref->val1DParameter (reevollng, phyAge, this);	     
	}
	/* node is older than the lag period, it is growing */
	else if (indexFromEnd >= axisref->val1DParameter (relaglng, phyAge, this))
	{
		aux = (float)(indexFromEnd - axisref->val1DParameter (relaglng, phyAge, this)) / axisref->val1DParameter (renbtestlng, phyAge, this);
		l *= 1. + aux * (axisref->val1DParameter (reevollng, phyAge, this)-1.);
	}

	/* length adjustment according to topological position */
	l *= axisref->val2DParameter (rgpcntlng, phyAge, currentPosForLng, this);


	return l;
}



