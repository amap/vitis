#include <math.h>
#include "Plant.h"
#include "InterNodePIPE.h"
#include "std_serialize.h"
#include "paramPipe.h"

InterNodePIPE::InterNodePIPE(Plant * plt):InterNodeAMAP(plt)
{
	terminal = true;
	nbleaves = 0; /* number of leaves above that element */
	diameter = 0;
	inverseOrder = 1;
	lngSegment = 0;
	nbUC = 1;
}

InterNodePIPE::~InterNodePIPE()
{
}

void InterNodePIPE::set (int nb, bool t, int inv)
{
	nbleaves = nb;
	terminal = t;
	inverseOrder = inv;
}

float InterNodePIPE::computeDiameter (int order, float previousSection, float borneSection, int nbBorne, int invOrder, float lngSeg)
{
	int os=order, ois=invOrder-1;
	float section, diam;

    if (os>3)  //ordre simplifi�, de 0 � 3
        os=3;

    if (ois>3)  //ordre inverse simplifi� de 0 � 3
        ois=3;

	if (ois==0)  //modele de feuille pour les branches terminales
        section = kf*nbleaves + klongF*lngSeg + kosF[os] + cstF;
	else
    {
        if (nbBorne==0)  //conicit� des axes
        {
        //            section= previousSection + (kconos[os]+cstCon)*lngSeg;
            section= previousSection+ M_PI*( pow(lngSeg*(cstCon+kconos[os]),2) + 2.*lngSeg*(cstCon+kconos[os])*sqrt(previousSection/M_PI) );
 //           std::cout << section<< " "<<ois <<"  -" << nbBorne ;
        }
       else  //modele d'embranchement pour les ramifications
            section = kemb*(previousSection+borneSection) + knemb*(nbBorne+1) + kois[ois] + (klongE+kol[os])*lngSeg + kosE[os] + cstE;
    }
    if (section<0)  //pour ne pas avoir de section n�gatives
        section=0;

//	if (borneData.empty())
//		std::cout << "entn sans ramif " <<std::endl;;

	BorneData::const_iterator p;
	std::cout<<borneData.size()<<" ramifs"<<std::endl;
	for (p=borneData.begin(); p!=borneData.end(); p++)
	{
		BorneFeature f = p->second;
	    std::cout<<"         branch "<<p->first<<" diam "<<f.diam<<" death "<<(int)f.death<<std::endl;
/*
		BorneFeature f = p->second;
		if (f.death)
			;
		else
			f.diam;
*/
	}


	diam = 2. * sqrt (section / M_PI);


	if (diameter < diam)  //crit�re historique, les axes ne retressissent pas
		diameter = diam;

	return diameter;
}


float InterNodePIPE::section ()
{
	return (M_PI / 4. * diameter * diameter);
}

void InterNodePIPE::serialize(Archive& ar )
{

	InterNodeAMAP::serialize(ar);

	if(ar.isWriting() )
	{
		ar<<terminal<<nbleaves<<diameter;

	}
	else
	{
		ar>>terminal>>nbleaves<<diameter;
	}


}

void InterNodePIPE::updateBorneDiameter (int ind, float section, char death)
{
	float diam = 2. * sqrt (section / M_PI);
	BorneData::const_iterator p;

	p=borneData.find(ind);
	BorneFeature f;
	f.death = death;
	f.diam = diam;

	if (p == borneData.end())
		borneData[ind]= f;
	else
	{
		BorneFeature bf = p->second;
		if (bf.diam < diam)
			f.diam = diam;
		if (death)
			f.death = death;

		borneData[ind]= f;
	}

}


