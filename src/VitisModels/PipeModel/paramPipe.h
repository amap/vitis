/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//mettre ici la declaration et la valeur des parametres du modele pipe
//genre int toto=8;
//ou bien float tata=3.14;

        //param�tres des mod�les de feuilles pour les terminaux
//	float kf=0.001225686, cstF=0, klongF=0, kosF[4]={0,0,0,0};
//	float kf=0.0008385083, cstF=0.1045637, klongF=0, kosF[4]={0,0,0,0};
	float kf=0.0007761973, cstF=0.1891597, klongF=0, kosF[4]={0,0,-0.06924485,-0.1116792};
//	float kf=0.0005812562, cstF=0.1160595, klongF=0.01264461, kosF[4]={0,0,-0.03445476,-0.05776906};
        //param�tres de ramification pour les embranchements
//	float kemb=1, cstE=0, knemb=0, kois[4]={0,0,0,0}, klongE=0, kol[4]={0,0,0,0}, kosE[4]={0,0,0,0};
//	float kemb=0.9761502, cstE=0, knemb=0, kois[4]={0,0,0,0}, klongE=0, kol[4]={0,0,0,0}, kosE[4]={0,0,0,0};
	float kemb=0.9789794, cstE=-0.2018655, knemb=0, kois[4]={0,0,0,0}, klongE=0, kol[4]={0,0,0,0}, kosE[4]={0,0,0,0};
//	float kemb=1.001318, cstE=1.461601, knemb=-0.5895848, kois[4]={0,0,-0.6280807,-8.32092}, klongE=0, kol[4]={0,0,0,0}, kosE[4]={0,0,0,0};
//	float kemb=1.011802, cstE=-2.826427, knemb=-0.5412701, kois[4]={0,0,0.08413925,-7.9917}, klongE=0.1113305, kol[4]={0,-0.0722763,-0.08921064,-0.04206791}, kosE[4]={0,3.721516,3.768937,3.266899};
        //param�tre de conicit�, correspond � tan(theta) o� theta est l'angle de conicit�
    float kconos[4]={0,0,0,0}, cstCon=0;
//    float kconos[4]={0,0,0,0}, cstCon=0.00005;
//    float kconos[4]={0,-0.00002862,-0.00007131,-0.00009993}, cstCon=0.00011232;
        //param�tre de recyclage
    float recycle=0.1;
