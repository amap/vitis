#ifndef _BRANCPIPE_H__
#define _BRANCPIPE_H__

#include "BrancAMAP.h"
#include "externPipe.h"

class PIPE_EXPORT BrancPIPE : public BrancAMAP
{
	public :
	
	BrancPIPE(class Plant *plt);
	BrancPIPE(BrancPIPE *b);
	virtual ~BrancPIPE();
	float updateDiameter();
	float computeLength();
};

#endif
