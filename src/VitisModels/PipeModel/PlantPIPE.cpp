#include "PlantPIPE.h"
#include "BrancPIPE.h"
#include "InterNodePIPE.h"
#include "PIPEMod.h"



PlantPIPE::PlantPIPE()
{
}

PlantPIPE::PlantPIPE(void *p)
{
}

PlantPIPE::PlantPIPE(const std::string & nameT, const std::string & nameP):PlantAMAP(nameT,nameP)
{
}

PlantPIPE::~PlantPIPE(void)
{
	delete v_topoMan;

	v_topoMan=NULL;
}


PlantBuilder * PlantPIPE::instanciatePlantBuilder()
{
	return new PIPEMod (this);
}


void PlantPIPE::computeGeometry()
{
	((BrancPIPE*)(getTopology().getTrunc()->getBranc()))->updateDiameter();

	Plant::computeGeometry();
}


extern "C" Plant * StartPlugin (void *p)
{
	return new PlantPIPE(p);
}

