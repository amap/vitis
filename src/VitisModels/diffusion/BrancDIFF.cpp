// BrancPIPE.cpp: implementation of the BrancPIPE class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include "InterNodePIPE.h"
#include "BrancPIPE.h"
#include "AxisReference.h"
#include "PlantAMAP.h"
#include "Bud.h"


BrancPIPE::BrancPIPE(Plant * plt):BrancAMAP(plt)
{
}

BrancPIPE::BrancPIPE(BrancPIPE * b):BrancAMAP(b)
{
}

BrancPIPE::~BrancPIPE()
{
}

float BrancPIPE::updateDiameter()
{	
	long  i, j, nbleaves;
	bool terminal, borneBranc;
	float previousSection, borneSection, lngPrev = 0;;
	int level, localInverseOrder=1, maxInverseOrder, nbBorneBranc;;
	int pos; 
	BrancPIPE *borne;
	InterNodePIPE *entn, *borneEntn;
	Hierarc *h=getCurrentHierarc();

	nbleaves = 0;
	terminal = true;
	previousSection = 0;

	/* resets buds datas */
	startPosit();

	if(!positOnFirstElementOfSubLevel(v_plt->getPlantFactory()->getLevelOfDecomposition()))
	{	
		endPosit();
		return -1;
	}

	level = v_plt->getPlantFactory()->getLevelOfDecomposition();

	lngPrev = 0;
	do
	{
		entn = (InterNodePIPE*)getCurrentElementOfSubLevel(level);
		lngPrev += computeLength();
		entn->lngSegment = lngPrev;

		for (j=0; j<h->getNbBorne(); j++)
		{
			borne = (BrancPIPE*)h->getBorne(j);
			if (   borne->currentHierarcPtr->indexOnBearerAtLowestLevel == pos
				&& borne->nature != 2
				&& borne->getCurrentEntnNumberWithinBranch() > 0)
			{
				lngPrev = 0;
				break;
			}
		}

	} while(positOnNextElementOfSubLevel(level));
	lngPrev = entn->lngSegment;

	/* scan each node of the branch */
	do
	{
		entn = (InterNodePIPE*)getCurrentElementOfSubLevel(level);
		pos = getCurrentElementIndexOfSubLevel(level);
		borneSection = 0;

		borneBranc = false;
		nbBorneBranc = 0;
		maxInverseOrder = 0;
		for (j=0; j<h->getNbBorne(); j++)
		{
			borne = (BrancPIPE*)h->getBorne(j);
			if (borne->currentHierarcPtr->indexOnBearerAtLowestLevel == pos)
			{
				/* if it is a leaf */
				if (borne->nature == 2)
					nbleaves ++;
				else if (borne->getCurrentEntnNumberWithinBranch() > 0)
				{
					terminal = false;
					borneBranc = true;
					nbBorneBranc ++;

					borneSection += borne->updateDiameter();

					borne->startPosit();
					borne->positOnFirstElementOfSubLevel(level);
					borneEntn = (InterNodePIPE*)(borne->getCurrentEntn());
					if (maxInverseOrder < borneEntn->getInverseOrder())
						maxInverseOrder = borneEntn->getInverseOrder();
					borne->endPosit();
				}
			}
		}

		if (borneBranc)
		{
			localInverseOrder ++;
			lngPrev = 0;
			lngPrev = entn->lngSegment;
		}
		else
			entn->lngSegment = lngPrev;

		if (localInverseOrder < maxInverseOrder)
			localInverseOrder = maxInverseOrder;

		entn->set (nbleaves, terminal, localInverseOrder);

		entn->computeDiameter (h->ordre, previousSection, borneSection, nbBorneBranc, entn->inverseOrder, entn->lngSegment);

		previousSection = entn->section();

	}while(positOnPreviousElementOfSubLevel(level));

	endPosit();

	return previousSection;
}

/* compute current length */
float BrancPIPE::computeLength ()
{
	float l, indexFromEnd, aux, scale;
	long  currentPosForLng;
	long phyAge, posElem, pos;

    AxisReference *axisref = ((PlantAMAP *)getPlant())->getAxisReference();

	/* position along the branch from theoritical end */
	indexFromEnd = testNumberWithinBranch - getTestIndexWithinBranch()-1;

	if (this != getBearer())
	{
		pos = axisref->getNbLPAxe()*getBearer()->getCurrentZoneIndex()+lpIndex;
		phyAge = getBearer()->getCurrentGu()->phyAgeGu - 1;
	}
	else
	{
		pos = 0;
		phyAge = (int)(*axisref)[glPhyAgeune].ymin - 1;
	}
	if (   getBud()->branchRamificationType == RETARDE1
			|| getBud()->branchRamificationType == RETARDE2)
		posElem = getBearer()->calcPosElem ((long)axisref->val1DParameter (NB_VAR+pos*NB_VAR_ENT+resensinitret, phyAge, this), 
				(long)axisref->val1DParameter (rtbasecompt, phyAge, this), 
				(long)axisref->val1DParameter (rttypetop, phyAge, this));
	else if (   getBud()->branchRamificationType == TRAUMAANT
			|| getBud()->branchRamificationType == TRAUMARET)
		posElem = getBearer()->calcPosElem ((long)axisref->val1DParameter (NB_VAR+pos*NB_VAR_ENT+resenstrau, phyAge, this), 
				(long)axisref->val1DParameter (rtbasecompt, phyAge, this), 
				(long)axisref->val1DParameter (rttypetop, phyAge, this));
	else
		posElem = getBearer()->calcPosElem ((long)axisref->val1DParameter (NB_VAR+pos*NB_VAR_ENT+resensinitant, phyAge, this), 
				(long)axisref->val1DParameter (rtbasecompt, phyAge, this), 
				(long)axisref->val1DParameter (rttypetop, phyAge, this));

	if (this == getBearer())
	{
		l = (*axisref)[gllnginit].ymin;
	}
	else if (getBud()->branchRamificationType == RETARDE1)
	{
		l = axisref->val2DParameter (NB_VAR+pos*NB_VAR_ENT+rclnginitret1, phyAge, posElem, getBearer());
	}
	else if (getBud()->branchRamificationType == RETARDE2)
	{
		l = axisref->val2DParameter (NB_VAR+pos*NB_VAR_ENT+rclnginitret2, phyAge, posElem, getBearer());
	}
	else if (   getBud()->branchRamificationType == TRAUMAANT
			|| getBud()->branchRamificationType == TRAUMARET)
	{
		l = axisref->val2DParameter (NB_VAR+pos*NB_VAR_ENT+rclnginittrau, phyAge, posElem, getBearer());
	}
	else if (getBud()->branchRamificationType == ANTICIPE)
	{
		l = axisref->val2DParameter (NB_VAR+pos*NB_VAR_ENT+rclnginitant, phyAge, posElem, getBearer());
	}
	else
	{
		l = axisref->val2DParameter (NB_VAR+pos*NB_VAR_ENT+rclnginitretc, phyAge, posElem, getBearer());
	}

	phyAge = getCurrentGu()->phyAgeGu - 1;
	/* computes toplogical position */
	currentPosForLng = calcPosElem (
			(long)axisref->val1DParameter (rgsensgeom, phyAge, this), 
			(long)axisref->val1DParameter (rtbasecompt, phyAge, this), 
			(long)axisref->val1DParameter (rttypetop, phyAge, this));


	/* length adjustment according to nodes age */
	/* max length is reached */
	if (indexFromEnd >= axisref->val1DParameter (renbtestlng, phyAge, this) + axisref->val1DParameter (relaglng, phyAge, this))
	{
		l *= axisref->val1DParameter (reevollng, phyAge, this);	     
	}
	/* node is older than the lag period, it is growing */
	else if (indexFromEnd >= axisref->val1DParameter (relaglng, phyAge, this))
	{
		aux = (float)(indexFromEnd - axisref->val1DParameter (relaglng, phyAge, this)) / axisref->val1DParameter (renbtestlng, phyAge, this);
		l *= 1. + aux * (axisref->val1DParameter (reevollng, phyAge, this)-1.);
	}

	/* length adjustment according to topological position */
	l *= axisref->val2DParameter (rgpcntlng, phyAge, currentPosForLng, this);


	return l;
}



