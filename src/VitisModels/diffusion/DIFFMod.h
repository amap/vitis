///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file PIPEMod.h																					
///			\brief Definition of PIPEMod class.																	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __PIPEMOD_H__
#define __PIPEMOD_H__

#include "AmapSimMod.h"
#include "externPipe.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class PIPEMod																				
///			\brief AMAPSIM+pipeModel Edification plant model	(based on Axis reference )
///	
///			This simulation model specialized the AMAPSIM model by adding the internodes diameter according to pi[pe model
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class PIPE_EXPORT PIPEMod : public AmapSimModele
{
public:
	
	/// \brief Default constructor
	PIPEMod(class Plant * plt);
	
	/// \brief Destructor
	~PIPEMod(void);
	


	/// \brief Create a new geomtrical branch PIPE
	/// @param brc The topological bearer
	/// @param gbr The geometrical bearer
	/// @param gt The geometrical manager
	///	@return An instance of GeomBrancPIPE
	virtual GeomBranc * createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr);

	/// \brief Create a new instance of an element of decomposition 
	///
	///	One level is specialized in this modele , level 5 (INTERNODE)
	/// @param level The level of the element to instanciate
	///	@return An instance of DecompAxeLevel, or GrowthUnitAMAP (if level==1), or InterNodePIPE (if level==5)
	virtual DecompAxeLevel * createInstanceDecompAxe (USint level);
	
#if 0
	/// \brief Create a new BudPIPE
	/// @param br The branc associated to the Bud
	/// @param guRam growth unit ramification type : ANTICIPE, RETARDE1, RETARDE2, TRAUMAANT, TRAUMARET, RETARDECYC, NONBRANCHETRAUMAANT, NONBRANCHEANT, NONBRANCHERETCYC, NONBRANCHETRAUMARET
	/// @param timeC Start time of the new bud
	/// @param phyAgeBeg PhyAge of the new bud
	/// @param ryt rythme of the new Bud
	///	@return An instance of BudPIPE
	virtual Bud * createInstanceBud (class BrancAMAP * br, char guRam, double timeC, long phyAgeBeg,double ryt);
	
	/// \brief Create a new geometrical element 
	///	@return An instance of GeomElemPIPE
	virtual GeomElem * createInstanceGeomElem ();
#endif

};



#endif
