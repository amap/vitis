///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file  InterNodePIPE.h																					
///			\brief Definition of InterNodePIPE class.																	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __INTERNODEPIPE_H__
#define __INTERNODEPIPE_H__

#include "InterNodeAMAP.h"
#include "externDll.h"
#include "externPipe.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class InterNodePIPE																				
///			\brief Specialization of InterNodeAMAP by adding of PIPE data
///	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class PIPE_EXPORT InterNodePIPE : public InterNodeAMAP
{
public:

	
	bool terminal; /**< does this internode belong to the terminal part of the branch */

	int inverseOrder;
	
	int nbleaves; /**< Number of leaves above that element */

	float diameter;

	float lngSegment;

	/// \brief Constructor
	InterNodePIPE(class Plant * plt);

	/// \brief Destructor
	virtual ~InterNodePIPE(void);
	
	void set (int nb, bool t, int inv);

	float computeDiameter (int order, float previousSection, float borneSection, int nbBorne, int invOrder, float lngSeg);

	float section ();
	int getInverseOrder(){return inverseOrder;};

	/// \brief Serialize a InterNodePIPE
	/// \param ar A reference to the archive \see Archive 
	virtual void serialize(Archive& ar );
};

#endif
