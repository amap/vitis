///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file  PlantPIPE.h																					
///			\brief Definition of PlantPIPE class.																	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __PLANTPIPE_H__
#define __PLANTPIPE_H__
#include "PlantAMAP.h"
#include "scheduler.h"
#include "externPipe.h"




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class PlantPIPE																				
///			\brief GreenLab V1 describtion for a plant , specialize PlantAMAP
///
///			The general sketch is :
///					- production (matter production)
///				 	- growth (organogenesys)
///				 	- allocation (volume increment)
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class PIPE_EXPORT PlantPIPE : public PlantAMAP
{
	public:


	 /// \brief Default Constructor
	 PlantPIPE();
	
	/// \brief Constructor
	 PlantPIPE(const std::string & nameT, const std::string & nameP);
	
	/// \brief Desstructor
	 ~PlantPIPE(void);

	virtual PlantBuilder * instanciatePlantBuilder();

	virtual void computeGeometry();

};

/// \return A instance of PlantPIPE
extern "C" PIPE_EXPORT  Plant * StartPlugin (void);


#endif
