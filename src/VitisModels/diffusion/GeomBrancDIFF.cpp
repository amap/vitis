#include "Plant.h"
#include "GeomBrancPIPE.h"
#include "InterNodePIPE.h"

GeomBrancPIPE::GeomBrancPIPE(Hierarc * brc, GeomBranc * gbr, Plant * gt):GeomBrancAMAP(brc,gbr,gt)
{
}

GeomBrancPIPE::~GeomBrancPIPE(void)
{
}

float GeomBrancPIPE::computeDiamUp ()
{
	float d;
	BrancAMAP * b =(BrancAMAP *)this->getBranc();
	
	if (b->nature == 2)
		d = GeomBrancAMAP::computeDiamUp();
	else
		d = ((InterNodePIPE *)(b->getCurrentEntn()))->diameter;

	return d;
}



