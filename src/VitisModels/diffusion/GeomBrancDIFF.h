///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file GeomBrancPIPE.h																					
///			\brief Definition of GeomElemPIPE and GeomBrancPIPE class.																	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __GEOMBRANCPIPE_H__
#define __GEOMBRANCPIPE_H__

#include "GeomBrancAMAP.h"
#include "GeomElemCone.h"
#include "externPipe.h"




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class GeomBrancPIPE																				
///			\brief Specialization of GeomBrancAMAP by adding of method to compute Diameter and length according allocated matter
///				
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GeomBrancPIPE : public GeomBrancAMAP
{
public:
	
	GeomBrancPIPE(Hierarc * brc, GeomBranc * gbr, class Plant * gt=NULL);

	virtual ~GeomBrancPIPE(void);
	
	/// \brief Specialized the standard function AMAPSIM computing the internode's diameter
	/// \return values : \li 0 : nothing was done \li 1 : a new diameter was computed
	virtual float computeDiamUp();

};

#endif
