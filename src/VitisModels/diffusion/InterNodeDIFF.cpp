#include <math.h>
#include "Plant.h"
#include "InterNodePIPE.h"
#include "std_serialize.h"

InterNodePIPE::InterNodePIPE(Plant * plt):InterNodeAMAP(plt)
{	
	terminal = true;
	nbleaves = 0; /* number of leaves above that element */
	diameter = 0;
	inverseOrder = 1;
	lngSegment = 0;
}

InterNodePIPE::~InterNodePIPE()
{	
}

void InterNodePIPE::set (int nb, bool t, int inv)
{
	nbleaves = nb;
	terminal = t;
	inverseOrder = inv;
}

float InterNodePIPE::computeDiameter (int order, float previousSection, float borneSection, int nbBorne, int invOrder, float lngSeg)
{
	float section, diam;
	float k1[7]={1,1,1,1,1,1,1}, k2[7]={1,1,1,1,1,1,1}, k3[7]={1,1,1,1,1,1,1};

	
	if (terminal)
		section = k1[order] * nbleaves;
	else
		section = previousSection + k2[order] * borneSection + k3[order];

	diam = 2. * sqrt (section / M_PI);

	if (diameter < diam)
		diam = diameter;

	return diameter;
}

float InterNodePIPE::section ()
{
	return (M_PI / 4. * diameter * diameter);
}

void InterNodePIPE::serialize(Archive& ar )
{		

	InterNodeAMAP::serialize(ar);

	if(ar.isWriting() )
	{   
		ar<<terminal<<nbleaves<<diameter;

	}
	else
	{
		ar>>terminal>>nbleaves<<diameter;
	}


}
