#include "PlantHydraulic.h"
#include "BrancAMAP.h"
#include "BudHYDRO.h"
#include "InterNodeHYDRO.h"
#include "UtilMath.h"
#include "HydraulicMod.h"
#include "DebugNew.h"



PlantHYDROSANSLUM::PlantHYDROSANSLUM()
{
	totalMatter=0;	
	glMaxRef=0;
	pithSink=NULL;
	layerSink=NULL;
	petioleSink=NULL;
	bladeSink=NULL;
	fruitSink=NULL;
	param_H=NULL;
	waterConsumption=0;
	totalResistivity=0;
	lastMatterTime=0;

	cumultotalOrganMatter=0;
	cumultotalBladeMatter=0;
	cumultotalPetioleMatter=0; 
	cumultotalPithMatter=0;
	cumultotalLayerMatter=0;
	cumultotalFruitMatter=0;
	totalOrganMatter=0;
	totalBladeMatter=0;
	totalPetioleMatter=0;
	totalPithMatter=0;
	totalLayerMatter=0;
	totalFruitMatter=0;
}

PlantHYDROSANSLUM::PlantHYDROSANSLUM(const std::string & nameT, const std::string & nameP):PlantAMAP(nameT,nameP)
{
	totalMatter=0;	
	glMaxRef=0;
	pithSink=NULL;
	layerSink=NULL;
	petioleSink=NULL;
	bladeSink=NULL;
	fruitSink=NULL;
	param_H=NULL;
	waterConsumption=0;
	totalResistivity=0;
	lastMatterTime=0;

	cumultotalOrganMatter=0;
	cumultotalBladeMatter=0;
	cumultotalPetioleMatter=0; 
	cumultotalPithMatter=0;
	cumultotalLayerMatter=0;
	cumultotalFruitMatter=0;
	totalOrganMatter=0;
	totalBladeMatter=0;
	totalPetioleMatter=0;
	totalPithMatter=0;
	totalLayerMatter=0;
	totalFruitMatter=0;


}

PlantHYDROSANSLUM::~PlantHYDROSANSLUM(void)
{
	delete v_topoMan;

	v_topoMan=NULL;

	if(pithSink!=NULL)
	{
		for(int i=0; i<glMaxRef; i++)
		{
			delete [] pithSink[i];
			delete [] layerSink[i];
			delete [] petioleSink[i];
			delete [] bladeSink[i];
			delete [] fruitSink[i];
		}

		delete [] pithSink;
		delete [] layerSink;
		delete [] petioleSink;
		delete [] bladeSink;
		delete [] fruitSink;
	}


	delete param_H;
}


void PlantHYDROSANSLUM::run (float timeStart, float timeStop)
{
	getScheduler().create_process((VProcess *)this,NULL,getScheduler().getHotStop(),2);

	PlantAMAP::run(timeStart,timeStop);

}


void PlantHYDROSANSLUM::seed (float time)
{
	PlantAMAP::seed(time);



	getScheduler().create_process((VProcess *)this,new EventEndCycle (),0,2);

	/*this->setState(HYDROSANSLUMSTART);
	  this->notify();	*/

	/* remove output target */



}

void PlantHYDROSANSLUM::init()
{
	PlantAMAP::init();

	std::string paramHydro = this->getParamName();
	size_t ext_pos=paramHydro.find_last_of( '1' );
	std::string pathHydro = paramHydro.substr(0,ext_pos);
	paramHydro=paramHydro.substr(ext_pos+1,paramHydro.size());
	paramHydro=pathHydro+paramHydro+".hyd";


	param_H= new ParamHYDROSANSLUM(paramHydro);

	param_H->readParam();

	glMaxRef=(float)(int)(*getAxisReference())[glmaxref].ymin;

	pithSink = normalizeExpansion ((int)(*getAxisReference())[glmaxref].ymin, param_H->getGrowthTime(), param_H->getPithSinkAgingN(), param_H->getPithSinkAgingP());

	layerSink = normalizeExpansion ((int)(*getAxisReference())[glmaxref].ymin, param_H->getGrowthTime(), param_H->getLayerSinkAgingN(), param_H->getLayerSinkAgingP());

	petioleSink = normalizeExpansion ((int)(*getAxisReference())[glmaxref].ymin, param_H->getGrowthTime(), param_H->getPetioleSinkAgingN(), param_H->getPetioleSinkAgingP());

	bladeSink = normalizeExpansion ((int)(*getAxisReference())[glmaxref].ymin, param_H->getGrowthTime(), param_H->getBladeSinkAgingN(), param_H->getBladeSinkAgingP());

	fruitSink = normalizeExpansion ((int)(*getAxisReference())[glmaxref].ymin, param_H->getGrowthTime(), param_H->getFruitSinkAgingN(), param_H->getFruitSinkAgingP());



}


PlantBuilder * PlantHYDROSANSLUM::instanciatePlantBuilder()
{
	return new HydraulicMod (this);
}


void PlantHYDROSANSLUM::computeGeometry()
{
	/* computes demand and split matter between the existing organs */
	double totalDemand = computeDemand (getScheduler().getTopClock());

	computeVolume (getScheduler().getTopClock(), totalDemand);

	Plant::computeGeometry();

}

void PlantHYDROSANSLUM::process_event(const EventData * msg)
{
	double totalDemand;

	float top_extern;

	std::cout<<"extern calling at"<<getScheduler().getTopClock();

	computeMatterCurrentTime();

	if(msg!=NULL)//si on est � la fin d'un cycle
	{

		/* sets the next external eventData */
		top_extern =getScheduler().getTopClock();   

		/* computes demand and split matter between the existing organs */
		totalDemand = computeDemand (top_extern);

		computeVolume (top_extern, totalDemand);

		/* computes resistivity for the next cycle */
		totalResistivity=computeResistivity (top_extern);

		if (top_extern+1 <= getScheduler().getHotStop())
		{		
			getScheduler().self_signal_event((EventData*)msg,top_extern+1,2);
		}
	}


	return;

}

void PlantHYDROSANSLUM::computeMatterCurrentTime () 
{
	double timeFraction=getScheduler().getTopClock()-lastMatterTime; /// Fraction cycle to compute
	double matter=0;

	/* first add the matter coming from seed */
	if (getScheduler().getTopClock() < param_H->getSeedTime())
		totalMatter +=  param_H->getSeedVolume() /  param_H->getSeedTime();

	// if the matter computing has been already done for this time , don't compute again
	if(timeFraction != 0 )
	{			

		//GOOD 
		//waterConsumption=Climate * totalResistivity * timeFraction;

		//BAD
		waterConsumption=param_H->getClimateParam() * totalResistivity * timeFraction;


		totalMatter+=waterConsumption;

		lastMatterTime=getScheduler().getTopClock();
	}

}

double ** PlantHYDROSANSLUM::normalizeExpansion (int phySz, AmapSimVariable & GrowthTime, AmapSimVariable & SinkAgingN, AmapSimVariable & SinkAgingP)
{
	int i,j,t;
	double **exp, s, x, n, p;

	exp = new double * [phySz];
	for (i=0; i<phySz; i++)
	{
		s = 0;
		t = param_H->val_axe_complement (GrowthTime, i);
		exp[i] = new double [t];
		for (j=0,x=0; j<t; j++,x+=1.)
		{
			n = param_H->val_axe_complement (SinkAgingN, j);
			p = param_H->val_axe_complement (SinkAgingP, j);
			exp[i][j] = pow ((x+.5)/t,(1+n)*p-1.) * pow (1.-(x+.5)/t, (1.+n)*(1.-p)-1.) / t;
			s += exp[i][j];
		}
		for (j=0; j<t; j++)
			exp[i][j] /= s;
	}
	return exp;
}






double PlantHYDROSANSLUM::computeResistivity (float top)
{
	double matter, leafResistivity, resistance;

	int  phyAge, fTime, topAge;

	BrancAMAP *b;

	GrowthUnitAMAP *Gu;

	int nb=0;


	/* compute climate */
	//	climate = getTempOfDay (sowYear, sowDay+top);

	//add SG DEMO
	//Climate=1000.000;

	/* get the global resistivity of the leaf of the forest */
	leafResistivity = 0;

	b = (BrancAMAP *)this->getTopology().seekTree (TopoManager::RESET);

	while (b != NULL)
	{
		b->startPosit();

		if(b->positOnFirstGuWithinBranc())
		{

			do
			{ 
				resistance=0;

				Gu=b->getCurrentGu();

				if(!b->positOnFirstEntnWithinGu())continue;

				phyAge = Gu->phyAgeGu;

				fTime =  param_H->val_axe_complement (param_H->getFunctioningTime(), phyAge-1);

				topAge = top-b->getCurrentEntn()->instant;


				/* is this a blade ? */
				if (   phyAge >= param_H->getMinBlade()
						&& phyAge <= param_H->getMaxBlade()
						&& topAge < fTime)
				{
					nb++;
					resistance += param_H->val_axe_complement(param_H->getBladeResistivity(), phyAge-1)*param_H->getBladeThickness()/((InterNodeHYDROSANSLUM *)(b->getCurrentEntn()))->volume[BLADE];
				}
				/* is this a petiole ? */
				if (   phyAge >= param_H->getMinPetiole()
						&& phyAge <= param_H->getMaxPetiole()
						&& topAge < fTime)
					resistance += param_H->val_axe_complement (param_H->getPetioleResistance(), phyAge-1);

				/* multiply by the number of instances of that leaf 

*/
				//						resistance *= b->totalAxeNumberWithinPlant;

				if (resistance > 0)
				{//SG MODIF
					leafResistivity += (double)b->totalAxeNumberWithinPlant / resistance;
					//leafResistivity += (double)b->hierarcPtrsNumber/resistance;

				}


			}		  
			while(b->positOnNextGuWithinBranc());	
		}//end if

		b->endPosit();



		b =(BrancAMAP *)this->getTopology().seekTree (TopoManager::NEXT);
	}//end while

	return leafResistivity;

}


double PlantHYDROSANSLUM::computeDemand (float top)
{
	double totalDemand = 0, demand;

	int organ, i, j, k, phyAge, gTime, topAge, cpt=0;

	BrancAMAP *b;

	GrowthUnitAMAP *Gu;

	totalDemand = 0;

	b = (BrancAMAP *)this->getTopology().seekTree(TopoManager::RESET);

	while (b != NULL)
	{


		demand = 0;
		organ = 0;
		b->startPosit();
		if(b->positOnFirstGuWithinBranc())
		{
			do
			{
				Gu=b->getCurrentGu();
				if(!b->positOnFirstEntnWithinGu())continue;
				phyAge = Gu->phyAgeGu;
				gTime = param_H->val_axe_complement (param_H->getGrowthTime(), phyAge-1);
				/* is this a blade ? */
				if (   phyAge >= param_H->getMinBlade()
						&& phyAge <= param_H->getMaxBlade())
				{
					organ = 1;
					topAge = top-b->getCurrentEntn()->instant;
					if (topAge < gTime)
					{
						demand += bladeSink[phyAge-1][topAge]*param_H->val_axe_complement (param_H->getBladeMasterSink(), phyAge-1);
					}
				}
				/* is this a petiole ? */
				if (   phyAge >= param_H->getMinPetiole()
						&& phyAge <= param_H->getMaxPetiole())
				{
					organ = 1;
					topAge = top-b->getCurrentEntn()->instant;
					if (topAge < gTime)
					{
						demand += petioleSink[phyAge-1][topAge]*param_H->val_axe_complement (param_H->getPetioleMasterSink(), phyAge-1);
					}
				}
				/* is this a fruit ? */
				if (   phyAge >= param_H->getMinFruit()
						&& phyAge <= param_H->getMaxFruit())
				{
					organ = 1;
					topAge = top-b->getCurrentEntn()->instant-1;
					if (   topAge < gTime
							&& topAge >= 0)
					{
						demand += fruitSink[phyAge-1][topAge]*param_H->val_axe_complement (param_H->getFruitMasterSink(), phyAge-1);
					}
				}
				if (!organ)
				{
					/* add layer sink if axe is older than one and bears functioning leaves */
					if (  getScheduler().getTopClock()> 1
							&& (Gu = ((BudHYDROSANSLUM *)b->getBud())->HasLayer ()) != NULL)
					{
						phyAge = Gu->phyAgeGu;
						demand += param_H->val_axe_complement (param_H->getLayerMasterSink(), phyAge-1);
					}

					/* go to last Gu for the pith */
					b->startPosit();
					b->positOnLastGuWithinBranc();
					Gu = b->getCurrentGu();
					if (b->getCurrentEntnNumberWithinGu() > 0)
					{
						phyAge = Gu->phyAgeGu;
						/* add pith sink(s) I assume that growingTime is given by the last Gu */
						gTime = param_H->val_axe_complement (param_H->getGrowthTime(), phyAge-1);

						b->positOnFirstEntnWithinGu();
						/* computes the age of last Gu (for terminated axes) */
						topAge = top-b->getCurrentEntn()->instant;

						/* add sink from end to bottom of branch */
						for (k=0; k<gTime-topAge && k < b->getGuNumber(); k++)
						{
							b->positOnGuWithinBrancAt(b->getGuNumber()-1-k);
							Gu=b->getCurrentGu();
							phyAge = Gu->phyAgeGu;
							demand += pithSink[phyAge-1][k+topAge]*param_H->val_axe_complement (param_H->getPithMasterSink(), phyAge-1);
						}
					}
					b->endPosit();
					/* no need to seek all the axe, we are only interested into the layer and the pith */
					break;
				}
			}while(b->positOnNextGuWithinBranc());	


			/* multiply by the number of instances of that organ */
			//demand *= b->hierarcPtrsNumber;
			demand *= b->totalAxeNumberWithinPlant;

			totalDemand += demand;

		}//endif
		b->endPosit();


		cpt+=b->totalAxeNumberWithinPlant;

		b = (BrancAMAP *)this->getTopology().seekTree(TopoManager::NEXT);

	}

	return totalDemand;
}


void PlantHYDROSANSLUM::computeVolume (float top, double totalDemand)
{
	double layerMatter=0, organMatter=0, unitMatter=0, pithVolume=0;
	int i, j, k, phyAge, organ, first, seenLeafNumber, totalLeafNumber;
	int topAge, gTime, inLayer;
	BrancAMAP *b;
	GrowthUnitAMAP *Gu, *Gul;

	double pithTotalVol;
	double nbEntnTotal;

	int totalNbLayers = 0;

	totalOrganMatter=0; totalBladeMatter=0; totalPetioleMatter=0; totalPithMatter=0; totalLayerMatter=0; totalFruitMatter=0;
	/* compute the base matter per demand unit */
	if (totalDemand == 0)
		unitMatter = 0;
	else
		unitMatter = totalMatter / totalDemand;

	//	globalAdjustUpperLeavesNumber ();

	//printf ("split %lf matter for global %lf demand\n",totalMatter,totalDemand);
	totalLeafNumber = 0;

	b = (BrancAMAP *)this->getTopology().seekTree(TopoManager::RESET);

	if(b == NULL) return;

	pithTotalVol=0.0;

	nbEntnTotal=0;

	do
	{


		organ = 0;
		first = 1;
		seenLeafNumber = 0;



		b->startPosit();
		if(b->positOnFirstGuWithinBranc())
		{
			do
			{


				Gu=b->getCurrentGu();
				if(!b->positOnFirstEntnWithinGu())continue;


				phyAge = Gu->phyAgeGu;
				gTime = param_H->val_axe_complement (param_H->getGrowthTime(), phyAge-1);
				/* is this a blade ? */
				if (   phyAge >= param_H->getMinBlade()
						&& phyAge <= param_H->getMaxBlade())
				{
					organ = 1;
					topAge = top-b->getCurrentEntn()->instant;
					if (topAge < gTime)
					{
						organMatter = bladeSink[phyAge-1][topAge]*param_H->val_axe_complement (param_H->getBladeMasterSink(), phyAge-1) * unitMatter;
						((InterNodeHYDROSANSLUM *)(b->getCurrentEntn()))->volume[BLADE] += organMatter;


						if (top > 0)
						{
							totalBladeMatter += organMatter*b->totalAxeNumberWithinPlant;
							totalOrganMatter += organMatter*b->totalAxeNumberWithinPlant;

						}
					}
				}
				/* is this a petiole ? */
				if (   phyAge >= param_H->getMinPetiole()
						&& phyAge <= param_H->getMaxPetiole())
				{
					organ = 1;
					topAge = top-b->getCurrentEntn()->instant;
					if (topAge < gTime)
					{
						organMatter = petioleSink[phyAge-1][topAge]*param_H->val_axe_complement (param_H->getPetioleMasterSink(), phyAge-1) * unitMatter;
						((InterNodeHYDROSANSLUM *)(b->getCurrentEntn()))->volume[PETIOLE] += organMatter;
						if (top > 0)
						{
							totalPetioleMatter += organMatter*b->totalAxeNumberWithinPlant;
							totalOrganMatter += organMatter*b->totalAxeNumberWithinPlant;
						}
					}
				}
				/* is this a fruit ? */
				if (   phyAge >= param_H->getMinFruit()
						&& phyAge <= param_H->getMaxFruit())
				{
					organ = 1;
					topAge = top-b->getCurrentEntn()->instant-1;
					if (   topAge < gTime
							&& topAge >= 0)
					{
						organMatter = fruitSink[phyAge-1][topAge]*param_H->val_axe_complement (param_H->getFruitMasterSink(), phyAge-1) * unitMatter;
						((InterNodeHYDROSANSLUM *)(b->getCurrentEntn()))->volume[FRUIT] += organMatter;
						if (top > 0)
						{
							totalFruitMatter += organMatter*b->totalAxeNumberWithinPlant;
							totalOrganMatter += organMatter*b->totalAxeNumberWithinPlant;
						}
					}
				}
				if (!organ)
				{
					if (first)
					{
						first = 0;
						/* add matter to the stack for layers if there is more than 1 Gu */
						if (  getScheduler().getTopClock() > 1.
								&& (Gul = ((BudHYDROSANSLUM *)b->getBud())->HasLayer() ) != NULL)
						{
							totalNbLayers ++;
							/* go to last Gu */

							phyAge = Gul->phyAgeGu;
							layerMatter += param_H->val_axe_complement(param_H->getLayerMasterSink(), phyAge-1) * unitMatter;
							if (top > 0)
							{
								totalLayerMatter += param_H->val_axe_complement (param_H->getLayerMasterSink(), phyAge-1) * unitMatter*b->totalAxeNumberWithinPlant;
								totalOrganMatter += param_H->val_axe_complement (param_H->getLayerMasterSink(), phyAge-1) * unitMatter*b->totalAxeNumberWithinPlant;
							}
						}

						/* go to last Gu for the pith matter */

						b->startPosit();
						b->positOnLastGuWithinBranc();
						Gul = b->getCurrentGu();

						if (b->getCurrentEntnNumberWithinGu() > 0)
						{
							phyAge = Gul->phyAgeGu;

							gTime = param_H->val_axe_complement (param_H->getGrowthTime(), phyAge-1);
							b->positOnFirstEntnWithinGu();
							/* computes the age of last Gu (for terminated axes) */
							topAge = top-b->getCurrentEntn()->instant;
							/* compute matter from end to bottom of branch */
							for (k=0; k<gTime-topAge && k < b->getGuNumber(); k++)
							{
								b->positOnGuWithinBrancAt(b->getGuNumber()-1-k);
								Gul=b->getCurrentGu();					   			
								phyAge = Gul->phyAgeGu;
								organMatter = pithSink[phyAge-1][k+topAge]*param_H->val_axe_complement (param_H->getPithMasterSink(), phyAge-1) * unitMatter;
								b->positOnFirstEntnWithinGu();
								((InterNodeHYDROSANSLUM *)(b->getCurrentEntn()))->volume[PITH] += organMatter/b->getCurrentEntnNumberWithinGu();
								if (top > 0)
								{
									totalPithMatter += organMatter*b->totalAxeNumberWithinPlant;
									totalOrganMatter += organMatter*b->totalAxeNumberWithinPlant;
								}
							}
						}
						b->endPosit();
					}

					b->positOnFirstEntnWithinGu();

					/* check if the Gu will get a new layer */
					if (getScheduler().getTopClock() -b->getCurrentEntn()->instant > 1)
						inLayer = 1;
					else
						inLayer = 0;

					k=0;
					/* sum the total number of functioning seen leaves and update pith volume for next entn */
					do
					{		
						nbEntnTotal++;

						InterNodeHYDROSANSLUM * d = (InterNodeHYDROSANSLUM *)b->getCurrentEntn();

						/* avoid the last apperaring GU */
						if (inLayer)
							seenLeafNumber += d->nbleaves;
						if (k == 0)
							pithVolume = d->volume[PITH];
						else
							d->volume[PITH] = pithVolume;

						pithTotalVol+=d->volume[PITH];

						k++;

					}while(b->positOnNextEntnWithinGu());
				}
			}while(b->positOnNextGuWithinBranc());	
		}//end posit

		b->endPosit();	 


		totalLeafNumber += seenLeafNumber * b->totalAxeNumberWithinPlant;

		//((BudHYDROSANSLUM *)b->getBud())->pithTotalVol=pithTotalVol;



	}while((b=(BrancAMAP *)this->getTopology().seekTree(TopoManager::NEXT))!=NULL);

	/* loop on the branches to split total layer matter according to nbUpperLeaves 
	 * and add a new ring to each node 
	 * */
	if (totalLeafNumber > 0)
		unitMatter = totalLayerMatter / totalLeafNumber;
	else
		unitMatter = 0;



	double layerMatterAverage=1.0;

	b = (BrancAMAP *)this->getTopology().seekTree(TopoManager::RESET);

	do
	{



		/* is this not a branch ? */
		b->startPosit();
		if(b->positOnFirstGuWithinBranc())
		{
			Gu=b->getCurrentGu();

			phyAge = Gu->phyAgeGu;
			if (   (   phyAge >= param_H->getMinBlade()
						&& phyAge <= param_H->getMaxBlade())
					|| (   phyAge >= param_H->getMinPetiole()
						&& phyAge <= param_H->getMaxPetiole())
					|| (   phyAge >= param_H->getMinFruit()
						&& phyAge <= param_H->getMaxFruit()))
			{
				b->endPosit();
				b = (BrancAMAP *)this->getTopology().seekTree(TopoManager::NEXT);
				continue;
			}

			/* split layer matter according to the number of seen leaves */
			do
			{

				Gu=b->getCurrentGu();

				if(!b->positOnFirstEntnWithinGu())continue;

				if (getScheduler().getTopClock() - b->getCurrentEntn()->instant < 1)
					continue;


				do
				{			


					InterNodeHYDROSANSLUM * d = (InterNodeHYDROSANSLUM *)b->getCurrentEntn();


					//organMatter = d.nbLeaves * unitMatter ;
					organMatter = d->nbleaves * unitMatter * (d->volume[PITH] / (pithTotalVol/(double)nbEntnTotal));

					/* adjust size of layer table */
					d->nbval ++;
					d->volume.push_back(organMatter);

				}while(b->positOnNextEntnWithinGu());
			}while(b->positOnNextGuWithinBranc());
		}//end if posit
		b->endPosit();


	}while((b=(BrancAMAP *)this->getTopology().seekTree(TopoManager::NEXT))!=NULL);

	cumultotalOrganMatter+=totalOrganMatter;

	cumultotalBladeMatter+=totalBladeMatter;

	cumultotalPetioleMatter+=totalPetioleMatter;

	cumultotalPithMatter+=totalPithMatter;

	cumultotalLayerMatter+=totalLayerMatter;

	cumultotalFruitMatter+=totalFruitMatter;

	if (top > 0)
	{
		printf ("top %f total matter %lf blade %lf petiole %lf pith %lf layer %lf (%d) fruit %lf\n",top,totalOrganMatter,totalBladeMatter,totalPetioleMatter,totalPithMatter,totalLayerMatter,totalNbLayers,totalFruitMatter);
		printf ("cumul  total matter %lf blade %lf petiole %lf pith %lf layer %lf (%d) fruit %lf\n",cumultotalOrganMatter,cumultotalBladeMatter,cumultotalPetioleMatter,cumultotalPithMatter,cumultotalLayerMatter,totalNbLayers,cumultotalFruitMatter);
	}
}




void PlantHYDROSANSLUM::serialize(Archive& ar )
{		
	VProcess::serialize(ar);
	PlantAMAP::serialize(ar);

	if(ar.isWriting() )
	{   
		ArchivableObject * arObj=(ArchivableObject *)this;
		this->SavePtr(ar,arObj);

		ar<<totalMatter<<cumultotalOrganMatter<<cumultotalBladeMatter<<cumultotalPetioleMatter<<cumultotalPithMatter;
		ar<<cumultotalLayerMatter<<cumultotalFruitMatter<<totalOrganMatter<<totalBladeMatter<<totalPetioleMatter;
		ar<<totalPithMatter<<totalLayerMatter<<totalFruitMatter;

	}
	else
	{
		ArchivableObject * arObj;
		this->LoadPtr(ar,arObj);
		arObj=(ArchivableObject *)this;
		this->AddPtr(ar,arObj);

		ar>>totalMatter>>cumultotalOrganMatter>>cumultotalBladeMatter>>cumultotalPetioleMatter>>cumultotalPithMatter;
		ar>>cumultotalLayerMatter>>cumultotalFruitMatter>>totalOrganMatter>>totalBladeMatter>>totalPetioleMatter;
		ar>>totalPithMatter>>totalLayerMatter>>totalFruitMatter;


	}




}

extern "C" Plant * StartPlugin (void)
{
	return new PlantHYDROSANSLUM();
}

