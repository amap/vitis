///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file GeomBrancHYDRO.h																					
///			\brief Definition of GeomElemHYDROSANSLUM and GeomBrancHYDROSANSLUM class.																	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __GEOMBRANCHYDROSANSLUM_H__
#define __GEOMBRANCHYDROSANSLUM_H__

#include "GeomBrancAMAP.h"
#include "GeomElemCone.h"
#include "externHydro.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class GeomElemHYDROSANSLUM																				
///			\brief Inherit from GeomElemCone and Subject 
///				
///			Launch a signal to observers when a new geometrical element is created 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class HYDROSANSLUM_EXPORT GeomElemHYDROSANSLUM : public GeomElemCone
{
public:

	GeomElemHYDROSANSLUM()	{};

	~GeomElemHYDROSANSLUM(){};
	
/*	virtual void setID(int i)
	{	GeomElem::setID(i);
		this->setState(NEWGEOME);
		this->notify();
	}
*/
};



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class GeomBrancHYDROSANSLUM																				
///			\brief Specialization of GeomBrancAMAP by adding of method to compute Diameter and length according allocated matter
///				
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GeomBrancHYDROSANSLUM :
	public GeomBrancAMAP
{
public:
	
	GeomBrancHYDROSANSLUM(Hierarc * brc, GeomBranc * gbr, class Plant * gt=NULL);

	virtual ~GeomBrancHYDROSANSLUM(void);
	
	/// \brief Specialized the standard function AMAPSIM computing the internode's diameter
	/// \return values : \li 0 : nothing was done \li 1 : a new diameter was computed
	virtual float computeDiamUp();

	/// \brief Specialized the standard function AMAPSIM computing the internode's length
	/// \return values : \li 0 : nothing was done \li 1 : a new length was computed
	virtual float computeLength ();


	double diameter (double volume, float ShapeA, float ShapeB);

	double length (double volume, float ShapeA, float ShapeB);

	
};

#endif
