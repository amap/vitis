#include "BudHYDRO.h"
#include "InterNodeHYDRO.h"
#include "PlantHydraulic.h"
#include "DebugNew.h"

BudHYDROSANSLUM::BudHYDROSANSLUM(BrancAMAP * br, char guRamificationType, double timeC ,long phyAgeBeg, double ryt):Bud(br,guRamificationType,timeC,phyAgeBeg,ryt)
{

	updateHierar (br->currentHierarcPtr);

}

BudHYDROSANSLUM::~BudHYDROSANSLUM(void)
{



}


void BudHYDROSANSLUM::updateHierar (Hierarc * hierarcPtr)
{

	ParamHYDROSANSLUM & vp_plt_h = ((PlantHYDROSANSLUM *)this->getBranc()->getPlant())->getParamHydro();

	Hierarc * saveH=this->getBranc()->currentHierarcPtr;

	this->getBranc()->currentHierarcPtr=hierarcPtr;


	/* if it is a blade : adjust the number of above leaves for the down hierarchy */
	if (   this->getPhyAge() >= vp_plt_h.getMinBlade()
			&& this->getPhyAge() <= vp_plt_h.getMaxBlade())
	{
		adjustUpperLeavesNumber (1);
	}

	this->getBranc()->currentHierarcPtr=saveH;



}

void BudHYDROSANSLUM::updatePruning ()
{

	float fTime, topAge;
	ParamHYDROSANSLUM & vp_plt_h = ((PlantHYDROSANSLUM *)this->getBranc()->getPlant())->getParamHydro();

	/* is this a blade ? */
	if (  this->getPhyAge()>=  vp_plt_h.getMinBlade()
			&& this->getPhyAge() <=  vp_plt_h.getMaxBlade())
	{

		fTime = vp_plt_h.val_axe_complement (vp_plt_h.getFunctioningTime(), this->getPhyAge()-1);
		/* compute age of the blade */
		//		brancAMAP->startPosit();
		//		brancAMAP->positOnFirstEntnWithinBranc();
		topAge = (float)brancAMAP->getPlant()->getScheduler().getTopClock()- (float)brancAMAP->getCurrentEntn()->instant;
		//		brancAMAP->endPosit();

		if ((brancAMAP->getPlant()->getScheduler().getTopClock() < brancAMAP->getPlant()->getScheduler().getHotStop()+EPSILON)&&(topAge < fTime))
		{
			/* if it is a blade : adjust the number of above leaves for the down hierarchy */
			adjustUpperLeavesNumber (-1);
		}


	}

}

/* seek down hierarchy from current b position and adjust the number of leaves that are seen
 * above the organ
 * */
void BudHYDROSANSLUM::adjustUpperLeavesNumber (int increment)
{
	int  i,currentIncrement;
	BrancAMAP *bearer;


	bearer = (BrancAMAP *)brancAMAP->getBearer ();

	currentIncrement=increment;


	/* adjust number of borne leaves from bearing position to bottom */
	if (bearer != brancAMAP)
	{


		//positioning on the bearer at gu and entn borne index 	
		bearer->startPosit();

		bearer->positOnEntnWithinBrancAt(brancAMAP->currentHierarcPtr->indexOnBearerAtLowestLevel);

		//adjust number of leaves for each above entn 
		do
		{


			InterNodeHYDROSANSLUM * d = (InterNodeHYDROSANSLUM *)bearer->getCurrentEntn();
			d->nbleaves += currentIncrement;

			if (d->nbleaves < 0)
				d->nbleaves = 0;

		}
		while(bearer->positOnPreviousEntnWithinBranc());	

		bearer->endPosit();

		Hierarc * tmph= bearer->currentHierarcPtr;


		// adjust the number of instances of that leaf that are seen up 
		for(i=0; i<bearer->hierarcPtrsNumber; i++)
		{			

			//update leaves number for each simplified instance
			((BudHYDROSANSLUM *)(((BrancAMAP *)bearer->hierarcPtrs[i]->getBranc())->getBud()))->adjustUpperLeavesNumber(currentIncrement);							

		}

		bearer->currentHierarcPtr=tmph;
	}

	return;
}


/* test if current Axe has a layer, returns the las Gu if true, returns NULL if not */
GrowthUnitAMAP * BudHYDROSANSLUM::HasLayer ()
{
	BrancAMAP *borne;
	Hierarc *cur;
	GrowthUnitAMAP *s = NULL;
	int i, nbBorne, exist;

	ParamHYDROSANSLUM & vp_plt_h = ((PlantHYDROSANSLUM *)this->getBranc()->getPlant())->getParamHydro();

	/* an axe bears a layer if it consists of more than one GU and bears functioning leaves
	 * or if it bears only axes consisting in one GU bearing functioning leaves 
	 * */
	/* seeks for the last GU of the axe */

	brancAMAP->startPosit();
	brancAMAP->positOnLastGuWithinBranc();
	s=brancAMAP->getCurrentGu();
	brancAMAP->endPosit();	

	if (   brancAMAP->getGuNumber() > 1
			&& this->seekForFunctioningLeaves ())
		return s;

	/* seeks for borne functioning leaves */
	cur = brancAMAP->currentHierarcPtr;
	nbBorne = cur->getNbBorne ();
	exist = 0;
	for (i=0; i<nbBorne; i++)
	{
		borne = (BrancAMAP *)cur->getBorne (i);
		/* if it is not wood, continue */
		if (   (   borne->getBud()->getPhyAge() >=  vp_plt_h.getMinBlade()
					&& borne->getBud()->getPhyAge() <=  vp_plt_h.getMaxBlade())
				|| (   borne->getBud()->getPhyAge() >=  vp_plt_h.getMinPetiole()
					&& borne->getBud()->getPhyAge() <=  vp_plt_h.getMaxPetiole())
				|| (   borne->getBud()->getPhyAge() >=  vp_plt_h.getMinFruit()
					&& borne->getBud()->getPhyAge() <=  vp_plt_h.getMaxFruit()))
			continue;

		if (   borne->getGuNumber() == 1
				&& ((BudHYDROSANSLUM *)(borne->getBud()))->seekForFunctioningLeaves ())
			exist = 1;
		if (   borne->getGuNumber() > 1
				&& ((BudHYDROSANSLUM *)(borne->getBud()))->seekForFunctioningLeaves ())
			return NULL;
	}
	if (exist)
		return s;
	else
		return NULL;
}




/* return 1 if b bears functioning leaves */
int BudHYDROSANSLUM::seekForFunctioningLeaves ()
{
	BrancAMAP *borne;
	Hierarc *cur;
	int i, nbBorne, fTime;
	float topAge;

	ParamHYDROSANSLUM & vp_plt_h = ((PlantHYDROSANSLUM *)this->getBranc()->getPlant())->getParamHydro();

	/* seeks for borne functioning leaves */
	cur = brancAMAP->currentHierarcPtr;
	nbBorne = cur->getNbBorne ();
	for (i=0; i<nbBorne; i++)
	{
		borne = (BrancAMAP *)cur->getBorne (i);
		if (   borne->getBud()->getPhyAge() >=  vp_plt_h.getMinBlade()
				&& borne->getBud()->getPhyAge() <=  vp_plt_h.getMaxBlade())
		{
			fTime =  vp_plt_h.val_axe_complement (vp_plt_h.getFunctioningTime(), borne->getBud()->getPhyAge()-1);

			borne->startPosit();
			borne->positOnFirstEntnWithinBranc();
			/* compute age of the blade */
			topAge = brancAMAP->getPlant()->getScheduler().getTopClock()- borne->getCurrentEntn()->instant;
			borne->endPosit();

			if (topAge < fTime)
				return 1;
		}
	}

	return 0;
}



