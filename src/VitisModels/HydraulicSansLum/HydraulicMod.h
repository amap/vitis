///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file HydraulicMod.h																					
///			\brief Definition of HydraulicMod class.																	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __HYDRAULICMOD_H__
#define __HYDRAULICMOD_H__

#include "AmapSimMod.h"
#include "externHydro.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class HydraulicMod																				
///			\brief GreenLab+AMAPSIM Edification plant model	(based on Axis reference and GreenLab v1)
///	
///			This simulation model specialized the AMAPSIM model by adding the organ size computation according to hydraulic flow and source/sink ratio within the plant
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class HYDROSANSLUM_EXPORT HydraulicMod :
	public AmapSimModele
{
public:
	
	/// \brief Default constructor
	HydraulicMod(class Plant * plt);
	
	/// \brief Destructor
	~HydraulicMod(void);
	


	/// \brief Create a new geomtrical branch HYDROSANSLUM
	/// @param brc The topological bearer
	/// @param gbr The geometrical bearer
	/// @param gt The geometrical manager
	///	@return An instance of GeomBrancHYDROSANSLUM
	virtual GeomBranc * createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr);

	/// \brief Create a new instance of an element of decomposition 
	///
	///	One level is specialized in this modele , level 5 (INTERNODE)
	/// @param level The level of the element to instanciate
	///	@return An instance of DecompAxeLevel, or GrowthUnitAMAP (if level==1), or InterNodeHYDROSANSLUM (if level==5)
	virtual DecompAxeLevel * createInstanceDecompAxe (USint level);
	
	/// \brief Create a new BudHYDROSANSLUM
	/// @param br The branc associated to the Bud
	/// @param guRam growth unit ramification type : ANTICIPE, RETARDE1, RETARDE2, TRAUMAANT, TRAUMARET, RETARDECYC, NONBRANCHETRAUMAANT, NONBRANCHEANT, NONBRANCHERETCYC, NONBRANCHETRAUMARET
	/// @param timeC Start time of the new bud
	/// @param phyAgeBeg PhyAge of the new bud
	/// @param ryt rythme of the new Bud
	///	@return An instance of BudHYDROSANSLUM
	virtual Bud * createInstanceBud (class BrancAMAP * br, char guRam, double timeC, long phyAgeBeg,double ryt);

	
	/// \brief Create a new geometrical element 
	///	@return An instance of GeomElemHYDROSANSLUM
	virtual GeomElem * createInstanceGeomElem ();

};



#endif
