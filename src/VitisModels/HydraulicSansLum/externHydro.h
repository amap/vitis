#ifndef __EXPORTHYDROSANSLUM_H__
#define __EXPORTHYDROSANSLUM_H__
#include "externAmapSim.h"
	
#pragma warning ( disable : 4251 )

#if !defined STATIC_LIB
	#if defined WIN32 

			#ifdef HYDROSANSLUM_EXPORTS
				#define HYDROSANSLUM_EXPORT __declspec(dllexport)
			#else
				#define HYDROSANSLUM_EXPORT __declspec(dllimport)
			#endif			
	#else 
			#define HYDROSANSLUM_EXPORT
			
	#endif

#else 
		#define HYDROSANSLUM_EXPORT
		
#endif


#endif
