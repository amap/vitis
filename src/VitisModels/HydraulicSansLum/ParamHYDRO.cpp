#include "ParamHYDRO.h"

ParamHYDROSANSLUM::ParamHYDROSANSLUM(const std::string & paramFileN):AmapSimParamFile(paramFileN)
{
}

ParamHYDROSANSLUM::~ParamHYDROSANSLUM(void)
{
}



void ParamHYDROSANSLUM::readParam ()
{
	char ligne[256];

	this->open();

	fic->getline(ligne,255);

	*fic>>MinBlade;
	*fic>>MaxBlade;
	*fic>>MinPetiole;
	*fic>>MaxPetiole;
	*fic>>MinFruit;
	*fic>>MaxFruit;
	*fic>>ClimateParam;
	*fic>>sowYear;
	*fic>>sowDay;



	read1DParameter(pithMasterSink); /*       Pith master sink */

	read1DParameter(layerMasterSink); /*       Layer master sink */

	read1DParameter(petioleMasterSink); /*       Petiole master sink */
	read1DParameter(bladeMasterSink); /*       Blade master sink */
	read1DParameter(fruitMasterSink); /*       Fruit master sink */
	read1DParameter(pithSinkAgingN); /*       Pith sink time evolution beta law first parameter */
	read1DParameter(pithSinkAgingP); /*       Pith sink time evolution beta law second parameter */
	read1DParameter(layerSinkAgingN); /*       Layer sink time evolution beta law first parameter */
	read1DParameter(layerSinkAgingP); /*       Layer sink time evolution beta law second parameter */
	read1DParameter(petioleSinkAgingN); /*       Petiole sink time evolution beta law first parameter */
	read1DParameter(petioleSinkAgingP); /*       Petiole sink time evolution beta law second parameter */
	read1DParameter(bladeSinkAgingN); /*       Blade sink time evolution beta law first parameter */
	read1DParameter(bladeSinkAgingP); /*       Blade sink time evolution beta law second parameter */
	read1DParameter(fruitSinkAgingN); /*       Fruit sink time evolution beta law first parameter */
	read1DParameter(fruitSinkAgingP); /*       Fruit sink time evolution beta law second parameter */
	*fic>>SeedVolume;
	*fic>>SeedTime;
	read1DParameter(bladeResistivity); /* Blade resistivity */
	read1DParameter(petioleResistance); /* Petiole resistivity */
	read1DParameter(GrowthTime); /*        growth time */
	read1DParameter(FunctioningTime); /*        functioning time */
	read1DParameter(shapeFactorA); /*       Length/surface ratio */
	read1DParameter(shapeFactorB); /*       Length factor */

	*fic>>bladeThickness;/*		   Blade thickness */

	fic->close();

}

void ParamHYDROSANSLUM::writeParam()
{
	this->open();

	*fic<<"AMAPSIM parameter file VERSION 10.1\n";

	*fic<<MinBlade<<"\n";
	*fic<<MaxBlade<<"\n";
	*fic<<MinPetiole<<"\n";
	*fic<<MaxPetiole<<"\n";
	*fic<<MinFruit<<"\n";
	*fic<<MaxFruit<<"\n";
	*fic<<ClimateParam<<"\n";
	*fic<<sowYear<<"\n";
	*fic<<sowDay<<"\n";

	write1DParameter(pithMasterSink,1.0);
	write1DParameter(layerMasterSink,1.0);
	write1DParameter(petioleMasterSink,1.0);
	write1DParameter(bladeMasterSink,1.0);
	write1DParameter(fruitMasterSink,1.0);
	write1DParameter(pithSinkAgingN,1.0);
	write1DParameter(pithSinkAgingP,1.0);
	write1DParameter(layerSinkAgingN,1.0);
	write1DParameter(layerSinkAgingP,1.0);
	write1DParameter(petioleSinkAgingN,1.0);
	write1DParameter(petioleSinkAgingP,1.0);
	write1DParameter(bladeSinkAgingN,1.0);
	write1DParameter(bladeSinkAgingP,1.0);
	write1DParameter(fruitSinkAgingN,1.0);
	write1DParameter(fruitSinkAgingP,1.0);

	*fic<<SeedVolume<<"\n";
	*fic<<SeedTime<<"\n";

	write1DParameter(bladeResistivity,1.0);
	write1DParameter(petioleResistance,1.0);
	write1DParameter(GrowthTime,1.0);
	write1DParameter(FunctioningTime,1.0);
	write1DParameter(shapeFactorA,1.0);
	write1DParameter(shapeFactorB,1.0);

	*fic<<bladeThickness<<"\n";

	fic->flush();

	fic->close();

}


/* this function returns the value at a particular position
 * within a parametric curve 
 * */
float ParamHYDROSANSLUM::val_axe_complement (AmapSimVariable & var, int pos)
{
	int		i;
	float        val;

	if (pos == var.currentPosition)
	{
		val = var.currentValue;
		return (val);
	}
	else if (pos <= var.currentX[0])
		var.currentValue = var.currentY[0];
	else if (pos >= var.currentX[var.nb_pos_current-1])
		var.currentValue = var.currentY[var.nb_pos_current-1];
	else
	{
		for (i=1; i<var.nb_pos_current; i++)
			if (var.currentX[i] >= pos)
				break;
		if (var.interpolation_position_current == 0)
		{
			if (var.currentX[i] == pos)
				var.currentValue = var.currentY[i];
			else
				var.currentValue = var.currentY[i-1];
		}
		else
			var.currentValue = var.currentY[i-1] + (var.currentY[i]-var.currentY[i-1]) * (pos-var.currentX[i-1]) / (var.currentX[i]-var.currentX[i-1]);
	}

	var.currentPosition = pos;
	val = var.currentValue;

	return (val);
}
