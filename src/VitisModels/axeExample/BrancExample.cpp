// BrancExample.cpp: implementation of the BrancExample class.
//
//////////////////////////////////////////////////////////////////////
#include <string>
#include "randomFct.h"
#include "BrancExample.h"
#include "Metamer.h"
#include "Plant.h"
#include "Bud.h"
#include <iostream>
#include <math.h>
#include "DebugNew.h"

//using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


BrancExample::BrancExample(Plant * plt):Branc(plt)
{

	theBud= NULL;

	currentMetamerNumberWithinBranch=0;

}



//Constructeur par copie
BrancExample::BrancExample(BrancExample * b):Branc(b)
{
	theBud= NULL;
	theBud = b->getBud();
	currentMetamerNumberWithinBranch=b->currentMetamerNumberWithinBranch;
}



BrancExample::~BrancExample()
{

	if (theBud != NULL)
	{
		delete theBud;
	}
}


void BrancExample::updateTopo (Hierarc * hierarcPtr)
{
	Branc::updateTopo(hierarcPtr);

	if(theBud != NULL)//On est sur la premiere instance de cet axe
		theBud->updateHierar(hierarcPtr);

}

void BrancExample::addMetamer (Metamer * met)
{

	currentMetamerNumberWithinBranch++;

	addSubElementAtLevel(met);

}

void BrancExample::addBud (Bud * b)
{
	//Cr�ation du Bud associ� !
	theBud=b;

}



Bud * BrancExample::getBud()
{
	return this->theBud;
}


BrancExample * BrancExample::getBearer ()
{
	return (BrancExample *)currentHierarcPtr->getBearer()->getBranc();
}


USint BrancExample::getCurrentMetamerIndexWithinBranc()
{
	return getCurrentElementIndexOfSubLevel(METAMER);
}

USint BrancExample::getCurrentMetamerNumberWithinBranc()
{
	return getElementNumberOfSubLevel(METAMER);
}


//Position method

USint BrancExample::positOnFirstMetamerWithinBranc()
{
	return positOnFirstElementOfSubLevel(METAMER);
}

USint BrancExample::positOnLastMetamerWithinBranc()
{
	return positOnLastElementOfSubLevel(METAMER);
}

USint BrancExample::positOnNextMetamerWithinBranc()
{
	return positOnNextElementOfSubLevel(METAMER);
}

USint BrancExample::positOnPreviousMetamerWithinBranc()
{
	return positOnPreviousElementOfSubLevel(METAMER);
}

USint BrancExample::positOnMetamerWithinBrancAt(USint pos)
{
	return positOnElementAtSubLevel(pos, METAMER);
}
USint BrancExample::positOnEntnWithinBrancAt(USint pos)
{
	return positOnElementAtSubLevel(pos, METAMER);
}


void BrancExample::serialize(Archive& ar )
{
	int nat;

	Branc::serialize(ar);


	if(ar.isWriting() )
	{
		ar<<currentMetamerNumberWithinBranch;

		//Save the eventData mechanism link to this branc
		if(this->SavePtr(ar,theBud))
		{
			ar<<*theBud;
		}


	}
	else
	{
		ar>>currentMetamerNumberWithinBranch;

		ArchivableObject * arBud;
		if(this->LoadPtr(ar,arBud))
		{
			theBud=new Bud();

			this->AddPtr(ar,theBud);

			ar>>*theBud;
		}
		//else pas de else car c ici kon doit les charger ( et pas au nivo du sequenceur en chargant les process


		//TODO SERIAL pointeur sur topoMan;


	}

}





