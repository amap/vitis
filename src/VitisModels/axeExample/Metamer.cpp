#include "Metamer.h"
#include "PlantExample.h"
#include "BrancExample.h"
#include "DebugNew.h"


Metamer::Metamer(Plant * plt):DecompAxeLevel(plt,METAMER)
{	
	instant=0;
}

Metamer::~Metamer(void)
{
}

void Metamer::serialize(Archive& ar )
{		

	DecompAxeLevel::serialize(ar);


	if(ar.isWriting() )
	{   
		ar<<instant;
	}
	else
	{
		ar>>instant;
	}
}



