///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file ExampleMod.h
///			\brief Definition of ExampleModele.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __EXAMPLEMODELE_H__
#define __EXAMPLEMODELE_H__
#include "PlantBuilder.h"
#include "Bud.h"




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class ExampleModele
///			\brief EXAMPLEM Edification plant model	(based on Axis reference)
///
///			Gives methods to get simulation object's instance specialized by this dynamic model
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class EXAMPLE_EXPORT ExampleModele: public PlantBuilder
{
	public:

		/// \brief Default constructor
		ExampleModele(class Plant * plt);

		/// \brief Destructor
		~ExampleModele();


		/// \brief Create a new branch AMAP ( Branc specialization)
		/// @param hbear The topological bearer
		///	@return An instance of BrancAMAP
		virtual Branc * createInstanceBranc ();

		/// \brief Create a new instance of an element of decomposition
		///
		///	Two level are specialized in this modele , level 1 (GROWTHUNIT) and level 5 (INTERNODE)
		/// @param level The level of the element to instanciate
		///	@return An instance of DecompAxeLevel, or GrowthUnitAMAP (if level==1), or InterNodeAMAP (if level==5)
		virtual DecompAxeLevel * createInstanceDecompAxe (USint level);

		/// \brief Create a new Bud (manage the growth and ramification)
		/// @param br The branc associated to the Bud
		/// @param guRam growth unit ramification type : ANTICIPE, RETARDE1, RETARDE2, TRAUMAANT, TRAUMARET, RETARDECYC, NONBRANCHETRAUMAANT, NONBRANCHEANT, NONBRANCHERETCYC, NONBRANCHETRAUMARET
		/// @param timeC Start time of the new bud
		/// @param phyAgeBeg PhyAge of the new bud
		/// @param ryt rythme of the new Bud
		///	@return An instance of Bud
		virtual Bud * createInstanceBud ( class BrancExample * br);





};



#endif
