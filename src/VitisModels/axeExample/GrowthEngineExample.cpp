#include "PlantExample.h"
#include "GrowthEngineExample.h"
#include "Metamer.h"
#include "Bud.h"
#include <math.h>
#include "randomFct.h"
#include "DebugNew.h"
#include "Exceptions.h"

GrowthEngineExample::GrowthEngineExample(Bud * b)
{
	theBud=b;

	theBranc=b->brancExample;
}

GrowthEngineExample::~GrowthEngineExample(void)
{
}



/* main loop for growth of a bud */
int GrowthEngineExample::growth ()
{
	return putMetamer ();
}


void GrowthEngineExample::serialize(Archive& ar )
{


}


int GrowthEngineExample::putMetamer( )
{
std::cout << "creation d'un nouveau metamere ordre "<< (int)theBud->ordre <<" a l'instant "<<theBranc->getPlant()->getScheduler().getTopClock()<<std::endl;

	Metamer *met=(Metamer *)theBranc->getPlant()->getPlantFactory()->createInstanceDecompAxe(METAMER);
	met->instant = theBranc->getPlant()->getScheduler().getTopClock();
	theBranc->addMetamer(met);
	
	theBranc->getPlant()->getScheduler().self_signal_event( NULL,(double)theBud->nextTime,theBranc->getPlant()->getScheduler().getTopPriority());

		
	int i = theBranc->getPlant()->getScheduler().getActionIndex(theBud->getPid());
	try
	{
		Action & a = theBranc->getPlant()->getScheduler().getAction(theBud->getPid());
std::cout << "index de l'action correspondante "<<i<<" date "<<a.time<<std::endl;



		Action & b = theBranc->getPlant()->getScheduler().getAction(1000);
	} catch (Vitis::CException e){
		std::cout <<e.what()<<std::endl;
		};
	
//	theBranc->getPlant()->getScheduler().changeAction(i, theBud->getPid(), theBud->nextTime+1, theBranc->getPlant()->getScheduler().getTopPriority()); 
	return 1;
}
