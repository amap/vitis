///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file BrancExample.h
///			\brief Definition of BrancExample class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _BRANCEXAMPLE_H
#define _BRANCEXAMPLE_H
#include "externExample.h"
#include "Branc.h"

#define METAMER 1

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class BrancExample
///			\brief Describe an Example axe of the plant (friend with Bud class)
///
///			Reprensentation of an axe according ExampleSIM formalism. The structure of this axe is updated by his Bud \n
///			An axe is composed in five decomposition level : GROWTHUNIT, GROWTHCYCLE, ZONE, TESTUNIT, INTERNODE
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class EXAMPLE_EXPORT BrancExample : public Branc
{

	friend class Bud;
	friend class Metamer;


	public:

	USint currentMetamerNumberWithinBranch;

	/// \brief Pointer to the Bud
	Bud * theBud;

	/// \brief Constructor
	/// \param idxPlt Current index of the mother plant into forest
	BrancExample(class Plant * plt);

	/// \brief Copy constructor
	BrancExample(BrancExample * b);

	/// \brief Destructor
	virtual ~BrancExample();

	/// \brief Add the given bud to this axe
	void addBud (Bud * b);

	/// \brief Add the given metamer to the end of the branc
	void addMetamer (Metamer * met);


	/// @name Member accessor
	//@{

	/// \return the BrancExample bearer
	BrancExample * getBearer ();

	/// \return the Bud
	Bud * getBud();

	/// \return the current metamer
	Metamer * getCurrentMetamer ();

	//@}


	/// @name Decomposition level index accessor
	/// \see DecompAxeLevel.h
	//@{

	/// \return the current Growth unit number into the axe
	USint getCurrentMetamerNumberWithinBranc ();

	/// \return the current metamer index into the axe
	USint getCurrentMetamerIndexWithinBranc ();


	//@}


	/// \name Seek method into the axe
	//@{


	/// \brief Positioning on the first internode of the axe
	USint positOnFirstMetamerWithinBranc();

	/// \brief Positioning on the last internode of the axe
	USint positOnLastMetamerWithinBranc();

	/// \brief Positioning on the current next internode of the axe
	USint positOnNextMetamerWithinBranc();

	/// \brief Positioning on the current previous internode of the axe
	USint positOnPreviousMetamerWithinBranc();

	/// \brief Positioning on the internode at given index into the axe
	USint positOnMetamerWithinBrancAt(USint pos);
	USint positOnEntnWithinBrancAt(USint pos);

	//@}

	/// \brief Fucntion called when a hierarc axe linked to this branc is added
	virtual void updateTopo (class Hierarc * hierarcPtr);


	/// \brief Serialize a BrancExample
	/// \param ar A reference to the archive \see Archive
	virtual void serialize(Archive& ar );


};





#endif

