#include "PlantExample.h"
#include "example.h"
#include "Bud.h"

PlantExample::PlantExample()
{
}

PlantExample::PlantExample(const std::string & nameT, const std::string & nameP):Plant(nameT, nameP)
{
}



PlantExample::~PlantExample(void)
{
}


void PlantExample::init()
{
	Plant::init();
}


void PlantExample::seed (float time)
{
	Plant::seed (time);

	BrancExample * bvit= (BrancExample *)v_pltBuilder->createInstanceBranc();

	v_topoMan->fork(bvit, NULL);

	bvit->addBud(((ExampleModele*)v_pltBuilder)->createInstanceBud(bvit));

	v_actionScheduler->create_process(bvit->getBud(),NULL,bvit->getBud()->timeForATest, 10);

}


PlantBuilder * PlantExample::instanciatePlantBuilder()
{
	return new ExampleModele (this);
}

#if !defined STATIC_LIB
extern "C" Plant * StartPlugin (void)
{
	return new PlantExample();
}
#endif

