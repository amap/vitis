///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  PlantAMAP.h
///			\brief Definition of PlantAMAP class.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __PLANTEXAMPLE_H__
#define __PLANTEXAMPLE_H__
#include "externExample.h"
#include "Plant.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class PlantAMAP
///			\brief AMAP describtion for a plant , specialize Plant with adding a pointer to an axis reference
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class EXAMPLE_EXPORT PlantExample:public Plant
{

public:

	/// \brief Default constructor
	PlantExample();

	/// \brief Constructor
	PlantExample(const std::string & nameT, const std::string & nameP);

	/// \brief Destructor
	virtual ~PlantExample(void);

	/// \brief Seed the plant  at the given time
	virtual void seed (float time);


	virtual void init();

	/// \brief  init the current instance of plant builder
	virtual PlantBuilder * instanciatePlantBuilder();

	/// \return the axis reference


};
#if defined EXAMPLE_EXPORTS
/// \brief C function , called by the "Load" function of SimulationModel in the Vitis framework to get the simulation modele
/// \return A instance of AmapSimModele
extern "C" EXAMPLE_EXPORT  Plant * StartPlugin (void);

#endif


#endif

