///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file InterNodeAMAP.h																					
///			\brief Definition of InterNodeAMAP class 
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _METAMEREXAMPLE_H
#define _METAMEREXAMPLE_H
#include "DecompAxeLevel.h"
#include "externExample.h"



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class InterNodeAMAP																				
///			\brief Describe an internode ( last sublevel decomposition of an AMAP axe )
///			
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class EXAMPLE_EXPORT Metamer :
	public DecompAxeLevel
{
	public:


		/// \brief birth time
		double  instant;
		
		/// \brief Constructor
		Metamer(class Plant * plt);

		/// \brief Destructor
		virtual ~Metamer(void);

		/// \brief Serialize a InterNodeAMAP
		/// \param ar A reference to the archive \see Archive 
		virtual void serialize(Archive& ar );

};

#endif

