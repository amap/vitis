#ifndef __EXPORTEXAMPLE_H__
#define __EXPORTEXAMPLE_H__
#include "externDll.h"
	
#pragma warning ( disable : 4251 )

#if !defined STATIC_LIB
	#if defined WIN32 

			#ifdef EXAMPLE_EXPORTS
				#define EXAMPLE_EXPORT __declspec(dllexport)
			#else
				#define EXAMPLE_EXPORT __declspec(dllimport)
			#endif			
	#else 
			#define EXAMPLE_EXPORT
			
	#endif

#else 
		#define EXAMPLE_EXPORT
		
#endif


#endif

