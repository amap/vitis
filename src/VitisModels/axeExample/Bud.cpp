#include "PlantExample.h"
#include "randomFct.h"
#include "BrancExample.h"
#include "Bud.h"
#include <iostream>
#include <math.h>
#include "DebugNew.h"

Bud::Bud()
{
	brancExample=NULL;
	groEngine=NULL;
	ramEngine=NULL;

}

Bud::Bud(BrancExample * br):VProcess()
{

	brancExample=br;

	death = 0;

	timeForATest = 1; /* time consumed for each test */

	BrancExample * bear =br->getBearer() ;

	if (bear == br)
	{
		ordre = 0;


	}
	else
	{
			ordre = bear->getBud()->ordre + 1;
	}

	groEngine= new GrowthEngineExample(this);

	ramEngine = new RamifEngineExample(this);




}




Bud::~Bud(void)
{
	brancExample->getPlant()->getScheduler().stop_process(this->process_id);

	delete groEngine;

	delete ramEngine;

}




void Bud::process_event (EventData * msg)
{
	int flag;

	this->nextTime=brancExample->getPlant()->getScheduler().getTopClock() + timeForATest;

	this->brancExample->currentHierarcPtr=this->brancExample->hierarcPtrs[0];

	/* if bud is dead or at end of reference axis then test self pruning */
	if(death)
	{
		pruning (); /* self prune */
		return;
	} /* else try to grow the bud */
	if (groEngine->growth ())
	{
		/* if a new internode was borne then try to put a ramification */
//			ramEngine->ramification ();
	}


}


void Bud::updateHierar (Hierarc * hierarcPtr)
{
	//rien
}

void Bud::updatePruning()
{
	//rien pr l'instant

}


int Bud::pruning  ()
{

	brancExample->getPlant()->getTopology().removeBranc(this->brancExample,1);

	return 0;

}

void Bud::serialize(Archive& ar )
{

	VProcess::serialize(ar);

	if(ar.isWriting() )
	{
		ar<<ordre<<death;

		ar<<deathInstant<<endInstant<<timeForATest;

		if(this->SavePtr(ar,brancExample))
		{
			std::cout<<"It should be saved before";
		}

		ar<<*groEngine<<*ramEngine;

	}
	else
	{
		ar>>ordre>>death;

		ar>>deathInstant>>endInstant>>timeForATest;

		ArchivableObject * arBr;
		if(this->LoadPtr(ar,arBr))
		{
			std::cout<<"It would be saved before";
		}
		else brancExample=(BrancExample *)arBr;

		groEngine=new GrowthEngineExample(this);

		ramEngine=new RamifEngineExample(this);

		ar>>*groEngine>>*ramEngine;

	}


}



