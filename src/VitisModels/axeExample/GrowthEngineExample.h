///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file GrowthEngineAMAP.h																					
///			\brief Definition of GrowthEngineAMAP class 
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __GROWTHENGINE__
#define __GROWTHENGINE__
#include "BrancExample.h"
#include "ArchivableObject.h"
using namespace Vitis;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class GrowthEngineAMAP																			
///			\brief Definition of GrowthEngineAMAP class 
///
///			\li Update topology of an axe by adding new growthunit, cycle , zone , test and internodes according axis reference data
///			\li Compute phyage differentiation(automaton)
///			
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GrowthEngineExample : public ArchivableObject 
{

	public:

		/// \brief pointer to the bud
		class Bud * theBud;

		class BrancExample * theBranc;		

		/// \brief Default Constructor
		GrowthEngineExample(Bud * b=NULL);

		/// \brief Destructor 
		~GrowthEngineExample(void);

		/// \brief Main growth computation's algorithm
		int growth ();
		int putMetamer();

		/// \brief Serialize a GrowthEngineAMAP
		/// \param ar A reference to the archive \see Archive 
		virtual void serialize(Archive& ar );
};

#endif

