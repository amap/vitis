///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file Bud.h																					
///			\brief Definition of Bud class 
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _BUD_H
#define _BUD_H
#include "scheduler.h"
#include "BrancExample.h"
#include "GrowthEngineExample.h"
#include "RamifEngineExample.h"



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class Bud																				
///			\brief Describe the mechanism of a bud
///			
///			Manage two edification algorithm : growth and ramification for his linked "BrancExample" axe. \n
///			A bud inherit from VProcess and this process is scheduled along simulation time.\n
///			For each bud eventData launched, the process_event method is called and launch growth and ramification
///			
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class EXAMPLE_EXPORT Bud: public VProcess
{

	friend class GrowthEngineExample;
	friend class RamifEngineExample;
	public:	


	/// \brief branching order for nature 0
	char    ordre; 

	/// \brief death flag 
	char    death;

	/// \brief death moment
	double   deathInstant;

	/// \brief time for end of current Gu 
	double   endInstant;
	/// \brief time length for a test
	double   timeForATest; 
	/// \brief next event
	double   nextTime; 

	/// \brief Pointer to the growth engine
	GrowthEngineExample * groEngine;

	/// \brief Pointer to the ramification engine
	RamifEngineExample * ramEngine;

	/// \brief Pointer to his linked axe
	class BrancExample * brancExample;


	/// \brief Default constructor
	Bud();

	/// \brief Constructor
	/// @param br The branc associated to the Bud
	///	@return An instance of Bud
	Bud(BrancExample * br);

	/// \brief Destructor
	~Bud(void);


	/// \brief The main loop of axe's edification model
	/// \param msg The eventData attached to this process
	/// 
	/// This loop manage the growth, ramification , death and pruning computation
	virtual void process_event(EventData * msg);

	/// \brief Launch the natural pruning of the associated axe and his children
	int pruning  ();

	/// \brief Function called when a hierarc axe linked to this bud is added 
	virtual void updateHierar (Hierarc * hierarcPtr);

	/// \brief Function called when a hierarc axe linked to this bud is removed 
	virtual void updatePruning();

	/// \name Accessor

	/// \return the ramification engine
	inline RamifEngineExample * getRamif(){return ramEngine;}

	/// \return the growth engine
	inline GrowthEngineExample * getGrowth(){return groEngine;}

	/// \return the associated branc
	inline class BrancExample * getBranc(){return brancExample;}

	/// \brief Serialize a Bud
	/// \param ar A reference to the archive \see Archive 
	virtual void serialize(Archive& ar );



};



#endif

