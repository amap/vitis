#include "PlantExample.h"
#include "BrancExample.h"
#include "example.h"
#include "RamifEngineExample.h"
#include "Bud.h"
#include "randomFct.h"
#include "DebugNew.h"


RamifEngineExample::RamifEngineExample(Bud * b)
{
	theBud=b;

	theBranc=b->brancExample;

}

RamifEngineExample::~RamifEngineExample(void)
{
}


void RamifEngineExample::ramification  ()
{
	if (theBud->ordre < 2)
	{
		putRamification(theBranc->currentMetamerNumberWithinBranch-2);
	}
}

void RamifEngineExample::serialize(Archive& ar )
{
}

void RamifEngineExample::putRamification ()
{
	putRamification(theBranc->currentMetamerNumberWithinBranch-1);
}

void RamifEngineExample::putRamification (int pos)
{
	if (pos < 0)
		return;
		
	theBranc->startPosit();
	
	theBranc->positOnMetamerWithinBrancAt(pos);
	
	ExampleModele * modele = (ExampleModele*)theBranc->getPlant()->getPlantFactory();
	BrancExample * bvit= (BrancExample *)modele->createInstanceBranc();

	bvit->getPlant()->getTopology().fork(bvit, theBranc->getCurrentHierarc() );
		
	bvit->addBud(modele->createInstanceBud(bvit));

	theBranc->getPlant()->getScheduler().create_process(bvit->getBud(),NULL,theBranc->getPlant()->getScheduler().getTopClock()+bvit->getBud()->timeForATest, 10);
	
	theBranc->endPosit();
}
