///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file RamifEngineAMAP.h																					
///			\brief Definition of RamifEngineAMAP class 
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __RAMIFENGINE__
#define __RAMIFENGINE__
#include "ArchivableObject.h"
using namespace Vitis;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class RamifEngineAMAP																			
///			\brief Definition of RamifEngineAMAP class 
///
///			\li Update hierarchical organisation of the plant by adding new axe beared by current axe
///			\li Compute phyage transition for born bud (automaton)
///			
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class RamifEngineExample: public ArchivableObject 
{
	/// \brief pointer to the bud
	class Bud * theBud;

	class BrancExample * theBranc;

	public:

	/// \brief Default constructor
	RamifEngineExample(class Bud * b);

	/// \brief Destructor
	~RamifEngineExample(void);

	/// \brief Main ramification's algorithm
	void ramification  ();
	void putRamification();
	void putRamification(int pos);

	/// \brief Serialize a RamifEngineAMAP
	/// \param ar A reference to the archive \see Archive 
	virtual void serialize(Archive& ar );


};

#endif

