#include "example.h"
#include "PlantExample.h"
#include "BrancExample.h"
#include "Metamer.h"


#include "DebugNew.h"


ExampleModele::ExampleModele(Plant * plt):PlantBuilder(plt)
{
	levelOfDecomp=1;
}



ExampleModele::~ExampleModele()
{

}


Branc * ExampleModele::createInstanceBranc ()
{
	return new BrancExample(v_plt);
}

DecompAxeLevel * ExampleModele::createInstanceDecompAxe (USint level)
{
	switch(level)
	{
		case METAMER : return new Metamer (v_plt); break;
		default : return new DecompAxeLevel(v_plt,level);break;
	}
	return NULL;

}

Bud * ExampleModele::createInstanceBud ( BrancExample * br)
{
	return new Bud( br);
}



