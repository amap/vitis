/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

// AxisReference.cpp: implementation of the AxisReference class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "AxisReference.h"
#include "VitisConfig.h"
#include "AmapConfig.h"
#include "defAMAP.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


AxisReference::AxisReference(std::string paramFileN):AmapSimParamFile(paramFileN)
{
    nbLPAxe=5;
	PLTypeAuthorized=NULL;
	ramificationModelType=NULL;
	set = false;

cout << "read ";
cout << paramFileN;
cout << std::endl;

	readAxis();
}

AxisReference::~AxisReference()
{
	for(int i=0; i<nbLPAxe; i++)
	{
		delete [] ramificationModelType[i];
		delete [] PLTypeAuthorized[i];
		delete [] lpName[i];
	}

	delete [] ramificationModelType;
	ramificationModelType = 0;
	delete [] PLTypeAuthorized;
	PLTypeAuthorized = 0;
	delete [] lpName;
	lpName = 0;
}

float AxisReference::valSingleParameter (int numvar/* parameter index */, BrancAMAP *b/* bud that is currently treated */)
{
	float val = axe[numvar].ymin;
//	AmapsimSignalInterface s;
	if (b)
		val = signal.sigSingleParameter(numvar, val, b);
	return val;
}

/* get the value of a parameter inside a parametric 2d curve */
float AxisReference::val1DParameter (int numvar, /* parameter index */
		int pos, /* index we are interested in */
		BrancAMAP *b,/* bud that is currently treated */
		int sig)
{
	float val = val1DParameter (numvar, pos);
	if (sig && b)
	{
//		AmapsimSignalInterface s;
		val = signal.sig1DParameter(numvar, val, b);
	}
	return val;
}

/* get the value of a parameter inside a parametric 2d curve */
float AxisReference::val1DParameter (int numvar, /* parameter index */
		int pos)
{
	int		i;
	AmapSimVariable    *var;
	float         val;

	/* gets the correct array */
	var = &axe[numvar];

	/* if we already seeked for that value last time directly return it */
	if (pos == var->currentPosition)
	{
		val = var->currentValue;
		return (val);
	}
	/* if pos < smaller x axis value */
	else if (pos <= var->currentX[0])
		var->currentValue = var->currentY[0];
	/* if pos > greater x axis value */
	else if (pos >= var->currentX[var->nb_pos_current-1])
		var->currentValue = var->currentY[var->nb_pos_current-1];
	/* get the right value */
	else
	{
		for (i=1; i<var->nb_pos_current; i++)
			if (var->currentX[i] >= pos)
				break;
		if (var->interpolation_position_current == 0)
		{
			if (var->currentX[i] == pos)
				var->currentValue = var->currentY[i];
			else
				var->currentValue = var->currentY[i-1];
		}
		else
			var->currentValue = var->currentY[i-1] + (var->currentY[i]-var->currentY[i-1]) * (pos-var->currentX[i-1]) / (var->currentX[i]-var->currentX[i-1]);
	}

	var->currentPosition = pos;
	val = var->currentValue;

	/* return the value */
	return val;
}


/* set the value of a parameter inside a parametric 2d curve */
void AxisReference::set1DParameter (int numvar, /* parameter index */
		int pos,
		float val)
{
	set1DParameter (numvar, pos, val, true);
}

void AxisReference::set1DParameter (int numvar, /* parameter index */
		int pos,
		float val,
		bool insert)
{
	int		ind, i;
	AmapSimVariable    *var;

	/* gets the correct array */
	var = &axe[numvar];
	var->set(pos, val, insert);

}


/* get the value of a parameter inside a parametric 3d curve */
float AxisReference::val2DParameter (int numvar, /* index for that parameter */
		int PhyAge, /* x axis value */
		int pos, /* y axis value */
		BrancAMAP *b) /* current bud */
{
	int		i;
	float 	val1, val2;
	AmapSimVariable    *var;
	float        val;

	var = &axe[numvar];

	if (   var->currentPhyAge == PhyAge
			&& var->currentPosition == pos)
	{
		val = var->currentValue;
		if (b)
		{
//			AmapsimSignalInterface s;
			val = signal.sig2DParameter(numvar, val, b);
		}
		return (val);
	}
	else if (   var->currentPhyAge == PhyAge
			&& var->interpolation_PhyAge == 0)
	{
		var->currentValue = val1DParameter (numvar, pos, b, 0);
		val = var->currentValue;
//		AmapsimSignalInterface s;
		if (b)
		{
			val = signal.sig2DParameter(numvar, val, b);
		}
		return (val);
	}
	else if (PhyAge <= var->num_PhyAge[0])
	{
		var->ind_PhyAge_current = 0;
		var->currentX= var->x[0];
		var->currentY = var->y[0];
		var->nb_pos_current = var->nb_pos[0];
		var->interpolation_position_current = var->interpolation_position[0];
		var->currentPosition = -1;
		var->currentValue = val1DParameter (numvar, pos, b, 0);
	}
	else if (PhyAge >= var->num_PhyAge[var->nb_PhyAge-1])
	{
		var->ind_PhyAge_current = var->nb_PhyAge-1;
		var->currentX= var->x[var->nb_PhyAge-1];
		var->currentY = var->y[var->nb_PhyAge-1];

		var->nb_pos_current = var->nb_pos[var->nb_PhyAge-1];
		var->interpolation_position_current = var->interpolation_position[var->nb_PhyAge-1];
		var->currentPosition = -1;
		var->currentValue = val1DParameter (numvar, pos, b, 0);
	}
	else
	{
		for (i=1; i<var->nb_PhyAge; i++)
			if (var->num_PhyAge[i] >= PhyAge)
				break;
		if (var->num_PhyAge[i] == PhyAge)

		{
			var->ind_PhyAge_current = i;
			var->currentX= var->x[i];
			var->currentY = var->y[i];
			var->nb_pos_current = var->nb_pos[i];
			var->interpolation_position_current = var->interpolation_position[i];
			var->currentPosition = -1;
			var->currentValue = val1DParameter (numvar, pos, b, 0);
		}
		else if (var->interpolation_PhyAge == 0)
		{
			var->ind_PhyAge_current = i-1;
			var->currentX= var->x[i-1];
			var->currentY = var->y[i-1];
			var->nb_pos_current = var->nb_pos[i-1];
			var->interpolation_position_current = var->interpolation_position[i-1];
			var->currentPosition = -1;
			var->currentValue = val1DParameter (numvar, pos, b, 0);
		}
		else
		{
			var->ind_PhyAge_current = i-1;
			var->currentX= var->x[i-1];
			var->currentY = var->y[i-1];
			var->nb_pos_current = var->nb_pos[i-1];
			var->interpolation_position_current = var->interpolation_position[i-1];
			var->currentPosition = -1;
			val1 = val1DParameter (numvar, pos, b, 0);
			var->ind_PhyAge_current = i;
			var->currentX= var->x[i];
			var->currentY = var->y[i];
			var->nb_pos_current = var->nb_pos[i];
			var->interpolation_position_current = var->interpolation_position[i];
			var->currentPosition = -1;
			val2 = val1DParameter (numvar, pos, b, 0);
			var->currentValue = val1 + (val2-val1) * (PhyAge-var->num_PhyAge[i-1]) / (var->num_PhyAge[i]-var->num_PhyAge[i-1]);
		}
	}

	var->currentPhyAge = PhyAge;
	var->currentPosition = pos;

	val = var->currentValue;
//	AmapsimSignalInterface s;
	if (b)
	{
		val = signal.sig2DParameter(numvar, val, b);
	}

	return (val);
}



/* set the value of a parameter inside a parametric 3d curve */
void AxisReference::set2DParameter (int numvar, /* index for that parameter */
		int PhyAge, /* x axis value */
		int pos, /* y axis value */
		float val) /* current value */
{
	set2DParameter (numvar, PhyAge, pos, val, true);
}
void AxisReference::set2DParameter (int numvar, /* index for that parameter */
		int PhyAge, /* x axis value */
		int pos, /* y axis value */
		float val,
		bool insert) /* current value */
{
	int		i;
	AmapSimVariable    *var;

	var = &axe[numvar];
	var->set (PhyAge, pos, val, insert);

}



int AxisReference::nextCtrlPoint2DParameter (int numvar, int pos)
{
	AmapSimVariable * var;
	int i;

	var = &axe[numvar];

	if(var->nb_pos_current == 1
			|| pos >= var->currentX[var->nb_pos_current-1])
		return 9999;

	/* get the right value */
	for(i=0; i<= var->nb_pos_current ; i++)
	{
		if(var->currentX[i] > pos)
			return var->currentX[i];
	}

	return 0;

}

void AxisReference::adjustNape (int index, int numzone, int numpl, int min, int max, float step)
{
	int i, ind;
	float v;

	if (numpl == -1)
		ind = index;
	else if (numzone == -1)
		ind = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*numpl+index;
	else
		ind = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*numpl+NB_GLOBAL_LP_VAR+numzone*NB_ZONE_VAR+index;

	for (i=min; i<max; i++)
	{
		v = val2DParameter (ind, i, 0, NULL);
		v += v*step;
		set2DParameter (ind, i, 0, v, false);
	}
}

void AxisReference::adjustAxe (int index, int numzone, int numpl, int min, int max, float step)
{
	int i, ind;
	float v;

   if (numpl == -1)
      ind = index;
	else if (numzone == -1)
		ind = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*numpl+index;
	else
		ind = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*numpl+NB_GLOBAL_LP_VAR+numzone*NB_ZONE_VAR+index;

	for (i=min; i<max; i++)
	{
		v = val1DParameter (ind, i, NULL);
		v += v*step;
		set1DParameter (ind, i, v, false);
	}
}

void AxisReference::adjustAxeProba (int index, int numzone, int numpl, int min, int max, float step)
{
	int i, ind;
	float v;

	if (numpl == -1)
      ind = index;
	else if (numzone == -1)
		ind = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*numpl+index;
	else
		ind = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*numpl+NB_GLOBAL_LP_VAR+numzone*NB_ZONE_VAR+index;

	for (i=min; i<max; i++)
	{
		v = val1DParameter (ind, i, NULL);
		v += v*step;
		v = adjustProba (v, step);
		set1DParameter (ind, i, v, false);
	}
}

float AxisReference::adjustProba (float val, int step)
{
	if (val > 1.0)
		val = 1.0;
	if (val <= 0)
		val = 0;
	if (   step > 0
		 && val < 0.1)
		val += 0.05;
	return val;
}


/******************************************************************************
  parameter file reading
 ******************************************************************************/
int  AxisReference::readAxis(void)
{

	long     i, j, nbent, index;
	float f;

	int sizefic;

	char ligne[256],str[100];

	this->set = false;
	this->open();

	sizefic=this->size();

	if (sizefic == -1)
	{
		cout << "failed opening" << std::endl;
		return 0;
	}


	/* parameter file version */
	fic->getline(ligne,255);
#ifdef WIN32
	sscanf_s (ligne, "#AMAPSIM parameter file Version %s", str, 99);
#else
	sscanf (ligne, "#AMAPSIM parameter file Version %s", str);
#endif
	if (strcmp (str, "2") != 0)
	{
		printf ("parameter file is not version 2 (%s)\n", str);
		printf ("use AmapSimEdit to convert it to the up-to-date version\n\n\n");
		return 0;
	}

	axe.resize(NB_VAR);

	/*   botanical variables reading */
	fic->getline(ligne,255);
	this->setName(std::string(ligne));

//	readSingleValue(axe[glnbcyc], 0);

//	readSingleValue(axe[glmaxref], 0);
	axe[glmaxref].ymin = 9999;

//	readSingleValue(axe[glmaxwr], 0);
	axe[glmaxwr].ymin = 9999;

	readSingleValue(axe[glmaxcalc], 0);

	readSingleValue(axe[glPhyAgeune], 0);

	readSingleValue(axe[glangins], 1);

	readSingleValue(axe[gllnginit], 1);

	readSingleValue(axe[gllnghoup], 1);

	readSingleValue(axe[glsurfhoup], 1);

//	readSingleValue(axe[glvitesse], 1);
	axe[glvitesse].ymin = 1;

//	nbent = (int)axe[glnbcyc].ymin;
	nbent = 2;

	nbLPAxe=(int)axe[gllnghoup].ymin;

	if (fabs(axe[glangins].ymin) < 0.00000001)
		axe[glangins].ymin = (float)0.00000001;
	axe[glangins].ymin *= FCONV;

	axe.resize(NB_VAR+NB_VAR_LP*nbLPAxe+1);

	read1DParameter(axe[rtmaxbrazero]);
	this->maxorder = 0;
	for (i=0; i<axe[rtmaxbrazero].nb_pos_current; i++)
	{
		if (axe[rtmaxbrazero].currentY[i] > this->maxorder)
			this->maxorder = (int)axe[rtmaxbrazero].currentY[i];
	}
	this->maxorder ++;

	read1DParameter(axe[rtmaxreiteration]);
	read1DParameter(axe[rtmaxanticipe]);

	read1DParameter(axe[rtnumsymb]);
	read1DParameter(axe[rtagelim]);

	read1DParameter(axe[rtmaxetap]);
	read1DParameter(axe[rtlagend]);
	read1DParameter(axe[rtlagdeath]);
	read2DParameter(axe[rtpbvie]);
	read1DParameter(axe[rtpruningcoef]);

	read1DParameter(axe[rtprogress]);

	read1DParameter(axe[rtrythme]);
	read1DParameter(axe[rtvitesse]);

	read1DParameter (axe[nbremain]);
	read1DParameter (axe[pbremain]);
	read1DParameter (axe[shiftremain]);
	read1DParameter (axe[nbtransition]);
	read1DParameter (axe[transitionfrequency1]);
	read1DParameter (axe[transitionjump1]);
	read1DParameter (axe[transitionfrequency2]);
	read1DParameter (axe[transitionjump2]);
	read1DParameter (axe[transitionfrequency3]);
	read1DParameter (axe[transitionjump3]);
	read1DParameter (axe[transitionfrequency4]);
	read1DParameter (axe[transitionjump4]);
	read1DParameter (axe[transitionfrequency5]);
	read1DParameter (axe[transitionjump5]);

	read1DParameter(axe[rtvitessepr]);
	read1DParameter(axe[rtprolgup]);
	read1DParameter(axe[rtprolgud]);
	read1DParameter(axe[rtproln]);
	read1DParameter(axe[rtprolp]);
	read1DParameter(axe[rtprold]);

	read1DParameter(axe[rtbasecompt]);
	read1DParameter(axe[rttypetop]);

	read1DParameter(axe[rtpbsympodeGu]);
	read1DParameter(axe[rtpbsympodeUC]);

	read1DParameter(axe[rtnben]);

	read1DParameter(axe[rtpolyini]);
	read1DParameter(axe[rtmonocyc]);
	read1DParameter(axe[rtpolycyc]);
	read1DParameter(axe[rt2cyc]);
	read1DParameter(axe[rt3cyc]);
	read1DParameter(axe[rt4cyc]);

	read1DParameter(axe[rtsleepcyc1]);
	read1DParameter(axe[rtneocyc1]);
	read1DParameter(axe[rtprefn1]);
	read1DParameter(axe[rtprefp1]);
	read1DParameter(axe[rtneod1]);
	read1DParameter(axe[rtneon1]);
	read1DParameter(axe[rtneop1]);
	read1DParameter(axe[rtneob1]);
	read1DParameter(axe[rtneopn1]);
	read1DParameter(axe[rtneopp1]);
	read1DParameter(axe[rtprefd1]);
	read1DParameter(axe[rtneopd1]);

	read1DParameter(axe[rtsleepcyc2]);
	read1DParameter(axe[rtneocyc2]);
	read1DParameter(axe[rtprefn2]);
	read1DParameter(axe[rtprefp2]);
	read1DParameter(axe[rtneod2]);
	read1DParameter(axe[rtneon2]);
	read1DParameter(axe[rtneop2]);
	read1DParameter(axe[rtneob2]);
	read1DParameter(axe[rtneopn2]);
	read1DParameter(axe[rtneopp2]);
	read1DParameter(axe[rtprefd2]);
	read1DParameter(axe[rtneopd2]);

	read1DParameter(axe[rtsleepcyca]);
	read1DParameter(axe[rtneocyca]);
	read1DParameter(axe[rtprefna]);
	read1DParameter(axe[rtprefpa]);
	read1DParameter(axe[rtneoda]);
	read1DParameter(axe[rtneona]);
	read1DParameter(axe[rtneopa]);
	read1DParameter(axe[rtneoba]);
	read1DParameter(axe[rtneopna]);
	read1DParameter(axe[rtneoppa]);
	read1DParameter(axe[rtprefda]);
	read1DParameter(axe[rtneopda]);

	read1DParameter(axe[rgtypins]);
	read1DParameter(axe[anginsinit],FCONV);
	read1DParameter(axe[deviation],FCONV);
	read1DParameter(axe[rgsympode],FCONV);
	read1DParameter(axe[rgyoung]);
	read1DParameter(axe[rginflexion]);
	read1DParameter(axe[rgconicite]);
	read1DParameter(axe[rgredres],FCONV);
	read1DParameter(axe[rgplagredre]);
	read1DParameter(axe[rgflexion], FCONV);

	read1DParameter(axe[rgaleatoire]);
	read1DParameter(axe[rgsensgeom]);

	read1DParameter(axe[lnginit]);
	read1DParameter(axe[reevollng]);
	read1DParameter(axe[renbtestlng]);
	read1DParameter(axe[relaglng]);
	read1DParameter(axe[reevolangins]);
	read1DParameter(axe[renbtestangins]);
	read1DParameter(axe[relagangins]);
	read1DParameter(axe[diaminit]);
	read1DParameter(axe[reevolgross]);
	read1DParameter(axe[renbtestgross]);
	read1DParameter(axe[relaggross]);
	read1DParameter(axe[reevolsymp],FCONV);
	read1DParameter(axe[renbtestsymp]);

	read2DParameter(axe[rgpcntlng]);
	read2DParameter(axe[rgpcntgross]);

	/*   lecture du detail entre_noeuds   */
	ramificationModelType=new int* [nbLPAxe];
	lpName=new char* [nbLPAxe];

	for(i=0; i<nbLPAxe; i++)
	{
		ramificationModelType[i] = new int [4];
		lpName[i] = new char [256];
	}

	for (i=0; i<nbLPAxe; i++)
	{
		index = NB_VAR+ NB_VAR_LP*i;

		fic->getline(lpName[i], 255);

		read1DParameter(axe[index+retype]);
		read1DParameter(axe[index+renbre]);
		read1DParameter(axe[index+repbaxil]);

		read1DParameter(axe[index+redebprod]);
		read1DParameter(axe[index+refinprod]);

		read1DParameter(axe[index+rgphase],FCONV);
		read1DParameter(axe[index+rgphyllotaxie],FCONV);

		read1DParameter(axe[index+resensbrancret]);
		read1DParameter(axe[index+resensinitret]);
		read1DParameter(axe[index+reDeathInfluence]);

		*fic>>ramificationModelType[i][0]>>ramificationModelType[i][1]>>ramificationModelType[i][2]>>ramificationModelType[i][3];
		fic->getline(ligne, 255);

		for (j=0; j<nbent; j++)
		{
			index = NB_VAR + NB_VAR_LP*i + NB_GLOBAL_LP_VAR + j*NB_ZONE_VAR;
			read2DParameter(axe[index+rcsautramiret1]);
			read2DParameter(axe[index+rcnbvarisautret1]);
			read2DParameter(axe[index+rcpbvarisautret1]);
			read2DParameter(axe[index+rcrythminitret1]);
			read2DParameter(axe[index+rcsautramiret2]);
			read2DParameter(axe[index+rcnbvarisautret2]);
			read2DParameter(axe[index+rcpbvarisautret2]);
			read2DParameter(axe[index+rcrythminitret2]);
			read2DParameter(axe[index+rcpbinitret0]);
			read2DParameter(axe[index+rcpbinitret1]);
			read2DParameter(axe[index+rcpb_st0]);
			read2DParameter(axe[index+rcpb_0_1]);
			read2DParameter(axe[index+rcpb_st1]);
			read2DParameter(axe[index+rcpb_1_0]);
			read2DParameter(axe[index+rcpb_st2]);
			read2DParameter(axe[index+rcpb_2_0]);

			read2DParameter(axe[index+rcsautramiant]);
			read2DParameter(axe[index+rcnbvarisautant]);
			read2DParameter(axe[index+rcpbvarisautant]);
			read2DParameter(axe[index+rcrythminitant]);
			read2DParameter(axe[index+rcpbinitant]);
			read2DParameter(axe[index+rcpb_nbant]);
			read2DParameter(axe[index+rcpb_bant]);

			read2DParameter(axe[index+rcsautramiretc]);
			read2DParameter(axe[index+rcnbvarisautretc]);
			read2DParameter(axe[index+rcpbvarisautretc]);
			read2DParameter(axe[index+rcrythminitretc]);
			read2DParameter(axe[index+rcpbinitretcyc]);
			read2DParameter(axe[index+rcpb_nbretcyc]);
			read2DParameter(axe[index+rcpb_bretcyc]);

			read2DParameter(axe[index+rcpbinittrau]);
			read2DParameter(axe[index+rcpb_nbtrau]);
			read2DParameter(axe[index+rcpb_btrau]);
			read2DParameter(axe[index+rcsautramitrau]);
			read2DParameter(axe[index+rcnbvarisauttrau]);
			read2DParameter(axe[index+rcpbvarisauttrau]);
			read2DParameter(axe[index+rcrythminittrau]);

			read2DParameter (axe[index+rcnb_st0]);
			read2DParameter (axe[index+rcd_st0]);
			read2DParameter (axe[index+rcnb_st1]);
			read2DParameter (axe[index+rcd_st1]);
			read2DParameter (axe[index+rcnb_st2]);
			read2DParameter (axe[index+rcd_st2]);
			read2DParameter (axe[index+rcnb_bant]);
			read2DParameter (axe[index+rcd_bant]);
			read2DParameter (axe[index+rcnb_nbant]);
			read2DParameter (axe[index+rcd_nbant]);
			read2DParameter (axe[index+rcnb_btrau]);
			read2DParameter (axe[index+rcd_btrau]);
			read2DParameter (axe[index+rcnb_nbtrau]);
			read2DParameter (axe[index+rcd_nbtrau]);
			read2DParameter (axe[index+rcnb_bretcyc]);
			read2DParameter (axe[index+rcd_bretcyc]);
			read2DParameter (axe[index+rcnb_nbretcyc]);
			read2DParameter (axe[index+rcd_nbretcyc]);

		}
	}

#if 0
	6FrE4b2
	/* JFB 21/03/05 for further add to reference axis structure */
	if (sizefic-fic->tellg() < 100)
	{
//		printf ("pas de markoveries de differenciation de mes deux, end of the show\n");
//		defaultAxe (&axe[nbremain], -1);
		defaultAxe (&axe[nbremain], 1);
		copyAxe (axe[rtprogress], &axe[pbremain]);
		for (i=0; i<axe[pbremain].nb_pos_current; i++)
		{
			f = axe[pbremain].currentY[i];
			f = 1. - f;
			axe[pbremain].currentY[i] = f;
		}
		defaultAxe (&axe[nbtransition], 1);
		defaultAxe (&axe[transitionfrequency1], 1);
		copyAxe (axe[NB_VAR+revitesse], &axe[transitionjump1]);
		defaultAxe (&axe[transitionfrequency2], 0);
		defaultAxe (&axe[transitionjump2], 0);
		defaultAxe (&axe[transitionfrequency3], 0);
		defaultAxe (&axe[transitionjump3], 0);
		defaultAxe (&axe[transitionfrequency4], 0);
		defaultAxe (&axe[transitionjump4], 0);
		defaultAxe (&axe[transitionfrequency5], 0);
		defaultAxe (&axe[transitionjump5], 0);
		defaultAxe (&axe[shiftremain], 0);
		defaultAxe (&axe[rtprefd1], 0);
		defaultAxe (&axe[rtneopd1], 0);
		defaultAxe (&axe[rtprefd2], 0);
		defaultAxe (&axe[rtneopd2], 0);
		defaultAxe (&axe[rtprefd3], 0);
		defaultAxe (&axe[rtneopd3], 0);
		for (j=0; j<nbent; j++)
		{
			for (i=0; i<nbLPAxe; i++)
			{
				index = NB_VAR+(nbLPAxe*j+i)*NB_VAR_ENT;
				defaultNape (&axe[index+rcnb_st0], -1);
				defaultNape (&axe[index+rcd_st0], 0);
				defaultNape (&axe[index+rcnb_st1], -1);
				defaultNape (&axe[index+rcd_st1], 0);
				defaultNape (&axe[index+rcnb_st2], -1);
				defaultNape (&axe[index+rcd_st2], 0);
				defaultNape (&axe[index+rcnb_bant], -1);
				defaultNape (&axe[index+rcd_bant], 0);
				defaultNape (&axe[index+rcnb_nbant], -1);
				defaultNape (&axe[index+rcd_nbant], 0);
				defaultNape (&axe[index+rcnb_btrau], -1);
				defaultNape (&axe[index+rcd_btrau], 0);
				defaultNape (&axe[index+rcnb_nbtrau], -1);
				defaultNape (&axe[index+rcd_nbtrau], 0);
				defaultNape (&axe[index+rcnb_bretcyc], -1);
				defaultNape (&axe[index+rcd_bretcyc], 0);
				defaultNape (&axe[index+rcnb_nbretcyc], -1);
				defaultNape (&axe[index+rcd_nbretcyc], 0);
			}
		}
		for (i=0; i<nbLPAxe; i++)
			for (j=0; j<4; j++)
				ramificationModelType[i][j] = MARKOV;

	}
	// introduction d'une loi de metamorphose de type semi Markov
	else
	{

		read1DParameter (axe[nbremain]);
		read1DParameter (axe[pbremain]);
		read1DParameter (axe[nbtransition]);
		read1DParameter (axe[transitionfrequency1]);
		read1DParameter (axe[transitionjump1]);
		read1DParameter (axe[transitionfrequency2]);
		read1DParameter (axe[transitionjump2]);
		read1DParameter (axe[transitionfrequency3]);
		read1DParameter (axe[transitionjump3]);
		read1DParameter (axe[transitionfrequency4]);
		read1DParameter (axe[transitionjump4]);
		read1DParameter (axe[transitionfrequency5]);
		read1DParameter (axe[transitionjump5]);
		read1DParameter (axe[shiftremain]);
		read1DParameter (axe[rtprefd1]);
		read1DParameter (axe[rtneopd1]);
		read1DParameter (axe[rtprefd2]);
		read1DParameter (axe[rtneopd2]);
		read1DParameter (axe[rtprefd3]);
		read1DParameter (axe[rtneopd3]);
		for (j=0; j<nbent; j++)
		{
			for (i=0; i<nbLPAxe; i++)
			{
				index = NB_VAR+(nbLPAxe*j+i)*NB_VAR_ENT;
				read2DParameter (axe[index+rcnb_st0]);
				read2DParameter (axe[index+rcd_st0]);
				read2DParameter (axe[index+rcnb_st1]);
				read2DParameter (axe[index+rcd_st1]);
				read2DParameter (axe[index+rcnb_st2]);
				read2DParameter (axe[index+rcd_st2]);
				read2DParameter (axe[index+rcnb_bant]);
				read2DParameter (axe[index+rcd_bant]);
				read2DParameter (axe[index+rcnb_nbant]);
				read2DParameter (axe[index+rcd_nbant]);
				read2DParameter (axe[index+rcnb_btrau]);
				read2DParameter (axe[index+rcd_btrau]);
				read2DParameter (axe[index+rcnb_nbtrau]);
				read2DParameter (axe[index+rcd_nbtrau]);
				read2DParameter (axe[index+rcnb_bretcyc]);
				read2DParameter (axe[index+rcd_bretcyc]);
				read2DParameter (axe[index+rcnb_nbretcyc]);
				read2DParameter (axe[index+rcd_nbretcyc]);
			}
		}
		for (i=0; i<nbLPAxe; i++)
			*fic>>ramificationModelType[i][0]>>ramificationModelType[i][1]>>ramificationModelType[i][2]>>ramificationModelType[i][3];
	}

	//   verif_topo ((int)axe[glmaxref].ymin, nbent);

	if (nbent == 1)
	{
		verif_nbentn (axe[rtneocyc1]);
		verif_nbentn (axe[rtneocyc2]);
		verif_nbentn (axe[rtneocyca]);
	}
#endif

	PLTypeAuthorized = new char* [nbLPAxe ];

	for (i=0; i<nbLPAxe; i++)
	{
		PLTypeAuthorized[i] = new char [4];
		{
			PLTypeAuthorized[i][0] = 0;
			PLTypeAuthorized[i][1] = 0;
			PLTypeAuthorized[i][2] = 0;
			PLTypeAuthorized[i][3] = 0;
			for (j=0; j<nbent; j++)
			{
				index = NB_VAR+NB_VAR_LP*i+NB_GLOBAL_LP_VAR+j*NB_ZONE_VAR;
				if (uniqueValueAxe(axe[index+repbaxil], 0))
					continue;

				if (   !uniqueValueNape(axe[index+rcpbinitret0], 1)
						|| !uniqueValueNape(axe[index+rcpbinitret1], 0)
						|| !uniqueValueNape(axe[index+rcpb_st0], 1)
						|| !uniqueValueNape(axe[index+rcpb_0_1], 0)
						|| !uniqueValueNape(axe[index+rcpb_st1], 0)
						|| !uniqueValueNape(axe[index+rcpb_1_0], 1)
						|| !uniqueValueNape(axe[index+rcpb_st2], 0)
						|| !uniqueValueNape(axe[index+rcpb_2_0], 1))
					PLTypeAuthorized[i][0] = 1;
				if (   !uniqueValueNape(axe[index+rcpbinitant], 0)
						|| !uniqueValueNape(axe[index+rcpb_nbant], 0)
						|| !uniqueValueNape(axe[index+rcpb_bant], 1))
					PLTypeAuthorized[i][1] = 1;
				if (   !uniqueValueNape(axe[index+rcpbinitretcyc], 0)
						|| !uniqueValueNape(axe[index+rcpb_nbretcyc], 0)
						|| !uniqueValueNape(axe[index+rcpb_bretcyc], 1))
					PLTypeAuthorized[i][2] = 1;
				if (   !uniqueValueNape(axe[index+rcpbinittrau], 0)
						|| !uniqueValueNape(axe[index+rcpb_nbtrau], 0)
						|| !uniqueValueNape(axe[index+rcpb_btrau], 1))
					PLTypeAuthorized[i][3] = 1;
			}
		}
	}


	/* return the reference axis for that specie */
	listaxe.push_back(&axe);

	fic->close();

	this->set = true;

	return 1;
}

/******************************************************************************
  parameter file writing
 ******************************************************************************/
int  AxisReference::writeAxis(void)
{

	long     i, j, nbent, index;
	float f;

	int sizefic;

	char ligne[256],str[100];

	if (!this->set)
		return 0;

	if (fic)
	{
		delete fic;
		open(std::ios::out);
	}
	else
		open(ios::out);

	/* parameter file version */

	*fic << "#AMAPSIM parameter file Version 2" << std::endl;

	/*   botanical variables */
	*fic << getName() << std::endl;

	*fic << axe[glmaxcalc].ymin << std::endl;

	*fic << axe[glPhyAgeune].ymin<< std::endl;

	*fic << axe[glangins].ymin << std::endl;

	*fic << axe[gllnginit].ymin << std::endl;

	*fic << axe[gllnghoup].ymin << std::endl;

	*fic << axe[glsurfhoup].ymin << std::endl;

	write1DParameter(axe[rtmaxbrazero]);

	write1DParameter(axe[rtmaxreiteration]);
	write1DParameter(axe[rtmaxanticipe]);

	write1DParameter(axe[rtnumsymb]);
	write1DParameter(axe[rtagelim]);

	write1DParameter(axe[rtmaxetap]);
	write1DParameter(axe[rtlagend]);
	write1DParameter(axe[rtlagdeath]);
	write2DParameter(axe[rtpbvie]);
	write1DParameter(axe[rtpruningcoef]);

	write1DParameter(axe[rtprogress]);

	write1DParameter(axe[rtrythme]);
	write1DParameter(axe[rtvitesse]);

	write1DParameter (axe[nbremain]);
	write1DParameter (axe[pbremain]);
	write1DParameter (axe[shiftremain]);
	write1DParameter (axe[nbtransition]);
	write1DParameter (axe[transitionfrequency1]);
	write1DParameter (axe[transitionjump1]);
	write1DParameter (axe[transitionfrequency2]);
	write1DParameter (axe[transitionjump2]);
	write1DParameter (axe[transitionfrequency3]);
	write1DParameter (axe[transitionjump3]);
	write1DParameter (axe[transitionfrequency4]);
	write1DParameter (axe[transitionjump4]);
	write1DParameter (axe[transitionfrequency5]);
	write1DParameter (axe[transitionjump5]);

	write1DParameter(axe[rtvitessepr]);
	write1DParameter(axe[rtprolgup]);
	write1DParameter(axe[rtprolgud]);
	write1DParameter(axe[rtproln]);
	write1DParameter(axe[rtprolp]);
	write1DParameter(axe[rtprold]);

	write1DParameter(axe[rtbasecompt]);
	write1DParameter(axe[rttypetop]);

	write1DParameter(axe[rtpbsympodeGu]);
	write1DParameter(axe[rtpbsympodeUC]);

	write1DParameter(axe[rtnben]);

	write1DParameter(axe[rtpolyini]);
	write1DParameter(axe[rtmonocyc]);
	write1DParameter(axe[rtpolycyc]);
	write1DParameter(axe[rt2cyc]);
	write1DParameter(axe[rt3cyc]);
	write1DParameter(axe[rt4cyc]);

	write1DParameter(axe[rtsleepcyc1]);
	write1DParameter(axe[rtneocyc1]);
	write1DParameter(axe[rtprefn1]);
	write1DParameter(axe[rtprefp1]);
	write1DParameter(axe[rtneod1]);
	write1DParameter(axe[rtneon1]);
	write1DParameter(axe[rtneop1]);
	write1DParameter(axe[rtneob1]);
	write1DParameter(axe[rtneopn1]);
	write1DParameter(axe[rtneopp1]);
	write1DParameter(axe[rtprefd1]);
	write1DParameter(axe[rtneopd1]);

	write1DParameter(axe[rtsleepcyc2]);
	write1DParameter(axe[rtneocyc2]);
	write1DParameter(axe[rtprefn2]);
	write1DParameter(axe[rtprefp2]);
	write1DParameter(axe[rtneod2]);
	write1DParameter(axe[rtneon2]);
	write1DParameter(axe[rtneop2]);
	write1DParameter(axe[rtneob2]);
	write1DParameter(axe[rtneopn2]);
	write1DParameter(axe[rtneopp2]);
	write1DParameter(axe[rtprefd2]);
	write1DParameter(axe[rtneopd2]);

	write1DParameter(axe[rtsleepcyca]);
	write1DParameter(axe[rtneocyca]);
	write1DParameter(axe[rtprefna]);
	write1DParameter(axe[rtprefpa]);
	write1DParameter(axe[rtneoda]);
	write1DParameter(axe[rtneona]);
	write1DParameter(axe[rtneopa]);
	write1DParameter(axe[rtneoba]);
	write1DParameter(axe[rtneopna]);
	write1DParameter(axe[rtneoppa]);
	write1DParameter(axe[rtprefda]);
	write1DParameter(axe[rtneopda]);

	write1DParameter(axe[rgtypins]);
	write1DParameter(axe[anginsinit],1./FCONV);
	write1DParameter(axe[deviation],1./FCONV);
	write1DParameter(axe[rgsympode],1./FCONV);
	write1DParameter(axe[rgyoung]);
	write1DParameter(axe[rginflexion]);
	write1DParameter(axe[rgconicite]);
	write1DParameter(axe[rgredres],1./FCONV);
	write1DParameter(axe[rgplagredre]);
	write1DParameter(axe[rgflexion], 1./FCONV);

	write1DParameter(axe[rgaleatoire]);
	write1DParameter(axe[rgsensgeom]);

	write1DParameter(axe[lnginit]);
	write1DParameter(axe[reevollng]);
	write1DParameter(axe[renbtestlng]);
	write1DParameter(axe[relaglng]);
	write1DParameter(axe[reevolangins]);
	write1DParameter(axe[renbtestangins]);
	write1DParameter(axe[relagangins]);
	write1DParameter(axe[diaminit]);
	write1DParameter(axe[reevolgross]);
	write1DParameter(axe[renbtestgross]);
	write1DParameter(axe[relaggross]);
	write1DParameter(axe[reevolsymp],1./FCONV);
	write1DParameter(axe[renbtestsymp]);

	write2DParameter(axe[rgpcntlng]);
	write2DParameter(axe[rgpcntgross]);

	for (i=0; i<nbLPAxe; i++)
	{
		index = NB_VAR+ NB_VAR_LP*i;

		*fic << lpName[i] << std::endl;

		write1DParameter(axe[index+retype]);
		write1DParameter(axe[index+renbre]);
		write1DParameter(axe[index+repbaxil]);

		write1DParameter(axe[index+redebprod]);
		write1DParameter(axe[index+refinprod]);

		write1DParameter(axe[index+rgphase],1./FCONV);
		write1DParameter(axe[index+rgphyllotaxie],1./FCONV);

		write1DParameter(axe[index+resensbrancret]);
		write1DParameter(axe[index+resensinitret]);
		write1DParameter(axe[index+reDeathInfluence]);

		*fic<< ramificationModelType[i][0] << " " << ramificationModelType[i][1] << " " << ramificationModelType[i][2] << " " << ramificationModelType[i][3] << std::endl;

		for (j=0; j<2; j++)
		{
			index = NB_VAR + NB_VAR_LP*i + NB_GLOBAL_LP_VAR + j*NB_ZONE_VAR;
			write2DParameter(axe[index+rcsautramiret1]);
			write2DParameter(axe[index+rcnbvarisautret1]);
			write2DParameter(axe[index+rcpbvarisautret1]);
			write2DParameter(axe[index+rcrythminitret1]);
			write2DParameter(axe[index+rcsautramiret2]);
			write2DParameter(axe[index+rcnbvarisautret2]);
			write2DParameter(axe[index+rcpbvarisautret2]);
			write2DParameter(axe[index+rcrythminitret2]);
			write2DParameter(axe[index+rcpbinitret0]);
			write2DParameter(axe[index+rcpbinitret1]);
			write2DParameter(axe[index+rcpb_st0]);
			write2DParameter(axe[index+rcpb_0_1]);
			write2DParameter(axe[index+rcpb_st1]);
			write2DParameter(axe[index+rcpb_1_0]);
			write2DParameter(axe[index+rcpb_st2]);
			write2DParameter(axe[index+rcpb_2_0]);

			write2DParameter(axe[index+rcsautramiant]);
			write2DParameter(axe[index+rcnbvarisautant]);
			write2DParameter(axe[index+rcpbvarisautant]);
			write2DParameter(axe[index+rcrythminitant]);
			write2DParameter(axe[index+rcpbinitant]);
			write2DParameter(axe[index+rcpb_nbant]);
			write2DParameter(axe[index+rcpb_bant]);

			write2DParameter(axe[index+rcsautramiretc]);
			write2DParameter(axe[index+rcnbvarisautretc]);
			write2DParameter(axe[index+rcpbvarisautretc]);
			write2DParameter(axe[index+rcrythminitretc]);
			write2DParameter(axe[index+rcpbinitretcyc]);
			write2DParameter(axe[index+rcpb_nbretcyc]);
			write2DParameter(axe[index+rcpb_bretcyc]);

			write2DParameter(axe[index+rcpbinittrau]);
			write2DParameter(axe[index+rcpb_nbtrau]);
			write2DParameter(axe[index+rcpb_btrau]);
			write2DParameter(axe[index+rcsautramitrau]);
			write2DParameter(axe[index+rcnbvarisauttrau]);
			write2DParameter(axe[index+rcpbvarisauttrau]);
			write2DParameter(axe[index+rcrythminittrau]);

			write2DParameter (axe[index+rcnb_st0]);
			write2DParameter (axe[index+rcd_st0]);
			write2DParameter (axe[index+rcnb_st1]);
			write2DParameter (axe[index+rcd_st1]);
			write2DParameter (axe[index+rcnb_st2]);
			write2DParameter (axe[index+rcd_st2]);
			write2DParameter (axe[index+rcnb_bant]);
			write2DParameter (axe[index+rcd_bant]);
			write2DParameter (axe[index+rcnb_nbant]);
			write2DParameter (axe[index+rcd_nbant]);
			write2DParameter (axe[index+rcnb_btrau]);
			write2DParameter (axe[index+rcd_btrau]);
			write2DParameter (axe[index+rcnb_nbtrau]);
			write2DParameter (axe[index+rcd_nbtrau]);
			write2DParameter (axe[index+rcnb_bretcyc]);
			write2DParameter (axe[index+rcd_bretcyc]);
			write2DParameter (axe[index+rcnb_nbretcyc]);
			write2DParameter (axe[index+rcd_nbretcyc]);

		}
	}

	fic->close();

	return 1;
}



void AxisReference::defaultNape (AmapSimVariable *axe, float val)
{
	axe->interpolation_PhyAge = 0;
	axe->nb_PhyAge = 1;

	axe->num_PhyAge.push_back(0);

	axe->nb_pos.push_back(1);

	axe->interpolation_position.push_back(0);

	axe->x.resize(axe->nb_PhyAge);
	axe->y.resize(axe->nb_PhyAge);

	axe->x[0].push_back(0);
	axe->y[0].push_back(val);

	axe->interpolation_position_current = 0;
	axe->nb_pos_current = 1;

	axe->currentX = axe->x[0];
	axe->currentY = axe->y[0];
	axe->ymin = axe->ymax = 0;
	axe->currentPosition = axe->currentPhyAge = -1;
}

void  AxisReference::defaultAxe (AmapSimVariable *axe, float val)
{
	axe->interpolation_position_current = 0;
	axe->nb_pos_current = 1;

	axe->currentX.push_back(0);
	axe->currentY.push_back(val);
	axe->ymin = axe->ymax = 0;
	axe->currentPosition = axe->currentPhyAge = -1;
}

void  AxisReference::copyAxe (AmapSimVariable src, AmapSimVariable *dst)
{

	dst->interpolation_position_current = src.interpolation_position_current;
	dst->nb_pos_current = src.nb_pos_current;


	dst->currentX=src.currentX;
	dst->currentY=src.currentY;


	dst->ymin = src.ymin;
	dst->ymax = src.ymax;

	dst->currentPhyAge = src.currentPhyAge;
	dst->currentPosition = src.currentPosition;

}





/* compute the max number of test to be tested inside a growth unit */
void AxisReference::calcule_maxtest (long maxref)
{
	long     i,nbcyc,maxcyc,borntest;
	long     rtmaxtest;
	float    som;

	for (i=0; i<maxref; i++)
	{
		/* max number of cycle */
		if (   val1DParameter (rtpolyini, i, NULL) == 1.
				&& val1DParameter (rtmonocyc, i, NULL) == 1.)
			maxcyc = 1;
		else
		{
			som = 0;
			som += val1DParameter (rt2cyc, i, NULL);
			if (som >= 1.)
				maxcyc = 2;
			else
			{
				som += val1DParameter (rt3cyc, i, NULL);
				if (som >= 1.)
					maxcyc = 3;
				else
				{
					som += val1DParameter (rt4cyc, i, NULL);
					if (som >= 1.)
						maxcyc = 4;
					else
						maxcyc = 5;
				}
			}
		}

		nbcyc = 0 ;

		/* max number of test according to the max number of cycle */
		rtmaxtest = 0;
		borntest = (int)val1DParameter (rtnben, i, NULL);
		//switch (maxcyc)
		//{
		//	case 5:
		//		borntest += (int)val1DParameter (rtnben5, i, NULL);
		//	case 4:
		//		borntest += (int)val1DParameter (rtnben4, i, NULL);
		//	case 3:
		//		borntest += (int)val1DParameter (rtnben3, i, NULL);
		//	case 2:
		//		borntest += (int)val1DParameter (rtnben2, i, NULL);
		//		break;
		//}

		do
		{
			if (rtmaxtest == 0)
			{
				if (val1DParameter (rtproln, i, NULL) >= 0.)
					rtmaxtest += (int)val1DParameter (rtproln, i, NULL) + (int)val1DParameter (rtprold, i, NULL);
				else
				{
					rtmaxtest = borntest;
					break;
				}
			}

			if (nbcyc == 0)
			{
				if (val1DParameter (rtneocyc1, i, NULL) == 1.)
				{
					if (   val1DParameter (rtneon1, i, NULL) >= 0.
							&& val1DParameter (rtneopn1, i, NULL) >= 0.)
						rtmaxtest = rtmaxtest + (int)val1DParameter (rtneon1, i, NULL) + (int)val1DParameter (rtneod1, i, NULL) + (int)val1DParameter (rtneopn1, i, NULL);
					else
					{
						rtmaxtest = borntest;
						break;
					}
				}
				else if (val1DParameter (rtneocyc1, i, NULL) == 0.)
				{
					if (val1DParameter (rtprefn1, i, NULL) >= 0.)
						rtmaxtest = rtmaxtest + (int)val1DParameter (rtprefn1, i, NULL);
					else
					{
						rtmaxtest = borntest;
						break;
					}
				}
				else
				{
					if (   val1DParameter (rtneon1, i, NULL) >= 0.
							&& val1DParameter (rtneopn1, i, NULL) >= 0.
							&& val1DParameter (rtprefn1, i, NULL) >= 0.)
					{
						if (val1DParameter (rtneon1, i, NULL) + val1DParameter (rtneod1, i, NULL) + val1DParameter (rtneopn1, i, NULL)> val1DParameter (rtprefn1, i, NULL))
							rtmaxtest = rtmaxtest + (int)val1DParameter (rtneon1, i, NULL) + (int)val1DParameter (rtneod1, i, NULL) + (int)val1DParameter (rtneopn1, i, NULL);
						else
							rtmaxtest = rtmaxtest + (int)val1DParameter (rtprefn1, i, NULL);
					}
					else
					{
						rtmaxtest = borntest;
						break;
					}
				}
			}
			else if (nbcyc == 1)
			{
				if (val1DParameter (rtneocyc2, i, NULL) == 1.)
				{
					if (   val1DParameter (rtneon2, i, NULL) >= 0.
							&& val1DParameter (rtneopn2, i, NULL) >= 0.)
						rtmaxtest = rtmaxtest + (int)val1DParameter (rtneon2, i, NULL) + (int)val1DParameter (rtneod2, i, NULL) + (int)val1DParameter (rtneopn2, i, NULL);
					else
					{
						rtmaxtest = borntest;
						break;
					}
				}
				else if (val1DParameter (rtneocyc2, i, NULL) == 0.)
				{
					if (val1DParameter (rtprefn2, i, NULL) >= 0.)
						rtmaxtest = rtmaxtest + (int)val1DParameter (rtprefn2, i, NULL);
					else
					{
						rtmaxtest = borntest;
						break;
					}
				}
				else
				{
					if (   val1DParameter (rtneon2, i, NULL) >= 0.
							&& val1DParameter (rtneopn2, i, NULL) >= 0.
							&& val1DParameter (rtprefn2, i, NULL) >= 0.)
					{
						if (val1DParameter (rtneon2, i, NULL) + val1DParameter (rtneod2, i, NULL) + val1DParameter (rtneopn2, i, NULL)>
								val1DParameter (rtprefn2, i, NULL))
							rtmaxtest = rtmaxtest + (long)val1DParameter (rtneon2, i, NULL) + (int)val1DParameter (rtneod2, i, NULL) + (int)val1DParameter (rtneopn2, i, NULL);
						else
							rtmaxtest = rtmaxtest + (long)val1DParameter (rtprefn2, i, NULL);
					}
					else
					{
						rtmaxtest = borntest;
						break;
					}
				}
			}
			else
			{
				if (val1DParameter (rtneocyca, i, NULL) == 1.)
				{
					if (   val1DParameter (rtneona, i, NULL) >= 0.
							&& val1DParameter (rtneopna, i, NULL) >= 0.)
						rtmaxtest = rtmaxtest + (long)val1DParameter (rtneona, i, NULL) + (int)val1DParameter (rtneoda, i, NULL) + (int)val1DParameter (rtneopna, i, NULL);
					else
					{
						rtmaxtest = borntest;
						break;
					}
				}
				else if (val1DParameter (rtneocyca, i, NULL) == 0.)
				{
					if (val1DParameter (rtprefna, i, NULL) >= 0.)
						rtmaxtest = rtmaxtest + (long)val1DParameter (rtprefna, i, NULL);
					else
					{
						rtmaxtest = borntest;
						break;
					}
				}
				else
				{
					if (   val1DParameter (rtneona, i, NULL) >= 0.
							&& val1DParameter (rtneopna, i, NULL) >= 0.
							&& val1DParameter (rtprefna, i, NULL) >= 0.)
					{
						if (val1DParameter (rtneona, i, NULL) + val1DParameter (rtneoda, i, NULL) + val1DParameter (rtneopna, i, NULL)> val1DParameter (rtprefna, i, NULL))
							rtmaxtest = rtmaxtest + (long)val1DParameter (rtneona, i, NULL) + (int)val1DParameter (rtneoda, i, NULL) + (int)val1DParameter (rtneopna, i, NULL);
						else
							rtmaxtest = rtmaxtest + (long)val1DParameter (rtprefna, i, NULL);
					}
					else
					{
						rtmaxtest = borntest;
						break;
					}
				}
			}

			nbcyc ++;
		} while (nbcyc < maxcyc);

		if (borntest < rtmaxtest)
			rtmaxtest = borntest;
	}
}

void AxisReference::verif_topo (long  maxref,
		long  nbent)
{
	long     i,j,k;
	long      erreur;

	erreur = 0;
	for (j=0; j<nbLPAxe; j++)
	{
			int ind = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*j;
			for (i=0; i<maxref; i++)
			{
				if (   val1DParameter (rttypetop, i, NULL)       == TOPOAXE
						&& val1DParameter (ind+resensbrancret, i, NULL) == APICAL)
				{
					//printf (aconfig->Tabmess[5],i,j,k);
					erreur = 1;
				}
				if (   val1DParameter (rttypetop, i, NULL)       == TOPOAXE
						&& val1DParameter (ind+resensinitret, i, NULL) == APICAL)
				{
					//printf (aconfig->Tabmess[6],i,j,k);
					erreur = 1;
				}
				//if (   val1DParameter (rttypetop, i, NULL)       == TOPOAXE
				//		&& val1DParameter (NB_VAR+(nbLPAxe*k+j)*NB_VAR_ENT+resensinitant, i, NULL) == APICAL)
				//{
				//	//printf (aconfig->Tabmess[7],i,j,k);
				//	erreur = 1;
				//}
			}
	}
	if (erreur)
	{
		printf("et ta soeur\n\007");
		return;
	}
}

void AxisReference::verif_rythme (long ,
		int  pl,
		int  zoneIndex)
{
	long     i,j,index;

	index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*pl+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;

	for (i=0; i<axe[rtrythme].nb_pos_current; i++)
	{
		if (axe[rtrythme].currentY[i] == 0.0)
		{
			//         printf (aconfig->Tabmess[8],i+1, pl+1, axe[index+rerythme].currentX[i]+1);
			return;
		}
	}

	for (i=0; i<axe[index+rcrythminitret1].nb_PhyAge; i++)
	{
		for (j=0; j<axe[index+rcrythminitret1].nb_pos[i]; j++)
			if (axe[index+rcrythminitret1].y[i][j] == 0.0)
			{
				//printf (aconfig->Tabmess[8],i+1, pl+1, axe[index+rcrythminitret1].y[i][j]);
				return;
			}
	}

	for (i=0; i<axe[index+rcrythminitret2].nb_PhyAge; i++)
	{
		for (j=0; j<axe[index+rcrythminitret2].nb_pos[i]; j++)
			if (axe[index+rcrythminitret2].y[i][j] == 0.0)
			{
				//            printf (aconfig->Tabmess[8],i+1, pl+1, axe[index+rcrythminitret2].x[i][j]);
				return;
			}
	}

	for (i=0; i<axe[index+rcrythminitretc].nb_PhyAge; i++)
	{
		for (j=0; j<axe[index+rcrythminitretc].nb_pos[i]; j++)
			if (axe[index+rcrythminitretc].y[i][j] == 0.0)
			{
				//            printf (aconfig->Tabmess[8],i+1, pl+1, axe[index+rcrythminitretc].x[i][j]);
				return;
			}
	}

	for (i=0; i<axe[index+rcrythminitant].nb_PhyAge; i++)
	{
		for (j=0; j<axe[index+rcrythminitant].nb_pos[i]; j++)
			if (axe[index+rcrythminitant].y[i][j] == 0.0)
			{
				//            printf (aconfig->Tabmess[8],i+1, pl+1, axe[index+rcrythminitant].x[i][j]);
				return;
			}
	}

	for (i=0; i<axe[index+rcrythminittrau].nb_PhyAge; i++)
	{
		for (j=0; j<axe[index+rcrythminittrau].nb_pos[i]; j++)
			if (axe[index+rcrythminittrau].y[i][j] == 0.0)
			{
				//            printf (aconfig->Tabmess[8],i+1, pl+1, axe[index+rcrythminittrau].x[i][j]);
				return;
			}
	}
}

int AxisReference::verif_nbentn (AmapSimVariable var)
{
	int i;

	for (i=0; i<var.nb_pos_current; i++)
		if (var.currentY[i] != 0)
		{
			printf ("non null z2 apparition probability for single type node plant\n");
			return 0;
		}
	return 0;
}


int AxisReference::uniqueValueAxe(AmapSimVariable var, float val)
{
	int	i;

	for (i=0; i<var.nb_pos_current; i++)
		if (var.currentY[i] != val)
			return 0;
	return 1;
}

int AxisReference::uniqueValueNape(AmapSimVariable var, float val)
{
	int	i, j;

	for (i=0; i<var.nb_PhyAge; i++)
		for (j=0; j<var.nb_pos[i]; j++)
			if (var.y[i][j] != val)
				return 0;
	return 1;
}

