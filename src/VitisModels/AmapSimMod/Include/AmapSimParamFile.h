/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef _AMAPSIMPARAMFILE_H
#define _AMAPSIMPARAMFILE_H
#include "File.h"
#include "VitisObject.h"
#include "externAmapSim.h"
#include <fstream>
#include <vector>
using namespace std;


namespace Vitis
{
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///
	///			\class AmapSimVariable
	///			\brief Description of a parameter from reference axis
	///
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
	class AMAPSIM_EXPORT AmapSimVariable : public VitisObject{

		public:
			int		nb_PhyAge;                  /**< \brief number of Phyage  described */
			vector<int>	num_PhyAge;                /**< \brief list of described Phyage  */
			int		ind_PhyAge_current;         /**< \brief current Phyage  number */
			int		nb_pos_current;             /**< \brief current number of described positions in the current Phyage  */
			vector<int> currentX;                  /**< \brief x values for each Phyage  */
			vector<float> currentY;                  /**< \brief parameter values for each Phyage  */
			int		ind_pt_current;             /**< \brief current position value */
			vector<int> nb_pos;                    /**< \brief number of position for each Phyage  */
			vector<vector<int> > x;                        /**< \brief position values */
			vector<vector<float> >y;                        /**< \brief parameter values */
			int		interpolation_PhyAge;       /**< \brief interpolation rule between two Phyage  */
			int		interpolation_position_current; /**< \brief current interpolation rule between two positions */
			vector<int> interpolation_position;    /**< \brief interpolation rule between two positions for each Phyage  */
			float		xmin; /**< \brief current minimum position value */
			float		xmax;/**< \brief current maximum position value */
			float		ymin; /**< \brief current minimum parameter value */
			float		ymax;/**< \brief current maximum parameter value */
			int		currentPhyAge;              /**< \brief current Phyage  */
			int		currentPosition;            /**< \brief current position */
			float		currentValue;               /**< \brief current parameter value */

			/// \brief Default constructor
			AmapSimVariable();

			/// \brief Destructor
			~AmapSimVariable();

			void set (int pos, float val);
			void set (int pos, int subPos, float val);
			void set (int pos, float val, bool insert);
			void set (int pos, int subPos, float val, bool insert);

	};


	//enum V_OpenMod {V_WRITE, V_READ};

	/////////////////////////////////////////////////////////////////////////////////////////////
	///
	///			\class AmapSimParamFile opens a file and gives some method to read and write
	///
	//////////////////////////////////////////////////////////////////////////////////////////////
	class AMAPSIM_EXPORT AmapSimParamFile : public CFile
	{

		protected:

			fstream  * fic;

		public:

			/// \brief Constructor
			AmapSimParamFile(const std::string& Name);

			/// \brief Destructor
			virtual ~AmapSimParamFile(void);

			void open(ios_base::openmode _Mode);
			void open();

			std::string getFileName() {return m_Name;};
			void setFileName(std::string n) {m_Name=n;};

			int size();

			/*@{*/

			/** apply a conversion factor conv to each value */
			/** @brief read a reference axis 1D parameter from file fic, store it in var and apply a conversion factor to each value */
			void read1DParameter(AmapSimVariable &var /**< reference axis parameter to be read */, float conv=1.0 /**< conversion factor */);

			/** @brief read a reference axis 2D parameter from file fic, store it in var and apply a conversion factor to each value */
			void read2DParameter(AmapSimVariable &var /**< reference axis parameter to be read */, float conv=1.0 /**< conversion factor */);

			/*@}*/
			void write1DParameter(/**< reference to the file to be read */  AmapSimVariable & var,	float   conv=1.0/**< conversion factor */);

			void write2DParameter(/**< reference to the file to be read */  AmapSimVariable & var,	float   conv=1.0/**< conversion factor */);


			/// \brief read a single value
			/// \param var AmapSimvariable to be read
			/// \param t integer or float value flag
			void readSingleValue (AmapSimVariable &var, /* variable to be read */long  t) /* integer or float value flag */;


			double readDouble ();

			/*@{*/


			/*@}*/
	};

};//end namespace


#endif

