/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file SimplifTopoAMAP.h
///			\brief Definition of SimplifTopoAMAP.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __SIMPLIFTOPOAMAP__
#define __SIMPLIFTOPOAMAP__
#include "SimplifTopo.h"



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class SimplifTopoAMAP
///			\brief Manage the association of Branc instances with criterias based on physiological age, rythm and bud outbreak time
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class SimplifTopoAMAP :
	public SimplifTopo
{
public:

	/// \brief Constructor
	SimplifTopoAMAP(class Plant *plt);

	/// \brief Destructor
	virtual ~SimplifTopoAMAP(void);

	/// \brief Format the criteria with given parameter : phy age, outbreak time, rythm and order
	const std::string formatCriteria(int phyage, double time, double rythm, char order);
	/// \brief Format the criteria with given parameter : phy age, outbreak time, rythm, order and birth date threshold
	const std::string formatCriteria(int phyage, double time, double rythm, char order, double birthThreshold);

	/// \return the branc at index \e indexInCriteria in the list of Branc associated to the criteria \e critere
	virtual Branc * getSimplifiedBranc(const std::string & critere, int indexInCriteria);

};

#endif

