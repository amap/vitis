/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef __AXISREFERENCE_INTERFACE_H
#define __AXISREFERENCE_INTERFACE_H

class AxisReferenceInterface
{
public:
    AxisReferenceInterface() {}
    virtual ~AxisReferenceInterface() {}


private:
    AxisReferenceInterface( const AxisReferenceInterface& source );
    void operator = ( const AxisReferenceInterface& source );
};


#endif // __AXISREFERENCE_INTERFACE_H
