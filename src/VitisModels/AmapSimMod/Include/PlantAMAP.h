/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  PlantAMAP.h
///			\brief Definition of PlantAMAP class.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __PLANTAMAP_H__
#define __PLANTAMAP_H__
#include "externAmapSim.h"
#include "Plant.h"
#include "AxisReference.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class PlantAMAP
///			\brief AMAP describtion for a plant , specialize Plant with adding a pointer to an axis reference
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class AMAPSIM_EXPORT PlantAMAP:public Plant
{

public:

	/// \brief Default constructor
	PlantAMAP();

	/// \brief Constructor
	PlantAMAP(void *p);

	/// \brief Constructor
	PlantAMAP(const std::string & nameT, const std::string & nameP);

	/// \brief Destructor
	virtual ~PlantAMAP(void);

	/// \brief Seed the plant  at the given time
	virtual void seedPlant (float time);


	virtual void init();

	/// \brief  init the current instance of plant builder
	virtual PlantBuilder * instanciatePlantBuilder();

};
#if defined AMAPSIM_EXPORTS
/// \brief C function , called by the "Load" function of SimulationModel in the Vitis framework to get the simulation modele
/// \return A instance of AmapSimModele
extern "C" AMAPSIM_EXPORT  Plant * StartPlugin (void *p);

#endif


#endif

