/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file AmapSimMod.h
///			\brief Definition of AmapSimModele.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __AMAPSIMODELE_H__
#define __AMAPSIMODELE_H__
#include "PlantBuilder.h"
#include "Bud.h"




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class AmapSimModele
///			\brief AMAPSIM Edification plant model	(based on Axis reference)
///
///			Gives methods to get simulation object's instance specialized by this dynamic model
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class AMAPSIM_EXPORT AmapSimModele: public PlantBuilder
{
	public:

		/// \brief Default constructor
		AmapSimModele(class Plant * plt);

		/// \brief Destructor
		~AmapSimModele();


		/// \brief Create a new geomtrical branch AMAP (Computed with axis reference data)
		/// @param brc The topological bearer
		/// @param gbr The geometrical bearer
		/// @param gt The geometrical manager
		///	@return An instance of GeomBrancAMAP
		virtual GeomBranc * createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr);

		/// \brief Create a new branch AMAP ( Branc specialization)
		/// @param hbear The topological bearer
		///	@return An instance of BrancAMAP
		virtual Branc * createInstanceBranc ();
		virtual Branc * createInstanceBranc (char *type) {return createInstanceBranc();};

		/// \brief Create a new instance of an element of decomposition
		///
		///	Two level are specialized in this modele , level 1 (GROWTHUNIT) and level 5 (INTERNODE)
		/// @param level The level of the element to instanciate
		///	@return An instance of DecompAxeLevel, or GrowthUnitAMAP (if level==1), or InterNodeAMAP (if level==5)
		virtual DecompAxeLevel * createInstanceDecompAxe (USint level);

		/// \brief Create a new Bud (manage the growth and ramification)
		/// @param br The branc associated to the Bud
		/// @param guRam growth unit ramification type : ANTICIPE, RETARDE1, RETARDE2, TRAUMAANT, TRAUMARET, RETARDECYC, NONBRANCHETRAUMAANT, NONBRANCHEANT, NONBRANCHERETCYC, NONBRANCHETRAUMARET
		/// @param timeC Start time of the new bud
		/// @param phyAgeBeg PhyAge of the new bud
		/// @param ryt rythme of the new Bud
		///	@return An instance of Bud
		//virtual VProcess * createInstanceBud ( class BrancAMAP * br, char guRam, double timeC, double phyAgeBeg, double ryt);
		virtual VProcess * createInstanceBud ( int n, ...);





};



#endif
