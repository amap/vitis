/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file RamifEngineAMAP.h
///			\brief Definition of RamifEngineAMAP class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __RAMIFENGINE__
#define __RAMIFENGINE__
#include "ArchivableObject.h"
using namespace Vitis;


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class RamifEngineAMAP
///			\brief Definition of RamifEngineAMAP class
///
///			\li Update hierarchical organisation of the plant by adding new axe beared by current axe
///			\li Compute phyage transition for born bud (automaton)
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class RamifEngineAMAP: public ArchivableObject
{

	/// \brief ramification state for previous node and each lateral production
	unsigned char  *  oldstate;

	/// \brief pointer to the bud
	class Bud * theBud;

	class BrancAMAP * theBranc;

	long * stock;

	char * stockNature;

	/// \brief nb lateral production
	int nblpa;

	public:

	/// \brief Default constructor
	RamifEngineAMAP(class Bud * b);

	/// \brief Destructor
	~RamifEngineAMAP(void);

	/// \brief Main ramification's algorithm
	void ramification  ();

	/// \brief Serialize a RamifEngineAMAP
	/// \param ar A reference to the archive \see Archive
	virtual void serialize(Archive& ar );


	private:
	/// \brief sets the new buds of a verticille
	///	\param lpIndex lateral production index
	///	\param timeloc current time
	///	\param type first Gu branching type (delayed, immediate ...)
	///
	int verticille (int lpIndex, double timeloc, int type);

	/// \brief immediate branching management
	void markovImmediateRamification (double timeloc, int lpIndex);
	void immediateRamification (double timeloc, int lpIndex);
	void semimarkovImmediateRamification (double timeloc, int lpIndex);

	/// \brief computes delayed branching along the last ut
	void markovDelayedRamification (double timeloc, int lpIndex, unsigned char oldstate);
	void delayedRamification (double timeloc, int lpIndex, unsigned char oldstate);
	void semimarkovDelayedRamification (double timeloc, int lpIndex, unsigned char oldstate);

	/// \brief computes delayed branching along the last cycle
	void markovCycleDelayedRamification (double timeloc, int lpIndex, unsigned char oldstate);
	void cycleDelayedRamification (double timeloc, int lpIndex, unsigned char oldstate);
	void semimarkovCycleDelayedRamification (double timeloc, int lpIndex, unsigned char oldstate);

	/// \brief computes traumatic branching along the last ut
	void markovTraumaticRamification (double timeloc, int lpIndex, unsigned char oldstate);
	void traumaticRamification (double timeloc, int lpIndex, unsigned char oldstate);
	void semimarkovTraumaticRamification (double timeloc, int lpIndex, unsigned char oldstate);

	/// \brief Compute new bud 's physiological age into vertcille
	/// \param lpIndex lateral production index
	/// \param branchRamificationType ramification type
	/// \param rhythm returned rhythm
	double computePhyAgeBeg (int lpIndex, char branchRamificationType, double *rhythm);

	/// \brief Test reset of branching state accordind to position and previous state
	/// \param typeramif ramification type
	/// \param direction counting direction (basal or apical)
	/// \param lpIndex lateral production index
	/// \return the initial state flag
	/// initial state is set if :
	///	\li first node of each zone of first cycle of first Gu in axe counting topology
	/// \li first node of each zone of first cycle of each Gu in ut counting topology
	/// \li first node of each zone of each cycle in cycle counting topology
	unsigned char testInitialState (long typeramif, long direction, long lpIndex);

	/// \brief check for delayed branching (markov process)
	/// \param direction counting direction (basal or apical)
	/// \param oldstate last ramification state
	/// \param lpIndex lateral production index
	/// \return NONBRANCHE, RETARDE1, RETARDE2
	char testDelayedRamification(long direction, unsigned char oldstate, int lpIndex);

	/// \brief check for cycle delayed branching (markov process)
	/// \param direction counting direction (basal or apical)
	/// \param oldstate last ramification state
	/// \param lpIndex lateral production index
	/// \return NONBRANCHERETCYC, RETCYC
	char testCycleDelayedRamification(long direction, unsigned char oldstate, int lpIndex);

	/// \brief check for traumatic branching (markov process)
	/// \param direction counting direction (basal or apical)
	/// \param oldstate last ramification state
	/// \param lpIndex lateral production index
	/// \return NONBRANCHETRAUMATIC, TRAUMATIC
	char testTraumaticRamification(long direction, unsigned char oldstate, int lpIndex);

	void checkFloatingBranchingZone (int lpIndex, int phyAge);


};

#endif

