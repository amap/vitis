/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file GrowthEngineAMAP.h
///			\brief Definition of GrowthEngineAMAP class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __GROWTHENGINE__
#define __GROWTHENGINE__
#include "defAMAP.h"
#include "CycleAMAP.h"
#include "ArchivableObject.h"
using namespace Vitis;

class CycleAMAP;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GrowthEngineAMAP
///			\brief Definition of GrowthEngineAMAP class
///
///			\li Update topology of an axe by adding new growthunit, cycle , zone , test and internodes according axis reference data
///			\li Compute phyage differentiation(automaton)
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GrowthEngineAMAP : public ArchivableObject
{

	public:

#if 0
		/// \brief proba to do neoformation
		float pbneo;

		/// \brief preformation (binomial)
		float prefn;

		float prefp;

		/// \brief neoformation (Bernouilli composee)
		float neon,neod;

		float neop,neob;

		float neopn,neopp;
#endif

		/// \brief growth end flag
		char    growthEnd;

		/// \brief reference axis index (integer phy age)
		long    phyAge;

		/// \brief phy age of next Gu (floating point value)
		double	 nextPhyAgeRef;

		/// \brief phy age of current Gu (floating point value)
		double   phyAgeRef;

		/// \brief max phy age
		long    maxPhyAge;

		/// \brief time for proleptique initial dormancy
		double   prolepticSleepingTime;

		/// \brief initial state flag
		char    BranchBegining;

		/// \brief z2 flag
		char    okZone2;

		/// \brief Number Growth Unit left keeping the same phyage
		long noDifferentiationGu;

		/// \brief number of tests for proleptique dormancy
		int   prolepticDormancyTestNumber;

		/// \brief immediate branching depth
		char    immediateBranchingOrder;

		/// \brief polycyclism flag
		char    polycyc;

		/// \brief number of cycle to be set in the current GU
		long	nbcyc;

		/// \brief number of test in the current zone
		long	testZone2Number;

		/// \brief number of test in the current GU
		long currentGuTestNumber;

		/// \brief max number of Gu inside branch
		int   maxGuNumber;

		/// \brief current rhythm
		float   rythme;

		/// \brief successfull test flag fo current internode
		bool	entnRealized;


		/// \brief pointer to the bud
		class Bud * theBud;

		class BrancAMAP * theBranc;

		/// \brief Default Constructor
		GrowthEngineAMAP(Bud * b=NULL);

		/// \brief Constructor
		/// \param b pointer to the bud
		/// \param phyAgeBeg bud phyage
		/// \param ryt growth rythme
		GrowthEngineAMAP(Bud * b, double phyAgeBeg, double ryt);

		/// \brief Destructor
		~GrowthEngineAMAP(void);

		/// \brief Main growth computation's algorithm
		int growth ();

		/// \brief Serialize a GrowthEngineAMAP
		/// \param ar A reference to the archive \see Archive
		virtual void serialize(Archive& ar );

	private:

		/// \brief computes the initial state of a bud according to its branching parameters (position, ramaification type )
		///	\param p current branch
		void calcul_parametres_axillaire(class BrancAMAP * p);

		//TODO change name function
		/// \brief Test proleptic sleeping and if axis reference and branching order has been reached
		int initProlepticSleep ();

		/// \brief Compute potential new cycle and new internode
		///
		///	Test the outbreak of new internode and new cycle
		CycleAMAP *setupNewCycle();

		/// \brief test death of current node according to its type
		short testBudDeath(CycleAMAP *c);

		/// \brief test end of current Gu and cycle
		short testEndGu(long& cycleEnd);

		/// \brief Set the fisrt growing eventData time in case of proleptic sleeping
		///
		/// the branch is still sleeping at its begining according to test law, then this function initialize the first growing eventData
		/// \return 0 if the bud is sleeping and a new later growth eventData has been set, 1 otherwise
		int testProlepticSleep ();

		/// \brief computes test time length and Gu end time
		/// \param phyAge  index phy age in axis reference
		/// \param instant current time
		void getTime ( long phyAge, double instant);

		/// \brief computes time length for a ut and a test according rhythm
		/// \param phyAge  index phy age in axis reference
		double getTimeGu (long phyAge);

		/// \brief In case of the first Gu of an immediate branch, one has to put a limit on the time to grow the first Gu
		void limitGuTime();

		/// \brief Tests polycyclism capability returns 0 if no polycyclism 1 if polycyclism
		/// \param oldstate state of previous Gu
		/// \param phyAge current phy age
		char calcPolycyc (unsigned char oldstate, int phyAge ) ;

		/// \brief Create and add a new gu with given phyage age_loc
		void createGu (double age_loc);

		/// \brief Phyage differentiation according axis reference automaton
		float differentiation ();


		/// \brief current Gu branching capability
		char testBranchingCapability();

		/// \brief current Gu dormancy
		char dormance();

		/// \brief Start a growth unit
		int startGu();

		/// \brief Stop a growth unit , put eventData at next GU start time
		int endGu();

};

#endif

