/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file GrowthUnitAMAP.h
///			\brief Definition of GrowthUnitAMAP class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _CYCLEAMAP_H
#define _CYCLEAMAP_H
#include "DecompAxeLevel.h"
#include "AxisReference.h"
#include "externAmapSim.h"

class AxisReference;
class BrancAMAP;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GrowthUnitAMAP
///			\brief Describe a growth unit ( GU ) : first sublevel decomposition of an AMAP axe
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class AMAPSIM_EXPORT CycleAMAP :
	public DecompAxeLevel
{
	public:

		/// \brief first Zone entn number
		long PreformEntnNumber;
		long PreformTestNumber;
		long NeoformTestNumberD;
		float NeoformTestNumberN;
		float NeoformTestNumberP;
		float NeoformP;
		USint okZ2;
		bool end;
		bool active;

		void init (USint index, AxisReference *axisref, long phyAgeLoc, BrancAMAP *theBranc);

		/// \brief Serialize a CycleAMAP
		/// \param ar A reference to the archive \see Archive
		virtual void serialize(Archive& ar );

		virtual std::string getName(){return std::string ("Cycle");};

		/// \brief Constructor
		CycleAMAP(class Plant * plt);

		/// \brief Destructor
		virtual ~CycleAMAP();

};

#endif

