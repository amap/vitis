/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file AxisReference.h
///			\brief Definition of class Variable and AxisReference
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _AXISREFERENCE_H
#define _AXISREFERENCE_H

#include <stdio.h>
#include <string>
#include <vector>
using std::vector;
#include "AmapSimModSignalInterface.h"
#include "AmapConfig.h"
#include "corresp.h"
#include "AmapSimParamFile.h"
using namespace Vitis;




//Unused
class BrancAMAP;

/// \brief Memory representation of an axis reference
typedef std::vector<AmapSimVariable> AxeVar;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class AxisReference
///			\brief Usefull to manipulate the reference axis parameter
///
///			For a detailed description of reference axis theory and plant parametrisation, go to http://amap.cirad.fr/amapsim/referman/accueil.html"
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class AMAPSIM_EXPORT AxisReference : public AmapSimParamFile
{

	private:
		/// \brief The current axis reference
		AxeVar  axe;

		/// \brief vector of axis reference (perhaps usefull)
		std::vector<AxeVar *>listaxe;
		std::string name;

		/// \brief Max branching order for nature 0 axes
		int maxorder;

		/// \brief Nb max of Lateral Projection
		int nbLPAxe;

		char **PLTypeAuthorized;
		char **lpName;

		int	**ramificationModelType;

		void defaultAxe(AmapSimVariable * axe, float val);

		/// \brief Copy the Variable src into dest
		void copyAxe(AmapSimVariable src, AmapSimVariable * dest);

		bool set;

		float val1DParameter (int var,
				int PhyAge);


		AmapsimSignalInterface signal;

	public:


		/// \brief Constructor
		/// \param paramFileN The axis reference paramaters filename
		AxisReference(std::string paramFileN);

		/// \brief Destructor
		virtual ~AxisReference();


		/// \brief Read the axis reference parameter file and store the data into memory
		int readAxis(void);

		/// \brief Write the axis reference parameter file from data into memory
		int writeAxis(void);

		inline int getNbLPAxe() { return nbLPAxe;}

		inline int getRamifModelType(int i , int j) { return ramificationModelType[i][j];}

		inline char getPLTypeAuthorized(int i , int j) { return PLTypeAuthorized[i][j];}
		inline char *getLpName(int i) { return lpName[i];}

		inline std::string getName() {return name;};
		inline void setName(std::string n) {name=n;};

		/*@{*/
		/** \return the value of the parameter at that position */
		float valSingleParameter (int numvar/* parameter index */, BrancAMAP *b/* bud that is currently treated */);

		/** @brief return value of 1D parameter with index var at phyage for current BrancVitis b */
		float val1DParameter (int var, /**< parameter index (see corresp.h) */
				int PhyAge, /**< Physiological Age (begins at 0) */
				BrancAMAP *b, /**< current bud */
				int sig=1);

		/** @brief return value of 2D parameter with index var at phyage and pos for current BrancVitis b */
		float val2DParameter (int numvar, /**< parameter index (see corresp.h) */
				int PhyAge, /**< Physiological Age (begins at 0) */
				int pos, /**< position whithin current counting unit */
				BrancAMAP *b /**< current bud */);
		/*@}*/


		/// \return the next control point index
		int nextCtrlPoint2DParameter (int numvar, int pos);

		/// \brief Check if topology is right
		void verif_topo (long  maxref, long  nbent);

		/// \brief Compute the max number of test to be tested inside a growth unit
		void calcule_maxtest (long maxref);

		/// \brief Check if rythm is right
		void verif_rythme (long maxref, int  pl, int  nument);

		/// \brief Check if number of internode is right
		int verif_nbentn (AmapSimVariable var);

		void defaultNape (AmapSimVariable *axe, float val);

		int uniqueValueNape(AmapSimVariable var, float val);

		int uniqueValueAxe(AmapSimVariable var, float val);

		float adjustProba (float val, int step);
		void adjustAxe (int index, int numzone, int numpl, int min, int max, float step);
		void adjustAxeProba (int index, int numzone, int numpl, int min, int max, float step);
		void adjustNape (int index, int numzone, int numpl, int min, int max, float step);

		/** @brief set value of 2D parameter with index numvar at phyage phy */
		void set1DParameter (int numvar, /* parameter index */
		int phy,
		float val);
		/** @brief set value of 2D parameter with index numvar at phyage phy */
		void set1DParameter (int numvar, /* parameter index */
		int phy,
		float val,
		bool insert);
		/** @brief set value of 3D parameter with index numvar at phyage pos and position subPos*/
		void set2DParameter (int numvar, /* parameter index */
		int pos,
		int subPos,
		float val);
		/** @brief set value of 3D parameter with index numvar at phyage pos and position subPos*/
		void set2DParameter (int numvar, /* parameter index */
		int pos,
		int subPos,
		float val,
		bool insert);

		/// \return The Variable into axis reference at indice k
		inline AmapSimVariable&   operator [] (int k)
		{
			return axe[k];
		}

		bool isSet () {return set;}

};

#endif
