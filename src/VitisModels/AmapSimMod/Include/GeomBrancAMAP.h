/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file GeomBrancAMAP.h
///			\brief Definition of GeomBrancAMAP class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _GEOMBRANCAMAP_H
#define _GEOMBRANCAMAP_H
#include "GeomBrancCone.h"
#include "BrancAMAP.h"
#include "AxisReference.h"



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GeomBrancAMAP
///			\brief Geometry of an axe based on "trunk-cone" and using axis reference data
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class AMAPSIM_EXPORT GeomBrancAMAP:public GeomBrancCone
{
	/// \brief Axis for the sympodial planar deviation
	Vector3	axesympode ;

	/// \brief	Scale
	float 	scale;

	/// \brief Insertion type:
	///
	///   \li INSER1 : constant insertion angle, current phyllotaxy
	///   \li INSER2 : constant insertion angle, plagiotropy
	///   \li INSER3 : straight insertion
	///   \li INSER4 : constant insertion angle according to vertical, current phyllotaxy
	///   \li INSER5 : pseudo-plagiotropy
	///   \li INSER6 : secondary direction to the top
	///   \li INSER7 : random insertion angle and phylotaxy (between 0 and PI)
	///   \li INSER8 : insertion angle computed according to gravity
	short typeInsert;

	/// \brief	Total bending for the branch
	float 	totalBending ;

	/// \brief Position of inverse bending
	float 	inflex;

	/// \brief The conicity of the axe
	float 	conicity;

	/// \brief Deviation angle
	float 	deviationAngle;

	/// strange...
	float 	anginit;

	/// \brief Initial diameter
	float 	initialDiameter;

	/// \brief Current phyllotaxy for each lateral production
	float *	phy ;

	/// \brief Pointer to axis reference
	AxisReference * axisref;

	/// \brief Theoritical Gu dimension without trucature
	int branchGuNumberWhithoutLimit;

	/// \brief Total number of test along the branch according axis reference
	int branchTotalTestNumbertheo;

	/// \brief Total number of existing nodes
	int branchTotalEntnNumber;




	public:

	/// \brief Physiological age of the axe
	double phyAgeCur;

	/// \brief Constructor
	/// \param brc Pointer to the hierarchical axe
	///	\param gbear Pointer to the geometrical bearer axe
	///	\param gbear Pointer to the GeomManager
	GeomBrancAMAP(Hierarc * brc=NULL,GeomBranc * gbear=NULL,  class Plant * gt=NULL);

	/// \brief Destructor
	virtual ~GeomBrancAMAP(void);

	/// \brief Compute initial values of geometric parameters for a branch : Specialization
	virtual void computeParamGeomInit ();

	/// \brief Function to initialize geometry : Specialization
	/// \return \li 0 This branc can be "super-simplified" \li 1 otherwise
	virtual int initGeomtry();

	/// \brief Compute bending angle for current node according to global flexion of branch
	virtual void flexionAtNode ();


	/// \brief Compute initial parameter value for length and diameter
	virtual void computeInitialLengthDiameter ();

	/// \brief Specialization \see GeomBrancCone
	virtual int computeBranc(bool silent);

	/// \brief Find the geometrical primitive identifier for each element : Specialization
	virtual int findSymbole ();

	/// \return the length of the current internode : Specialization
	virtual float computeLength ();

	/// \return the phyllotaxy angle of insertion on the bearer axe : Specialization
	virtual float computeInsertAnglePhy();

	/// \return the angle of insertion on the bearer axe : Specialization
	virtual float computeInsertAngle();

	/// \brief Update the current phyllotaxy along the axe( called after each internode computation) : Specialization
	virtual void updatePhyllotaxy ();

	/// \return the up-diameter of the current internode
	virtual float computeTopDiam ();

	/// \brief Function to compute the current element transformation according the bending of the axe : Specialization
	void computeBending (GeomElem *ge);


	/// \brief Insert a new internode on his bearer with the given data
	/// \param currentTriedron
	/// \param referenceTriedron bearer triedron (initial one)
	/// \param typins insertion type (see above)
	/// \param angins insertopn angle
	/// \param angphy phyllotaxy angle
	/// \param aleas randomization factor
	/// \param rnd random value
	virtual Matrix4 axeInsertionOn (Matrix4 & currentTriedron,
			Matrix4 & referenceTriedron, /* bearer triedron (initial one) */
			long typins,    /* insertion type (see above) */
			float angins,   /* insertopn angle */
			float angphy,   /* phyllotaxy angle */
			float aleas,    /* randomization factor */
			double rnd);


	/// \brief rotate mat_ref according to mat_plan to get main direction of mat_ref in first plan of mat_plan
	/// Rotated matrix is stored in plagMat
	void determine_plagio (Matrix4 & plagMat, const Vector3 mat_plan, const Vector3 mat_ref);


	virtual Matrix4 computeDeviationTranslation(const Matrix4 & currentTriedron, Matrix4 & referenceTriedron);

	/// \name Write and read Accessor

	/// \return the current phyllotaxy
	inline float * getPhyllotaxy() {return phy;}

	/// \return the current phyllotaxy for a specified lateral production
	inline float  getPhyllotaxyAt(int i) {return phy[i];}

	/// \return the initial insertion angle
	inline float getAngInit() {return anginit;}

	/// \brief set the initial insertion angle
	inline void setAngInit(float a) {anginit=a;}

	/// \brief set the total bending for the branch
	inline void setTotalBending(float bend) {totalBending=bend;}

	//! \brief Function to simplify the geometry of the axe
	/*! Concatenate successive cone if they are approximatively colinear and got the same symbol number
	*/
	void simplification ();


};

#endif

