/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file InterNodeAMAP.h
///			\brief Definition of InterNodeAMAP class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _INTERNODEAMAP_H
#define _INTERNODEAMAP_H
#include "DecompAxeLevel.h"
#include "externAmapSim.h"



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class InterNodeAMAP
///			\brief Describe an internode ( last sublevel decomposition of an AMAP axe )
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class AMAPSIM_EXPORT InterNodeAMAP :
	public DecompAxeLevel
{
	public:


		/// \brief birth time
		double  instant;

		/// \brief 0 or 1 if there is a sympod or not
		char  sympod;

		/// \brief 0 or 1 if there is a sympod in a plan or not
		char  sympodPlan;

		/// \brief branching flag for each lateral production
		unsigned char *  branchent;

		/// \brief Constructor
		InterNodeAMAP(class Plant * plt);

		/// \brief Destructor
		virtual ~InterNodeAMAP();

		virtual std::string getName(){return std::string ("InterNode");};

		/// \brief Serialize a InterNodeAMAP
		/// \param ar A reference to the archive \see Archive
		virtual void serialize(Archive& ar );

};

#endif

