/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

/** \file corresp.h
 * \brief reference axis parameter indices
 *
 * This definition will be usefull for thoose who want to trap reference axis parameter values trough the \e effectParameterModif function./n
 * For a detailed description of each parameter and its use, go to http://amap.cirad.fr/amapsim/referman/accueil.html" \n
 * Each parameter can be accessed either through its decimal index or through its symbolic name. \n
 * For parameters belonging to the Lateral Production section there must be some shift according to the Lateral Production index and to the zone index.
 * \n
 * For parameters belonging to the LP global group. For instance to access the axilary probability for lateral production lpIndex, you should test that value :
 * \f$NB\_VAR+NB\_VAR\_LP*lpInd+repbaxilf$
 * \n
  * That is to say :
 * 	- skip the \f$NB\_VAR\f$ non lateral production parameters,
 * 	- skip to the current \f$lpInd\f$ lateral production
 * \n
 * For parameters belonging to the LP specific zone group. For instance to access the initial imediate physiological age for lateral production lpIndex in zone zoneInd, you should test that value :
 * \f$NB\_VAR+NB\_VAR\_LP*lpInd+NB\_GLOBAL\_LP\_VAR+zoneInd*NB\_ZONE\_VAR+rcsautramiantf$
 * \n
 * That is to say :
 * 	- skip the \f$NB\_VAR\f$ non lateral production parameters,
 * 	- skip to the current \f$lpInd\f$ lateral production,
 * 	- skip to the global parameters for lateral production,
 * 	- skip the first zone (if \f$zoneInd\f$=0 then the current node is in zone 1, if \f$zoneInd\f$=1 then the current node is in zone 2),
 * 	- skip to \f$rcsautramiant\f$ parameter.
 *
 * A simple code to be inserted into effectParameterModif function could look like that :
 * \code
 * Bud *b;

 * b= getBud (hierarcIndex);
 * if (parameterIndex == rtnben)
 * {
 *    // modify symbol number value for dead leaves
 *    if (   b->phyAge == PHY_AGE_FOR_LEAVES
 *        && b->death == 1)
 *       *val = SYMBOL_NUMBER_FOR_DEAD_LEAF;
 * }
 * else if (parameterIndex == NB_VAR + 2*NB_VAR_LP+NB_GLOBAL_LP_VAR+NB_ZONE_VAR + repbinitret0)
 * {
 *    // modify initial branching probability for delayed axes borne in second zone for LP 3
 *    if (climate == VERY_COLD)
 *       *val = 0;
 *    else if (climate == WARM_SUNNY_HUMID)
 *       *val = 1;
 * }
 * \endcode
 * */
/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2002 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef CORRESP
#define CORRESP

#define NB_VAR 		127 /**< \brief number of parameters outside lateral production definition */
#define NB_GLOBAL_LP_VAR 	10 /**< \brief number of global parameters into lateral production definition */
#define NB_ZONE_VAR 	55 /**< \brief number of zone parameters into lateral production definition */
#define NB_VAR_LP 120 /**< \brief total number of parameters for lateral production definition */

#define glnbcyc			1 /**< \brief node kind count (single)*/
#define glmaxref		2 /**< \brief reference axis size (single)*/
#define glmaxwr			3 /**< \brief maximum step for output(single) */
#define glmaxcalc		4 /**< \brief maximum age (single)*/
#define glPhyAgeune		5 /**< \brief initial step (single)*/
#define glangins		6 /**< \brief initial insertion angle (0 to vertical) (single)*/
#define gllnginit		7 /**< \brief initial length of trunc nodes (single)*/
#define gllnghoup		8 /**< \brief deprecated */
#define glsurfhoup		9 /**< \brief initial diameter of trunc nodes (single)*/
#define glvitesse		10 /**< \brief initila rythm (single)*/
#define rtmaxbrazero	11 /**< \brief max branching order for nature 0 axes */
#define rtmaxreiteration		12 /**< \brief max reiteration order */
#define rtmaxanticipe	13 /**< \brief max immmediate branching order */
#define rtnumsymb		14 /**< \brief symbol number */
#define rtagelim		15 /**< \brief max growth unit count */
#define rtprogress		19 /**< \brief probability for progression along reference axis */
#define rtvitessepr		20 /**< \brief time sleeping lag */
#define rtprolgup		21 /**< \brief time sleeping probability */
#define rtprolgud		22 /**< \brief time sleeping lag */
#define rtproln			23 /**< \brief test sleeping number */
#define rtprolp			24 /**< \brief test sleeping probability */
#define rtprold			25 /**< \brief test sleeping lag */
#define rtbasecompt		26 /**< \brief counting base */
#define rttypetop		27 /**< \brief counting unit */
#define rtpbsympodeGu	29 /**< \brief sympod probability between Gu */
#define rtpbsympodeUC	30 /**< \brief sympod probability between cycles */
#define rtnben			32 /**< \brief max test number */
#define rtpolyini		37 /* polycyclisme */ /**< \brief initial growth units polycyclism probability */
#define rtmonocyc		38 /**< \brief remaining monocyclic probability */
#define rtpolycyc		39 /**< \brief remaining polycyclism probability */
#define rt2cyc			40 /**< \brief probability to have 2 cycles */
#define rt3cyc			41 /**< \brief probability to have 3 cycles */
#define rt4cyc			42 /**< \brief probability to have 4 cycles */
#define rtsleepcyc1		43 /**< \brief probability to have first cycle */
#define rtneocyc1		44 /**< \brief probability to have two zones */
#define rtprefn1		45 /**< \brief test number first zone */
#define rtprefp1		46 /**< \brief elongagtion probability first zone */
#define rtneod1			47 /**< \brief second zone test number lag */
#define rtneon1			48 /**< \brief second max test number */
#define rtneop1			49 /**< \brief second zone test probability */
#define rtneob1			50 /**< \brief second zone elongation probability */
#define rtneopn1		51 /**< \brief first zone test number */
#define rtneopp1		52 /**< \brief elongagtion probability first zone */
#define rtsleepcyc2		53 /**< \brief probability to have second cycle */
#define rtneocyc2		54 /**< \brief probability to have two zones */
#define rtprefn2		55 /**< \brief test number first zone */
#define rtprefp2		56 /**< \brief elongagtion probability first zone */
#define rtneod2			57 /**< \brief second zone test number lag */
#define rtneon2			58 /**< \brief second max test number */
#define rtneop2			59 /**< \brief second zone test probability */
#define rtneob2			60 /**< \brief second zone elongation probability */
#define rtneopn2		61 /**< \brief first zone test number */
#define rtneopp2		62 /**< \brief elongagtion probability first zone */
#define rtsleepcyca		63 /**< \brief probability to have other cycle */
#define rtneocyca		64 /**< \brief probability to have two zones */
#define rtprefna		65 /**< \brief test number first zone */
#define rtprefpa		66 /**< \brief elongagtion probability first zone */
#define rtneoda			67 /**< \brief second zone test number lag */
#define rtneona			68 /**< \brief second max test number */
#define rtneopa			69 /**< \brief second zone test probability */
#define rtneoba			70 /**< \brief second zone elongation probability */
#define rtneopna		71 /**< \brief first zone test number */
#define rtneoppa		72 /**< \brief elongagtion probability first zone */
#define rgtypins		73 /**< \brief insertion type */
#define rgsympode		74 /**< \brief initial sympod angle */
#define rgyoung			75 /**< \brief Young module */
#define rginflexion		76 /**< \brief force location ratio */
#define rgconicite		77 /**< \brief conicity */
#define rgredres		78 /**< \brief straightening angle */
#define rgplagredre		79 /**< \brief straightening area */
#define rgflexion		80 /**< \brief organ bending angle */
#define rgaleatoire		81 /**< \brief random ration */
#define rgsensgeom		82 /**< \brief geometry counting direction */
#define reevollng		83 /**< \brief length variation ratio */
#define renbtestlng		84 /**< \brief length variation area */
#define relaglng		85 /**< \brief length variation lag */
#define reevolangins	86 /**< \brief insertion angle variation ratio */
#define renbtestangins	87 /**< \brief insertion angle variation area */
#define relagangins		88 /**< \brief insertion angle variation lag */
#define reevolgross		89 /**< \brief diameter variation ratio */
#define renbtestgross	90 /**< \brief diameter variation area */
#define relaggross		91 /**< \brief diameter variation lag */
#define reevolsymp		92 /**< \brief final sympod angle */
#define renbtestsymp	93 /**< \brief sympod angle variation area */
#define rgpcntlng		94 /**< \brief initial length ratio along the current unit (2D) */
#define rgpcntgross		95 /**< \brief initial diameter ratio along the current unit (2D) */
#define rtpruningcoef	96 /**< \brief progressive pruning coef (for old fashioned growth engine compatibility) */
/* JFB 21/03/05 new semi Markov differentiation */
#define nbremain				97 /**< \brief test number for no differenciation binomial law (if negative then an inverse binomial law will be computed) */
#define pbremain				98 /**< \brief test probability for no differenciation binomial law */
#define nbtransition			99 /**< \brief number of possible differentiation states */
#define transitionfrequency1	100 /**< \brief differentiation state 1 frequency */
#define transitionjump1			101 /**< \brief differentiation state 1 phyAge */
#define transitionfrequency2	102 /**< \brief differentiation state 2 frequency */
#define transitionjump2			103 /**< \brief differentiation state 2 phyAge */
#define transitionfrequency3	104 /**< \brief differentiation state 3 frequency */
#define transitionjump3			105 /**< \brief differentiation state 3 phyAge */
#define transitionfrequency4	106 /**< \brief differentiation state 4 frequency */
#define transitionjump4			107 /**< \brief differentiation state 4 phyAge */
#define transitionfrequency5	108 /**< \brief differentiation state 5 frequency */
#define transitionjump5			109 /**< \brief differentiation state 5 phyAge */
#define shiftremain				110 /**< \brief minimum number for no differenciation binomial law (if negative then an inverse binomial law will be computed) */
#define rtprefd1				111 /**< \brief first zone test number lag */
#define rtneopd1				112 /**< \brief first zone test number lag */
#define rtprefd2				113 /**< \brief first zone test number lag */
#define rtneopd2				114 /**< \brief first zone test number lag */
#define lnginit					115 /**< \brief initial length of organs */
#define diaminit				116 /**< \brief initial diameter of organs */
#define anginsinit				117 /**< \brief initial insertion angle */
#define deviation				118 /**< \brief bearer deviation angle */
#define rtmaxetap				119 /**< \brief brach max phy age */
#define rtlagend				120 /**< \brief pruning age after end */
#define rtlagdeath				121 /**< \brief pruning age after death */
#define rtpbvie					122 /**< \brief living probability (2D)*/
#define rtrythme				123 /**< \brief local growth rhythm */
#define rtvitesse				124 /**< \brief phy age increment */
#define rtprefda				125 /**< \brief shift on pure preform shoot distribution law */
#define rtneopda				126 /**< \brief shift on preform part of Z1/Z2 shoot distribution law */


/** @name Global LP variables (there are NB_GLOBAL_LP_VAR of that parameter type)
#define rename           0 /**< \brief name of that LP */
#define retype           1 /**< \brief nature */
#define renbre           2 /**< \brief axilary number */
#define repbaxil         3 /**< \brief axilary probability */
#define redebprod        4  /**< \brief begining of production area */
#define refinprod        5  /**< \brief end of production area */
#define rgphase          6  /**< \brief axilary angle */
#define rgphyllotaxie    7  /**< \brief phylotaxy angle */
#define resensbrancret   8  /**< \brief delayed branching count direction */
#define resensinitret    9  /**< \brief delayed branch strength count direction */
#define reDeathInfluence 10  /**< \brief death influence */
/*@}*/
/** @name delayed branch type 1
 * */
/*@{*/
#define rcsautramiret1   1  /**< \brief minimum jump (2D) */
#define rcnbvarisautret1 2  /**< \brief max variation for jump (2D) */
#define rcpbvarisautret1 3  /**< \brief probability for variation for jump (2D) */
#define rcrythminitret1  4  /**< \brief initial rythm (2D) */
/*@}*/
/** @name delayed branch type 2
 * */
/*@{**/
#define rcsautramiret2   5  /**< \brief minimum jump for delayed branch type 2 (2D) */
#define rcnbvarisautret2 6  /**< \brief max variation for jump (2D) */
#define rcpbvarisautret2 7  /**< \brief probability for variation for jump (2D) */
#define rcrythminitret2  8  /**< \brief initial rythm (2D) */
/*@}*/
/** @name 2 states Markov automaton for delayed branching
 * */
/*@{*/
#define rcpbinitret0     9  /**< \brief initial probality not branched (2D) */
#define rcpbinitret1     10  /**< \brief initial probability type 1 (2D) */
#define rcpb_st0         11  /**< \brief remaining not branched probability (2D) */
#define rcnb_st0         12  /**< \brief number of test to remain not branched (2D) */
#define rcd_st0			 13 //34 /**< \brief minimum node number not branched */
#define rcpb_0_1         14  /**< \brief not branch to type 1 probability (2D) */
#define rcpb_st1         15  /**< \brief remaining type 1 probability (2D) */
#define rcnb_st1         16  /**< \brief number of test to remain branched state 1 (2D) */
#define rcd_st1          17  /**< \brief minimum node number branched state 1 (2D) */
#define rcpb_1_0         18  /**< \brief type 1 to not branched probability (2D) */
#define rcpb_st2         19  /**< \brief remaining type 2 probability (2D) */
#define rcnb_st2         20  /**< \brief number of test to remain branched state 2 (2D) */
#define rcd_st2          21  /**< \brief minimum node number branched state 2 (2D) */
#define rcpb_2_0         22  /**< \brief type 2 to not branched probability (2D) */
/*@}*/
/** @name imediate branching
 * */
/*@{*/
#define rcsautramiant    23  /**< \brief minimum jump (2D) */
#define rcnbvarisautant  24  /**< \brief jump variation area (2D) */
#define rcpbvarisautant  25  /**< \brief jump variation probability (2D) */
#define rcrythminitant   26  /**< \brief initial rythm (2D) */
#define rcpbinitant      27  /**< \brief initial branching probability (2D) */
#define rcpb_bant        28  /**< \brief remaining probability no branched (2D) */
#define rcnb_bant        29  /**< \brief number of test remaining no branched (2D) */
#define rcd_bant         30  /**< \brief minimum node number no branched (2D) */
#define rcpb_nbant       31  /**< \brief remaining probability branched (2D) */
#define rcnb_nbant       32  /**< \brief number of test remaining no branched (2D) */
#define rcd_nbant        33  /**< \brief minimum node number no branched (2D) */
/*@}*/
/** @name cycle delayed branching
 * */
/*@{*/
#define rcsautramiretc   34  /**< \brief minimum jump (2D) */
#define rcnbvarisautretc 35 /**< \brief jump variation area (2D) */
#define rcpbvarisautretc 36  /**< \brief jump variation probability (2D) */
#define rcrythminitretc  37 /**< \brief initial rythm (2D) */
#define rcpbinitretcyc   38  /**< \brief initial branching probability (2D) */
#define rcpb_bretcyc     39  /**< \brief remaining probability no branched (2D) */
#define rcnb_bretcyc     40  /**< \brief number of test remaining no branched (2D) */
#define rcd_bretcyc      41  /**< \brief minimum node number no branched (2D) */
#define rcpb_nbretcyc    42  /**< \brief remaining probability branched (2D) */
#define rcnb_nbretcyc    43  /**< \brief number of test remaining no branched (2D) */
#define rcd_nbretcyc     44  /**< \brief minimum node number no branched (2D) */
/*@}*/
/** @name traumatic branching
 * */
/*@{*/
#define rcpbinittrau     45  /**< \brief initial branching probability (2D) */
#define rcpb_btrau       46  /**< \brief remaining probability no branched (2D) */
#define rcnb_btrau       47  /**< \brief number of test remaining no branched (2D) */
#define rcd_btrau        48  /**< \brief minimum node number no branched (2D) */
#define rcpb_nbtrau      49  /**< \brief remaining probability branched (2D) */
#define rcnb_nbtrau      50  /**< \brief number of test remaining no branched (2D) */
#define rcd_nbtrau       51  /**< \brief minimum node number no branched (2D) */
#define rcsautramitrau   52  /**< \brief minimum jump (2D) */
#define rcnbvarisauttrau 53  /**< \brief jump variation area (2D) */
#define rcpbvarisauttrau 54  /**< \brief jump variation probability (2D) */
#define rcrythminittrau  55  /**< \brief initial rythm (2D) */
/*@}*/

#endif

