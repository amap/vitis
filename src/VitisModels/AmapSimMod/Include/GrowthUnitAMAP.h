/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file GrowthUnitAMAP.h
///			\brief Definition of GrowthUnitAMAP class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _GROWTHUNITAMAP_H
#define _GROWTHUNITAMAP_H
#include "DecompAxeLevel.h"
#include "externAmapSim.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GrowthUnitAMAP
///			\brief Describe a growth unit ( GU ) : first sublevel decomposition of an AMAP axe
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class AMAPSIM_EXPORT GrowthUnitAMAP :
	public DecompAxeLevel
{
	public:

		/// \brief Physiological age
		double phyAgeGu;

		/// \brief time length for a test
		double timeForATest;

		/// \brief sleeping flag
		char   actifGu;

		/// \brief sympod flag
		unsigned char   sympodeGu;

		/// \brief branching capability flag
		char   GuBranchingCapability;

		/// \brief end growth flag
		char   growthEnd;

		/// \brief Constructor
		GrowthUnitAMAP(class Plant * plt);

		/// \brief Destructor
		virtual ~GrowthUnitAMAP();

		/// \brief Set the physiological age and the time length for each test in this GU
		void set( double age_loc, double timeForATest);

		virtual std::string getName(){return std::string ("GU");};

		/// \brief Serialize a GrowthUnitAMAP
		/// \param ar A reference to the archive \see Archive
		virtual void serialize(Archive& ar );

};

#endif

