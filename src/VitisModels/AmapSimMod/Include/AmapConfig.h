/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file AmapConfig.h
///			\brief Interface for the AmapConfig class.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _AMAPCONFIG_H
#define _AMAPCONFIG_H
#include "VitisConfig.h"
#include "externAmapSim.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class VitisConfig
///			\brief Description of the configuration of AMAPSIM Modele
///			\warning Notice that this is a struct designed to have only static members.
///			\warning It should therefore be seen and used more as a module than a class.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class AMAPSIM_EXPORT AmapConfig : public VitisConfig
{

	public:

		AmapConfig();

		~AmapConfig();





};

#endif
