/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file AmapSimModSignalInterface.h
///			\brief Definition of the signal interface provided by AmapSim
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _AMAPSIMMODSIGNALINTERFACE_
#define _AMAPSIMMODSIGNALINTERFACE_
#include "externAmapSim.h"
#include "BrancAMAP.h"
#include "Notifier.h"
using namespace Vitis;

class BrancAMAP;

/// \enum messageAxeRef
/// \brief the different kind of message sent by AmapSim
enum messageAxeRef {Val1DParam, Val2DParam, ValSingleParam};

/// \struct paramAxisRef
/// \brief message sent upon parameter access
struct paramAxisRef:public paramMsg
{
	int numVar; //Parameter identifier
	float var;
	BrancAMAP *b;

	paramAxisRef () { numVar=0; var=0; b=0;}
	paramAxisRef (int nv, float v, BrancAMAP *bb) { numVar=nv; var=v; b=bb;}
	void set (int nv, float v, BrancAMAP *bb) { numVar=nv; var=v; b=bb;}
};



//Astuce car j'avais un probl�me de liaison (�vident apr�s coup) avec les dll ..un template ne peut pas s'exporter,
//donc on d�fini une instanciation de ce template que l'on exporte ...tous les message devront �tre de type typeMsg
template class AMAPSIM_EXPORT notifier<messageAxeRef>;





/// GEOMETRY
enum messageGeom {Length, Diam, Orientation };

//message param
struct paramGeom:public paramMsg
{
	int numVar;
	float var;

	paramGeom (int nv, float v) { numVar=nv; var=v;}
};



//Astuce car j'avais un probl�me de liaison (�vident apr�s coup) avec les dll ..un template ne peut pas s'exporter,
//donc on d�fini une instanciation de ce template que l'on exporte ...tous les message devront �tre de type typeMsg
template class AMAPSIM_EXPORT notifier<messageGeom> ;

/// \struct AmapsimSignalInterface
/// \brief AmapSim Parameter management Signal interface
/** This structure allows plug-ins to be notified on AmapSim parameter access \n
    A typical registering call to one of thoose Signal would be :
	 /** \code
struct ParameterPlugin:public subscriber<messageAxeRef> {
	ParameterPlugin (Plant *p) {subscribe(p);};
	virtual ~ParameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};
         * \endcode

**/
struct AmapsimSignalInterface
{
	AmapsimSignalInterface() {};
	~AmapsimSignalInterface() {};
        /// \brief a reference to a local AmapSim Parameter
	paramAxisRef param;

	/// \brief Signal sent on Single Parameter access
        /// \param numvar the index of the parameter (see Corresp.h)
        /// \param val the default parameter value
        /// \param b the branch which asks for this parameter value
        /// \return the potentially modified value of the parameter
	float sigSingleParameter (int numvar, float val, BrancAMAP *b)
	{
//		paramAxisRef * param= new paramAxisRef(numvar, val, b);
		param.set(numvar, val, b);

//        notifier<messageAxeRef> *t= &(notifier<messageAxeRef>::instance());

//		notifier<messageAxeRef>::instance().notify(ValSingleParam, param);
		Branc *bb = (Branc*)b;
		notifier<messageAxeRef>::instance().notify(ValSingleParam, &param, bb->getPlant());

//		val=param->var;
		val=param.var;

//		delete param;
		return val;
	};

	/// \brief Signal sent on 1D Parameter access
        /// \param numvar the index of the parameter (see Corresp.h)
        /// \param val the default parameter value
        /// \param b the branch which asks for this parameter value
        /// \return the potentially modified value of the parameter
	float sig1DParameter (int numvar, float val, BrancAMAP *b)
	{
//		paramAxisRef * param= new paramAxisRef(numvar, val, b);
		param.set(numvar, val, b);

//        notifier<messageAxeRef> *t= &(notifier<messageAxeRef>::instance());

//		notifier<messageAxeRef>::instance().notify(Val1DParam, param);
		Branc *bb = (Branc*)b;
		notifier<messageAxeRef>::instance().notify(Val1DParam, &param, bb->getPlant());

//		val=param->var;
		val=param.var;

//		delete param;
		return val;
	};
	/// \brief Signal sent on 2D Parameter access
        /// \param numvar the index of the parameter (see Corresp.h)
        /// \param val the default parameter value
        /// \param b the branch which asks for this parameter value
        /// \return the potentially modified value of the parameter
	float sig2DParameter (int numvar, float val, BrancAMAP *b)
	{
//		paramAxisRef * param= new paramAxisRef(numvar, val, b);
		param.set(numvar, val, b);

//		notifier<messageAxeRef>::instance().notify(Val2DParam, param);
		Branc *bb = (Branc*)b;
		notifier<messageAxeRef>::instance().notify(Val2DParam, &param, bb->getPlant());

//		val=param->var;
		val=param.var;

//		delete param;
		return val;
	};
};


#endif

