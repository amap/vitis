/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file Bud.h
///			\brief Definition of Bud class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _BUD_H
#define _BUD_H
#include "scheduler.h"
#include "AxisReference.h"
#include "BrancAMAP.h"
#include "GrowthEngineAMAP.h"
#include "RamifEngineAMAP.h"
#include "defAMAP.h"

class AxisReference;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class Bud
///			\brief Describe the mechanism of a bud
///
///			Manage two edification algorithm : growth and ramification for his linked "BrancAMAP" axe. \n
///			A bud inherit from VProcess and this process is scheduled along simulation time.\n
///			For each bud eventData launched, the process_event method is called and launch growth and ramification
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class AMAPSIM_EXPORT Bud: public VProcess
{

	friend class GrowthEngineAMAP;
	friend class RamifEngineAMAP;
	public:


	/// \brief branching order
	char    ordrez;

	/// \brief current Gu ramification type (immediate,delayed,trauma)
	char    guRamificationType;

	/// \brief branche ramification type (immediate,delayed,trauma)
	char    branchRamificationType;

	/// \brief death flag
	char    death;

	/// \brief branch end flag
	///	\li	bit 1 : end of branch
	///	\li bit 2 : end of Gu
	///	\li bit 3 : end of cycle
	char    endUnit;

	/// \brief birth moment
	double   birthTime;

	/// \brief death moment
	double   deathInstant;

	/// \brief time for end of current Gu
	double   endInstant;

	/// \brief time for end of previous Gu
	double   previousInstant;

	/// \brief time length for a test
	double   timeForATest;

	/// \brief Pointer to axis reference
	AxisReference *axisref;

	/// \brief Pointer to the growth engine
	GrowthEngineAMAP * groEngine;

	/// \brief Pointer to the ramification engine
	RamifEngineAMAP * ramEngine;

	/// \brief Pointer to his linked axe
	BrancAMAP * brancAMAP;


	/// \brief Next eventData time
	double nextTime;

	/// \brief Flag to know which pruning policy to apply
	char toBePruned;


	/// \brief Default constructor
	Bud();

	/// \brief Constructor
	/// @param br The branc associated to the Bud
	/// @param guRam growth unit ramification type : ANTICIPE, RETARDE1, RETARDE2, TRAUMAANT, TRAUMARET, RETARDECYC, NONBRANCHETRAUMAANT, NONBRANCHEANT, NONBRANCHERETCYC, NONBRANCHETRAUMARET
	/// @param timeC Start time of the new bud
	/// @param phyAgeBeg PhyAge of the new bud
	/// @param ryt rythme of the new Bud
	///	@return An instance of Bud
	Bud(BrancAMAP * br, char guRamificationType, double timeC ,double phyAgeBeg, double ryt);

	/// \brief Destructor
	~Bud(void);


	/// \brief The main loop of axe's edification model
	/// \param msg The eventData attached to this process
	///
	/// This loop manage the growth, ramification , death and pruning computation
	virtual void process_event(EventData * msg);

	/// \brief Launch the natural pruning of the associated axe and his children
	int pruning  ();

	/// \brief Function called when a hierarc axe linked to this bud is added
	virtual void updateHierar (Hierarc * hierarcPtr);

	/// \brief Function called when a hierarc axe linked to this bud is removed
	virtual void updatePruning();

	/// \name Accessor

	/// \return the ramification engine
	inline RamifEngineAMAP * getRamif(){return ramEngine;}

	/// \return the growth engine
	inline GrowthEngineAMAP * getGrowth(){return groEngine;}

	/// \return the associated branc
	inline class BrancAMAP * getBranc(){return brancAMAP;}

	/// \return the current phy age
	inline long getPhyAge(){return groEngine->phyAge;}

	/// \brief Serialize a Bud
	/// \param ar A reference to the archive \see Archive
	virtual void serialize(Archive& ar );



};



#endif

