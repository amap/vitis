/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

/** \file def.h
 *
 * \brief some constant value and symbolic name and macro definition
 * */

#ifndef _DEFAMAP_H
#define _DEFAMAP_H

#include "defVitis.h"

#define LGMAXNOM 100


#ifdef SUN
#define INDEFINI    -1
#else
#define INDEFINI    255
#endif
#define NONBRANCHE    0
#define ANTICIPE      1
#define RETARDE1      2
#define RETARDE2      3
#define TRAUMATIC     4
#define RETARDECYC    6
#define NONBRANCHEANT    8
#define NONBRANCHERETCYC    9
#define NONBRANCHETRAUMATIC    10

#define SYNCHRONE 0
#define ASYNCHRONE 1

#define RANDMARKOVA    1
#define RANDMARKOVR    2
#define RANDGU         3
#define RANDVERTICILLE 4
#define RANDVIE        5
#define RANDSYMPODEUC  6
#define RANDPROGRESS   7
#define RANDSAUTA      8
#define RANDSAUTR      9
#define RANDMARKOVP    10
#define RANDMARKOVT    11
#define RANDSAUTT      12
#define RANDMARKOVRC   13
#define RANDSAUTRC     14
#define RANDMARKOVGU   15
#define RANDSYMPODEGU  16
#define RANDDORMGU     17
#define RANDANGSYMPODEUC  18
#define RANDEVENEMENT  19

#define TEST 0
#define ENTN 1

#define BASAL 0
#define APICAL 1

#define TOPOGU 0
#define TOPOUC 1
#define TOPOAXE 2

#define AXE	0
#define GROWTHUNIT 1
#define GROWTHCYCLE 2
#define ZONE 3
#define TESTUNIT 4
#define INTERNODE 5

#define NOPRUNING 0
#define TOTALPRUNING 1
#define PROGRESSIVEPRUNING 2
#define WAITING 3

#define MARKOV 0
#define SEMIMARKOV 1

#define FINISHED 1
#define ENDGU 2
#define ENDCYCLE 4

#define binwrite(f,n) fwrite((VChar *)&(n),sizeof(n),1,f)
#define binread(f,n) fread((VChar *)&n, sizeof(n), 1, f)



#endif

