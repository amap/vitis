/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file BrancAMAP.h
///			\brief Definition of BrancAMAP class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _BRANCAMAP_H
#define _BRANCAMAP_H
#include "Branc.h"
#include "Bud.h"
#include "GrowthUnitAMAP.h"
#include "InterNodeAMAP.h"

/// \enum The different natures of an axe
enum NatureAxe {
	MainBranch,
	SecondaryBranch,
	SimpleImmediateOrgan,
	Root_e
};

class Bud;
class PlantAMAP;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class BrancAMAP
///			\brief Describe an AMAP axe of the plant (friend with Bud class)
///
///			Reprensentation of an axe according AMAPSIM formalism. The structure of this axe is updated by his Bud \n
///			An axe is composed in five decomposition level : GROWTHUNIT, GROWTHCYCLE, ZONE, TESTUNIT, INTERNODE
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class AMAPSIM_EXPORT BrancAMAP : public Branc
{

	friend class Bud;


	public:

	/// \brief branch kind
	NatureAxe	nature;

	/// \brief reiteration order
	char    reiterationOrder;

	/// \brief Lateral Production index
	USint   lpIndex;

	/// \brief phy age at birth of branch (floating point value)
	double   phyAgeInit;

	/// \brief Pointer to the Bud
	Bud * theBud;



	/// \brief Constructor
	/// \param idxPlt Current index of the mother plant into forest
	BrancAMAP(class Plant * plt);

	/// \brief Copy constructor
	BrancAMAP(BrancAMAP * b);

	/// \brief Destructor
	virtual ~BrancAMAP();

	/// \brief Set the branch kind and his lateral production index into axis reference
	void set(NatureAxe nature, int lpIndex, double phy);
	void set(NatureAxe nature, int lpIndex, const std::string &c, double phy);
	void set(NatureAxe nature, int lpIndex, const std::string &c, double phy, int reit);

	/// \brief Add the given bud to this axe
	void addBud (Bud * b);

	/// \brief Add the given inter-node to the end of the last zone
	void addIntn (InterNodeAMAP * intn);

	/// \brief Add a test to the end of the last zone
	void addTest();

	/// \brief Add a zone to the end of the last growth cycle
	void addZone();

	/// \brief Add a cycle to the end of the last growth unit
	void addCycle ();

	/// \brief Add the given growth unit at the end of the axe
	void addGu (GrowthUnitAMAP * gu);




	/// @name Member accessor
	//@{

	/// \return the BrancAMAP bearer
	BrancAMAP * getBearer ();

	/// \return the Bud
	Bud * getBud();

	/// \return the current growth unit
	GrowthUnitAMAP * getCurrentGu ();

	/// \return the current internode
	InterNodeAMAP * getCurrentEntn ();

	//@}


	/// @name Decomposition level index accessor
	/// \see DecompAxeLevel.h
	//@{

	/// \return the current Growth unit number into the axe
	int getGuNumber ();

	/// \return the current Growth unit index into the axe
	int getCurrentGuIndex ();

	/// \return the current Growth cycle index into the axe
	USint getCurrentCycleIndex ();

	/// \return the current zone index into the axe
	USint getCurrentZoneIndex ();

	/// \return the current internode index into the current GU
	int getCurrentEntnIndexWithinGu();

	/// \return the current internode index into the axe
	int getCurrentEntnIndexWithinBranch();

	/// \return the current internode index into the current cycle
	int getCurrentEntnIndexWithinCycle();

	/// \return the current cycle index into the current GU
	USint getCurrentCycleIndexWithinGu();

	/// \return the current internode number into the current GU
	int getCurrentEntnNumberWithinGu();

	/// \return the current internode number into the current Cycle
	int getCurrentEntnNumberWithinCycle();

	/// \return the current internode number into the axe
	int getCurrentEntnNumberWithinBranch();

	//@}


	/// \name test index accessor
	//@{

	/// \return the current test index into the current GU
	int getTestIndexWithinGu ();

	/// \return the current test index into the current Cycle
	int getTestIndexWithinCycle ();

	/// \return the current test index into the axe
	int getTestIndexWithinBranch ();

	/// \return the current test number into the current GU
	int getTestNumberWithinGu();

	/// \return the current test number into the current Cycle
	int getTestNumberWithinCycle();

	/// \return the current test number into the axe
	int getTestNumberWithinBranch();

	/// \return the current test index into the current Zone
	int getTestIndexWithinCurrentZone2 ();

	/// \return the current test number into the current Zone
	int getTestNumberWithinCurrentZone2 ();


	//@}


	/// \name Seek method into the axe
	//@{


	/// \brief Positioning on the first internode of the axe
	USint positOnFirstEntnWithinBranc();

	/// \brief Positioning on the last internode of the axe
	USint positOnLastEntnWithinBranc();

	/// \brief Positioning on the first cycle of the axe
	USint positOnFirstCycleWithinBranc();

	/// \brief Positioning on the last cycle of the axe
	USint positOnLastCycleWithinBranc();

	/// \brief Positioning on the current next internode of the axe
	USint positOnNextEntnWithinBranc();

	/// \brief Positioning on the current previous internode of the axe
	USint positOnPreviousEntnWithinBranc();

	/// \brief Positioning on the first GU of the axe
	USint positOnFirstGuWithinBranc();

	/// \brief Positioning on the last GU of the axe
	USint positOnLastGuWithinBranc();

	/// \brief Positioning on the current next GU of the axe
	USint positOnNextGuWithinBranc();

	/// \brief Positioning on the current previous GU of the axe
	USint positOnPreviousGuWithinBranc();

	/// \brief Positioning on the first internode into the current GU
	USint positOnFirstEntnWithinGu();

	/// \brief Positioning on the last internode into the current GU
	USint positOnLastEntnWithinGu();

	/// \brief Positioning on the current next internode into the current GU
	USint positOnNextEntnWithinGu();

	/// \brief Positioning on the current previous internode into the current GU
	USint positOnPreviousEntnWithinGu();

	/// \brief Positioning on the first internode into the current Cycle
	USint positOnFirstEntnWithinCycle();

	/// \brief Positioning on the last internode into the current Cycle
	USint positOnLastEntnWithinCycle();

	/// \brief Positioning on the current next internode into the current Cycle
	USint positOnNextEntnWithinCycle();

	/// \brief Positioning on the current previous internode into the current Cycle
	USint positOnPreviousEntnWithinCycle();

	/// \brief Positioning on the internode at given index into the axe
	USint positOnEntnWithinBrancAt(USint pos);

	/// \brief Positioning on the GU at given index into the axe
	USint positOnGuWithinBrancAt(USint pos);

	/// \brief Positioning on the internode at given index into the current GU
	USint positOnEntnWithinGuAt(USint pos);

	//@}


	/// \brief computes current position according to position rules
	/// \param senscompt BASAL, APICAL
	/// \param basecompt ENTN , TEST
	/// \param typetop AXE, Gu, CYCLE
	/// \return The current position
	long calcPosElem (long senscompt, long basecompt ,long typetop);

	/// \brief Function called when a hierarc axe linked to this branc is pruned
	virtual void updatePruning();

	/// \brief Compute the number of internode to prune into this axe and launch the total pruning of borne axe
	virtual void progressivePruning();

	/// \brief Fucntion called when a hierarc axe linked to this branc is added
	virtual void updateTopo (class Hierarc * hierarcPtr);

	void computeInitialLengthDiameter (float *initialLength, float *initialDiameter);
	float computeLength (float scale, int branchTotalTestNumbertheo);
	float computeDiam (float initialDiameter, int branchTotalTestNumbertheo);

	/// \brief Serialize a BrancAMAP
	/// \param ar A reference to the archive \see Archive
	virtual void serialize(Archive& ar );

	virtual std::string getName();

	private :

	/** @name Index and number for decomposition levels
	  Current indexes and number into decomposition levels of this axe */
	//@{


	int testIndexWithinGu;

	int testNumberWithinCycle;

	int currentEntnNumberWithinBranch;

	int currentEntnNumberWithinGu;

	USint currentZoneIndex;

	int testNumberWithinGu;

	USint currentCycleIndexWithinGu;

	int testNumberWithinBranch;

	//@}

};





#endif

