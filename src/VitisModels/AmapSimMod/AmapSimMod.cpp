/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <cstdarg>
#include "AmapSimMod.h"
#include "PlantAMAP.h"
#include "GeomBrancAMAP.h"
#include "CycleAMAP.h"
#include "defAMAP.h"


#include "DebugNew.h"


AmapSimModele::AmapSimModele(Plant * plt):PlantBuilder(plt)
{
    setLevelOfDecomposition("AMAPSimMod", 5);
}



AmapSimModele::~AmapSimModele()
{

}


GeomBranc * AmapSimModele::createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr)
{
	return new GeomBrancAMAP(brc,gbr,V_plt());
}

Branc * AmapSimModele::createInstanceBranc ()
{
	return new BrancAMAP(V_plt());
}

DecompAxeLevel * AmapSimModele::createInstanceDecompAxe (USint level)
{
	switch(level)
	{
		case GROWTHUNIT : return new GrowthUnitAMAP (V_plt()); break;
		case INTERNODE : return new InterNodeAMAP (V_plt()); break;
		case GROWTHCYCLE : return new CycleAMAP (V_plt()); break;
		default : return new DecompAxeLevel(level);break;
	}
}

VProcess * AmapSimModele::createInstanceBud ( int n, ...)
// BrancAMAP * br, char guRam, double timeC, double phyAgeBeg, double ryt)
{
    BrancAMAP *br;
    int guRam;
    double timeC;
    double phyAgeBeg;
    double ryt;
    int num;
    va_list arguments;                     // A place to store the list of arguments

    va_start ( arguments, n ); // Initializing arguments to store all values after n

    br = va_arg ( arguments, BrancAMAP *);
    guRam = va_arg ( arguments, int);
    timeC = va_arg (arguments, double);
    phyAgeBeg = va_arg (arguments, double);
    ryt = va_arg (arguments, double);

    va_end ( arguments );                  // Cleans up the list

	return new Bud( br, guRam, timeC, phyAgeBeg,ryt);
}



