#include "InterNodeAMAP.h"
#include "PlantAMAP.h"
/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "defAMAP.h"
#include "AmapConfig.h"
#include "DebugNew.h"


InterNodeAMAP::InterNodeAMAP(Plant * plt):DecompAxeLevel(plt, INTERNODE)
{
	instant=0;
	sympod=0;
	sympodPlan=0;

	int nblp=((AxisReference *)plt->getParamData(string("AMAPSimMod")))->getNbLPAxe();

	branchent = new unsigned char [nblp];

	for(int i=0; i<nblp; i++)
	{
		branchent[i]=0xff;
	}
}

InterNodeAMAP::~InterNodeAMAP()
{
	delete [] branchent;
}

void InterNodeAMAP::serialize(Archive& ar )
{


	DecompAxeLevel::serialize(ar);

	int nblp=((AxisReference *)getPlant()->getParamData(string("AMAPSimMod")))->getNbLPAxe();

	if(ar.isWriting() )
	{
		ar<<instant<<sympod<<sympodPlan;

		for(int i=0; i<nblp; i++)
		{
			ar<<branchent[i];
		}

	}
	else
	{
		ar>>instant>>sympod>>sympodPlan;

		for(int i=0; i<nblp; i++)
		{
			ar>>branchent[i];
		}

	}
}



