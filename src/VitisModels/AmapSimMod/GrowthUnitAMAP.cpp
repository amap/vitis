/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "GrowthUnitAMAP.h"
#include "defAMAP.h"
#include "DebugNew.h"

GrowthUnitAMAP::GrowthUnitAMAP(Plant * plt):DecompAxeLevel(plt, GROWTHUNIT)
{
	phyAgeGu=0;
	actifGu = 1;/* sleeping flag */
	sympodeGu = 0;/* Gu sympod flag */
	GuBranchingCapability = 0;/* branching flag */
	growthEnd = 0;/* growth end flag */
	this->timeForATest=0;

}


GrowthUnitAMAP::~GrowthUnitAMAP()
{
}

void GrowthUnitAMAP::set( double age_loc, double timeForATest)
{
	phyAgeGu=age_loc;

	this->timeForATest=timeForATest;
}


void GrowthUnitAMAP::serialize(Archive& ar )
{

	DecompAxeLevel::serialize(ar);


	if(ar.isWriting() )
	{
		ar<<phyAgeGu<<timeForATest<<actifGu<<sympodeGu<<GuBranchingCapability<<growthEnd;

	}
	else
	{
		ar>>phyAgeGu>>timeForATest>>actifGu>>sympodeGu>>GuBranchingCapability>>growthEnd;

	}
}



