/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "AmapSimMod.h"
#include "PlantAMAP.h"
#include "BrancAMAP.h"
#include "GeomBrancAMAP.h"
#include "SimplifTopoAMAP.h"
#include "Bud.h"
#include "DebugNew.h"

PlantAMAP::PlantAMAP()
{
	v_topoMan->setTopoSimplifier(new SimplifTopoAMAP(this));

}

PlantAMAP::PlantAMAP(void *p)
{
	v_topoMan->setTopoSimplifier(new SimplifTopoAMAP(this));

}

PlantAMAP::PlantAMAP(const std::string & nameT, const std::string & nameP):Plant(nameT, nameP)
{
	v_topoMan->setTopoSimplifier(new SimplifTopoAMAP(this));

	//if(v_config==NULL)
	//v_config= new AmapConfig();


}



PlantAMAP::~PlantAMAP(void)
{

}

void PlantAMAP::init()
{
	Plant::init();
    getPlantFactory()->setLevelOfDecomposition(string("AMAPSimMod"), 5);
	if(getParamData(string("AMAPSimMod"))==NULL)
		setParamData(string("AMAPSimMod"), new AxisReference(getParamName(string("AMAPSimMod"))));
}


void PlantAMAP::seedPlant (float time)
{
	Plant::seedPlant (time);

	AxisReference *axisref = (AxisReference*)getParamData(string("AMAPSimMod"));
	if (v_actionScheduler->getHotStop() > (*axisref)[glmaxcalc].ymin)
	{
		return;
	}

	BrancAMAP * bvit= (BrancAMAP *)v_pltBuilder->createInstanceBranc("AMAPSimMod");

	bvit->set(MainBranch, 0, (*axisref)[glPhyAgeune].ymin);

	v_topoMan->fork(bvit, NULL);

	bvit->addBud((Bud*)v_pltBuilder->createInstanceBud(5, bvit, (int)RETARDE1, 0 , (*axisref)[glPhyAgeune].ymin, 1));

	//bvit->getBud()->setTimeAndPriority(bvit->getBud()->timeForATest,10);

	v_actionScheduler->create_process(bvit->getBud(),NULL,bvit->getBud()->timeForATest+time, 10);

}


PlantBuilder * PlantAMAP::instanciatePlantBuilder()
{
	return new AmapSimModele (this);
}

#if !defined STATIC_LIB
//extern "C" Plant * StartPlugin (void *p)
//{
//	return new PlantAMAP(p);
//}
#endif

