#include "PlantAMAP.h"
#include "randomFct.h"
#include "BrancAMAP.h"
/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "Bud.h"
#include "defAMAP.h"
#include <iostream>
#include <math.h>
#include "DebugNew.h"

Bud::Bud()
{
	axisref=NULL;
	brancAMAP=NULL;
	groEngine=NULL;
	ramEngine=NULL;

}

Bud::Bud(BrancAMAP * br,char guRamificationType, double timeC, double phyAgeBeg, double ryt):VProcess()
{

	brancAMAP=br;

	toBePruned=NOPRUNING;

	axisref=(AxisReference*)br->getPlant()->getParamData(string("AMAPSimMod"));

	death = 0;

	this->branchRamificationType = this->guRamificationType = guRamificationType;  /* type de la branche (anticipe,retarde,trauma) */

	//time=nextTime=timeC;
	nextTime=birthTime=timeC;

	timeForATest = 0; /* time consumed for each test */

	endUnit = ENDGU; /* growth end flag */

	BrancAMAP * bear =br->getBearer() ;

	if (bear == br)
	{

		ordrez = 0;
	}
	else
	{

		if (br->nature == MainBranch)
		{
			ordrez = bear->getBud()->ordrez + 1;
		}
		else
		{
			ordrez = bear->getBud()->ordrez;
		}

	}

	groEngine= new GrowthEngineAMAP(this, phyAgeBeg, ryt);

	ramEngine = new RamifEngineAMAP(this);


}




Bud::~Bud(void)
{
	Scheduler::instance()->stop_process(this->process_id);

	delete groEngine;

	delete ramEngine;

}



void Bud::process_event (EventData * )
{
	int flag;

	this->nextTime=Scheduler::instance()->getTopClock();
	//setTimeAndPriority(this->nextTime, this->priority);

	if (this->brancAMAP->HierarcPtrs().size() == 0)
        return;

	this->brancAMAP->CurrentHierarcPtr(*(this->brancAMAP->HierarcPtrs().begin()));

	/* if bud is dead or at end of reference axis then test self pruning */
	if(death)
	{
		pruning (); /* self prune */
		return;
	} /* else try to grow the bud */
	else if ((flag = groEngine->growth ()) != 0)
	{
		/* if a new internode was borne then try to put a ramification */
		if(   brancAMAP->currentEntnNumberWithinGu
		   && (   groEngine->entnRealized
		       || endUnit & ENDGU
			   || (   endUnit & ENDCYCLE
			       && !(endUnit & ENDGU))))
		{
			// to prevent for for last internode of a cycle that was completed with empty tests
			bool reposit = false;
			if (   !brancAMAP->getCurrentEntn()
				&& ((CycleAMAP*)brancAMAP->getCurrentElementOfSubLevel(GROWTHCYCLE))->active)
			{
				reposit = true;
				brancAMAP->startPosit();
				brancAMAP->positOnLastEntnWithinCycle();
			}

			ramEngine->ramification ();

			if (reposit)
				brancAMAP->endPosit();
		}
	}

	/* if bud is dead or at end of reference axis, sets the self pruning eventData */
	if ( death || endUnit & FINISHED)
	{
		float lagdeath, lagend, pruningInstant;

		// don't forget to test traumatic branching
		if (death)
			ramEngine->ramification ();

		// compute the self pruning moment
		lagdeath = 9999;
		lagend = 9999;
		int nbGU = brancAMAP->getGuNumber();
		// patch to take the first fictive collar GU of RoCoCau (with phyage=0)
		pruningInstant = Scheduler::instance()->getTopClock();
		if (nbGU > 0)
		{
            brancAMAP->startPosit();
            float phyage = brancAMAP->positOnFirstGuWithinBranc();
            phyage = brancAMAP->getCurrentGu()->phyAgeGu-1;
            if (phyage < 0)
                phyage = 0;
            brancAMAP->endPosit();
            if (death)
                lagdeath = axisref->val1DParameter (rtlagdeath, (int) phyage, this->brancAMAP);
            if (   endUnit & FINISHED
                && brancAMAP->getGuNumber() > 0)
            {
                lagend = axisref->val1DParameter (rtlagend, (int) phyage, this->brancAMAP);

                //MODIF SG 09/05 D�calage du pruning � la fin du cycle courant (attention ajouter cas des plantes tropicales)
                /*if (brancAMAP->nature == SimpleImmediateOrgan)
                  lagend += 1;*/
                lagend += (float)(2.*EPSILON);
            }
    //		pruningInstant = (float)this->nextTime;
            if (lagend < lagdeath)
                pruningInstant += lagend;
            else
                pruningInstant += lagdeath;

            //MODIF SG 09/05 D�calage du pruning pour les organes � la fin du cycle courant (attention ajouter cas des plantes tropicales)
            /*if (brancAMAP->nature == SimpleImmediateOrgan)
              {
              pruningInstant=(int)Scheduler::instance().getTopClock()+1;
              }*/
        }


		// if self pruning moment then sets the total pruning eventData

//		this->nextTime=pruningInstant;
		if (brancAMAP->getGuNumber() > 0)
		{
            brancAMAP->startPosit();
            // patch to take the first fictive collar GU of RoCoCau (with phyage=0)
            float phyage = brancAMAP->positOnFirstGuWithinBranc();
            phyage = brancAMAP->getCurrentGu()->phyAgeGu-1;
            if (phyage < 0)
                phyage = 0;
            brancAMAP->endPosit();

			float pruningcoef = axisref->val1DParameter (rtpruningcoef, (int) phyage, this->brancAMAP);


			brancAMAP->CurrentHierarcPtr()->sortPosHierarc();

			if (pruningcoef >0	&& this->nextTime + 1 < pruningInstant	)
			{
				toBePruned=PROGRESSIVEPRUNING;
				death=1;
				Scheduler::instance()->self_signal_event( NULL,(double)this->nextTime + 1, Scheduler::instance()->getTopPriority());

			}
			else
			{
				toBePruned=TOTALPRUNING;
				death=1;
				Scheduler::instance()->self_signal_event( NULL,(double)pruningInstant, Scheduler::instance()->getTopPriority());
            }
		}

	}
}


void Bud::updateHierar (Hierarc * )
{
	//rien
}

void Bud::updatePruning()
{
	//rien pr l'instant

}


int Bud::pruning  ()
{
	if(toBePruned==PROGRESSIVEPRUNING)
	{
		if (brancAMAP->sizeElement > 0)
		{
            // patch to take the first fictive collar GU of RoCoCau (with phyage=0)
            brancAMAP->startPosit();
            float phyage = brancAMAP->positOnFirstGuWithinBranc();
            phyage = brancAMAP->getCurrentGu()->phyAgeGu-1;
            if (phyage < 0)
                phyage = 0;
            brancAMAP->endPosit();
			float pruningcoef = axisref->val1DParameter (rtpruningcoef, (int) phyage, this->brancAMAP);
			pruningcoef -= floor(pruningcoef);
			if(bernouilli(brancAMAP->getPlant(), pruningcoef))
			{
				toBePruned=TOTALPRUNING;
			}
			else
			{
				brancAMAP->progressivePruning();
				Scheduler::instance()->self_signal_event( NULL,(double)this->nextTime + 1, Scheduler::instance()->getTopPriority());
			}
		}
		else
			toBePruned=TOTALPRUNING;
	}

	if(toBePruned==TOTALPRUNING)
	{
		brancAMAP->getPlant()->getTopology().removeBranc(this->brancAMAP,1);
	}

	return 0;

}


/*

	void Bud::setTimeAndPriority( double time , int priority)
	{
	this->time=time;
	this->nextTime=time;
	this->priority=priority;
	}
	*/

void Bud::serialize(Archive& ar )
{

	VProcess::serialize(ar);

	if(ar.isWriting() )
	{
		ar<<ordrez<<guRamificationType<<branchRamificationType<<death<<endUnit;

		ar<<deathInstant<<endInstant<<previousInstant<<timeForATest<<nextTime<<toBePruned;

		if(this->SavePtr(ar,brancAMAP))
		{
			std::cout<<"It should be saved before";
		}

		ar<<*groEngine<<*ramEngine;

	}
	else
	{
		ar>>ordrez>>guRamificationType>>branchRamificationType>>death>>endUnit;

		ar>>deathInstant>>endInstant>>previousInstant>>timeForATest>>nextTime>>toBePruned;

		ArchivableObject * arBr;
		if(this->LoadPtr(ar,arBr))
		{
			std::cout<<"It would be saved before";
		}
		else brancAMAP=(BrancAMAP *)arBr;

		groEngine=new GrowthEngineAMAP(this);

		ramEngine=new RamifEngineAMAP(this);

		axisref=(AxisReference*)brancAMAP->getPlant()->getParamData(string("AMAPSimMod"));

		ar>>*groEngine>>*ramEngine;

	}


}



