/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <string.h>
#include <math.h>
#include "PlantAMAP.h"
#include "AmapSimMod.h"
#include "RamifEngineAMAP.h"
#include "Bud.h"
#include "randomFct.h"
#include "AmapConfig.h"
#include "SimplifTopoAMAP.h"
#include "DebugNew.h"


RamifEngineAMAP::RamifEngineAMAP(Bud * b)
{
	theBud=b;

	theBranc=b->brancAMAP;

	nblpa=((AxisReference*)b->getBranc()->getPlant()->getParamData(string("AMAPSimMod")))->getNbLPAxe();

	oldstate = new unsigned char [nblpa]; /* ramification state of previous node */

	stock = new long [nblpa];

	stockNature = new char[nblpa];


	for(int i=0; i<nblpa; i++)
	{
		stock[i]=0;
		oldstate[i]=0xff;
		stockNature[i]=0xff;
	}

}

RamifEngineAMAP::~RamifEngineAMAP(void)
{
	delete [] oldstate;

	delete [] stockNature;

	delete [] stock;
}


void RamifEngineAMAP::ramification  ()
{



	NatureAxe nature;
	long phyAge, lpIndex;
	long maxprol0;
	long ramif_autorisee_prol0, ramif_autorisee_ant;
	double instantsav;
	double timeloc;
	int testedGuIndex=theBranc->CurrentIndex();
	USint zoneIndex=theBranc->getCurrentZoneIndex();




	/* nature 2 never branch */
	if (theBranc->nature == SimpleImmediateOrgan)
		return;

	/* branching is forbidden for all ut */
	if (!((GrowthUnitAMAP *)theBranc->Ss()[testedGuIndex])->GuBranchingCapability)
		return;

	/* too late */
	//MODIF STOP TIME
	//if (Scheduler::instance().getTopClock() >= (double)(Scheduler::instance().getStopTime())+EPSILON)
	//  return;

	/* saves a local eventData */

	instantsav = timeloc= Scheduler::instance()->getTopClock();

	phyAge =  (int)((GrowthUnitAMAP *)theBranc->Ss()[testedGuIndex])->phyAgeGu;

	/* maximum depth for ramification */
    // patch to take the first fictive collar GU of RoCoCau (with phyage=0)
    theBranc->startPosit();
    float phyage = theBranc->positOnFirstGuWithinBranc();
    phyage = theBranc->getCurrentGu()->phyAgeGu-1;
    if (phyage < 0)
        phyage = 0;
    theBranc->endPosit();
	maxprol0 = (int)theBud->axisref->val1DParameter (rtmaxbrazero, (int)phyage, this->theBranc);

	ramif_autorisee_prol0 = 0;
	ramif_autorisee_ant = 0;

	/* current phy age is smaller than max phyAge */
	if (theBud->getPhyAge() < (int)(*(theBud->axisref))[glmaxref].ymin)
	{
		if (theBud->ordrez < maxprol0)
			ramif_autorisee_prol0 = 1;
	}

	/* immediate ramification is allways possible */
	ramif_autorisee_ant = 1;

	/* inspect each lateral production */
	/* there is a particular order to check branching :
	 * for a particular PL
	 * 1 : check for immediate traumatic branching
	 * 2 : if not branched, check immediate branching
	 * 3 : if not branched, check cycle delayed branching
	 * 4 : if not branched, check traumatic delayed branching
	 * 5 : if not branched, check delayed branching
	 * */
	for (lpIndex=0; lpIndex<nblpa; lpIndex++)
	{
		int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;

		nature = (NatureAxe)(int)theBud->axisref->val1DParameter (index+retype, phyAge, this->theBranc);
		// lp is inhibited
		if (nature >= 10)
			continue;

		/* check for max branching order */
		if (   nature == MainBranch
			&& !ramif_autorisee_prol0)
			continue;

		/* allways check for immediate branching */
		if (ramif_autorisee_ant)
		{
			if(theBranc->getCurrentEntn())
			{
				/* check for floating production zone */
				int phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu- 1;
				if ((int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc) == APICAL)
					checkFloatingBranchingZone (lpIndex, phyAge);

				immediateRamification (timeloc, lpIndex);
			}
		}

		/* cycle delayed at beginning of a cycle */
		if (   ramif_autorisee_ant
			&& theBud->endUnit & ENDCYCLE
			&& !(theBud->endUnit & ENDGU))
		{
			// std::cout<<"cycle delayed ramif"<<std::endl;
			cycleDelayedRamification (timeloc, lpIndex, 0);
		}

		/* traumatic delayed branching */
		if (   theBud->death
			&& theBud->axisref->val1DParameter (index+reDeathInfluence, (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu , this->theBranc) == 1)
		{
			//std::cout<<"traumatic ramif"<<std::endl;
			timeloc = theBud->previousInstant;
			traumaticRamification (timeloc, lpIndex, 0);
			timeloc = instantsav;
		}

		/* delayed branching at begining of ut */
		if (theBud->endUnit & ENDGU)
			//MODIF STOP TIME//&& theBud->previousInstant < (double)(Scheduler::instance().getStopTime()))
		{

			timeloc = theBud->previousInstant;
			delayedRamification (timeloc, lpIndex, 0);
			timeloc = instantsav;

		}
	}

}

void RamifEngineAMAP::checkFloatingBranchingZone (int lpIndex, int phyAge)
{
	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	int finprod = theBud->axisref->val1DParameter (index+refinprod, phyAge, theBranc);
	int unitType = (int)theBud->axisref->val1DParameter (rttypetop, phyAge, this->theBranc);
	int sizeUnit = 0;
	bool finUnit=false;

	switch (unitType)
	{
	case TOPOAXE:
		sizeUnit = theBranc->getCurrentEntnNumberWithinBranch();
		break;
	case TOPOGU:
		sizeUnit = theBranc->getCurrentEntnNumberWithinGu();
		break;
	case TOPOUC:
		sizeUnit = theBranc->getCurrentEntnNumberWithinCycle();
		break;
	}
	if (finprod >= sizeUnit)
		return;

	Hierarc *h = theBranc->getCurrentHierarc();
	int size = theBranc->getCurrentEntnNumberWithinBranch();
	int nb = theBranc->getCurrentHierarc()->getNbBorne();
	for (int i=theBranc->getCurrentHierarc()->getNbBorne()-1; i>=0; i--)
	{
		int k = h->bornePosition[i];
		if (   ((BrancAMAP*)(h->getBorne(i)))->lpIndex == lpIndex
			&& size - k <= sizeUnit
			&& size - k > finprod)
		{
			if (h->bornePosition[i] > sizeUnit)
				finUnit = true;
			theBranc->getPlant()->getTopology().removeBranc(h->getBorne(i), 0);
			h = theBranc->getCurrentHierarc();
			i=theBranc->getCurrentHierarc()->getNbBorne()-1;
		}

	}
}


void RamifEngineAMAP::immediateRamification (double timeloc, int lpIndex)
{
	if (theBud->axisref->getRamifModelType(lpIndex,1) == MARKOV)
		markovImmediateRamification (timeloc, lpIndex);
	else
		semimarkovImmediateRamification (timeloc, lpIndex);
}

/* immediate branching management */
void RamifEngineAMAP::semimarkovImmediateRamification (double timeloc, int lpIndex)
{
	long    poselemant;
	unsigned char    res;
	int     phyAge, indexpb,index,gap2nextvalue;
	float pb;

	USint zoneIndex=theBranc->getCurrentZoneIndex();



	res = 0;



	/* computes current phy age and position of node */
	phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu  - 1;
	poselemant = theBranc->calcPosElem (BASAL, (long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, theBranc), (long)theBud->axisref->val1DParameter (rttypetop, phyAge, theBranc));

	index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	if ((int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc) == BASAL)
	{
		/* check for ramification limitation according to position */
		if (   poselemant < theBud->axisref->val1DParameter (index+redebprod, phyAge, theBranc)-1
			|| poselemant > theBud->axisref->val1DParameter (index+refinprod, phyAge, theBranc)-1)
		{
			return ;
		}
	}


	/* computes reset of branching state */
	if (testInitialState (ANTICIPE, BASAL, lpIndex) == 0xff)
		stock[lpIndex] = 0;


	if (stock[lpIndex] <= 0)
	{
		index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;
		if (oldstate[lpIndex] == 0xff)
		{
			pb = theBud->axisref->val2DParameter (index+rcpbinitant, phyAge, poselemant, theBranc);
			if (bernouilli (theBranc->getPlant(), pb))
				stockNature[lpIndex] = NONBRANCHEANT;
			else
				stockNature[lpIndex] = ANTICIPE;
		}
		if (stockNature[lpIndex] == NONBRANCHEANT)
		{
			stock[lpIndex] = binodec (theBranc->getPlant(), theBud->axisref->val2DParameter (index+rcnb_bant, phyAge, poselemant, theBranc),
					1-theBud->axisref->val2DParameter (index+rcpb_bant, phyAge, poselemant, theBranc),
					(int)theBud->axisref->val2DParameter (index+rcd_bant, phyAge, poselemant, theBranc));
			if (stock[lpIndex] == -1)
				stock[lpIndex] = 999999;
			stock[lpIndex] += 1;
			oldstate[lpIndex] = ANTICIPE;
			stockNature[lpIndex] = ANTICIPE;
			indexpb = index + rcpb_bant;
		}
		else
		{
			stock[lpIndex] = binodec (theBranc->getPlant(), theBud->axisref->val2DParameter (index+rcnb_nbant, phyAge, poselemant, theBranc),
					1-theBud->axisref->val2DParameter (index+rcpb_nbant, phyAge, poselemant, theBranc),
					(int)theBud->axisref->val2DParameter (index+rcd_nbant, phyAge, poselemant, theBranc));
			if (stock[lpIndex] == -1)
				stock[lpIndex] = 999999;
			stock[lpIndex] += 1;
			oldstate[lpIndex] = NONBRANCHEANT;
			stockNature[lpIndex] = NONBRANCHEANT;
			indexpb = index + rcpb_nbant;
		}

		/* stock limitation to the next value of proba or zone/cycle/gu change */
		gap2nextvalue = theBud->axisref->nextCtrlPoint2DParameter (indexpb, poselemant) - poselemant;
		if (stock[lpIndex] > gap2nextvalue)
			stock[lpIndex] = gap2nextvalue;
	}



	/* if brranching took place sets the current verticille */
	if (stockNature[lpIndex] == ANTICIPE)
	{	//std::cout<<"immediate ramif"<<std::endl;
		this->verticille (lpIndex, timeloc, ANTICIPE);

	}

	stock[lpIndex]--;


}

/* immediate branching management */
void RamifEngineAMAP::markovImmediateRamification (double timeloc, int lpIndex)
{
	long    poselemant,poselemPrev;
	unsigned char    res;
	unsigned char * oldstate;
	int     phyAge, prevphyAge;
	long    state;

	USint zoneIndex=theBranc->getCurrentZoneIndex();

	oldstate=new unsigned char [nblpa];

	res = 0;

	/* save current bud and ramification state */

	memcpy (oldstate, this->oldstate, nblpa*sizeof(char));

	/* computes current phy age and position of node */
	phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu  - 1;

	poselemant = theBranc->calcPosElem (BASAL, (long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, theBranc), (long)theBud->axisref->val1DParameter (rttypetop, phyAge, theBranc));

	int indexZone = NB_VAR+NB_VAR_LP*lpIndex+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;
	int index = NB_VAR+NB_VAR_LP*lpIndex;
	/* check for ramification limitation according to position */
	if ((int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc) == BASAL)
	{
		if (   poselemant < theBud->axisref->val1DParameter (index+redebprod, phyAge, theBranc)-1
			|| poselemant > theBud->axisref->val1DParameter (index+refinprod, phyAge, theBranc)-1)
		{
			delete [] oldstate;
			return ;
		}
	}

	/* computes reset of brranching state */
	testInitialState (ANTICIPE, BASAL, lpIndex);
	if (   poselemant > 0
		&& theBranc->getCurrentEntnNumberWithinBranch() > 1)
	{

		oldstate[lpIndex] = this->oldstate[lpIndex];
		if (oldstate[lpIndex] == 0xff)
			oldstate[lpIndex] = 0;
		theBranc->startPosit();

		theBranc->positOnPreviousEntnWithinBranc();

		prevphyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu - 1;
		poselemPrev = theBranc->calcPosElem (BASAL, (long)theBud->axisref->val1DParameter (rtbasecompt, prevphyAge, theBranc), (long)theBud->axisref->val1DParameter (rttypetop, prevphyAge, theBranc));

		if (   theBud->axisref->val2DParameter (indexZone+rcpbinitant, phyAge, poselemant, theBranc)
				!= theBud->axisref->val2DParameter (NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex+NB_GLOBAL_LP_VAR+NB_ZONE_VAR*theBranc->getCurrentZoneIndex()+rcpbinitant, phyAge, poselemPrev, theBranc))
			oldstate[lpIndex] = 0xff;


		theBranc->endPosit();
	}
	else
		oldstate[lpIndex] = 0xff;

	state = oldstate[lpIndex];
	if (   state == NONBRANCHEANT
		|| state == NONBRANCHERETCYC)
		state = NONBRANCHE;

	/* activate markov process */
	res = (unsigned char)markovSimulation2 (theBranc->getPlant(),state,
			(float)1.-theBud->axisref->val2DParameter (indexZone+rcpbinitant, phyAge, poselemant, theBranc),
			theBud->axisref->val2DParameter (indexZone+rcpb_bant,   phyAge, poselemant, theBranc),
			theBud->axisref->val2DParameter (indexZone+rcpb_nbant,  phyAge, poselemant, theBranc),
			(int)RANDMARKOVA);

	if (!res)
		res = NONBRANCHEANT;


	this->oldstate[lpIndex] = oldstate[lpIndex] = ((InterNodeAMAP *)(theBranc->getCurrentEntn()))->branchent[lpIndex] = res;

	if (   res == NONBRANCHEANT
			&& poselemant > 0
			&& theBranc->getCurrentEntnNumberWithinBranch() > 1
			&& this->oldstate[lpIndex] == 0xff)
		oldstate[lpIndex] = 0xff;

	/* if brranching took place sets the current verticille */
	if (res == ANTICIPE)
	{	//std::cout<<"immediate ramif"<<std::endl;
		this->verticille (lpIndex, timeloc, ANTICIPE);
	}
	this->oldstate[lpIndex] = oldstate[lpIndex];
	delete [] oldstate;

}









void RamifEngineAMAP::traumaticRamification (double timeloc, int lpIndex, unsigned char oldstate)
{

	if (theBud->axisref->getRamifModelType(lpIndex,0) == MARKOV)
		markovTraumaticRamification (timeloc, lpIndex,oldstate);
	else
		semimarkovTraumaticRamification (timeloc, lpIndex,oldstate);


}

void RamifEngineAMAP::semimarkovTraumaticRamification (double timeloc, int lpIndex, unsigned char oldstate)
{

	long     phyAge, direction, stockt, poselemtrau;
	int gap2nextvalue, indexpb;

	float prob [3];

	long     res,entn,endIndex;
	USint zoneIndex=theBranc->getCurrentZoneIndex();
	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	int indexZone = index+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;



	phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu- 1;

	/* initialize toplogy according to counting base, counting direction */
	direction = (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc);


	if (theBranc->getCurrentEntnNumberWithinGu() == 0)
		return ;


	/* save current values */
	theBranc->startPosit();

	endIndex = theBranc->getCurrentEntnNumberWithinGu();

	if (direction == BASAL)
	{
		theBranc->positOnFirstEntnWithinGu();
	}
	else
	{
		theBranc->positOnLastEntnWithinGu();
	}


	/* computes reset of branching state */
	oldstate = this->testInitialState (TRAUMATIC, (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc), lpIndex);

	if (oldstate != 0xff)
	{
		InterNodeAMAP * tmp;
		if (direction == BASAL)
		{
			theBranc->startPosit();
			theBranc->positOnPreviousEntnWithinBranc();
			tmp=(InterNodeAMAP *)theBranc->getCurrentEntn();
			if(tmp != NULL)
				oldstate = tmp->branchent[lpIndex];
			else
				oldstate = 0xff;
			theBranc->endPosit();
		}
		else
			oldstate = 0xff;
	}

	stockt=0;



	/* check every nodes of current ut in the correct direction */
	for (entn=0;entn!=endIndex;entn++)
	{
		zoneIndex = theBranc->getCurrentZoneIndex();

		poselemtrau=theBranc->calcPosElem((long)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rttypetop, phyAge, theBranc));

		index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;
		if (stockt <= 0)
		{
			/* computes new stock */
			if (   oldstate == TRAUMATIC
				|| oldstate == ANTICIPE
				|| oldstate == RETARDECYC)
			{
				oldstate = NONBRANCHETRAUMATIC;
				stockt = binodec (theBranc->getPlant(), theBud->axisref->val2DParameter (index+rcnb_nbtrau, phyAge, poselemtrau, theBranc),
						1-theBud->axisref->val2DParameter (index+rcpb_nbtrau, phyAge, poselemtrau, theBranc),
						(int)theBud->axisref->val2DParameter (index+rcd_nbtrau, phyAge, poselemtrau, theBranc));
				if (stockt == -1)
					stockt = 999999;
				stockt += 1;
				indexpb = index+rcpb_nbtrau;
			}
			else
			{
				oldstate = TRAUMATIC;
				stock[lpIndex] = binodec (theBranc->getPlant(), theBud->axisref->val2DParameter (index+rcnb_btrau, phyAge, poselemtrau, theBranc),
						1-theBud->axisref->val2DParameter (index+rcpb_btrau, phyAge, poselemtrau, theBranc),
						(int)theBud->axisref->val2DParameter (index+rcd_btrau, phyAge, poselemtrau, theBranc));
				if (stockt == -1)
					stockt = 999999;
				stockt += 1;
				indexpb = index+rcpb_btrau;
			}
			/* stock limitation to the next value of proba */
			gap2nextvalue = theBud->axisref->nextCtrlPoint2DParameter (indexpb, poselemtrau) - poselemtrau;
			if (stockt > gap2nextvalue)
				stockt = gap2nextvalue;
		}//end if stock<=0

		/* if branching took place, create the verticile */
		index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
		if (   poselemtrau >= theBud->axisref->val1DParameter (index+redebprod, phyAge, theBranc)-1
			&& poselemtrau <= theBud->axisref->val1DParameter (index+refinprod, phyAge, theBranc)-1
			&& oldstate == TRAUMATIC
			&& stockt > 0)
			this->verticille (lpIndex, timeloc, oldstate);

		stockt --;


		if (direction == BASAL)
			theBranc->positOnNextEntnWithinGu();
		else
			theBranc->positOnPreviousEntnWithinGu();

	}

	/* restore values */
	theBranc->endPosit();


	this->oldstate[lpIndex] = oldstate;




}


/* computes delayed branching along the last ut */
void RamifEngineAMAP::markovTraumaticRamification (double timeloc, int lpIndex, unsigned char oldstate)
{

	long     phyAge, direction;


	long     entn,endIndex;
	USint zoneIndex=theBranc->getCurrentZoneIndex();
	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	int indexZone = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;




	phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu- 1;

	/* initialize toplogy according to counting base, counting direction */
	direction = (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc);


	if (theBranc->getCurrentEntnNumberWithinGu() == 0)
		return ;



	/* save current values */
	theBranc->startPosit();

	endIndex = theBranc->getCurrentEntnNumberWithinGu();

	if (direction == BASAL)
	{
		theBranc->positOnFirstEntnWithinGu();
	}
	else
	{
		theBranc->positOnLastEntnWithinGu();
	}


	/* computes reset of branching state */
	oldstate = this->testInitialState (TRAUMATIC, (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc), lpIndex);

	if (oldstate != 0xff)
	{
		InterNodeAMAP * tmp;
		if (direction == BASAL)
		{
			theBranc->startPosit();
			theBranc->positOnPreviousEntnWithinBranc();
			tmp=(InterNodeAMAP *)theBranc->getCurrentEntn();
			if(tmp != NULL)
				oldstate = tmp->branchent[lpIndex];
			else
				oldstate = 0xff;
			theBranc->endPosit();
		}
		else
			oldstate = 0xff;
	}


	/* check every nodes of current ut in the correct direction */
	for (entn=0;entn!=endIndex;entn++)
	{


		zoneIndex = theBranc->getCurrentZoneIndex();


		if (   (   ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == INDEFINI
				|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHEANT
				|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHERETCYC
				|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHETRAUMATIC
				|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHE)
			&& theBud->axisref->val1DParameter (index+repbaxil, phyAge, theBranc) != 0.0)
		{
			/* check delayed branching */

			oldstate = ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] = testTraumaticRamification (direction, oldstate, lpIndex);
			/* if branching took place, create the verticile */
			if (oldstate == TRAUMATIC)
			{
				//std::cout<<"delayed ramif"<<std::endl;
				this->verticille (lpIndex, timeloc, oldstate);
			}

		}
		if (direction == BASAL)
			theBranc->positOnNextEntnWithinGu();
		else
			theBranc->positOnPreviousEntnWithinGu();

	}

	/* restore values */
	theBranc->endPosit();


	this->oldstate[lpIndex] = oldstate;

}



void RamifEngineAMAP::delayedRamification (double timeloc, int lpIndex, unsigned char oldstate)
{

	if (theBud->axisref->getRamifModelType(lpIndex,0) == MARKOV)
		markovDelayedRamification (timeloc, lpIndex,oldstate);
	else
		semimarkovDelayedRamification (timeloc, lpIndex,oldstate);


}



void RamifEngineAMAP::semimarkovDelayedRamification (double timeloc, int lpIndex, unsigned char oldstate)
{

	long     phyAge, direction, stockt, poselemret;
	int gap2nextvalue, indexpb;

	float prob [3];

	long     res,entn,endIndex;
	USint zoneIndex=theBranc->getCurrentZoneIndex();
	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	int indexZone = index+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;



	phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu- 1;

	/* initialize toplogy according to counting base, counting direction */
	direction = (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc);


	if (theBranc->getCurrentEntnNumberWithinGu() == 0)
		return ;



	/* save current values */
	theBranc->startPosit();

	endIndex = theBranc->getCurrentEntnNumberWithinGu();

	if (direction == BASAL)
	{

		theBranc->positOnFirstEntnWithinGu();
	}
	else
	{
		theBranc->positOnLastEntnWithinGu();
	}


	/* computes reset of branching state */
	oldstate = this->testInitialState (RETARDE1, (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc), lpIndex);

	if (oldstate != 0xff)
	{
		InterNodeAMAP * tmp;
		if (   direction == BASAL)
		{		theBranc->startPosit();
			theBranc->positOnPreviousEntnWithinBranc();
			tmp=(InterNodeAMAP *)theBranc->getCurrentEntn();
			if(tmp != NULL)	oldstate = tmp->branchent[lpIndex];
			else oldstate = 0xff;
			theBranc->endPosit();
		}
		else
			oldstate = 0xff;
	}

	stockt=0;



	/* check every nodes of current ut in the correct direction */
	for (entn=0;entn!=endIndex;entn++)
	{
		///* computes reset of branching state */
		//oldstate = this->testInitialState (RETARDE1, (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc), lpIndex);

		zoneIndex = theBranc->getCurrentZoneIndex();

		poselemret=theBranc->calcPosElem((long)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rttypetop, phyAge, theBranc));

		index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;
		if (stockt <= 0)
		{
			if (oldstate == NONBRANCHE)
			{
				prob[0] = 0;
				prob[1] = theBud->axisref->val2DParameter (index+rcpb_0_1, phyAge, poselemret, theBranc);
				prob[2] = 1 - prob[1];
			}
			else if (oldstate == RETARDE1)
			{
				prob[0] = theBud->axisref->val2DParameter (index+rcpb_1_0, phyAge, poselemret, theBranc);
				prob[1] = 0;
				prob[2] = 1 - prob[0];
			}
			else if (oldstate == RETARDE2)
			{
				prob[0] = theBud->axisref->val2DParameter (index+rcpb_2_0, phyAge, poselemret, theBranc);
				prob[1] = 1 - prob[0];
				prob[2] = 0;
			}
			else
			{
				prob[0] = theBud->axisref->val2DParameter (index+rcpbinitret0, phyAge, poselemret, theBranc);
				prob[1] = theBud->axisref->val2DParameter (index+rcpbinitret1, phyAge, poselemret, theBranc);
				prob[2] = 1 - prob[0] - prob[1];
			}

			/* here the markov process whith return value
			 * 0 : not branched
			 * 1 : state 1
			 * 2 : state 2
			 * */
			res = cumulativeLaw (theBranc->getPlant(),prob, 3,  RANDMARKOVR);

			/* computes new stock */
			if (res == 0)
			{
				this->oldstate[lpIndex] = oldstate = NONBRANCHE;
				stockt = binodec (theBranc->getPlant(), theBud->axisref->val2DParameter (index+rcnb_st0, phyAge, poselemret, theBranc),
						1-theBud->axisref->val2DParameter (index+rcpb_st0, phyAge, poselemret, theBranc),
						(int)theBud->axisref->val2DParameter (index+rcd_st0, phyAge, poselemret, theBranc));
				if (stockt == -1)
					stockt = 999999;
				stockt += 1;
				indexpb = index+rcpb_st0;
			}
			else if (res == 1)
			{
				this->oldstate[lpIndex] = oldstate = RETARDE1;
				stockt = binodec (theBranc->getPlant(), theBud->axisref->val2DParameter (index+rcnb_st1, phyAge, poselemret, theBranc),
						1-theBud->axisref->val2DParameter (index+rcpb_st1, phyAge, poselemret, theBranc),
						(int)theBud->axisref->val2DParameter (index+rcd_st1, phyAge, poselemret, theBranc));
				if (stockt == -1)
					stockt = 999999;
				stockt += 1;
				indexpb = index+rcpb_st1;
			}
			else
			{
				this->oldstate[lpIndex] = oldstate = RETARDE2;
				stockt = binodec (theBranc->getPlant(), theBud->axisref->val2DParameter (index+rcnb_st2, phyAge, poselemret, theBranc),
						1-theBud->axisref->val2DParameter (index+rcpb_st2, phyAge, poselemret, theBranc),
						(int)theBud->axisref->val2DParameter (index+rcd_st2, phyAge, poselemret, theBranc));
				if (stockt == -1)
					stockt = 999999;
				stockt += 1;
				indexpb = index+rcpb_st2;
			}

			/* stock limitation to the next value of proba */
			gap2nextvalue = theBud->axisref->nextCtrlPoint2DParameter (indexpb, poselemret) - poselemret;
			if (stockt > gap2nextvalue)
				stockt = gap2nextvalue;
		}//end if stock<=0

		/* if branching took place, create the verticile */
		index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
		if (   poselemret >= theBud->axisref->val1DParameter (index+redebprod, phyAge, theBranc)-1
			&& poselemret <= theBud->axisref->val1DParameter (index+refinprod, phyAge, theBranc)-1
			&& (   oldstate == RETARDE1
				|| oldstate == RETARDE2)
			&& stockt > 0)
			this->verticille (lpIndex, timeloc, oldstate);

		stockt --;


		if (direction == BASAL)
			theBranc->positOnNextEntnWithinGu();
		else
			theBranc->positOnPreviousEntnWithinGu();

	}

	/* restore values */
	theBranc->endPosit();


	this->oldstate[lpIndex] = oldstate;




}


/* computes delayed branching along the last ut */
void RamifEngineAMAP::markovDelayedRamification (double timeloc, int lpIndex, unsigned char oldstate)
{

	long     phyAge, direction;


	long     entn,endIndex;
	USint zoneIndex=theBranc->getCurrentZoneIndex();
	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	int indexZone = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;


	phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu- 1;

	/* initialize toplogy according to counting base, counting direction */
	direction = (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc);


	if (theBranc->getCurrentEntnNumberWithinGu() == 0)
		return ;


	/* save current values */
	theBranc->startPosit();

	endIndex = theBranc->getCurrentEntnNumberWithinGu();

	if (direction == BASAL)
	{

		theBranc->positOnFirstEntnWithinGu();
	}
	else
	{
		theBranc->positOnLastEntnWithinGu();
	}


	/* computes reset of branching state */
	oldstate = this->testInitialState (RETARDE1, (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc), lpIndex);

	if (oldstate != 0xff)
	{
		InterNodeAMAP * tmp;
		if (direction == BASAL)
		{
			theBranc->startPosit();
			theBranc->positOnPreviousEntnWithinBranc();
			tmp=(InterNodeAMAP *)theBranc->getCurrentEntn();
			if(tmp != NULL)
				oldstate = tmp->branchent[lpIndex];
			else
				oldstate = 0xff;
			theBranc->endPosit();
		}
		else
			oldstate = 0xff;
	}


	/* check every nodes of current ut in the correct direction */
	for (entn=0;entn!=endIndex;entn++)
	{

		///* computes reset of branching state */
		//oldstate = this->testInitialState (RETARDE1, (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc), lpIndex);

		zoneIndex = theBranc->getCurrentZoneIndex();

		unsigned char toto =((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex];

		if (   (   ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == INDEFINI
				|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHEANT
				|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHERETCYC
				|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHETRAUMATIC
				|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHE)
			&& theBud->axisref->val1DParameter (index+repbaxil, phyAge, theBranc) != 0.0)
		{
			/* check delayed branching */

			this->oldstate[lpIndex] = oldstate = ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] = testDelayedRamification (direction, oldstate, lpIndex);
			/* if branching took place, create the verticile */
			if (   oldstate == RETARDE1
				|| oldstate == RETARDE2)
			{
				//std::cout<<"delayed ramif"<<std::endl;
				this->verticille (lpIndex, timeloc, oldstate);


			}

		}
		if (direction == BASAL)
			theBranc->positOnNextEntnWithinGu();
		else
			theBranc->positOnPreviousEntnWithinGu();

	}

	/* restore values */
	theBranc->endPosit();


	this->oldstate[lpIndex] = oldstate;

}

void RamifEngineAMAP::cycleDelayedRamification (double timeloc, int lpIndex, unsigned char oldstate)
{

	if (theBud->axisref->getRamifModelType(lpIndex,2) == MARKOV)
		markovCycleDelayedRamification (timeloc, lpIndex,oldstate);
//	else
//		semimarkovDelayedRamification (timeloc, lpIndex,oldstate);


}






/* computes delayed branching along the last ut */
void RamifEngineAMAP::markovCycleDelayedRamification (double timeloc, int lpIndex, unsigned char oldstate)
{

	long     phyAge, direction;


	long     entn,endIndex;
	USint zoneIndex=theBranc->getCurrentZoneIndex();




	phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu- 1;

	int indexZone = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;
	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	/* initialize toplogy according to counting base, counting direction */
	direction = (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc);


	if (theBranc->getCurrentEntnNumberWithinCycle() == 0)
		return ;



	/* save current values */
	theBranc->startPosit();

	endIndex = 0;
	endIndex = (long)theBranc->getCurrentEntnNumberWithinCycle();

	if (direction == BASAL)
	{

		theBranc->positOnFirstEntnWithinCycle();
	}
	else
	{
		theBranc->positOnLastEntnWithinCycle();
	}


	/* computes reset of branching state */
	oldstate = this->testInitialState (RETARDE1, (int)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, theBranc), lpIndex);

	if (oldstate != 0xff)
	{
		InterNodeAMAP * tmp;
		if (   direction == BASAL)
		{		theBranc->startPosit();
			theBranc->positOnPreviousEntnWithinBranc();
			tmp=(InterNodeAMAP *)theBranc->getCurrentEntn();
			if(tmp != NULL)	oldstate = tmp->branchent[lpIndex];
			else oldstate = 0xff;
			theBranc->endPosit();
		}
		else
			oldstate = 0xff;
	}


	/* check every nodes of current ut in the correct direction */
	for (entn=0;entn!=endIndex;entn++)
	{


		zoneIndex = theBranc->getCurrentZoneIndex();

		if ((   ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == INDEFINI
					|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHEANT
					|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHETRAUMATIC
					|| ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] == NONBRANCHE)
				&& theBud->axisref->val1DParameter (index+repbaxil, phyAge, theBranc) != 0.0)
		{
			/* check cycle delayed branching */

			oldstate = ((InterNodeAMAP *)theBranc->getCurrentEntn())->branchent[lpIndex] = testCycleDelayedRamification (direction, oldstate, lpIndex);
			/* if branching took place, create the verticile */
			if (oldstate == RETARDECYC)
			{
				//std::cout<<"delayed ramif"<<std::endl;
				this->verticille (lpIndex, timeloc, oldstate);


			}

		}
		if (direction == BASAL)
			theBranc->positOnNextEntnWithinCycle();
		else
			theBranc->positOnPreviousEntnWithinCycle();

	}

	/* restore values */
	theBranc->endPosit();


	this->oldstate[lpIndex] = oldstate;

}


/* check for delayed branching */
char RamifEngineAMAP::testDelayedRamification(long direction, unsigned char oldstate, int lpIndex)
{
	long    poselemret,poselemPrev;
	char    res;
	float   prob[3];
	int phyAge, prevphyAge, bornePhyAge;
	USint zoneIndexPrev=65535;
	//On recupere les position courante ( car on en positionnement)
	USint zoneIndex=theBranc->getCurrentZoneIndex();
	int testIndexWithinBranch=theBranc->getTestIndexWithinBranch();

	res = 0;

	phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu - 1;

	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	int indexZone = index+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;

	// JFB patch to include reiteration that MUST be apical relative to GU
	double rythm;
	long typetop;

	int poselem = theBranc->calcPosElem ((long)theBud->axisref->val1DParameter (index+resensinitret, phyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rttypetop, phyAge, theBranc));
	int sautrami = (int)theBud->axisref->val2DParameter (indexZone+rcsautramiret1, phyAge, poselem, theBranc);

	if (sautrami == 0)
	{
		typetop = TOPOGU;
	}
	else
	{
		typetop = (long)theBud->axisref->val1DParameter (rttypetop, phyAge, this->theBranc);
	}

	/* first check if we are into the authorized ramification zone */
	poselemret = theBranc->calcPosElem ( BASAL,
			(long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, this->theBranc),
			typetop);


	if (poselemret < theBud->axisref->val1DParameter (index+redebprod, phyAge, this->theBranc)-1)
		return 0xff;
	else if (poselemret > theBud->axisref->val1DParameter (index+refinprod, phyAge, this->theBranc)-1)
		return 0;

	/* computes current and previous position */
	poselemret = theBranc->calcPosElem ((long)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, this->theBranc),
			(long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, this->theBranc),
			typetop);



	/* computes reset of branching state */
	if (poselemret > 0 && testIndexWithinBranch > 0)
	{
		theBranc->startPosit();
		if (direction == BASAL)
			theBranc->positOnPreviousEntnWithinBranc();
		else
			theBranc->positOnNextEntnWithinBranc();
		int indexZonePrev = index+NB_GLOBAL_LP_VAR+theBranc->getCurrentZoneIndex()*NB_ZONE_VAR;

		prevphyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu - 1;
		poselemPrev = theBranc->calcPosElem ( (long)theBud->axisref->val1DParameter (index+resensbrancret, prevphyAge, this->theBranc),
				(long)theBud->axisref->val1DParameter (rtbasecompt, prevphyAge, theBranc),
				typetop);
		theBranc->endPosit();

		if (   oldstate == NONBRANCHE
			|| oldstate == NONBRANCHEANT
			|| oldstate == NONBRANCHERETCYC
			|| oldstate == NONBRANCHETRAUMATIC)
		{
			if (   theBud->axisref->val2DParameter (indexZone+rcpbinitret0, phyAge, poselemret, this->theBranc)
				!= theBud->axisref->val2DParameter (indexZonePrev+rcpbinitret0, phyAge, poselemPrev, this->theBranc))
				oldstate = 0xff;
		}
		else if (   oldstate == ANTICIPE
				|| oldstate == RETARDECYC
				|| oldstate == TRAUMATIC
				|| oldstate == RETARDE1)
		{
			if (   theBud->axisref->val2DParameter (indexZone+rcpbinitret1, phyAge, poselemret, this->theBranc)
					!= theBud->axisref->val2DParameter (indexZonePrev+rcpbinitret1, phyAge, poselemPrev, this->theBranc))
				oldstate = 0xff;
		}
		else if (oldstate == RETARDE2)
		{
			if (   1.- theBud->axisref->val2DParameter (indexZone+rcpbinitret0, phyAge, poselemret, this->theBranc)
					- theBud->axisref->val2DParameter (indexZone+rcpbinitret1, phyAge, poselemret, this->theBranc)
					!= 1.- theBud->axisref->val2DParameter (indexZonePrev+rcpbinitret0, phyAge, poselemPrev, this->theBranc)
					- theBud->axisref->val2DParameter (indexZonePrev+rcpbinitret1, phyAge, poselemPrev, this->theBranc))
				oldstate = 0xff;
		}
	}
	else if (poselemret == 0)
		oldstate = 0xff;

	/* computes probabilities for markov process */
	if (   oldstate == NONBRANCHE
		|| oldstate == NONBRANCHEANT
		|| oldstate == NONBRANCHERETCYC
		|| oldstate == NONBRANCHETRAUMATIC)
	{
		prob[0] = theBud->axisref->val2DParameter (indexZone+rcpb_st0, phyAge, poselemret, this->theBranc);
		prob[1] = theBud->axisref->val2DParameter (indexZone+rcpb_0_1, phyAge, poselemret, this->theBranc);
		prob[2] = 1 - prob[0] - prob[1];
	}
	else if (   oldstate == ANTICIPE
			|| oldstate == RETARDECYC
			|| oldstate == TRAUMATIC
			|| oldstate == RETARDE1)
	{
		prob[0] = theBud->axisref->val2DParameter (indexZone+rcpb_st1, phyAge, poselemret, this->theBranc);
		prob[1] = theBud->axisref->val2DParameter (indexZone+rcpb_1_0, phyAge, poselemret, this->theBranc);
		prob[2] = 1 - prob[0] - prob[1];
	}
	else if (oldstate == RETARDE2)
	{
		prob[0] = theBud->axisref->val2DParameter (indexZone+rcpb_st2, phyAge, poselemret, this->theBranc);
		prob[1] = theBud->axisref->val2DParameter (indexZone+rcpb_2_0, phyAge, poselemret, this->theBranc);
		prob[2] = 1 - prob[0] - prob[1];
	}
	else
	{
		prob[0] = theBud->axisref->val2DParameter (indexZone+rcpbinitret0, phyAge, poselemret, this->theBranc);
		prob[1] = theBud->axisref->val2DParameter (indexZone+rcpbinitret1, phyAge, poselemret, this->theBranc);
		prob[2] = 1 - prob[0] - prob[1];
	}



	/* here the markov process whith return value
	 * 0 : not branched
	 * 1 : state 1
	 * 2 : state 2
	 * */
	res = (char)cumulativeLaw (theBranc->getPlant(),prob, 3,  RANDMARKOVR);

	/* interpretes result of simulation according to previous branching state */
	if (   oldstate == NONBRANCHE
		|| oldstate == NONBRANCHEANT
		|| oldstate == NONBRANCHERETCYC
		|| oldstate == NONBRANCHETRAUMATIC)
	{
		if (res == 0)
			res = oldstate;
		else if (res == 1)
			res = RETARDE1;
		else if (res == 2)
			res = RETARDE2;
	}
	else if (   oldstate == ANTICIPE
			|| oldstate == RETARDECYC
			|| oldstate == TRAUMATIC
			|| oldstate == RETARDE1)
	{
		if (res == 0)
			res = oldstate;
		else if (res == 1)
			res = NONBRANCHE;
		else if (res == 2)
			res = RETARDE2;
	}
	else if (oldstate == RETARDE2)
	{
		if (res == 0)
			res = oldstate;
		else if (res == 1)
			res = NONBRANCHE;
		else if (res == 2)
			res = RETARDE1;
	}
	else
	{
		if (res == 0)
			res = NONBRANCHE;
		else if (res == 1)
			res = RETARDE1;
		else if (res == 2)
			res = RETARDE2;
	}

	return(res);
}






/* check for delayed branching */
char RamifEngineAMAP::testCycleDelayedRamification(long direction, unsigned char oldstate, int lpIndex)
{
	long    poselemret,poselemPrev;
	char    res;
	float   prob[3];
	int phyAge, prevphyAge;
	USint zoneIndexPrev=65535;
	//On recupere les position courante ( car on en positionnement)
	USint zoneIndex=theBranc->getCurrentZoneIndex();
	int testIndexWithinBranch=theBranc->getTestIndexWithinBranch();

	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	int indexZone = index+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;

	res = 0;

	phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu - 1;

	/* first check if we are into the authorized ramification zone */
	poselemret = theBranc->calcPosElem ( BASAL,
			(long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, this->theBranc),
			(long)theBud->axisref->val1DParameter (rttypetop, phyAge, this->theBranc));



	if (poselemret < theBud->axisref->val1DParameter (index+redebprod, phyAge, this->theBranc)-1)
		return 0xff;
	else if (poselemret > theBud->axisref->val1DParameter (index+refinprod, phyAge, this->theBranc)-1)
		return 0;

	/* computes current and previous position */
	poselemret = theBranc->calcPosElem ((long)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, this->theBranc),
			(long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, this->theBranc),
			(long)theBud->axisref->val1DParameter (rttypetop, phyAge, this->theBranc));



	/* computes reset of branching state */
	if (poselemret > 0 && testIndexWithinBranch > 0)
	{
		theBranc->startPosit();
		if (direction == BASAL)theBranc->positOnPreviousEntnWithinBranc();
		else theBranc->positOnNextEntnWithinBranc();
		int indexZonePrev = index+NB_GLOBAL_LP_VAR+theBranc->getCurrentZoneIndex()*NB_ZONE_VAR;

		prevphyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu - 1;
		poselemPrev = theBranc->calcPosElem ( (long)theBud->axisref->val1DParameter (index+resensbrancret, prevphyAge, this->theBranc),
				(long)theBud->axisref->val1DParameter (rtbasecompt, prevphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rttypetop, prevphyAge, theBranc));
		theBranc->endPosit();

		if (   oldstate == NONBRANCHE
			|| oldstate == NONBRANCHEANT
			|| oldstate == NONBRANCHETRAUMATIC)
		{
			if (   theBud->axisref->val2DParameter (indexZone+rcpbinitretcyc, phyAge, poselemret, this->theBranc)
					!= theBud->axisref->val2DParameter (indexZonePrev+rcpbinitretcyc, phyAge, poselemPrev, this->theBranc))
				oldstate = 0xff;
		}
		else if (   oldstate == ANTICIPE
				|| oldstate == RETARDECYC
				|| oldstate == TRAUMATIC)
		{
			if (   theBud->axisref->val2DParameter (indexZone+rcpbinitretcyc, phyAge, poselemret, this->theBranc)
					!= theBud->axisref->val2DParameter (indexZonePrev+rcpbinitretcyc, phyAge, poselemPrev, this->theBranc))
				oldstate = 0xff;
		}
	}

	if (   oldstate == NONBRANCHEANT
		|| oldstate == NONBRANCHERETCYC)
		oldstate = NONBRANCHE;
	if (   oldstate == ANTICIPE
		|| oldstate == RETARDECYC)
		oldstate = RETARDE1;

	/* activate markov process */
	res = (unsigned char)markovSimulation2 (theBranc->getPlant(),oldstate,
			(float)1.-theBud->axisref->val2DParameter (indexZone+rcpbinitretcyc, phyAge, poselemret, theBranc),
			theBud->axisref->val2DParameter (indexZone+rcpb_bretcyc,   phyAge, poselemret, theBranc),
			theBud->axisref->val2DParameter (indexZone+rcpb_nbretcyc,  phyAge, poselemret, theBranc),
			(int)RANDMARKOVA);

	if (!res)
		res = NONBRANCHERETCYC;
	else
		res = RETARDECYC;


	return(res);
}



/* check for delayed branching */
char RamifEngineAMAP::testTraumaticRamification(long direction, unsigned char oldstate, int lpIndex)
{
	long    poselemret,poselemPrev;
	char    res;
	float   prob[3];
	int phyAge, prevphyAge;
	USint zoneIndexPrev=65535;
	//On recupere les position courante ( car on en positionnement)
	USint zoneIndex=theBranc->getCurrentZoneIndex();
	int testIndexWithinBranch=theBranc->getTestIndexWithinBranch();

	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	int indexZone = index+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;

	res = 0;

	phyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu - 1;

	/* first check if we are into the authorized ramification zone */
	poselemret = theBranc->calcPosElem ( BASAL,
			(long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, this->theBranc),
			(long)theBud->axisref->val1DParameter (rttypetop, phyAge, this->theBranc));



	if (poselemret < theBud->axisref->val1DParameter (index+redebprod, phyAge, this->theBranc)-1)
		return 0xff;
	else if (poselemret > theBud->axisref->val1DParameter (index+refinprod, phyAge, this->theBranc)-1)
		return 0;

	/* computes current and previous position */
	poselemret = theBranc->calcPosElem ((long)theBud->axisref->val1DParameter (index+resensbrancret, phyAge, this->theBranc),
			(long)theBud->axisref->val1DParameter (rtbasecompt, phyAge, this->theBranc),
			(long)theBud->axisref->val1DParameter (rttypetop, phyAge, this->theBranc));



	/* computes reset of branching state */
	if (poselemret > 0 && testIndexWithinBranch > 0)
	{
		theBranc->startPosit();
		if (direction == BASAL)
			theBranc->positOnPreviousEntnWithinBranc();
		else
			theBranc->positOnNextEntnWithinBranc();
		int indexZonePrev = index+NB_GLOBAL_LP_VAR+theBranc->getCurrentZoneIndex()*NB_ZONE_VAR;

		prevphyAge = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu - 1;
		poselemPrev = theBranc->calcPosElem ( (long)theBud->axisref->val1DParameter (index+resensbrancret, prevphyAge, this->theBranc),
				(long)theBud->axisref->val1DParameter (rtbasecompt, prevphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rttypetop, prevphyAge, theBranc));
		theBranc->endPosit();

		if (   oldstate == NONBRANCHE
			|| oldstate == NONBRANCHEANT
			|| oldstate == NONBRANCHERETCYC
			|| oldstate == NONBRANCHETRAUMATIC)
		{
			if (   theBud->axisref->val2DParameter (indexZone+rcpbinittrau, phyAge, poselemret, this->theBranc)
				!= theBud->axisref->val2DParameter (indexZonePrev+rcpbinittrau, phyAge, poselemPrev, this->theBranc))
				oldstate = 0xff;
		}
		else if (   oldstate == ANTICIPE
				|| oldstate == RETARDECYC
				|| oldstate == TRAUMATIC)
		{
			if (   theBud->axisref->val2DParameter (indexZone+rcpbinittrau, phyAge, poselemret, this->theBranc)
				!= theBud->axisref->val2DParameter (indexZonePrev+rcpbinittrau, phyAge, poselemPrev, this->theBranc))
				oldstate = 0xff;
		}
	}

	if (   oldstate == NONBRANCHEANT
		|| oldstate == NONBRANCHERETCYC
		|| oldstate == NONBRANCHETRAUMATIC)
		oldstate = NONBRANCHE;
	if (   oldstate == ANTICIPE
		|| oldstate == RETARDECYC
		|| oldstate == TRAUMATIC)
		oldstate = RETARDE1;

	/* activate markov process */
	res = (unsigned char)markovSimulation2 (theBranc->getPlant(),oldstate,
			(float)1.-theBud->axisref->val2DParameter (indexZone+rcpbinittrau, phyAge, poselemret, theBranc),
			theBud->axisref->val2DParameter (indexZone+rcpb_btrau,   phyAge, poselemret, theBranc),
			theBud->axisref->val2DParameter (indexZone+rcpb_nbtrau,  phyAge, poselemret, theBranc),
			(int)RANDMARKOVA);

	if (!res)
		res = NONBRANCHETRAUMATIC;
	else
		res = TRAUMATIC;


	return(res);
}




/* test reset of branching state accordind to position and previous state */
unsigned char RamifEngineAMAP::testInitialState (long typeramif, long direction, long lpIndex)
{
	int topotype;
	//Pas en posit on pe recup les index courant
	int testedGuIndex=theBranc->CurrentIndex();
	int testIndexWithinCycle=theBranc->getTestIndexWithinCycle();
	int testIndexWithinGu=theBranc->getTestIndexWithinGu();
	int zonetestIndex=theBranc->getTestIndexWithinCurrentZone2();
	USint cycleIndex= theBranc->getCurrentCycleIndexWithinGu();

	/* initial state is set if :
	 * first node of each zone of first cycle of first Gu in axe counting topology
	 * first node of each zone of first cycle of each Gu in ut counting topology
	 * first node of each zone of each cycle in cycle counting topology
	 * */

	topotype = (int)theBud->axisref->val1DParameter (rttypetop, (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu, this->theBranc);

	if (   topotype == TOPOAXE
		&& theBranc->getTestIndexWithinBranch() == 0)
		//&& testedGuIndex == 0
		//&& ( testIndexWithinCycle == 0))
				//|| (zonetestIndex == 0
				//	&& cycleIndex == 0)))
	{
		this->oldstate[lpIndex] = 0xff;
		//bPrev->oldstate[lpIndex] = 0xff;
	}
	else if (topotype == TOPOGU)
	{
		if (direction == BASAL)
		{
			if (   testIndexWithinGu == 0)
				//|| (   zonetestIndex == 0
				//	&& cycleIndex == 0))
			{
				this->oldstate[lpIndex] = 0xff;
				//bPrev->oldstate[lpIndex] = 0xff;
			}
		}
		else if (testIndexWithinGu == theBranc->getTestNumberWithinGu()-1)
				//|| (zonetestIndex == 0
				//	&& cycleIndex == 0))
		{
			this->oldstate[lpIndex] = 0xff;
			//bNext->oldstate[lpIndex] = 0xff;
		}
	}
	else if (   topotype == TOPOUC
			 || typeramif == RETARDECYC)
	{
		if (direction == BASAL)
		{
			if (   testIndexWithinCycle == 0)
				//|| zonetestIndex == 0)
			{
				this->oldstate[lpIndex] = 0xff;
				//bPrev->oldstate[lpIndex] = 0xff;
			}
		}
		else if (   testIndexWithinCycle ==  theBranc->getTestNumberWithinCycle()-1)
				 //|| zonetestIndex == 0)
		{
			this->oldstate[lpIndex] = 0xff;
			//bNext->oldstate[lpIndex] = 0xff;
		}
	}

	return this->oldstate[lpIndex];
}






double RamifEngineAMAP::computePhyAgeBeg (//calcul le prochain phyAge
		int lpIndex, /* lateral production index */
		char branchRamificationType, /* ramification type */
		double *rhythm) /* returned rhythm */
{
	long		poselem;
	double	    phyAgeNext;
	long		bearerphyAge;
	double	    bearerphyAgef, bearerIncrement;
	double		sautrami;
	int absoluteJump;
	USint zoneIndex=theBranc->getCurrentZoneIndex();

	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	int indexZone = index+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;


	bearerphyAgef = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu- 1;
	bearerphyAgef = ((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu- 1;
	bearerphyAge = floor(bearerphyAgef);
	bearerIncrement = theBud->axisref->val1DParameter (rtvitesse, bearerphyAge, theBranc);

	if (branchRamificationType == RETARDE1)
	{
		poselem = theBranc->calcPosElem ((long)theBud->axisref->val1DParameter (index+resensinitret, bearerphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rtbasecompt, bearerphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rttypetop, bearerphyAge, theBranc));
		sautrami = (int)theBud->axisref->val2DParameter (indexZone+rcsautramiret1, bearerphyAge, poselem, theBranc);

		if (sautrami < 0)
		{
			absoluteJump=1;
			sautrami = -sautrami;
		}
		else
			absoluteJump=0;

		if (sautrami == 0)
		{
			float pbProgress = theBud->axisref->val1DParameter (rtprogress, bearerphyAge, theBranc);
			sautrami = bearerphyAgef + 1;
			if (pbProgress == 1.)
				sautrami +=  1;
		}
		phyAgeNext = binodec (theBranc->getPlant(),theBud->axisref->val2DParameter (indexZone+rcnbvarisautret1, bearerphyAge, poselem, theBranc),
				theBud->axisref->val2DParameter (indexZone+rcpbvarisautret1, bearerphyAge, poselem, theBranc),
				0) + sautrami;

		if (   phyAgeNext == 0
			|| (   absoluteJump == 0
				&& phyAgeNext <= bearerphyAgef+1))
			phyAgeNext = theBud->getGrowth()->phyAgeRef + 1;

		if (phyAgeNext <= 1)
			phyAgeNext = 99999;
		*rhythm = theBud->axisref->val2DParameter (indexZone+rcrythminitret1, bearerphyAge, poselem, theBranc);
	}
	else if (branchRamificationType == RETARDE2)
	{
		poselem = theBranc->calcPosElem ((long)theBud->axisref->val1DParameter (index+resensinitret, bearerphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rtbasecompt, bearerphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rttypetop, bearerphyAge, theBranc));
		sautrami = (long)theBud->axisref->val2DParameter (indexZone+rcsautramiret2, bearerphyAge, poselem, theBranc);

		if (sautrami < 0)
		{
			absoluteJump=1;
			sautrami = -sautrami;
		}
		else
			absoluteJump=0;

		if (sautrami == 0)
		{
			float pbProgress = theBud->axisref->val1DParameter (rtprogress, bearerphyAge, theBranc);
			sautrami = bearerphyAgef + 1;
			if (pbProgress == 1.)
				sautrami +=  1;
		}
		phyAgeNext = binodec (theBranc->getPlant(),theBud->axisref->val2DParameter (indexZone+rcnbvarisautret2, bearerphyAge, poselem, theBranc),
				theBud->axisref->val2DParameter (indexZone+rcpbvarisautret2, bearerphyAge, poselem, theBranc),
				0) + sautrami;

		if (   phyAgeNext == 0
			|| (   absoluteJump == 0
				&& phyAgeNext <= bearerphyAgef+1))
			phyAgeNext = theBud->getGrowth()->phyAgeRef + 1;

		if (phyAgeNext <= 1)
			phyAgeNext = 99999;
		*rhythm = theBud->axisref->val2DParameter (indexZone+rcrythminitret2, bearerphyAge, poselem, theBranc);
	}
	else if (branchRamificationType == TRAUMATIC)
	{
//		poselem = theBranc->calcPosElem ( (long)theBud->axisref->val1DParameter (NB_VAR+(nblpa*zoneIndex+lpIndex)*NB_VAR_ENT+resenstrau, bearerphyAge, theBranc),
//		poselem = theBranc->calcPosElem ( APICAL,
		poselem = theBranc->calcPosElem ( (long)theBud->axisref->val1DParameter (index+resensinitret, bearerphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rtbasecompt, bearerphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rttypetop, bearerphyAge, theBranc));
		sautrami = (int)theBud->axisref->val2DParameter (indexZone+rcsautramitrau, bearerphyAge, poselem, theBranc);

		if (sautrami < 0)
		{
			absoluteJump=1;
			sautrami = -sautrami;
		}
		else
			absoluteJump=0;

		if (sautrami == 0)
		{
			float pbProgress = theBud->axisref->val1DParameter (rtprogress, bearerphyAge, theBranc);
			sautrami = bearerphyAgef + 1;
			if (pbProgress == 1.)
				sautrami +=  1;
		}
		phyAgeNext = sautrami + binodec (theBranc->getPlant(),theBud->axisref->val2DParameter (indexZone+rcnbvarisauttrau, bearerphyAge, poselem, theBranc),
				theBud->axisref->val2DParameter (indexZone+rcpbvarisauttrau, bearerphyAge, poselem, theBranc),
				0);

		if (   phyAgeNext == 0
			|| (   absoluteJump == 0
				&& phyAgeNext <= bearerphyAgef+1))
			phyAgeNext = theBud->getGrowth()->phyAgeRef + 1;

		if (phyAgeNext <= 1)
			phyAgeNext = 99999;
		else if (phyAgeNext == 1)
			phyAgeNext = 99999;
		*rhythm = theBud->axisref->val2DParameter (indexZone+rcrythminittrau, bearerphyAge, poselem, theBranc);
	}
	else if (branchRamificationType == ANTICIPE)
	{
		poselem = theBranc->calcPosElem (BASAL,
				(long)theBud->axisref->val1DParameter (rtbasecompt, bearerphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rttypetop, bearerphyAge, theBranc));
		sautrami = (int)theBud->axisref->val2DParameter (indexZone+rcsautramiant, bearerphyAge, poselem, theBranc);

		if (sautrami < 0)
		{
			absoluteJump=1;
			sautrami = -sautrami;
		}
		else
			absoluteJump=0;

		if (sautrami == 0)
		{
//			sautrami = bearerphyAgef + bearerIncrement;
			if (theBranc->getBud()->endUnit & ENDGU)
				sautrami = theBranc->getBud()->getGrowth()->phyAgeRef + 1;
			else
				sautrami = bearerphyAgef + 1;
		}
		phyAgeNext = sautrami + binodec (theBranc->getPlant(),theBud->axisref->val2DParameter (indexZone+rcnbvarisautant, bearerphyAge, poselem, theBranc),
				theBud->axisref->val2DParameter (indexZone+rcpbvarisautant, bearerphyAge, poselem, theBranc),
				0);

		if (   phyAgeNext == 0
			|| (   absoluteJump == 0
				&& phyAgeNext <= theBud->getGrowth()->phyAgeRef + 1))
			phyAgeNext = theBud->getGrowth()->phyAgeRef + 1;

		if (phyAgeNext < 1)
			phyAgeNext = 99999;
		*rhythm = theBud->axisref->val2DParameter (indexZone+rcrythminitant, bearerphyAge, poselem, theBranc);
	}
	else
	{
		poselem = theBranc->calcPosElem ((long)theBud->axisref->val1DParameter (index+resensinitret, bearerphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rtbasecompt, bearerphyAge, theBranc),
				(long)theBud->axisref->val1DParameter (rttypetop, bearerphyAge, theBranc));
		sautrami = (int)theBud->axisref->val2DParameter (indexZone+rcsautramiretc, bearerphyAge, poselem, theBranc);

		if (sautrami < 0)
		{
			absoluteJump=1;
			sautrami = -sautrami;
		}
		else
			absoluteJump=0;

		if (sautrami == 0)
			sautrami = bearerphyAgef + 1;
		phyAgeNext = sautrami + binodec (theBranc->getPlant(),theBud->axisref->val2DParameter (indexZone+rcnbvarisautretc, bearerphyAge, poselem, theBranc),
				theBud->axisref->val2DParameter (indexZone+rcpbvarisautretc, bearerphyAge, poselem, theBranc),
				0);

		if (   phyAgeNext == 0
			|| (   absoluteJump == 0
				&& phyAgeNext <= theBud->getGrowth()->phyAgeRef + 1))
			phyAgeNext = theBud->getGrowth()->phyAgeRef + 1;

		if (phyAgeNext < 1)
			phyAgeNext = 99999;
		*rhythm = theBud->axisref->val2DParameter (indexZone+rcrythminitretc, bearerphyAge, poselem, theBranc);
	}

	return phyAgeNext;
}


/* stes the new buds of a verticille */
int RamifEngineAMAP::verticille (int lpIndex, double timeloc, int type)
{
	int i, nbre,  phyAgeLoc, nb=0;
	double	phyAgeNext;
	NatureAxe nature;
	float pb;
	double instant, nextInstant, rythm;
	std::string critere;

	BrancAMAP * bVert=NULL;
	USint zoneIndex=theBranc->getCurrentZoneIndex();

	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*lpIndex;
	int indexZone = index+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;

	/* computes the number of buds for that verticille */
	phyAgeLoc = (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu-1;

	nbre = (int)theBud->axisref->val1DParameter (index+renbre, phyAgeLoc, theBranc);


	if ((pb = theBud->axisref->val1DParameter (index+repbaxil, phyAgeLoc, theBranc)) == 0.0)
		return 0;

	/* computes next eventData moment */
	nature = (NatureAxe)(int)theBud->axisref->val1DParameter (index+retype, phyAgeLoc, theBranc);
	if (type == ANTICIPE && nature == SimpleImmediateOrgan)
		instant = timeloc - theBud->timeForATest;
	else
		instant = timeloc;


	/* sets a new bud if there is enough time */
	//MODIF STOP TIME
	//if (instant > (double)(Scheduler::instance().getStopTime()) + EPSILON)
	//return 0;


	phyAgeNext= computePhyAgeBeg (lpIndex, (char)type, &rythm);
	int reitOrder = 0;
	if (phyAgeNext <= phyAgeLoc + 2)
	{
		// this is a reiteration : check for max reit order
        // patch to take the first fictive collar GU of RoCoCau (with phyage=0)
        theBranc->startPosit();
        float phyage = theBranc->positOnFirstGuWithinBranc();
        phyage = theBranc->getCurrentGu()->phyAgeGu-1;
        if (phyage < 0)
            phyage = 0;
        theBranc->endPosit();
		int maxreit = (int)theBud->axisref->val1DParameter (rtmaxreiteration, (int)phyage, this->theBranc);
		if (theBranc->reiterationOrder >= maxreit)
			return 0;
		reitOrder = theBranc->reiterationOrder + 1;
	}


	if(   (   theBranc->getPlant()->getConfigData().cfg_topoSimp == SIMP_LEAVES
		   && nature==2)
		|| theBranc->getPlant()->getConfigData().cfg_topoSimp >= SIMP_LEAVES_AND_BRANC_MANY)
	{
		critere=((SimplifTopoAMAP *)(theBranc->getPlant()->getTopology().getTopoSimplifier()))->formatCriteria(phyAgeNext,instant,rythm,theBranc->CurrentHierarcPtr()->ordre,theBranc->getPlant()->getConfigData().cfg_birthDateThresholdTopoSimp);
	}



	/* for each of them check for probality */
	for (i = 0; i<nbre; i++)
	{

		if (!bernouilli (theBranc->getPlant(),pb))
			continue;

		if (   theBranc->getPlant()->getConfigData().cfg_topoSimp < SIMP_LEAVES_AND_BRANC_MANY
			|| (   nature != 2
			    && theBranc->getPlant()->getConfigData().cfg_topoSimp == SIMP_LEAVES))
			bVert = NULL;
		else
			bVert=(BrancAMAP *)theBranc->getPlant()->getTopology().getInstanceBrancSimplified(critere);


		if(bVert)
		{
			theBranc->getPlant()->getTopology().fork(bVert, theBranc->CurrentHierarcPtr(), (char)i+1, (USint)nbre);
		}
		else
		{


			bVert= (BrancAMAP *)theBranc->getPlant()->getPlantFactory()->createInstanceBranc("AMAPSimMod");
			bVert->set(nature, lpIndex, critere, phyAgeNext, reitOrder);

			theBranc->getPlant()->getTopology().fork(bVert, theBranc->CurrentHierarcPtr(), (char)i+1, (USint)nbre);

			bVert->addBud((Bud*)theBranc->getPlant()->getPlantFactory()->createInstanceBud(5, bVert,(int)type,instant, phyAgeNext, rythm));

			theBranc->getPlant()->getTopology().updateSimplifier(bVert,critere);

			nextInstant = instant + bVert->getBud()->timeForATest;
			if (   nextInstant > Scheduler::instance()->getHotStop()
				&& nextInstant < Scheduler::instance()->getHotStop()+EPSILON)
				nextInstant = Scheduler::instance()->getHotStop();
			Scheduler::instance()->create_process(bVert->getBud(), NULL,nextInstant, Scheduler::instance()->getTopPriority()+11);

		}


		nb++;

	}
	return nb;
}

void RamifEngineAMAP::serialize(Archive& ar )
{



	if(ar.isWriting() )
	{

		for(int i=0; i<nblpa; i++)
		{
			ar<<oldstate[i];
		}


	}
	else
	{
		for(int i=0; i<nblpa; i++)
		{
			ar>>oldstate[i];
		}

	}
}
