/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

// BrancAMAP.cpp: implementation of the BrancAMAP class.
//
//////////////////////////////////////////////////////////////////////
#include <string>
#include "randomFct.h"
#include "PlantAMAP.h"
#include "BrancAMAP.h"
#include "Plant.h"
#include "Bud.h"
#include "defAMAP.h"
#include <iostream>
#include <math.h>
#include "DebugNew.h"

//using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


BrancAMAP::BrancAMAP(Plant * plt):Branc(plt)
{

	theBud= NULL;

	currentEntnNumberWithinBranch=0;

	currentEntnNumberWithinGu=0;

	currentZoneIndex=65535;

	testNumberWithinGu=0;

	currentCycleIndexWithinGu=65535;

	testIndexWithinGu=-1;

	testNumberWithinCycle=0;

	testNumberWithinBranch=0;

	reiterationOrder = 0;

	CurrentHierarcPtr(0);

	setClassType("AMAPSimMod");

}



//Constructeur par copie
BrancAMAP::BrancAMAP(BrancAMAP * b):Branc(b)
{
	lpIndex=b->lpIndex;
	reiterationOrder = b->reiterationOrder;

	theBud= NULL;

    setClassType("AMAPSimMod");

}



BrancAMAP::~BrancAMAP()
{

//	getPlant()->getTopology().removeInstanceBrancSimplified(this, critere);
	if (theBud != NULL)
	{
		delete theBud;
	}
}


void BrancAMAP::set(NatureAxe nature, int lpIndex, const std::string &c, double phy, int reit)
{
	reiterationOrder = reit;

	set (nature, lpIndex, c, phy);
}

void BrancAMAP::set(NatureAxe nature, int lpIndex, const std::string &c, double phy)
{
	setCritere(c);

	this->lpIndex = (USint)lpIndex;         /* numero de PL */

	this->nature=nature;

	this->phyAgeInit = phy;
}

void BrancAMAP::set(NatureAxe nature, int lpIndex, double phy)
{

	this->lpIndex = (USint)lpIndex;         /* numero de PL */

	this->nature=nature;

	this->phyAgeInit = phy;

}



void BrancAMAP::updateTopo (Hierarc * hierarcPtr)
{
	Branc::updateTopo(hierarcPtr);

	if(theBud != NULL)//On est sur la premiere instance de cet axe
		theBud->updateHierar(hierarcPtr);

}

void BrancAMAP::updatePruning()
{
	theBud->updatePruning();

}

void BrancAMAP::progressivePruning()
{
	startPosit();

	positOnFirstEntnWithinGu();

	// position ds la branche du 1er entrenoeud de la GU currente
	int positIntnBeginGU = getCurrentEntnIndexWithinBranch();

	positOnLastEntnWithinGu();

	// position ds la branche du dernier entrenoeud de la GU currente
	int positIntnEndGU = getCurrentEntnIndexWithinBranch();

	endPosit();

	removeSubElementAtLevel (ss.back());

	int nbBorne=this->CurrentHierarcPtr()->getNbBorne();

	for (int i=0; i<nbBorne; i++)
	{
		if(   CurrentHierarcPtr()->bornePosition[i]>=positIntnBeginGU
		   && CurrentHierarcPtr()->bornePosition[i]<=positIntnEndGU)
		{
			getPlant()->getTopology().removeBranc (CurrentHierarcPtr()->getBorne(i), 1);
			nbBorne--;
			i=0;
		}
	}
}

long BrancAMAP::calcPosElem (long senscompt, /* BASAL, APICAL */ long basecompt /*ENTN , TEST*/ ,long typetop) /* AXE, Gu, CYCLE */
{
	long position;
	int i;

	if (basecompt == TEST)
	{
		if (senscompt == BASAL)
		{
			if (typetop == TOPOGU)
				position = getTestIndexWithinGu();
			else if (typetop == TOPOUC)
				position = getTestIndexWithinCycle();
			else
				position = getTestIndexWithinBranch();
		}
		else
		{
			if (typetop == TOPOGU)
				position = getTestNumberWithinGu() - (getTestIndexWithinGu()+1);
			else if (typetop == TOPOUC)
				position = getTestNumberWithinCycle() - (getTestIndexWithinCycle()+1);
			else
				position = testNumberWithinBranch - (getTestIndexWithinBranch()+1);
		}
	}
	else
	{
		if (senscompt == BASAL)
		{
			if (typetop == TOPOGU)
				position = getCurrentEntnIndexWithinGu();
			else if (typetop == TOPOUC)
				position = getCurrentEntnIndexWithinCycle();
			else
				position = getCurrentEntnIndexWithinBranch();
		}
		else
		{
			if (typetop == TOPOGU)
				position = getCurrentEntnNumberWithinGu() - (getCurrentEntnIndexWithinGu() + 1);
			else if (typetop == TOPOUC)
			{
				position = 0;
				long entnIndexWithinCycle=getCurrentEntnIndexWithinCycle();
				long cycleEntnNumber=getCurrentEntnNumberWithinCycle();

				position += cycleEntnNumber - entnIndexWithinCycle;
			}
			else
				position = currentEntnNumberWithinBranch - getCurrentEntnIndexWithinBranch() - 1;
		}

	}

	return position;



}

void BrancAMAP::addTest()
{
	testNumberWithinGu++;

	testIndexWithinGu++;

	testNumberWithinCycle++;

	testNumberWithinBranch++;

	addSubElementAtLevel(new DecompAxeLevel(getPlant(), TESTUNIT));

}

void BrancAMAP::addIntn (InterNodeAMAP * intn)
{

	currentEntnNumberWithinGu ++;

	currentEntnNumberWithinBranch++;

	addTest();

	addSubElementAtLevel(intn);

}

void BrancAMAP::addGu (GrowthUnitAMAP * gu)
{
	currentEntnNumberWithinGu=0;

	testNumberWithinGu=0;

	testIndexWithinGu=-1;

	currentCycleIndexWithinGu=65535;

	addSubElementAtLevel(gu);
}

void BrancAMAP::addCycle()
{
	currentZoneIndex=65535;

	currentCycleIndexWithinGu++;

	testNumberWithinCycle=0;

	addSubElementAtLevel(V_plt()->getPlantFactory()->createInstanceDecompAxe(GROWTHCYCLE, "AMAPSimMod"));

}

void BrancAMAP::addZone ()
{
	currentZoneIndex++;

	addSubElementAtLevel(new DecompAxeLevel(getPlant(), ZONE));

}


void BrancAMAP::addBud (Bud * b)
{
	//Cr�ation du Bud associ� !
	theBud=b;

}



Bud * BrancAMAP::getBud()
{
	return this->theBud;
}


BrancAMAP * BrancAMAP::getBearer ()
{
	return (BrancAMAP *)CurrentHierarcPtr()->getBearer()->getBranc();
}


USint BrancAMAP::getCurrentZoneIndex ()//Take care this index is within Cycle
{
	return (USint)getCurrentElementOfSubLevel(GROWTHCYCLE)->getCurrentElementIndexOfSubLevel(ZONE);

}


int BrancAMAP::getTestIndexWithinGu ()
{
	return getCurrentElementOfSubLevel(GROWTHUNIT)->getCurrentElementIndexOfSubLevel(TESTUNIT);

}

int BrancAMAP::getTestIndexWithinCycle ()
{
	return getCurrentElementOfSubLevel(GROWTHCYCLE)->getCurrentElementIndexOfSubLevel(TESTUNIT);

}

int BrancAMAP::getTestIndexWithinBranch ()
{
	return getCurrentElementIndexOfSubLevel(TESTUNIT);

}


int BrancAMAP::getTestNumberWithinGu()
{
	return getCurrentElementOfSubLevel(GROWTHUNIT)->getElementNumberOfSubLevel(TESTUNIT);

}

int BrancAMAP::getTestNumberWithinCycle()
{
	return getCurrentElementOfSubLevel(GROWTHCYCLE)->getElementNumberOfSubLevel(TESTUNIT);

}

int BrancAMAP::getTestNumberWithinBranch()
{
	return getElementNumberOfSubLevel(TESTUNIT);
}



int BrancAMAP::getTestIndexWithinCurrentZone2 ()
{
	return getCurrentElementOfSubLevel(ZONE)->getCurrentElementIndexOfSubLevel(TESTUNIT);

}

int BrancAMAP::getTestNumberWithinCurrentZone2 ()
{
	return getCurrentElementOfSubLevel(ZONE)->getElementNumberOfSubLevel(TESTUNIT);

}


int BrancAMAP::getGuNumber ()
{
	return getElementNumberOfSubLevel(GROWTHUNIT);

}

int BrancAMAP::getCurrentGuIndex ()
{
	return getCurrentElementIndexOfSubLevel(GROWTHUNIT);
}

USint BrancAMAP::getCurrentCycleIndex ()
{
	return (USint)getCurrentElementIndexOfSubLevel(GROWTHCYCLE);
}

int BrancAMAP::getCurrentEntnIndexWithinGu()
{
	return getCurrentElementOfSubLevel(GROWTHUNIT)->getCurrentElementIndexOfSubLevel(INTERNODE);
}

int BrancAMAP::getCurrentEntnIndexWithinBranch()
{
	return getCurrentElementIndexOfSubLevel(INTERNODE);
}

int BrancAMAP::getCurrentEntnIndexWithinCycle()
{
	return getCurrentElementOfSubLevel(GROWTHCYCLE)->getCurrentElementIndexOfSubLevel(INTERNODE);
}

USint BrancAMAP::getCurrentCycleIndexWithinGu()
{
	return (USint)getCurrentElementOfSubLevel(GROWTHUNIT)->getCurrentElementIndexOfSubLevel(GROWTHCYCLE);
}

int BrancAMAP::getCurrentEntnNumberWithinGu()
{
	return getCurrentElementOfSubLevel(GROWTHUNIT)->getElementNumberOfSubLevel(INTERNODE);
}

int BrancAMAP::getCurrentEntnNumberWithinCycle()
{
	return getCurrentElementOfSubLevel(GROWTHCYCLE)->getElementNumberOfSubLevel(INTERNODE);
}

int BrancAMAP::getCurrentEntnNumberWithinBranch()
{
	return getElementNumberOfSubLevel(INTERNODE);
}


GrowthUnitAMAP *  BrancAMAP::getCurrentGu ()
{
	return (GrowthUnitAMAP *)getCurrentElementOfSubLevel(GROWTHUNIT);
}

InterNodeAMAP * BrancAMAP::getCurrentEntn ()
{
	return (InterNodeAMAP *)getCurrentElementOfSubLevel(INTERNODE);
}

//Position method

USint BrancAMAP::positOnFirstEntnWithinBranc()
{
	return positOnFirstElementOfSubLevel(INTERNODE);
}

USint BrancAMAP::positOnLastEntnWithinBranc()
{
	return positOnLastElementOfSubLevel(INTERNODE);
}


USint BrancAMAP::positOnFirstCycleWithinBranc()
{
	return positOnFirstElementOfSubLevel(GROWTHCYCLE);
}

USint BrancAMAP::positOnLastCycleWithinBranc()
{
	return positOnLastElementOfSubLevel(GROWTHCYCLE);
}

USint BrancAMAP::positOnNextEntnWithinBranc()
{
	return positOnNextElementOfSubLevel(INTERNODE);
}

USint BrancAMAP::positOnPreviousEntnWithinBranc()
{
	return positOnPreviousElementOfSubLevel(INTERNODE);
}

USint BrancAMAP::positOnFirstEntnWithinGu()
{
	return getCurrentElementOfSubLevel(GROWTHUNIT)->positOnFirstElementOfSubLevel(INTERNODE);
}

USint BrancAMAP::positOnLastEntnWithinGu()
{
	return getCurrentElementOfSubLevel(GROWTHUNIT)->positOnLastElementOfSubLevel(INTERNODE);
}

USint BrancAMAP::positOnFirstEntnWithinCycle()
{
	return getCurrentElementOfSubLevel(GROWTHCYCLE)->positOnFirstElementOfSubLevel(INTERNODE);
}

USint BrancAMAP::positOnLastEntnWithinCycle()
{
	return getCurrentElementOfSubLevel(GROWTHCYCLE)->positOnLastElementOfSubLevel(INTERNODE);
}

USint BrancAMAP::positOnNextEntnWithinGu()
{
	return getCurrentElementOfSubLevel(GROWTHUNIT)->positOnNextElementOfSubLevel(INTERNODE);
}

USint BrancAMAP::positOnPreviousEntnWithinGu()
{
	return getCurrentElementOfSubLevel(GROWTHUNIT)->positOnPreviousElementOfSubLevel(INTERNODE);
}


USint BrancAMAP::positOnNextEntnWithinCycle()
{
	return getCurrentElementOfSubLevel(GROWTHCYCLE)->positOnNextElementOfSubLevel(INTERNODE);
}

USint BrancAMAP::positOnPreviousEntnWithinCycle()
{
	return getCurrentElementOfSubLevel(GROWTHCYCLE)->positOnPreviousElementOfSubLevel(INTERNODE);
}


USint BrancAMAP::positOnFirstGuWithinBranc()
{
	return positOnFirstElementOfSubLevel(GROWTHUNIT);
}

USint BrancAMAP::positOnLastGuWithinBranc()
{
	return positOnLastElementOfSubLevel(GROWTHUNIT);
}

USint BrancAMAP::positOnNextGuWithinBranc()
{
	return positOnNextElementOfSubLevel(GROWTHUNIT);
}

USint BrancAMAP::positOnPreviousGuWithinBranc()
{
	return positOnPreviousElementOfSubLevel(GROWTHUNIT);
}

USint BrancAMAP::positOnEntnWithinBrancAt(USint pos)
{
	return positOnElementAtSubLevel(pos, INTERNODE);
}

USint BrancAMAP::positOnGuWithinBrancAt(USint pos)
{
	return positOnElementAtSubLevel(pos, GROWTHUNIT);
}


USint BrancAMAP::positOnEntnWithinGuAt(USint pos)
{
	return getCurrentElementOfSubLevel(GROWTHUNIT)->positOnElementAtSubLevel(pos, INTERNODE);
}


float BrancAMAP::computeLength (float scale, int branchTotalTestNumbertheo)
{
	float d, indexFromEnd, aux;
	long  currentPosForLng;
	int   phyAge;
	AxisReference *axisref = getBud()->axisref;

	/* position along the branch from theoritical end */
	indexFromEnd = (float)(branchTotalTestNumbertheo - getTestIndexWithinBranch()- 1.);

	d = scale;

	phyAge = (int)(getCurrentGu()->phyAgeGu - 1.);
	if (phyAge == -1)
        return 0;

	/* computes toplogical position */
	currentPosForLng = calcPosElem (
			(long)axisref->val1DParameter (rgsensgeom, phyAge, this),
			(long)axisref->val1DParameter (rtbasecompt, phyAge, this),
			(long)axisref->val1DParameter (rttypetop, phyAge, this));


	/* length adjustment according to nodes age */
	/* max length is reached */
	if (indexFromEnd >= axisref->val1DParameter (renbtestlng, phyAge, this) + axisref->val1DParameter (relaglng, phyAge, this))
	{
		d *= axisref->val1DParameter (reevollng, phyAge, this);
	}
	/* node is older than the lag period, it is growing */
	else if (indexFromEnd >= axisref->val1DParameter (relaglng, phyAge, this))
	{
		aux = (float)(indexFromEnd - axisref->val1DParameter (relaglng, phyAge, this)) / axisref->val1DParameter (renbtestlng, phyAge, this);
		d *= (float)(1. + aux * (axisref->val1DParameter (reevollng, phyAge, this)-1.));
	}

	/* length adjustment according to topological position */
	d *= axisref->val2DParameter (rgpcntlng, phyAge, currentPosForLng, this);


	return d;
}

float BrancAMAP::computeDiam (float initialDiameter, int branchTotalTestNumbertheo)
{
	float varinit, evolgross, nbtestgross, laggross, diam, aux, indexFromEnd;
	int   phyAge, currentPosForLng;
	AxisReference *axisref = getBud()->axisref;


	/* position along the branch from theoritical end */
	indexFromEnd = (float)(branchTotalTestNumbertheo - getTestIndexWithinBranch()- 1.);

	phyAge = (int)(getCurrentGu()->phyAgeGu - 1.);
	if (phyAge == -1)
        phyAge = 0;

	/* computes toplogical position */
	currentPosForLng = calcPosElem ((long)axisref->val1DParameter (rgsensgeom, phyAge, this),
			(long)axisref->val1DParameter (rtbasecompt, phyAge, this),
			(long)axisref->val1DParameter (rttypetop, phyAge, this));

	/* initial diameter */
	varinit = initialDiameter;

	/* diameter adjustment according to topological position */
	diam = varinit * axisref->val2DParameter (rgpcntgross, phyAge, currentPosForLng, this);

	/* diameter adjustment according to nodes age and  FIRST GU PHY AGE
	 * ONLY IF evolgross is GREATER THAN ZERO
	 * */
	double phyAgeCur = ((GrowthUnitAMAP *)ss[0])->phyAgeGu - 1;
	// patch to take first GU of trunk into RoCoCau (phyage = 0)
	if (phyAgeCur < 0)
        phyAgeCur = 0;
	evolgross = (float)axisref->val1DParameter (reevolgross, (int)phyAge, this);
	if (evolgross >= 0)
	{
		evolgross = fabs (axisref->val1DParameter (reevolgross, (int)phyAgeCur, this));
		nbtestgross = (float)axisref->val1DParameter (renbtestgross, (int)phyAgeCur, this);
		laggross = (float)axisref->val1DParameter (relaggross, (int)phyAgeCur, this);
	}
	/* diameter adjustment according to nodes age and  CURRENT GU PHY AGE
	 * ONLY IF evolgross is LESS THAN ZERO
	 * */
	else
	{
		evolgross = fabs (axisref->val1DParameter (reevolgross, phyAge, this));
		nbtestgross = axisref->val1DParameter (renbtestgross, phyAge, this);
		laggross = axisref->val1DParameter (relaggross, phyAge, this);
	}

	/* diameter adjustment according to nodes age */
	if (indexFromEnd >= nbtestgross + laggross)
		diam *= evolgross;
	else if (indexFromEnd >= laggross)
	{
		aux = (float)(indexFromEnd - laggross) / nbtestgross;
		diam *= (float)1. + aux * (evolgross-(float)1.);
	}

	return diam;
}

/* compute initial parameter value for length and diameter */
void BrancAMAP::computeInitialLengthDiameter (float *initialLength, float *initialDiameter)
{
	AxisReference *axisref = getBud()->axisref;
	*initialLength = axisref->val1DParameter (lnginit, phyAgeInit-1, this);
	*initialDiameter = axisref->val1DParameter (diaminit, phyAgeInit-1, this);
}

std::string BrancAMAP::getName()
{
	return std::string (((AxisReference*)getPlant()->getParamData(string("AMAPSimMod")))->getLpName(lpIndex));
}


void BrancAMAP::serialize(Archive& ar )
{
	int nat;

	Branc::serialize(ar);


	if(ar.isWriting() )
	{
		switch(nature)
		{
			case MainBranch: nat=0; break;
			case SecondaryBranch: nat=1; break;
			case SimpleImmediateOrgan: nat=2; break;
			case Root_e: nat=3; break;
			default: nat=-1;break;
		}
		ar<<reiterationOrder<<nat<<currentEntnNumberWithinBranch<<currentEntnNumberWithinGu<<currentZoneIndex<<testNumberWithinGu;
		ar<<currentCycleIndexWithinGu<<testIndexWithinGu<<testNumberWithinCycle<<testNumberWithinBranch<<lpIndex;

		//Save the eventData mechanism link to this branc
		if(this->SavePtr(ar,theBud))
		{
			ar<<*theBud;
		}


	}
	else
	{
		ar>>nat;
		switch(nat)
		{
			case 0: nature=MainBranch; break;
			case 1:nature=SecondaryBranch; break;
			case 2:nature=SimpleImmediateOrgan; break;
			case 3:nature=Root_e; break;
		}
		ar>>reiterationOrder>>currentEntnNumberWithinBranch>>currentEntnNumberWithinGu>>currentZoneIndex>>testNumberWithinGu;
		ar>>currentCycleIndexWithinGu>>testIndexWithinGu>>testNumberWithinCycle>>testNumberWithinBranch>>lpIndex;

		ArchivableObject * arBud;
		if(this->LoadPtr(ar,arBud))
		{
			theBud=new Bud();

			this->AddPtr(ar,theBud);

			ar>>*theBud;
		}
		//else pas de else car c ici kon doit les charger ( et pas au nivo du sequenceur en chargant les process


		//TODO SERIAL pointeur sur topoMan;


	}

}





