/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "PlantAMAP.h"
#include "GrowthEngineAMAP.h"
#include "CycleAMAP.h"
#include "Bud.h"
#include <math.h>
#include "randomFct.h"
#include "DebugNew.h"

GrowthEngineAMAP::GrowthEngineAMAP(Bud * b)
{
	theBud=b;

	theBranc=b->brancAMAP;
}

GrowthEngineAMAP::GrowthEngineAMAP(Bud * b, double phyAgeBeg, double ryt)
{
	BrancAMAP * bearBrc = b->brancAMAP->getBearer();

	theBud=b;

	theBranc=b->brancAMAP;

	BranchBegining =1;

	growthEnd = 0;

	prolepticSleepingTime = 0;

	prolepticDormancyTestNumber = 0; /* number of test for proleptic dormancy */

	currentGuTestNumber=0;

	polycyc = 0xff;/* polycyclism flag */

	nbcyc=testZone2Number=0;


	if (   theBud->branchRamificationType == ANTICIPE
		|| theBud->branchRamificationType == RETARDECYC)
	{
		this->immediateBranchingOrder =bearBrc->getBud()->getGrowth()->immediateBranchingOrder + 1;   /* immediate current branching order */
	}
	else
		this->immediateBranchingOrder = 0;

	/* Is it the first bud -> the trunc *//* branch == bearer -> i.e. trunc */

	/* if first branch */
	if (bearBrc == theBranc)
	{

		this->immediateBranchingOrder = 0;

		this->rythme = theBud->axisref->valSingleParameter (glvitesse, this->theBranc);

		phyAgeBeg = this->phyAge =(int)(*(theBud->axisref))[glPhyAgeune].ymin;    /* current phy age */

		this->maxPhyAge = (long)theBud->axisref->val1DParameter (rtmaxetap, this->phyAge-1, theBranc);

		if (this->maxPhyAge > (int)((*(theBud->axisref))[glmaxref].ymin))
			this->maxPhyAge = (int)(*(theBud->axisref))[glmaxref].ymin;

		this->phyAgeRef = this->nextPhyAgeRef = this->phyAge-1;

	}
	else /* lateral branch */
	{
		this->rythme = (float)ryt;

		this->phyAge = phyAgeBeg;
		this->phyAgeRef = this->nextPhyAgeRef = phyAgeBeg -1;

		calcul_parametres_axillaire (bearBrc);

	}

	this->maxGuNumber = (int)theBud->axisref->val1DParameter (rtagelim, this->phyAgeRef, theBranc); /* max number of Gu for the branch */

	this->noDifferentiationGu= binodec(theBud->getBranc()->getPlant(),
		                              theBud->axisref->val1DParameter(nbremain, (int)this->phyAgeRef, theBranc),
									  theBud->axisref->val1DParameter(pbremain,(int)this->phyAgeRef, theBranc),
									  (int)theBud->axisref->val1DParameter(shiftremain, (int)this->phyAgeRef, theBranc));
	if (this->noDifferentiationGu == -1)
		this->noDifferentiationGu = 999999;
	if (this->noDifferentiationGu == 0)
		this->noDifferentiationGu = 1;
//	this->noDifferentiationGu += 1;

	this->nextPhyAgeRef = this->phyAgeRef;

	this->differentiation();

	this->noDifferentiationGu--;

	getTimeGu (phyAgeBeg-1);

}

GrowthEngineAMAP::~GrowthEngineAMAP(void)
{
}



/* computes the initial state of a bud according to its branching parameters (position, ramaification type */
void GrowthEngineAMAP::calcul_parametres_axillaire(BrancAMAP * p)
{
	long     poselem;
	int	    phyAgeLoc;
	long     sautrami;
	int absoluteJump; //saut negatif
	USint zoneIndex;



	if(p == NULL)
		return;


	/* computes bearer situation according to the bud position
	 * to which we are looking to */

	//MAJ INFO GU
	//p->startPosit();

	// p->positOnEntnWithinBrancAt(theBranc->currentHierarcPtr->indexOnBearerAtLowestLevel);

	zoneIndex=p->getCurrentZoneIndex();

	/* current phyAge of the bearer */
	phyAgeLoc = (int)((GrowthUnitAMAP *)p->Ss()[p->getCurrentGuIndex()])->phyAgeGu - 1;

	/* case of an immediate branch borne a the end of a Gu
	 * i.e the first gu will behave like a delayed one */
	int index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*theBranc->lpIndex+NB_GLOBAL_LP_VAR+zoneIndex*NB_ZONE_VAR;
	if (   theBud->branchRamificationType == ANTICIPE
		|| theBud->branchRamificationType == RETARDECYC)
	{
		if (   theBud->guRamificationType == RETARDE1
			&& theBud->axisref->val2DParameter (index+rcsautramiant, phyAgeLoc, poselem, theBranc) == 0)
		{
			this->phyAgeRef=p->getBud()->getGrowth()->nextPhyAgeRef;
			this->phyAge= (int)this->phyAgeRef+1;
		}
	}
	/* maximum phy age */
	this->maxPhyAge = (long)theBud->axisref->val1DParameter (rtmaxetap, this->phyAge-1, theBranc);
	if (this->maxPhyAge > (int)((*(theBud->axisref))[glmaxref].ymin))
		this->maxPhyAge = (int)(*(theBud->axisref))[glmaxref].ymin;

	//p->endPosit();


}

/* main loop for growth of a bud */
int GrowthEngineAMAP::growth ()
{

	Bud *p;

	long phyAgeLoc, cycleEnd=0;

	short budDeath;

	p = theBranc->getBearer()->getBud();

	/* if first time, test proleptic sleeping */
	if (BranchBegining == 1)
	{
		this->limitGuTime();
		if(!initProlepticSleep())
			return 0;
	}

	/* at the end of the current Gu begin a new Gu */
	if (   theBud->endUnit & ENDGU
		|| theBranc->getTestNumberWithinGu() == this->currentGuTestNumber)
	{
		createGu (this->phyAgeRef+1);
	}



//	this->phyAgeRef = ((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->phyAgeGu - 1;
	phyAgeLoc = (int)this->phyAgeRef;

	/* first node of the Gu */
	if (theBud->endUnit & ENDGU)
	{
		if(!startGu())
			return 0;
	}


	/* if the current Gu is an immediate one, limit the growing time to the bearer end Gu time */
	if (   theBranc->nature != SimpleImmediateOrgan
		&& theBranc->CurrentIndex() == 0
		&& theBud->guRamificationType == ANTICIPE)
	{

		/* if bearer is at end of current Gu then borne will be at end also */
		if (   p->endUnit & ENDGU
			&& p->endInstant < Scheduler::instance()->getTopClock()-EPSILON)
		{
			theBud->nextTime = p->endInstant;
			theBud->previousInstant = theBud->endInstant;
			theBud->endUnit |= ENDGU;


			if (   theBud->nextTime > Scheduler::instance()->getHotStop()
				&& theBud->nextTime < Scheduler::instance()->getHotStop()+EPSILON)
				theBud->nextTime = Scheduler::instance()->getHotStop();

			//MODIF STOP TIME
			//if (theBud->nextTime <= (double)(Scheduler::instance().getStopTime()) + EPSILON)
			//{
			Scheduler::instance()->self_signal_event(NULL,(double)theBud->nextTime,Scheduler::instance()->getTopPriority());
			//}
			return 1;
		}
	}


	if(!testProlepticSleep())
		return 0;


	/* sets a new internode */


	CycleAMAP *c = setupNewCycle(); //ajout cycle ,entrenoeud , test

	budDeath=testBudDeath(c);

	/* if current Gu is ended */
	if (   budDeath
		|| testEndGu(cycleEnd)
		|| theBranc->getTestNumberWithinGu() == this->currentGuTestNumber)
	{
		endGu();
	}
	else /* current Gu has to grow further */
	{
		/* sets next eventData */
		//MODIF STOP TIME
		//if (   theBud->nextTime <= (double)(Scheduler::instance().getStopTime()) + EPSILON
		//   && !theBud->death)
		if(!theBud->death)
		{

			if (   theBud->nextTime > Scheduler::instance()->getHotStop()
				&& theBud->nextTime < Scheduler::instance()->getHotStop()+EPSILON)
				theBud->nextTime = Scheduler::instance()->getHotStop();

			Scheduler::instance()->self_signal_event( NULL,(double)theBud->nextTime,Scheduler::instance()->getTopPriority());
		}
	}

	if (theBud->death)
		return 0;
	else if (!cycleEnd)
		return 2;
	else
		return 1;

}



int GrowthEngineAMAP::initProlepticSleep ()
{
	float prolgup, prolgud;      /* prolepsy gu*/
	float prolepticDormancyTestNumber, prolp, prold;        /* prolepsy entn*/
	long  prolgu, phyAgeLoc;
	int i;

	int testedGuIndex;
	USint zoneIndex;

	testedGuIndex=0;

	zoneIndex=0;



//	phyAgeLoc = (int)((GrowthUnitAMAP *)theBranc->Ss()[0])->phyAgeGu - 1;
	phyAgeLoc = (int)this->phyAgeRef;

	/* bud has reached max phyAge in reference axis */
	if (   phyAgeLoc >= ((*(theBud->axisref))[glmaxref].ymin)
			|| phyAgeLoc >= this->maxPhyAge)
	{


		theBud->endUnit = FINISHED;
		//Modif SG A gerer
		return 0;
	}

	/* for immediate branch check the max depth for branching order */
	if ( immediateBranchingOrder > theBud->axisref->val1DParameter (rtmaxanticipe, phyAgeLoc, theBranc) + 1
			&& theBranc->nature != SimpleImmediateOrgan)
		return 0;


	this->BranchBegining = 0;

	prolgup = theBud->axisref->val1DParameter (rtprolgup, phyAgeLoc, theBranc);
	prolgud = theBud->axisref->val1DParameter (rtprolgud, phyAgeLoc, theBranc);
	prolepticDormancyTestNumber = theBud->axisref->val1DParameter (rtproln, phyAgeLoc, theBranc);
	prolp = theBud->axisref->val1DParameter (rtprolp, phyAgeLoc, theBranc);
	prold = theBud->axisref->val1DParameter (rtprold, phyAgeLoc, theBranc);

	/* Gu sleeping */
	for (prolgu=(int)prolgud; prolgu<this->maxGuNumber; prolgu++)
	{
		if (!bernouilli (theBud->getBranc()->getPlant(),prolgup))
			break;
	}
	/* node sleeping */
	this->prolepticDormancyTestNumber = 0;
	if (prolepticDormancyTestNumber > 0) /* binomial law */
	{
		this->prolepticDormancyTestNumber = (int)prold;
		for (i=0; i<prolepticDormancyTestNumber; i++)
		{
			if (bernouilli (theBud->getBranc()->getPlant(),prolp))
				this->prolepticDormancyTestNumber++;
		}
	}
	else if (   prolepticDormancyTestNumber == 0
			&& prolp != 0.0) /* reverse geometric */
	{
		if (prolp == 1.)
		{
			theBud->death = 1;
			theBud->deathInstant = Scheduler::instance()->getTopClock();
		}
		else
		{
			this->prolepticDormancyTestNumber = (int)prold;
			while (bernouilli (theBud->getBranc()->getPlant(),prolp))
				this->prolepticDormancyTestNumber ++;
		}
	}
	else if (prolepticDormancyTestNumber < 0) /* negative binomial law */
	{
		if ((this->prolepticDormancyTestNumber = (int)binodec (theBranc->getPlant(),prolepticDormancyTestNumber, prolp, (long)prold)) == 0xffff)
		{
			theBud->death = 1;
			theBud->deathInstant = Scheduler::instance()->getTopClock();
		}
		this->prolepticDormancyTestNumber = this->prolepticDormancyTestNumber + (int)prold;
	}
	else
	{
		this->prolepticDormancyTestNumber = (int)prold;
	}

	if (   !theBud->death
			&& prolgu) /* there are still some cycle to sleep */
	{
		theBud->nextTime=Scheduler::instance()->getTopClock()+ prolgu;/* compute next eventData time */

		theBud->endInstant += prolgu;
		if (theBud->axisref->val1DParameter(rtvitessepr, phyAgeLoc, theBranc) == 1) /* aging during sleep -> compute the new phyAge */
		{
			for (i=0; i<prolgu; i++)
			{
				differentiation();
				this->phyAge= (int)this->phyAgeRef+1;
				if (this->theBranc->getCurrentGu())
					this->theBranc->getCurrentGu()->set(this->phyAge, this->theBranc->getCurrentGu()->timeForATest);
			}
		}
		if (   (   theBud->guRamificationType == RETARDECYC
					|| theBud->guRamificationType == ANTICIPE)
				&& testedGuIndex == 0)
			this->prolepticSleepingTime = prolgu;


		if (   theBud->nextTime > Scheduler::instance()->getHotStop()
			 && theBud->nextTime < Scheduler::instance()->getHotStop()+EPSILON)
			theBud->nextTime = Scheduler::instance()->getHotStop();

		//MODIF STOP TIME
		//if ( theBud->nextTime <= (double)((double)(Scheduler::instance().getStopTime())) + EPSILON)
		//{
		Scheduler::instance()->self_signal_event( NULL,(double)theBud->nextTime,Scheduler::instance()->getTopPriority());

		//}
		return 0;
	}


	if(this->prolepticDormancyTestNumber && theBud->axisref->val1DParameter(rtvitessepr, phyAgeLoc, theBranc)==0)
	{
		theBud->nextTime += this->prolepticDormancyTestNumber * theBud->timeForATest;

		if (   theBud->nextTime > Scheduler::instance()->getHotStop()
			 && theBud->nextTime < Scheduler::instance()->getHotStop()+EPSILON)
			theBud->nextTime = Scheduler::instance()->getHotStop();

		Scheduler::instance()->self_signal_event( NULL,(double)theBud->nextTime,Scheduler::instance()->getTopPriority());
		while (theBud->nextTime > theBud->endInstant)
			theBud->endInstant += getTimeGu (phyAge-1);

		this->BranchBegining=0;
		this->prolepticDormancyTestNumber=0;
		return 0;
	}


	return 1;
}



//Initialise le nouveaux cycle et l'entrenoeud courrant
CycleAMAP *GrowthEngineAMAP::setupNewCycle()
{


	/* in case of end of cycle create a new cycle */
	if (theBud->endUnit & ENDCYCLE)
	{
		theBud->endUnit = 0;
		theBranc->addCycle();
		theBranc->addZone();
	}

	float psympodeUC;
	char  z2;
	InterNodeAMAP * intn;
	USint cycIndex=theBranc->getCurrentCycleIndexWithinGu();
	long phyAgeLoc = (int)this->phyAgeRef;
	long actifIntn=0;

	entnRealized=false;

	/* determine the current growing law according to cycle position */
	int testNumberWithinCycle=theBranc->getTestNumberWithinCycle();

	intn=(InterNodeAMAP *)theBranc->getPlant()->getPlantFactory()->createInstanceDecompAxe(INTERNODE, "AMAPSimMod");

	CycleAMAP *cycle = (CycleAMAP*)theBranc->getCurrentElementOfSubLevel(GROWTHCYCLE);
	int entnNumberWithinCycle = cycle->getElementNumberOfSubLevel(INTERNODE);

	/* create a new cycle */
	if (testNumberWithinCycle == 0)
	{
		cycle->init (theBranc->getCurrentCycleIndexWithinGu(), theBud->axisref, phyAgeLoc, this->theBranc);

		/* test sympodial death and store it in flag */
		psympodeUC = theBud->axisref->val1DParameter (rtpbsympodeUC, phyAgeLoc, this->theBranc);
		if (psympodeUC < 0)
		{
			psympodeUC = -psympodeUC;
			intn->sympodPlan = 1;
		}
		intn->sympod=(char)bernouilli(theBranc->getPlant(),psympodeUC);
	}

	intn->instant = Scheduler::instance()->getTopClock();


	/* test growth according to node kind (z1, z'1, z2) */
	z2 = 0;
	if (   cycle->PreformEntnNumber == 0
		&& !cycle->okZ2)
		cycle->end = true;

	if (entnNumberWithinCycle < cycle->PreformEntnNumber)
	{
		actifIntn = 1;
		if (   !cycle->okZ2
			&& entnNumberWithinCycle == cycle->PreformEntnNumber-1)
				cycle->end = true;
	}
	else if (cycle->okZ2)
	{
		int nbTestZ2;
		if (cycle->okZ2 == 1)
			nbTestZ2 = 1;
		else
			nbTestZ2 = cycle->getCurrentElementOfSubLevel(ZONE)->getElementNumberOfSubLevel(TESTUNIT)+1;

		if (cycle->okZ2 == 1)
		{
			int nb = binodec (theBranc->getPlant(), cycle->NeoformTestNumberN, cycle->NeoformTestNumberP, 0);
			if (binodec (theBranc->getPlant(), nb, cycle->NeoformP, cycle->NeoformTestNumberD) == 0)
			{
				cycle->end = true;
				actifIntn = -1;
			}
			else
			{
				theBranc->addZone();
				cycle->okZ2 = 2;
			}
		}
		if (!cycle->end)
		{
			z2 = 1;

			if (nbTestZ2 <= cycle->NeoformTestNumberD)
				actifIntn = 1;
			else
				actifIntn= bernouilli (theBranc->getPlant(),cycle->NeoformP);
			int nb = binodec (theBranc->getPlant(), cycle->NeoformTestNumberN, cycle->NeoformTestNumberP, 0);
			if (nbTestZ2 >= binodec (theBranc->getPlant(), nb, cycle->NeoformP, cycle->NeoformTestNumberD))
				cycle->end = true;
		}
	}



	if(actifIntn == 1)
	{
		cycle->active = true;
		theBranc->addIntn(intn);
		entnRealized=true;
	}
	else
	{
		cycle->active = false;
		delete intn;
		if (actifIntn == 0)
			theBranc->addTest();
	}


	return cycle;
}



int GrowthEngineAMAP::testProlepticSleep ()
{

	if (this->prolepticDormancyTestNumber)
	{
		/* the branch is still sleeping at its begining according to test law */
		/* set the fisrt growing eventData time */


		int i;
		Bud * p ;

		p = theBranc->getBearer()->getBud();

		//Modif SG Pas s�r
		long phyAgeLoc = (int)this->phyAgeRef ;

		//MODIF STOP TIME
		//if (Scheduler::instance().getTopClock() <= (double)(Scheduler::instance().getStopTime()) + EPSILON)
		//{
		this->BranchBegining = 0;
		/* due to random selection onevents to be computed
		 * one should compute first the bearer in case of end of Gu */
		//if (   (p->endUnit & ENDGU)
		//		&& (p->previousInstant < Scheduler::instance().getTopClock()-EPSILON))
		//{
		//	theBud->nextTime += EPSILON/2.;
		//	this->prolepticDormancyTestNumber ++;
		//}
		//else
		{
			//if (p->endUnit & ENDGU)
			//	theBud->endInstant = p->previousInstant;
			//else
			//	theBud->endInstant = p->endInstant;

			theBud->nextTime += theBud->timeForATest;
			/* compute current phy age and current time for initial growing eventData */
			if (theBud->nextTime > theBud->endInstant + EPSILON)
			{
				theBud->nextTime -= theBud->timeForATest;

				i = bernouilli (theBranc->getPlant(),theBud->axisref->val1DParameter (rtprogress, phyAgeLoc, this->theBranc));
				if (i)
				{
					differentiation();
					this->phyAge= (int)this->phyAgeRef+1;
					this->theBranc->getCurrentGu()->set(this->phyAge, this->theBranc->getCurrentGu()->timeForATest);
				}

				phyAgeLoc = (int)this->phyAgeRef;
				getTime (phyAgeLoc, Scheduler::instance()->getTopClock());

				theBud->nextTime += theBud->timeForATest;

			}
		}

		if (   theBud->nextTime > Scheduler::instance()->getHotStop()
			 && theBud->nextTime < Scheduler::instance()->getHotStop()+EPSILON)
			theBud->nextTime = Scheduler::instance()->getHotStop();

		/* sets the first growing eventData */
		//MODIF STOP TIME
		//if (theBud->nextTime <= (double)(Scheduler::instance().getStopTime()) + EPSILON)
		//{
		Scheduler::instance()->self_signal_event( NULL,(double)theBud->nextTime,Scheduler::instance()->getTopPriority());
		//}

		//}

		this->prolepticDormancyTestNumber--;
		return 0;
	}
	else return 1;


}




void GrowthEngineAMAP::limitGuTime()
{

	double duree;

	Bud * p;

	duree = getTimeGu (phyAge-1);

	theBud->endInstant = theBud->nextTime - theBud->timeForATest + duree;

	p=theBranc->getBearer()->getBud();

	if (p==theBud)
		return;

	/* in case of the first Gu of an immediate branch, one has to put a limit
	 * on the time to grow the first Gu */
	if (   theBud->guRamificationType == RETARDECYC
		|| theBud->guRamificationType == ANTICIPE)
	{
		double bearerInstant;

		if (p->endUnit & ENDGU)
			bearerInstant = p->previousInstant;
		else
			bearerInstant = p->endInstant;

		if (theBud->endInstant > bearerInstant + prolepticSleepingTime)
			theBud->endInstant = bearerInstant + prolepticSleepingTime;

		if (   theBranc->nature != SimpleImmediateOrgan
//			&& theBud->nextTime >= bearerInstant-EPSILON)
			&& theBud->nextTime > bearerInstant+EPSILON)
		{
			theBud->guRamificationType = RETARDE1;

			p->getBranc()->startPosit();

			p->getBranc()->positOnEntnWithinBrancAt(p->getBranc()->CurrentHierarcPtr()->indexOnBearerAtLowestLevel);

			//if ( ((GrowthUnitAMAP *)theBranc->Ss()[0])->phyAgeGu ==  p->getBranc()->getCurrentGu()->phyAgeGu)
			//{
			//	((GrowthUnitAMAP *)theBranc->Ss()[0])->phyAgeGu = ((GrowthUnitAMAP *)theBranc->Ss()[0])->phyAgeGu+1;
			//	maxPhyAge = (long)theBud->axisref->val1DParameter (rtmaxetap,  (int)((GrowthUnitAMAP *)theBranc->Ss()[0])->phyAgeGu-1, this->theBranc);
			//	if (maxPhyAge > (int)(*(theBud->axisref))[glmaxref].ymin)
			//		maxPhyAge = (int)(*(theBud->axisref))[glmaxref].ymin;
			//}

			p->getBranc()->endPosit();

			theBud->endInstant += duree;
		}
	}
}



/* computes test time length and Gu end time */
void GrowthEngineAMAP::getTime ( long phyAge, /* current phy age */
		double instant) /* current time */
{
	int   typefonct;
	double duree;

	duree = getTimeGu (phyAge);

	/* time consuming for a test */
	((GrowthUnitAMAP *)theBranc->Ss()[theBranc->CurrentIndex()])->timeForATest = theBud->timeForATest;

	/* time for end of Gu */
	theBud->endInstant = instant + duree;

	/* if current branch is synchronized whith bearer
	 * adjust to integer */
	/*	Obsolete since holly year 2008
	typefonct = (int)theBud->axisref->val1DParameter (rttypefonct, phyAge, this->theBranc);

	if (   typefonct == SYNCHRONE
			&& this->currentGuTestNumber > 1)
	{
		if (theBud->branchRamificationType == ANTICIPE)
			theBud->endInstant = floor(theBud->endInstant) + theBud->timeForATest;
		else if (theBud->branchRamificationType == RETARDECYC)
			theBud->endInstant = floor(theBud->endInstant) + theBud->timeForATest;
	}
	*/
}


/* computes time length for a ut and a test according rhythm */
double GrowthEngineAMAP::getTimeGu (long phyAge)
{
	double duree;
	int   typefonct;


	calcPolycyc (polycyc, phyAge);
	this->currentGuTestNumber = (int)theBud->axisref->val1DParameter (rtnben, phyAge, this->theBranc);
	//switch (this->nbcyc)
	//{
	//	case 5:
	//		this->currentGuTestNumber += (int)theBud->axisref->val1DParameter (rtnben5, phyAge, this->theBranc);
	//	case 4:
	//		this->currentGuTestNumber += (int)theBud->axisref->val1DParameter (rtnben4, phyAge, this->theBranc);
	//	case 3:
	//		this->currentGuTestNumber += (int)theBud->axisref->val1DParameter (rtnben3, phyAge, this->theBranc);
	//	case 2:
	//		this->currentGuTestNumber += (int)theBud->axisref->val1DParameter (rtnben2, phyAge, this->theBranc);
	//		break;
	//}
	/* if synchronized functioning time is fixed to 1 */
//	typefonct = (int)theBud->axisref->val1DParameter (rttypefonct, phyAge, this->theBranc);
//	if (typefonct == SYNCHRONE)
//		duree = 1.;
/*
	else if (this->theBranc == this->theBranc->getBearer())
		duree = 1. / theBud->axisref->valSingleParameter (glvitesse, this->theBranc) / theBud->axisref->val1DParameter (NB_VAR+theBranc->lpIndex*NB_VAR_ENT+rerythme,phyAge, this->theBranc);
*/
//	else
	{
//		duree = 1. / rythme / theBud->axisref->val1DParameter (NB_VAR+(theBud->axisref->getNbLPAxe()*theBranc->getCurrentZoneIndex()+theBranc->lpIndex)*NB_VAR_ENT+rerythme,phyAge, this->theBranc);
//		duree = 1. / rythme / theBud->axisref->val1DParameter (NB_VAR+theBranc->lpIndex*NB_VAR_ENT+rerythme,phyAge, this->theBranc);
		duree = 1. / rythme / theBud->axisref->val1DParameter (rtrythme,phyAge, this->theBranc);
	}

	theBud->timeForATest = duree / this->currentGuTestNumber;

	return duree;
}

/* tests polycyclism capability returns 0 if no polycyclism 1 if polycyclism */
char GrowthEngineAMAP::calcPolycyc (unsigned char oldstate, /* state of previous Gu */
		int phyAge /* current phy age */
		)
{
	long polycyc;
	float prob[5];

	polycyc = markovSimulation2 (theBranc->getPlant(),(long)oldstate,
			theBud->axisref->val1DParameter (rtpolyini, phyAge, this->theBranc),
			theBud->axisref->val1DParameter (rtmonocyc, phyAge, this->theBranc),
			theBud->axisref->val1DParameter (rtpolycyc, phyAge, this->theBranc),
			RANDMARKOVP);

	if (polycyc == 0)
		this->nbcyc = 1;
	else
	{
		prob[0] = theBud->axisref->val1DParameter (rt2cyc, phyAge, this->theBranc);
		prob[1] = theBud->axisref->val1DParameter (rt3cyc, phyAge, this->theBranc);
		prob[2] = theBud->axisref->val1DParameter (rt4cyc, phyAge, this->theBranc);
		prob[3] = (float)1. - prob[0] - prob[1] - prob[2];
		this->nbcyc = cumulativeLaw (theBranc->getPlant(),prob, 4,  RANDMARKOVP) + 2;
	}

	return ((char)polycyc);
}


void GrowthEngineAMAP::createGu (double age_loc)
{
	Plant * plt=theBud->getBranc()->getPlant();
	theBud->endUnit = ENDGU;

	okZone2 = 0; /* z2 flag whithin current cycle */

	growthEnd = 0;

	GrowthUnitAMAP * guamap=(GrowthUnitAMAP *)plt->getPlantFactory()->createInstanceDecompAxe(GROWTHUNIT, "AMAPSimMod");
	guamap->set(age_loc, theBud->timeForATest);
	theBranc->addGu(guamap);
	theBranc->addCycle();
	theBranc->addZone();


	if (theBranc->CurrentIndex() > 0)
		theBud->guRamificationType = RETARDE1;

	// branch is considered as a reiteration only on first GU (to control reiteration order)
//	if (theBranc->getGuNumber() > 1)
//		theBranc->reiterationOrder = 0;

}



float GrowthEngineAMAP::differentiation ()
{
	float prob[5], jump;
	long nextstate, nb;

	phyAgeRef = nextPhyAgeRef;
	noDifferentiationGu --;
	if (noDifferentiationGu <= 0)
	{
		nb = (int)theBud->axisref->val1DParameter (nbtransition, (int)phyAgeRef, theBranc);
		prob[0] = theBud->axisref->val1DParameter (transitionfrequency1, (int)phyAgeRef, theBranc);
		prob[1] = theBud->axisref->val1DParameter (transitionfrequency2, (int)phyAgeRef, theBranc) + prob[0];
		prob[2] = theBud->axisref->val1DParameter (transitionfrequency3, (int)phyAgeRef, theBranc) + prob[1];
		prob[3] = theBud->axisref->val1DParameter (transitionfrequency4, (int)phyAgeRef, theBranc) + prob[2];
		prob[4] = theBud->axisref->val1DParameter (transitionfrequency5, (int)phyAgeRef, theBranc) + prob[3];
		nextstate = cumulativeLaw (theBranc->getPlant(),prob, nb, 0);
		switch (nextstate)
		{
			case 0:
				jump = theBud->axisref->val1DParameter (transitionjump1, (int)phyAgeRef, theBranc);
				break;
			case 1:
				jump = theBud->axisref->val1DParameter (transitionjump2, (int)phyAgeRef, theBranc);
				break;
			case 2:
				jump = theBud->axisref->val1DParameter (transitionjump3, (int)phyAgeRef, theBranc);
				break;
			case 3:
				jump = theBud->axisref->val1DParameter (transitionjump4, (int)phyAgeRef, theBranc);
				break;
			case 4:
				jump = theBud->axisref->val1DParameter (transitionjump5, (int)phyAgeRef, theBranc);
				break;
		}
		if (jump < 0)
			nextPhyAgeRef = -jump-1;
		else
			nextPhyAgeRef += jump;
		if (nextPhyAgeRef < phyAgeRef)
			nextPhyAgeRef = phyAgeRef;
		noDifferentiationGu = binodec (theBranc->getPlant(),
			                           theBud->axisref->val1DParameter (nbremain, (int)nextPhyAgeRef, theBranc),
									   theBud->axisref->val1DParameter (pbremain, (int)nextPhyAgeRef, theBranc),
									   theBud->axisref->val1DParameter (shiftremain, (int)nextPhyAgeRef, theBranc));
		if (noDifferentiationGu == -1)
			noDifferentiationGu = 999999;
	}
	return (float)phyAgeRef;
}





/* current Gu dormancy */
char GrowthEngineAMAP::dormance()
{
	int phyAgeLoc;
	char    res=1;

	//phyAgeLoc =  (int)((GrowthUnitAMAP *)theBranc->Ss()[theBranc->currentIndex])->phyAgeGu - 1;
	//res = (char)(1. - bernouilli (theBranc->getPlant(),theBud->axisref->val1DParameter (rtsleepgu, phyAgeLoc, theBranc)));
	return (res);
}




int GrowthEngineAMAP::startGu()
{

	double  duree;
	long  phyAgeLoc;
	int testedGuIndex;


	theBud->endUnit = 0;

	//Bud * p =theBranc->getBearer();

	phyAgeLoc=(int)this->phyAgeRef;

	this->polycyc = calcPolycyc (this->polycyc, (int)this->phyAgeRef);

	testedGuIndex=theBranc->CurrentIndex();

	((GrowthUnitAMAP *)theBranc->Ss()[testedGuIndex])->sympodeGu = (unsigned char)bernouilli(theBranc->getPlant(),fabs(theBud->axisref->val1DParameter (rtpbsympodeGu, (int)this->phyAgeRef, this->theBranc)));
	if (theBud->axisref->val1DParameter (rtpbsympodeGu, (int)this->phyAgeRef, this->theBranc) < 0)
		((GrowthUnitAMAP *)theBranc->Ss()[testedGuIndex])->sympodeGu *= 0xff;


	((GrowthUnitAMAP *)theBranc->Ss()[testedGuIndex])->actifGu = dormance();

	/* check Gu branching capability */

	((GrowthUnitAMAP *)theBranc->Ss()[testedGuIndex])->GuBranchingCapability =  testBranchingCapability();


	/* current Gu is inactive */
	if ( ((GrowthUnitAMAP *)theBranc->Ss()[testedGuIndex])->actifGu == 0)
	{
		theBud->endUnit |= ENDGU;
		this->phyAgeRef =  ((GrowthUnitAMAP *) theBranc->Ss()[testedGuIndex])->phyAgeGu;
		theBud->previousInstant = theBud->endInstant;
		/* compute current length of the cycle */
/*
		if (this->theBranc == this->theBranc->getBearer())
			duree = 1. / theBud->axisref->valSingleParameter (glvitesse, this->theBranc) / theBud->axisref->val1DParameter (NB_VAR+theBranc->lpIndex*NB_VAR_ENT+rerythme,phyAge, this->theBranc);
		else
*/
		duree = 1. / this->rythme / theBud->axisref->val1DParameter (rtrythme,phyAgeLoc, this->theBranc);
		theBud->endInstant += duree;
		theBud->nextTime += duree;
		if (   (   theBud->guRamificationType == RETARDECYC
					|| theBud->guRamificationType == ANTICIPE)
				&& testedGuIndex == 0)

			this->prolepticSleepingTime = duree;

		if (   theBud->nextTime > Scheduler::instance()->getHotStop()
			 && theBud->nextTime < Scheduler::instance()->getHotStop()+EPSILON)
			theBud->nextTime = Scheduler::instance()->getHotStop();

		/* next eventData */
		//MODIF STOP TIME
		//if (theBud->nextTime <= (double)(Scheduler::instance().getStopTime()) + EPSILON)
		//{
		Scheduler::instance()->self_signal_event( NULL,(double)theBud->nextTime,Scheduler::instance()->getTopPriority());
		//}
		return 0;
	}

	return 1;

}


int GrowthEngineAMAP::endGu()
{

	long phyAgeLoc = (int)this->phyAgeRef;

	theBud->endUnit |= ENDGU;
	theBud->previousInstant = theBud->endInstant;

	/* go to instant for nex Gu */
	if (theBud->nextTime < theBud->endInstant)
		theBud->nextTime  = theBud->endInstant;
	/* compute current phy age */
	differentiation();

	phyAgeLoc = (int)this->phyAgeRef;



	if (   !this->prolepticDormancyTestNumber
			&& this->prolepticSleepingTime)
		this->prolepticSleepingTime = 0;

	if (phyAgeLoc < this->maxPhyAge)
	{
		getTimeGu (phyAgeLoc);
	}
	else /* branch has reached its max phy age */
	{
		theBud->endUnit |= FINISHED;

	}


	/* branch has reached its max number of Gu */
	if (theBranc->getGuNumber() >= this->maxGuNumber)
	{
		theBud->endUnit |= FINISHED;
	}
	//MODIF STOP TIME
	//else if (   theBud->nextTime +theBud->timeForATest <= (double)(Scheduler::instance().getStopTime()) + EPSILON /* sets next eventData */
	//        && !theBud->death
	//      && !(theBud->endUnit & FINISHED))
	else if (!theBud->death
			&& !(theBud->endUnit & FINISHED))
	{
		getTime (phyAgeLoc, theBud->nextTime );
		theBud->nextTime  += theBud->timeForATest;

		if (   theBud->nextTime > Scheduler::instance()->getHotStop()
			 && theBud->nextTime < Scheduler::instance()->getHotStop()+EPSILON)
			theBud->nextTime = Scheduler::instance()->getHotStop();

		Scheduler::instance()->self_signal_event( NULL,(double)theBud->nextTime,Scheduler::instance()->getTopPriority());
	}
	return 0;
}

short GrowthEngineAMAP::testBudDeath(CycleAMAP *c)
{

	long  phyAgeLoc;
	short guEnd;
	int   poselem;


	guEnd = 0;

	phyAgeLoc=(int)this->phyAgeRef;
	theBud->endUnit = 0;

	long basecompt = (long)theBud->axisref->val1DParameter (rtbasecompt, phyAgeLoc, this->theBranc);


	poselem = theBranc->calcPosElem (BASAL, basecompt, (long)theBud->axisref->val1DParameter (rttypetop, phyAge, this->theBranc)) ;

	/* test death of current node according to its type (z1, z2) */
	float pbvie;
	if (poselem == -1)
		pbvie = 1;
	else
		pbvie = theBud->axisref->val2DParameter (rtpbvie, phyAgeLoc, poselem, this->theBranc);

	theBud->death = (char)(1-bernouilli(theBranc->getPlant(), pbvie));

	// if cycle has ended and counting base is TEST, test death until end of the test stack
	if (   c->end
		&& !theBud->death
		&& basecompt == TEST)
	{
		int nb = c->PreformTestNumber + c->NeoformTestNumberD + c->NeoformTestNumberN;
		for (int i=theBranc->getTestIndexWithinCycle()+1; i<nb; i++)
		{
			theBranc->addTest();
			poselem ++;
			/* test death of current node according to its type (z1, z2) */
			if (!theBud->death)
			{
				float pbvie = theBud->axisref->val2DParameter (rtpbvie, phyAgeLoc, poselem, this->theBranc);
				theBud->death = (char)(1-bernouilli(theBranc->getPlant(), pbvie));
			}
		}
	}

	if (theBud->death == 1)
	{
		theBud->deathInstant = Scheduler::instance()->getTopClock();
		guEnd = 1;
	}

	return guEnd;

}

short GrowthEngineAMAP::testEndGu(long& cycleEnd)
{
	short  guEnd=0;

	theBud->nextTime += theBud->timeForATest;
	if (   (theBud->nextTime >  theBud->endInstant + EPSILON)
		&& !this->prolepticDormancyTestNumber)
	{
		theBud->nextTime -= theBud->timeForATest;
		guEnd = 1;
	}

	CycleAMAP *cycle = (CycleAMAP*)this->theBranc->getCurrentElementOfSubLevel(GROWTHCYCLE);
	if (cycle->end)
	{
		cycleEnd = 1;
		theBud->endUnit |= ENDCYCLE;
		if (theBranc->getCurrentCycleIndexWithinGu() == this->nbcyc-1)
			guEnd = 1;
	}
	else
		cycleEnd = 0;


	return guEnd;

}






/* current Gu branching capability */
char GrowthEngineAMAP::testBranchingCapability()
{
	int phyAgeLoc, prevphyAge;
	char    res = 1;
	unsigned char oldstate=0xff;
	USint testedGuIndex=theBranc->CurrentIndex();


	//phyAgeLoc = (int)((GrowthUnitAMAP *)theBranc->Ss()[testedGuIndex])->phyAgeGu - 1;

	//if(testedGuIndex>0)
	//{

	//	prevphyAge =  (int)((GrowthUnitAMAP *)theBranc->Ss()[testedGuIndex-1])->phyAgeGu - 1;

	//	if (theBud->axisref->val1DParameter (rtguinit, phyAgeLoc, theBranc) != theBud->axisref->val1DParameter (rtguinit, prevphyAge, theBranc))
	//		oldstate = 0xff;
	//}


	//if ((long)oldstate > 0)
	//	oldstate = 1;

	//res = (char)markovSimulation2 (theBranc->getPlant(),(long)oldstate,
	//		(float)1.-theBud->axisref->val1DParameter (rtguinit, phyAgeLoc, theBranc),
	//		theBud->axisref->val1DParameter (rtgunb, phyAgeLoc, theBranc),
	//		theBud->axisref->val1DParameter (rtgub, phyAgeLoc, theBranc),
	//		RANDMARKOVGU);
	return(res);
}


void GrowthEngineAMAP::serialize(Archive& ar )
{



	if(ar.isWriting() )
	{
		ar<<growthEnd<<phyAge<<nextPhyAgeRef<<phyAgeRef<<maxPhyAge<<prolepticSleepingTime<<BranchBegining;

		ar<<okZone2<<noDifferentiationGu<<prolepticDormancyTestNumber<<immediateBranchingOrder<<polycyc<<nbcyc<<testZone2Number;

		ar<<currentGuTestNumber<<maxGuNumber<<rythme<<entnRealized;



	}
	else
	{
		ar>>growthEnd>>phyAge>>nextPhyAgeRef>>phyAgeRef>>maxPhyAge>>prolepticSleepingTime>>BranchBegining;

		ar>>okZone2>>noDifferentiationGu>>prolepticDormancyTestNumber>>immediateBranchingOrder>>polycyc>>nbcyc>>testZone2Number;

		ar>>currentGuTestNumber>>maxGuNumber>>rythme>>entnRealized;


	}
}

