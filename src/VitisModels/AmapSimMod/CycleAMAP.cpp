/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "CycleAMAP.h"
#include "BrancAMAP.h"
#include "Bud.h"
#include "randomFct.h"
#include "defAMAP.h"
#include "DebugNew.h"

CycleAMAP::CycleAMAP(Plant * plt):DecompAxeLevel(plt, GROWTHCYCLE)
{
	PreformEntnNumber = -1;
	NeoformTestNumberD = 0;
	NeoformTestNumberN = 0;
	NeoformTestNumberP = 0;
	NeoformP = 0;
	okZ2 = 0;
	end = false;
}


CycleAMAP::~CycleAMAP()
{
}

void CycleAMAP::init (USint index, AxisReference *axisref, long phyAgeLoc, BrancAMAP *theBranc )
{
	float pbneo, prefn, prefp;
	int prefd;
	int maxTest = theBranc->getBud()->groEngine->currentGuTestNumber;

	if (index > 0)
		maxTest -= theBranc->getTestIndexWithinGu();

	switch (index)
	{
		case 0:
			pbneo = axisref->val1DParameter (rtneocyc1, phyAgeLoc, theBranc);
			break;
		case 1:
			pbneo = axisref->val1DParameter (rtneocyc2, phyAgeLoc, theBranc);
			break;
		default:
			pbneo = axisref->val1DParameter (rtneocyca, phyAgeLoc, theBranc);
			break;
	}

	okZ2 = (bool)bernouilli ((Plant*)theBranc->getPlant(),pbneo);

	if (okZ2)
	{
		switch (index)
		{
			case 0:
				NeoformTestNumberD = axisref->val1DParameter (rtneod1, phyAgeLoc, theBranc);
				NeoformTestNumberN = axisref->val1DParameter (rtneon1, phyAgeLoc, theBranc);
				NeoformTestNumberP = axisref->val1DParameter (rtneop1, phyAgeLoc, theBranc);
				NeoformP = axisref->val1DParameter (rtneob1, phyAgeLoc, theBranc);
				prefn = axisref->val1DParameter (rtneopn1, phyAgeLoc, theBranc);
				prefp = axisref->val1DParameter (rtneopp1, phyAgeLoc, theBranc);
				prefd = axisref->val1DParameter (rtneopd1, phyAgeLoc, theBranc);
				break;
			case 1:
				NeoformTestNumberD = axisref->val1DParameter (rtneod2, phyAgeLoc, theBranc);
				NeoformTestNumberN = axisref->val1DParameter (rtneon2, phyAgeLoc, theBranc);
				NeoformTestNumberP = axisref->val1DParameter (rtneop2, phyAgeLoc, theBranc);
				NeoformP = axisref->val1DParameter (rtneob2, phyAgeLoc, theBranc);
				prefn = axisref->val1DParameter (rtneopn2, phyAgeLoc, theBranc);
				prefp = axisref->val1DParameter (rtneopp2, phyAgeLoc, theBranc);
				prefd = axisref->val1DParameter (rtneopd2, phyAgeLoc, theBranc);
				break;
			default:
				NeoformTestNumberD = axisref->val1DParameter (rtneoda, phyAgeLoc, theBranc);
				NeoformTestNumberN = axisref->val1DParameter (rtneona, phyAgeLoc, theBranc);
				NeoformTestNumberP = axisref->val1DParameter (rtneopa, phyAgeLoc, theBranc);
				NeoformP = axisref->val1DParameter (rtneoba, phyAgeLoc, theBranc);
				prefn = axisref->val1DParameter (rtneopna, phyAgeLoc, theBranc);
				prefp = axisref->val1DParameter (rtneoppa, phyAgeLoc, theBranc);
				prefd = axisref->val1DParameter (rtneopda, phyAgeLoc, theBranc);
				break;
		}
	}
	else
	{
		switch (index)
		{
			case 0:
				prefn = axisref->val1DParameter (rtprefn1, phyAgeLoc, theBranc);
				prefp = axisref->val1DParameter (rtprefp1, phyAgeLoc, theBranc);
				prefd = axisref->val1DParameter (rtprefd1, phyAgeLoc, theBranc);
				break;
			case 1:
				prefn = axisref->val1DParameter (rtprefn2, phyAgeLoc, theBranc);
				prefp = axisref->val1DParameter (rtprefp2, phyAgeLoc, theBranc);
				prefd = axisref->val1DParameter (rtprefd2, phyAgeLoc, theBranc);
				break;
			default:
				prefn = axisref->val1DParameter (rtprefna, phyAgeLoc, theBranc);
				prefp = axisref->val1DParameter (rtprefpa, phyAgeLoc, theBranc);
				prefd = axisref->val1DParameter (rtprefda, phyAgeLoc, theBranc);
				break;
		}
	}

	if (prefn + prefd > maxTest)
	{
		if (prefn > maxTest)
			prefn = maxTest;
		else
		{
			if (prefd > maxTest - prefn)
				prefd = maxTest;
			else
				prefd = 0;
			prefn = 0;
		}
	}

	PreformTestNumber = prefn + prefd;
	PreformEntnNumber = binodec ((Plant*)theBranc->getPlant(), prefn, prefp, prefd);

	//if (   theBranc->getBearer() == theBranc
	//	&& index == 0
	//	&& PreformEntnNumber == 0)
	//	PreformEntnNumber = 1;
}


void CycleAMAP::serialize(Archive& ar )
{
	DecompAxeLevel::serialize(ar);

	if(ar.isWriting() )
	{
		ar<<PreformEntnNumber<<NeoformTestNumberD<<NeoformTestNumberN<<NeoformTestNumberP<<NeoformP<<okZ2<<end<<active;
	}
	else
	{
		ar>>PreformEntnNumber>>NeoformTestNumberD>>NeoformTestNumberN>>NeoformTestNumberP>>NeoformP>>okZ2>>end>>active;
	}
}



