/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "PlantAMAP.h"
#include "GeomBrancAMAP.h"
#include "corresp.h"
#include "UtilMath.h"
#include "randomFct.h"
#include "Bud.h"
#include "AmapConfig.h"
#include "DebugNew.h"




GeomBrancAMAP::GeomBrancAMAP(Hierarc * brc,  GeomBranc * gbear, Plant * gt):GeomBrancCone(brc,gbear, gt)
{
	phy = NULL;
	if (hierarc != NULL)
	{
		axisref=(AxisReference*)getBranc()->getPlant()->getParamData(string("AMAPSimMod"));

		phy = new float [axisref->getNbLPAxe()];

		for (int i=0; i<axisref->getNbLPAxe(); i++)
			this->phy[i] = 0;

		initialDiameter = 1.;

		scale = 1;

		anginit = 0;


		phyAgeCur=((GrowthUnitAMAP *)hierarc->getBranc()->Ss()[0])->phyAgeGu-1;
		// patch for first fictive Gu in RoCoCau
		if (phyAgeCur < 0)
            phyAgeCur = 0;

		typeInsert=(short)axisref->val1DParameter (rgtypins, (int)phyAgeCur, (BrancAMAP *)hierarc->getBranc());

		axesympode[0]=2.;

		branchTotalEntnNumber=((BrancAMAP *)brc->getBranc())->getCurrentEntnNumberWithinBranch();
	}


}



GeomBrancAMAP::~GeomBrancAMAP(void)
{
	if (phy != NULL)
		delete [] phy;

}

/* computes initial values of geometric parameters for a branch */
void GeomBrancAMAP::computeParamGeomInit ()
{
	BrancAMAP* b=(BrancAMAP *)this->hierarc->getBranc();

	insertionAngle = axisref->val1DParameter (anginsinit, (int)b->phyAgeInit-1, b);
	deviationAngle = axisref->val1DParameter (deviation, (int)b->phyAgeInit-1, b);
}

Matrix4  GeomBrancAMAP::computeDeviationTranslation(const Matrix4 & currentTriedron, Matrix4 & referenceTriedron)
{

	Vector3 axerot=currentTriedron.getMainVector()^referenceTriedron.getMainVector();


	if (   axerot.x() == 0.
			&& axerot.y() == 0.
			&& axerot.z() == 0.)
		axerot.y()= 1.;

	axerot.normalize();

	return Matrix4::axisRotation(axerot, this->deviationAngle)*referenceTriedron;

}



void GeomBrancAMAP::determine_plagio (Matrix4 & plagMat, const Vector3 mat_ref , const Vector3 mat_plan)
{



	Vector3 proj;
	Vector3 perp;



	Vector3 np =mat_plan;

	Vector3 ref =mat_ref;

	proj=projection_plan(ref,np);

	proj.normalize();

	perp=Vector3::axisRotation(ref,proj,(float)GEOM_HALF_PI);

	plagMat.setSecondaryVector(proj.x(), proj.y(), proj.z());

	plagMat.setNormalVector(perp.x(), perp.y(), perp.z());

	plagMat.setMainVector(ref.x(), ref.y(), ref.z());


}


int GeomBrancAMAP::initGeomtry()
{

	//Variable destin� � sauvegarder la matrice de l'�l�ment porteur
	Matrix4   referenceTriedron;

	Vector3 transla;

	GeomElem *ge, *gprev;

	float angphy, angins;


	Hierarc * p = hierarc->getBearer();


	this->computeParamGeomInit ();

	this->computeInitialLengthDiameter();

	ge=v_plt->getPlantFactory()->createInstanceGeomElem();

	ge->setPtrBrc(this);

	elemGeo.push_back(ge);

	//GeomElment of internode bearer
	if (hierarc == p)
	{
		gprev=ge;
	}
	else
	{	//le dernier �l�ment du tableau d'entrenoeuds correspond � l'entrenoeud porteur de cette branche
		//Modif sg 23.07.07
		if (posOnBearer >= gbearer->getElementsGeo().size())
 			posOnBearer = gbearer->getElementsGeo().size() - 1;
		gprev=gbearer->getElementsGeo().at(posOnBearer);
		referenceTriedron = gprev->getMatrix();
		referenceTriedron = Matrix4::translation(referenceTriedron.getMainVector()*(float)gprev->getLength())*referenceTriedron;

	}

	ge->setMatrix(referenceTriedron);

	Matrix4 & currentTriedron = ge->getMatrix();

	transla=currentTriedron.getTranslation();
	transla=((GeomBrancCone *)gbearer)->getCurrentTransform().getTranslation();

	currentTriedron.setTranslation(0,0,0);

	referenceTriedron.setTranslation(0,0,0);

	angins=computeInsertAngle();

	angphy=computeInsertAnglePhy();

	currentTriedron=axeInsertionOn (currentTriedron,referenceTriedron,typeInsert,angins,angphy,axisref->val1DParameter (rgaleatoire, (int)phyAgeCur, (BrancAMAP *)hierarc->getBranc()),this->alea);

	computeBending (ge);

	/* deviation du porteur */
	if (   this->deviationAngle != 0.
		&& hierarc != p)
	{
		Matrix4 originBearerTransform = ((GeomBrancCone *)gbearer)->getCurrentTransform();
		originBearerTransform.setTranslation (0, 0, 0);
//		Matrix4 m = computeDeviationTranslation(currentTriedron,referenceTriedron)*((GeomBrancCone *)gbearer)->getCurrentTransform();
//		Matrix4 m = Matrix4::translation(transla)*computeDeviationTranslation(currentTriedron,referenceTriedron);
		Matrix4 m = Matrix4::translation(transla)*computeDeviationTranslation(currentTriedron,originBearerTransform);
		((GeomBrancCone *)gbearer)->setCurrentTransform(m);

	}


	currentTriedron.setTranslation(transla.x(), transla.y(), transla.z());

	/* G.XII        Nouvelle origine                                        */
	if (hierarc != p)
	{
		if (   typeInsert != INSER1
			&& angins != 0)
				currentTriedron = computeInsertTranslation(currentTriedron,gprev)*currentTriedron;
		else
			currentTriedron = Matrix4::translation(referenceTriedron.getMainVector()*(float)gprev->getLength())*currentTriedron;
	}




	this->currentTrans=ge->getMatrix();


	if(hierarc!=p)
        gprev->getGeomChildren().push_back(this);



	if(computeSuperSimplif())
        return 0;




	return 1;

}


Matrix4 GeomBrancAMAP::axeInsertionOn(	Matrix4 & currentTriedron, /* current triedron  */
		Matrix4 & referenceTriedron, /* bearer triedron (initial one) */
		long typins,    /* insertion type (see above) */
		float angins,   /* insertopn angle */
		float angphy,   /* phyllotaxy angle */
		float aleas,    /* randomization factor */
		double rnd)     /* particular random value for that axe */
{
	long   i;
	float  ins, phy, aux;
	Vector3 vec;
	Matrix4 matmp;
	double randi, randp;
	Matrix4 gravity = Matrix4(Vector4(0,-1,0,0),Vector4(1,0,0,0),Vector4(0,0,1,0),Vector4(0,0,0,1));



	if (   fabs(referenceTriedron(0,2)) < .0001
			&& fabs(referenceTriedron(1,2)) < .0001)
	{
		referenceTriedron(0,2) = (float).0001;
	}


	/* corection between 0 and 2*GEOM_PI */
	i = (long)(angins / FGEOM_TWO_PI);
	if (i != 0)
		angins = angins -FGEOM_TWO_PI*i;
	i = (long)(angphy / FGEOM_TWO_PI);
	if (i != 0)
		angphy = angphy - FGEOM_TWO_PI*i;

	/* angle randomizaion */
	//randi = rnd;
	//aleains (&randi, &randi);
	//aleains (&randi, &randp);
	//randi = 0.5 - randi;
	//randp = 0.5 - randp;
	randi = 0.5 - rnd;
	randp = 0.5 - rnd;


	switch (typins)
	{
		case INSERINIT:
			/* according to gravity */
			ins = angins * ((float)1. + aleas * (float)randi);
			phy = angphy + FGEOM_PI * aleas * (float)randp;

			currentTriedron=Matrix4::axisRotation(referenceTriedron.getMainVector(),phy)*Matrix4::axisRotation(referenceTriedron.getSecondaryVector(),-ins)*currentTriedron;


			break;

		case INSER1:
		case INSER9:
			/*  general case, INSER9 will adjest insertion position to the bearers diameter */
			ins = angins * ((float)1. + aleas * (float)randi);
			phy = angphy;
			phy = angphy + FGEOM_PI * aleas * (float)randp;
			currentTriedron=Matrix4::axisRotation(referenceTriedron.getMainVector(),phy)*Matrix4::axisRotation(referenceTriedron.getNormalVector(),-ins)*currentTriedron;


			break;

		case INSER2:
			/*  plagiotropy  */
			ins = angins * ((float)1. + aleas * (float)randi);
			if (hierarc->ordre < 2)
				phy = angphy + FGEOM_PI * aleas * (float)randp;
			else
				phy = /*angphy + */FGEOM_PI * ((float)hierarc->indexOnBearerAtLowestLevel + aleas * (float)randp);

			determine_plagio (currentTriedron, referenceTriedron.getMainVector(), gravity.getMainVector());

			currentTriedron=Matrix4::axisRotation(referenceTriedron.getMainVector(),phy)*Matrix4::axisRotation(referenceTriedron.getSecondaryVector(),-ins)*currentTriedron;

			determine_plagio (currentTriedron, currentTriedron.getMainVector(), gravity.getMainVector());
			break;

		case INSER3:
			/*  straight */

			break;

		case INSER4:
			/*  according to gravity */
			ins = angins * ((float)1. + aleas * (float)randi);
			if (fabs(ins) < .00001)
				ins = (float).00001;
			phy = angphy + FGEOM_PI * aleas * (float)randp;

			currentTriedron=gravity*Matrix4(Matrix4::axisRotation (gravity.getNormalVector(), -ins));

			currentTriedron=Matrix4(Matrix4::axisRotation (gravity.getMainVector(), phy))*currentTriedron;

			break;

		case INSER5:
			/*  pseudo_plagiotropy  */
			ins = angins * ((float)1. + aleas * (float)randi);
			phy = angphy + FGEOM_PI * aleas * (float)randp;
			vec = referenceTriedron.getMainVector();
			vec.normalize();
			aux = gravity(0,2)*vec.x() + gravity(1,2)*vec.y() + gravity(2,2)*vec.z();
			if (fabs(aux) < 1e-4)
				aux = 0.0;
			else
			{
				float phyd;
				phyd = log10(fabs(aux));
				phyd = fabs(phyd);
				phyd = ceil(phyd);
				aux = pow (aux, phyd);
				aux = fabs(aux);
			}
			phy = (float)fmod ((double)phy, 2.*FGEOM_PI);
			if (phy < 0)
				phy += 2 * FGEOM_PI;
			if (phy < FGEOM_PI / 2.)
				phy *= aux;
			else if (phy < 3. * FGEOM_PI / 2.)
			{
				phy -= FGEOM_PI;
				phy = FGEOM_PI + phy * aux;
			}
			else
			{
				phy -= 2. * FGEOM_PI;
				phy = phy * aux;
			}

			determine_plagio (currentTriedron, referenceTriedron.getMainVector(), gravity.getMainVector());

			currentTriedron=Matrix4::axisRotation(referenceTriedron.getMainVector(),phy)*Matrix4::axisRotation(currentTriedron.getSecondaryVector(),-ins)*currentTriedron;

			break;

		case INSER6:
			/*  secondary direction to top */
			ins = angins * ((float)1. + aleas * (float)randi);
			phy = angphy + FGEOM_PI * aleas * (float)randp;

			currentTriedron=Matrix4::axisRotation(referenceTriedron.getMainVector(),phy)*Matrix4::axisRotation(referenceTriedron.getNormalVector(),-ins)*currentTriedron;

			determine_plagio (currentTriedron, currentTriedron.getMainVector(), gravity.getMainVector());

			break;

		case INSER7:
			/*  random  */
			ins = FGEOM_PI * (float)randi;
			phy = FGEOM_TWO_PI * (float)randp;

			currentTriedron=gravity*Matrix4(Matrix4::axisRotation (gravity.getNormalVector(), -ins));

			currentTriedron=currentTriedron*Matrix4(Matrix4::axisRotation (gravity.getMainVector(), phy));

			break;

		case INSER8:
			/*  fixed angle to the down direction */

			ins = angins;

			determine_plagio (matmp, currentTriedron.getMainVector(), v_plt->getConfigData().light);

			currentTriedron=Matrix4::axisRotation(matmp.getNormalVector(),ins)*currentTriedron;

			/*  secondary direction to top */
			determine_plagio (currentTriedron, currentTriedron.getMainVector(), gravity.getMainVector());


			break;

		default:
			/*  INSER1 default  */

			ins = angins * ((float)1. + aleas * (float)randi);
			phy = angphy + FGEOM_PI * aleas * (float)randp;

			currentTriedron=Matrix4::axisRotation(referenceTriedron.getMainVector(),phy)*Matrix4::axisRotation(referenceTriedron.getNormalVector(),ins)*currentTriedron;


			break;
	}

	return currentTriedron;
}


float GeomBrancAMAP::computeInsertAnglePhy()
{
	float vert,phy;

	BrancAMAP * b =(BrancAMAP *)this->hierarc->getBranc();

	GeomBrancAMAP * gbrp=(GeomBrancAMAP *)this->gbearer;

	BrancAMAP * p = b->getBearer();

	long phyAgeport = (int)p->getCurrentGu()->phyAgeGu - 1;
	/* G.VII.1       verticille                                        */
	vert = FGEOM_TWO_PI * (this->hierarc->indexIntoVerticille-1) / (float)this->hierarc->verticilleBranchNumber;

	if (b != p)
		phy = gbrp->getAngInit() + gbrp->getPhyllotaxyAt((int)b->lpIndex);
	else
		phy = 0;

	phy += vert;

	if (   b != p
			|| b->getCurrentGuIndex() != 0)
	{
		int typent;
		typent = p->getCurrentZoneIndex();
		phy += axisref->val1DParameter (NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*b->lpIndex+rgphase, phyAgeport, b);
	}


	return phy;

}

float GeomBrancAMAP::computeInsertAngle()
{

	float ang, angmin, angmax;
	long   plagins, posit;
	BrancAMAP* b=(BrancAMAP *)this->hierarc->getBranc();
	BrancAMAP * p = b->getBearer();

	posit = ((GeomBrancAMAP *)gbearer)->branchGuNumberWhithoutLimit - p->getCurrentGuIndex() - 2;

	if (b->getBud()->branchRamificationType == ANTICIPE)
		posit ++;


	//if (b == p)
	//	angmax = axisref->valSingleParameter(glangins, b);
	//else
		angmax = this->insertionAngle;

	angmin = angmax*axisref->val1DParameter (reevolangins, (int)phyAgeCur, b);

	plagins = (long)axisref->val1DParameter (renbtestangins, (int)phyAgeCur, b);

	if (posit > axisref->val1DParameter (relagangins, (int)phyAgeCur, b))
		ang = interpole_lineaire(1, (long)axisref->val1DParameter (relagangins, (int)phyAgeCur, b), angmin, plagins, angmax, posit);
	else
		ang = angmax;

	if (   (   ((GrowthUnitAMAP *)b->getCurrentGu())->sympodeGu == 255
				|| (((InterNodeAMAP *)b->getCurrentEntn())->sympod))
			&& this->axesympode[0] == 2.
			&& axisref->val1DParameter (rgsympode, b->getBud()->getPhyAge(), b) != 0)
	{
		if (b == p)
			this->anginit = 875;
		else
			this->anginit = (float)25.*(p->getTestIndexWithinBranch()+34)/(p->getCurrentGuIndex()+1);
		this->axesympode[0] = 3;
	}


	return ang;

}



/* compute initial parameter value for length and diameter */
void GeomBrancAMAP::computeInitialLengthDiameter ()
{

	BrancAMAP * b =(BrancAMAP *)this->hierarc->getBranc();

	b->computeInitialLengthDiameter (&this->scale, &this->initialDiameter);

}

int GeomBrancAMAP::computeBranc(bool silent)
{
	float instant;

	if(hierarc == NULL)
        return -1;

	BrancAMAP * bv =(BrancAMAP *)this->hierarc->getBranc();

//	v_plt->getConfigData().getRandomGenerator()->initState(b->ordre+2-(int)floor((double)(b->ordre+2)/NBCOMPTRAND)*NBCOMPTRAND);
	int savCounter = v_plt->getConfigData().getRandomGenerator()->getCounter();
	int savIdx = v_plt->getConfigData().getRandomGenerator()->setState(hierarc->ordre+2+(getPosOnBearer()+hierarc->indexIntoVerticille)%(NBCOMPTRAND-hierarc->ordre-2));

	v_plt->getConfigData().getRandomGenerator()->initState(hierarc->ordre+2+(getPosOnBearer()+hierarc->indexIntoVerticille)%(NBCOMPTRAND-hierarc->ordre-2));

	this->alea = (float)v_plt->getConfigData().getRandomGenerator()->Random();
	this->alea = (float)v_plt->getConfigData().getRandomGenerator()->Random();
	//this->alea = 0;

	int testNumb=bv->getTestNumberWithinGu();

	instant = (float)bv->theBud->endInstant;

	branchTotalTestNumbertheo = bv->getTestNumberWithinBranch();

	branchGuNumberWhithoutLimit = bv->getGuNumber();
	/* while (instant <= VitisConfig::simuTotalTime + EPSILON)
		{
		bv->branchGuNumberWhithoutLimit ++;
		bv->branchTotalTestNumbertheo += ((GrowthUnitAMAP *)b->getLastGu())->getTestNumberWithinGu();
		instant += 1. / bv->getBud()->getGrowth()->rythme / axisref->val1DParameter (NB_VAR+(axisref->getNbLPAxe()*bv->getCurrentZoneIndex()+bv->lpIndex)*NB_VAR_ENT+rerythme,bv->getBud()->getPhyAge(), bv);
		}*/

	while (instant < Scheduler::instance()->getTopClock() - EPSILON)
	{
		branchGuNumberWhithoutLimit ++;
		branchTotalTestNumbertheo = branchTotalTestNumbertheo + testNumb;
		instant += (float)1. / bv->getBud()->getGrowth()->rythme / axisref->val1DParameter (rtrythme,bv->getBud()->getPhyAge(), bv);
	}

	int ret = GeomBrancCone::computeBranc(silent);

	v_plt->getConfigData().getRandomGenerator()->setState(savIdx);
	v_plt->getConfigData().getRandomGenerator()->setCounter(savCounter);

	return ret;
}



void GeomBrancAMAP::updatePhyllotaxy ()
{

	BrancAMAP * bv =(BrancAMAP *)hierarc->getBranc();
	int entnInBr=	bv->getCurrentEntnIndexWithinBranch();

	int testDo = 1;


	int basecompt=(int)axisref->val1DParameter (rtbasecompt,(int)bv->getCurrentGu()->phyAgeGu, bv);

	if(basecompt == 0 && entnInBr != 0)
	{
		bv->startPosit();
		bv->positOnPreviousEntnWithinBranc();
		bv->endPosit();
	}

	for(int i=0; i<testDo; i++)
	{
		for (int k=0; k<axisref->getNbLPAxe(); k++)

			this->phy[k] += axisref->val1DParameter (NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*k+rgphyllotaxie, (int)bv->getCurrentGu()->phyAgeGu,bv);
	}

}


int GeomBrancAMAP::findSymbole ()
{
	BrancAMAP * bv= (BrancAMAP *)hierarc->getBranc();
	return (int)axisref->val1DParameter (rtnumsymb, (int)bv->getCurrentGu()->phyAgeGu - 1, bv);
}

/* compute current length */
float GeomBrancAMAP::computeLength ()
{
	BrancAMAP * bv= (BrancAMAP *)this->hierarc->getBranc();

	return bv->computeLength (this->scale, branchTotalTestNumbertheo);
}





/* compute current diameter */
float GeomBrancAMAP::computeTopDiam ()
{
	BrancAMAP * bv= (BrancAMAP *)this->hierarc->getBranc();

	return bv->computeDiam (this->initialDiameter, branchTotalTestNumbertheo);

}


/* compute bending angle for current node according to global flexion of branch */
void GeomBrancAMAP::flexionAtNode ()
{
	float   f, lbre, prds, plageredre;
	long    comp, endphyAge;
	long    ntf, realIndexFromEnd;

	BrancAMAP * bv =(BrancAMAP *)hierarc->getBranc();

	int entnIndexWithinBranch=bv->getCurrentEntnIndexWithinBranch();

	Matrix4 currentTriedron = this->currentTrans;
	Vector3 v;
	v = currentTriedron.getMainVector();
	v.normalize();
	currentTriedron.setMainVector(v.x(), v.y(), v.z());
	v = currentTriedron.getSecondaryVector();
	v.normalize();
	currentTriedron.setSecondaryVector(v.x(), v.y(), v.z());
	v = currentTriedron.getNormalVector();
	v.normalize();
	currentTriedron.setNormalVector(v.x(), v.y(), v.z());

	currentTriedron.setTranslation(0,0,0);

	int testIndexWithinBranch=bv->getTestIndexWithinBranch();


	f = 0.0;
	ntf = branchTotalTestNumbertheo - testIndexWithinBranch - 1;
	endphyAge = (int)((GrowthUnitAMAP *)bv->Ss()[bv->CurrentIndex()])->phyAgeGu;

	if (entnIndexWithinBranch > 0)
	{
		float flre, aux;

		//      norme (currentTriedron,dp);

		currentTriedron.getMainVector().normalize();
		prds = currentTriedron(2,2);
		if (prds > 1.)
			prds = 1.;
		if (prds < -1.)
			prds = -1.;
		f = (float)2.0 * (prds - this->totalBending);
		aux = (float)1.0 - this->conicity * entnIndexWithinBranch;
		aux *= aux;

//		flre = (float)1.0 / (sqrt(axisref->val1DParameter (rgyoung, (int)bv->getCurrentGu()->phyAgeGu - 1, bv)) * aux);
		flre = (float)1.0 / (sqrt(axisref->val1DParameter (rgyoung, (int)bv->phyAgeInit - 1, bv)) * aux);
		if (f < 0.0)
			f = 0.0;
		f = flre * sqrt(f);
		f = f/(float)(bv->getCurrentEntnNumberWithinGu());
		if (f > 10000)
		{
			//printf(Tabmess[1]);
			// exit(0);
		}

		lbre = (float)(bv->getCurrentEntnNumberWithinBranch());
		if (lbre < 1.0)
			lbre = 1.0;

		/* if straightening point is passed then reverse bending */
//		comp = (long)(fabs(axisref->val1DParameter (rginflexion, (int)bv->getCurrentGu()->phyAgeGu, bv)) * lbre);
		comp = (long)(fabs(axisref->val1DParameter (rginflexion, (int)bv->phyAgeInit, bv)) * lbre);
		if (entnIndexWithinBranch > comp-1)
			f = - f;

		/* end straightening is managed from end of branch according to phy age of the last ut */
		realIndexFromEnd = bv->getTestNumberWithinBranch() - testIndexWithinBranch-1;
		if (   axisref->val1DParameter (rgplagredre,endphyAge - 1, bv) > 0
				&& realIndexFromEnd <= axisref->val1DParameter (rgplagredre,endphyAge - 1, bv))
		{
			if (realIndexFromEnd == axisref->val1DParameter (rgplagredre,endphyAge - 1, bv))
			{
				this->inflex = (asin(prds)-axisref->val1DParameter (rgredres,endphyAge - 1, bv))/axisref->val1DParameter (rgplagredre,endphyAge - 1, bv);
			}
			else
			{
				f = this->inflex;
			}
		}

		/* computes new triedron according to bending */
		axeInsertionOn (currentTriedron,currentTriedron, 8, -f, 0.0, 0.0, 0.0);
	}
	else if (entnIndexWithinBranch == 0)
	{

		currentTriedron.getMainVector().normalize();
		prds=currentTriedron(2,2);
		if (prds > 1.)
			prds = 1.;
		if (prds < -1.)
			prds = -1.;
		plageredre = (float)ntf;
		if (plageredre > axisref->val1DParameter (rgplagredre,endphyAge - 1, bv))
			plageredre = axisref->val1DParameter (rgplagredre,endphyAge - 1, bv);
		if (plageredre == 0)
			plageredre = 1;
		this->inflex = (asin(prds)-axisref->val1DParameter (rgredres,endphyAge - 1, bv))/plageredre;
	}

	/* if there was a sympod, try to introduce sympod deviation */
	if (   (   bv->getCurrentGuIndex() > 0              /* sympod between ut */
				&& ((GrowthUnitAMAP *)bv->getCurrentGu())->sympodeGu
				&& bv->getCurrentEntnIndexWithinGu() == 0)
			|| (   bv->getCurrentCycleIndexWithinGu() > 0            /* sympod between cycle */
				&& (((InterNodeAMAP *)bv->getCurrentEntn())->sympod)
				&& bv->getCurrentEntnIndexWithinCycle() == 0))
	{
		float prov, angins;
		int phyAge, sympodeplan;


		/* computes deviation angle */
		bv->startPosit();
		bv->positOnPreviousEntnWithinBranc();

		phyAge = (int)bv->getCurrentGu()->phyAgeGu - 1;
		if (ntf < axisref->val1DParameter (renbtestsymp, phyAge, bv))
		{
			prov = ntf / axisref->val1DParameter (renbtestsymp, phyAge, bv);
			angins = axisref->val1DParameter (rgsympode, phyAge, bv) * (1 - prov) + axisref->val1DParameter (reevolsymp, phyAge, bv) * prov;
		}
		else if (axisref->val1DParameter (renbtestsymp, phyAge, bv) > 0)
			angins = axisref->val1DParameter (reevolsymp, phyAge, bv);
		else
			angins = axisref->val1DParameter (rgsympode, phyAge, bv);

		bv->endPosit();

		/* deviation should occur in a plan */
		if (   (   bv->getCurrentGuIndex()> 0
					&& ((GrowthUnitAMAP *)bv->getCurrentGu())->sympodeGu == 255)
				|| (((InterNodeAMAP *)bv->getCurrentEntn())->sympodPlan))
			sympodeplan = 1;
		else
			sympodeplan = 0;

		/* randomly choose a deviation direction */
		//if (!sympodeplan)
		{
			prov = (float)bernouilli(v_plt,.5);
			if (prov)
				angins *= -1.;
		}
		/* the planar deviation was already computed */
		if (   sympodeplan
				&& this->axesympode[0] <= 1.)
		{
			currentTriedron=Matrix4::axisRotation(axesympode, -angins)*currentTriedron;
			//triedronRotation (0, b->axesympode, currentTriedron, -angins, currentTriedron);
		}
		/* sympod probability is less than zero then deviation in a horizontal plan
		 * this is to avoid global change of direction (for instance : never to the top)
		 * */
		else if (  !sympodeplan
				&& axisref->val1DParameter (rgsympode, phyAge, bv) < 0)
		{
			Vector3 axe;

			axe[0] = (float).05;
			axe[1] = (float).05;
			axe[2] = (float).9;
			currentTriedron=Matrix4::axisRotation(axe, -angins)*currentTriedron;
			//triedronRotation (0, axe, currentTriedron, -angins, currentTriedron);
		}
		else
		{
			Matrix4 axe;
//			float sympodDirection;
			BrancAMAP * p =(BrancAMAP *)hierarc->getBearer()->getBranc();


			//if (   sympodeplan
			//		&& this->axesympode[0] == 2.
			//		&& axisref->val1DParameter (rgsympode, bv->getBud()->getPhyAge(), bv) != 0)
			//	sympodDirection = (float)25.*(p->getTestIndexWithinBranch()+34)/(p->getCurrentGuIndex()+1);
			//else if (   !sympodeplan
			//		&& axisref->val1DParameter (rgsympode, bv->getBud()->getPhyAge(), bv) != 0)
			//	sympodDirection = (float)25.*(testIndexWithinBranch+34)/(bv->getCurrentGuIndex()+1);
			/* rotation around the first vector to give the next rotation triedron */
			axe=Matrix4::eulerRotationXYZ(Vector3(0,0,this->anginit))*currentTriedron;
			//triedronRotation (0, currentTriedron, currentTriedron, b->anginit, axe);

			/* rotation of current triedron around the second deviated axis */
			currentTriedron=Matrix4::axisRotation(axe.getNormalVector(), -angins)*currentTriedron;
			//triedronRotation (2, axe, currentTriedron, -angins, currentTriedron);
			/* if not already initialized, this will be the planar deviation */
			if (sympodeplan)
			{
				this->axesympode=axe.getNormalVector();
			}

		}

	}

	currentTriedron.setTranslation(currentTrans.getTranslation().x(),currentTrans.getTranslation().y(),currentTrans.getTranslation().z());
	currentTrans=currentTriedron;
	//gelm->setMatrix(currentTriedron);
	/*TODO if external computation flag is set for orientation */


}



void GeomBrancAMAP::computeBending (GeomElem *ge)
{
	float prds, young, rgalea;
	long  phyAge;
	Matrix4 & currentTriedron = ge->getMatrix();
	BrancAMAP * bv =(BrancAMAP *)this->hierarc->getBranc();
	BrancAMAP * p =(BrancAMAP *)hierarc->getBearer()->getBranc();



	phyAge = (int)bv->getCurrentGu()->phyAgeGu - 1;
	rgalea = axisref->val1DParameter (rgaleatoire, phyAge, bv);

	prds = currentTriedron(2,2);
	if (   bv->nature != SimpleImmediateOrgan
			/* JFB 10/01/97 on calcule la fleche aussi pour les elements de nature 2
				qui comprenent plus de 1 entre-noeud */
			|| branchTotalEntnNumber > 1)
	{
		float  flre, length;

		length = (float)branchTotalEntnNumber;
//		young = axisref->val1DParameter (rgyoung, phyAge, bv);
		young = axisref->val1DParameter (rgyoung, (int)bv->phyAgeInit-1, bv);
		if ((long)length == 0)
			this->totalBending = 0.0;
		else
		{
			float tempoyoung;

			this->conicity = axisref->val1DParameter (rgconicite, phyAge, bv) / length;
			tempoyoung = young;
			if (   rgalea
					&& bv != p)
				tempoyoung *= (float)1.0 + rgalea/(float)10.0 * this->alea;
			flre = (float)1.0 / sqrt(tempoyoung);
			this->totalBending = bending( flre, prds, length, this->conicity);
		}
	}
	else
	{
		Vector3 transla=currentTriedron.getTranslation();

		currentTriedron.setTranslation(0,0,0);

		if (axisref->val1DParameter (rgflexion, phyAge, bv) < 0.0)
			this->totalBending = axisref->val1DParameter (rgflexion, phyAge, bv) * (prds - (float)1.0);
		else
			this->totalBending = -axisref->val1DParameter (rgflexion, phyAge, bv) * (prds + (float)1.0);
		axeInsertionOn (currentTriedron,currentTriedron, 8, this->totalBending, (float)0.0, axisref->val1DParameter (rgaleatoire, (int)phyAgeCur, bv), this->alea);



	}
}

void GeomBrancAMAP::simplification ()
{

	Vector3 positionTop;

	Vector3 positionBottom;

	Vector3 tmpVec, rotVec, transla, vectmp;

	unsigned int i,j;

	float scalarDir;

	float dif;

	GeomElemCone * gelmprec, * gelm;

	i=1;


	if(!v_plt->getConfigData().cfg_geoSimp)
		return ;

	while(i<elemGeo.size())
	{

		gelm=(GeomElemCone *)elemGeo[i];

		gelmprec=(GeomElemCone *)elemGeo[i-1];


		Matrix4 & currentTriedron = gelm->getMatrix();

		Matrix4 & precTriedron = gelmprec->getMatrix();


		dif=0;
		/* computes distance between top of current level and bottom of organ */
		positionTop=currentTriedron.getTranslation();

		positionBottom=precTriedron.getMainVector()*(float)(gelmprec->getLength())+precTriedron.getTranslation();

		dif += fabs(positionTop[0]-positionBottom[0]);
		dif += fabs(positionTop[1]-positionBottom[1]);
		dif += fabs(positionTop[2]-positionBottom[2]);


		/* computes direction shift between current level and organ */

		scalarDir = currentTriedron.getMainVector()*precTriedron.getMainVector();


		if (gelm->getSymbole() != gelmprec->getSymbole())
			i++;
		else if (  (   scalarDir > 0.90
					&& dif < 0.10
					&& gelm->getGeomChildren().empty())
				|| (scalarDir >= 0.9999))
		{
			/* concatenate current level and organ */

			gelmprec->setTopDiam(gelm->getTopDiam());

			if(scalarDir<0.999)
			{

				tmpVec=(precTriedron.getMainVector()*(float)gelmprec->getLength())+(currentTriedron.getMainVector()*(float)gelm->getLength());

				vectmp=(precTriedron.getMainVector()*(float)gelmprec->getLength());

				gelmprec->setLength(norm(tmpVec));

				vectmp.normalize();

				tmpVec.normalize();

				rotVec=vectmp^tmpVec;

				float norm = rotVec.normalize();

				if (norm > 0)
				{
					transla=precTriedron.getTranslation();

					precTriedron=Matrix4::translation(-transla)*precTriedron;

					precTriedron=Matrix4::axisRotation(rotVec, angle(vectmp,tmpVec))*precTriedron;

					precTriedron=Matrix4::translation(transla)*precTriedron;
				}
			}
			else
			{
				gelmprec->setLength(gelmprec->getLength()+gelm->getLength());
			}



			elemGeo.erase(elemGeo.begin()+i);


			for(j=0; j<gelm->getGeomChildren().size(); j++)
			{
				gelm->getGeomChildren().at(j)->setPosOnBearer(i);

				gelmprec->getGeomChildren().push_back(gelm->getGeomChildren().at(j));
			}

			this->v_plt->getGeometry().updateElemNumber(false);

			gelmprec->setPosInGeomBranc(i-1);
			gelmprec->setPosInBranc(gelm->getPosInBranc());

			delete gelm;
		}
		else
			i++;


	}//end while


}



