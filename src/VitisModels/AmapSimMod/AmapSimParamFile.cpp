/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "AmapSimParamFile.h"
#include "Exceptions.h"
using namespace Vitis;

namespace Vitis
{

	AmapSimVariable::AmapSimVariable()
	{
		nb_PhyAge=-1;

		ind_PhyAge_current=0;

		nb_pos_current=0;

		ind_pt_current=0;

		interpolation_PhyAge=0;

		interpolation_position_current=0;

		xmin=0;

		xmax=0;

		ymin=0;

		ymax=0;

		currentPhyAge=0;

		currentPosition=0;

		currentValue=0;

	}

	AmapSimVariable::~AmapSimVariable()
	{

	}

	void AmapSimVariable::set (int pos, float val)
	{
		set (pos, val, true);
	}

	void AmapSimVariable::set (int pos, float val, bool insert)
	{
		int ind;

		for (ind=0; ind<nb_pos_current; ind++)
			if (currentX[ind] >= pos)
				break;

		if (   ind < nb_pos_current
		    && currentX[ind] == pos)
		{
			currentY[ind] = val;
			currentPosition = pos;
			currentValue =  val;
		}
		else if (insert)
		{
			currentX.resize(nb_pos_current+1);
			currentY.resize(nb_pos_current+1);

			if (nb_pos_current > 0)
			{
				for (int i=nb_pos_current-1; i>=ind; i--)
				{
					currentX[i+1] = currentX[i];
					currentY[i+1] = currentY[i];
				}
			}
			currentX[ind] = pos;
			currentY[ind] = val;

			nb_pos_current++;
			currentPosition = pos;
			currentValue =  val;
		}
	}

	void AmapSimVariable::set (int pos, int subPos, float val)
	{
		set (pos, subPos, val, true);
	}

	void AmapSimVariable::set (int pos, int subPos, float val, bool insert)
	{
		int ind;

		for (ind=0; ind<nb_PhyAge; ind++)
			if (num_PhyAge[ind] >= pos)
				break;

		if (	!insert
			&& (   ind >= nb_PhyAge
			    || num_PhyAge[ind] != pos))
			return;

		if (   insert
			&& (   ind >= nb_PhyAge
			    || num_PhyAge[ind] != pos))
		{
			num_PhyAge.resize(nb_PhyAge+1);
			nb_pos.resize(nb_PhyAge+1);
			x.resize(nb_PhyAge+1);
			y.resize(nb_PhyAge+1);
			interpolation_position.resize(nb_PhyAge+1);
			for (int i=nb_PhyAge-1; i>=ind; i--)
			{
				num_PhyAge[i+1] = num_PhyAge[i];
				nb_pos[i+1] = nb_pos[i];
				x[i+1] = x[i];
				y[i+1] = y[i];
				interpolation_position[i+1] = interpolation_position[i];
			}
			interpolation_position[ind] = 0;
			num_PhyAge[ind] = pos;
			nb_pos[ind] = 1;
			x[ind].resize(1);
			x[ind][0] = subPos;
			y[ind].resize(1);
			y[ind][0] = val;
			nb_PhyAge ++;
		}

		ind_PhyAge_current = ind;
		currentX= x[ind];
		currentY = y[ind];
		nb_pos_current = nb_pos[ind];
		interpolation_position_current = 0;
		currentPosition = -1;
		currentPhyAge = pos;
		currentPosition = subPos;
		currentValue = val;

		set(subPos, val, insert);
	}

	AmapSimParamFile::AmapSimParamFile(const std::string& Name):CFile(Name)
	{

	}

	AmapSimParamFile::~AmapSimParamFile(void)
	{
		if(fic->is_open())
			fic->close();
		delete fic;
	}


	void AmapSimParamFile::open()
	{
		fic= new fstream (m_Name.c_str(), ios::in);
	}

	void AmapSimParamFile::open(ios_base::openmode _Mode)
	{
		fic= new fstream (m_Name.c_str(), _Mode);
	}

	int AmapSimParamFile::size()
	{

		// sauvegarder la position courante
		long pos = fic->tellg();
		// se placer en fin de fichier
		fic->seekg( 0 , std::ios_base::end );
		// r�cup�rer la nouvelle position = la taille du fichier
		long size = fic->tellg() ;
		// restaurer la position initiale du fichier
		fic->seekg( pos,  std::ios_base::beg ) ;

		return size ;

	}

	void AmapSimParamFile::write1DParameter( AmapSimVariable & var,float   conv)
	{
		int i;
		float f, g;

		Assert(fic->is_open());

		if (var.nb_pos_current < 2)
			var.interpolation_position_current = 0;

		*fic <<var.interpolation_position_current;
		*fic << " " <<var.nb_pos_current;
		for (i=0; i<var.nb_pos_current; i++)
		{
			f =  var.currentX[i]+(float)1.;
			g =  var.currentY[i] * conv;
			*fic << " " << f << " " << g;
		}
		*fic << "\n";

	}


	/* read a 2dimensional table */
	void AmapSimParamFile::read1DParameter(AmapSimVariable & var, float conv)
	{
		long  i;
		char lig[256];
		float valf, valY;

		Assert(fic->is_open());

		*fic>>var.interpolation_position_current; /* interpolation function */

		*fic>>var.nb_pos_current; /* number of described control points */

//		var.currentX.resize(var.nb_pos_current);

//		var.currentY.resize(var.nb_pos_current);

		for (i=0; i<var.nb_pos_current; i++)
		{
			*fic>>valf>>valY;

//			var.currentX[i]=(int)(valf-(float)1.);

			var.currentX.push_back((int)(valf-(float)1.));

			var.currentY.push_back(valY*conv);
		}

		var.ymin = (float)var.currentX[0];
		var.ymax = (float)var.currentX[var.nb_pos_current-1];

		var.currentPhyAge = var.currentPosition = -1;
		var.nb_PhyAge=0;

		fic->getline(lig,255);
	}


	/* reads a 3dimensional table */
	void AmapSimParamFile::read2DParameter(AmapSimVariable &var, float conv)
	{
		float val,val2,tmp;
		long  i, j;
		char lig[256];


		Assert(fic->is_open());

		*fic>>var.interpolation_PhyAge; /* first dimension interpolation functoin */
		*fic>>var.nb_PhyAge; /* number of control points in the first dimension */

		var.num_PhyAge.resize(var.nb_PhyAge);
		var.nb_pos.resize(var.nb_PhyAge);
		var.interpolation_position.resize(var.nb_PhyAge);
		var.x.resize(var.nb_PhyAge);
		var.y.resize(var.nb_PhyAge);

		for (i=0; i<var.nb_PhyAge; i++)
		{
			*fic>>val>>tmp; /* index into the first dimension */

			var.num_PhyAge[i] = (int)val-1;

			*fic>>var.interpolation_position[i]>>var.nb_pos[i]; /* interpolation and nb ctrl points on 2nd dimension */

//			var.x[i].resize(var.nb_pos[i]);

//			var.y[i].resize(var.nb_pos[i]);

			for (j=0; j<var.nb_pos[i]; j++)
			{
				*fic>>val>>val2;

				var.y[i].push_back (val2*conv);

				var.x[i].push_back ((int)val-1);
			}
		}

		var.currentPhyAge = var.currentPosition = -1;

		fic->getline(lig,255);
	}


	/* reads a 3dimensional table */
	void AmapSimParamFile::write2DParameter(AmapSimVariable &var, float conv)
	{
		long  i, j;


		Assert(fic->is_open());

		*fic<<var.interpolation_PhyAge<< " "; /* first dimension interpolation functoin */
		*fic<<var.nb_PhyAge; /* number of control points in the first dimension */

		for (i=0; i<var.nb_PhyAge; i++)
		{
			*fic<<" "<<var.num_PhyAge[i]+1<<" 0 "; /* index into the first dimension */

			*fic<<var.interpolation_position[i]<<" "<<var.nb_pos[i]; /* interpolation and nb ctrl points on 2nd dimension */

			for (j=0; j<var.nb_pos[i]; j++)
			{
				*fic<<" "<<var.x[i][j]+1<<" "<<var.y[i][j];

			}
		}

		*fic << endl;

	}


	/* red a single value */
	void AmapSimParamFile::readSingleValue (AmapSimVariable &var, /* variable to be read */long  ) /* integer or float value flag */
	{
		char lig[256];

		*fic>>var.ymin;

		var.nb_PhyAge=-1;

		fic->getline(lig,255);
	}


	/* this function reads a double value into a ASCII file */
	double AmapSimParamFile::readDouble ()
	{
		char line[256];
		double d = 0;

		fic->getline(line, 255);
#ifdef WIN32
		while (sscanf_s (line, "%lf", &d) < 1)
#else
		while (sscanf (line, "%lf", &d) < 1)
#endif
		{
			fic->getline(line, 255);
  			if ( (fic->rdstate() & std::ifstream::failbit ) != 0 )
				break;
		}

		return d;
	}




}; //end namespace
