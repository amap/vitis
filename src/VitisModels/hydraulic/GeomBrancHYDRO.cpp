#include "PlantHydraulic.h"
#include "GeomBrancHYDRO.h"
#include "InterNodeHYDRO.h"
#include "Bud.h"
#include "UtilMath.h"
#include "DebugNew.h"

GeomBrancHYDRO::GeomBrancHYDRO(Hierarc * brc, GeomBranc * gbr, Plant * gt):GeomBrancAMAP(brc,gbr,gt)
{
}

GeomBrancHYDRO::~GeomBrancHYDRO(void)
{
}

float GeomBrancHYDRO::computeDiamUp ()
{

	int i, phyAge;
	double l, v;
	double vol;

	BrancAMAP * b =(BrancAMAP *)this->getBranc();

	ParamHYDRO & vp_plt_h = ((PlantHYDRO *)b->getPlant())->getParamHydro();

	phyAge = b->getBud()->getPhyAge();



	if (   phyAge >= vp_plt_h.getMinBlade()
			&& phyAge <= vp_plt_h.getMaxBlade())
	{

		return (float)sqrt (((InterNodeHYDRO *)(b->getCurrentEntn()))->volume[BLADE]/vp_plt_h.getBladeThickness()) ;

	}
	else if (   phyAge >= vp_plt_h.getMinPetiole()
			&& phyAge <= vp_plt_h.getMaxPetiole())
	{
		//TODO verif si util
		b->startPosit();
		b->positOnFirstEntnWithinGu();
		vol = ((InterNodeHYDRO *)(b->getCurrentEntn()))->volume[PETIOLE] / b->getCurrentEntnNumberWithinGu();
		b->endPosit();

		return (float)diameter (vol,
				vp_plt_h.val_axe_complement (vp_plt_h.getshapeFactorA(), phyAge-1),
				vp_plt_h.val_axe_complement (vp_plt_h.getshapeFactorB(), phyAge-1));


	}
	else if (   phyAge >= vp_plt_h.getMinFruit()
			&& phyAge <= vp_plt_h.getMaxFruit())
	{
		/* assume it is a cylindar ???? 
		 * *d = diameter (((DonnComp*)(b->s[b->testedGuIndex].ss[b->testIndexWithinGu-1].donn[dataIndex]))->volume[FRUIT], 
		 *					val_axe_complementaire (&shapeFactorA, b->phyAge-1),
		 *					val_axe_complementaire (&shapeFactorB, b->phyAge-1));
		 * */
		/* assume it is a sphere */


		return (float)2. * pow (3. * ((InterNodeHYDRO *)(b->getCurrentEntn()))->volume[FRUIT] / M_PI /4., 1./3.);

	}
	else /* it is wood */
	{
		/* first compute length according to pith volume */


		l = (float)length (((InterNodeHYDRO *)(b->getCurrentEntn()))->volume[PITH], 
				vp_plt_h.val_axe_complement (vp_plt_h.getshapeFactorA(), phyAge-1),
				vp_plt_h.val_axe_complement (vp_plt_h.getshapeFactorB(), phyAge-1));
		/* compute total volume */
		v = 0; 
		for (i=0; i<((InterNodeHYDRO *)(b->getCurrentEntn()))->nbval; i++)
			v += ((InterNodeHYDRO *)(b->getCurrentEntn()))->volume[i];
		/* assume it is a circular cylinder */
		return (float)sqrt (4. * v / M_PI / l);

	}
	return 0;

}


float GeomBrancHYDRO::computeLength () /* returned length */
{
	int phyAge;

	double vol;


	BrancAMAP * b =(BrancAMAP *)this->getBranc();

	ParamHYDRO & vp_plt_h = ((PlantHYDRO *)b->getPlant())->getParamHydro();

	phyAge = b->getBud()->getPhyAge();

	if (   phyAge >= vp_plt_h.getMinBlade()
			&& phyAge <= vp_plt_h.getMaxBlade())
	{	

		return (float)sqrt (((InterNodeHYDRO *)(b->getCurrentEntn()))->volume[BLADE]/vp_plt_h.getBladeThickness()) ;

	}
	else if (   phyAge >= vp_plt_h.getMinPetiole()
			&& phyAge <= vp_plt_h.getMaxPetiole())
	{

		//TODO Verif utilit� du posit 
		b->startPosit();
		b->positOnFirstEntnWithinGu();
		vol = ((InterNodeHYDRO *)(b->getCurrentEntn()))->volume[PETIOLE] / b->getCurrentEntnNumberWithinGu();
		b->endPosit();


		return (float)length ( vol,
				vp_plt_h.val_axe_complement (vp_plt_h.getshapeFactorA(), phyAge-1),
				vp_plt_h.val_axe_complement (vp_plt_h.getshapeFactorB(), phyAge-1));



	}
	else if (   phyAge >= vp_plt_h.getMinFruit()
			&& phyAge <= vp_plt_h.getMaxFruit())
	{
		/* assume it is a cylindar ????
		 * *l = length (((DonnComp*)(b->s[b->testedGuIndex].ss[b->testIndexWithinGu-1].donn[dataIndex]))->volume[FRUIT], 
		 *					val_axe_complementaire (&shapeFactorA, b->phyAge-1),
		 *					val_axe_complementaire (&shapeFactorB, b->phyAge-1));
		 *	*/
		/* assumem it is a sphere */



		return (float)2. * pow (3. * ((InterNodeHYDRO *)(b->getCurrentEntn()))->volume[FRUIT] / M_PI /4., 1./3.);

	}
	else /* it is wood */
	{
		/* compute length according to pith volume */

		return (float)length (((InterNodeHYDRO *)(b->getCurrentEntn()))->volume[PITH], 
				vp_plt_h.val_axe_complement (vp_plt_h.getshapeFactorA(), phyAge-1),
				vp_plt_h.val_axe_complement (vp_plt_h.getshapeFactorB(), phyAge-1));


	}
	return 0;

}


double GeomBrancHYDRO::diameter (double volume, float ShapeA, float ShapeB)
{
	double d, s;

	s = pow (volume, 1.-ShapeB) / ShapeA;
	s = sqrt (s);
	/* assume it is a circular cylinder */
	d = sqrt (4. * s / M_PI);

	return d;
}

double GeomBrancHYDRO::length (double volume, float ShapeA, float ShapeB)
{
	double l;

	l = pow (volume, 1.+ShapeB) * ShapeA;
	l = sqrt (l);

	return l;
}
