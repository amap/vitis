#include "PlantHydraulic.h"
#include "InterNodeHYDRO.h"
#include "std_serialize.h"
#include "DebugNew.h"

InterNodeHYDRO::InterNodeHYDRO(Plant * plt):InterNodeAMAP(plt)
{	
	nbval = 1;
	volume.resize(2);
	volume[0] = volume[1] = 0;
	nbleaves = 0; /* number of leaves above that element */

	
}

InterNodeHYDRO::~InterNodeHYDRO()
{	
	
}

void InterNodeHYDRO::serialize(Archive& ar )
{		
	
		InterNodeAMAP::serialize(ar);
		
		if(ar.isWriting() )
		{   
			ar<<nbval<<nbleaves<<volume;
				
		}
		else
		{
			ar>>nbval>>nbleaves>>volume;
				
		}
	
		
}
