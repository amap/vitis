///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file  PlantHydraulic.h																					
///			\brief Definition of PlantHYDRO class.																	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __PLANTHYDRO_H__
#define __PLANTHYDRO_H__
#include "PlantAMAP.h"
#include "scheduler.h"
#include "ParamHYDRO.h"
#include "UtilMath.h"




/** @name some constant definition
 * */
/*@{*/
#define PITH 0
#define FRUIT 0
#define PETIOLE 0
#define BLADE 1
/*@}*/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class EventEndCycle																				
///			\brief EventData launch at end-cycle date
////////////////////////////////////////////////////////////////////////////////////////////////////
class EventEndCycle:public EventData
{
	public :

		EventEndCycle(){};

		~EventEndCycle() {};

};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class PlantHYDRO																				
///			\brief GreenLab V1 describtion for a plant , specialize PlantAMAP
///
///			The general sketch is :
///					- production (matter production)
///				 	- growth (organogenesys)
///				 	- allocation (volume increment)
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class HYDRO_EXPORT PlantHYDRO :
	public PlantAMAP
{
	double **pithSink;

	double **layerSink;

	double **petioleSink;

	double **bladeSink;

	double **fruitSink;

	
	 double totalMatter;
	 double cumultotalOrganMatter;
	 double cumultotalBladeMatter;
	 double cumultotalPetioleMatter;
	 double cumultotalPithMatter;
	 double cumultotalLayerMatter;
	 double cumultotalFruitMatter;
	 double totalOrganMatter;
	 double totalBladeMatter;
	 double totalPetioleMatter;
	 double totalPithMatter;
	 double totalLayerMatter;
	 double totalFruitMatter;
	 double Climate;
     float glMaxRef;

	 double height_USI;

	 double houppier_height_USI;

	 double coeffComp;
	 
	 /// \brief Total resisitivity of the plant for the cycle( sum of resisitivity of each leaf)
	 double	totalResistivity; 

	 /// \brief Last date of water consumption computing
	 double lastMatterTime;

	 /// \brief Water consumption since the last consumption computing
	 double waterConsumption; 


	 // Coordonnées polaires
	 double rho;

	 double theta;

	 //projection de dominance
	 double rsp;

	 /// \brief Pointer to the hydro parameters manager 
	 ParamHYDRO * param_H;

	

	public:


	 /// \brief Default Constructor
	 PlantHYDRO();
	
	/// \brief Constructor
	 PlantHYDRO(const std::string & nameT, const std::string & nameP);
	
	/// \brief Desstructor
	 ~PlantHYDRO(void);

	 void setClimate(double c);

	 inline void setCoeffComp(double coef) {coeffComp=coef;}
	
	 inline double getClimate() { return Climate;}

	 inline double getWaterConsumption () {return waterConsumption;}

	 inline double getRSP () { return rsp; }

	 inline double get_height_USI() { return height_USI;}

	 inline double get_houppier_height_USI() { return houppier_height_USI;}

	 inline double getPNC() { return exp(-(getLeafSurface() / ( GEOM_PI * getRSP() * getRSP() ) ) ); }

	 inline double getTheta () { return theta; }

	 inline double getLeafSurface () { return  totalBladeMatter/param_H->getBladeThickness();}

	 inline double getRho () { return rho; }

	 inline double getCoeffComp () { return coeffComp; }

	inline ParamHYDRO & getParamHydro() {return *param_H;}	

	/// \brief Set the position of the plant in the plantation and compute polar position
	virtual void setPosition(const Vector3 & pos);

	/// \brief Redifinition -> inscribe computeMatterCurrentTime action 
	void run (float timeStart=-1, float timeStop=-1);

	/// \brief Seed the plant at the given time
	virtual void seed (float time);

	virtual void init();

	/// \brief Launch by the scheduler at regular step time to launch the computation of matter production, organogenesys and volume incremen.
	virtual void process_event(EventData * msg);
	

	/// \brief Compute matter produced for current fraction cycle, with current Climate but with the same leafResistivty ( the same value along a cycle)
	void computeMatterCurrentTime () ;
	

	/// \brief compute demand for each organ 
	///
	/// The demand for each kind of functional organ (fruit, petiole, blade, pith, layer) is computed as the master sink of the organ multiply by the expansion law function value at physiological and chronological age of the organ.
	/// The demand for branches is computed as the sum of the demand for the layer associated to that branch (if any, see HasLayer function) and the demand of the pith.
	/// \return the total demand for the plant
	double computeDemand (float top);

	/// \brief split the current matter stack within organs of the plant and compute the global volume of each organ.
	///
	/// The matter is split according to proportionality between demands of each functional organs (pith, layers, petiole, blade and fruit).
	/// 
	/// \f$Q_Organ = totalMatter * demand_Organ / totalDemand\f$
	///
	/// There is a second step to compute te volume of rings (split matter of layers) from layer volumes.\n
	/// Every layer put its allocated matter into a global stack.
	/// \nEach Gu gets a quantity of matter from global layer matter stack that is proportional to the number of functioning leaves that are above it (according to topology).
	void computeVolume (float top, double totalDemand);


	/// \brief computes matter production of leaves
	///
	/// according to water use efficiency assumption the quantity of fresh matter produced by leaves is proportional to the quantity of transpired water. The plant is represented as a resistance network (branches, petiole, blades) where a potential gradient between soil and air is applied (i.e. base of the trunc and blades of leaves). Then a water flux can be computed using electric analogy. 
	/// \nAssuming that one can neglect the resistancy of branches compared to resistancy of petiole and blade, the network is solved like a set of resistances put in parallel.
	/// Each leaf has its resistancy computed as a set of two resitancy put in series, resistancy of the petiole is constant and resistancy of the blade is inverse proportional to its surface. 
	///
	/// \f$Rl = Rpetiole + resistivity*Thickness/Surface\f$
	/// 
	/// Under parallel resistancy assumption we have
	///
	/// 	\f$globalResistivity = \sum_{0}^n (1/Rl_i)\f$  where n the total number of leaves
	///
	/// The global matter production is then computed as
	/// 
	/// 	\f$Q = E * globalResistivity\f$
	/// 	
	/// Where E is the potential gradient between soil and air (we integrate in that variable everything that deals with climate and assume that every leaves are in the same conditions)
	double computeResistivity (float top);

	/// \brief computes normalized expansion law.
	///
	/// the expansion law is modelized as a beta law along the time. \n
	/// 	let t be the expansion time \n
	/// 	let n and p the beta law parameters \n
	/// 	then the value of expansion at integer time x will be :
	///\n		\f$expansion(x) = \frac{((x+.5)/t)^{(1+n)*p-1.} * (1-(x+.5)/t)^{(1+n)*(1-p)-1)}}t\f$
	///\n		the surface of the curve is then normalized (i.e. the total surface is brought to 1, \f$\int_0^texpansion(x)dx=1\f$)
	///\n		The expansion law is computed for for each posssible physiological age
	double ** normalizeExpansion (int phySz, AmapSimVariable & GrowthTime, AmapSimVariable & SinkAgingN, AmapSimVariable & SinkAgingP);

	virtual PlantBuilder * instanciatePlantBuilder();

	virtual void computeGeometry();

	/// \brief Serialize a PlantHYDRO
	/// \param ar A reference to the archive \see Archive
	virtual void serialize(Archive& ar );


};

#if defined HYDRO_EXPORTS
/// \return A instance of PlantHYDRO
extern "C" HYDRO_EXPORT  Plant * StartPlugin (void);

#endif

#endif
