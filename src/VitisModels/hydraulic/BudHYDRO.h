///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file  BudHYDRO.h																					
///			\brief Definition of BudHYDRO class.																	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __BUDHYDRO_H__
#define __BUDHYDRO_H__

#include "Bud.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class BudHYDRO																				
///			\brief Specialization of Bud by adding method to compute upper leaves seen by each internode of hydraulic data
///	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class BudHYDRO :
	public Bud
{
public:

		
	BudHYDRO(BrancAMAP * br, char guRamificationType, double timeC ,long phyAgeBeg, double ryt);

	virtual ~BudHYDRO(void);

	virtual void updateHierar (Hierarc * hierarcPtr);

	virtual void updatePruning();


	/// \brief set up functioning leaves counter that is maintained for each node of the plant
 	void adjustUpperLeavesNumber (int increment);

	/// \brief check if the branch associated to the bud bears layers
	///
	/// An axe bears a layer if it consists of more than one GU and bears functioning leaves
	/// or if it bears only axes consisting in one GU bearing functioning leaves 
	/// \return \li the last Gu of the branch if success
	/// \li NULL if no layer
	GrowthUnitAMAP * HasLayer ();

	/// \brief Seek for functioning leaves
	/// \return 1 if b bears functioning leaves
	int seekForFunctioningLeaves ();

};

#endif
