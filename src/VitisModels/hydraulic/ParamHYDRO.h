#ifndef _PARAM_HYDRO_H_
#define _PARAM_HYDRO_H_
#include "externHydro.h"
#include "AmapSimParamFile.h"
using namespace Vitis;

class HYDRO_EXPORT ParamHYDRO :
	public AmapSimParamFile
{
	 double MinBlade; /**<		minimum phy age for Blade */
	 double MaxBlade; /**<		maximum phy age for Blade */
	 double MinPetiole; /**<	minimum phy age for Petiole */
	 double MaxPetiole; /**<	maximum phy age for Petiole */
	 double MinFruit; /**<		minimum phy age for Fruits */
	 double MaxFruit; /**<		maximum phy age for Fruits */
	 double sowYear; /**<		sowing year */
	 double sowDay; /**<		sowing julean day */
	 AmapSimVariable pithMasterSink; /**<       Pith master sink */
	 AmapSimVariable pithSinkAgingN; /**<       Pith sink time evolution beta law first parameter */
	 AmapSimVariable pithSinkAgingP; /**<       Pith sink time evolution beta law second parameter */
	 
	 AmapSimVariable layerMasterSink; /**<       Layer master sink */
	 AmapSimVariable layerSinkAgingN; /**<       Layer sink time evolution beta law first parameter */
	 AmapSimVariable layerSinkAgingP; /**<       Layer sink time evolution beta law second parameter */
	 
	 AmapSimVariable petioleMasterSink; /**<       Petiole master sink */
	 AmapSimVariable petioleSinkAgingN; /**<       Petiole sink time evolution beta law first parameter */
	 AmapSimVariable petioleSinkAgingP; /**<       Petiole sink time evolution beta law second parameter */
	 
	 AmapSimVariable bladeMasterSink; /**<       Blade master sink */
	 AmapSimVariable bladeSinkAgingN; /**<       Blade sink time evolution beta law first parameter */
	 AmapSimVariable bladeSinkAgingP; /**<       Blade sink time evolution beta law second parameter */
	 
	 AmapSimVariable fruitMasterSink; /**<       Fruit master sink */
	 AmapSimVariable fruitSinkAgingN; /**<       Fruit sink time evolution beta law first parameter */
	 AmapSimVariable fruitSinkAgingP; /**<       Fruit sink time evolution beta law second parameter */
	 
	 double SeedVolume; /**<			Seed volume */
	 double SeedTime; /**<			Seed diffusiom time */
	 AmapSimVariable bladeResistivity; /**< Blade resistivity */
	 AmapSimVariable petioleResistance; /**< Petiole resistivity */
	 AmapSimVariable GrowthTime; /**<        growth time */
	 AmapSimVariable FunctioningTime; /**<        functioning time */
	 AmapSimVariable shapeFactorA; /**<       Length/surface ratio */
	 AmapSimVariable shapeFactorB; /**<       Length factor */
	 double bladeThickness; /**<		   Blade thickness */
	 double ClimateParam; /**<  Param Climate */

	 /// \brief Rayon de la surface de projection au sol du houppier
	 double v_rsp;

public:
	ParamHYDRO(const std::string & paramFileN);

	virtual ~ParamHYDRO(void);

	void readParam ();
	
	void writeParam();
	
	float val_axe_complement(AmapSimVariable & var, int pos);

	/// \name Read accessor
	//@{

	    inline AmapSimVariable & getPithMasterSink() { return pithMasterSink;}
		inline AmapSimVariable & getPithSinkAgingN() { return pithSinkAgingN;}
		inline AmapSimVariable & getPithSinkAgingP() { return pithSinkAgingP;}

	    inline AmapSimVariable & getLayerMasterSink() { return layerMasterSink;}
		inline AmapSimVariable & getLayerSinkAgingN() { return layerSinkAgingP;}
		inline AmapSimVariable & getLayerSinkAgingP() { return layerSinkAgingP;}

	    inline AmapSimVariable & getPetioleMasterSink() { return petioleMasterSink;}
		inline AmapSimVariable & getPetioleSinkAgingN() { return petioleSinkAgingP;}
		inline AmapSimVariable & getPetioleSinkAgingP() { return petioleSinkAgingP;}

	    inline AmapSimVariable & getBladeMasterSink() { return bladeMasterSink;}
		inline AmapSimVariable & getBladeSinkAgingN() { return bladeSinkAgingP;}
		inline AmapSimVariable & getBladeSinkAgingP() { return bladeSinkAgingP;}


	    inline AmapSimVariable & getFruitMasterSink() { return fruitMasterSink;}
		inline AmapSimVariable & getFruitSinkAgingN() { return fruitSinkAgingP;}
		inline AmapSimVariable & getFruitSinkAgingP() { return fruitSinkAgingP;}

	    inline AmapSimVariable & getBladeResistivity() { return bladeResistivity;}
		
	    inline AmapSimVariable & getPetioleResistance() { return petioleResistance;}
	    inline AmapSimVariable & getGrowthTime() { return GrowthTime;}
	    inline AmapSimVariable & getFunctioningTime() { return FunctioningTime;}
	    inline AmapSimVariable & getshapeFactorA() { return shapeFactorA;}
	    inline AmapSimVariable & getshapeFactorB() { return shapeFactorB;}

			
		inline double getBladeThickness() {return bladeThickness;}
		inline double getSeedVolume () {return SeedVolume;}
		inline double getSeedTime () {return SeedTime;}

		inline double getMinBlade() {return MinBlade;}
		inline double getMaxBlade () {return MaxBlade;}
		inline double getMinPetiole () {return MinPetiole;}
		inline double getMaxPetiole () {return MaxPetiole;}
		inline double getMinFruit () {return MinFruit;}
		inline double getMaxFruit () {return MaxFruit;}

		inline double getRSP () {return v_rsp;}
		
		inline double getClimateParam () {return ClimateParam;}


	//@}

  /// \name Write accessor
  //@{

	  
	   inline void setBladeThickness(double b) {bladeThickness=b;}
	   inline void setSeedVolume (double sv) {SeedVolume=sv;}
	   inline void setSeedTime (double st) {SeedTime=st;}
	   inline void setClimateParam(double c) {ClimateParam=c;}


	 


  //@}
};


#endif