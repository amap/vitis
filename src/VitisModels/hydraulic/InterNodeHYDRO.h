///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file  InterNodeHYDRO.h																					
///			\brief Definition of InterNodeHYDRO class.																	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __INTERNODEHYDRO_H__
#define __INTERNODEHYDRO_H__

#include "InterNodeAMAP.h"
#include "externDll.h"
#include "externHydro.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class InterNodeHYDRO																				
///			\brief Specialization of InterNodeAMAP by adding of hydraulic data
///	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class HYDRO_EXPORT InterNodeHYDRO :
	public InterNodeAMAP
{
public:

	
	int nbval; /**< Number of values in the next area */
	
	std::vector<double> volume; 	/**< In case of a leaf, 
					 						* since I don't know if petiole and blade will be describded
					 						* in different phy age (composed leaf) or not,
					 						* I lonely decided to allways put blade volume into 
					 						* the second field and petiole volume into the first
					 						* */
											/**< In case of a node,
											 * first value is for the pith and next for layers
											 * */
	int nbleaves; /**< Number of leaves above that element */

	/// \brief Constructor
	InterNodeHYDRO(class Plant * plt);

	/// \brief Destructor
	virtual ~InterNodeHYDRO(void);

	/// \brief Serialize a InterNodeHYDRO
	/// \param ar A reference to the archive \see Archive 
	virtual void serialize(Archive& ar );
};

#endif