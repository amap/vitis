#include "HydraulicMod.h"
#include "GeomBrancHYDRO.h"
#include "InterNodeHYDRO.h"
#include "PlantHydraulic.h"
#include "BudHYDRO.h"
#include "DebugNew.h"

HydraulicMod::HydraulicMod(Plant * plt):AmapSimModele(plt)
{
}

HydraulicMod::~HydraulicMod(void)
{
}


GeomBranc * HydraulicMod::createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr)
{
	return new GeomBrancHYDRO(brc,gbr,v_plt);

}

DecompAxeLevel * HydraulicMod::createInstanceDecompAxe (USint level)
{
	switch(level)
	{
		case INTERNODE : return new InterNodeHYDRO (v_plt); break;



	}
	return AmapSimModele::createInstanceDecompAxe(level);
}


Bud * HydraulicMod::createInstanceBud (class BrancAMAP * br, char guRam, double timeC, long phyAgeBeg, double ryt)
{

	return new BudHYDRO(br,guRam,timeC,phyAgeBeg, ryt);

}


GeomElem * HydraulicMod::createInstanceGeomElem ()
{
	return new GeomElemHYDRO ();
}




