#ifndef __EXPORTHYDRO_H__
#define __EXPORTHYDRO_H__
#include "externAmapSim.h"
	
#pragma warning ( disable : 4251 )

#if !defined STATIC_LIB
	#if defined WIN32 

			#ifdef HYDRO_EXPORTS
				#define HYDRO_EXPORT __declspec(dllexport)
			#else
				#define HYDRO_EXPORT __declspec(dllimport)
			#endif			
	#else 
			#define HYDRO_EXPORT
			
	#endif

#else 
		#define HYDRO_EXPORT
		
#endif


#endif
