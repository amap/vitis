///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\file GeomBrancHYDRO.h																					
///			\brief Definition of GeomElemHYDRO and GeomBrancHYDRO class.																	
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __GEOMBRANCHYDRO_H__
#define __GEOMBRANCHYDRO_H__

#include "GeomBrancAMAP.h"
#include "GeomElemCone.h"
#include "externHydro.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class GeomElemHYDRO																				
///			\brief Inherit from GeomElemCone and Subject 
///				
///			Launch a signal to observers when a new geometrical element is created 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class HYDRO_EXPORT GeomElemHYDRO : public GeomElemCone
{
public:

	GeomElemHYDRO()	{};

	~GeomElemHYDRO(){};
	
};



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///																											
///			\class GeomBrancHYDRO																				
///			\brief Specialization of GeomBrancAMAP by adding of method to compute Diameter and length according allocated matter
///				
///																											
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GeomBrancHYDRO :
	public GeomBrancAMAP
{
public:
	
	GeomBrancHYDRO(Hierarc * brc, GeomBranc * gbr, class Plant * gt=NULL);

	virtual ~GeomBrancHYDRO(void);
	
	/// \brief Specialized the standard function AMAPSIM computing the internode's diameter
	/// \return values : \li 0 : nothing was done \li 1 : a new diameter was computed
	virtual float computeDiamUp();

	/// \brief Specialized the standard function AMAPSIM computing the internode's length
	/// \return values : \li 0 : nothing was done \li 1 : a new length was computed
	virtual float computeLength ();


	double diameter (double volume, float ShapeA, float ShapeB);

	double length (double volume, float ShapeA, float ShapeB);

	
};

#endif