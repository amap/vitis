/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  RootSystem.h
///			\brief Definition of RootSystem class.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WHOLEPLANT_H__
#define __WHOLEPLANT_H__

#include "externWholePlant.h"
#include "RootSystem.h"
#include "PlantAMAP.h"
#include "BrancAMAP.h"
#include "VitisCoreSignalInterface.h"

class WholePlant;

struct WholePlantGeom : public subscriber<messageVitisBeginGeomCompute> {
public:
    WholePlant *wholePlant;

    WholePlantGeom(void *p);

    virtual ~WholePlantGeom();

    void on_notify(const state &st, paramMsg *data = NULL);
};

struct WholePlantendGeom : public subscriber<messageVitisEndGeomCompute> {
public:
    WholePlant *wholePlant;

    WholePlantendGeom(void *p);

    virtual ~WholePlantendGeom();

    void on_notify(const state &st, paramMsg *data = NULL);
};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class RootSystem
///			\brief AMAP describtion for a plant , specialize Plant with adding a pointer to an axis reference
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class WHOLEPLANT_EXPORT WholePlant : public Plant {
    /// \brief Pointer to the axis reference

    int nbType;
    int maxRamif;
    int *maxReit;
    WholePlantGeom *theGeom;
    WholePlantendGeom *theEndGeom;

public:


    /// \brief Default constructor
    WholePlant();

    /// \brief Constructor
    WholePlant (void *p);

    /// \brief Constructor
    WholePlant(const std::string &nameT, const std::string &nameP);

    /// \brief Destructor
    virtual ~WholePlant(void);

    /// \brief Seed the plant  at the given time
    virtual void seedPlant(float time);


    virtual void init();

    /// \brief  init the current instance of plant builder
    virtual PlantBuilder *instanciatePlantBuilder();

    virtual void serialize(Archive &ar);

};

#if defined WHOLEPLANT_EXPORTS
/// \brief C function , called by the "Load" function of SimulationModel in the Vitis framework to get the simulation modele
/// \return A instance of AmapSimModele
extern "C" WHOLEPLANT_EXPORT  Plant * StartPlugin (void *p);

#endif


#endif

