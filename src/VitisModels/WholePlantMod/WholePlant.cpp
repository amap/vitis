/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "WholePlant.h"
#include "SimplifTopoWholePlant.h"
#include "WholePlantMod.h"

WholePlantGeom::WholePlantGeom(void *p) {
    wholePlant = reinterpret_cast<WholePlant *>(p);
    subscribe(p);
}

WholePlantendGeom::WholePlantendGeom(void *p) {
    wholePlant = reinterpret_cast<WholePlant *>(p);
    subscribe(p);
}

WholePlantGeom::~WholePlantGeom() {
    unsubscribe();
}

WholePlantendGeom::~WholePlantendGeom() {
    unsubscribe();
}

void WholePlantGeom::on_notify(const state &, paramMsg *data) {
    Hierarc *hierc = ((RootSystem*)wholePlant)->getTopology().getTrunc("DigRMod");

    Root *p = (Root *) hierc->getBranc();
    // if(p->theBud->lastGrowthTime <= Scheduler::instance()->getTopClock())

    p->growTo(Scheduler::instance()->getTopClock());

}

void WholePlantendGeom::on_notify(const state &, paramMsg *data) {
    ((RootSystem*)wholePlant)->getGeometry().HasToBeComputed(true);
}

WholePlant::WholePlant() {
    maxReit = NULL;
    theGeom = new WholePlantGeom(this);
    theEndGeom = new WholePlantendGeom(this);

   	v_topoMan->setTopoSimplifier(new SimplifTopoWholePlant(this));

}

WholePlant::WholePlant(void *p) {
    maxReit = NULL;
    theGeom = new WholePlantGeom(this);
    theEndGeom = new WholePlantendGeom(this);

   	v_topoMan->setTopoSimplifier(new SimplifTopoWholePlant(this));

}

WholePlant::WholePlant(const std::string &nameT, const std::string &nameP) {
    maxReit = NULL;
    theGeom = new WholePlantGeom(this);
    theEndGeom = new WholePlantendGeom(this);

   	v_topoMan->setTopoSimplifier(new SimplifTopoWholePlant(this));

}


WholePlant::~WholePlant(void) {
    maxReit = 0;
    if (theGeom) {
        delete theGeom;
        theGeom = 0;
    }
    if (theEndGeom) {
        delete theEndGeom;
        theEndGeom = 0;
    }
}


void WholePlant::init() {
	Plant::init();

   	this->getPlantFactory()->setLevelOfDecomposition(std::string("AMAPSimMod"), 5);
   	this->getPlantFactory()->setLevelOfDecomposition(std::string("DigRMod"), 1);

	if(getParamData(string("AMAPSimMod"))==NULL)
		setParamData(string("AMAPSimMod"), new AxisReference(getParamName(string("AMAPSimMod"))));

//    getPlantFactory()->setLevelOfDecomposition(1);
    if (getParamData(string("DigRMod")) == NULL) {
        setParamData(string("DigRMod"), new ParamSetDigR(getParamName(string("DigRMod"))));
    }

    getConfigData().simuTotalTime = Scheduler::instance()->getHotStop();

    ParamSetDigR *paramSet = (ParamSetDigR*)getParamData(string("DigRMod"));
    nbType = paramSet->getParameter("nbType")->value();

    maxRamif = paramSet->getParameter("maxRamif")->value();
    maxReit = new int[nbType];
    for (int i = 0; i < nbType; i++)
        maxReit[i] = paramSet->getParameter("MaxReit", i)->value();

}


void WholePlant::seedPlant(float time) {
	Plant::seedPlant (time);
    BrancAMAP *b;
    Root *r;
	AxisReference *axisref = (AxisReference*)getParamData(string("AMAPSimMod"));
	if (v_actionScheduler->getHotStop() <= (*axisref)[glmaxcalc].ymin)
	{

        b = (BrancAMAP *)v_pltBuilder->createInstanceBranc("AMAPSimMod");

        b->set(MainBranch, 0, (*axisref)[glPhyAgeune].ymin);
        GrowthUnitAMAP *gu = new GrowthUnitAMAP(this);
        gu->set (0, 0.0000001);
        b->addGu(gu);
        b->addCycle();
        b->addZone();
        b->addIntn(new InterNodeAMAP(this));

        v_topoMan->fork(b, NULL);

        b->addBud((Bud*)v_pltBuilder->createInstanceBud(5, b, (int)RETARDE1, 0 , (*axisref)[glPhyAgeune].ymin, 1));

        //bvit->getBud()->setTimeAndPriority(bvit->getBud()->timeForATest,10);

        v_actionScheduler->create_process(b->getBud(),NULL,b->getBud()->timeForATest+time, 10);
	}

    ParamSetDigR *paramSet = (ParamSetDigR*)getParamData(string("DigRMod"));
    if (v_actionScheduler->getHotStop() <= paramSet->getParameter("maxAge")->value()) {

        r = (Root *) v_pltBuilder->createInstanceBranc("DigRMod");

        r->set(0, 0);

        v_topoMan->fork(r, b->getCurrentHierarc());

        r->addBud((RootBud*)v_pltBuilder->createInstanceBud(3, r, 0, 0));

        //bvit->getBud()->setTimeAndPriority(bvit->getBud()->timeForATest,10);

        v_actionScheduler->create_process(r->getBud(), NULL, 0, 10);
    }
}


PlantBuilder *WholePlant::instanciatePlantBuilder() {
    return new WholePlantModele(this);
}


void WholePlant::serialize(Archive &ar) {

/*
    Plant::serialize(ar);

    if (ar.isWriting()) {
        ar << nbType << maxRamif;
        for (int i = 0; i < nbType; i++)
            ar << maxReit[i];
    } else {
        ar >> nbType >> maxRamif;
        maxReit = new int[nbType];
        for (int i = 0; i < nbType; i++)
            ar >> maxReit[i];
    }
*/
}


#if !defined STATIC_LIB
extern "C" Plant *StartPlugin(void *p) {
    return new WholePlant(p);
}
#endif

