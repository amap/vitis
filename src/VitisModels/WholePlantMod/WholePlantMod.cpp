/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/


#include <cstdarg>
#include "WholePlantMod.h"
#include "defROOT.h"
#include "RootSystem.h"
#include "GeomRoot.h"
#include "Segment.h"

#include "PlantAMAP.h"
#include "GeomBrancAMAP.h"
#include "CycleAMAP.h"
#include "defAMAP.h"



WholePlantModele::WholePlantModele(Plant * plt) : PlantBuilder(plt)
{
}



WholePlantModele::~WholePlantModele()
{

}


GeomBranc * WholePlantModele::createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr)
{
    Branc* b = (Branc*)brc->brancPtr;

//    if (   gbr == NULL
//        || dynamic_cast<BrancAMAP*>( brc ))
    if (b->getClassType() == "AMAPSimMod")
    {
        return new GeomBrancAMAP(brc,gbr,V_plt());
    }
    else if (b->getClassType() == "DigRMod")
    {
        return new GeomRoot(brc,gbr,V_plt());
    }
    else
    {
        cout << "Unknown " << b->getClassType() << " branc class type"<< endl;
        return NULL;
    }
}

Branc * WholePlantModele::createInstanceBranc (char *type)
{
    if (strcmp(type, "AMAPSimMod") == 0)
        return new BrancAMAP(V_plt());
    else
        return new Root(V_plt());
}

DecompAxeLevel * WholePlantModele::createInstanceDecompAxe (USint level, char *type)
{
    if (strcmp(type, "DigRMod") == 0)
    {
        switch(level)
        {
            case SEGMENT : return new Segment (V_plt()); break;
            default : return new DecompAxeLevel(level);break;
        }
    }
	else
	{
        switch(level)
        {
            case GROWTHUNIT : return new GrowthUnitAMAP (V_plt()); break;
            case INTERNODE : return new InterNodeAMAP (V_plt()); break;
            case GROWTHCYCLE : return new CycleAMAP (V_plt()); break;
            default : return new DecompAxeLevel(level);break;
        }
	}
}

VProcess * WholePlantModele::createInstanceBud ( int n, ...)
{
    if (n == 3)
    {
        Root *br;
        double timeC;
        int phyAgeBeg;
        int num;
        va_list arguments;                     // A place to store the list of arguments

        va_start ( arguments, n ); // Initializing arguments to store all values after n

        br = va_arg ( arguments, Root *);
        timeC = va_arg (arguments, double);
        phyAgeBeg = va_arg (arguments, int);

        va_end ( arguments );                  // Cleans up the list

        return new RootBud( br, timeC, phyAgeBeg);
    }
    else
    {
        BrancAMAP *br;
        char guRam;
        double timeC;
        double phyAgeBeg;
        double ryt;
        int num;
        va_list arguments;                     // A place to store the list of arguments

        va_start ( arguments, n ); // Initializing arguments to store all values after n

        br = va_arg ( arguments, BrancAMAP *);
        guRam = (char)va_arg ( arguments, int);
        timeC = va_arg (arguments, double);
        phyAgeBeg = va_arg (arguments, double);
        ryt = va_arg (arguments, double);

        va_end ( arguments );                  // Cleans up the list

        return new Bud( br, guRam, timeC, phyAgeBeg,ryt);
    }
}


