/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file Root.h
///			\brief Definition of Root class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _ROOT_H
#define _ROOT_H

#include "RootBud.h"
#include "Branc.h"
#include "externDigR.h"
#include "Segment.h"
#include "Geometry/EllipticConeCylinderConstraint.h"
#include "Geometry/ConstrainedRoot.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class Root
///			\brief Describe an axe of the root system (friend with Bud class)
///
///			Representation of an axe according DIGR formalism. The structure of this axe is updated by his Bud \n
///			A root is composed in one decomposition level : SEGMENT, there is one segment between two ramifications
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

class RootSystem;

class DIGR_EXPORT Root : public Branc {

    friend class RootBud;


public:

    /**
     * grow this root and all its children to the given age
     * @param age
     */
    void growTo(double age);

    /// \brief Pointer to the Bud
    RootBud *theBud;


    /// \brief Constructor
    /// \param idxPlt Current index of the mother plant into forest
    Root(class Plant *plt);

    /// \brief Copy constructor
    Root(Root *b);

    /// \brief Destructor
    virtual ~Root();

    /**
     * compute the constraint for this Root.
     */
//    void caclulateConstraint();

    /// \brief Set the branch kind and his lateral production index into axis reference
    void set(int nature, int curReit);

    void set(int nature, int curReit, const std::string &c);

    /// \brief Add the given bud to this axe
    void addBud(RootBud *b);

    /// \brief Add the given inter-node to the end of the last zone
    void addSegment(Segment *s, int _index = 0);


    /// @name Member accessor
    //@{

    /// \return the Root bearer
    Root *getBearer();

    /// \return the Bud
    RootBud *getBud();

    /// \return the current internode
    Segment *getCurrentSegment();

    int Type() { return type; };

    //@}


    /// @name Decomposition level index accessor
    /// \see DecompAxeLevel.h
    //@{

    /// \return the current Growth unit number into the axe
    int getSegmentNumber();

    /// \return the current Growth unit index into the axe
    int getCurrentSegmentIndex();

    //@}



    /// \name Seek method into the axe
    //@{


    /// \brief Positioning on the first internode of the axe
    USint positOnFirstSegmentWithinRoot();

    /// \brief Positioning on the last internode of the axe
    USint positOnLastSegmentWithinRoot();

    /// \brief Positioning on the current next internode of the axe
    USint positOnNextSegmentWithinRoot();

    /// \brief Positioning on the current previous internode of the axe
    USint positOnPreviousSegmentWithinRoot();

    USint positOnSegmentWithinRootAt(USint pos);
    //@}


    /// \brief computes current position according to position rules
    /// \param senscompt BASAL, APICAL
    /// \param basecompt ENTN , TEST
    /// \param typetop AXE, Gu, CYCLE
    /// \return The current position
    long calcPosElem(long senscompt);

    /// \brief Function called when a hierarc axe linked to this branc is pruned
    virtual void updatePruning();

    /// \brief Fucntion called when a hierarc axe linked to this branc is added
    virtual void updateTopo(class Hierarc *hierarcPtr);

    float computeInitialDiameter();

    double Length() { return length; };

    void Length(double l) { length = l; };

    int CurReit() { return curReit; };

    void CurReit(int c) { curReit = c; };

    /// \brief Serialize a Root
    /// \param ar A reference to the archive \see Archive
    virtual void serialize(Archive &ar);

//    EllipticConeCylinderConstraint *getConstraint() const;
//
//    void setConstraint(EllipticConeCylinderConstraint *constraint);
//
    /// this was added to solve the phylotaxiy problem, its contain the lastGrowthPosition, because theBud->lastGrowthPosition keeps changing.
    float getOffset() const;

    void setOffset(float offset);

private :
    /** @name Index and number for decomposition levels
      Current indexes and number into decomposition levels of this axe */
    //@{
    //@}

    /// \brief Root type
    USint type;

    double length;

    int curReit;

    float offset;

    /// \brief pointer to EllipticConeCylinderConstraint, if null then there is no constraint for this Root.
    /// the computation begin geomRoot class.
//    EllipticConeCylinderConstraint *constraint;
};


#endif

