/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

// Root.cpp: implementation of the Root class.
//
//////////////////////////////////////////////////////////////////////
#include <string>
#include "randomFct.h"
#include "defROOT.h"
#include "Root.h"
#include "Plant.h"
#include "RootBud.h"
#include <iostream>
#include <math.h>
#include "Vector.h"


//using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


Root::Root(Plant * plt):Branc(plt)
{

	theBud= nullptr;

	CurrentHierarcPtr(0);

	length = 0;
	type = -1;
	curReit = 0;
//    constraint = 0;
	offset = 0;

	setClassType("DigRMod");

}

//EllipticConeCylinderConstraint *Root::getConstraint() const {
//	return constraint;
//}
//
//void Root::setConstraint(EllipticConeCylinderConstraint *constraint) {
//	Root::constraint = constraint;
//}
//
//Constructeur par copie
Root::Root(Root * b):Branc(b)
{

	type= static_cast<unsigned short>(b->Type());

	theBud= nullptr;

	length = b->Length();
	curReit = b->curReit;

//    constraint = 0;

	setClassType("DigRMod");

}



Root::~Root()
{
//	getPlant()->getTopology().removeInstanceBrancSimplified(this, critere);
		delete theBud;
//		delete constraint;
}


void Root::set(int nature, int curReit, const std::string &c)
{
	setCritere(c);

	this->type = (USint)nature;
	this->curReit = curReit;
}

void Root::set(int nature, int curReit)
{
	this->type = (USint)nature;
	this->curReit = curReit;
}



void Root::updateTopo (Hierarc * hierarcPtr)
{
	Branc::updateTopo(hierarcPtr);

	if(theBud != nullptr)//On est sur la premiere instance de cet axe
		theBud->updateHierar(hierarcPtr);

}

void Root::updatePruning()
{
	theBud->updatePruning();
}

long Root::calcPosElem (long senscompt)
{
	long position;

	if (senscompt == BASAL)
		position = getCurrentSegmentIndex();
	else
		position = getSegmentNumber() - (getCurrentSegmentIndex()+1);

	return position;
}

void Root::addSegment (Segment * s, int _index)
{
	addSubElementAtLevel(s, _index);
	//length += s->Length();
}

void Root::addBud (RootBud * b)
{
	//Cr�ation du Bud associ� !
	theBud=b;
}



RootBud * Root::getBud()
{
	return this->theBud;
}


Root * Root::getBearer ()
{
	return (Root *)CurrentHierarcPtr()->getBearer()->getBranc();
}


int Root::getSegmentNumber ()
{
	return getElementNumberOfSubLevel(SEGMENT);

}

int Root::getCurrentSegmentIndex ()
{
	return getCurrentElementIndexOfSubLevel(SEGMENT);
}

Segment *  Root::getCurrentSegment ()
{
	return (Segment *)getCurrentElementOfSubLevel(SEGMENT);
}

USint Root::positOnFirstSegmentWithinRoot()
{
	return positOnFirstElementOfSubLevel(SEGMENT);
}

USint Root::positOnLastSegmentWithinRoot()
{
	return positOnLastElementOfSubLevel(SEGMENT);
}


USint Root::positOnNextSegmentWithinRoot()
{
	return positOnNextElementOfSubLevel(SEGMENT);
}

USint Root::positOnPreviousSegmentWithinRoot()
{
	return positOnPreviousElementOfSubLevel(SEGMENT);
}

USint Root::positOnSegmentWithinRootAt(USint pos)
{
	return positOnElementAtSubLevel(pos, SEGMENT);
}

/* compute initial parameter value for length and diameter */
float Root::computeInitialDiameter ()
{
	ParamSet *param = getBud()->paramSet;
	float val = param->getParameter("InitialDiameter",Type())->value(this, this->getPlant());
	double stdDev = param->getParameter("InitialDiameterStdDev",Type())->value(this, this->getPlant());
	val += ((Plant*)getPlant())->getConfigData().getRandomGenerator()->nextGaussian () * stdDev;

	return val;
}

void Root::growTo(double age){
    if (!theBud->death)
        theBud->groEngine->growTo(age);

    if (theBud->death)
        return;

    Hierarc* h = getCurrentHierarc();
    if(h == nullptr)
        return;

    for(int i = 0 ; i < h->getNbBorne() ; i++){
        Root* r = (Root*)h->getBorne(i);
        if(r)
        {
            r->growTo(age);
            if (r->getBud()->toBePruned)
            {
                getBud()->clearEvents (r->getBud());
                getPlant()->getTopology().removeBranc(r, 1);
            }
            if (i >= h->getNbBorne())
                break;
        }
    }
}


void Root::serialize(Archive& ar )
{
	Branc::serialize(ar);

	if(ar.isWriting() )
	{
		ar<<type<<length<<curReit;

		//Save the eventData mechanism link to this branc
		if(this->SavePtr(ar,theBud))
		{
			ar<<*theBud;
		}


	}
	else
	{
		ar>>type>>length>>curReit;

		ArchivableObject * arBud;
		if(this->LoadPtr(ar,arBud))
		{
			theBud=new RootBud();

			this->AddPtr(ar,theBud);

			ar>>*theBud;
		}
		//else pas de else car c ici kon doit les charger ( et pas au nivo du sequenceur en chargant les process


		//TODO SERIAL pointeur sur topoMan;


	}

}

//void Root::caclulateConstraint() {
//    constraint = 0;
//    double d1 = theBud->paramSet->getParameter("BoundingVerticalConeAngle", type)->value();
//    double d2 = theBud->paramSet->getParameter("BoundingHorizontalConeAngle", type)->value();
//    double d3 = theBud->paramSet->getParameter("BoundingConeLength", type)->value();
//    if((d1 < 45)
//       || (d2 < 45)
//       || d3 < 9999){
//        Vector3 o(0, 0, 0);
//        Vector3 dir(0, 0, -1);
//        auto *c = new EllipticConeCylinder(o, dir, d1, d2, d3);
//        auto *constrainedRoot = new ConstrainedRoot(this);
//        constraint = new EllipticConeCylinderConstraint(constrainedRoot, c);
//    }
//
//}
//
float Root::getOffset() const {
    return offset;
}

void Root::setOffset(float offset) {
    Root::offset = offset;
}




