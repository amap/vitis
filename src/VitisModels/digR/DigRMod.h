/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file DigRMod.h
///			\brief Definition of DigRModele.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __DIGRMODELE_H__
#define __DIGRMODELE_H__
#include "PlantBuilder.h"
#include "GeomRoot.h"
#include "RootBud.h"




///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class DigRModele
///			\brief DIGR Edification root system model	(based on simplified Axis reference)
///
///			Gives methods to get simulation object's instance specialized by this dynamic model
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DIGR_EXPORT DigRModele: public PlantBuilder
{
	public:

		/// \brief Default constructor
		DigRModele(class Plant * plt);

		/// \brief Destructor
		~DigRModele();


		/// \brief Create a new geomtrical branch AMAP (Computed with axis reference data)
		/// @param brc The topological bearer
		/// @param gbr The geometrical bearer
		/// @param gt The geometrical manager
		///	@return An instance of GeomBrancAMAP
		virtual GeomBranc * createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr);

		/// \brief Create a new branch AMAP ( Branc specialization)
		/// @param hbear The topological bearer
		///	@return An instance of BrancAMAP
		virtual Branc * createInstanceBranc ();
		Branc * createInstanceBranc (char *t){return createInstanceBranc ();};

		/// \brief Create a new instance of an element of decomposition
		///
		///	Two level are specialized in this modele , level 1 (GROWTHUNIT) and level 5 (INTERNODE)
		/// @param level The level of the element to instanciate
		///	@return An instance of DecompAxeLevel, or GrowthUnitAMAP (if level==1), or InterNodeAMAP (if level==5)
		virtual DecompAxeLevel * createInstanceDecompAxe (USint level);

		/// \brief Create a new Bud (manage the growth and ramification)
		/// @param br The branc associated to the Bud
		/// @param guRam growth unit ramification type : ANTICIPE, RETARDE1, RETARDE2, TRAUMAANT, TRAUMARET, RETARDECYC, NONBRANCHETRAUMAANT, NONBRANCHEANT, NONBRANCHERETCYC, NONBRANCHETRAUMARET
		/// @param timeC Start time of the new bud
		/// @param rootTypeBeg RootType of the new bud
		/// @param ryt rythme of the new Bud
		///	@return An instance of Bud
		//virtual RootBud * createInstanceBud ( class Root * br, double timeC, int rootType);
        virtual VProcess * createInstanceBud ( int n, ...);





};



#endif
