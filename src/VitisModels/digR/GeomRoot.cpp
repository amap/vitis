/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "defROOT.h"
#include "RootSystem.h"
#include "GeomRoot.h"
#include "UtilMath.h"
#include "randomFct.h"
#include "RootBud.h"
#include "Geometry/BoundingBox3f.h"
#include "Geometry/EllipticConeCylinderConstraint.h"


GeomRoot::GeomRoot(Hierarc * brc,  GeomBranc * gbear, Plant * gt):GeomBrancCone(brc,gbear, gt)
{
    if (hierarc != nullptr)
    {
        paramSet=(ParamSetDigR *)brc->getBranc()->getPlant()->getParamData(string("DigRMod"));

        initialDiameter = static_cast<float>(1.);
        scale = 1;
        length = 0;
        currentPhyllotaxy = new float[((RootSystem*)(getBranc()->getPlant()))->NbType()];
        for (int i=0; i<((RootSystem*)(getBranc()->getPlant()))->NbType(); i++)
            currentPhyllotaxy[i] = 0;

        typeInsert = static_cast<short>(paramSet->getParameter("insertionType", ((Root *)brc->getBranc())->Type())->value(brc->getBranc(), brc->getBranc()->getPlant()));
        constraint = 0;

    }
}



GeomRoot::~GeomRoot() {
    if (constraint)
        delete constraint;
}

/* computes initial values of geometric parameters for a branch */
void GeomRoot::computeParamGeomInit ()
{
    double stdDev;
    Root *b = (Root *)this->hierarc->getBranc();
    Root *p = b->getBearer();

    if (   b != p
        && b->getClassType() == p->getClassType())
    {
        if (p->Type() == b->Type()) {
            insertionAngle = paramSet->getParameter("ReitAngle", b->Type())->value(b, b->getPlant());
        } else {
            insertionAngle = paramSet->getParameter("InsertionAngle", b->Type())->value(b, b->getPlant());
            stdDev = paramSet->getParameter("InsertionAngleStdDev", b->Type())->value(b, b->getPlant());
            insertionAngle += b->theBud->getConfigData().getRandomGenerator()->nextGaussian() * stdDev;
        }
    }
    else
    {
        insertionAngle = 180;
    }

    evolDiamStdDev = static_cast<float>((double)b->theBud->getConfigData().getRandomGenerator()->normalNoise(((ParametricParameter*)paramSet->getParameter("DiameterIncreaseRatioStdDev", b->Type()))->value (b, b->getPlant())));
    timeEvolDiamStdDev = static_cast<float>((double)b->theBud->getConfigData().getRandomGenerator()->normalNoise(((ParametricParameter*)paramSet->getParameter("DiameterIncreaseTimeStdDev", b->Type()))->value (b, b->getPlant())));
    delayEvolDiamStdDev = static_cast<float>((double)b->theBud->getConfigData().getRandomGenerator()->normalNoise(((ParametricParameter*)paramSet->getParameter("DiameterIncreaseDelayStdDev", b->Type()))->value (b, b->getPlant())));
}


void GeomRoot::determine_plagio (Matrix4 & plagMat, const Vector3 mat_ref, const Vector3 mat_plan)
{



    Vector3 proj;
    Vector3 perp;



    Vector3 np =mat_plan;

    Vector3 ref =mat_ref;

    proj=projection_plan(ref,np);

    proj.normalize();

    perp=Vector3::axisRotation(ref,proj,(float)GEOM_HALF_PI);

    plagMat.setSecondaryVector(proj.x(), proj.y(), proj.z());

    plagMat.setNormalVector(perp.x(), perp.y(), perp.z());

    plagMat.setMainVector(ref.x(), ref.y(), ref.z());


}


int GeomRoot::initGeomtry()
{

    //Variable destin� � sauvegarder la matrice de l'�l�ment porteur
    Matrix4   referenceTriedron;

    Vector3 transla;

    GeomElem *ge, *gprev;

    float angphy, angins;


    double rnd;
    rnd = ((Root*)hierarc->getBranc())->theBud->getConfigData().getRandomGenerator()->Random();
    rnd = ((Root*)hierarc->getBranc())->theBud->getConfigData().getRandomGenerator()->Random();
    rnd = ((Root*)hierarc->getBranc())->theBud->getConfigData().getRandomGenerator()->Random();

    Hierarc * p = hierarc->getBearer();
    if (p->getBranc() == NULL)
        p = hierarc;


    this->computeParamGeomInit ();

    this->computeInitialDiameter();

    ge=v_plt->getPlantFactory()->createInstanceGeomElem();

    ge->setPtrBrc(this);

    elemGeo.push_back(ge);

    //GeomElment of internode bearer
    if (hierarc == p)
    {
        gprev=ge;
    }
    else
    {
        //le dernier �l�ment du tableau d'entrenoeuds correspond � l'entrenoeud porteur de cette branche
        //Modif sg 23.07.07
        if (posOnBearer >= gbearer->getElementsGeo().size())
            posOnBearer = static_cast<int>((long)gbearer->getElementsGeo().size() - 1);
        gprev=gbearer->getElementsGeo().at(static_cast<unsigned long>((long)posOnBearer));
        referenceTriedron = gprev->getMatrix();
//		referenceTriedron = Matrix4::translation(referenceTriedron.getMainVector()*(float)gprev->getLength())*referenceTriedron;

    }

    ge->setMatrix(referenceTriedron);

    Matrix4 & currentTriedron = ge->getMatrix();

    transla=currentTriedron.getTranslation();

    currentTriedron.setTranslation(0,0,0);

    referenceTriedron.setTranslation(0,0,0);

    angins=insertionAngle;

    angphy = computeSecondDirection();

    currentTriedron=axeInsertionOn (currentTriedron,referenceTriedron,typeInsert,angins,angphy);

    currentTriedron.setTranslation(transla.x(), transla.y(), transla.z());

    if (hierarc != p)
    {
        if (typeInsert != INSER1)
        {
            if (angins != 0)
                currentTriedron = computeInsertTranslation(currentTriedron,gprev)*currentTriedron;
        }
        else
            currentTriedron = Matrix4::translation(referenceTriedron.getMainVector()*(float)gprev->getLength())*currentTriedron;
    }

    this->currentTrans=ge->getMatrix();



    if(hierarc!=p)
        gprev->getGeomChildren().push_back(this);

    if(computeSuperSimplif())
        return 0;

    return 1;
}


Matrix4 GeomRoot::axeInsertionOn(	Matrix4 & currentTriedron, /* current triedron  */
                                    Matrix4 & referenceTriedron, /* bearer triedron (initial one) */
                                    long typins,    /* insertion type (see above) */
                                    float angins,   /* insertopn angle */
                                    float angphy)   /* phyllotaxy angle */
{
    long   i;
    float  ins, phy, aux;
    Vector3 vec, vec2;
    Matrix4 matmp;
    Matrix4 gravity = Matrix4(Vector4(1,0,0,0),Vector4(0,1,0,0),Vector4(0,0,-1,0),Vector4(0,0,0,1));



    if (   fabs(referenceTriedron(0,2)) < .0001
            && fabs(referenceTriedron(1,2)) < .0001)
    {
        referenceTriedron(0,2) = (float).0001;
    }

    angins = angins * FGEOM_PI / 180;


    /* corection between 0 and 2*GEOM_PI */
    i = (long)(angins / FGEOM_TWO_PI);
    if (i != 0)
        angins = angins -FGEOM_TWO_PI*i;
    i = (long)(angphy / FGEOM_TWO_PI);
    if (i != 0)
        angphy = angphy - FGEOM_TWO_PI*i;

    switch (typins)
    {
    case ABSOLUTEPLAN:
        /* according to gravity */
        ins = angins;
        if (fabs(ins) < .00001)
            ins = (float).00001;
        phy = angphy;
        phy *= FGEOM_PI / 180;

        currentTriedron=Matrix4::axisRotation(referenceTriedron.getMainVector(),phy)*Matrix4::axisRotation(referenceTriedron.getSecondaryVector(),-ins)*currentTriedron;

        break;

    case RHIZOTAXY:
        /*  general case, adjust insertion position to the bearers diameter */
        ins = angins;
        phy=computeInsertAnglePhy(angphy);
        phy *= FGEOM_PI / 180;

        currentTriedron= Matrix4::axisRotation(referenceTriedron.getMainVector(),phy)
                        *Matrix4::axisRotation(referenceTriedron.getNormalVector(),-ins)
                        *currentTriedron;

        break;

    case BEARERPLAN:
        /*  plagiotropy  */
        ins = angins;
        phy = angphy;
        phy = phy * FGEOM_PI / 180;

        determine_plagio (currentTriedron, referenceTriedron.getMainVector(), gravity.getMainVector());

        currentTriedron=Matrix4::axisRotation(referenceTriedron.getMainVector(),phy)*Matrix4::axisRotation(referenceTriedron.getSecondaryVector(),-ins)*currentTriedron;

        determine_plagio (currentTriedron, currentTriedron.getMainVector(), gravity.getMainVector());
        break;

    default:
        /*  TO_VERTICAL default  */

        ins = angins;
        phy = angphy;

        phy = phy * FGEOM_PI / 180;

        currentTriedron = gravity * Matrix4::axisRotation(referenceTriedron.getMainVector(),phy);
        break;
    }

    return currentTriedron;
}


float GeomRoot::computeSecondDirection()
{
    float phy;
    Root * b =(Root *)this->hierarc->getBranc();

    if (b != b->getBearer())
    {
        if (b->Type() != b->getBearer()->Type()) {
            phy = paramSet->getParameter("Reitrhizotaxy", b->Type())->value(b, b->getPlant());
        }else {
            ParametricParameter *foo = (ParametricParameter *) paramSet->getParameter("phyllotaxyAngle", b->Type());
            foo->setCurrentInterpolationY(true);
            phy = foo->value(b->getOffset(), b, b->getPlant());
            double stdDev = paramSet->getParameter("PhyllotaxyAngleStdDev", b->Type())->value(b, b->getPlant());
            phy += b->theBud->getConfigData().getRandomGenerator()->nextGaussian() * stdDev;
        }
    }
    else
        phy = 0;

    return phy;
}

float GeomRoot::computeInsertAnglePhy(float phy)
{
    float vert;

    Root * b =(Root *)this->hierarc->getBranc();

    GeomBranc *gb = this->gbearer;
    GeomRoot *gbrp;
    if (dynamic_cast<const GeomRoot*> (gb) == NULL)
        return 0;
    else
        gbrp=(GeomRoot *)gb;

    Root * p = b->getBearer();

    long bearerType = p->Type();

    vert = FGEOM_TWO_PI * (this->hierarc->indexIntoVerticille-1) / (float)this->hierarc->verticilleBranchNumber;

    if (b != p)
    {
        ParametricParameter *foo = (ParametricParameter *) paramSet->getParameter("phyllotaxyAngle", b->Type());
        foo->setCurrentInterpolationY(true);
        phy = foo->value(b->getOffset(), b, b->getPlant());
        double stdDev = paramSet->getParameter("PhyllotaxyAngleStdDev",b->Type())->value(b, b->getPlant());
        phy += b->theBud->getConfigData().getRandomGenerator()->nextGaussian() * stdDev;
        gbrp->currentPhyllotaxy[b->Type()] += phy;
        phy = gbrp->currentPhyllotaxy[b->Type()];
    }
    else
        phy = 0;

    phy += vert;

    return phy;

}

/* compute initial parameter value for length and diameter */
void GeomRoot::computeInitialDiameter ()
{

    Root * b =(Root *)this->hierarc->getBranc();

    this->initialDiameter = b->computeInitialDiameter ();

}

int GeomRoot::computeBranc(bool silent)
{
    return computeBranc (ASCENDING, silent);
}

int GeomRoot::computeBranc(BrancSeekMode bsm, bool silent)
{
    if(hierarc == nullptr)
        return -1;

	short idx = hierarc->ordre+2+(getPosOnBearer()+hierarc->indexIntoVerticille)%(NBCOMPTRAND-hierarc->ordre-2);

	int savCounter = v_plt->getConfigData().getRandomGenerator()->getCounter();
	int savIdx = v_plt->getConfigData().getRandomGenerator()->setState(idx);

	v_plt->getConfigData().getRandomGenerator()->initState(idx);

	this->alea = (float)v_plt->getConfigData().getRandomGenerator()->Random();
	this->alea = (float)v_plt->getConfigData().getRandomGenerator()->Random();

	idx = v_plt->getConfigData().getRandomGenerator()->getIdx();
    Root * bv =(Root *)this->hierarc->getBranc();

    bv->startPosit();

    bv->positOnFirstSegmentWithinRoot();
    if(bv->getCurrentSegment()->getTransformation() == 0)
    {
        bv->getCurrentSegment()->setTransformation(&currentTrans);
    }


    if(getConstraint() == 0)
        caclulateConstraint();


    bv->endPosit();

    int ret = GeomBrancCone::computeBranc(bsm, silent);


	v_plt->getConfigData().getRandomGenerator()->setState(savIdx);
	v_plt->getConfigData().getRandomGenerator()->setCounter(savCounter);

    return ret;
}


int GeomRoot::findSymbole ()
{
    Root * bv= (Root *)hierarc->getBranc();
    return (int)((ParametricParameter*)paramSet->getParameter("symbolNumber",bv->Type()))->value((float)bv->theBud->lastGrowthPosition, bv, bv->getPlant());
}

/* compute current length */
float GeomRoot::computeLength ()
{
    Root * bv= (Root *)this->hierarc->getBranc();

    return static_cast<float>(bv->getCurrentSegment()->Length ());
}





/* compute current diameter */
float GeomRoot::computeTopDiam ()
{
    Segment * s= (Segment *)((Root*)this->hierarc->getBranc())->getCurrentSegment();

    return s->computeDiam (initialDiameter, evolDiamStdDev, timeEvolDiamStdDev, delayEvolDiamStdDev);

}


/* compute bending angle for current node according to global flexion of branch */
void GeomRoot::flexionAtNode ()
{
    /// compute deviation angel.

    double stdDev;
    bool constraintSatisfied = true;
    Vector3 transla;
    Vector3 axe;
    int nbTry = 0;

    Root * bv =(Root *)hierarc->getBranc();
    Matrix4 trans (currentTrans);

    do {
        nbTry ++;

        if (bv->getCurrentSegmentIndex() != 0) {
            deviationAngle = paramSet->getParameter("DeviationAngle", bv->Type())->value(bv, bv->getPlant());
            stdDev = paramSet->getParameter("DeviationAngleStdDev", bv->Type())->value(bv, bv->getPlant());
            float rnd = bv->theBud->getConfigData().getRandomGenerator()->nextGaussian();
            deviationAngle +=  rnd * stdDev;
            deviationAngle = deviationAngle * FGEOM_PI / 120;
        } else {
            deviationAngle = 0;
        }

        trans = currentTrans;

        transla=trans.getTranslation();

        axe = caclRandomNormalAxeToMainAxe(trans.getMainVector());

        trans.setTranslation(0,0,0);

        axe.normalize();


        if (bv->theBud->getConfigData().getRandomGenerator()->Random() < 0.5)
           axe[1]*=-1;

        if (bv->theBud->getConfigData().getRandomGenerator()->Random() < 0.5)
           axe[2]*=-1;

        int f1 = static_cast<int>(bv->theBud->getConfigData().getRandomGenerator()->Random() * 1000);
        if(f1 % 2)
            deviationAngle *= -1;


        trans =  Matrix4::translation(transla) * Matrix4::axisRotation(axe, deviationAngle)*trans;

        trans.setTranslation(transla.x(), transla.y(), transla.z());

    //    Vector3 m=currentTrans.getMainVector();
    //    Vector3 s=currentTrans.getSecondaryVector();
    //    Vector3 ss=currentTrans.getNormalVector();
    //    Vector3 t=currentTrans.getTranslation();


        EllipticConeCylinderConstraint * constraint = getConstraint();
        if (constraint != nullptr) {
            if(constraint->isConstrained(trans)){
                constraint->transform(trans);
                constraintSatisfied = true;
           }
        }
    } while (!constraintSatisfied);

    currentTrans = trans;

}

Vector3 GeomRoot::caclRandomNormalAxeToMainAxe(Vector3 main) {

    Root * bv =(Root *)hierarc->getBranc();

    Vector3 i,j;

    float x = main.x();
    float y = main.y();
    float z = main.z();
    bool axex=false, axey=false, axez=false;
    if (abs(x) > 0.98)
        axex == true;
    if (abs(y) > 0.98)
        axey == true;
    if (abs(z) > 0.98)
        axez == true;

    if(!axex && !axey && !axez){
        i = Vector3(-y,x,0);
        j = Vector3(x*z,-y*z,x*x+y*y);
        if (   abs(x) < .001
            && abs(y) < .001) {
            i = Vector3(-1,1,0);
            j = Vector3(0,0,1);
        }
    } else if(axez){
        float f1 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        float f2 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        i = Vector3(-f1,f2,0);
        j = Vector3(f1,-f2,0);
    } else if(axex){
        float f1 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        float f2 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        i = Vector3(0,-f1,f2);
        j = Vector3(0,f1,-f2);
    } else {
        float f1 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        float f2 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        i = Vector3(-f1,0,f2);
        j = Vector3(f1,0,-f2);
    }
    assert(i != j && "i is equal to j, the direction will not be correct!\n");

    double alpha = 1 - 2*bv->theBud->getConfigData().getRandomGenerator()->Random();
    double beta  = 1 - 2*bv->theBud->getConfigData().getRandomGenerator()->Random();
    Vector3 axe = (i * alpha) + (j * beta);
    axe.normalize();

    return axe;
}

void GeomRoot::computeEntn(int pos, bool first)
{
    GeomBrancCone::computeEntn(pos, first);

    Root * bv= (Root *)this->hierarc->getBranc();

    length += bv->getCurrentSegment()->Length ();
}


void GeomRoot::simplification ()
{

    Vector3 positionTop;

    Vector3 positionBottom;

    Vector3 tmpVec, rotVec, transla, vectmp;

    unsigned int i,j;

    float scalarDir;

    float dif;

    GeomElemCone * gelmprec, * gelm;

    i=1;


    if(!v_plt->getConfigData().cfg_geoSimp)
        return ;

    while(i<elemGeo.size())
    {

        gelm=(GeomElemCone *)elemGeo[i];

        gelmprec=(GeomElemCone *)elemGeo[i-1];


        Matrix4 & currentTriedron = gelm->getMatrix();

        Matrix4 & precTriedron = gelmprec->getMatrix();


        dif=0;
        /* computes distance between top of current level and bottom of organ */
        positionTop=currentTriedron.getTranslation();

        positionBottom=precTriedron.getMainVector()*(float)(gelmprec->getLength())+precTriedron.getTranslation();

        dif += fabs(positionTop[0]-positionBottom[0]);
        dif += fabs(positionTop[1]-positionBottom[1]);
        dif += fabs(positionTop[2]-positionBottom[2]);


        /* computes direction shift between current level and organ */

        scalarDir = currentTriedron.getMainVector()*precTriedron.getMainVector();


        if (gelm->getSymbole() != gelmprec->getSymbole())
            i++;
        else if (  (   scalarDir > 0.90
                       && dif < 0.10
                       && gelm->getGeomChildren().empty())
                   || (scalarDir >= 0.9999))
        {
            /* concatenate current level and organ */

            gelmprec->setTopDiam(gelm->getTopDiam());

            if(scalarDir<0.999)
            {

                tmpVec=(precTriedron.getMainVector()*(float)gelmprec->getLength())+(currentTriedron.getMainVector()*(float)gelm->getLength());

                vectmp=(precTriedron.getMainVector()*(float)gelmprec->getLength());

                gelmprec->setLength(norm(tmpVec));

                vectmp.normalize();

                tmpVec.normalize();

                rotVec=vectmp^tmpVec;

                rotVec.normalize();

                transla=precTriedron.getTranslation();

                precTriedron=Matrix4::translation(-transla)*precTriedron;

                precTriedron=Matrix4::axisRotation(rotVec, angle(vectmp,tmpVec))*precTriedron;

                precTriedron=Matrix4::translation(transla)*precTriedron;

            }
            else
            {
                gelmprec->setLength(gelmprec->getLength()+gelm->getLength());
            }



            elemGeo.erase(elemGeo.begin()+i);


            for(j=0; j<gelm->getGeomChildren().size(); j++)
            {
                gelm->getGeomChildren().at(j)->setPosOnBearer(i);

                gelmprec->getGeomChildren().push_back(gelm->getGeomChildren().at(j));
            }

            this->v_plt->getGeometry().updateElemNumber(false);

            gelmprec->setPosInGeomBranc(i-1);
            gelmprec->setPosInBranc(gelm->getPosInBranc());

            delete gelm;
        }
        else
            i++;


    }//end while


}

void GeomRoot::caclulateConstraint() {
    constraint = 0;
    Root *r = (Root *)getBranc();
    double d1 = r->getBud()->paramSet->getParameter("BoundingVerticalConeAngle", r->Type())->value(r, r->getPlant());
    double d2 = r->getBud()->paramSet->getParameter("BoundingHorizontalConeAngle", r->Type())->value(r, r->getPlant());
    double d3 = r->getBud()->paramSet->getParameter("BoundingConeLength", r->Type())->value(r, r->getPlant());
    if((d1 < 45)
       || (d2 < 45)
       || d3 < 9999){
        Vector3 o(0, 0, 0);
        Vector3 dir(0, 0, -1);
        EllipticConeCylinder *c = new EllipticConeCylinder(o, dir, d1, d2, d3);
        ConstrainedRoot *constrainedRoot = new ConstrainedRoot(this);
        constraint = new EllipticConeCylinderConstraint(constrainedRoot, c);
    }

}

EllipticConeCylinderConstraint *GeomRoot::getConstraint() const {
	return constraint;
}

void GeomRoot::setConstraint(EllipticConeCylinderConstraint *constraint) {
	GeomRoot::constraint = constraint;
}


//Constructeur par copie







