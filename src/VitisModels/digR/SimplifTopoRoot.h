/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file SimplifTopoAMAP.h
///			\brief Definition of SimplifTopoAMAP.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __SIMPLIFTOPOROOT__
#define __SIMPLIFTOPOROOT__
#include "SimplifTopo.h"



///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class SimplifTopoRoot
///			\brief Manage the association of Root instances with criterias based on root type, rythm and bud outbreak time
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class SimplifTopoRoot :
	public SimplifTopo
{
public:

	/// \brief Constructor
	SimplifTopoRoot(class Plant *plt);

	/// \brief Destructor
	virtual ~SimplifTopoRoot(void);

	/// \brief Format the criteria with given parameter : type, outbreak time, rythm and order
	const std::string formatCriteria(int type, double time, double rythm, char order);
	/// \brief Format the criteria with given parameter : type, outbreak time, rythm, order and birth date threshold
	const std::string formatCriteria(int type, double time, double rythm, char order, double birthThreshold);

	/// \return the root at index \e indexInCriteria in the list of Branc associated to the criteria \e critere
	virtual Branc * getSimplifiedRoot(const std::string & critere, int indexInCriteria);

};

#endif

