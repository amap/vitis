/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file RootBud.h
///			\brief Definition of Root Bud class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _ROOTBUD_H
#define _ROOTBUD_H

#include "externDigR.h"
#include "scheduler.h"
#include "ParamFile.h"
#include "GrowthEngineRoot.h"
#include "Root.h"
#include "Plant.h"

class growthData;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class RootBud
///			\brief Describe the mechanism of a root bud
///
///			Manage two edification algorithm : growth and ramification for his linked "Root" axe. \n
///			A root bud inherits from VProcess and this process is scheduled along simulation time.\n
///			For each bud eventData launched, the process_event method is called and launch growth and ramification
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DIGR_EXPORT RootBud : public VProcess {

//    VitisConfig *v_config;


    friend class GrowthEngineRoot;

public:

    ///each bud has his own growth speed and time growth speed, which will be calculated in the constractor.
    ParametricParameter *growthSpeed;
    ParametricParameter *timeGrowthSpeed;


    /// \brief branching order
    int ramifOrder;

    /// \brief branche ramification type (Acropete, repiercing)
    char rootRamificationType;

    /// \brief death flag
    bool death;

    /// \brief birth moment
    double birthTime;

    /// \brief death moment
    double lastDeathTestTime;
    double lastDeathTestPosition;
    double lastGrowthPosition;
    double lastGrowthTime;

    /// \brief time for end of current Segment
    double nextInstant;

    /// \brief Pointer to axis reference
    ParamSet *paramSet;

    /// \brief Pointer to the growth engine
    GrowthEngineRoot *groEngine;

    /// \brief Pointer to his linked axe
    class Root *root;

    /// \brief Next eventData time
    double nextTime;

    /// \brief Flag to know which pruning policy to apply
    char toBePruned;


    /// \brief Default constructor
    RootBud();

    /// \brief Constructor
    /// @param br The branc associated to the Bud
    /// @param guRam growth unit ramification type : ANTICIPE, RETARDE1, RETARDE2, TRAUMAANT, TRAUMARET, RETARDECYC, NONBRANCHETRAUMAANT, NONBRANCHEANT, NONBRANCHERETCYC, NONBRANCHETRAUMARET
    /// @param timeC Start time of the new bud
    /// @param rootTypeBeg RootType of the new bud
    /// @param ryt rythme of the new Bud
    ///	@return An instance of Bud
//	RootBud(Root * br, double timeC ,int rootType);
    RootBud(Root *br, double timeC, int rootType);

    /// \brief Destructor
    ~RootBud(void);


    /// \brief The main loop of axe's edification model
    /// \param msg The eventData attached to this process
    ///
    /// This loop manage the growth, ramification , death and pruning computation, reiteration.
    virtual void process_event(EventData *msg);

    ///checks if death happens on this age. (age == time)
    short testDeath(double time);

    /// \brief Launch the natural pruning of the associated axe and his children
    void pruning(DeathBudEvent *e);

    void pruning();

    /// \brief Function called when a hierarc axe linked to this bud is added
    virtual void updateHierar(Hierarc *hierarcPtr);

    /// \brief Function called when a hierarc axe linked to this bud is removed
    virtual void updatePruning();

    /// \name Accessor

    /// \return the growth engine
    inline GrowthEngineRoot *getGrowth() { return groEngine; }

    /// \return the associated branc
    inline class Root *getRoot() { return root; }

    /// \return the current phy age
    inline int getRootType() { return groEngine->rootType; }

    /// \brief Serialize a Bud
    /// \param ar A reference to the archive \see Archive
    virtual void serialize(Archive &ar);

    /// calculates the growth time for a specific growth speed.
    void constructTimeGrowthSpeed();


    /// \return the configuration data
    VitisConfig &getConfigData();

    void clearEvents (RootBud *b);


};


#endif

