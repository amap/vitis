/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef __EXPORTDIGR_H__
#define __EXPORTDIGR_H__
#include "externDll.h"


#if !defined STATIC_LIB
	#if defined WIN32

#pragma warning ( disable : 4251 )
			#ifdef DIGR_EXPORTS
				#define DIGR_EXPORT __declspec(dllexport)
				#define NOTIFIER_EXPORT __declspec(dllexport)
			#else
				#define DIGR_EXPORT __declspec(dllimport)
				#define NOTIFIER_EXPORT __declspec(dllimport)
			#endif
	#else
			#define DIGR_EXPORT
			#define NOTIFIER_EXPORT

	#endif

#else
	#define DIGR_EXPORT
        #define NOTIFIER_EXPORT

#endif


#endif

