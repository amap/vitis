/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef PARAMSETDIGR_H
#define PARAMSETDIGR_H

#include <fstream>
#include <vector>
#include "File.h"
#include "tinystr.h"
#include "tinyxml.h"
#include "VitisCoreSignalInterface.h"
#include "VitisObject.h"
#include "externDll.h"
#include "ParamFile.h"
#include "externDigR.h"


class DIGR_EXPORT ParamSetDigR : public ParamSet {

protected:


public:

    /// \brief Constructor
    ParamSetDigR(const std::string &Name);

    /// \brief Destructor
    virtual ~ParamSetDigR(void) {
        ;
    };

    TiXmlDocument *open(const char *name);

    void appliquer(TiXmlNode *noeud);

    void readParameter(TiXmlNode *pParent, unsigned int indent = 0);

    int GetParamType(TiXmlElement *pElement, unsigned int indent);

    const char *getIndentAlt(unsigned int numIndents);

    const char *getIndent(unsigned int numIndents);


    /*@{*/

    /** @brief read a set of parameter from file */
    //MapParameter read ();

    /*@}*/
    /*@{*/
    /*@}*/

};

#endif // PARAMSETDIGR_H

