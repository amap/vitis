/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include "tinyxml.h"
#include "ParamSetDigR.h"
#include "tinystr.h"
#include <fstream>
#include "ParamFile.h"
#include "defROOT.h"


using namespace std;
int indexType = -1;

const unsigned int NUM_INDENTS_PER_SPACE = 2;

ParamSetDigR::ParamSetDigR(const std::string &Name) {

    open(Name.c_str());
}

TiXmlDocument *ParamSetDigR::open(const char *pFilename) {
    TiXmlDocument *doc = new TiXmlDocument(pFilename);
    bool loadOkay = doc->LoadFile();
    MapParameter m;

    if (loadOkay) {
   		setParameterSignal(new ParameterSignal);
        printf("\n%s:\n", pFilename);
        this->readParameter(doc);

    } else {
        printf("Failed to load file \"%s\"\n", pFilename);
    }
    return doc;
}

void ParamSetDigR::appliquer(TiXmlNode *noeud) {
    TiXmlNode *suivant;
    string val, valParam, valParam2;
    int p = noeud->Type();
    TiXmlText *pText;
    TiXmlNode *elem;
    string key;
    float valParam1, valParam3;
    TiXmlNode *child;
    TiXmlNode *child2;
    TiXmlNode *elem2;
    TiXmlNode *elem3;
    TiXmlText *text;

    if (p == TiXmlNode::TEXT) {

    } else if (p == TiXmlNode::ELEMENT) {

        val = noeud->Value();

        if (val == "jeeb.workspace.digr.DigRParam_-RootType") {
            indexType += 1;
        }

        if (val == "plantName") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *plantName = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (plantName != 0) {

                plantName->setName(val);
                plantName->set(valParam1);

                this->addParameter(key, plantName);
            }
        }
        if (val == "maxAge") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            // key=formatName(val,indexType);

            SingleValueParameter *maxAge = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (maxAge != 0) {

                maxAge->setName(val);
                maxAge->set(valParam1);

                this->addParameter(val, maxAge);
            }
        }
        if (val == "withSoil") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            if (valParam == "true") {
                valParam1 = true;
            } else {
                valParam1 = false;
            }
            // key=formatName(val,indexType);

            SingleValueParameter *withSoil = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (withSoil != 0) {

                withSoil->setName(val);

                withSoil->set(valParam1);

                this->addParameter(val, withSoil);
            }
        }


        if (val == "maxRamif") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            // key=formatName(val,indexType);

            SingleValueParameter *maxRamif = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (maxRamif != 0) {

                maxRamif->setName(val);
                maxRamif->set(valParam1);

                this->addParameter(val, maxRamif);
            }
        }
        if (val == "age") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            // key=formatName(val,indexType);

            SingleValueParameter *age = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (age != 0) {

                age->setName(val);
                age->set(valParam1);

                this->addParameter(val, age);
            }
        }
        if (val == "seed") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            // key=formatName(val,indexType);

            SingleValueParameter *seed = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (seed != 0) {

                seed->setName(val);
                seed->set(valParam1);

                this->addParameter(val, seed);
            }
        }
        if (val == "outputStep") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            // key=formatName(val,indexType);

            SingleValueParameter *outputStep = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (outputStep != 0) {

                outputStep->setName(val);
                outputStep->set(valParam1);

                this->addParameter(val, outputStep);
            }
        }
        if (val == "evolutionTime") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            //   key=formatName(val,indexType);

            SingleValueParameter *evolutionTime = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (evolutionTime != 0) {

                evolutionTime->setName(val);
                evolutionTime->set(valParam1);

                this->addParameter(val, evolutionTime);
            }
        }
        if (val == "name") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *name = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (name != 0) {

                name->setName(val);
                name->set(valParam1);

                this->addParameter(key, name);
            }
        }
        if (val == "VerticilleNumber") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *verticilleNumber = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (verticilleNumber != 0) {

                verticilleNumber->setName(val);
                verticilleNumber->set(valParam1);

                this->addParameter(key, verticilleNumber);
            }
        }
        if (val == "verticilleProba") {
            elem = noeud->FirstChild();
            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *verticilleProba = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");


            for (; elem != NULL; elem = elem->NextSibling()) {
                elem = elem->FirstChild("position");

                child = elem->FirstChild();

                child2 = elem->NextSibling();
                child2 = child2->FirstChild();

                TiXmlText *text = child->ToText();
                valParam = text->Value();
                valParam1 = atof(valParam.c_str());

                text = child2->ToText();
                valParam2 = text->Value();
                valParam3 = atof(valParam2.c_str());

                verticilleProba->set(valParam1, valParam3);

                elem = elem->Parent();
            }

            verticilleProba->setName(val);
            this->addParameter(key, verticilleProba);
        }
        if (val == "growthDelay") {
            elem = noeud->FirstChild();
            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *growthDelay = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");


            for (; elem != NULL; elem = elem->NextSibling()) {
                elem = elem->FirstChild("position");

                child = elem->FirstChild();

                child2 = elem->NextSibling();
                child2 = child2->FirstChild();

                TiXmlText *text = child->ToText();
                valParam = text->Value();
                valParam1 = atof(valParam.c_str());

                text = child2->ToText();
                valParam2 = text->Value();
                valParam3 = atof(valParam2.c_str());

                growthDelay->set(valParam1, valParam3);

                elem = elem->Parent();
            }

            growthDelay->setName(val);
            this->addParameter(key, growthDelay);
        }
        if (val == "growthSpeed") {
            elem = noeud->FirstChild();
            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *growthSpeed = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");


            for (; elem != NULL; elem = elem->NextSibling()) {

                elem = elem->FirstChild("position");

                child = elem->FirstChild();

                child2 = elem->NextSibling();
                child2 = child2->FirstChild();

                TiXmlText *text = child->ToText();
                valParam = text->Value();
                valParam1 = atof(valParam.c_str());

                text = child2->ToText();
                valParam2 = text->Value();
                valParam3 = atof(valParam2.c_str());

                growthSpeed->set(valParam1, valParam3);

                elem = elem->Parent();
            }

            growthSpeed->setName(val);
            this->addParameter(key, growthSpeed);
        }


        if (val == "GlobalPercentOnGrowthSpeed") {

            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *globalPercentOnGrowthSpeed = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (globalPercentOnGrowthSpeed != 0) {

                globalPercentOnGrowthSpeed->setName(val);
                globalPercentOnGrowthSpeed->set(valParam1);

                this->addParameter(key, globalPercentOnGrowthSpeed);
            }
        }
        if (val == "timeGrowthSpeed") {
            elem = noeud->FirstChild();
            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *timeGrowthSpeed = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");


            for (; elem != NULL; elem = elem->NextSibling()) {
                elem = elem->FirstChild("position");

                child = elem->FirstChild();

                child2 = elem->NextSibling();
                child2 = child2->FirstChild();

                TiXmlText *text = child->ToText();
                valParam = text->Value();
                valParam1 = atof(valParam.c_str());

                text = child2->ToText();
                valParam2 = text->Value();
                valParam3 = atof(valParam2.c_str());

                timeGrowthSpeed->set(valParam1, valParam3);

                elem = elem->Parent();
            }

            timeGrowthSpeed->setName(val);
            this->addParameter(key, timeGrowthSpeed);
        }
        if (val == "deathProba") {

            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *deathProba = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");

            for (elem = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble");
                 elem != NULL; elem = elem->NextSibling("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble")) {

                for (elem2 = elem->FirstChild("position"); elem2 != NULL; elem2 = elem2->NextSibling("position")) {
                    child = elem2->FirstChild();

                    child2 = elem2->NextSibling();
                    child2 = child2->FirstChild();

                    TiXmlText *text = child->ToText();
                    valParam = text->Value();
                    valParam1 = atof(valParam.c_str());

                    text = child2->ToText();
                    valParam2 = text->Value();
                    valParam3 = atof(valParam2.c_str());

                    deathProba->set(valParam1, valParam3);

                }
            }
            deathProba->setName(val);
            this->addParameter(key, deathProba);
        }
        if (val == "PruningLag") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *pruningLag = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (pruningLag != 0) {

                pruningLag->setName(val);
                pruningLag->set(valParam1);

                this->addParameter(key, pruningLag);
            }
        }
        if (val == "PruningLagStdDev") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *pruningLagStdDev = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (pruningLagStdDev != 0) {

                pruningLagStdDev->setName(val);
                pruningLagStdDev->set(valParam1);

                this->addParameter(key, pruningLagStdDev);
            }
        }
        if (val == "PruningExceptionPercent") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *pruningExceptionPercent = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (pruningExceptionPercent != 0) {

                pruningExceptionPercent->setName(val);
                pruningExceptionPercent->set(valParam1);

                this->addParameter(key, pruningExceptionPercent);
            }
        }

        if (val == "symbolNumber") {

            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *symbolNumber = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");

            for (elem = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleInteger");
                 elem != NULL; elem = elem->NextSibling("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleInteger")) {

                for (elem2 = elem->FirstChild("position"); elem2 != NULL; elem2 = elem2->NextSibling("position")) {
                    child = elem2->FirstChild();

                    child2 = elem2->NextSibling();
                    child2 = child2->FirstChild();

                    TiXmlText *text = child->ToText();
                    valParam = text->Value();
                    valParam1 = atof(valParam.c_str());

                    text = child2->ToText();
                    valParam2 = text->Value();
                    valParam3 = atof(valParam2.c_str());

                    symbolNumber->set(valParam1, valParam3);

                }
            }
            symbolNumber->setName(val);
            this->addParameter(key, symbolNumber);
        }
        if (val == "insertionType") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();

            valParam1 = 0.0;
            if (strcmp(valParam.c_str(), "RHIZOTAXY") == 0)
                valParam1 = RHIZOTAXY;
            else if (strcmp(valParam.c_str(), "BEARERPLAN") == 0)
                valParam1 = BEARERPLAN;
            else if (strcmp(valParam.c_str(), "ABSOLUTEPLAN") == 0)
                valParam1 = ABSOLUTEPLAN;

            key = formatName(val, indexType);

            SingleValueParameter *insertionType = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (insertionType != 0) {

                insertionType->setName(val);
                insertionType->set(valParam1);

                this->addParameter(key, insertionType);
            }
        }
        if (val == "InsertionAngle") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *insertionAngle = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (insertionAngle != 0) {

                insertionAngle->setName(val);
                insertionAngle->set(valParam1);

                this->addParameter(key, insertionAngle);
            }
        }
        if (val == "InsertionAngleStdDev") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *insertionAngleStdDev = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (insertionAngleStdDev != 0) {

                insertionAngleStdDev->setName(val);
                insertionAngleStdDev->set(valParam1);

                this->addParameter(key, insertionAngleStdDev);
            }
        }
        if (val == "phyllotaxyAngle") {

            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *phyllotaxyAngle = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");

            for (elem = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble");
                 elem != NULL; elem = elem->NextSibling("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble")) {

                for (elem2 = elem->FirstChild("position"); elem2 != NULL; elem2 = elem2->NextSibling("position")) {
                    child = elem2->FirstChild();

                    child2 = elem2->NextSibling();
                    child2 = child2->FirstChild();

                    TiXmlText *text = child->ToText();
                    valParam = text->Value();
                    valParam1 = atof(valParam.c_str());

                    text = child2->ToText();
                    valParam2 = text->Value();
                    valParam3 = atof(valParam2.c_str());

                    phyllotaxyAngle->set(valParam1, valParam3);

                }
            }
            phyllotaxyAngle->setName(val);
            this->addParameter(key, phyllotaxyAngle);
        }
        if (val == "PhyllotaxyAngleStdDev") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *phyllotaxyAngleStdDev = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (phyllotaxyAngleStdDev != 0) {

                phyllotaxyAngleStdDev->setName(val);
                phyllotaxyAngleStdDev->set(valParam1);

                this->addParameter(key, phyllotaxyAngleStdDev);
            }
        }
        if (val == "DeviationAngle") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *deviationAngle = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (deviationAngle != 0) {

                deviationAngle->setName(val);
                deviationAngle->set(valParam1);

                this->addParameter(key, deviationAngle);
            }
        }
        if (val == "DeviationAngleStdDev") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *deviationAngleStdDev = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (deviationAngleStdDev != 0) {

                deviationAngleStdDev->setName(val);
                deviationAngleStdDev->set(valParam1);

                this->addParameter(key, deviationAngleStdDev);
            }
        }
        if (val == "BoundingVerticalConeAngle") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *boundingVerticalConeAngle = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (boundingVerticalConeAngle != 0) {

                boundingVerticalConeAngle->setName(val);
                boundingVerticalConeAngle->set(valParam1);

                this->addParameter(key, boundingVerticalConeAngle);
            }
        }
        if (val == "BoundingHorizontalConeAngle") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *boundingHorizontalConeAngle = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (boundingHorizontalConeAngle != 0) {

                boundingHorizontalConeAngle->setName(val);
                boundingHorizontalConeAngle->set(valParam1);

                this->addParameter(key, boundingHorizontalConeAngle);
            }
        }
        if (val == "BoundingConeLength") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *boundingConeLength = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (boundingConeLength != 0) {

                boundingConeLength->setName(val);
                boundingConeLength->set(valParam1);

                this->addParameter(key, boundingConeLength);
            }
        }

        if (val == "finalAbsoluteDirectionToVerticalList") {

            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *finalAbsoluteDirectionToVerticalList = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");

            for (elem = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble");
                 elem != NULL; elem = elem->NextSibling("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble")) {

                for (elem2 = elem->FirstChild("position"); elem2 != NULL; elem2 = elem2->NextSibling("position")) {
                    child = elem2->FirstChild();

                    child2 = elem2->NextSibling();
                    child2 = child2->FirstChild();

                    TiXmlText *text = child->ToText();
                    valParam = text->Value();
                    valParam1 = atof(valParam.c_str());

                    text = child2->ToText();
                    valParam2 = text->Value();
                    valParam3 = atof(valParam2.c_str());

                    finalAbsoluteDirectionToVerticalList->set(valParam1, valParam3);

                }
            }
            finalAbsoluteDirectionToVerticalList->setName(val);
            this->addParameter(key, finalAbsoluteDirectionToVerticalList);
        }

        if (val == "bendingLengthList") {

            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *bendingLengthList = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");

            for (elem = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble");
                 elem != NULL; elem = elem->NextSibling("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble")) {

                for (elem2 = elem->FirstChild("position"); elem2 != NULL; elem2 = elem2->NextSibling("position")) {
                    child = elem2->FirstChild();

                    child2 = elem2->NextSibling();
                    child2 = child2->FirstChild();

                    TiXmlText *text = child->ToText();
                    valParam = text->Value();
                    valParam1 = atof(valParam.c_str());

                    text = child2->ToText();
                    valParam2 = text->Value();
                    valParam3 = atof(valParam2.c_str());

                    bendingLengthList->set(valParam1, valParam3);

                }
            }
            bendingLengthList->setName(val);
            this->addParameter(key, bendingLengthList);
        }

        if (val == "InitialDiameter") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *initialDiameter = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (initialDiameter != 0) {

                initialDiameter->setName(val);
                initialDiameter->set(valParam1);

                this->addParameter(key, initialDiameter);
            }
        }

        if (val == "InitialDiameterStdDev") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *initialDiameterStdDev = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (initialDiameterStdDev != 0) {

                initialDiameterStdDev->setName(val);
                initialDiameterStdDev->set(valParam1);

                this->addParameter(key, initialDiameterStdDev);
            }
        }
        if (val == "diameterIncreaseRatio") {

            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *diameterIncreaseRatio = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");

            for (elem = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble");
                 elem != NULL; elem = elem->NextSibling("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble")) {

                for (elem2 = elem->FirstChild("position"); elem2 != NULL; elem2 = elem2->NextSibling("position")) {
                    child = elem2->FirstChild();

                    child2 = elem2->NextSibling();
                    child2 = child2->FirstChild();

                    TiXmlText *text = child->ToText();
                    valParam = text->Value();
                    valParam1 = atof(valParam.c_str());

                    text = child2->ToText();
                    valParam2 = text->Value();
                    valParam3 = atof(valParam2.c_str());

                    diameterIncreaseRatio->set(valParam1, valParam3);

                }
            }
            diameterIncreaseRatio->setName(val);
            this->addParameter(key, diameterIncreaseRatio);
        }
        if (val == "diameterIncreaseTime") {

            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *diameterIncreaseTime = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");

            for (elem = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble");
                 elem != NULL; elem = elem->NextSibling("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble")) {

                for (elem2 = elem->FirstChild("position"); elem2 != NULL; elem2 = elem2->NextSibling("position")) {
                    child = elem2->FirstChild();

                    child2 = elem2->NextSibling();
                    child2 = child2->FirstChild();

                    TiXmlText *text = child->ToText();
                    valParam = text->Value();
                    valParam1 = atof(valParam.c_str());

                    text = child2->ToText();
                    valParam2 = text->Value();
                    valParam3 = atof(valParam2.c_str());

                    diameterIncreaseTime->set(valParam1, valParam3);

                }
            }
            diameterIncreaseTime->setName(val);
            this->addParameter(key, diameterIncreaseTime);
        }

        if (val == "diameterIncreaseDelay") {

            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *diameterIncreaseDelay = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");

            for (elem = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble");
                 elem != NULL; elem = elem->NextSibling("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble")) {

                for (elem2 = elem->FirstChild("position"); elem2 != NULL; elem2 = elem2->NextSibling("position")) {
                    child = elem2->FirstChild();

                    child2 = elem2->NextSibling();
                    child2 = child2->FirstChild();

                    TiXmlText *text = child->ToText();
                    valParam = text->Value();
                    valParam1 = atof(valParam.c_str());

                    text = child2->ToText();
                    valParam2 = text->Value();
                    valParam3 = atof(valParam2.c_str());

                    diameterIncreaseDelay->set(valParam1, valParam3);

                }
            }
            diameterIncreaseDelay->setName(val);
            this->addParameter(key, diameterIncreaseDelay);
        }
        if (val == "DiameterIncreaseRatioStdDev") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *diameterIncreaseRatioStdDev = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (diameterIncreaseRatioStdDev != 0) {

                diameterIncreaseRatioStdDev->setName(val);
                diameterIncreaseRatioStdDev->set(valParam1);

                this->addParameter(key, diameterIncreaseRatioStdDev);
            }
        }
        if (val == "DiameterIncreaseTimeStdDev") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *diameterIncreaseTimeStdDev = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (diameterIncreaseTimeStdDev != 0) {

                diameterIncreaseTimeStdDev->setName(val);
                diameterIncreaseTimeStdDev->set(valParam1);

                this->addParameter(key, diameterIncreaseTimeStdDev);
            }
        }

        if (val == "DiameterIncreaseDelayStdDev") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *diameterIncreaseDelayStdDev = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (diameterIncreaseDelayStdDev != 0) {

                diameterIncreaseDelayStdDev->setName(val);
                diameterIncreaseDelayStdDev->set(valParam1);

                this->addParameter(key, diameterIncreaseDelayStdDev);
            }
        }
        if (val == "MaxReit") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *maxReit = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (maxReit != 0) {

                maxReit->setName(val);
                maxReit->set(valParam1);

                this->addParameter(key, maxReit);
            }
        }

        if (val == "ReitAngle") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *reitAngle = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (reitAngle != 0) {

                reitAngle->setName(val);
                reitAngle->set(valParam1);

                this->addParameter(key, reitAngle);
            }
        }

        if (val == "ReitDeviation") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *reitDeviation = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (reitDeviation != 0) {

                reitDeviation->setName(val);
                reitDeviation->set(valParam1);

                this->addParameter(key, reitDeviation);
            }
        }

        if (val == "Reitrhizotaxy") {
            elem = noeud->FirstChild();
            TiXmlText *text = elem->ToText();
            valParam = text->Value();
            valParam1 = atof(valParam.c_str());
            key = formatName(val, indexType);

            SingleValueParameter *reitrhizotaxy = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (reitrhizotaxy != 0) {

                reitrhizotaxy->setName(val);
                reitrhizotaxy->set(valParam1);

                this->addParameter(key, reitrhizotaxy);
            }
        }

        if (val == "reitDistance") {

            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *reitDistance = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");

            for (elem = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble");
                 elem != NULL; elem = elem->NextSibling("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble")) {

                for (elem2 = elem->FirstChild("position"); elem2 != NULL; elem2 = elem2->NextSibling("position")) {
                    child = elem2->FirstChild();

                    child2 = elem2->NextSibling();
                    child2 = child2->FirstChild();

                    TiXmlText *text = child->ToText();
                    valParam = text->Value();
                    valParam1 = atof(valParam.c_str());

                    text = child2->ToText();
                    valParam2 = text->Value();
                    valParam3 = atof(valParam2.c_str());

                    reitDistance->set(valParam1, valParam3);

                }
            }
            reitDistance->setName(val);
            this->addParameter(key, reitDistance);
        }

        if (val == "reitFrequency") {

            string blabla = noeud->Value();
            key = formatName(val, indexType);

            ParametricParameter *reitFrequency = (ParametricParameter *) ParamFactory::instance().createParameter(
                    "ParametricParameter");

            for (elem = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble");
                 elem != NULL; elem = elem->NextSibling("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble")) {

                for (elem2 = elem->FirstChild("position"); elem2 != NULL; elem2 = elem2->NextSibling("position")) {
                    child = elem2->FirstChild();

                    child2 = elem2->NextSibling();
                    child2 = child2->FirstChild();

                    TiXmlText *text = child->ToText();
                    valParam = text->Value();
                    valParam1 = atof(valParam.c_str());

                    text = child2->ToText();
                    valParam2 = text->Value();
                    valParam3 = atof(valParam2.c_str());

                    reitFrequency->set(valParam1, valParam3);

                }
            }
            reitFrequency->setName(val);
            this->addParameter(key, reitFrequency);

        }


        if (val == "ramificationSets") {
            int ramicCount = 0;


            for (TiXmlNode *elem4 = noeud->FirstChild("jeeb.workspace.digr.DigRParam_-RamificationSet");
                 elem4 != NULL;
                 elem4 = elem4->NextSibling("jeeb.workspace.digr.DigRParam_-RamificationSet"), ramicCount++) {


                for (elem = elem4->FirstChild(); elem != NULL; elem = elem->NextSibling()) {
                    string val2 = elem->Value();

                    if (val2 == "name") {
                        string name = "nameRamification";
                        TiXmlNode *elem2 = elem->FirstChild();
                        TiXmlText *text = elem2->ToText();
                        valParam = text->Value();
                        valParam1 = atof(valParam.c_str());
                        key = formatName(name + std::to_string(indexType), ramicCount);

                        SingleValueParameter *nameRamification = (SingleValueParameter *) ParamFactory::instance().createParameter(
                                "SingleValueParameter");
                        if (nameRamification != 0) {

                            nameRamification->setName(val2);
                            nameRamification->set(valParam1);

                            this->addParameter(key, nameRamification);
                        }
                    }
                    if (val2 == "typeFrequency") {

                        key = formatName(val2 + std::to_string(indexType), ramicCount);

                        ParametricParameter *typeFrequency = (ParametricParameter *) ParamFactory::instance().createParameter(
                                "ParametricParameter");

                        for (elem3 = elem->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleIntegerDouble");
                             elem3 != NULL; elem3 = elem3->NextSibling(
                                "jeeb.workspace.digr.DigRParam_-MyCoupleIntegerDouble")) {
                            string elem4 = elem3->Value();

                            for (elem2 = elem3->FirstChild(); elem2 != NULL; elem2 = elem2->NextSibling("index")) {

                                child = elem2->FirstChild();

                                child2 = elem2->NextSibling();
                                child2 = child2->FirstChild();

                                TiXmlText *text = child->ToText();
                                valParam = text->Value();
                                valParam1 = atof(valParam.c_str());

                                text = child2->ToText();
                                valParam2 = text->Value();
                                valParam3 = atof(valParam2.c_str());

                                typeFrequency->set(1., valParam1, valParam3);

                            }
                        }
                        typeFrequency->setName(val2);
                        this->addParameter(key, typeFrequency);
                    }
                    if (val2 == "TypeFrequencyStdDev") {
                        elem2 = elem->FirstChild();
                        TiXmlText *text = elem2->ToText();
                        valParam = text->Value();
                        valParam1 = atof(valParam.c_str());
                        key = formatName(val2 + std::to_string(indexType), ramicCount);

                        SingleValueParameter *typeFrequencyStdDev = (SingleValueParameter *) ParamFactory::instance().createParameter(
                                "SingleValueParameter");
                        if (typeFrequencyStdDev != 0) {

                            typeFrequencyStdDev->setName(val2);
                            typeFrequencyStdDev->set(valParam1);

                            this->addParameter(key, typeFrequencyStdDev);
                        }
                    }
                    if (val2 == "interRamifDistance") {
                        key = formatName(val2 + std::to_string(indexType), ramicCount);

                        ParametricParameter *interRamifDistance = (ParametricParameter *) ParamFactory::instance().createParameter(
                                "ParametricParameter");

                        for (elem3 = elem->FirstChild("jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble");
                             elem3 != NULL; elem3 = elem3->NextSibling(
                                "jeeb.workspace.digr.DigRParam_-MyCoupleDoubleDouble")) {

                            for (elem2 = elem3->FirstChild("position");
                                 elem2 != NULL; elem2 = elem2->NextSibling("position")) {

                                child = elem2->FirstChild();

                                child2 = elem2->NextSibling();
                                child2 = child2->FirstChild();

                                TiXmlText *text = child->ToText();
                                valParam = text->Value();
                                valParam1 = atof(valParam.c_str());

                                text = child2->ToText();
                                valParam2 = text->Value();
                                valParam3 = atof(valParam2.c_str());

                                interRamifDistance->set(1., valParam1, valParam3);

                            }
                        }
                        interRamifDistance->setName(val2);
                        this->addParameter(key, interRamifDistance);
                    }
                    if (val2 == "InterRamifDistanceStdDev") {
                        elem2 = elem->FirstChild();
                        TiXmlText *text = elem2->ToText();
                        valParam = text->Value();
                        valParam1 = atof(valParam.c_str());
                        key = formatName(val2 + std::to_string(indexType), ramicCount);

                        SingleValueParameter *interRamifDistanceStdDev = (SingleValueParameter *) ParamFactory::instance().createParameter(
                                "SingleValueParameter");
                        if (interRamifDistanceStdDev != 0) {

                            interRamifDistanceStdDev->setName(val2);
                            interRamifDistanceStdDev->set(valParam1);

                            this->addParameter(key, interRamifDistanceStdDev);

                        }
                    }
                }

            }

            key = formatName(val, indexType);

            SingleValueParameter *ramificationSets = (SingleValueParameter *) ParamFactory::instance().createParameter(
                    "SingleValueParameter");
            if (ramificationSets != 0) {

                ramificationSets->setName(val);
                ramificationSets->set(ramicCount);

                this->addParameter(key, ramificationSets);
            }

        }

    } else if (p == TiXmlNode::UNKNOWN) {

    } else if (p == TiXmlNode::DOCUMENT) {

    } else if (p == TiXmlNode::COMMENT) {

    } else {

    }

    if (!noeud->NoChildren()) {
        suivant = noeud->FirstChild();
        appliquer(suivant);

    }

    suivant = noeud->NextSibling();

    if (suivant != NULL) {
        appliquer(suivant);
    }


}


void ParamSetDigR::readParameter(TiXmlNode *pParent, unsigned int indent) {
    if (!pParent) return;

    appliquer(pParent);

    SingleValueParameter *nbType = (SingleValueParameter *) ParamFactory::instance().createParameter(
            "SingleValueParameter");

    nbType->setName("nbType");
    nbType->set(indexType + 1);

    this->addParameter("nbType", nbType);
}
