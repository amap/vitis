/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "RootSystem.h"
#include "ParamFile.h"
#include "scheduler.h"
#include "Root.h"
#include "Segment.h"
#include "defROOT.h"

Segment::Segment(Plant * plt):DecompAxeLevel(plt, SEGMENT)
{
	growthEnd = 0;/* growth end flag */
	length = 1;
	pos = 0;
	time = Scheduler::instance()->getTopClock();
	transformation = 0;
}


float Segment::computeDiam (float initialDiameter, float evolDiamStdDev, float timeEvolDiamStdDev, float delayEvolDiamStdDev )
{
	double age, evolDiam, timeEvolDiam, delayEvolDiam, diam, aux;
	ParamSet *param = (ParamSetDigR *)getPlant()->getParamData(string("DigRMod"));

	/* initial diameter */
	diam = initialDiameter;

	// compute current age
	age = Scheduler::instance()->getTopClock() - Time();

	int type = ((Root*)getParentOfLevel(0))->Type();

	evolDiam = ((ParametricParameter*)param->getParameter("diameterIncreaseRatio", type))->value ((float)pos, getParentOfLevel(0), ((Branc*)getParentOfLevel(0))->getPlant());
	timeEvolDiam = ((ParametricParameter*)param->getParameter("diameterIncreaseTime",type))->value ((float)pos,getParentOfLevel(0), ((Branc*)getParentOfLevel(0))->getPlant());
	delayEvolDiam = ((ParametricParameter*)param->getParameter("diameterIncreaseDelay",type))->value ((float)pos,getParentOfLevel(0), ((Branc*)getParentOfLevel(0))->getPlant());

	evolDiam += evolDiamStdDev;
	timeEvolDiam += timeEvolDiamStdDev;
	delayEvolDiam += delayEvolDiamStdDev;

	/* diam adjustment according to nodes age */
	if (age >= delayEvolDiam){
        if (evolDiam > 0 && age-delayEvolDiam < timeEvolDiam){
							double aa = diam * (evolDiam -1) / timeEvolDiam;
							double bb = diam;
							diam = aa * (age-delayEvolDiam) + bb;
						}else if (evolDiam > 0){
							diam *= evolDiam;
						}
	}

	return (float)diam;
}

void Segment::serialize(Archive& ar )
{

	DecompAxeLevel::serialize(ar);

	if(ar.isWriting() )
	{
		ar<<length<<growthEnd<<time<<pos;
	}
	else
	{
		ar>>length>>growthEnd>>time>>pos;
	}
}

Root *Segment::getComplex() const {
    return get_complex;
}

void Segment::setComplex(Root *get_complex) {
    Segment::get_complex = get_complex;
}

Matrix4 *Segment::getTransformation() const {
	return transformation;
}

void Segment::setTransformation(Matrix4 *transformation) {
	Segment::transformation = transformation;
}



