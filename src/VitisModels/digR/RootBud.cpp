/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "ParamFile.h"
#include "RootSystem.h"
#include "randomFct.h"
#include "defROOT.h"
#include "GrowthEngineRoot.h"
#include "Root.h"
#include "RootBud.h"
#include <iostream>
#include <math.h>
#include <typeinfo>

RootBud::RootBud() {
    growthSpeed = NULL;
    timeGrowthSpeed = NULL;
    ramifOrder = 1;
    rootRamificationType = 0;
    birthTime = 0;
    death = false;
    lastDeathTestTime = 0;
    lastDeathTestPosition = 0;
    lastGrowthTime = 0;
    lastGrowthPosition = 0;
    nextInstant = 0;
    paramSet = NULL;
    root = NULL;
    groEngine = NULL;
    nextTime = 0;
    toBePruned = false;
}

RootBud::RootBud(Root *br, double timeC, int phyAgeBeg) : VProcess() {

    growthSpeed = NULL;
    timeGrowthSpeed = NULL;
    ramifOrder = 1;
    rootRamificationType = 0;
    birthTime = 0;
    death = false;
    lastDeathTestTime = 0;
    lastDeathTestPosition = 0;
    lastGrowthTime = timeC;
    lastGrowthPosition = 0;
    nextInstant = 0;
    paramSet = NULL;
    root = NULL;
    groEngine = NULL;
    nextTime = 0;
    toBePruned = false;

    root = br;

    birthTime = timeC;

    paramSet = (ParamSetDigR *) br->getPlant()->getParamData(string("DigRMod"));

    Root *bear = br->getBearer();
    if (   bear == br
        || bear->getClassType() != br->getClassType())
        ramifOrder = 1;
    else
        ramifOrder = bear->getBud()->ramifOrder + 1;

    groEngine = new GrowthEngineRoot(this, phyAgeBeg);

//    v_config = new VitisConfig();
//    v_config->initRandomGenerator();

    ParametricParameter *tempo = (ParametricParameter *) paramSet->getParameter("growthSpeed", getRootType());

    growthSpeed = new ParametricParameter(*tempo);
    growthSpeed->setSignal (paramSet->parameterSignal());

    growthSpeed->setCurrentInterpolationY(true);

    SingleValueParameter *globalPercentOnGrowthSpeed
            = (SingleValueParameter *) paramSet->getParameter("GlobalPercentOnGrowthSpeed", getRootType());

    if (globalPercentOnGrowthSpeed->value(root, root->getPlant()) != 0) {
        double coeff = 1 + (root->getPlant()->getConfigData().getRandomGenerator()->Random() - 0.5) * globalPercentOnGrowthSpeed->value(root, root->getPlant()) * 2 / 100;
//        for (float &i : growthSpeed->getCurrentValTab()) {
        for (int i=0; i<growthSpeed->CurrentNb_Y(); i++) {
            growthSpeed->set (growthSpeed->getCurrentYTab()[i], growthSpeed->getCurrentValTab()[i] *coeff);
        }
    }

    constructTimeGrowthSpeed();
}

void RootBud::constructTimeGrowthSpeed() {

    timeGrowthSpeed = (ParametricParameter *) ParamFactory::instance().createParameter("ParametricParameter");
    timeGrowthSpeed->setSignal (paramSet->parameterSignal());
    double p1, v1, time;
    if (growthSpeed->CurrentNb_Y() > 0) {
        p1 = growthSpeed->getCurrentYTab()[0];
        v1 = growthSpeed->getCurrentValTab()[0];
        time = p1 / v1;
        timeGrowthSpeed->set((float) time, (float) v1);
        for (int i = 1; i < growthSpeed->CurrentNb_Y(); i++) {
            if (growthSpeed->getCurrentValTab()[i] - v1 != 0) {
                time +=   (growthSpeed->getCurrentYTab()[i] - p1)
                        / (growthSpeed->getCurrentValTab()[i] - v1)
                        * std::log(growthSpeed->getCurrentValTab()[i] / v1);
            } else {
                time += growthSpeed->getCurrentYTab()[i] / growthSpeed->getCurrentValTab()[i];
            }
            timeGrowthSpeed->set((float) time, (float) growthSpeed->getCurrentValTab()[i]);
            p1 = growthSpeed->getCurrentYTab()[i];
            v1 = growthSpeed->getCurrentValTab()[i];
        }
    }
}


RootBud::~RootBud(void) {
    Scheduler::instance()->stop_process(this->process_id);

    delete groEngine;

    delete growthSpeed;

    delete timeGrowthSpeed;

}


void RootBud::process_event(EventData *e) {

    if (e != nullptr) {
        growthData *groEve = reinterpret_cast<growthData *>(e);
        if (groEve->Time() > Scheduler::instance()->getHotStop()) {
            delete e;
            return;
        }
    }
    //initial event
    if (e == nullptr) {

        double ramifSet = 0;

        SingleValueParameter *ramificationSets =
                (SingleValueParameter *) paramSet->getParameter("ramificationSets", this->getRootType());
        if (ramificationSets) {
            ramifSet = ramificationSets->value(root, root->getPlant());
        }


        //current time
        double time = Scheduler::instance()->getTopClock();

        for (int index = 0; index < ramifSet; index++) {
            RamificationGrowthBudEvent *ev = new RamificationGrowthBudEvent(index, lastGrowthPosition, time);
            groEngine->setNextRamification(ev);
        }

        //safe check for MaxReit step otherwise 0.
        double maxReitVal = 0;
        SingleValueParameter *maxReit = (SingleValueParameter *) paramSet->getParameter("MaxReit", getRootType());
        if (maxReit)
            maxReitVal = maxReit->value(root, root->getPlant());


        if (maxReitVal > 0) {

            ReiterationGrowthBudEvent *rEv = new ReiterationGrowthBudEvent(static_cast<unsigned short>(getRootType()), lastGrowthPosition, time);

            groEngine->setNextReiterationEvent(rEv);

        }

        if (   ramifSet == 0
            && maxReitVal == 0){

            double len = groEngine->expIntegrate(this, 0, 1);
            RamificationGrowthBudEvent *ev = new RamificationGrowthBudEvent(-1, lastGrowthPosition, time, len);
            groEngine->setNextRamification(ev);

        }
    }

        //type events....
    else if (e->getClassType() == "DeathBudEvent") {
        pruning(dynamic_cast<DeathBudEvent *>(e));
    } else if (e->getClassType() == "RamificationGrowthBudEvent") {
        groEngine->nextRamification(dynamic_cast<RamificationGrowthBudEvent *>(e));
    } else if (e->getClassType() == "ReiterationGrowthBudEvent") {
        groEngine->nextReiteration(dynamic_cast<ReiterationGrowthBudEvent *>(e));
    } else {
        growthData *groEv = dynamic_cast<growthData *>(e);
        groEngine->setSegment(groEv->Time(), groEv->Pos(), groEv->getLength());
    }

}


short RootBud::testDeath(double _time) {

    double deathProba = 0;
    short pruned = 0;
    int type = getRootType();

    toBePruned = 0;
    while (lastDeathTestTime + birthTime < _time && !death) {

        //extracting deathProba.
        ParametricParameter *deathProbaPara = (ParametricParameter *) paramSet->getParameter("deathProba", type);
        if (deathProbaPara) {
            deathProbaPara->setCurrentInterpolationY(true);
            deathProba = deathProbaPara->value((float) lastDeathTestPosition, root, root->getPlant());
        }

        double d = getConfigData().getRandomGenerator()->Random();

        if (deathProba > d) {
            death = true;
            //extracting pruningExceptionPercent.
            double pruningExceptionPercent = 0;
            SingleValueParameter *pruningExceptionPercentPara =
                    (SingleValueParameter *) paramSet->getParameter("PruningExceptionPercent", type);
            if (pruningExceptionPercentPara) { pruningExceptionPercent = pruningExceptionPercentPara->value(root, root->getPlant()); }

            d = getConfigData().getRandomGenerator()->Random() * 100;

            if (d >= pruningExceptionPercent) {

                root->startPosit();
                root->positOnFirstSegmentWithinRoot();
                Segment *s = root->getCurrentSegment();
                root->endPosit();

                if (s != NULL) {

                    //extracting pruningLag.
                    double pruningLag = 0;
                    SingleValueParameter *pruningLagPara = (SingleValueParameter *) paramSet->getParameter("PruningLag", type);
                    if (pruningLagPara) {
                        pruningLag = pruningLagPara->value(root, root->getPlant());
                    }

                    //extracting pruningLag.
                    double pruningLagStdDev = 0;
                    SingleValueParameter *pruningLagStdDevPara = (SingleValueParameter *) paramSet->getParameter("PruningLagStdDev", type);
                    if (pruningLagStdDevPara) {
                        pruningLagStdDev = pruningLagStdDevPara->value(root, root->getPlant());
                    }

                    //GaussianSimulation
                    double normalNoise = getConfigData().getRandomGenerator()->nextGaussian() * pruningLagStdDev;
                    pruningLag = pruningLag + normalNoise;

                    double pruningTime = lastDeathTestTime + birthTime + pruningLag;

                    if (pruningTime <= Scheduler::instance()->getTopClock()) {
                        pruningTime = Scheduler::instance()->getTopClock();
//                        pruning();
                        pruned = 1;
                        toBePruned = 1;
                        DeathBudEvent *ev = new DeathBudEvent(pruningTime);
                        Scheduler::instance()->signal_event(process_id, ev, pruningTime, Scheduler::instance()->getTopPriority());
                    }
                    else {

                        DeathBudEvent *ev = new DeathBudEvent(pruningTime);

                        Scheduler::instance()->signal_event(process_id, ev, pruningTime, Scheduler::instance()->getTopPriority());
                    }
                }
            }


            break;

        }
        if (lastDeathTestTime + birthTime == _time) {
            break;
        }

        double _nextTime = lastDeathTestTime + 1;
        if (_nextTime > _time) {
            _nextTime = _time;
        }

        lastDeathTestPosition += groEngine->expIntegrate(this, lastDeathTestTime, _nextTime);
        lastDeathTestTime = _nextTime;
    }


    return pruned;
}


void RootBud::updateHierar(Hierarc *) {
    //rien
}

void RootBud::updatePruning() {

}

void RootBud::pruning(DeathBudEvent *e) {
    pruning();
    if (e) {
        delete e;
        e = NULL;
    }
}

void RootBud::clearEvents(RootBud *b) {
    if (b->getRoot() != 0){
        for (int i=0; i < b->getRoot()->getCurrentHierarc()->getNbBorne(); i++) {
            clearEvents ((RootBud*)((Root*)b->getRoot()->getCurrentHierarc()->getBorne(i))->getBud());
        }
    }

    if (!Scheduler::instance()->clear(b))
        cout << "strange" << endl;
}

void RootBud::pruning() {
    clearEvents (this);

    // empty the root.
    root->getPlant()->getTopology().removeBranc(root, 1);
}


void RootBud::serialize(Archive &ar) {

    VProcess::serialize(ar);

    if (ar.isWriting()) {
        ar << ramifOrder << rootRamificationType << death;

        ar << nextInstant << lastDeathTestPosition << lastDeathTestTime << nextTime << toBePruned;

        if (this->SavePtr(ar, root)) {
            std::cout << "It should be saved before";
        }

        ar << *groEngine;
    } else {
        ar >> ramifOrder >> rootRamificationType >> death;

        ar >> nextInstant >> lastDeathTestPosition >> lastDeathTestTime >> nextTime >> toBePruned;

        ArchivableObject *arBr;
        if (this->LoadPtr(ar, arBr)) {
            std::cout << "It would be saved before";
        } else
            root = (Root *) arBr;

        groEngine = new GrowthEngineRoot(this);

        paramSet = (ParamSetDigR *) root->getPlant()->getParamData(string("DigRMod"));
        ar >> *groEngine;
    }
}

VitisConfig &RootBud::getConfigData()
{
    Plant *plant = root->getPlant();
    return plant->getConfigData();
}



