/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file GrowthEngineAMAP.h
///			\brief Definition of GrowthEngineAMAP class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef __GROWTHENGINEROOT__
#define __GROWTHENGINEROOT__

#include "ArchivableObject.h"
#include <math.h>
#include "Segment.h"

using namespace Vitis;

class growthData : public EventData {
private :
    USint lpIndex; // root ramification index that setup this event
    double pos; // position for this ramification
    double time; // birth time
    double length; //length of this ramification

public :
    growthData(double t) : EventData() { time = t; };

    growthData(USint l, double p, double t) : EventData() {
        lpIndex = l;
        pos = p;
        time = t;
        setLength(1);
    };

    growthData(USint l, double p, double t, double len) : EventData() {
        lpIndex = l;
        pos = p;
        time = t;
        setLength(len);
    };

    int LpIndex() { return lpIndex; };

    double Pos() { return pos; };

    double Time() { return time; };

    double getLength() { return length; };

    USint LpIndex(USint l) { lpIndex = l; };

    void Pos(double p) { pos = p; };

    void setLength(double l) { length = l; };

    void Time(double t) { time = t; };

    void serialize(Archive &ar) {
        if (ar.isWriting()) {
            ar << lpIndex << pos << time;
        } else {
            ar >> lpIndex >> pos >> time;
        }
    }


};

class DeathBudEvent : public growthData {

public:

    DeathBudEvent(double t) : growthData(t) {
        setClassType("DeathBudEvent");
    };

};

class RamificationGrowthBudEvent : public growthData {

public:

    RamificationGrowthBudEvent(USint l, double p, double t) : growthData(l, p, t) {
        setClassType("RamificationGrowthBudEvent");
    };

    RamificationGrowthBudEvent(USint l, double p, double t, double len) : growthData(l, p, t, len) {
        setClassType("RamificationGrowthBudEvent");
    };

};

class ReiterationGrowthBudEvent : public growthData {

public:
    ReiterationGrowthBudEvent(USint l, double p, double t) : growthData(l, p, t) {
        setClassType("ReiterationGrowthBudEvent");
    };

    ReiterationGrowthBudEvent(USint l, double p, double t, double len) : growthData(l, p, t, len) {
        setClassType("ReiterationGrowthBudEvent");
    };

};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GrowthEngineRoot
///			\brief Definition of GrowthEngineRoot class
///
///			\li Update topology of an axe by adding new segment according axis reference data
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GrowthEngineRoot : public ArchivableObject {

public:

    /// \brief reference axis index (integer phy age)
    int rootType;

    /// \brief ramification process number
    int lpNumber;

    /// \brief pointer to the bud
    class RootBud *theBud;

    /// \brief pointer to the Root.
    class Root *theRoot;

    /// \brief Default Constructor
    GrowthEngineRoot(class RootBud *b = NULL);

    /// \brief Constructor
    /// \param b pointer to the bud
    /// \param phyAgeBeg bud phyage
    /// \param ryt growth rythme
    GrowthEngineRoot(class RootBud *b, int rootTypeBeg);

    /// \brief Destructor
    ~GrowthEngineRoot(void);

    /// \brief Serialize a GrowthEngineAMAP
    /// \param ar A reference to the archive \see Archive
    virtual void serialize(Archive &ar);

    /**
     *
     * @param rBud
     * @param _beginTime
     * @param _endTime
     * @return the exponential integral between _beginTime and _endTime using the rBud growth speed and growth speed.
     */
    double expIntegrate(RootBud *rBud, double _beginTime, double _endTime);

    /**
     *
     * @param t
     * @param p
     * @param time
     * @return help method for expIntegrate
     */
    double individualExpIntegrate(ParametricParameter *t, ParametricParameter *p, double time);

    /// set the next ramification according to ramification mix
    /// death may occur and avoid ramification
    void nextRamification(RamificationGrowthBudEvent *d);

    /**
     * calculate the attribute for a given RamificationGrowthBudEvent.
     * @param d
     */
    void setNextRamification(RamificationGrowthBudEvent *d);

    ///adds new Root to RootSystem, when ramification happens.
    void setRamification(int id, Segment *bearer, double position, double time);

    /**
     *
     * @param age
     * @return the same segment growed to the given age, null when death happens.
     */
    Segment *growTo(double age);

    /**
     *
     * @param _time
     * @param _position
     * @param _length
     * @return new segment instance with the given parameter, further his attribute may change after some calculation.
     */
    Segment *setSegment(double _time, double _position, double _length);

    /**
     * calculate the attribute for a given ReiterationGrowthBudEvent.
     * @param d
     */
    void setNextReiterationEvent(ReiterationGrowthBudEvent *d);

    /// set the next reiteration
    /// death may occur and avoid reiteration
    void nextReiteration(ReiterationGrowthBudEvent *ev);

    ///adds new Root to RootSystem, when reiteration happens.
    void setReiteration(Segment *pSegment, double position, double time);

private:

    /// \brief initialisation function
    void init(RootBud *b);

    float *ramifTypeFrequency; // frequency of each type
    float **ramifTypeFrequencyNoise; // frequency perturbation of each type
};

#endif

