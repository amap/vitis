/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 28.08.18.
//

#include "Mesh.h"

Mesh::Mesh() : SimpleMesh(){

    enableScaling = true;
}

Mesh::Mesh(std::vector<Vector3>& pts)
        : SimpleMesh(pts) {

    enableScaling = true;

}

Mesh::Mesh(std::vector<Vector3> &pts, std::vector<std::vector<int>> &connections)
        : SimpleMesh(pts, connections) {

    enableScaling = true;

}

Mesh::Mesh(std::vector<Vector3> &points, std::vector<std::vector<int>> &paths, std::vector<Vector3> &normals)
        : SimpleMesh(points, paths, normals) {

    enableScaling = true;

}


bool Mesh::isEnableScaling() const {
    return enableScaling;
}

void Mesh::setEnableScaling(bool enableScaling) {
    Mesh::enableScaling = enableScaling;
}


Mesh::~Mesh() = default;

void Mesh::newRectangularMesh(int m, int n) {
    paths.clear();
    paths.resize(((const size_t)(m * n)), {});
    for(std::vector<int>& e : paths)
        e.resize(4,0);

    for (int k = 0; k < n; k++) {
        for (int j = 0; j < m; j++) {
            int f = k * m + j;
            int v = k * (m + 1) + j;
            paths[f][3] = v;
            paths[f][2] = v + 1;
            paths[f][1] = v + m + 1 + 1;
            paths[f][0] = v + m + 1;
        }
    }

//    for (int i = 0; i < ((m + 1) * (n + 1)); i++) {
//        points.emplace_back(0);
//        normals.emplace_back(0);
//    }
    points.clear();
    normals.clear();
    points.resize((m + 1) * (n + 1));
    normals.resize((m + 1) * (n + 1));
}

void Mesh::rectangularMesh(int m, int n) {

    newRectangularMesh(m,n);
    Vector3 vertex(0, 0, 0);
    Vector3 normal(0, 0, 1);

    for (int k = 0; k <= n; k++) {
        for (int j = 0; j <= m; j++) {
            vertex[0] = ((float) (2. * j / m - 1));
            vertex[1] = ((float) (2. * k / n - 1));

            points[k * (m + 1) + j] = vertex;
            normals[k * (m + 1) + j] = normal;

        }
    }
}
