/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 24.08.18.
//

#include <algorithm>
#include "ExtrudedMesh.h"




ExtrudedMesh::ExtrudedMesh(std::vector<std::vector<double>> &O, std::vector<std::vector<double>> &P,
                           std::vector<double> &SW, std::vector<double> &SH, bool closed) : Mesh() {
    section.clear();
    axisPath.clear();
    sectionWidth.clear();
    sectionHeight.clear();
    epsilon = 0.01;

    wire(O, P, SW, SH, closed);

    triangulate();
}

void ExtrudedMesh::wire(std::vector<std::vector<double>> &section, std::vector<std::vector<double>> &pathArray,
                        std::vector<double> &w, std::vector<double> &h, bool closed) {
    extrusion(section, pathArray, w, h, closed);
    computeNormals();
}

void ExtrudedMesh::extrusion(std::vector<std::vector<double>> &O, std::vector<std::vector<double>> &P,
                             std::vector<double> &SW, std::vector<double> &SH, bool closed) {

    saveParam(O, P, SW, SH);

    int m = static_cast<int>(O.size() - 1);
    int n = static_cast<int>(P.size() - 1);

    rectangularMesh(m, n);

    std::array<double, 3> U;
    std::array<double, 3> V;
    std::array<double, 3> W;

    bool loop = same(P[0], P[n]);

    int N = 0;
    double distance = 0;
    for (int j = 0; j <= n; j++) {
        for (int k = 0; k < 3; k++) {
            U[k] = P[j][k + 3];
            W[k] = j == n ? (loop ? P[1][k] - P[0][k] : P[n][k] - P[n - 1][k]) : P[j + 1][k] - P[j][k];
        }


    Vector3 Uv(static_cast<const float &>(U[0]), static_cast<const float &>(U[1]), static_cast<const float &>(U[2]));
    Vector3 Wv(static_cast<const float &>(W[0]), static_cast<const float &>(W[1]), static_cast<const float &>(W[2]));
    Vector3 Vv;

    double norma = norm(Wv) / 100.0;

    if(norm(Wv) != 0 && norm(Uv) != 0) {
        Wv.normalize ();

        Uv.normalize ();

        Vv = cross(Wv, Uv);
        Vv.normalize ();

        Uv = cross(Vv, Wv);
        Uv.normalize ();

        for (int i = 0; i < 3; ++i) {
            U[i] = Uv[i];
            V[i] = Vv[i];
            W[i] = Wv[i];
        }
    }

    double width = SW.empty() ? 1 : SW[j];
    double height = SH.empty() ? 1 : SH[j];

        for (int i = 0; i <= m; ++i) {
            double x = O[i][0] * width;
            double y = O[i][1] * height;
            double z = O[i][2];

            points[N] = Vector3 ((float) (P[j][0] + (x * U[0] - y * V[0] + z * W[0])),
                                     (float) (P[j][1] + (x * U[1] - y * V[1] + z * W[1])),
                                     (float) (P[j][2] + (x * U[2] - y * V[2] + z * W[2])));


            N++;
        }

        distance+=norma;
    }

    if (loop) {
        for (int i = 0; i <= points.size(); i++) {
            if (indx (m, n, n) < points.size()) {
                points[indx (m, n, n)][0] = ( points[indx (m, n, 0)].x() );
                points[indx (m, n, n)][1] = ( points[indx (m, n, 0)].y() );
                points[indx (m, n, n)][2] = ( points[indx (m, n, 0)].z() );
            }
        }
    }

    if(closed){

        std::vector< std::vector<int> > savPaths(paths);
        std::vector< Vector3 > savPoints(points);

        paths.clear();
        points.clear();

        paths.resize(m*n +2, {});
        for(std::vector<int>& i : paths){
            i.resize(4, 0);
        }

        for (int i = 0; i < (m + 1) * (n + 3); i++) {
            points.emplace_back(0);
        }
        points.resize((m + 1) * (n + 3));

        for (int i = 0; i < m * n; i++) {
            paths[i] = savPaths[i];
        }
        for (int i = 0; i < (m + 1) * (n + 1); i++) {
            points[i] = savPoints[i];

        }

        // add the beginning face
        paths[m * n].resize(m + 1, 0);
        for (int i = 0; i <= m; i++) {
            paths[m * n][i] = (m + 1) * (n + 1) + i;
            points[(m + 1) * (n + 1) + i] = Vector3 (savPoints[i]);

        }

        // add the ending face
        paths[m * n + 1].resize(m + 1, 0);
        for (int i = 0; i <= m; i++) {
            paths[m * n + 1][i] = static_cast<int>(points.size() - i - 1);
            points[(m + 1) * (n + 2) + i] = Vector3 (savPoints[savPoints.size() - i - 1]);

        }

        std::reverse(paths[m * n + 1].begin(),paths[m * n + 1].end());

    }

}

void ExtrudedMesh::saveParam(std::vector<std::vector<double>> &section2, std::vector<std::vector<double>> &axisPath2,
                             std::vector<double> &sectionHeight2, std::vector<double> &sectionWidth2) {

    section = section2;
    axisPath = axisPath2;
    sectionHeight = sectionHeight2;
    sectionWidth = sectionWidth2;


}

bool ExtrudedMesh::same(std::vector<double> a, std::vector<double> b) {
    return    std::abs(a[0] - b[0]) < epsilon
           && std::abs(a[1] - b[1]) < epsilon
           && std::abs(a[2] - b[2]) < epsilon;
}

int ExtrudedMesh::indx(int m, int n, int j) {
    return j * (m + 1);
}

ExtrudedMesh::~ExtrudedMesh() {

}

const std::vector<std::vector<double>> &ExtrudedMesh::getAxisPath() const {
    return axisPath;
}

void ExtrudedMesh::setAxisPath(const std::vector<std::vector<double>> &axisPath) {
    ExtrudedMesh::axisPath = axisPath;
}


