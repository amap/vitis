/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 29.08.18.
//

#include "BoundingBox3f.h"
#include <limits>

Vector3 *BoundingBox3f::getMin() const {
    return min;
}

void BoundingBox3f::setMin(Vector3 *min) {
    BoundingBox3f::min = min;
}

Vector3 *BoundingBox3f::getMax() const {
    return max;
}

void BoundingBox3f::setMax(Vector3 *max) {
    BoundingBox3f::max = max;
}

BoundingBox3f::BoundingBox3f() {
    min = new Vector3(std::numeric_limits<float>::max(),std::numeric_limits<float>::max(),std::numeric_limits<float>::max());
    max = new Vector3(-std::numeric_limits<float>::max(),-std::numeric_limits<float>::max(),-std::numeric_limits<float>::max());
}

BoundingBox3f::BoundingBox3f(Vector3 *inf, Vector3 *sup) : min(inf), max(sup) {}

Vector3 BoundingBox3f::getMin(BoundingBox3f bb) {
    Vector3 temp(0,0,0);

    if (getMin()->x() < bb.getMin()->x())
        temp[0] =(getMin()->x());
    else
        temp[0] =(bb.getMin()->x());

    if (getMin()->y() < bb.getMin()->y())
        temp[1] =(getMin()->y());
    else
        temp[1] =(bb.getMin()->y());

    if (getMin()->z() < bb.getMin()->z())
        temp[2] =(getMin()->z());
    else
        temp[2] =(bb.getMin()->z());

    return temp;
}

Vector3 BoundingBox3f::getMax(BoundingBox3f bb) {
    Vector3 temp(0, 0, 0);

    if (getMax()->x() > bb.getMax()->x())
        temp[0] =(getMax()->x());
    else
        temp[0] =(bb.getMax()->x());

    if (getMax()->y() > bb.getMax()->y())
        temp[1] =(getMax()->y());
    else
        temp[1] =(bb.getMax()->y());

    if (getMax()->z() > bb.getMax()->z())
        temp[2] =(getMax()->z());
    else
        temp[2] =(bb.getMax()->z());

    return temp;
}

void BoundingBox3f::update(Vector3 p) {
    (*min)[0] = (std::min(min->x(), p.x()));
    (*min)[1] = (std::min(min->y(), p.y()));
    (*min)[2] = (std::min(min->z(), p.z()));

    (*min)[0] = (std::max(max->x(), p.x()));
    (*min)[1] = (std::max(max->y(), p.y()));
    (*min)[2] = (std::max(max->z(), p.z()));
}

    /**
	 * method used to translate a bounding box
	 *
	 * @param x
	 * @param y
	 * @param z
	 */
void BoundingBox3f::translateBB(float x, float y, float z) {
    getMax()->add(Vector3(x, y, z));
    getMin()->add(Vector3(x, y, z));
}

bool BoundingBox3f::in(Vector3 p) {
    return    p.x() > min->x()
           && p.y() > min->y()
           && p.z() > min->z()
           && p.x() < max->x()
           && p.y() < max->y()
           && p.z() < max->z();
}

bool BoundingBox3f::out(Vector3 p) {
    return    p.x() < min->x()
           || p.y() < min->y()
           || p.z() < min->z()
           || p.x() > max->x()
           || p.y() > max->y()
           || p.z() > max->z();
}

bool BoundingBox3f::on(Vector3 p) {
    return    p.x() == min->x()
           || p.y() == min->y()
           || p.z() == min->z()
           || p.x() == max->x()
           || p.y() == max->y()
           || p.z() == max->z();
}

