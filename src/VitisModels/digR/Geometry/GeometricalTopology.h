/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 29.08.18.
//

#ifndef THEVITISNEW_GEOMETRICALTOPOLOGY_H
#define THEVITISNEW_GEOMETRICALTOPOLOGY_H

#include "Vector.h"

class GeometricalTopology {

    virtual bool in (Vector3 p) = 0;
    virtual bool out (Vector3 p) = 0;
    virtual bool on (Vector3 p) = 0;
};


#endif //THEVITISNEW_GEOMETRICALTOPOLOGY_H
