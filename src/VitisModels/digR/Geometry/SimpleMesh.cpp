/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 29.08.18.
//

#include "SimpleMesh.h"

SimpleMesh::SimpleMesh() {
    triangulated = false;
    points = {};
    normals = {};
    paths = {};
    isClosedVolume = false;
}

SimpleMesh::SimpleMesh(std::vector<Vector3>& p) : SimpleMesh() {
    points = p;
    std::vector<int> temp{};
    temp.reserve(p.size());
    for (int i = 0; i < p.size(); ++i) {
        temp.push_back(i);
    }
    paths.push_back(temp);

    triangulate();
}

SimpleMesh::SimpleMesh(std::vector<Vector3> &pts, std::vector<std::vector<int>> &pats) : SimpleMesh() {
    points = pts;
    paths = pats;

    triangulate();
}

SimpleMesh::SimpleMesh(std::vector<Vector3> &pts, std::vector<std::vector<int>> &pats,
                       std::vector<Vector3> &nls) : SimpleMesh(){
    points = pts;
    paths = pats;
    normals = nls;

    triangulate();
}


void SimpleMesh::triangulate() {
    purifyMesh();

    std::vector<std::vector<int>> faceList{};

    for(int f = 0; f < paths.size() ; ++f){
        std::vector<int> &face = paths[f];

        if(face.size() >= 3){
            int vertex = 0;
            while(vertex < face.size() - 2){
                std::vector<int> newFace{};
                newFace.push_back(face[0]);
                for (int i = 1; i < 3; ++i) {
                    newFace.push_back(face[(vertex + i) % face.size()]);
                }
                faceList.push_back(newFace);
                vertex++;
            }
        }
    }

//    paths = faceList;
    paths.assign (faceList.begin(), faceList.end());
    triangulated = true;
}

void SimpleMesh::purifyMesh() {


    std::vector< std::vector<int> > faceList;

    faceList = paths;
    paths.clear();

    for (int f = 0; f < faceList.size(); ++f){

        //0 - If the original face doesn't have at least 3 points, ignore it.
        if(faceList[f].size() < 3)
            continue;

        //1-If two consecutive identical points, remove the second.
        std::vector<int> newFace;
        Vector3 pt1 = points[faceList[f][0]];
        newFace.push_back(faceList[f][0]);
        for (int p = 1; p < (faceList[f].size() - 1); ++p) {
            Vector3 pt2 = points[faceList[f][p]];
            if(pt1 != pt2){
                newFace.push_back(faceList[f][p]);
                pt1 = pt2;
            }
        }

        //2-If the first and the last points are identical, remove the last
        pt1 = points[faceList[f][0]];
        Vector3 pt2 = points[faceList[f][faceList[f].size() - 1]];
        if (pt2 != pt1) {
            newFace.push_back(faceList[f][faceList[f].size() - 1]);
        }

        //3 - If the purified face doesn't have at least 3 points, ignore it
        if (newFace.size () >= 3) {
            paths.push_back(newFace);
        }
    }
}

SimpleMesh::~SimpleMesh() = default;

const std::vector<Vector3> &SimpleMesh::getPoints() const {
    return points;
}

void SimpleMesh::setPoints(const std::vector<Vector3> &points) {
    SimpleMesh::points = points;
}

const std::vector<Vector3> &SimpleMesh::getNormals() const {
    return normals;
}

void SimpleMesh::setNormals(const std::vector<Vector3> &normals) {
    SimpleMesh::normals = normals;
}

bool SimpleMesh::isIsClosedVolume() const {
    return isClosedVolume;
}

void SimpleMesh::setIsClosedVolume(bool isClosedVolume) {
    SimpleMesh::isClosedVolume = isClosedVolume;
}

bool SimpleMesh::isTriangulated() const {
    return triangulated;
}

void SimpleMesh::setTriangulated(bool triangulated) {
    SimpleMesh::triangulated = triangulated;
}

void SimpleMesh::computeNormals() {
    normals.clear();

    Vector3 _normal;
    for (int i = 0; i < paths.size(); i++) {
        int np = static_cast<int>(paths[i].size());
        Vector3 edge1(points[paths[i][np-1]].x(),
                      points[paths[i][np-1]].y(),
                      points[paths[i][np-1]].z());
        edge1-= Vector3(points[paths[i][0]].x(),
                        points[paths[i][0]].y(),
                        points[paths[i][0]].z());
        edge1.normalize();
        for (int j = 0; j < np; j++) {
            Vector3 edge2(points[paths[i][(j+1)%np]].x(),
                          points[paths[i][(j+1)%np]].y(),
                          points[paths[i][(j+1)%np]].z());
            edge2-= Vector3(points[paths[i][j]].x(),
                            points[paths[i][j]].y(),
                            points[paths[i][j]].z());
            edge2.normalize();

            _normal = cross(edge1, edge2);
            _normal.normalize();
            _normal *= -1;

            normals.push_back(_normal);

            edge1 = edge2;
            edge1 *= -1;
        }
    }
}

const std::vector<std::vector<int>> &SimpleMesh::getPaths() const {
    return paths;
}

void SimpleMesh::setPaths(const std::vector<std::vector<int>> &paths) {
    SimpleMesh::paths = paths;
}



