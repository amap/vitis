/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 29.08.18.
//

#ifndef THEVITISNEW_SIMPLEMESH_H
#define THEVITISNEW_SIMPLEMESH_H

#include <iostream>
#include <vector>
#include "Vector.h"
#include "BoundingBox3f.h"


class SimpleMesh {

private:

    /// true if the mesh is a closed volume, false means that the mesh is a surface
    bool isClosedVolume;

    bool triangulated;

protected:
    std::vector<Vector3> normals;
    std::vector<Vector3> points;
/**
 * Index of the faces that specifies the order and vertices that define individual faces.First
 * index indicates face number and the second indicates vertex indices.
 * Clockwise order
 */
std::vector< std::vector<int> > paths;
public:

    explicit SimpleMesh();

    /**
	 * Creates a new <code>Mesh</code> instance to represent a simple path in 3D space.
	 *
	 * @param points The points along the simple path (in order of the path)
	 */
    explicit SimpleMesh(std::vector<Vector3>& p);

    /**
	 * Creates a new <code>Mesh</code> instance with the given set of points and connections between
	 * them.
	 *
	 * @param points the collection of vertices for this mesh.
	 * @param paths an <code>std::vector<std::vector<int>></code> instance representing
     *        all the connections between
	 *        vertices in the mesh. Each entry in connections represents a closed polygon. Each
	 *        entry in a single entry of connections represents the index of a vertex. So
	 *        connections[0] is the first closed polygon described in this mesh. The entries of
	 *        connections[0] will be indexes into <code>pts</code>. The sequence of these entries
	 *        defines the closed path in 3D space. For example, if connections[0] = {1, 3, 4, 5},
	 *        then connections[0] represents a quadrilateral (in 3D space) whose vertices are given
	 *        by pts[1], pts[3], pts[4] and pts[5] in that order.
	 */
    explicit SimpleMesh (std::vector<Vector3>& points, std::vector< std::vector<int> >& paths);


    /**
	 * Creates a new <code>Mesh</code> instance with the given set of points and connections between
	 * them and normals.
	 *
	 * @param points the collection of vertices for this mesh.
	 * @param paths an <code>int[][]</code> instance representing all the connections between
	 *        vertices in the mesh. Each entry in connections represents a closed polygon. Each
	 *        entry in a single entry of connections represents the index of a vertex. So
	 *        connections[0] is the first closed polygon described in this mesh. The entries of
	 *        connections[0] will be indexes into <code>pts</code>. The sequence of these entries
	 *        defines the closed path in 3D space. For example, if connections[0] = {1, 3, 4, 5},
	 *        then connections[0] represents a quadrilateral (in 3D space) whose vertices are given
	 *        by pts[1], pts[3], pts[4] and pts[5] in that order.
	 * @param normals the collection of normal of each vertices for this mesh.
	 */
    explicit SimpleMesh (std::vector<Vector3>& points, std::vector< std::vector<int> >& paths, std::vector<Vector3>& normals);

    /** Triangulation of the mesh */
    void triangulate();

    /** Remove useless vertices, needed by the triangulation */
    void purifyMesh();

    virtual ~SimpleMesh();

    const std::vector<Vector3> &getPoints() const;

    void setPoints(const std::vector<Vector3> &points);

    const std::vector<Vector3> &getNormals() const;

    void setNormals(const std::vector<Vector3> &normals);

    bool isIsClosedVolume() const;

    void setIsClosedVolume(bool isClosedVolume);

    bool isTriangulated() const;

    void setTriangulated(bool triangulated);

    const std::vector< std::vector<int> > &getPaths() const;

    void setPaths(const std::vector< std::vector<int> > &paths);

    /** Compute normals according to points and paths, doesn't need triangulation */
    void computeNormals();

};


#endif //THEVITISNEW_SIMPLEMESH_H
