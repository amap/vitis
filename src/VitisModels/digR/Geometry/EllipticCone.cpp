/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 24.08.18.
//

#include "EllipticCone.h"
#include <cmath>
#include "UtilMath.h"


EllipticCone::EllipticCone(Vector3 v, Vector3 d, double a, double s) : AxisSymetryShape() {

    vertex = new Vector3(v);
    direction = new Vector3(d);
    height = norm(d);
    direction->normalize();
    primaryAngle = a;
    tg = std::tan( (a * FGEOM_PI / 180) );

    setSecondaryAngle(s);

    computeBase();

}

void EllipticCone::setSecondaryAngle(double secondaryAngle) {
    EllipticCone::secondaryAngle = secondaryAngle;
    tgs = std::tan( ( secondaryAngle * FGEOM_PI / 180) );
    computeBase();
}

EllipticCone::~EllipticCone() {
    delete vertex;
    delete direction;
}

void EllipticCone::computeBase() {
     Vector3 p(static_cast<const float &>(vertex->x() + height * direction->x()),
               static_cast<const float &>(vertex->y() + height * direction->y()),
               static_cast<const float &>(vertex->z() + height * direction->z()));
     Vector3 h;
     double dh = height * tg;
     double dv = height * tgs;
     if(std::abs(direction->z()) > 0.9999){
        h.set(1,0,0);
     } else{
         h.set(direction->y(), -direction->x(), 0);
         h.normalize();
     }
     Vector3 v = cross(*direction,h);
     v.normalize();

     v*=dh;
     h*=dv;

     b1 = Vector3(p.x()+v.x()+h.x(), p.y()+v.y()+h.y(), p.z()+v.z()+h.z());
     b2 = Vector3(p.x()-v.x()+h.x(), p.y()-v.y()+h.y(), p.z()-v.z()+h.z());
     b3 = Vector3(p.x()+v.x()-h.x(), p.y()+v.y()-h.y(), p.z()+v.z()-h.z());
     b4 = Vector3(p.x()-v.x()-h.x(), p.y()-v.y()-h.y(), p.z()-v.z()-h.z());

}

double EllipticCone::getTg() const {
    return tg;
}

void EllipticCone::setTg(double tg) {
    EllipticCone::tg = tg;
}

double EllipticCone::getTgs() const {
    return tgs;
}

void EllipticCone::setTgs(double tgs) {
    EllipticCone::tgs = tgs;
}
