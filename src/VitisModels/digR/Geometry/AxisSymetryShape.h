/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 24.08.18.
//

#ifndef THEVITISNEW_AXISSYMETRYSHAPE_H
#define THEVITISNEW_AXISSYMETRYSHAPE_H

#include "Vector.h"

class AxisSymetryShape {

protected:
    Vector3* vertex;
    Vector3* direction;
public:

    Vector3 *getVertex() const;

    void setVertex(Vector3 *vertex);

    Vector3 *getDirection() const;

    void setDirection(Vector3 *direction);

    Vector3 computeVerticalDirection();

    Vector3 computeHorizontalDirection();

};


#endif //THEVITISNEW_AXISSYMETRYSHAPE_H
