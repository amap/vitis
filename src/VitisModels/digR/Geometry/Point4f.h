/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 29.08.18.
//

#ifndef THEVITISNEW_POINT4F_H
#define THEVITISNEW_POINT4F_H

#include <iostream>


class Point4f {

public:

         float x;
         float y;
         float z;
         float w;

    Point4f(float x, float y, float z, float w);

    explicit Point4f(float p[]);

    Point4f(const Point4f& p);

    float distanceSquared(Point4f p1);

    float distance(Point4f p1);

    float distanceL1(Point4f p1);

    float distanceLinf(Point4f p1);

    void project(Point4f p1);


};


#endif //THEVITISNEW_POINT4F_H
