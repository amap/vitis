/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 24.08.18.
//

#ifndef THEVITISNEW_ELLIPTICCONECYLINDERCONSTRAINT_H
#define THEVITISNEW_ELLIPTICCONECYLINDERCONSTRAINT_H

#include "EllipticConeCylinder.h"
#include "Matrix.h"

class ConstrainedRoot;

class EllipticConeCylinderConstraint {

private:

    ConstrainedRoot* constrainedRoot;
    EllipticConeCylinder* coneCylinder;
    bool initialized;

public:
    EllipticConeCylinderConstraint(ConstrainedRoot *constrainedRoot, EllipticConeCylinder *coneCylinder);

    bool isInitialized() const;

    void setInitialized(bool initialized);

    void init();

    EllipticConeCylinder *getConeCylinder() const;

    bool isConstrained (Matrix4& geom);

    void transform(Matrix4& geom);

    void setConeCylinder(EllipticConeCylinder *coneCylinder);
};


#endif //THEVITISNEW_ELLIPTICCONECYLINDERCONSTRAINT_H
