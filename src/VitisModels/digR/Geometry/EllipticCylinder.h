/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 24.08.18.
//

#ifndef THEVITISNEW_ELLIPTICCYLINDER_H
#define THEVITISNEW_ELLIPTICCYLINDER_H

#include "AxisSymetryShape.h"

class EllipticCylinder : AxisSymetryShape {

private:

    double diameter, secondaryDiameter;


public:
    EllipticCylinder(Vector3 v, Vector3 d, double a, double b);

    virtual ~EllipticCylinder();
};


#endif //THEVITISNEW_ELLIPTICCYLINDER_H
