/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 23.08.18.
//

#include "ConstrainedRoot.h"
#include "EllipticConeCylinderConstraint.h"
#include "cmath"
#include "UtilMath.h"


ConstrainedRoot::ConstrainedRoot(GeomRoot *Parent) {
    parent = Parent;
}

void ConstrainedRoot::initConstraint(EllipticConeCylinderConstraint *g) {
    computeBoundingMesh(parent, g->getConeCylinder());
}

void ConstrainedRoot::computeBoundingMesh(GeomRoot *r, EllipticConeCylinder *c) {

    vector< vector< vector<double> > > axis;
    computeConeAxis((Root*)r->getBranc(), axis);

    vector< vector<double> >p = axis[0];
    vector<double>sh = axis[1][0];
    vector<double>sw = axis[2][0];
    std::vector< std::vector<double> > O;
    O.resize(5,{});
    for(std::vector<double> &i : O){
        i.resize(6,0.0);
    }
    O[0][0] = -0.5;
    O[0][1] = 0.5;
    O[0][2] = 0;
    O[0][3] = 0;
    O[0][4] = 0;
    O[0][5] = 1;
    O[1][0] = -0.5;
    O[1][1] = -0.5;
    O[1][2] = 0;
    O[1][3] = 0;
    O[1][4] = 0;
    O[1][5] = 1;
    O[2][0] = 0.5;
    O[2][1] = -0.5;
    O[2][2] = 0;
    O[2][3] = 0;
    O[2][4] = 0;
    O[2][5] = 1;
    O[3][0] = 0.5;
    O[3][1] = 0.5;
    O[3][2] = 0;
    O[3][3] = 0;
    O[3][4] = 0;
    O[3][5] = 1;
    O[4][0] = -0.5;
    O[4][1] = 0.5;
    O[4][2] = 0;
    O[4][3] = 0;
    O[4][4] = 0;
    O[4][5] = 1;

    static ExtrudedMesh m = ExtrudedMesh(O, p, sh, sw, true);

    c->setMesh(&m);

    std::vector<Vector3> axisPoints;
    for (int i = 0; i < p.size() ; i++) {
        Vector3 v(p[i][0], p[i][1], p[i][2]);
        axisPoints.push_back(v);
    }
    c->setAxisPoints(axisPoints);
}

void ConstrainedRoot::computeConeAxis(Root *r, vector< vector< vector<double> > >&Axis) {
        double rootLength = r->Length();

        vector< std::vector<double> >path;
        vector<double> s_H;
        vector<double> s_W;

        ParamSet* paramSet = r->theBud->paramSet;

//        r->startPosit();
//        r->positOnFirstSegmentWithinRoot();
//        Segment* n = r->getCurrentSegment();
//        r->endPosit();

        int type = r->Type();

        double cLength = r->theBud->paramSet->getParameter("BoundingConeLength",type)->value(r, r->getPlant());
        double vTan = std::tan( r->theBud->paramSet->getParameter("BoundingVerticalConeAngle",type)->value(r, r->getPlant()) * FGEOM_PI /180 ) * 2;
        double hTan = std::tan( r->theBud->paramSet->getParameter("BoundingHorizontalConeAngle",type)->value(r, r->getPlant()) * FGEOM_PI /180 ) * 2;
        ParametricParameter* angleParam = (ParametricParameter*)r->theBud->paramSet->getParameter("finalAbsoluteDirectionToVerticalList", type);

        Matrix4 geom = parent->getCurrentTransform();
        Vector3 t = geom.getTranslation();
        Vector3 m = geom.getMainVector();

//        Root * ppp = r->getBearer();
//        if(ppp == r){
//            geom.set(-4.371139E-8, 0.0, 1.0, 0.0,
//                      0.0, 1.0, 0.0, 0.0,
//                      -1.0, 0.0, -4.371139E-8, 0.0,
//                      0.0, 0.0, 0.0, 1.0);
//            geom.set(-1, 0.0, 0.0, 0.0,
//                      0.0, 1.0, 0.0, 0.0,
//                      0.0, 0.0, -1, 0.0,
//                      0.0, 0.0, 0.0, 1.0);
//        }
//        else{
//            ppp->startPosit();
//            ppp->positOnFirstSegmentWithinRoot();
//            Segment* n2 = ppp->getCurrentSegment();
//            while (n2->getTransformation() == 0) {
//                ppp->positOnPreviousSegmentWithinRoot();
//                n2 = ppp->getCurrentSegment();
//            }
//            ppp->endPosit();
////            n2 = ppp->getElementsGeo().at(r->posOnBearer);
//            Vector4 poss1(n2->getTransformation()->getTranslation(), 1);
//       if(ppp != r){
//            geom *= parent->getCurrentTransform();
//            geom.setLength(n2->getTransformation()->getLength());
//            Vector4 poss(n2->getTransformation()->getMainVector(), n2->getTransformation()->getLength());
//            poss += n2->getTransformation()->getTopPosition();
//            geom.setTranslation(poss.x(), poss.y(), poss.z());
//
//        }

        if(isBended(paramSet, type)){
            vector<double> bendingLengthList;
            vector<double> finalAbsoluteDirectionToVertical;
            vector<double> bendingStartPosition;
            computeBendingParameters(paramSet,r,bendingLengthList, finalAbsoluteDirectionToVertical, bendingStartPosition);

            vector<Vector3> axisPoints;
            vector<Vector3> axisDirections;
            vector<Vector3> curveCenters;
            computeAxisFromParameters(t, m, bendingLengthList, finalAbsoluteDirectionToVertical, bendingStartPosition, axisPoints, curveCenters, axisDirections);

            std::vector<double> keyPoint;
            bool coneEnded = false;

            Vector3 pt, dir, center;
            double  pos = 0;
            double length;
            Vector3 axis, rd, _normal(1,1,1);
            bool normalSet = false;
            double angPrec = 0;

            for (int i = 0; i < axisPoints.size() -1; i++) {
                pt = axisPoints[i];
                dir = axisDirections[i];
                center = curveCenters[i];

                if(i == 0){
                    keyPoint.resize(6, 0);
                    keyPoint[0] = pt.x();
                    keyPoint[1] = pt.y();
                    keyPoint[2] = pt.z();
                    if(!normalSet){
                        normalSet = true;
                        if(std::abs(dir.z()) > 0.9990001){
                            _normal[0] = 0;
                            _normal[1] = 1;
                            _normal[2] = 0;
                            if(dir.z() > 0)
                                _normal[1] = static_cast<float>(0.999);
                            else
                                _normal[1] = -0.999;
                            _normal[2] = 0.002;
                        } else{
                            _normal[0] = dir.y();
                            _normal[1] = -dir.x();
                            _normal[2] = 0;
                            _normal.normalize();
                        }
                    }
                    keyPoint[3] = _normal.x();
                    keyPoint[4] = _normal.y();
                    keyPoint[5] = _normal.z();
                    path.push_back( keyPoint );

                    s_H.push_back(0.01);
                    s_W.push_back(0.01);

                    axis[0] = m.x();
                    axis[1] = m.y();
                    axis[2] = m.z();

                    angPrec = std::acos(-axis.z()) * 180 / FGEOM_PI;
                }
                if( center == Vector3(-999999,-999999,-999999)){
                    axis.set(axisPoints[i+1].x()-pt.x(), axisPoints[i+1].y()-pt.y(),axisPoints[i+1].z()-pt.z());
                    if( pos + norm(axis) > rootLength){
                        axis.normalize();
                        axis *= (rootLength-pos);
                        axisPoints[i+1].set(pt.x() + axis.x(), pt.y() + axis.y(), pt.z() + axis.z());
                    }

                    // try to insert the cone endpoint before cylinder section:
                    if( !coneEnded && pos + norm(axis) > cLength){
                        dir.normalize();
                        dir *= (cLength-pos);
                        std::fill( std::begin( keyPoint ), std::end( keyPoint ), 0 );
                        keyPoint[0] = pt.x()+dir.x();
                        keyPoint[1] = pt.y()+dir.y();
                        keyPoint[2] = pt.z()+dir.z();
                        keyPoint[3] = _normal.x();
                        keyPoint[4] = _normal.y();
                        keyPoint[5] = _normal.z();
                        path.push_back(keyPoint);
                        s_W.push_back(cLength*hTan);
                        s_H.push_back(cLength*vTan);
                        pos = cLength;
                        coneEnded = true;
                        axis.normalize();
                        axis *= (norm(dir));
                    }
                    keyPoint[0] = axisPoints[(i+1)].x();
                    keyPoint[1] = axisPoints[(i+1)].y();
                    keyPoint[2] = axisPoints[(i+1)].z();
                    if(!normalSet){
                        normalSet = true;
                        if(std::abs(dir.z()) > 0.9990001){
                            _normal[0] = 0;
                            _normal[1] = 1;
                            _normal[2] = 0;
                            if(dir.z() > 0)
                                _normal[1] = static_cast<float>(0.999);
                            else
                                _normal[1] = -0.999;
                            _normal[2] = 0.002;
                        } else{
                            _normal[0] = dir.y();
                            _normal[1] = -dir.x();
                            _normal[2] = 0;
                            _normal.normalize();
                        }
                    }
                    keyPoint[3] = _normal.x();
                    keyPoint[4] = _normal.y();
                    keyPoint[5] = _normal.z();
                    path.push_back(keyPoint);

                    pos += norm(axis);

                    if(pos >= cLength){
                        s_W.push_back(cLength*hTan);
                        s_H.push_back(cLength*vTan);
                    } else{
                        s_W.push_back(pos*hTan);
                        s_H.push_back(pos*vTan);
                    }
                } else{
                    ParametricParameter* angleParam = (ParametricParameter*)paramSet->getParameter("finalAbsoluteDirectionToVerticalList", type);
                    angleParam->setCurrentInterpolationY(true);
                    double angCur = angleParam->value(static_cast<float>(pos), r, r->getPlant());
                    double angle = (angCur - angPrec) * FGEOM_PI / 180;
                    angPrec = angCur;
                    angleParam->setCurrentInterpolationY(false);

                    if(angle != 0){
                        if(i < bendingLengthList.size())
                            length = bendingLengthList[i];
                        else
                            length = bendingLengthList[bendingLengthList.size()-1];
                        rd.set(center.x()-pt.x(), center.y()-pt.y(),center.z()-pt.z());
                        double radius = norm(rd);

                        if(!normalSet){
                            normalSet = true;
                            if(std::abs(dir.z()) > 0.9990001){
                                _normal[0] = 0;
                                _normal[1] = 1;
                                _normal[2] = 0;
                                if(dir.z() > 0)
                                    _normal[1] = static_cast<float>(0.999);
                                else
                                    _normal[1] = -0.999;
                                _normal[2] = 0.002;
                            } else{
                                _normal[0] = dir.y();
                                _normal[1] = -dir.x();
                                _normal[2] = 0;
                                _normal.normalize();
                            }
                        }

                        addArc3d(center, radius, pt, dir, _normal, angle, length, pos, path, s_H, s_W, cLength, hTan, vTan);
                        pos += length;
                        if(pos >= cLength){
                            coneEnded = true;
                        }
                    } else{
                        axis.set(axisPoints[i+1].x()-pt.x(), axisPoints[i+1].y()-pt.y(),axisPoints[i+1].z()-pt.z());
                        if(pos + norm(axis) > rootLength){
                            axis.normalize();
                            axis *= (rootLength - pos);
                            axisPoints[i+1].set (pt.x() + axis.x(), pt.y() + axis.y(), pt.z() + axis.z());
                        }

                        // try to insert the cone endpoint before cylinder section
                        if (   !coneEnded
                            && pos + norm(axis) > cLength){
                            dir.normalize();
                            dir *= (cLength - pos);
                            std::fill( std::begin( keyPoint ), std::end( keyPoint ), 0 );
                            keyPoint[0] = pt.x() + dir.x();
                            keyPoint[1] = pt.y() + dir.y();
                            keyPoint[2] = pt.z() + dir.z();
                            keyPoint[3] = _normal.x();
                            keyPoint[4] = _normal.y();
                            keyPoint[5] = _normal.z();
                            path.push_back(keyPoint);
                            s_W.push_back(cLength*hTan);
                            s_H.push_back(cLength*vTan);
                            pos = cLength;
                            coneEnded = true;
                            axis.normalize();
                            axis *= ( norm(dir));
                        }
                        std::fill( std::begin( keyPoint ), std::end( keyPoint ), 0 );
                        keyPoint[0] = axisPoints[i+1].x();
                        keyPoint[1] = axisPoints[i+1].y();
                        keyPoint[2] = axisPoints[i+1].z();
                        if(!normalSet){
                            normalSet = true;
                            if (std::abs (dir.z()) > 0.9990001){
                                _normal[0] = 0;
                                _normal[1] = 1;
                                _normal[2] = 0;
                                if (dir.z() > 0)
                                    _normal[1] = static_cast<float>(0.999);
                                else
                                    _normal[1] = -0.999;
                                _normal[2] = 0.002;
                            }else{
                                _normal[0] = dir.y();
                                _normal[1] = -dir.x();
                                _normal[2] = 0;
                                _normal.normalize ();
                            }
                        }
                        keyPoint[3] = _normal.x();
                        keyPoint[4] = _normal.y();
                        keyPoint[5] = _normal.z();
                        path.push_back(keyPoint);

                        pos += norm(axis);

                        if (pos >= cLength){
                            s_W.push_back(cLength*hTan);
                            s_H.push_back(cLength*vTan);
                        }else{
                            s_W.push_back(pos*hTan);
                            s_H.push_back(pos*vTan);
                        }
                    }
                }

                if (pos >= rootLength){
                    break;
                }
            }
        } else{
            std::vector<double> keyPoint;
            keyPoint.resize(6, 0);
            Vector3 _normal;
            if(std::abs(m.z()) > 0.9990001){
                _normal[0] = 0;
                _normal[1] = 1;
                _normal[2] = 0;
                if (m.z() > 0)
                    _normal[1] = static_cast<float>(0.999);
                else
                    _normal[1] = -0.999;
                _normal[2] = 0.002;
            } else{
                _normal[0] = m.y();
                _normal[1] = -m.x();
                _normal[2] = 0;
                _normal.normalize ();
            }
            keyPoint[0] = t.x();
            keyPoint[1] = t.y();
            keyPoint[2] = t.z();
            keyPoint[3] = _normal.x();
            keyPoint[4] = _normal.y();
            keyPoint[5] = _normal.z();
            path.push_back(keyPoint);
            s_H.push_back(0.01);
            s_W.push_back(0.01);
            if(cLength < r->Length()){
                std::fill( std::begin( keyPoint ), std::end( keyPoint ), 0 );
                keyPoint[0] = t.x()+cLength*m.x();
                keyPoint[1] = t.y()+cLength*m.y();
                keyPoint[2] = t.z()+cLength*m.z();
                keyPoint[3] = _normal.x();
                keyPoint[4] = _normal.y();
                keyPoint[5] = _normal.z();
                path.push_back (keyPoint);
                s_W.push_back(cLength*hTan);
                s_H.push_back(cLength*vTan);
            }
            std::fill( std::begin( keyPoint ), std::end( keyPoint ), 0 );
            keyPoint[0] = t.x()+r->Length()*m.x()*1.1;
            keyPoint[1] = t.y()+r->Length()*m.y()*1.1;
            keyPoint[2] = t.z()+r->Length()*m.z()*1.1;
            keyPoint[3] = _normal.x();
            keyPoint[4] = _normal.y();
            keyPoint[5] = _normal.z();
            path.push_back (keyPoint);
            s_W.push_back(cLength*hTan);
            s_H.push_back(cLength*vTan);
        }


    vector<vector<double>> p{};
    vector<double> vector1{};
    for (int i=0; i<path.size(); i++){
        vector1.clear();
        for (int j=0; j<6; j++){
            vector1.push_back(path[i][j]);
        }
        p.push_back(vector1);
    }

    vector<vector<double>> sw;
    vector1.clear();
    for (double i : s_W) {
        vector1.push_back(i);
    }
    sw.push_back(vector1);

    vector<vector<double>> sh;
    vector1.clear();
    for (double i : s_H) {
        vector1.push_back(i);
    }
    sh.push_back(vector1);

    Axis.push_back(p);
    Axis.push_back(sw);
    Axis.push_back(sh);
}

bool ConstrainedRoot::isBended(ParamSet *paramSet, int type) {

    ParametricParameter  * bendingLengthList = dynamic_cast<ParametricParameter *>(paramSet->getParameter("bendingLengthList", type));
    if(bendingLengthList->CurrentNb_Y() > 0){
        for (int i = 0; i < bendingLengthList->CurrentNb_Y(); ++i) {
            if(bendingLengthList->getCurrentValTab()[i] > 0){
                return true;
            }
        }
    }
    return false;
}

void ConstrainedRoot::computeBendingParameters(ParamSet *paramSet, Root *r, vector<double>& bendingLength,
                                               vector<double>& finalAbsoluteDirectionToVertical,
                                               vector<double>& bendingStartPosition) {
    bool first = true;
    bool firstAdded = false;
    ParametricParameter* f_a_d_t_v_l = dynamic_cast<ParametricParameter *>(paramSet->getParameter("finalAbsoluteDirectionToVerticalList", r->Type()));

    for (int i = 0; i < f_a_d_t_v_l->CurrentNb_Y(); ++i) {
        if(first){
            if(f_a_d_t_v_l->getCurrentYTab()[i] > 0){
                bendingStartPosition.push_back(0.0);
                bendingLength.push_back(-1.0);
                finalAbsoluteDirectionToVertical.push_back(0.001);
                firstAdded = true;
            }
        }
        bendingStartPosition.push_back(f_a_d_t_v_l->getCurrentYTab()[i]);
        first = false;
    }
    first = true;
    ParametricParameter* b_l_l = dynamic_cast<ParametricParameter *>(paramSet->getParameter("bendingLengthList", r->Type()));

    for (int j = 0; j < b_l_l->CurrentNb_Y(); ++j) {
        bool add = true;
        double pos = b_l_l->getCurrentYTab()[j];
        if(first){
            pos = 0;
        }
        first = false;
        int index = 0;
        for (double &mm : bendingStartPosition) // access by reference to avoid copying
        {
           if(pos == mm){
               add = false;
               break;
           } else if(pos < mm){
               break;
           }
           index++;
        }
        if(add){
            bendingStartPosition.insert(bendingStartPosition.begin()+index, pos);
        }
        }

    first = true;
    for(double &m: bendingStartPosition ){
        if( first && firstAdded){
            first = false;
            continue;
        }
        bendingLength.push_back(paramSet->getParameter("bendingLengthList", r->Type())->value(static_cast<float>(m), r, r->getPlant()));
        double d = paramSet->getParameter("finalAbsoluteDirectionToVerticalList", r->Type())->value(static_cast<float>(m), r, r->getPlant());
        if(std::abs(d) < 0.1){
            if(d < 0)
                d = -0.1;
            else
                d = 0.1;
        }
        finalAbsoluteDirectionToVertical.push_back( (d * FGEOM_PI / 180) );
        first = false;
    }

}

void ConstrainedRoot::computeAxisFromParameters(Vector3 initPoint, Vector3 initDir, vector<double>& bendingLengthList,
                                                vector<double>& finalDirectionList, vector<double>& bendingStartList,
                                                vector<Vector3>& axisPoints, vector<Vector3>& curveCenters,
                                                vector<Vector3>& axisDirections) {
    Vector3 prevPoint(initPoint.x(), initPoint.y(), initPoint.z());
    Vector3 prevDir(initDir.x(), initDir.y(), initDir.z());
    if( std::abs(prevDir.z()) > 0.9990001 ){
        if(prevDir.z() > 0)
            prevDir[2] = static_cast<float>(0.999);
        else
            prevDir[2] = static_cast<float>(-0.999);
        prevDir[0] = 0.002;
    }
    Vector3 point, center;
    bool next = false;

    double p, pprec = 0, d, l;
    int index = 0;

    double dprec = std::asin(prevDir.z()) + FGEOM_PI / 2;


    while( index < bendingStartList.size()){
        p = bendingStartList[index];
        d = finalDirectionList[index];
        l = bendingLengthList[index];

        // init state:
        if(l == -1){
            center = Vector3(-999999,-999999,-999999);
            l = bendingStartList[index+1];
            bendingLengthList[index] = l;
            d = dprec;
            next = true;
        }

        // non curved section:
        else if(p > pprec){
            center = Vector3(-999999,-999999,-999999);
            l = p - pprec;
            next = false;
        }

        // curved section:
        else if(d != dprec){
            center = computeCenter(prevPoint, prevDir, d-dprec, l);
            next = true;
        } else{
            center = Vector3(-999999,-999999,-999999);
            l = p - pprec;
            next = true;
        }

        axisPoints.push_back(prevPoint);
        curveCenters.push_back(center);
        axisDirections.push_back(prevDir);

        prevDir = Vector3(prevDir);
        point = computeNextPoint(prevPoint, prevDir, center, dprec, d, l);

        if(next){
            dprec = d;
            index++;
            pprec = p + l;
        } else
            pprec = p;

        prevPoint = point;
    }

    axisPoints.push_back(prevPoint);
    curveCenters.push_back(Vector3(-999999,-999999,-999999));
    axisDirections.push_back(prevDir);

    prevPoint = Vector3 (point.x()+99999999*prevDir.x(), point.y()+99999999*prevDir.y(), point.z()+99999999*prevDir.z());
    axisPoints.push_back(prevPoint);
    curveCenters.push_back(Vector3(-999999,-999999,-999999));
    axisDirections.push_back(prevDir);

}

Vector3 ConstrainedRoot::computeCenter(Vector3 &point, Vector3 &dir, double a, double length) {
    double radius = length/a;
    Vector3 center, hn;
    if(std::abs(dir.z()) == 1)
        hn.set(1, 0, 0);
    else
        hn.set(-dir.y(), dir.x(), 0);

    hn.normalize();

    center.set(static_cast<const float &>(radius * (dir.y() * hn.z() - dir.z() * hn.y()) + point.x()),
               static_cast<const float &>(radius * (dir.z() * hn.x() - dir.x() * hn.z()) + point.y()),
               static_cast<const float &>(radius * (dir.x() * hn.y() - dir.y() * hn.x()) + point.z()));

    return center;
}

Vector3
ConstrainedRoot::computeNextPoint(Vector3& prevPoint, Vector3 &prevDir, Vector3 &center, double dprec, double d,
                                  double l) {
    Vector3 point;

    if(center == Vector3(-999999,-999999,-999999)){
        point.set(static_cast<const float &>(prevPoint.x() + l * prevDir.x()),
                  static_cast<const float &>(prevPoint.y() + l * prevDir.y()),
                  static_cast<const float &>(prevPoint.z() + l * prevDir.z()));
    }else{
        Vector3 newDir(center.x()-prevPoint.x(), center.y()-prevPoint.y(), center.z()-prevPoint.z());

        //string length:
        double lng = std::abs(2 * norm(newDir) * std::sin((d-dprec)/2));
        double cos = std::sin(d - FGEOM_PI / 2);
        double coef = prevDir.x()*prevDir.x()+prevDir.y()*prevDir.y();

        if(coef > 0){
            coef = std::sqrt( (1 - cos * cos) / coef);
            if( (   dprec > 0
                   && dprec <= FGEOM_PI
                   && d > FGEOM_PI
                   && d <= 2*FGEOM_PI)
               || (   dprec > FGEOM_PI
                      && dprec <= 2*FGEOM_PI
                      && d > 0
                      && d <= FGEOM_PI)
               || (   dprec >= FGEOM_PI/2
                      && dprec < 3*FGEOM_PI/2
                      && d > 3*FGEOM_PI/2
                      && d <= 5*FGEOM_PI/2)
               || (   dprec >= 3*FGEOM_PI/2
                      && dprec < 5*FGEOM_PI/2
                      && d > FGEOM_PI/2
                      && d <= 3*FGEOM_PI/2) ){
                coef *= -1;
            }
            newDir.set(static_cast<const float &>(coef * prevDir.x()), static_cast<const float &>(coef * prevDir.y()),
                       static_cast<const float &>(cos));
        } else{
            double sin = std::sin( (d - dprec) );
            newDir.set(static_cast<const float &>(cos * std::sqrt (1 - cos * cos)),
                       static_cast<const float &>(sin * std::sqrt (1 - cos * cos)),
                       static_cast<const float &>(cos));
        }
        newDir.normalize();
        if(std::abs(newDir.z()) > 0.9990001){
            if(newDir.z() > 0)
                newDir[2] = 0.999;
            else
                newDir[2] = static_cast<float>(-0.999);
            if(prevDir.y() == 0){
                newDir[1] = 0;
                if(prevDir.x() > 0)
                    newDir[0] = 0.04;
                else
                    newDir[0] = static_cast<float>(-0.04);
            } else{
                newDir[1] = static_cast<float>(std::sqrt(0.0016 / (1 + (prevDir.x() * prevDir.x()) / (prevDir.y() * prevDir.y()))));
                if (prevDir.y() < 0)
                    newDir[1] *= -1;
                newDir[0] = static_cast<float>(std::sqrt(0.0016 - newDir.y() * newDir.y()));
                if (prevDir.x() < 0)
                    newDir[0] *= -1;
            }
        }
        newDir.normalize();
        Vector3 dir((prevDir.x()+newDir.x())/2, (prevDir.y()+newDir.y())/2, (prevDir.z()+newDir.z())/2);
        dir.normalize();

        point.set(static_cast<const float &>(lng * dir.x() + prevPoint.x()),
                  static_cast<const float &>(lng * dir.y() + prevPoint.y()),
                  static_cast<const float &>(lng * dir.z() + prevPoint.z()));

        prevDir.set(newDir.x(), newDir.y(), newDir.z());
    }
    return point;
}

void
ConstrainedRoot::addArc3d(Vector3 &center, double radius, Vector3 &startPoint, Vector3 &initialDirection, Vector3 &axis,
                          double angle, double length, double pos, vector<std::vector<double>> &path,
                          vector<double> &sh, vector<double> &sw, double cLength, double hCos, double vCos) {
    int nbDeviations = static_cast<int>(std::abs(18 / FGEOM_PI * angle));
    std::vector<double> keyPoint{};
    Vector3 _normal;
    _normal.set(initialDirection);
    Matrix4 a = Matrix4::axisRotation(axis, FGEOM_PI/2);
    Matrix4 m;
    m.setIdentity();
    m = a * m;
    m.transform(_normal);

    double l;
    if(nbDeviations < 6){
        nbDeviations = 6;
        l = length / nbDeviations;
    } else
        l = std::abs(2 * radius * std::sin(angle / nbDeviations / 2));

    Vector3 direction(initialDirection);
    Vector3 vertex(startPoint);

    a = Matrix4::axisRotation(axis, static_cast<const float &>(angle / nbDeviations / 2));
    m.setIdentity();
    m = a * m;
    m.transform(direction);
    m.transform(_normal);

    vertex[0] += direction.x() * l;
    vertex[1] += direction.y() * l;
    vertex[2] += direction.z() * l;
    keyPoint.resize(6, 0);
    std::fill( std::begin( keyPoint ), std::end( keyPoint ), 0 );
    keyPoint[0] = vertex.x();
    keyPoint[1] = vertex.y();
    keyPoint[2] = vertex.z();
    keyPoint[3] = axis.x();
    keyPoint[4] = axis.y();
    keyPoint[5] = axis.z();
    path.push_back(keyPoint);

    pos += l;
    if(pos >= cLength){
        sw.push_back(cLength*hCos);
        sh.push_back(cLength*vCos);
    } else{
        sw.push_back(pos*hCos);
        sh.push_back(pos*vCos);
    }

    a = Matrix4::axisRotation(axis, static_cast<const float &>(angle / nbDeviations));
    m.setIdentity();
    m = a * m;

    for (int i = 2; i < nbDeviations+1; ++i) {
        m.transform(direction);
        m.transform(_normal);
        vertex[0] += direction.x() * l;
        vertex[1] += direction.y() * l;
        vertex[2] += direction.z() * l;
        std::fill( std::begin( keyPoint ), std::end( keyPoint ), 0 );
        keyPoint[0] = vertex.x();
        keyPoint[1] = vertex.y();
        keyPoint[2] = vertex.z();
        keyPoint[3] = axis.x();
        keyPoint[4] = axis.y();
        keyPoint[5] = axis.z();
        path.push_back(keyPoint);

        pos += l;
        if (pos >= cLength){
            sw.push_back(cLength*hCos);
            sh.push_back(cLength*vCos);
        }else{
            sw.push_back(pos*hCos);
            sh.push_back(pos*vCos);
        }
    }

}



Vector3 ConstrainedRoot::caclRandomNormalAxeToMainAxe(Vector3 main) {

    Root * bv = (Root*)parent->getBranc();

    Vector3 i,j;

    float x = main.x();
    float y = main.y();
    float z = main.z();

    if( (x != 0 && y != 0 && z != 0)
        || (x == 0 && y != 0 && z != 0)
        || (x != 0 && y == 0 && z != 0)
        || (x != 0 && y != 0 && z == 0) ){
        i = Vector3(y,-x,0);
        j = Vector3(0,-z,y);
    } else if(x == 0 && y == 0){
        float f1 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        float f2 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        i = Vector3(-f1,f2,0);
        j = Vector3(f1,-f2,0);
    } else if(y == 0 && z == 0){
        float f1 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        float f2 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        i = Vector3(0,-f1,f2);
        j = Vector3(0,f1,-f2);
    } else if(x == 0 && z == 0){
        float f1 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        float f2 = static_cast<float>(bv->theBud->getConfigData().getRandomGenerator()->Random());
        i = Vector3(-f1,0,f2);
        j = Vector3(f1,0,-f2);
    }
//    assert(i != j && "i is equal to j, the direction will not be correct!\n");

    double alpha = bv->theBud->getConfigData().getRandomGenerator()->Random();
    double beta  = bv->theBud->getConfigData().getRandomGenerator()->Random();
    Vector3 axe = (i * alpha) + (j * beta);

    return axe;
}
