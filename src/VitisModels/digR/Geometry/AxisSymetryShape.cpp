/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 24.08.18.
//

#include "AxisSymetryShape.h"

Vector3 *AxisSymetryShape::getVertex() const {
    return vertex;
}

void AxisSymetryShape::setVertex(Vector3 *vertex) {
    AxisSymetryShape::vertex = vertex;
}

Vector3 *AxisSymetryShape::getDirection() const {
    return direction;
}

void AxisSymetryShape::setDirection(Vector3 *direction) {
    AxisSymetryShape::direction = direction;
    AxisSymetryShape::direction->normalize();
}

Vector3 AxisSymetryShape::computeVerticalDirection() {
    double x,y,z;

    if(direction->x() != 0 && direction->z() != 0){
        x = -direction->x();
        y = -direction->y();
        z = (direction->x() * direction->x() + direction->y() * direction->y()) / direction->z() /direction->x();
    } else if(direction->z() == 0){
        x = 0;
        y = 0;
        z = 1;
    } else{
        x = 0;
        y = direction->z();
        z = -direction->y();
    }

    return Vector3(static_cast<const float &>(x), static_cast<const float &>(y), static_cast<const float &>(z));
}

Vector3 AxisSymetryShape::computeHorizontalDirection() {
    Vector3 vv = computeVerticalDirection();
    return Vector3(direction->y()*vv.z() - direction->z()*vv.y(),
                    direction->z()*vv.x() - direction->x()*vv.z(),
                     direction->x()*vv.y() - direction->y()*vv.x());
}
