/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 23.08.18.
//

#include "EllipticConeCylinder.h"
#include "cmath"
#include "UtilMath.h"

EllipticConeCylinder::EllipticConeCylinder(Vector3 v, Vector3 d, double a, double b, double l) {
        distanceh = 0;
        distancev = 0;
        sizeh = 0;
        sizev = 0;

        d.normalize();
        d*= static_cast<float>(l);
        cone = new EllipticCone(v, d, a, b);
        double diam = l * std::sin( (a * FGEOM_PI / 180));
        double sdiam = l * std::sin( (b * FGEOM_PI / 180));
        cylinder = new EllipticCylinder(v, d, diam, sdiam);
        coneLength = l;
        mesh = nullptr;
        axisPoints = {};
}

EllipticConeCylinder::~EllipticConeCylinder() {
        delete cone;
        delete cylinder;
        delete mesh;
}

ExtrudedMesh *EllipticConeCylinder::getMesh() const {
        return mesh;
}

void EllipticConeCylinder::setMesh(ExtrudedMesh *mesh) {
        EllipticConeCylinder::mesh = mesh;
}

const std::vector<Vector3> &EllipticConeCylinder::getAxisPoints() const {
        return axisPoints;
}

void EllipticConeCylinder::setAxisPoints(const std::vector<Vector3> &axisPoints) {
        EllipticConeCylinder::axisPoints = axisPoints;
}

int EllipticConeCylinder::findProjectionSegment(Vector3 p) {
    Vector3 A, B, op, dir;

    double amin = 1e34;
    double distmin = 1e34;
    int imin = 0;
    for (int i=0; i < getAxisPoints().size() - 1; i++) {
        A = getAxisPoints()[i];
        B = getAxisPoints()[i+1];
        op.set(p.x()-A.x(), p.y()-A.y(), p.z()-A.z());
        dir.set(B.x()-A.x(), B.y()-A.y(), B.z()-A.z());
        double l = norm(dir);
        double a = dot(dir,op) / l;
        if( (std::abs(a) <= amin || i - imin >= 1) && norm(op) < distmin){
            amin = std::abs(a);
            distmin = norm(op);
            imin = i;
        }
    }
    return imin;
}

Position EllipticConeCylinder::position(Vector3 p, int index) {
    pos = IN;

    Vector3 proj = projectionPoint(index, p);
    Vector3 ph(p.x()-proj.x(), p.y()-proj.y(), p.z()-proj.z());
    double h = distanceFromOrigin (proj, index);

    // compute vector orthogonal to direction and in a vertical plane
    Vector3 vv = computeVerticalDirection(index);
    distanceh = (std::abs(dot(ph, vv)));

    // compute vector orthogonal to direction and in a horizontal plane
    Vector3 vh = computeHorizontalDirection (index);
    distancev = (std::abs(dot(ph ,vh)));

    if (   h >= coneLength
        && distanceh > coneLength*cone->getTg())
        pos = OUTVERTICAL;
    if (   h < coneLength
        && distanceh > h*cone->getTg())
        pos = OUTVERTICAL;

    if (   h >= coneLength
        && distancev > coneLength*cone->getTgs()){
        if (pos == IN)
            pos = OUTHORIZONTAL;
        else
            pos = OUT;
    }
    if (   h < coneLength
        && distancev > h*cone->getTgs()){
        if (pos == IN)
            pos = OUTHORIZONTAL;
        else
            pos = OUT;
    }

    if (h >= coneLength)
    {
        distanceh = coneLength*cone->getTg() - distanceh;
        sizeh = coneLength*cone->getTg();
        distancev = coneLength*cone->getTgs() - distancev;
        sizev = coneLength*cone->getTgs();
    }
    else
    {
        distanceh = h*cone->getTg() - distanceh;
        sizeh = h*cone->getTg();
        distancev = h*cone->getTgs() - distancev;
        sizev = h*cone->getTgs();
    }

    return pos;
}

Vector3 EllipticConeCylinder::projectionPoint(int i, Vector3 p) {
    Vector3 A = getAxisPoints ()[i];
    Vector3 B = getAxisPoints ()[i+1];
    Vector3 op(p.x()-A.x(), p.y()-A.y(), p.z()-A.z());
    Vector3 dir(B.x()-A.x(), B.y()-A.y(), B.z()-A.z());
    dir.normalize();
    double d = dot(dir, op);
    return Vector3(static_cast<const float &>(A.x() + d * dir.x()),
                   static_cast<const float &>(A.y() + d * dir.y()),
                   static_cast<const float &>(A.z() + d * dir.z()));
}

double EllipticConeCylinder::distanceFromOrigin(Vector3 proj, int index) {
    Vector3 A, B;
    double abciss = 0;

    // perform projection
    for (int ii=0; ii<index; ii++){
        A = getAxisPoints ()[ii];
        B = getAxisPoints ()[ii+1];
        abciss += norm( Vector3(B.x()-A.x(), B.y()-A.y(), B.z()-A.z()) );
    }
    A = getAxisPoints ()[index];
    B = getAxisPoints ()[index+1];
    Vector3 op(proj.x()-A.x(), proj.y()-A.y(), proj.z()-A.z());
    abciss += norm(op);

    return abciss;
}

Vector3 EllipticConeCylinder::computeVerticalDirection(int i) {
    Vector3 vh = computeHorizontalDirection (i);
    Vector3 A = getAxisPoints ()[i];
    Vector3 B = getAxisPoints ()[i+1];
    Vector3 dir(B.x()-A.x(), B.y()-A.y(), B.z()-A.z());
    dir.normalize ();
    Vector3 vv;
    vv = cross (vh, dir);

    return vv;
}

Vector3 EllipticConeCylinder::computeHorizontalDirection(int i) {
//    Vector3 A = getAxisPoints ()[i];
//    Vector3 B = getAxisPoints ()[i+1];
    Vector3 A = getAxisPoints ()[0];
    Vector3 B = getAxisPoints ()[1];
    Vector3 dir(B.x()-A.x(), B.y()-A.y(), B.z()-A.z());
    Vector3 dir1;

    if (abs (dir.z()) < 0.98)
        dir1.set(dir.y(), -dir.x(), 0);
    else {
        dir1.set(cos(10*dir.z()), sin(10*dir.z()), 0);
    }

//    if (abs(dir.z()) < 0.0001){
//        dir1.set(dir.y(), -dir.x(), dir.z());
//    } else {
//        dir1.set(dir.x(), dir.y(), 0);
//        dir1 = cross (dir, dir1);
//     }

    dir1.normalize();
    return dir1;
}

EllipticCone *EllipticConeCylinder::getCone() const {
    return cone;
}

void EllipticConeCylinder::setCone(EllipticCone *cone) {
    EllipticConeCylinder::cone = cone;
}

EllipticCylinder *EllipticConeCylinder::getCylinder() const {
    return cylinder;
}

void EllipticConeCylinder::setCylinder(EllipticCylinder *cylinder) {
    EllipticConeCylinder::cylinder = cylinder;
}

double EllipticConeCylinder::getConeLength() const {
    return coneLength;
}

void EllipticConeCylinder::setConeLength(double coneLength) {
    EllipticConeCylinder::coneLength = coneLength;
}

double EllipticConeCylinder::getDistanceh() const {
    return distanceh;
}

void EllipticConeCylinder::setDistanceh(double distanceh) {
    EllipticConeCylinder::distanceh = distanceh;
}

double EllipticConeCylinder::getDistancev() const {
    return distancev;
}

void EllipticConeCylinder::setDistancev(double distancev) {
    EllipticConeCylinder::distancev = distancev;
}

double EllipticConeCylinder::getSizeh() const {
    return sizeh;
}

void EllipticConeCylinder::setSizeh(double sizeh) {
    EllipticConeCylinder::sizeh = sizeh;
}

double EllipticConeCylinder::getSizev() const {
    return sizev;
}

void EllipticConeCylinder::setSizev(double sizev) {
    EllipticConeCylinder::sizev = sizev;
}

Position EllipticConeCylinder::getPos() const {
    return pos;
}

void EllipticConeCylinder::setPos(Position pos) {
    EllipticConeCylinder::pos = pos;
}

double EllipticConeCylinder::getTg() {
        return cone->getTg();
}

double EllipticConeCylinder::getTgs() {
    return cone->getTgs();
}

