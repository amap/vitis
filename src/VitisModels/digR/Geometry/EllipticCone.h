/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 24.08.18.
//

#ifndef THEVITISNEW_ELLIPTICCONE_H
#define THEVITISNEW_ELLIPTICCONE_H

#include "AxisSymetryShape.h"

class EllipticCone : public AxisSymetryShape{

private:

    double primaryAngle, secondaryAngle;
    double tg, tgs;
    double height;
    Vector3 b1,  b2,  b3,  b4;


public:

    EllipticCone(Vector3 v, Vector3 d, double a, double s);

    void setSecondaryAngle(double secondaryAngle);

    void computeBase();

    virtual ~EllipticCone();

    double getTg() const;

    void setTg(double tg);

    double getTgs() const;

    void setTgs(double tgs);

};


#endif //THEVITISNEW_ELLIPTICCONE_H
