/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 23.08.18.
//
#ifndef THEVITISNEW_CONSTRAINEDROOT_H
#define THEVITISNEW_CONSTRAINEDROOT_H

//#include "../Root.h"
#include "../GeomRoot.h"
#include "EllipticConeCylinder.h"


class EllipticConeCylinderConstraint;
class GeomRoot;

class ConstrainedRoot {

    GeomRoot *parent;

public:
    explicit ConstrainedRoot(GeomRoot *parent);

    void initConstraint(EllipticConeCylinderConstraint *g);

    void computeBoundingMesh(GeomRoot *r, EllipticConeCylinder *c);

    void computeConeAxis(Root *r, vector< vector< vector<double> > >&Axis);

    bool isBended(ParamSet *paramSet, int type);

    void computeBendingParameters(ParamSet *paramSet, Root *r, vector<double>& bendingLength,
                                  vector<double>& finalAbsoluteDirectionToVertical, vector<double>& bendingStartPosition);

    void computeAxisFromParameters (Vector3 initPoint, Vector3 initDir, vector<double>& bendingLengthList, vector<double>& finalDirectionList, vector<double>& bendingStartList, vector<Vector3>& axisPoints, vector<Vector3>& curveCenters, vector<Vector3>& axisDirections);

    Vector3 computeCenter (Vector3& point, Vector3& dir, double a, double length);

    Vector3 computeNextPoint (Vector3& prevPoint, Vector3& prevDir, Vector3& center, double dprec, double d, double l);

    void addArc3d (Vector3& center, double radius, Vector3& startPoint, Vector3& initialDirection, Vector3& axis, double angle, double length,
                   double pos, vector< vector<double> >& path,
                   vector<double>& sh, vector<double>& sw, double cLength, double hCos, double vCos);

    Vector3 caclRandomNormalAxeToMainAxe(Vector3 main);

    };
#endif //THEVITISNEW_CONSTRAINEDROOT_H
