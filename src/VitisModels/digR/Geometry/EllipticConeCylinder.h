/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 23.08.18.
//

#ifndef THEVITISNEW_ELLIPTICCONECYLINDER_H
#define THEVITISNEW_ELLIPTICCONECYLINDER_H

#include "EllipticCone.h"
#include "EllipticCylinder.h"
#include "ExtrudedMesh.h"
#include <vector>
#include "Vector.h"
#include "Position.h"
#include "GeometricalTopology.h"


class EllipticConeCylinder {

private:

    EllipticCone *cone;
    EllipticCylinder *cylinder;
    double coneLength;
    ExtrudedMesh *mesh;
    std::vector<Vector3> axisPoints;
    double distanceh, distancev, sizeh, sizev;
    Position pos;


public:

    EllipticConeCylinder(Vector3 v, Vector3 d, double a, double b, double l);

    virtual ~EllipticConeCylinder();

    ExtrudedMesh *getMesh() const;

    void setMesh(ExtrudedMesh *mesh);

    const std::vector<Vector3> &getAxisPoints() const;

    void setAxisPoints(const std::vector<Vector3> &axisPoints);

    int findProjectionSegment (Vector3 p);

    Position position (Vector3 p, int index);

    Vector3 projectionPoint (int i, Vector3 p);

    double distanceFromOrigin (Vector3 proj, int index);

    Vector3 computeVerticalDirection (int i);

    Vector3 computeHorizontalDirection (int i);

    EllipticCone *getCone() const;

    void setCone(EllipticCone *cone);

    EllipticCylinder *getCylinder() const;

    void setCylinder(EllipticCylinder *cylinder);

    double getConeLength() const;

    void setConeLength(double coneLength);

    double getDistanceh() const;

    void setDistanceh(double distanceh);

    double getDistancev() const;

    void setDistancev(double distancev);

    double getSizeh() const;

    void setSizeh(double sizeh);

    double getSizev() const;

    void setSizev(double sizev);

    Position getPos() const;

    void setPos(Position pos);

    double getTg();

    double getTgs();

};


#endif //THEVITISNEW_ELLIPTICCONECYLINDER_H
