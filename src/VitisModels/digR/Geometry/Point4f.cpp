/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 29.08.18.
//

#include <cmath>
#include "Point4f.h"

Point4f::Point4f(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}

Point4f::Point4f(float *p) {
    x = p[0];
    y = p[1];
    z = p[2];
    w = p[3];
}

Point4f::Point4f(const Point4f& p) {
    x = p.x;
    y = p.y;
    z = p.z;
    w = p.w;
}


float Point4f::distanceSquared(Point4f p1) {
    float dx = x - p1.x;
    float dy = y - p1.y;
    float dz = z - p1.z;
    float dw = w - p1.w;
    return dx * dx + dy * dy + dz * dz + dw * dw;
}

float Point4f::distance(Point4f p1) {
    float dx = x - p1.x;
    float dy = y - p1.y;
    float dz = z - p1.z;
    float dw = w - p1.w;
    return (float)std::sqrt((double)(dx * dx + dy * dy + dz * dz + dw * dw));
}

float Point4f::distanceL1(Point4f p1) {
    return std::abs(x - p1.x) + std::abs(y - p1.y) + std::abs(z - p1.z) + std::abs(w - p1.w);
}

float Point4f::distanceLinf(Point4f p1) {
    float t1 = std::max(std::abs(x - p1.x), std::abs(y - p1.y));
    float t2 = std::max(std::abs(z - p1.z), std::abs(w - p1.w));
    return std::max(t1, t2);
}

void Point4f::project(Point4f p1) {
    float oneOw = 1.0F / p1.w;
    x = p1.x * oneOw;
    y = p1.y * oneOw;
    z = p1.z * oneOw;
    w = 1.0F;
}
