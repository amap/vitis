/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 29.08.18.
//

#ifndef THEVITISNEW_BOUNDINGBOX3D_H
#define THEVITISNEW_BOUNDINGBOX3D_H

#include <iostream>
#include <cmath>
#include "GeometricalTopology.h"

class BoundingBox3f : public GeometricalTopology{
private:

    Vector3* min;
    Vector3* max;


public:

    BoundingBox3f();

    /**
	 * Constructor
	 *
	 * @param inf
	 *            inferior point that define the box
	 * @param sup
	 *            superior point that define the box
	 */
    BoundingBox3f(Vector3 *inf, Vector3 *sup);

    void translateBB(float x, float y, float z);

    bool in (Vector3 p) override;

    bool out (Vector3 p) override;

    bool on (Vector3 p) override;


    void update(Vector3 p);


    /**
	 * method to return the minimum between the main BoundingBox and the given
	 * one
	 *
	 * @param bb
	 *            the given bounding box.
	 * @return Point3D the minimum of the two bounding box.
	 */
    Vector3 getMin(BoundingBox3f bb);

    /**
	 * method to return the maximum between the main BoundingBox and the given
	 * one
	 *
	 * @param bb
	 *            the given bounding box.
	 * @return Point3D the maximum point of the two bounding box.
	 */
    Vector3 getMax(BoundingBox3f bb);

    Vector3 *getMin() const;

    void setMin(Vector3 *min);

    Vector3 *getMax() const;

    void setMax(Vector3 *max);



};


#endif //THEVITISNEW_BOUNDINGBOX3D_H
