/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 24.08.18.
//

#ifndef THEVITISNEW_EXTRUDEDMESH_H
#define THEVITISNEW_EXTRUDEDMESH_H

#include <vector>
#include "Mesh.h"

class ExtrudedMesh : public Mesh {

private:

    std::vector< std::vector<double> > section;
    std::vector< std::vector<double> > axisPath;
    std::vector<double> sectionWidth;
    std::vector<double> sectionHeight;
    double epsilon;


public:

    explicit ExtrudedMesh (std::vector< std::vector<double> >& O, std::vector< std::vector<double> >& P, std::vector<double>& SW, std::vector<double>& SH, bool closed);

    void wire(std::vector< std::vector<double> >& section, std::vector< std::vector<double> >& pathArray, std::vector<double>& w, std::vector<double>& h,bool closed);

    void extrusion (std::vector< std::vector<double> >& section, std::vector< std::vector<double> >& pathArray, std::vector<double>& w, std::vector<double>& h,bool closed);

    void saveParam(std::vector< std::vector<double> > &section, std::vector< std::vector<double> > &pathArray,
                   std::vector<double> &w, std::vector<double> &h);

    bool same (std::vector<double> a, std::vector<double> b);

    int indx (int m, int n, int j);

    ~ExtrudedMesh() override;

    const std::vector< std::vector<double> > &getAxisPath() const;

    void setAxisPath(const std::vector< std::vector<double> > &axisPath);

};


#endif //THEVITISNEW_EXTRUDEDMESH_H
