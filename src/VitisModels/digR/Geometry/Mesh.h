/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 28.08.18.
//

#ifndef THEVITISNEW_MESH_H
#define THEVITISNEW_MESH_H

#include <iostream>
#include <vector>
#include "Point4f.h"
#include "SimpleMesh.h"

class Mesh : public SimpleMesh{
protected:
    bool enableScaling;


public:

    explicit Mesh();

    explicit Mesh(std::vector<Vector3>& pts);

    explicit Mesh(std::vector<Vector3>& pts, std::vector< std::vector<int> >& connections);

    explicit Mesh(std::vector<Vector3>& points, std::vector< std::vector<int> >& paths, std::vector<Vector3>& normals);


    bool isEnableScaling() const;

    void setEnableScaling(bool enableScaling);



    ~Mesh() override;

    /**
	 * Creates a new m by n rectangular mesh ( rows by columns ).
	 *
	 * @param m number of rows
	 * @param n number of columns
	 * @return the current shape geometry
	 */
    void rectangularMesh (int m, int n);

    void newRectangularMesh (int m, int n);

};


#endif //THEVITISNEW_MESH_H
