/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//
// Created by mustafa on 24.08.18.
//

#include "EllipticConeCylinderConstraint.h"
#include "ConstrainedRoot.h"

EllipticConeCylinderConstraint::EllipticConeCylinderConstraint(ConstrainedRoot *constrainedRoot,
                                                               EllipticConeCylinder *coneCylinder) : constrainedRoot(
        constrainedRoot), coneCylinder(coneCylinder) {
    EllipticConeCylinderConstraint::initialized = false;
}

bool EllipticConeCylinderConstraint::isInitialized() const {
    return initialized;
}

void EllipticConeCylinderConstraint::setInitialized(bool initialized) {
    EllipticConeCylinderConstraint::initialized = initialized;
}

void EllipticConeCylinderConstraint::init() {
    constrainedRoot->initConstraint(this);
    EllipticConeCylinderConstraint::initialized = true;
}

EllipticConeCylinder *EllipticConeCylinderConstraint::getConeCylinder() const {
    return coneCylinder;
}

void EllipticConeCylinderConstraint::setConeCylinder(EllipticConeCylinder *coneCylinder) {
    EllipticConeCylinderConstraint::coneCylinder = coneCylinder;
}

bool EllipticConeCylinderConstraint::isConstrained(Matrix4 &geom) {
    if(!isInitialized()){
        init();
    }
    // specific method adapted to ellipticconeCylinder
    Vector3 p(geom.getTranslation().x()+geom.getLength()*geom.getMainVector().x(),
              geom.getTranslation().y()+geom.getLength()*geom.getMainVector().y(),
              geom.getTranslation().z()+geom.getLength()*geom.getMainVector().z());
    int index = coneCylinder->findProjectionSegment(p);
    Position pos = coneCylinder->position(p, index);

    return !(pos == IN);
}

void EllipticConeCylinderConstraint::transform(Matrix4 &geom) {

    // put current main direction to cylinder axis

    Vector3 v, p;
    p.set(geom.getTopPosition ().x(), geom.getTopPosition ().y(), geom.getTopPosition ().z());
    Position pos;

    int index = coneCylinder->findProjectionSegment(p);
    pos = coneCylinder->getPos();

/*
    Vector3 proj = coneCylinder->projectionPoint(index, p);
    Vector3 vv = coneCylinder->computeVerticalDirection(index);
    Vector3 vh = coneCylinder->computeHorizontalDirection(index);

    double vs = dot (vv, Vector3(p.x()-proj.x(), p.y()-proj.y(), p.z()-proj.z()));
    double verticalSign = (vs >= 0) ?  1 : -1;
    double hs = dot (vh, Vector3(p.x()-proj.x(), p.y()-proj.y(), p.z()-proj.z()));
    double horizontalSign = (hs >= 0) ?  1 : -1;

    double currentWidth, currentHeight;

    double a = coneCylinder->distanceFromOrigin (proj, index);
    if (a < coneCylinder->getConeLength()){
        currentWidth = horizontalSign*a*coneCylinder->getTgs();
        currentHeight = verticalSign*a*coneCylinder->getTg();
    }else{
        currentWidth = horizontalSign*coneCylinder->getConeLength()*coneCylinder->getTgs();
        currentHeight = verticalSign*coneCylinder->getConeLength()*coneCylinder->getTg();
    }


    if (pos == OUT) {
        // put it back to the cylinder corner
        v.set (static_cast<const float &>(currentWidth * vh.x() + currentHeight * vv.x() + proj.x() - geom.getColumn(3).x()),
               static_cast<const float &>(currentWidth * vh.y() + currentHeight * vv.y() + proj.y() - geom.getColumn(3).y()),
               static_cast<const float &>(currentWidth * vh.z() + currentHeight * vv.z() + proj.z() - geom.getColumn(3).z()));
    }else if (pos == OUTVERTICAL){
        // compute projection on vertical axis orthogonal to cylinder axis, put it back to cylinder edge
        double d1 = dot (vh, Vector3(p.x()-proj.x(), p.y()-proj.y(), p.z()-proj.z()));
        v.set(static_cast<const float &>(d1 * vh.x() + currentHeight * vv.x() + proj.x() - geom.getColumn(3).x()),
              static_cast<const float &>(d1 * vh.y() + currentHeight * vv.y() + proj.y() - geom.getColumn(3).y()),
              static_cast<const float &>(d1 * vh.z() + currentHeight * vv.z() + proj.z() - geom.getColumn(3).z()));
    }else{
        // compute projection on horizontal axis orthogonal to cylinder axis, put it back to cylinder edge
        double d2 = dot (vv, Vector3(p.x()-proj.x(), p.y()-proj.y(), p.z()-proj.z()));
        v.set (static_cast<const float &>(currentWidth * vh.x() + d2 * vv.x() + proj.x() - geom.getColumn(3).x()),
               static_cast<const float &>(currentWidth * vh.y() + d2 * vv.y() + proj.y() - geom.getColumn(3).y()),
               static_cast<const float &>(currentWidth * vh.z() + d2 * vv.z() + proj.z() - geom.getColumn(3).z()));
    }
*/

    double l = norm(v);

    Vector3 A = coneCylinder->getAxisPoints ()[index];
    Vector3 B = coneCylinder->getAxisPoints ()[index+1];
    A = B - A;
    Vector3 m = geom.getMainVector();
    m.normalize();
    A.normalize();

    if (pos == OUT) {
        // put it back to the cylinder corner
        v.set (A.x(), A.y(), A.z());
    }else if (pos == OUTVERTICAL){
        // compute projection on vertical axis orthogonal to cylinder axis, put it back to cylinder edge
        v.set (m.x(), m.y(), A.z());
    }else{
        v.set (A.x(), A.y(), m.z());
    }

    // control weird redirection (more than 90 degrees)
    Vector4 originalDirection(geom.getDirection (false));
    if (v.x()*originalDirection.x() + v.y()*originalDirection.y() + v.z()*originalDirection.z() < 0){
        v.set (A.x(), A.y(), A.z());
    }

    l = geom.getLength ();
    v.normalize();

    Matrix4 rot(geom);

    rot.setMainVector(v.x(),v.y(),v.z());
    rot.setPlace(12,0);

    // compute second direction in the plan v,d2
    Vector3 d2(geom.getSecondaryDirection().x(),
            geom.getSecondaryDirection().y(),
            geom.getSecondaryDirection().z());
    double scal = dot(d2, v);
    double coeff = norm(Vector3(static_cast<const float &>(-scal * v.x() + d2.x()),
                                static_cast<const float &>(-scal * v.y() + d2.y()),
                                static_cast<const float &>(-scal * v.z() + d2.z())));
    if (coeff != 0)
        coeff = 1 /coeff;
    else
        coeff = 1;
    double coeff2 = -coeff * scal;
    Vector3 second;
    second.set (static_cast<const float &>(coeff2 * v.x() + coeff * d2.x()),
                static_cast<const float &>(coeff2 * v.y() + coeff * d2.y()),
                static_cast<const float &>(coeff2 * v.z() + coeff * d2.z()));

    second.normalize();
    if (dot(second, d2) < 0){
        second *= -1;
    }

    // adjust secondary direction so that philotaxy is maintained
    rot.setSecondaryVector((float)second.x(), (float)second.y(), (float)second.z());
    rot.setPlace(13,0);

    Vector3 third;
    third = cross(v, second);
    third.normalize();
    rot.setNormalVector((float)third.x(), (float)third.y(), (float)third.z());
    rot.setPlace(14,0);

    rot.setTranslation(geom.getTranslation().x(),geom.getTranslation().y(), geom.getTranslation().z());
    rot.setPlace(15, geom.getColumn(3).w());
    geom = rot;
    geom.setLength(l);

}
