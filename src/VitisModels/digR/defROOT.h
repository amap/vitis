/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

/** \file def.h
 *
 * \brief some constant value and symbolic name and macro definition
 * */

#ifndef _DEFROOT_H
#define _DEFROOT_H

#include "defVitis.h"


#ifdef SUN
#define INDEFINI    -1
#else
#define INDEFINI    255
#endif
#define NONBRANCHE    0
#define ACROPETE      1
#define REPIERCING	  2

#define BASAL 0
#define APICAL 1

#define RHIZOTAXY 1
#define BEARERPLAN 2
#define ABSOLUTEPLAN 3

#define ROOT	0
#define SEGMENT 1

#define NOPRUNING 0
#define TOTALPRUNING 1
#define PROGRESSIVEPRUNING 2




#define binwrite(f,n) fwrite((Char *)&(n),sizeof(n),1,f)
#define binread(f,n) fread((Char *)&n, sizeof(n), 1, f)



#endif

