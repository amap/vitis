/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file GrowthUnitAMAP.h
///			\brief Definition of GrowthUnitAMAP class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _SEGMENT_H
#define _SEGMENT_H
#include "Root.h"
#include "DecompAxeLevel.h"
#include "externDigR.h"

class Root;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GrowthUnitAMAP
///			\brief Describe a growth unit ( GU ) : first sublevel decomposition of an AMAP axe
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DIGR_EXPORT Segment :
	public DecompAxeLevel
{
	private:
		/// \brief length of this root segment
		double length;

		/// \brief position of this root segment along root
		double pos;

		/// \brief time this segment was set
		double time;

		Root* get_complex;

		Matrix4 *transformation;

	public:

		/// \brief end growth flag
		char   growthEnd;

		/// \brief Constructor
		Segment(class Plant * plt, double l);
		Segment(class Plant * plt);

		/// \brief Set the length
		void Length(double l){length = l;};
		double Length(){return length;};

		/// \brief Set the birth time
		void Time(double t){time = t;};
		double Time(){return time;};

		/// \brief Set the position along root
		void Pos(double p){pos = p;};
		double Pos(){return pos;};

		/// \brief compute the current diameter of a segment
		float computeDiam (float initialDiameter, float evolDiamStdDev, float timeEvolDiamStdDev, float delayEvolDiamStdDev );

		/// \brief Serialize a GrowthUnitAMAP
		/// \param ar A reference to the archive \see Archive
		virtual void serialize(Archive& ar );

		///setter getter to get the complex(parent root)
        Root *getComplex() const;

        void setComplex(Root *get_complex);

	Matrix4 *getTransformation() const;

	void setTransformation(Matrix4 *transformation);


};

#endif

