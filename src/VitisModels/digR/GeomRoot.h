/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file GeomRoot.h
///			\brief Definition of GeomRoot class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#ifndef _GEOMROOT_H
#define _GEOMROOT_H

#include "Root.h"
#include "GeomBrancCone.h"
#include "ParamFile.h"
#include "Geometry/EllipticConeCylinderConstraint.h"
#include "Geometry/ConstrainedRoot.h"


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class GeomRoot
///			\brief Geometry of an axe based on "trunk-cone" and using axis reference data
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DIGR_EXPORT GeomRoot : public GeomBrancCone {
    /// \brief Axis for the sympodial planar deviation
    Vector3 axesympode;

    /// phyllotaxy for each borne root type
    float *currentPhyllotaxy;

    /// \brief Insertion type:
    ///
    ///   \li INSER1 : constant insertion angle, current phyllotaxy
    ///   \li INSER2 : constant insertion angle, plagiotropy
    ///   \li INSER3 : straight insertion
    ///   \li INSER4 : constant insertion angle according to vertical, current phyllotaxy
    ///   \li INSER5 : pseudo-plagiotropy
    ///   \li INSER6 : secondary direction to the top
    ///   \li INSER7 : random insertion angle and phylotaxy (between 0 and PI)
    ///   \li INSER8 : insertion angle computed according to gravity
    short typeInsert;

    /// \brief	Total bending for the branch
    float totalBending;

    /// \brief Position of inverse bending
    float inflex;

    /// \brief The conicity of the axe
    float conicity;

    /// \brief Deviation angle
    float deviationAngle;

    /// \brief Initial diameter
    float initialDiameter;

    /// \brief Pointer to axis reference
    ParamSet *paramSet;

    float evolDiamStdDev;
    float timeEvolDiamStdDev;
    float delayEvolDiamStdDev;

    float length;

    /// \brief pointer to EllipticConeCylinderConstraint, if null then there is no constraint for this Root.
    /// the computation begin geomRoot class.
    EllipticConeCylinderConstraint *constraint;



public:

    /// \brief Constructor
    /// \param brc Pointer to the hierarchical axe
    ///	\param gbear Pointer to the geometrical bearer axe
    ///	\param gbear Pointer to the GeomManager
    explicit GeomRoot(Hierarc *brc = 0, GeomBranc *gbear = 0, class Plant *gt = 0);

    /// \brief Destructor
    ~GeomRoot() override;

    //! \brief Function to compute the cone geometry of the internode positioned at index \it pos
    void computeEntn(int pos, bool first) override;

    /// \brief Compute initial values of geometric parameters for a branch : Specialization
    virtual void computeParamGeomInit();

    /// \brief Function to initialize geometry : Specialization
    /// \return \li 0 This branc can be "super-simplified" \li 1 otherwise
    int initGeomtry() override;

    /// \brief Compute bending angle for current node according to global flexion of branch.
    /// Compute Deviation and checks if the Root is inside the constraint,
    /// if not perform the transformation to put it inside the constraint again.
    void flexionAtNode() override;

    /// cacl Random Normal Axe To MainAxe of a matrix.
    Vector3 caclRandomNormalAxeToMainAxe(Vector3 main);


    /// \brief Compute initial parameter value for length and diameter
    virtual void computeInitialDiameter();

    //! \brief Function to compute axe geometry and lauch the geometry computing of its children according to bsm order
    ///
    /*! \param bsm Branch Seeking Order (ASCENDING or DESCENDING)
    \return \li 0 This branc will be "super-simplified" \li 1 otherwise */
    int computeBranc(BrancSeekMode bsm, bool silent = false) override;

    //! \brief Function to compute axe geometry and lauch the geometry computing of its children in default ASCENDING order
    ///
    /*! \return \li 0 This branc will be "super-simplified" \li 1 otherwise */
    int computeBranc(bool silent = false) override;

    /// \brief Find the geometrical primitive identifier for each element : Specialization
    int findSymbole() override;

    /// \return the length of the current internode : Specialization
    float computeLength() override;

    /// \return the phyllotaxy angle of insertion on the bearer axe : Specialization
    virtual float computeInsertAnglePhy(float phy);

    virtual float computeSecondDirection();

    /// \return the up-diameter of the current internode
    float computeTopDiam() override;


    /// \brief Insert a new internode on his bearer with the given data
    /// \param currentTriedron
    /// \param referenceTriedron bearer triedron (initial one)
    /// \param typins insertion type (see above)
    /// \param angins insertopn angle
    /// \param angphy phyllotaxy angle
    /// \param aleas randomization factor
    /// \param rnd random value
    virtual Matrix4 axeInsertionOn(Matrix4 &currentTriedron,
                                   Matrix4 &referenceTriedron, /* bearer triedron (initial one) */
                                   long typins,    /* insertion type (see above) */
                                   float angins,   /* insertopn angle */
                                   float angphy);   /* phyllotaxy angle */


    /// \brief rotate mat_ref according to mat_plan to get main direction of mat_ref in first plan of mat_plan
    /// Rotated matrix is stored in plagMat
    void determine_plagio(Matrix4 &plagMat, Vector3 mat_plan, Vector3 mat_ref);


    /// \name Write and read Accessor

    /// \brief set the total bending for the branch
    inline void setTotalBending(float bend) { totalBending = bend; }

    //! \brief Function to simplify the geometry of the axe
    /*! Concatenate successive cone if they are approximatively colinear and got the same symbol number
    */
    void simplification() override;

    float Length() { return length; };

    EllipticConeCylinderConstraint *getConstraint() const;

    void setConstraint(EllipticConeCylinderConstraint *constraint);

    void caclulateConstraint();

};

#endif

