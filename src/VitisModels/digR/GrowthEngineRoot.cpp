/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "DigRMod.h"
#include "RootSystem.h"
#include "GrowthEngineRoot.h"
#include "SimplifTopoRoot.h"
#include "RootBud.h"
#include "defROOT.h"
#include "randomFct.h"
#include <cmath>
#include <limits.h>


GrowthEngineRoot::GrowthEngineRoot(RootBud *b) {
    init(b);
}

GrowthEngineRoot::GrowthEngineRoot(RootBud *b, int type) {
    init(b);

    this->rootType = type;
}

void GrowthEngineRoot::init(RootBud *b) {
    theBud = b;

    theRoot = b->root;

    this->rootType = theRoot->Type();

    lpNumber = static_cast<int>(((ParametricParameter *) theBud->paramSet->getParameter("VerticilleNumber",
                                                                                        this->rootType))->value(theRoot, theRoot->getPlant()));

}

GrowthEngineRoot::~GrowthEngineRoot(void) {

}

void GrowthEngineRoot::setNextRamification(RamificationGrowthBudEvent *d) {

    double pos;
    double time;
    double length;
    if (d->LpIndex() != 65535)
    {
        ParametricParameter *interRamifDistance = (ParametricParameter *) theBud->paramSet->getParameter(
                "interRamifDistance" + std::to_string(rootType), d->LpIndex());

        SingleValueParameter *interRamifDistanceStdDev = (SingleValueParameter *) theBud->paramSet->getParameter(
                "InterRamifDistanceStdDev" + std::to_string(rootType), d->LpIndex());

        pos = d->Pos();
        time = d->Time();
        length = d->getLength();


        // compute next ramification Length according to interRamifDistance
        // density and standard deviation, set the interpolation to true using set interpolation setter.
        if (interRamifDistance != NULL) {
            interRamifDistance->setCurrentInterpolationY(true);
            length = interRamifDistance->value((float) pos, theRoot, theRoot->getPlant());
        }

        //Gaussian Simulation.
        double stdDev = 0;
        if (interRamifDistanceStdDev != NULL)
            stdDev = interRamifDistanceStdDev->value(theRoot, theRoot->getPlant());

        double normalNoise = theBud->getConfigData().getRandomGenerator()->nextGaussian() * stdDev;
        length += normalNoise;
        if (length <= 0.001)
            length = 0.001;
    } else {
        pos = d->Pos();
        time = d->Time();
        length = d->getLength();
    }

    ///growthSpeed has been calculated in theBud constructor.
    // compute time to reach this length.
    time += theBud->growthSpeed->logIntegrate(pos, pos + length, theRoot, theRoot->getPlant());

    d->Time(time);
    d->setLength(length);

    Scheduler::instance()->signal_event(theBud->process_id, d, time, INT_MAX - theBud->ramifOrder);
}

void GrowthEngineRoot::setNextReiterationEvent(ReiterationGrowthBudEvent *d) {

    ParametricParameter *reitDistance = (ParametricParameter *) theBud->paramSet->getParameter("reitDistance",
                                                                                               rootType);
    reitDistance->setCurrentInterpolationY(true);

    double length = reitDistance->value((float) d->Pos(), theRoot, theRoot->getPlant());
    if (length <= 0.001)
        length = 0.001;

    ///growthSpeed has been calculated in theBud constructor.
    double time = theBud->growthSpeed->logIntegrate(d->Pos(), d->Pos() + length, theRoot, theRoot->getPlant());

    d->Time(d->Time() + time);
    d->setLength(length);
    Scheduler::instance()->signal_event(theBud->process_id, d, d->Time(), INT_MAX - theBud->ramifOrder);

}

void GrowthEngineRoot::nextRamification(RamificationGrowthBudEvent *d) {


    double length = d->getLength();
    double time = d->Time();

    // check if death position is closer than ramification
    if (theBud->death && theBud->lastGrowthPosition + length < theBud->lastDeathTestPosition) {
        length = theBud->lastDeathTestPosition - theBud->lastGrowthPosition;
        time = theBud->lastDeathTestTime;
    }

    if (length == 0) {
        return;
    }

    //Time stamp Grow to implementation.

    Segment *node = NULL;
    if (std::floor(theBud->lastGrowthTime) < std::floor(time)) {
        node = growTo(std::floor(time));
    }


    if (   !theBud
        || theBud->death) {
        return;
    }

    length = d->Pos() + d->getLength() - theBud->lastGrowthPosition;
    if (length > 0) {
        node = setSegment(time, theBud->lastGrowthPosition, length);
    }

    if (node == NULL) {
        theRoot->startPosit();
        theRoot->positOnLastSegmentWithinRoot();
        node = theRoot->getCurrentSegment();
        theRoot->endPosit();
    }

    // if bud is not dead or next ramification occur before death set up ramification event
    if (node != NULL) {
        // compute ramification type and set new root
        if (theBud->ramifOrder < ((RootSystem *) theRoot->getPlant())->MaxRamif()) {
            setRamification(d->LpIndex(), node, theBud->lastGrowthPosition, d->Time());
        }
    }

    d->Pos(d->Pos() + d->getLength());
    setNextRamification(d);

}

void GrowthEngineRoot::nextReiteration(ReiterationGrowthBudEvent *ev) {

    double length = ev->getLength();
    double time = ev->Time();

    // check if death position is closer than reiteration
    if (theBud->death && theBud->lastGrowthPosition + length < theBud->lastDeathTestPosition) {
        length = theBud->lastDeathTestPosition - theBud->lastGrowthPosition;
        time = theBud->lastDeathTestTime;
    }

    if (length == 0)
        return;

    Segment *node = NULL;
    if (std::floor(theBud->lastGrowthTime) < std::floor(time)) {
        node = growTo(std::floor(time));
    }

    if (theBud->death)
        return;

    length = ev->Pos() + ev->getLength() - theBud->lastGrowthPosition;
    if (length > 0) {
        node = setSegment(time, theBud->lastGrowthPosition, length);
    }

    if (node == NULL) {
        theRoot->startPosit();
        theRoot->positOnLastSegmentWithinRoot();
        node = theRoot->getCurrentSegment();
        theRoot->endPosit();
    }

    if (!theBud->death && node != NULL) {
        // compute ramification type and set new root
        int order = 0;
        int maxOrder = theBud->paramSet->getParameter("MaxReit", rootType)->value(theRoot, theRoot->getPlant());
        Root *current = theRoot;
        while (   current->getBearer() != current
               && current->getBearer()->Type() == rootType) {
            order++;
            current = current->getBearer();
        }
        if (order < maxOrder) {
            setReiteration(node, theBud->lastGrowthPosition, ev->Time());
        }
    }

    ev->Pos(ev->Pos() + ev->getLength());
    setNextReiterationEvent(ev);

}

double GrowthEngineRoot::expIntegrate(RootBud *rBud, double _beginTime, double _endTime) {

    ///growthSpeed has been calculated in theBud constructor.
    double p1 = individualExpIntegrate(rBud->timeGrowthSpeed, rBud->growthSpeed, _beginTime);
    double p2 = individualExpIntegrate(rBud->timeGrowthSpeed, rBud->growthSpeed, _endTime);

    return p2 - p1;

}

double GrowthEngineRoot::individualExpIntegrate(ParametricParameter *_t, ParametricParameter *_p, double _time) {
    int t_counter = 0, p_counter = 0;
    double sum, v1, p1, t1, v2, p2, t2;

//    _p->CurrentNb_Y(); //count
//    _p->getCurrentYTab(); //position
//    _p->getCurrentValTab(); // value

    if (p_counter >= _p->CurrentNb_Y()) {
        return 0;
    }

    vector<float> tPos = _t->getCurrentYTab();
    vector<float> pPos = _p->getCurrentYTab();


    double mt = tPos[t_counter++];
    double mp = pPos[p_counter++];

    if (_time <= mt) {
        sum = _t->value((float) mt, theRoot, theRoot->getPlant()) * _time;
    } else {
        sum = _t->value((float) mt, theRoot, theRoot->getPlant()) * mt;
    }

    p1 = mp;
    t1 = mt;
    v1 = _t->value((float) mt, theRoot, theRoot->getPlant());

    while (t_counter < _t->CurrentNb_Y()) {

        mt = tPos[t_counter++];
        mp = pPos[p_counter++];
        p2 = mp;
        v2 = _t->value((float) mt, theRoot, theRoot->getPlant());

        if (mt <= _time) {
            t2 = mt;
        } else {
            t2 = _time;
        }
        if (std::abs(v2 - v1) > 0.000001) {
            sum += std::exp((v2 - v1) / (p2 - p1) * (t2 - t1))
                   * (p1 + (v1 * p2 - v2 * p1) / (v2 - v1))
                   - (v1 * p2 - v2 * p1) / (v2 - v1) - p1;
        } else {
            sum += (t2 - t1) * v2;
        }

        if (mt >= _time) {
            break;
        }

        p1 = mp;
        t1 = mt;
        v1 = _t->value((float) mt, theRoot, theRoot->getPlant());

    }

    if (_time > mt) {
        sum += _t->value((float) mt, theRoot, theRoot->getPlant()) * (_time - mt);
    }

    return sum;
}

Segment *GrowthEngineRoot::growTo(double age) {

    if (theBud->death) {
        return NULL;
    }

    if (age <= theBud->birthTime) {
        return NULL;
    }

    Segment *node = NULL;
    double t = theBud->lastGrowthTime + 1;
    if (t > age) {
        t = age;
    }

    double length = 0;

    while (t <= age) {

        length = expIntegrate(theBud, theBud->lastGrowthTime - theBud->birthTime, t - theBud->birthTime);

        if (length > 0) {

            short pruned = theBud->testDeath(t);

            if (   theBud
                && theBud->death) {
                if (   !pruned
                    && theBud->lastDeathTestPosition - theBud->lastGrowthPosition > 0) {
                    node = setSegment(t, theBud->lastGrowthPosition,
                                      theBud->lastDeathTestPosition - theBud->lastGrowthPosition);
                }
                break;
            }
            if (!pruned)
                node = setSegment(t, theBud->lastGrowthPosition, length);
        }

        t++;

        if (t > age && t < age + 1) {
            t = age;
        }
    }

    return node;
}


Segment *GrowthEngineRoot::setSegment(double _time, double _position, double _length) {

    if (_length <= 0) {
        return 0;
    }

    Segment *n = (Segment *) theRoot->V_plt()->getPlantFactory()->createInstanceDecompAxe(SEGMENT, "DigRMod");
    n->Length(_length);
    n->Time(_time);
    n->setComplex(theRoot);
//        double* bTime = new double(_time);
//		n->addAdditionalData("birthTime", bTime);


   if (theBud->lastGrowthPosition > _position + _length) {

        theRoot->startPosit();

        theRoot->positOnFirstSegmentWithinRoot();

        Segment *predecessor = theRoot->getCurrentSegment();
        if (predecessor != NULL) {

            double curLength = predecessor->Length();

            while (predecessor != NULL &&
                   curLength < _position + _length) {

                theRoot->positOnNextSegmentWithinRoot();
                Segment *predecessor = theRoot->getCurrentSegment();

                if (predecessor != NULL) {
                    curLength += predecessor->Length();
                }

            }

            if (predecessor != NULL) {
                //insert a predecessor
                theRoot->addSegment(n, theRoot->getCurrentSegmentIndex() - 1);


                predecessor->Length(curLength - _position - _length);
            }
        } else {
            theRoot->addSegment(n);
        }

        theRoot->endPosit();



    } else {

        _length = _position + _length - theBud->lastGrowthPosition;

        if (_length > 1e-10) {
            n->Length(_length);
            theRoot->addSegment(n);
        } else {
            delete n;
            theRoot->startPosit();
            theRoot->positOnLastSegmentWithinRoot();
            n = theRoot->getCurrentSegment();
            _length = 0;
            theRoot->endPosit();
        }
        theBud->lastGrowthTime = _time;
        theBud->lastGrowthPosition += _length;
        theRoot->Length(theBud->lastGrowthPosition);
    }

    theBud->testDeath(_time);

    return n;

}

void GrowthEngineRoot::setRamification(int id, Segment *bearer, double position, double time) {
    //    _p->CurrentNb_Y(); //count
    //    _p->getCurrentYTab(); //position
    //    _p->getCurrentValTab(); // value

    // build the deviated sum
    int type_Count = 0;

    ParametricParameter *typeFrequency = (ParametricParameter *) theBud->paramSet->getParameter(
            "typeFrequency" + std::to_string(rootType), id);
    if (typeFrequency) {
        type_Count = typeFrequency->CurrentNb_Y();
    }

    double stdDev = 0;
    SingleValueParameter *typeFrequencyStdDev = (SingleValueParameter *) theBud->paramSet->getParameter(
            "TypeFrequencyStdDev" + std::to_string(rootType), id);
    if (typeFrequencyStdDev) {
        stdDev = typeFrequencyStdDev->value(theRoot, theRoot->getPlant());
    }

    // GaussianSimulation saved in this double array
    double type_val_array[type_Count];
    for (int i = 0; i < type_Count; i++) {
        float val = typeFrequency->value (0, i, theRoot, theRoot->getPlant());
        type_val_array[i] = theRoot->getPlant()->getConfigData().getRandomGenerator()->nextGaussian () *
//                (stdDev * typeFrequency->getCurrentValTab()[i] / 100)
                (stdDev * val / 100) + val;
//                + typeFrequency->getCurrentValTab()[i];
    }
    int type = -1;

    // compute the new root type
    double threshold = theRoot->getPlant()->getConfigData().getRandomGenerator()->Random();
    double cur = 0;
    for (int i = 0; i < type_Count; i++) {
        cur += type_val_array[i] / 100;
        if (threshold <= cur) {
            type = static_cast<int>(typeFrequency->getCurrentYTab()[i]);
            break;
        }
    }


    if (type >= 0) {
        Root *rb;

        if (bearer == 0){
            rb = 0;
        }else{
            rb = bearer->getComplex();
        }

        if (   rb == 0
            || rb->Type() != type) {
            double proba = 0;
            ParametricParameter *verticilleProba = (ParametricParameter *) theBud->paramSet->getParameter(
                    "verticilleProba", type);
            if (verticilleProba) {
                verticilleProba->setCurrentInterpolationY(true);
                proba = verticilleProba->value((float) position, theRoot, theRoot->getPlant());
            }

            int VerticilleNumber = static_cast<int>(((ParametricParameter *) theBud->paramSet->getParameter(
                    "VerticilleNumber",
                    type))->value(theRoot, theRoot->getPlant()));

            for (int i = 0; i < VerticilleNumber; i++) {
                threshold = theBud->getConfigData().getRandomGenerator()->Random();
                if (threshold <= proba) {

                    double growthDelayVal = 0;
                    ParametricParameter *growthDelay = (ParametricParameter *) theBud->paramSet->getParameter(
                            "growthDelay", type);
                    if (growthDelay) {
                        growthDelay->setCurrentInterpolationY(true);
                        growthDelayVal = growthDelay->value((float) position, theRoot, theRoot->getPlant());
                    }

                    double nextInstant = time + growthDelayVal;

                    if (nextInstant > Scheduler::instance()->getHotStop()
                        && nextInstant < Scheduler::instance()->getHotStop() + EPSILON)
                        nextInstant = Scheduler::instance()->getHotStop();

                    if (nextInstant >= time) {
                        Root *bVert = (Root *) theRoot->getPlant()->getPlantFactory()->createInstanceBranc("DigRMod");

                        bVert->set(type, 0);

                        theRoot->getPlant()->getTopology().fork(bVert, theRoot->CurrentHierarcPtr(), (char) i + 1,
                                                                (USint) VerticilleNumber);

                        bVert->addBud(
                                (RootBud *) (theRoot->getPlant()->getPlantFactory())->createInstanceBud(
                                        3, bVert, nextInstant, type));

                        bVert->setOffset(static_cast<float>(theBud->lastGrowthPosition));

                        // setup verticile at the end of the segment
                        std::string critere;
                        if (theRoot->getPlant()->getConfigData().cfg_topoSimp == SIMP_LEAVES_AND_BRANC_MANY
                            || theRoot->getPlant()->getConfigData().cfg_topoSimp == SIMP_LEAVES_AND_BRANC_ONCE)
                            critere = ((SimplifTopoRoot *) (theRoot->getPlant()->getTopology().getTopoSimplifier()))->formatCriteria(
                                    type, nextInstant, 1, theBud->ramifOrder);

                        theRoot->getPlant()->getTopology().updateSimplifier(bVert, critere);

                        Scheduler::instance()->create_process(bVert->getBud(), NULL, nextInstant,
                                                              INT_MAX - theBud->ramifOrder);

                        bVert->theBud->getConfigData().getRandomGenerator()->Random();
                        bVert->theBud->getConfigData().getRandomGenerator()->Random();

                    }
                }
            }
        }
    }

}

void GrowthEngineRoot::setReiteration(Segment *pSegment, double position, double time) {

    double threshold = theRoot->getPlant()->getConfigData().getRandomGenerator()->Random();
    double proba = 0;
    ParametricParameter *reitFrequency = (ParametricParameter *) theBud->paramSet->getParameter("reitFrequency",
                                                                                                rootType);
    if (reitFrequency) {
        reitFrequency->setCurrentInterpolationY(true);
        proba = reitFrequency->value((float) position, theRoot, theRoot->getPlant());
    }
    if (proba < threshold * 100)
        return;

    //create the new root.
    Root *bVert = (Root *) theRoot->getPlant()->getPlantFactory()->createInstanceBranc("DigRMod");

    bVert->set(rootType, 0);

    theRoot->getPlant()->getTopology().fork(bVert, theRoot->CurrentHierarcPtr(), (char) 1,
                                            (USint) lpNumber);

    pSegment->getComplex()->startPosit();
    pSegment->getComplex()->positOnFirstSegmentWithinRoot();
    double birthTime = pSegment->getComplex()->getCurrentSegment()->Time();
    pSegment->getComplex()->endPosit();

    bVert->addBud(
            (RootBud *) ((DigRModele *) theRoot->getPlant()->getPlantFactory())->createInstanceBud(
                    3, bVert, birthTime, rootType));

    bVert->theBud->lastGrowthPosition = theBud->lastGrowthPosition;
    bVert->theBud->lastDeathTestPosition = theBud->lastDeathTestPosition;
    bVert->theBud->lastGrowthTime = theBud->lastGrowthTime;
    bVert->theBud->lastDeathTestTime = theBud->lastDeathTestTime;

    bVert->setOffset(static_cast<float>(theBud->lastGrowthPosition));

    // setup verticile at the end of the segment
    std::string critere;
    if (theRoot->getPlant()->getConfigData().cfg_topoSimp == SIMP_LEAVES_AND_BRANC_MANY
        || theRoot->getPlant()->getConfigData().cfg_topoSimp == SIMP_LEAVES_AND_BRANC_ONCE)
        critere = ((SimplifTopoRoot *) (theRoot->getPlant()->getTopology().getTopoSimplifier()))->formatCriteria(
                rootType, time, 1, theBud->ramifOrder);

    theRoot->getPlant()->getTopology().updateSimplifier(bVert, critere);

    Scheduler::instance()->create_process(bVert->getBud(), NULL, time,
                                          INT_MAX - theBud->ramifOrder);

    bVert->theBud->getConfigData().getRandomGenerator()->Random();
    bVert->theBud->getConfigData().getRandomGenerator()->Random();

}

void GrowthEngineRoot::serialize(Archive &ar) {
    int nb = ((RootSystem *) (theRoot->getPlant()))->NbType();
    if (ar.isWriting()) {
        ar << rootType << lpNumber << nb;
        for (int i = 0; i < lpNumber; i++) {
            for (int j = 0; j < nb; j++) {
                ar << ramifTypeFrequencyNoise[i][j];
            }
        }
    } else {
        int nb;
        ar >> rootType >> lpNumber >> nb;
        ramifTypeFrequencyNoise = new float *[lpNumber];
        for (int i = 0; i < lpNumber; i++) {
            ramifTypeFrequencyNoise[i] = new float[((RootSystem *) (theRoot->getPlant()))->NbType()];
            for (int j = 0; j < nb; j++) {
                ar >> ramifTypeFrequencyNoise[i][j];
            }
        }
    }
}







