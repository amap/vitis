/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  RootSystem.h
///			\brief Definition of RootSystem class.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __ROOTSYSTEM_H__
#define __ROOTSYSTEM_H__

#include "externDigR.h"
#include "Plant.h"
#include "ParamFile.h"
#include "ParamSetDigR.h"
#include "Root.h"
#include "VitisCoreSignalInterface.h"

class RootSystem;

struct Geom : public subscriber<messageVitisBeginGeomCompute> {
public:
    RootSystem *rootSys;

    Geom(void *p);

    virtual ~Geom();

    void on_notify(const state &st, paramMsg *data = NULL);
};

struct endGeom : public subscriber<messageVitisEndGeomCompute> {
public:
    RootSystem *rootSys;

    endGeom(void *p);

    virtual ~endGeom();

    void on_notify(const state &st, paramMsg *data = NULL);
};


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class RootSystem
///			\brief AMAP describtion for a plant , specialize Plant with adding a pointer to an axis reference
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DIGR_EXPORT RootSystem : public Plant {
    /// \brief Pointer to the axis reference
    int nbType;
    int maxRamif;
    int *maxReit;
public:

    Geom *theGeom;
    endGeom *theEndGeom;

    /// \brief Default constructor
    RootSystem();

    /// \brief Constructor
    RootSystem(void *p);

    /// \brief Constructor
    RootSystem(const std::string &nameT, const std::string &nameP);

    /// \brief Destructor
    virtual ~RootSystem(void);

    /// \brief Seed the plant  at the given time
    virtual void seedPlant(float time);


    virtual void init();

    /// \brief  init the current instance of plant builder
    virtual PlantBuilder *instanciatePlantBuilder();

    virtual void serialize(Archive &ar);

    /// \return the axis reference
    inline int NbType() { return nbType; };

    inline int MaxReit(int i) { if (i < nbType) return maxReit[i]; else return 0; };

    inline int MaxRamif() { return maxRamif; };

    inline void setMaxReit (int *m) {maxReit = m;};


};

#if defined DIGR_EXPORTS
/// \brief C function , called by the "Load" function of SimulationModel in the Vitis framework to get the simulation modele
/// \return A instance of AmapSimModele
extern "C" DIGR_EXPORT  Plant * StartPlugin (void *p);

#endif


#endif

