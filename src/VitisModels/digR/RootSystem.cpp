/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "ParamFile.h"
#include "DigRMod.h"
#include "RootSystem.h"
#include "Root.h"
#include "SimplifTopoRoot.h"
#include "RootBud.h"

Geom::Geom(void *p) {
    rootSys = reinterpret_cast<RootSystem *>(p);
    subscribe(p);
}

endGeom::endGeom(void *p) {
    rootSys = reinterpret_cast<RootSystem *>(p);
    subscribe(p);
}

Geom::~Geom() {
    unsubscribe();
}

endGeom::~endGeom() {
    unsubscribe();
}

void Geom::on_notify(const state &, paramMsg *data) {
    Hierarc *hierc = rootSys->getTopology().getTrunc();

    Root *p = (Root *) hierc->getBranc();
    // if(p->theBud->lastGrowthTime <= Scheduler::instance()->getTopClock())

    p->growTo(Scheduler::instance()->getTopClock());


}

void endGeom::on_notify(const state &, paramMsg *data) {
    rootSys->getGeometry().HasToBeComputed(true);
}

RootSystem::RootSystem() {
    v_topoMan->setTopoSimplifier(new SimplifTopoRoot(this));
    maxReit = NULL;


}

RootSystem::RootSystem(void *p) {
    v_topoMan->setTopoSimplifier(new SimplifTopoRoot(this));
    maxReit = NULL;
    theGeom = new Geom(this);
    theEndGeom = new endGeom(this);

}

RootSystem::RootSystem(const std::string &nameT, const std::string &nameP) : Plant(nameT, nameP) {
    v_topoMan->setTopoSimplifier(new SimplifTopoRoot(this));
    maxReit = NULL;


}


RootSystem::~RootSystem(void) {
    if (maxReit)
        delete[] maxReit;
    maxReit = 0;
    if (theGeom) {
        delete theGeom;
        theGeom = 0;
    }
    if (theEndGeom) {
        delete theEndGeom;
        theEndGeom = 0;
    }

}


void RootSystem::init() {
    Plant::init();
    getPlantFactory()->setLevelOfDecomposition(string("DigRMod"), 1);
    if (getParamData(string("DigRMod")) == NULL) {
        setParamData(string("DigRMod"), new ParamSetDigR(getParamName(string("DigRMod"))));
    }

    getConfigData().simuTotalTime = Scheduler::instance()->getHotStop();

    ParamSetDigR *paramSet = (ParamSetDigR*)getParamData(string("DigRMod"));

    nbType = paramSet->getParameter("nbType")->value();

    maxRamif = paramSet->getParameter("maxRamif")->value();
    maxReit = new int[nbType];
    for (int i = 0; i < nbType; i++)
        maxReit[i] = paramSet->getParameter("MaxReit", i)->value();

//	paramSet->write();
}


void RootSystem::seedPlant(float time) {
    Plant::seedPlant(time);
    ParamSetDigR *paramSet = (ParamSetDigR*)getParamData(string("DigRMod"));

    if (v_actionScheduler->getHotStop() > paramSet->getParameter("maxAge")->value()) {
        return;
    }

    Root *bvit = (Root *) v_pltBuilder->createInstanceBranc("DigRMod");

    bvit->set(0, 0);

    v_topoMan->fork(bvit, NULL);

    bvit->addBud((RootBud*)v_pltBuilder->createInstanceBud(3, bvit, 0, 0));

    //bvit->getBud()->setTimeAndPriority(bvit->getBud()->timeForATest,10);

    v_actionScheduler->create_process(bvit->getBud(), NULL, 0, 10);

}


PlantBuilder *RootSystem::instanciatePlantBuilder() {
    return new DigRModele(this);
}


void RootSystem::serialize(Archive &ar) {

    Plant::serialize(ar);

    if (ar.isWriting()) {
        ar << nbType << maxRamif;
        for (int i = 0; i < nbType; i++)
            ar << maxReit[i];
    } else {
        ar >> nbType >> maxRamif;
        maxReit = new int[nbType];
        for (int i = 0; i < nbType; i++)
            ar >> maxReit[i];
    }
}


#if !defined STATIC_LIB
extern "C" Plant *StartPlugin(void *p) {
    return new RootSystem(p);
}
#endif

