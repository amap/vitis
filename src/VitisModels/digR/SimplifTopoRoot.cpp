/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include "RootSystem.h"
#include "SimplifTopoRoot.h"
#include "Root.h"
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <math.h>

SimplifTopoRoot::SimplifTopoRoot(Plant * plt):SimplifTopo(plt)
{
}

SimplifTopoRoot::~SimplifTopoRoot(void)
{
}

const std::string SimplifTopoRoot::formatCriteria(int phyAge, double time, double rythm, char ordre, double birthThreshold)
{
	double t;

	if (birthThreshold == 0)
		t = time;
	else
		t = time - fmod (time, birthThreshold);

	return formatCriteria (phyAge, t, rythm, ordre);

}

const std::string SimplifTopoRoot::formatCriteria(int phyAge, double time, double rythm, char ordre)
{
	std::ostringstream oss;


	//oss<<std::fixed<<std::setprecision(5)<<rythm<<time<<phyAge;
	oss<<(int)rythm<<time<<phyAge<<ordre;
	//oss<<phyAge<<ordre;

	return oss.str();

}

Branc * SimplifTopoRoot::getSimplifiedRoot(const std::string & critere, int indexInCriteria)
{

	if (   v_plt->getConfigData().cfg_topoSimp == SIMP_LEAVES_AND_BRANC_ONCE
		 || v_plt->getConfigData().cfg_topoSimp == SIMP_LEAVES)
		indexInCriteria = 0;
	else
	{

		int savCounter = v_plt->getConfigData().getRandomGenerator()->getCounter();
		int savIdx = v_plt->getConfigData().getRandomGenerator()->setState(1);

		if (v_plt->getConfigData().cfg_topoSimp == SIMP_LEAVES_AND_BRANC_MANY)
			indexInCriteria = (int)ceil(v_plt->getConfigData().getRandomGenerator()->Random()*2-1);
		else
			indexInCriteria = (int)ceil(v_plt->getConfigData().getRandomGenerator()->Random() * (v_plt->getConfigData().cfg_topoSimp-2)-1);

		v_plt->getConfigData().getRandomGenerator()->setState(savIdx);
		v_plt->getConfigData().getRandomGenerator()->setCounter(savCounter);
	}
	return SimplifTopo::getSimplifiedBranc(critere, indexInCriteria);

}
