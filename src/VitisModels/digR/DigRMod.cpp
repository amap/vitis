/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <cstdarg>
#include "DigRMod.h"
#include "defROOT.h"
#include "RootSystem.h"
#include "GeomRoot.h"
#include "Segment.h"



DigRModele::DigRModele(Plant * plt):PlantBuilder(plt)
{
}



DigRModele::~DigRModele()
{

}


GeomBranc * DigRModele::createInstanceGeomBranc (Hierarc * brc, GeomBranc * gbr)
{
	return new GeomRoot(brc,gbr,V_plt());
}

Branc * DigRModele::createInstanceBranc ()
{
	return new Root(V_plt());
}

DecompAxeLevel * DigRModele::createInstanceDecompAxe (USint level)
{
	switch(level)
	{
		case SEGMENT : return new Segment (V_plt()); break;
		default : return new DecompAxeLevel(level);break;
	}
}


VProcess * DigRModele::createInstanceBud ( int n, ...)
// Root * br, double timeC, int phyAgeBeg)
{
    Root *br;
    double timeC;
    int phyAgeBeg;
    int num;
    va_list arguments;                     // A place to store the list of arguments

    va_start ( arguments, n ); // Initializing arguments to store all values after n

    br = va_arg ( arguments, Root *);
    timeC = va_arg (arguments, double);
    phyAgeBeg = va_arg (arguments, int);

    va_end ( arguments );                  // Cleans up the list
    return new RootBud( br, timeC, phyAgeBeg);
}





