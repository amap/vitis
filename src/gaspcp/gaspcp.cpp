/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WIN32
#ifdef DEBUG
//#include <vld.h>

#endif
#endif
#include <stdio.h>
#include <string.h>
#include <cstring>
#include "Output.h"
#include "RootSystem.h"
#include "tinyxml.h"
#include "WholePlant.h"
#include "ObserverManager.h"
#include "PlotManager.h"

#include "MIRScript.h"


void read_wholePlant_arguments(long  nbarg, char **arguments, WholePlant * plt, ObserverManager *omgr);
void read_arguments(long  nbarg, char **arguments, ObserverManager *omgr);

//OutputManager & outpMan= OutputManager::instance();
OutputManager *outpMan = OutputManager::instance();

int main ( int argc, char **argv)
{
	ObserverManager omgr;

	read_arguments(argc, argv, &omgr);

	Scheduler::instance()->run_simulation();

	exit (1);
}

void usage(void)
{
	printf ("usage : gaspcp aerial root age [seed] [-sGeomSimplification] [-STopoSimplification [-deltaDelta]] [-g[step]] [-gl[step]] [-a[step]] [-oOutputFileName] [-cModule [-eModuleData]] [[-fsstep] -foCustomOutputFormat]\n");
	printf ("        aerial : name of the aerial part of the plant (if single name, .cfg file will be used for parameter file location)\n");
	printf ("        root : name of the root part of the plant (if single name, .cfg file will be used for parameter file location)\n");
	printf ("        age : age to be computed\n");
	printf ("        seed : random number initialisation number\n");
	printf ("        -s : GeomSimplification is the simplification level on geometrical output (from 0 to 4)\n");
	printf ("        -S : TopoSimplification is the simplification level on topological computing (from 0 to 3)\n");
	printf ("        -delta : birth date threshold to build simplified classes\n");
	printf ("        step : output step (only at simulation end by default)\n");
	printf ("        -o : output to a particular OutputFileName file (without extension), this MUST be a complete path/filename\n");
	printf ("        -g : output linetree format\n");
	printf ("        -gl : output opf format\n");
	printf ("        -a : output mtg format\n");
	printf ("        -n : output natFX format\n");
	printf ("        -c : dynamicaly link the module file Module to the simulation kernel\n");
	printf ("        -e : provide the current Module with ModuleData as private data configuration file\n");
	printf ("        -swap : change bytes output order (switch between little and big endian\n");
	printf ("        -fs : custom output step\n");
	printf ("        -fo : load a custom output format module\n");
	printf ("        -posx : the x world position of this plant (cm)\n");
	printf ("        -posy : the y world position of this plant (cm)\n");
	printf ("        -posz : the z world position of this plant (cm)\n");
	printf ("        -begin : the absolute beginning time for growth simulation\n");
	printf ("or    : gaspcp Scene\n");
	printf ("        Scene : a text file describing a scene which means that each line consists in a single plant simulation command\n");
#ifdef WIN32
	Sleep(10000);

#endif
	exit (0);
}

int extractArg (char *lin, char **arg)
{
	int nb = 0;
	int pos = 0;
	char curArg[499], *line=lin;
	int posarg;

//cout << "traitement de " << lin << endl;

	while (sscanf (line, "%s", curArg) == 1)
	{
		line = strstr (line, curArg);
		if ((strchr(curArg, '"')) != NULL)
		{
			// remove first quote
			pos = posarg = 0;
			while (line[pos] != '"')
			{
				curArg[posarg] = line[pos];
				posarg++;
				curArg[posarg] = 0;
				pos++;
			}
			pos++;
			// seek for next quote
			while (   line[pos] != '"'
				   && line[pos] != 0)
			{
				curArg[posarg] = line[pos];
				posarg ++;
				curArg[posarg] = 0;
				pos++;
			}
			if (line[pos] != 0)
				line = &line[pos+1];
		}
		else
		{
			line = &line[strlen(curArg)];
		}

		strcpy (arg[nb], curArg);
//cout << arg[nb] << "  reste "<< line << endl;
		nb++;

	}
	return nb;
}


void read_arguments(long  nbarg, char **arguments, ObserverManager *omgr)
{
	WholePlant *p;
	if (nbarg == 2)
	{
        char str[256];
	    if (arguments[1][0] == '\"')
	    {
            strcpy (str, &arguments[1][1]);
            str[strlen(str)-1] = 0;
	    }
        else
            strcpy (str, arguments[1]);

        ifstream fic (str);
		char line[2000];
		long nb;
		char **arg;

		if (!fic)
		{
			perror (arguments[1]);
			return;
		}
		while (!fic.eof())
		{
			fic.getline (line, 1999);
			if (line[0] == '#')
				continue;

			arg = new char*[20];
			for (int i=0; i<20; i++)
			{
				arg[i] = new char[500];
				for (int j=0; j<500; j++)
					arg[i][j] = 0;
			}

			nb = 0;
//			nb = sscanf (line, "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s", arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], arg[6], arg[7], arg[8], arg[9], arg[10], arg[11], arg[12], arg[13], arg[14], arg[15], arg[16], arg[17], arg[18], arg[19]);
			nb = extractArg (line, arg);
			if (nb < 2)
			{
				continue;
			}
			if (strncmp (arg[0], "gaspcp", 5) == 0)
			{
				p = new WholePlant;
				read_wholePlant_arguments (nb, arg, p, omgr);
				PlotManager::instance()->checkCoordinates(p);
			}
			else if (strncmp (arg[0], "MIRScript", 5) == 0)
			{
				if (nb < 4)
				{
					cout << "usage : MIRScript MIRParamFileName SceneFileName SimeoDirectoryName [startTime]" << endl;
					cout << "        MIRParamFileName : the absolute path to the MIR configuration file" << endl;
					cout << "        SceneFileName : the absolute path to the scene file " << endl;
					cout << "        SimeoDirectoryName : the absolute path to the directory where to find simeo" << endl;
					cout << "        startTime : time when to start Mir computing" << endl;
#ifdef WIN32
					Sleep(10000);
#endif
					exit(0);
				}
				if (nb == 4)
					MIRScript *m = new MIRScript (arg[1], arg[2], arg[3]);
				else
				{
					float st=atof(arg[4]);
					MIRScript *m = new MIRScript (arg[1], arg[2], arg[3], st);
				}
			}
			else
			{
				continue;
			}
		}

		fic.close();

	}
	else
	{
		p = new WholePlant;
		read_wholePlant_arguments (nbarg, arguments, p, omgr);
	}
}

void read_wholePlant_arguments(long  nbarg, char **arguments, WholePlant *plt, ObserverManager *omgr)
{
	bool trouve, outplOk=false, outpglOk=false, outpmOk=false, outpcOk=false, outpnfxOk=false, swap_bytes=false;;
	long  i;
	float f;
	string nameOutput, outpcName;
	float step_LineTree=0;
	float step_Natfx=0;
	float step_AmapMod=0;
	float step_Custom=0;
	float step_Glds=0;
	float x=0, y=0, z=0;
	float beginTime=0;
	Vector3 position;

	outpcName.empty();
	nameOutput.empty();

	if(nbarg<=1)
	{
		usage();
	}
	else
	{

		do
		{
			trouve=0;


			if (strncmp (arguments[nbarg-1], "-o", 2) == 0)
			{
				nameOutput=&arguments[nbarg-1][2];
				nbarg --;
				trouve = true;
			}



			if (strncmp (arguments[nbarg-1], "-gl", 3) == 0)
			{
			    outpglOk = true;
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][3], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][3], "%f", &f) > 0)
#endif
				{
					step_Glds = f;
				}
				nbarg --;
				trouve = 1;
			}
			if (strncmp (arguments[nbarg-1], "-swap", 5) == 0)
			{

				swap_bytes = true;
				nbarg --;
				trouve = 1;
			}
			if (strncmp (arguments[nbarg-1], "-g", 2) == 0)
			{
			    outplOk = true;
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][2], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%f", &f) > 0)
#endif
				{
					step_LineTree = f;
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-a", 2) == 0)
			{
			    outpmOk = true;
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][2], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%f", &f) > 0)
#endif
				{
					step_AmapMod = f;
				}
				nbarg --;
				trouve = 1;
			}



			if (strncmp (arguments[nbarg-1], "-n", 2) == 0)
			{
			    outpnfxOk = true;
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][2], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%f", &f) > 0)
#endif
				{
					step_Natfx = f;
				}
				nbarg --;
				trouve = 1;
			}



			if (strncmp (arguments[nbarg-1], "-s", 2) == 0)
			{
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][2], "%ld", &i) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%ld", &i) > 0)
#endif
				{
					if (   i >= 0
						&& i < 5)
						plt->getConfigData().cfg_geoSimp = (USint)i;
					else if (i >= 5)
					{
						plt->getConfigData().cfg_geoSimp = 1;
						plt->getConfigData().cfg_supersimplif = (USint)i;
					}
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-S", 2) == 0)
			{
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][2], "%ld", &i) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%ld", &i) > 0)
#endif
				{
					if (i < 0)
						i = 0;
					//if (i > 3)
					//	i = 3;
					plt->getConfigData().cfg_topoSimp = (USint)i;
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-delta", 6) == 0)
			{
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][6], "%ld", &i) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%ld", &i) > 0)
#endif
				{
					plt->getConfigData().cfg_birthDateThresholdTopoSimp = (float)i;
				}
				nbarg --;
				trouve = 1;
			}


			if (strncmp (arguments[nbarg-1], "-fo", 3) == 0)
			{
			    outpcOk = true;
				outpcName = &arguments[nbarg-1][3];
				nbarg --;
				trouve = 1;
				if (strncmp (arguments[nbarg-1], "-fs", 3) == 0)
				{

#ifdef WIN32
					if (sscanf_s (&arguments[nbarg-1][3], "%f", &f) > 0)
#else
					if (sscanf (&arguments[nbarg-1][3], "%f", &f) > 0)
#endif
					{
						step_Custom= f;
					}
					nbarg --;
					trouve = 1;
				}
			}

			if (strncmp (arguments[nbarg-1], "-e", 2) == 0)
			{
				if (strncmp (arguments[nbarg-2], "-c", 2) == 0)
				{
					plt->getConfigData().addExternalModule (&arguments[nbarg-2][2], &arguments[nbarg-1][2]);
					nbarg -=2;
					trouve = 1;
				}
				else
				{
//					plt->getConfigData().addExternalModule (&arguments[nbarg-1][2]);
					nbarg --;
					trouve = 1;
				}
			}

			if (strncmp (arguments[nbarg-1], "-c", 2) == 0)
			{
				plt->getConfigData().addExternalModule (&arguments[nbarg-1][2]);
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-posx", 5) == 0)
			{
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][5], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][5], "%f", &f) > 0)
#endif
				{
					x = f;
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-posy", 5) == 0)
			{
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][5], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][5], "%f", &f) > 0)
#endif
				{
					y = f;
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-posz", 5) == 0)
			{
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][5], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][5], "%f", &f) > 0)
#endif
				{
					z = f;
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-begin", 2) == 0)
			{
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][6], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][6], "%f", &f) > 0)
#endif
				{
					beginTime = f;
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-h", 2) == 0)
			{
				usage ();
			}

		} while (trouve);


		float absoluteFinalAge;
		if (nbarg > 3)
		{
			string acro (arguments[1]);
			plt->setParamName(string("AMAPSimMod"), acro);

			acro.assign (arguments[2]);
			plt->setParamName(string("DigRMod"), acro);

			absoluteFinalAge = atof(arguments[3])+beginTime;
			plt->setHotStop(absoluteFinalAge);
			plt->getConfigData().simuTotalTime = absoluteFinalAge;
			nbarg -= 2;

			if (nbarg > 2)
			{
				plt->setSeed((USint)atoi(arguments[4]));
//				plt->setSeed(10);
			}
		}
		else
		{
			usage ();

		}

		if (nameOutput.size() == 0)
			nameOutput = "wholeplant";

		plt->setName(nameOutput);

		if(!step_LineTree)
			step_LineTree=absoluteFinalAge;
		if(!step_Natfx)
			step_Natfx=absoluteFinalAge;
		if(!step_AmapMod)
			step_AmapMod=absoluteFinalAge;
		if(!step_Glds)
			step_Glds=absoluteFinalAge;
		if(!step_Custom)
			step_Custom=absoluteFinalAge;
		if(outplOk)
		{
#ifndef WIN32
			outpMan->addOutputDevice("libOutputLineTreeAmapsim.so");
#else
			outpMan->addOutputDevice("OutputLineTreeAmapsim.dll");
#endif
			outpMan->create_and_schedule_output(plt,outpMan->getNumberOfOutputDevice()-1,nameOutput,step_LineTree,swap_bytes);
		}
		if(outpglOk)
		{
#ifndef WIN32
			outpMan->addOutputDevice("libOutputOpf.so");
#else
			outpMan->addOutputDevice("OutputOpf.dll");
#endif
			// copy header file from parameter directory to output directory
			string in (arguments[1]);
			if (in.find(".fpa") != 0)
			{
				in.resize(in.size()-4);
			}

			ifstream inf;
			ofstream ouf;
			if (in.compare (nameOutput) != 0)
			{
				ifstream in2;
				bool diff = false;
				in2.open((nameOutput+".headeropf").c_str());
				inf.open((in+".headeropf").c_str());
				if (   !inf.fail()
					&& !ouf.fail())
				{
					char str1[512], str2[512];
					while (!inf.eof())
					{
						inf.getline (str1, 512);
						in2.getline (str2, 512);
						if (strcmp (str1, str2) != 0)
						{
							diff = true;
							break;
						}
					}
				}
				inf.close();
				in2.close();

				if (diff)
				{
					ouf.open((nameOutput+".headeropf").c_str());
					inf.open((in+".headeropf").c_str());
					if (   !inf.fail()
						&& !ouf.fail())
					{
						char str[512];
						while (!inf.eof())
						{
							inf.getline (str, 512);
							ouf << str << endl;
						}
					}
					inf.close();
					ouf.close();
				}
			}

			outpMan->create_and_schedule_output(plt,outpMan->getNumberOfOutputDevice()-1,nameOutput,step_Glds);
		}
		if(outpmOk)
		{
#ifndef WIN32
			outpMan->addOutputDevice("libOutputMtgAmapsim.so");
#else
			outpMan->addOutputDevice("OutputMtgAmapsim.dll");
#endif
			outpMan->create_and_schedule_output(plt,outpMan->getNumberOfOutputDevice()-1,nameOutput,step_AmapMod);
		}
		if(outpcOk)
		{
			outpMan->addOutputDevice(outpcName);
			outpMan->create_and_schedule_output(plt,outpMan->getNumberOfOutputDevice()-1,nameOutput,step_Custom);
		}
		if(outpnfxOk)
		{
#ifndef WIN32
			outpMan->addOutputDevice("libOutputTreeNatfx.so");
#else
			outpMan->addOutputDevice("OutputLineTreeNatfx.dll");
#endif
			outpMan->create_and_schedule_output(plt,outpMan->getNumberOfOutputDevice()-1,nameOutput,step_Natfx);
		}


		//outpMan->addOutputDevice("OutputForestry.dll");
		//outpMan->create_and_schedule_output(&plt,outpMan->getNumberOfOutputDevice()-1,nameOutput,Scheduler::instance().getHotStop());

		plt->setHotStart(0);

		plt->init();

		if (!((AxisReference*)plt->getParamData("AMAPSimMod"))->isSet())
			usage();

		omgr->scanForExternalObservers(plt);

		position.set(x, y, z);
		plt->setPosition(position);
		plt->run(beginTime);

	}//end else


}



