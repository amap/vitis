/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "ParamFile.h"
#include "Voxel.h"
#include "externF.h"


#include <vector>

class FPlugin;



#include "defAMAP.h"

class FEventData: public EventData
{
public:
	FEventData (int action){this->action = action;};
	int Action (){return action;};
	void setAction (int a){action = a;};
private:
	int action; // 1:compute geometry; 2:read light
};

class FLightData : public PrintableObject
{
public :
	double v;
	std::string to_string ()
	{
		std::ostringstream oss;
		// �crire la valeur dans le flux
		oss << v ;
		// renvoyer une string
		return oss.str();
	};
};


struct F_EXPORT ParameterPlugin:public subscriber<messageAxeRef> {
	FPlugin * Fplugin_ptr;

	ParameterPlugin (Plant *p) {subscribe(p);};
	virtual ~ParameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct F_EXPORT DecompPlugin:public subscriber<messageVitisDecompAxeLevel> {
	FPlugin * Fplugin_ptr;

	DecompPlugin (Plant *p) {subscribe(p);};
	virtual ~DecompPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct F_EXPORT PlantPlugin:public subscriber<messageVitisPlant> {
	FPlugin * Fplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct F_EXPORT PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	FPlugin * Fplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

enum messageLightParameter {SigGetParameter};

// specialized signal class for light
class F_EXPORT lightParameterSignal:public ParameterSignal
{
public:
	float sigGetParameter (float & vv, Parameter *pp, VitisObject *oo);
};


class F_EXPORT FPlugin : public VProcess
{
public :
	FPlugin (const std::string& f, const void *p);
	virtual ~FPlugin(){;};
	void setPlant (Plant  *p) {plant = (PlantAMAP*)p;};
	Plant *getPlant () {return plant;};
	void process_event(EventData *msg);
	void setParameterPlugin (ParameterPlugin *p){l = p;};
	void setDecompPlugin (DecompPlugin *p){d = p;};

	float startTime;
	float livingThreshold;
	float ramificationThreshold;
	float cat1Attenuation;
	float cat2Attenuation;
	int cat1Threshold;
	int cat2Threshold;
	int lightningModel;

	float siteAttenuationCoefficient;
	float cloneAttenuationCoefficient;

	// MIR parameters
	float latitude;
	float clearnessIndex;
	int beginingDay, endingDay;
	float refLight;

	float voxelSize;

protected :
    bool initFParam (const std::string& f);
	void computeInterception (float top_extern);
	void computeInterceptionModelIsolated (float top_extern);
	void computeInterceptionModelCanopy (float top_extern);
	void computeInterceptionModelMIR (float top_extern);
	void computeInterceptionModelVoxel (float top_extern);
	void computeUpperPoint(Vector3 *v);
	float computeCenterPoint (Vector3 *v);
	void affectLight (int id, float light);
	BrancAMAP* findBranc(int id);
	float computeRefLight();
	float extraTerrestrialDaily (int doy);
	double SunSetHourAngle (float declination);

	void updateInterceptedLightMIR (const std::string& f);
	void updateInterceptedLightVoxel ();

	void writeOPF(const std::string &finame);

	PlantAMAP *plant;
	BrancAMAP *curBranc;

	ParameterPlugin *l;
	DecompPlugin *d;
	PlantPlugin *p;
	PlantDeletePlugin *pp;

//	VoxelSpace *voxelSpace;
	void computePlantMap (float top_extern);
};

extern "C" F_EXPORT  FPlugin * StartPlugin (const std::string& f, const void *p)
{
	std::cout << "loading AmapSim FPlugin module with parameter " << f << std::endl;

	return new FPlugin (f, p) ;
}
