/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "PlantAMAP.h"
#include "ObserverManager.h"

#ifdef WIN32
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "PlantAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "GeomBrancAMAP.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "GeomElemCone.h"
#include "externF.h"
#include "F.h"
#define STATIC_LIB
#include "Output.h"
#include "lightThroughVoxels.h"
#include "GldsFormat.h"
#undef STATIC_LIB


void ParameterPlugin::on_notify (const state& st, paramMsg *data)
{
    //Cast message parameters
    paramAxisRef * p = (paramAxisRef *)data;

    //Retrieve branch
    BrancAMAP * b = (BrancAMAP *)p->b;

	if (p->numVar == lnginit)
	{
		if (b->phyAgeInit >= 1000)
		{
			p->var *= pow(Fplugin_ptr->siteAttenuationCoefficient,1.5);
			p->var *= pow(Fplugin_ptr->cloneAttenuationCoefficient,1.5);
//			p->var *= pow(Fplugin_ptr->siteAttenuationCoefficient,1);
//			p->var *= pow(Fplugin_ptr->cloneAttenuationCoefficient,1);
		}
		else
		{
			p->var *= pow(Fplugin_ptr->siteAttenuationCoefficient,1);
			p->var *= pow(Fplugin_ptr->cloneAttenuationCoefficient,1);
		}
	}
	else if (   p->numVar == diaminit
            && b->phyAgeInit >= 1000)
	{
		p->var *= pow(Fplugin_ptr->siteAttenuationCoefficient,1.5);
		p->var *= pow(Fplugin_ptr->cloneAttenuationCoefficient,1.5);
//			p->var *= pow(Fplugin_ptr->siteAttenuationCoefficient,1);
//			p->var *= pow(Fplugin_ptr->cloneAttenuationCoefficient,1);
	}


	FLightData *light = (FLightData*)b->getAdditionalData("light");
	if (!light)
		return;

	float top = Scheduler::instance()->getTopClock();

	// entn test number
//	if (   p->numVar == rtprefn1
//	    || p->numVar == rtneopn1
//	    || p->numVar == rtneon1)
	if (   p->numVar == rtneon1
	    || p->numVar == rtprefn1) // it is a choice to include (or not) the preform zone into the model
	{
	    if (b->getCurrentHierarc()->ordre == 0)
            p->var *= Fplugin_ptr->cloneAttenuationCoefficient;
        else
            p->var *= pow(Fplugin_ptr->cloneAttenuationCoefficient,.3);
		p->var *= Fplugin_ptr->siteAttenuationCoefficient;
//		p->var *= pow(Fplugin_ptr->siteAttenuationCoefficient,4);
//		p->var *= pow(Fplugin_ptr->cloneAttenuationCoefficient,4);

		if (   light->v == 1
		    || top < Fplugin_ptr->startTime)
			return;

		float d1, d2, v1, v2, coef;
		int cat;
		if (b->phyAgeInit < Fplugin_ptr->cat1Threshold)
			cat = 1;
		else if ((b->phyAgeInit < Fplugin_ptr->cat2Threshold))
			cat = 2;
		else
			cat = 3;

		if (light->v < Fplugin_ptr->livingThreshold)
		{
			v1 = 0;
			v2 = 0;
			d2 = 1;
			d1 = 0;
		}else if (light->v < Fplugin_ptr->ramificationThreshold)
		{
			d1 = Fplugin_ptr->livingThreshold;
			d2 = Fplugin_ptr->ramificationThreshold;
			switch (cat)
			{
			case 1 :
				v1 = 0;
				v2 = Fplugin_ptr->cat1Attenuation;
				break;
			case 2 :
				v1 = 0;
				v2 = Fplugin_ptr->cat2Attenuation;
				break;
			case 3 :
				v1 = 0;
				v2 = 1;
				break;
			}
		}else
		{
			d1 = Fplugin_ptr->ramificationThreshold;
			d2 = 1;
			switch (cat)
			{
			case 1 :
				v1 = Fplugin_ptr->cat1Attenuation;
				v2 = 1;
				break;
			case 2 :
				v1 = Fplugin_ptr->cat2Attenuation;
				v2 = 1;
				break;
			case 3 :
				v1 = 1;
				v2 = 1;
				break;
			}
		}
		coef = v1 + (v2 - v1) * (1. - (d2-light->v)/(d2 - d1));
		p->var *= coef;
	}


// CAUTION this set of control is coded
//         assuning lateral production order is branch-leaf-reiteration
//         and both Z1 and Z2 are branched

	// initial branched first state proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpbinitret0
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpbinitret0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret0)
	{
		if (   light->v < Fplugin_ptr->ramificationThreshold
		    && top >= Fplugin_ptr->startTime)
			p->var = 1;
		else
		{
//			p->var = 1 - (1-p->var) * sqrt(Fplugin_ptr->cloneAttenuationCoefficient * Fplugin_ptr->siteAttenuationCoefficient);
		}
	}
	// initial branched first state proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpbinitret1
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpbinitret1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret1)
	{
		if (   light->v < Fplugin_ptr->ramificationThreshold
		    && top >= Fplugin_ptr->startTime)
			p->var = 0;
		else
		{
//			p->var *= sqrt(Fplugin_ptr->siteAttenuationCoefficient);
//			p->var *= sqrt(Fplugin_ptr->cloneAttenuationCoefficient);
		}
	}
	// remaining not branched proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_st0
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_st0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st0)
	{
		if (   light->v < Fplugin_ptr->ramificationThreshold
		    && top >= Fplugin_ptr->startTime)
			p->var = 1;
		else
		{
//			if (b->phyAgeInit < Fplugin_ptr->cat1Threshold)
//			p->var = 1 - (1-p->var) * sqrt(Fplugin_ptr->cloneAttenuationCoefficient * Fplugin_ptr->siteAttenuationCoefficient);
			p->var = 1 - (1-p->var) * pow(Fplugin_ptr->cloneAttenuationCoefficient * Fplugin_ptr->siteAttenuationCoefficient, .25);
		}
	}
	// remaining first state proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_st1
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_st1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st1)
	{
		if (   light->v < Fplugin_ptr->ramificationThreshold
		    && top >= Fplugin_ptr->startTime)
			p->var = 0;
		else
		{
//			if (b->phyAgeInit < Fplugin_ptr->cat1Threshold)
			{
//			p->var *= sqrt(Fplugin_ptr->siteAttenuationCoefficient);
//			p->var *= sqrt(Fplugin_ptr->cloneAttenuationCoefficient);
			p->var *= pow(Fplugin_ptr->siteAttenuationCoefficient, .25);
			p->var *= pow(Fplugin_ptr->cloneAttenuationCoefficient, .25);
			}
		}
	}
	// remaining second state proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_st2
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st2
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_st2
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st2)
	{
		if (   light->v < Fplugin_ptr->ramificationThreshold
		    && top >= Fplugin_ptr->startTime)
			p->var = 0;
	}
	// transition not branched -> first state proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_0_1
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_0_1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_0_1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_0_1)
	{
		if (   light->v < Fplugin_ptr->ramificationThreshold
		    && top >= Fplugin_ptr->startTime)
			p->var = 0;
		else
		{
//			if (b->phyAgeInit < Fplugin_ptr->cat1Threshold)
			{
//			p->var *= sqrt(Fplugin_ptr->siteAttenuationCoefficient);
//			p->var *= sqrt(Fplugin_ptr->cloneAttenuationCoefficient);
			p->var *= pow(Fplugin_ptr->siteAttenuationCoefficient, .25);
			p->var *= pow(Fplugin_ptr->cloneAttenuationCoefficient, .25);
			}
		}
	}
	// transition first state -> not branched proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_1_0
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_1_0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_1_0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_1_0)
	{
		if (   light->v < Fplugin_ptr->ramificationThreshold
		    && top >= Fplugin_ptr->startTime)
			p->var = 1;
		else
		{
//			p->var *= 1/Fplugin_ptr->siteAttenuationCoefficient;
//			p->var *= 1/Fplugin_ptr->cloneAttenuationCoefficient;
//			if (b->phyAgeInit < Fplugin_ptr->cat1Threshold)
			{
//			p->var = 1 - (1-p->var) * sqrt(Fplugin_ptr->cloneAttenuationCoefficient * Fplugin_ptr->siteAttenuationCoefficient);
			p->var = 1 - (1-p->var) * pow(Fplugin_ptr->cloneAttenuationCoefficient * Fplugin_ptr->siteAttenuationCoefficient, .25);
			}
		}
	}
	// transition second state -> not branched proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_2_0
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_2_0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_2_0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_2_0)
	{
		if (   light->v < Fplugin_ptr->ramificationThreshold
		    && top >= Fplugin_ptr->startTime)
			p->var = 1;
	}
	// living proba
	else if (p->numVar == rtpbvie)
	{
		if (   light->v > 0
		    && light->v < Fplugin_ptr->livingThreshold
		    && top >= Fplugin_ptr->startTime
			&& b->getBud()->ordrez > 0)
			p->var = 0;
		else
		{
//			p->var *= Fplugin_ptr->siteAttenuationCoefficient;
//			p->var *= Fplugin_ptr->cloneAttenuationCoefficient;
		}
	}
}

/////////////////////////////////////////////////////
//
// This method get called for each new topology creation
//
// It stores UT global parameters
//
/////////////////////////////////////////////////////
void DecompPlugin::on_notify (const state& s, paramMsg * data)
{
    //Cast msg parameters
    paramDecompAxeLevel *p = (paramDecompAxeLevel *)data;

    //Retrieve branch
    DecompAxeLevel *I = p->d;

	if (s == SigDelDecompAxeLevel)
	{
		FLightData *d;
		if ((d = (FLightData*)I->getAdditionalData("light")))
		{
			delete d;
			I->removeAdditionalData("light");
		}
		return;
	}

	//Swith on axe object level (0 stand for branch, etc..)
	if (I->getLevel() == 0)
	{
		FLightData *L = new FLightData;
		L->v = 1;
		I->addAdditionalData ("light", L, VITISGLDSPRINTABLE);
	}

}

/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{

    paramPlant * d = (paramPlant*)data; //Retrieve msg params

	Fplugin_ptr->setPlant (d->p);
	if (Fplugin_ptr->getPlant()->getConfigData().cfg_topoSimp > 1)
        Fplugin_ptr->getPlant()->getConfigData().cfg_topoSimp = 1;

	FEventData *e = new FEventData(1);
	if (Fplugin_ptr->lightningModel == 2)
        Scheduler::instance()->create_process(Fplugin_ptr,e,1,103);
    else
        Scheduler::instance()->create_process(Fplugin_ptr,e,1,100);
//	Scheduler::instance()->create_process(Fplugin_ptr,e,Fplugin_ptr->startTime,3);
//	Scheduler::instance()->create_process(Fplugin_ptr,NULL,1,2);
//	Scheduler::instance()->create_process(Fplugin_ptr,(EventData*)d->p,Scheduler::instance().getHotStop(),1);
}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{

	Fplugin_ptr->setPlant (NULL);
}

void FPlugin::process_event(EventData *msg)
{
    if (plant == NULL)
        return;

	FEventData *e = (FEventData*)msg;

	Scheduler *s = Scheduler::instance();

	/* sets the next external eventData */
	float top_extern = s->getTopClock();

    if (lightningModel == 2)
    {
        if (e->Action() == 1)
        {
            plant->computeGeometry();

            if (top_extern >= startTime)
                writeOPF (plant->getName());

            e->setAction (2);
            s->signal_event(this->getPid(),e,top_extern,101);
        }
        else if (e->Action() == 2)
        {
            if (top_extern >= startTime)
            {
                cout << "compute interception at time "<< top_extern <<" for "<<plant->getName()<<endl;
                /* computes light interception for the next cycle */
                computeInterception (top_extern);
                //writeOPF (plant->getName());

    //			plant->getGeometry().clear();
            }

            e->setAction (1);
            if (top_extern+1 < s->getHotStop())
            {
                s->signal_event(this->getPid(),e,top_extern+1,103);
            }
        }
    }
    else
    {
        if (e->Action() == 1)
        {
            if (top_extern < startTime)
            {
                plant->computeGeometry();
                if (top_extern+1 < startTime)
                    s->signal_event(this->getPid(),e,top_extern+1,100);
                else
                {
                    e->setAction (2);
                    s->signal_event(this->getPid(),e,top_extern+1,103);
                }
            }
            else
            {
    cout <<endl<< "Voxel space cleaning at " << top_extern << " for "<<plant->getName()<<endl;
                // clean voxel space at the end of the step
                VoxelSpace::instance()->resetContent (plant);
//                plant->getGeometry().clear();

                // register for next lightingmap step
                e->setAction (2);
                s->signal_event(this->getPid(),e,top_extern+1,103);

//BrancAMAP *b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
//b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
//FLightData *light=0;
//while (b)
//{
//    if (b->nature!=SimpleImmediateOrgan)
//    if ((light = (FLightData*)b->getAdditionalData("light")) != NULL)
//    {
//       if (light->v > 0.5)
//        {
//        cout<<light->v<<endl;
//        }
//    }
//    b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
//}
            }
        }
        else if (e->Action() == 2)
        {

    cout <<endl<< "plant mapping at " << top_extern<<" for "<<plant->getName()<<endl;
            int cfgsav = plant->getConfigData().cfg_geoSimp;
            if (cfgsav != 0)
            {
                plant->getConfigData().cfg_geoSimp = 0;
                plant->getGeometry().HasToBeComputed(true);
            }
            plant->computeGeometry();
            if (cfgsav != 0)
            {
                plant->getGeometry().HasToBeComputed(true);
                plant->getConfigData().cfg_geoSimp = cfgsav;
            }

//            if (top_extern >= startTime)
//                writeOPF (plant->getName());

    //voxelSpace->displayContent();
            computePlantMap (top_extern);
    //voxelSpace->displayContent();

            // register for timidity computing
            e->setAction (3);
            s->signal_event(this->getPid(),e,top_extern,101);
//BrancAMAP *b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
//b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
//FLightData *light=0;
//while (b)
//{
//    if (b->nature!=SimpleImmediateOrgan)
//    if ((light = (FLightData*)b->getAdditionalData("light")) != NULL)
//    {
//        if (light->v > 0.5)
//        {
//        cout<<light->v<<endl;
//        }
//    }
//    b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
//}
        }
        else if (e->Action() == 3)
        {

    cout <<endl<< "lightning at " << top_extern<< " for "<<plant->getName()<<endl;
            computeInterception (top_extern);

    //cout << "end timidity at " << top_extern <<endl;

            // register for cleaning at the end of the step
            e->setAction (1);
            s->signal_event(this->getPid(),e,top_extern,100);
//BrancAMAP *b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
//b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
//FLightData *light=0;
//while (b)
//{
//    if (b->nature!=SimpleImmediateOrgan)
//    if ((light = (FLightData*)b->getAdditionalData("light")) != NULL)
//    {
//       if (light->v > 0.5)
//        {
//        cout<<light->v<<endl;
//        }
//    }
//    b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
//}
        }
    }
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
FPlugin::FPlugin(const std::string& f, const void *plant) {

    if (!initFParam (f))
    {
		cout << "Failed reading light parameter file " << f << endl;
		cout << "File sould look like this :" << endl<<endl;
		cout << "# WARNING"<<endl;
		cout << "# The syntax for this parameter file is not even"<<endl<<"# Try to keep the \"SingleValueParameter\" keyword as it is"<<endl;
		cout << "# just adjust the numerical values"<<endl<<endl;
		cout << "# start time for F function"<<endl<<"startTime SingleValueParameter 3"<<endl<<endl;;
		cout << "# maximum phyage value for category one axis"<<endl<<"cat1Threshold SingleValueParameter 60"<<endl<<endl;;
		cout << "# maximum phyage value for category two axis"<<endl<<"cat2Threshold SingleValueParameter 80"<<endl<<endl;
		cout << "# minimum light for life"<<endl<<"livingThreshold SingleValueParameter 0.1"<<endl<<endl;
		cout << "# minimum light for ramification"<<endl<<"ramificationThreshold SingleValueParameter 0.4"<<endl<<endl;
		cout << "# category 1 axis sensibility for light at ramification threshold"<<endl<<"cat1Attenuation SingleValueParameter 0.7"<<endl<<endl;
		cout << "# category 2 axis sensibility for light at ramification threshold"<<endl<<"cat2Attenuation SingleValueParameter 0.9"<<endl<<endl;
		cout << "# parameter attenuation due to site within the same clone"<<endl<<"siteAttenuationCoefficient SingleValueParameter 1"<<endl<<endl;
		cout << "# parameter attenuation between similar clones"<<endl<<"cloneAttenuationCoefficient SingleValueParameter 1"<<endl<<endl;
		cout << "# lightning model type (0 : isolated Tree, 1 : canopy tree, 2 : MIR)"<<endl<<"lightningModel SingleValueParameter 1"<<endl;
 		cout << endl<< "**** Failed reading light parameter file " << f << " ****" << endl;
		return;
	}
	this->plant = (PlantAMAP*)plant;
    //child -> parent binding PTR
	l = new ParameterPlugin (this->plant);
    l->Fplugin_ptr = this;
	d = new DecompPlugin (this->plant);
    d->Fplugin_ptr = this;
	p = new PlantPlugin (this->plant);
    p->Fplugin_ptr = this;
	pp = new PlantDeletePlugin (this->plant);
    pp->Fplugin_ptr = this;

}

bool FPlugin::initFParam (const std::string& f) {
	lightningModel = 2;
	ParamSet * param_H = new ParamSet(f);

	if (!param_H->Exists())
		return false;

	startTime = 1;
	if (param_H->getParameter("startTime"))
		startTime = param_H->getParameter("startTime")->value();
	else
	{
		cout << "Failed finding startTime parameter in file " << f << endl;
		cout << "Assuming startTime=1" << endl;
	}

	if (param_H->getParameter("cat1Threshold"))
		cat1Threshold = param_H->getParameter("cat1Threshold")->value();
	else
	{
		cout << "Failed finding cat1Threshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cat2Threshold"))
		cat2Threshold = param_H->getParameter("cat2Threshold")->value();
	else
	{
		cout << "Failed finding cat2Threshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("livingThreshold"))
		livingThreshold = param_H->getParameter("livingThreshold")->value();
	else
	{
		cout << "Failed finding livingThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("ramificationThreshold"))
		ramificationThreshold = param_H->getParameter("ramificationThreshold")->value();
	else
	{
		cout << "Failed finding ramificationThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cat1Attenuation"))
		cat1Attenuation = param_H->getParameter("cat1Attenuation")->value();
	else
	{
		cout << "Failed finding cat1Attenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cat2Attenuation"))
		cat2Attenuation = param_H->getParameter("cat2Attenuation")->value();
	else
	{
		cout << "Failed finding cat2Attenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("siteAttenuationCoefficient"))
		siteAttenuationCoefficient = param_H->getParameter("siteAttenuationCoefficient")->value();
	else
	{
		cout << "Failed finding siteAttenuationCoefficient parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cloneAttenuationCoefficient"))
		cloneAttenuationCoefficient = param_H->getParameter("cloneAttenuationCoefficient")->value();
	else
	{
		cout << "Failed finding cloneAttenuationCoefficient parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("lightningModel"))
		lightningModel = param_H->getParameter("lightningModel")->value();
	else
	{
		cout << "Failed finding lightningModel parameter in file " << f << endl;
		cout << "Assuming Archimed model" << endl;
	}

	switch (lightningModel)
	{
	case 0 :
		cout << "isolated tree simulation" << endl;
		break;
	case 1 :
		cout << "canopy tree simulation" << endl;
		break;
	case 2 :
		cout << "MIR light simulation" << endl;
		break;
	case 3 :
		cout << "voxel light simulation" << endl;
		break;
	}

	if (lightningModel == 3)
	{
        if (param_H->getParameter("voxelSize"))
            VoxelSpace::instance()->VoxelSize(param_H->getParameter("voxelSize")->value());
        else
        {
            cout << "Failed finding voxelSize parameter in file " << f << endl;
            VoxelSpace::instance()->VoxelSize(1);
            return false;
        }
	}

	if (lightningModel == 2)
	{
		if (param_H->getParameter("clearnessIndex"))
			clearnessIndex = param_H->getParameter("clearnessIndex")->value();
		else
		{
			cout << "Failed finding clearnessIndex parameter in file " << f << endl;
			return false;
		}
		if (param_H->getParameter("latitude"))
		{
			latitude = param_H->getParameter("latitude")->value();
			latitude *= M_PI / 180.;
		}
		else
		{
			cout << "Failed finding latitude parameter in file " << f << endl;
			return false;
		}
		if (param_H->getParameter("beginingDay"))
			beginingDay = param_H->getParameter("beginingDay")->value();
		else
		{
			cout << "Failed finding beginingDay parameter in file " << f << endl;
			return false;
		}
		if (param_H->getParameter("endingDay"))
			endingDay = param_H->getParameter("endingDay")->value();
		else
		{
			cout << "Failed finding endingDay parameter in file " << f << endl;
			return false;
		}

		refLight = computeRefLight ();
		cout << "refLight "<<refLight<<endl;
	}

	return true;
}

void FPlugin::computeInterception (float top_extern){
	switch (lightningModel)
	{
	case 0:
		computeInterceptionModelIsolated(top_extern);
		break;
	case 1:
		computeInterceptionModelCanopy(top_extern);
		break;
	case 2:
		computeInterceptionModelMIR(top_extern);
		break;
	case 3:
		computeInterceptionModelVoxel(top_extern);
		break;
	}
}

void FPlugin::computeInterceptionModelIsolated (float top_extern){
	FLightData *L;
	GeomBranc *geom;
	GeomElem *g;
	Branc *b;
	float surfLeaf, sphereRadius=0, v;
	Vector3 refPoint, curPoint;

	// isolated tree : compute center and sphere diameter
	sphereRadius = computeCenterPoint (&refPoint);

	for (int br=0; br<plant->getGeometry().getGeomBrancStack().size(); br++)
	{
		geom = plant->getGeometry().getGeomBrancStack().at(br);
		b = geom->getBranc();

		if (b->getAdditionalData ("light") == 0)
			continue;

		g = geom->getElementsGeo().at(geom->getElementsGeo().size()-1);

		curPoint.set(g->getMatrix().getMainVector());
		curPoint *= g->getLength();
		curPoint +=(g->getMatrix().getTranslation());

		surfLeaf = 0;
		Hierarc *h = b->HierarcPtrs()[0];
		for (int i=0; i<h->borneNumber; i++){
			if (h->getBorne(i)->getAdditionalData ("light") == 0)
			{
				surfLeaf += 1;
			}
		}

		L = (FLightData*)b->getAdditionalData ("light");

		curPoint -= refPoint;
		v = curPoint.normalize();
		L->v = v / sphereRadius;

	}
}

void FPlugin::computeInterceptionModelCanopy (float top_extern){
	FLightData *L;
	GeomBranc *geom;
	GeomElem *g;
	Branc *b;
	BrancAMAP *trunk;
	Branc *borne;
	float surfLeaf;
	Vector3 refPoint, curPoint;

	// find the upper point
	computeUpperPoint(&refPoint);

	for (int br=0; br<plant->getGeometry().getGeomBrancStack().size(); br++)
	{
		geom = plant->getGeometry().getGeomBrancStack().at(br);
		b = geom->getBranc();

		if (b->getAdditionalData ("light") == 0)
			continue;

		g = geom->getElementsGeo().at(geom->getElementsGeo().size()-1);

		curPoint.set(g->getMatrix().getMainVector());
		curPoint *= g->getLength();
		curPoint +=(g->getMatrix().getTranslation());

		surfLeaf = 0;
		Hierarc *h = b->HierarcPtrs()[0];
		for (int i=0; i<h->borneNumber; i++){
			if (h->getBorne(i)->getAdditionalData ("light") == 0)
			{
				surfLeaf += 1;
			}
		}

		L = (FLightData*)b->getAdditionalData ("light");

		if (curPoint[2] < refPoint[2]/2.)
			L->v = 0;
		else
			L->v = 2. * (curPoint[2] - refPoint[2]/2.) / refPoint[2];
	}
}

void FPlugin::computeInterceptionModelMIR(float top_extern)
{
	updateInterceptedLightMIR (string(plant->getName()+".opf"));
}

void FPlugin::computeInterceptionModelVoxel(float top_extern)
{
	updateInterceptedLightVoxel ();
}

// assuming upper point is at the top of the trunk
void FPlugin::computeUpperPoint(Vector3 *v)
{
	GeomBranc *geom;
	GeomElem *g;
	BrancAMAP *trunk;

	(*v)[2] = 0;
	for (int br=0; br<plant->getGeometry().getGeomBrancStack().size(); br++)
	{
		geom = plant->getGeometry().getGeomBrancStack().at(br);
		trunk = (BrancAMAP*)geom->getBranc();
		if (trunk->getAdditionalData("light") == 0)
			continue;

		if (trunk->phyAgeInit == 1)
		{
			g = geom->getElementsGeo().at(geom->getElementsGeo().size()-1);
			(*v).set(g->getMatrix().getMainVector());
			(*v) *= g->getLength();
			(*v) +=(g->getMatrix().getTranslation());
			break;
		}
	}
}

float FPlugin::computeCenterPoint (Vector3 *v)
{
	GeomBranc *geom;
	GeomElem *g;
	Branc *b;
	Vector3 curPoint;
	float f, sphereRadius=0;

	// assume center point is at half tree height
	computeUpperPoint (v);
	(*v) *= 0.5;

	for (int br=0; br<plant->getGeometry().getGeomBrancStack().size(); br++)
	{
		geom = plant->getGeometry().getGeomBrancStack().at(br);
		b = geom->getBranc();

		if (b->getAdditionalData ("light") == 0)
			continue;

		geom = plant->getGeometry().getGeomBrancStack().at(br);
		g = geom->getElementsGeo().at(geom->getElementsGeo().size()-1);

		curPoint.set(g->getMatrix().getMainVector());
		curPoint *= g->getLength();
		curPoint +=(g->getMatrix().getTranslation());
		curPoint -= (*v);
		f = curPoint.normalize();
		if (f > sphereRadius)
			sphereRadius = f;
	}

	return sphereRadius;
}

void FPlugin::updateInterceptedLightMIR (const std::string& f)
{
	ifstream fic (f.c_str(), ios_base::in);
	char line[50000];
	char *substr;
	int id, idBr, idBearer[100], curOrder=0;
	float l;
	BrancAMAP *bearer, *brLight[100];

	if (!fic.good())
	{
		perror (f.c_str());
		return;
	}

	BrancAMAP *b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
	FLightData *light;
	while (b)
	{
		if ((light = (FLightData*)b->getAdditionalData("light")) != NULL)
			light->v = 0;
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}

	curBranc = NULL;
	bool newlight = false;
	while (!fic.eof())
	{
		fic.getline (line, 50000);

		if ((substr=strstr (line, "<light>")) != NULL)
		{
			newlight = true;
		}
		else if (   newlight
				 && (substr=strstr (line, "topoId")) != NULL)
		{
			newlight = false;
			sscanf (&substr[7], "%d", &id);
			brLight[curOrder-1] = findBranc(id);
			curBranc = brLight[curOrder-1];
		}
		else if ((substr=strstr (line, "<branch")) != NULL)
		{
			brLight[curOrder] = NULL;
			curOrder++;
		}
		else if ((substr=strstr (line, "</branch")) != NULL)
		{
			brLight[curOrder] = NULL;
			int i = curOrder;
			while (brLight[i] == NULL)
				i--;
			curBranc = brLight[i];
			curOrder--;
		}
		else if ((substr=strstr (line, "MJ_m2>")) != NULL)
		{
			sscanf (&substr[6], "%f", &l);
			if (l > refLight)
				l = refLight;

			if ((light = (FLightData*)curBranc->getAdditionalData("light")) != NULL)
			{
				if (light->v < l/refLight)
					light->v = l/refLight;
			}
		}
	}

	// update light for new born buds (with no internodes) and for buds bearing leaves
	b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
	while (b)
	{
		if (b->getTestNumberWithinBranch() == 0)
		{
			bearer = (BrancAMAP*)b->getCurrentHierarc()->getBearer()->getBranc();
			if ((light = (FLightData*)bearer->getAdditionalData("light")) != NULL)
			{
				l = light->v;
				light = (FLightData*)(b->getAdditionalData("light"));
				light->v = l;
			}
		}
		else if (b->nature == SimpleImmediateOrgan)
        {
			if ((light = (FLightData*)b->getAdditionalData("light")) != NULL)
			{
				l = light->v;
                bearer = (BrancAMAP*)b->getCurrentHierarc()->getBearer()->getBranc();
				light = (FLightData*)bearer->getAdditionalData("light");
				if (light->v < l)
                    light->v = l;
			}
        }
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}

}

void FPlugin::updateInterceptedLightVoxel ()
{
	GeomElemCone *g;
	float l;
	BrancAMAP *bearer;
	int i,j,k;
	Voxel *v;

	BrancAMAP *b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);

	FLightData *light;
	LightData *d;
	while (b)
	{
		if ((light = (FLightData*)b->getAdditionalData("light")) != NULL)
			light->v = 0;
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}

    int imin=0, imax=VoxelSpace::instance()->XSize(), jmin=0, jmax=VoxelSpace::instance()->YSize();
    float xmin = plant->getGeometry().getMin().x() + plant->getPosition().x();
    float ymin = plant->getGeometry().getMin().y() + plant->getPosition().y();
    float xmax = plant->getGeometry().getMax().x() + plant->getPosition().x();
    float ymax = plant->getGeometry().getMax().y() + plant->getPosition().y();
    VoxelSpace::instance()->findVoxel(xmin, ymin, 0, &imin, &jmin, &k);
    VoxelSpace::instance()->findVoxel(xmax, ymax, 0, &imax, &jmax, &k);

    if (imin < 0)
        imin = 0;
    if (jmin < 0)
        jmin = 0;
    if (imax > VoxelSpace::instance()->XSize())
        imax=VoxelSpace::instance()->XSize();
    if (jmax > VoxelSpace::instance()->YSize())
        jmax=VoxelSpace::instance()->YSize();

	for (i=imin; i<imax; i++)
	for (j=jmin; j<jmax; j++)
	for (k=0; k<VoxelSpace::instance()->ZSize(); k++)
	{
        v = VoxelSpace::instance()->addrAt (i, j, k);
        d = (LightData*)v->getVoxelData("light");
        l = d->light;

        for (vector<GeomElemCone*>::iterator it=v->content.begin(); it != v->content.end(); it++)
		{
			g = *it;

			if (g->getPtrBrc()->getBranc()->getPlant() != plant)
                continue;

			b = (BrancAMAP*)g->getPtrBrc()->getBranc();
			if (b->nature == SimpleImmediateOrgan)
                b = b->getBearer();

			if ((light = (FLightData*)b->getAdditionalData("light")) != NULL)
			{
                if (light->v < l)
                    light->v = l;
			}
		}
	}

	// update light for new born buds (with no internodes)
	b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
	while (b)
	{
		if (b->getTestNumberWithinBranch() == 0)
		{
			bearer = (BrancAMAP*)b->getCurrentHierarc()->getBearer()->getBranc();
			if ((light = (FLightData*)bearer->getAdditionalData("light")) != NULL)
			{
				l = light->v;
				light = (FLightData*)(b->getAdditionalData("light"));
				if (light->v < l)
                    light->v = l;
			}
		}
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}

}

BrancAMAP* FPlugin::findBranc(int id)
{
	BrancAMAP* b;
	b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
	while (b)
	{
		if (b->getId() == id)
		{
			return b;
		}
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}
}

void FPlugin::affectLight (int id, float l)
{

	DecompAxeLevel *d=NULL;

	if (curBranc != NULL)
		d = curBranc->getDecompAxeLevel (id);
	if (d == NULL)
		d = plant->getDecompAxeLevel (id);

	FLightData *light;
	BrancAMAP *b;
	if ((light = (FLightData*)d->getAdditionalData("light")) != NULL)
	{
		curBranc = (BrancAMAP*)d;
		if (light->v < l)
			light->v = l;
	}
	else
	{
		b = (BrancAMAP*)d->getParentOfLevel();
		b = (BrancAMAP*)b->getCurrentHierarc()->getBearer()->getBranc();
		if ((light = (FLightData*)b->getAdditionalData("light")) != NULL)
		{
			curBranc = b;
			if (light->v < l)
				light->v = l;
		}
	}
}

float FPlugin::extraTerrestrialDaily (int doy)
{
	float solarCste= 0.0820f;	// in MJ m-2 min-1 (<=> SolarCste= 1367 in J m-2 s-1)
	double doyAngle=  2 * M_PI * doy/365.;
	double declination= 0.409 * sin(doyAngle-1.39); // !!!
	double sun_earth= 1 + (0.033 * cos(doyAngle));		// inverse relative distance
	double sunSetHourAngle= SunSetHourAngle ((float) declination);

	double extra_rad;
	extra_rad  = sunSetHourAngle * sin(latitude) * sin(declination);
	extra_rad += cos(latitude)*cos(declination)*sin(sunSetHourAngle);
	extra_rad *= (24*60*solarCste*sun_earth/M_PI);

	return (float) extra_rad;
}

double FPlugin::SunSetHourAngle (float declination)
{
	double sunSetHourAngle= -tan(latitude)*tan(declination);
	if (sunSetHourAngle < -1)
		sunSetHourAngle = -1;
	if (sunSetHourAngle > 1)
		sunSetHourAngle = 1;
	//sunSetHourAngle= max(-1, sunSetHourAngle);
	//sunSetHourAngle= min( 1, sunSetHourAngle);
	sunSetHourAngle= acos(sunSetHourAngle);

	return sunSetHourAngle;
}

float FPlugin::computeRefLight()
{
	float refLight=0;

	for (int i=beginingDay; i<endingDay; i++)
	{
		refLight += extraTerrestrialDaily (i);
	}

	return refLight * clearnessIndex;
}

void FPlugin::writeOPF(const std::string &finame)
{
	int i;

	std::stringstream sout;
	sout << finame;
	std::string fname (sout.str());

	GldsFormat *outFormat= new GldsFormat(fname);

	outFormat->initFormat(&(plant->getGeometry()));

	std::cout<<"F Output Geometry opf to : "  << fname << ".opf" << std::endl;

	if (plant->getGeometry().getGeomBrancStack().size())
	{
		outFormat->printData(plant->getGeometry().getGeomBrancStack().at(0));
	}
	else
	{
		std::cout<<"WARNING : -------> Empty Geometry"<<std::endl;
	}

	((GldsFormat*)outFormat)->closeFormat();

	delete outFormat;
}

void FPlugin::computePlantMap (float top_extern){

	float voxelSize = VoxelSpace::instance()->VoxelSize();

	// compute bounding box dimensions
	Vector3 min = plant->getGeometry().getMin();
	Vector3 max = plant->getGeometry().getMax();
	Vector3 pos = plant->getPosition();

	int x = ceil (max[0] / voxelSize + 0.5) - floor (min[0] / voxelSize - 0.5);
	int y = ceil (max[1] / voxelSize + 0.5) - floor (min[1] / voxelSize - 0.5);
	int z = ceil (max[2] / voxelSize + 0.5) - floor (min[2] / voxelSize - 0.5);
	int indXOrigin = floor ((pos[0]+min[0]) / voxelSize - 0.5);
	int indYOrigin = floor ((pos[1]+min[1]) / voxelSize - 0.5);
	int indZOrigin = floor ((pos[2]+min[2]) / voxelSize - 0.5);
	float xOrig = indXOrigin * voxelSize;
	float yOrig = indYOrigin * voxelSize;
	float zOrig = indZOrigin * voxelSize;

	VoxelSpace::instance()->adjust (xOrig, yOrig, x, y, z);

	int zMax = 0;
	zMax = 0;
	int zMin = VoxelSpace::instance()->ZSize();
	int ib, jb, kb;
	int ie, je, ke;
	GeomElemCone *e;
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	vector<Voxel*> vv;
    float xorig, yorig, zorig;
    float xend, yend, zend;

	for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
		GeomBranc *g = *b;

		BrancAMAP *br = (BrancAMAP*)g->getBranc();

		vector<GeomElem *> ges = g->getElementsGeo ();

		for (vector<GeomElem *>::iterator gj=ges.begin(); gj!=ges.end(); gj++)
		{
			vv.clear();
			e = (GeomElemCone*)*gj;
            xorig = e->getMatrix().getTranslation().x() + pos[0];
            yorig = e->getMatrix().getTranslation().y() + pos[1];
            zorig = e->getMatrix().getTranslation().z() + pos[2];

            xend = xorig + e->getMatrix().getMainVector().x()*e->getLength();
            yend = yorig + e->getMatrix().getMainVector().y()*e->getLength();
            zend = zorig + e->getMatrix().getMainVector().z()*e->getLength();

			if (VoxelSpace::instance()->findVoxel (xorig, yorig, zorig, &ib, &jb, &kb))
			{
				if (VoxelSpace::instance()->findVoxel (xend, yend, zend, &ie, &je, &ke))
				{
					if (ib!=ie || jb!=je || kb!=ke)
					{
						VoxelSpace::instance()->bresenham3D (ib, jb, kb, ie, je, ke, &vv);
					}
					else
                    {
						vv.push_back(VoxelSpace::instance()->addrAt(ib, jb, kb));
                    }

					for (vector<Voxel*>::iterator it=vv.begin(); it!=vv.end(); it++)
					{
						Voxel *v = *it;
						v->content.push_back(e);
                        v->part.push_back(1./vv.size());
                        if (e->getBottomDiam() < voxelSize)
                            v->containedSurface += e->getLength() / vv.size() * e->getBottomDiam() / 2.;
                        else
                            v->containedSurface += e->getLength() / vv.size() * voxelSize / 2.;
					}
				}
			}
		}
	}


//	voxelSpace->displayContent();

	VoxelSpace::instance()->NbPlant(VoxelSpace::instance()->NbPlant()+1);

}




