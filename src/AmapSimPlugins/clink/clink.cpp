/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Output.h"
#include "PlantAMAP.h"
#include "ObserverManager.h"

#ifdef WIN32
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "AmapSimMod.h"
#include "PlantAMAP.h"
#include "GeomBrancAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "externClink.h"
#include "clink.h"
#define STATIC_LIB
#include "GldsFormat.h"
#undef STATIC_LIB



void ParameterPlugin::on_notify (const state& st, paramMsg *data)
{
    //Cast message parameters
    paramAxisRef *p = (paramAxisRef *)data;

    //Retrieve branch
    BrancAMAP *b = (BrancAMAP *)p->b;

    // final straightening
//    if (   p->numVar == rgredres
//        || p->numVar == rgplagredre)
//    {
//        int *i;
//        if ((i = (int*)b->hasAdditionalData("overcanopy")) != NULL)
//        {
//            if (p->numVar == rgplagredre)
//                p->var = 50;
//            else
//                p->var = 0;
//        }
//        else if ((i = (int*)b->getAdditionalData("growthStatus")) != NULL)
//        {
//            if (   *i == BENDED
//                || *i == DEADANDBENDED)
//            {
//                if (p->numVar == rgplagredre)
//                    p->var = 50;
//                else
//                    p->var = 0;
//            }
//            else if (   *i == NOTHOOKED
//                     || *i == DEAD)
//            {
//                // only if no descendent is already hooked
//                if (!clinkplugin_ptr->findHookedDescendent(b))
//                {
//                    {
//                        if (p->numVar == rgplagredre)
//                            p->var = 50;
//                        else
//                            p->var = 0;
//                    }
//                    if (*i == NOTHOOKED)
//                        *i = BENDED;
//                    else if (*i == DEAD)
//                        *i = DEADANDBENDED;
//                }
//            }
//        }
//    }
    // young module
    if (   p->numVar == rgyoung
        && !b->hasAdditionalData("overcanopy"))
    {
//return;
        int *i;
        if ((i = (int*)b->getAdditionalData("growthStatus")) != NULL)
        {
            if (   *i == BENDED
                || *i == DEADANDBENDED)
                p->var = clinkplugin_ptr->BendingYoung();
            else if (   *i == NOTHOOKED
                     || *i == DEAD)
            {
                // only if no descendent is already hooked
                if (!clinkplugin_ptr->findHookedDescendent(b))
                {
                    p->var = clinkplugin_ptr->BendingYoung();
                    if (*i == NOTHOOKED)
                        *i = BENDED;
                    else if (*i == DEAD)
                        *i = DEADANDBENDED;
                }
            }
        }
    }
}


/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{

	paramPlant * d = (paramPlant*)data; //Retrieve msg params

	clinkplugin_ptr->setPlant (d->p);

	// to avoid complexity due to simplication !!!
	d->p->getConfigData().cfg_topoSimp = 0;
	d->p->getConfigData().cfg_geoSimp = 0;

	ClinkEventData *e = new ClinkEventData(0);
	Scheduler::instance()->create_process(clinkplugin_ptr,e,1,103);

}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{
	clinkplugin_ptr->setPlant (NULL);
}

void clinkPlugin::process_event(EventData *msg)
{
    if (plant == NULL)
        return;

    if (plant->getTopology().getTrunc()->getBranc()->getAdditionalData("growthStatus") == NULL)
    {
        int *h = new int;
        *h = NEW;
        plant->getTopology().getTrunc()->getBranc()->addAdditionalData("growthStatus", h);
    }

	ClinkEventData *e = (ClinkEventData*)msg;

	Scheduler *s = Scheduler::instance();
	float top_extern = s->getTopClock();

cout << "top "<<top_extern<<endl;

    if (overCanopy == true)
    {
        cerr <<"SUCCESS " << s->getTopClock() << " " << globalCost <<" overcanopy" << plant->getConfigData().randSeed<<endl;
//        exit(0);
    }

    plant->getGeometry().clear();
    plant->computeGeometry();

    computeBiomass (top_extern);

    buildGrowthStrategy(top_extern);

//		removeBrancs();
    plant->getGeometry().clear();

    // register for next growth step
    e->setAction (e->Action()+1);
    s->signal_event(this->getPid(),e,s->getTopClock()+1,103);
    if (s->getTopClock()+1 > s->getHotStop())
    {
        {
//            cerr <<"FAIL " << s->getTopClock() << " " << globalCost << " " << NbBranc << " " << plant->getConfigData().randSeed << endl;
            cerr <<"FAIL " << s->getTopClock() << " " << globalCost << endl;
        }
    }
//    else
//        cerr << "globalCost " << globalCost << endl;
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
clinkPlugin::clinkPlugin(const std::string& f, const void *plant) {

	leafPhyAge = 1;
	woodMaxPhyAge = 1;

	this->plant = (PlantAMAP*)plant;

	srand (this->plant->getConfigData().randSeed);

	if (!initClinkParam (f))
	{
		cout << "Failed reading clink parameter file " << f << endl;
		cout << "File sould look like this :" << endl<<endl;
 		cout << endl<< "**** Failed reading clink parameter file " << f << " ****" << endl;

		return;
	}
	//child -> parent binding PTR
	l = new ParameterPlugin (this->plant);
	l->clinkplugin_ptr = this;
	p = new PlantPlugin (this->plant);
	p->clinkplugin_ptr = this;
	pp = new PlantDeletePlugin (this->plant);
	pp->clinkplugin_ptr = this;
	eg= new ElemGeometryPlugin(this->plant);
    eg->clinkplugin_ptr = this;

//	srand(this->plant->getConfigData().randSeed);
	clinkParameterFileName = f;

	biomassStock = reserveStock = 0;
	globalCost = 0;
	overCanopy = false;
}

bool clinkPlugin::initClinkParam (const std::string& f) {
	ParamSet * param_H = new ParamSet(f);

	if (!param_H->Exists())
		return false;

	if (param_H->getParameter("hCrown"))
		hCrown = param_H->getParameter("hCrown")->value();
	else
	{
		cout << "Failed finding hCrown parameter in file " << f << endl;
		cout << "setting default value to 0.4 " << endl;
		hCrown = 0.4;
	}

	if (param_H->getParameter("maxLight"))
		maxLight = param_H->getParameter("maxLight")->value();
	else
	{
		cout << "Failed finding maxLight parameter in file " << f << endl;
		cout << "setting default value to 1 " << endl;
		maxLight = 1;
	}

	if (param_H->getParameter("kBeer"))
		kBeer = param_H->getParameter("kBeer")->value();
	else
	{
		cout << "Failed finding kBeer parameter in file " << f << endl;
		cout << "setting default value to 1 " << endl;
		kBeer = 1;
	}

	if (param_H->getParameter("hCanopy"))
		hCanopy = param_H->getParameter("hCanopy")->value();
	else
	{
		cout << "Failed finding hCanopy parameter in file " << f << endl;
		cout << "setting default value to 10 " << endl;
		hCanopy = 10;
	}

	if (param_H->getParameter("leafCost"))
		leafCost = param_H->getParameter("leafCost")->value();
	else
	{
		cout << "Failed finding leafCost parameter in file " << f << endl;
		cout << "setting default value to 1 " << endl;
		leafCost = 1;
	}

	if (param_H->getParameter("woodCost"))
		woodCost = param_H->getParameter("woodCost")->value();
	else
	{
		cout << "Failed finding woodCost parameter in file " << f << endl;
		cout << "setting default value to 10 " << endl;
		woodCost = 10;
	}

	if (param_H->getParameter("reserveRatio"))
		reserveRatio = param_H->getParameter("reserveRatio")->value();
	else
	{
		cout << "Failed finding reserveRatio parameter in file " << f << endl;
		cout << "setting default value to 0.1 " << endl;
		reserveRatio = 0.1;
	}

	if (param_H->getParameter("leafPhyAge"))
		leafPhyAge = param_H->getParameter("leafPhyAge")->value();
	else
	{
		cout << "Failed finding leafPhyAge parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("woodMaxPhyAge"))
		woodMaxPhyAge = param_H->getParameter("woodMaxPhyAge")->value();
	else
	{
		cout << "Failed finding woodMaxPhyAge parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("woodResistivity"))
		woodResistivity = param_H->getParameter("woodResistivity")->value();
	else
	{
		cout << "Failed finding woodResistivity parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("leafResistivity"))
		leafResistivity = param_H->getParameter("leafResistivity")->value();
	else
	{
		cout << "Failed finding leafResistivity parameter in file " << f << endl;
		return false;
	}


	if (param_H->getParameter("maxBranchNumberPerShoot"))
		maxBranchNumberPerShoot = param_H->getParameter("maxBranchNumberPerShoot")->value();
	else
	{
		cout << "Failed finding maxBranchNumberPerShoot parameter in file " << f << endl;
		cout << "setting default value to 3 " << endl;
		maxBranchNumberPerShoot = 3;
	}

	if (param_H->getParameter("bendingYoung"))
		bendingYoung = param_H->getParameter("bendingYoung")->value();
	else
	{
		cout << "Failed finding bendingYoung parameter in file " << f << endl;
		cout << "setting default value to 10 " << endl;
		bendingYoung = 10;
	}

/*
    FloatData *bs = new FloatData;
    bs->val = 0;
    plant->addAdditionalData("addedWoodVolume", bs, VITISGLDSPRINTABLE);
*/
	return true;
}

// Every leaf contributes to the global stock according to its surface and height into the canopy
void clinkPlugin::computeBiomass (float top_extern)
{
    float addedStock = 0;
    float l, d, z;
    float phyAgeGu;
	GeomElemCone *e;
	vector<GeomElem *> ges;

	biomassStock = reserveStock;
	float *f = (float*)plant->getAdditionalData("addedWoodVolume");
	if (f != NULL)
{
        biomassStock -= *f * woodCost;
cout << "added wood " << *f << endl;
}
else
cout << "no added wood, check pipe model plugin"<<endl;
	reserveStock = 0;

    updateNbLeaves();

	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
 	for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
		GeomBrancAMAP *g = (GeomBrancAMAP*)*b;

		BrancAMAP *ba = (BrancAMAP*)g->getBranc();
        phyAgeGu = ba->getCurrentGu()->phyAgeGu;

        if (phyAgeGu < leafPhyAge-1)
            continue;
        if (ba->getCurrentEntnNumberWithinBranch() == 0)
            continue;

        l = g->computeLength();
        d = g->computeBottomDiam();

		ges = g->getElementsGeo ();
		e = (GeomElemCone*)ges[ges.size()-1];
		z = e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength();
		if (z > hCanopy)
            z = hCanopy;
        // no light extinction under crown
        if (z < hCanopy * (1-hCrown))
            z = hCanopy * (1-hCrown);

        float RPath = computePathResistance(g);
        float RLeaf = l * d * leafResistivity;

        addedStock += l * d * maxLight * exp(-kBeer*(hCanopy-z)/100) * RLeaf / (RLeaf + RPath);
    }
    biomassStock += addedStock * (1-reserveRatio);
    reserveStock += addedStock * reserveRatio;

cout << "biomass stock "<< biomassStock<< "  added "<<addedStock<<"  reserve "<<reserveStock<<endl;
}

// function to sort buds according to their decreasing height
struct by_height
{
    bool operator()(sortedBud const &left, sortedBud const &right)
    {
        return left.height > right.height;
    }
};

// build a list of buds that are candidate to growth
// a new bud is candidate if it is at the highest part of a bearer shoot (apical for hooked shoots, medium for bending shoots)
// a bud that is already grown and either NEW or HOOKED and not dead is systematicaly candidate with a higher prority compared to new buds
// the number of candidates per bearer shoot is fixed. It may be inversely linked to the length of the bearer shoot
void clinkPlugin::shootSites (float top_extern, std::vector<sortedBud> *totalBudList)
{
//    totalBudList->clear();
    totalBudList->erase(totalBudList->begin(), totalBudList->end());
    float phyAgeGu;
    std::vector<sortedBud> shootBudList;
    sortedBud s;
    GeomBranc *borne;
    BrancAMAP *borneBranc;
    GrowthUnitAMAP *bearerGU;
    Bud *borneBud;
    float height;

    int *control = (int*)0x55555561ce70;
    totalBudList->erase(totalBudList->begin(), totalBudList->end());
int nbBranc = 0;
int nbLeaf = 0;

	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
		GeomBrancAMAP *g = (GeomBrancAMAP*)*b;

		BrancAMAP *ba = (BrancAMAP*)g->getBranc();

         // never branch on first GU
        if (ba->getGuNumber() <= 1)
            continue;

        phyAgeGu = ba->getCurrentGu()->phyAgeGu;

        if (phyAgeGu >= leafPhyAge-1)
        {
            nbLeaf ++;
            continue;
        }

        s.b = ba->getBud();
        GeomElem *elem = g->getElementsGeo()[g->getElementsGeo().size()-1];
        height = elem->getMatrix().getTranslation().z()+elem->getMatrix().getMainVector().z()*elem->getLength();
        s.height = height;

        // add branch if not dead
        int *h = (int*)ba->getAdditionalData("growthStatus");
        if (h == NULL)
        {
            h = new int;
            *h = NEW;
            ba->addAdditionalData("growthStatus", h);
        }
        if (   *h != DEAD
            && *h != DEADANDBENDED)
            totalBudList->push_back(s);

nbBranc++;
        int maxNumber = MaxBranchNumberPerShoot(ba);
        for (int i=0; i<ba->getCurrentHierarc()->getNbBorne(); i++)
        {
            borneBranc = (BrancAMAP*)ba->getCurrentHierarc()->getBorne(i);
            phyAgeGu = borneBranc->phyAgeInit;
            if (phyAgeGu >= leafPhyAge-1)
                continue;

            if (borneBranc->getCurrentEntnNumberWithinBranch() > 0)
                continue;

            int pos = borneBranc->getCurrentHierarc()->indexOnBearerAtLowestLevel;

            // only brancs borne on the last GU
            ba->startPosit();
            ba->positOnElementAtSubLevel(pos);
            if (ba->getCurrentGuIndex() != ba->getGuNumber()-1)
            {
                ba->endPosit();
                continue;
            }
            ba->endPosit();

            elem = g->getElementsGeo()[pos];
            height = elem->getMatrix().getTranslation().z()+elem->getMatrix().getMainVector().z()*elem->getLength();
            s.b = borneBranc->getBud();
            int *h = (int*)borneBranc->getAdditionalData("growthStatus");
            if (h != NULL)
            {
                borneBranc->removeAdditionalData("growthStatus");
                delete h;
            }
            if (height >= hCanopy)
            {
                borneBranc->addAdditionalData("overcanopy", NULL);
                overCanopy = true;
            }
            s.height = height;
            shootBudList.push_back(s);
        }
        std::sort(shootBudList.begin(), shootBudList.end(), by_height());

        // select MaxNumber buds and stop others
        int addedBud = 0;
        for (vector<sortedBud>::iterator sbud=shootBudList.begin(); sbud!=shootBudList.end(); sbud++)
        {
            if (sbud->b == NULL)
                continue;

            if (addedBud < maxNumber)
            {
                totalBudList->push_back(*sbud);
                addedBud++;
            }
            else
            {
                // stop bud
                Scheduler::instance()->stop_process(sbud->b->process_id);
                sbud->b = NULL;
            }

        }

        shootBudList.erase(shootBudList.begin(), shootBudList.end());
    }
cout << totalBudList->size()<< " sites de pousse potentiels pour "<<nbBranc<<" porteuses"<<endl;
}

// method to compute the number of axial meristems that may grow fron this branch
// for now, it is ruled by a fixed value. It also may be linked to the size of the shoot: for instance, the longer the shoot, the fewer the axis number
int clinkPlugin::MaxBranchNumberPerShoot (BrancAMAP *b)
{
    return maxBranchNumberPerShoot;
}

// compute which buds will sprout according to the current biomass stock.
// for long shoots, determine if they will hook or not
void clinkPlugin::buildGrowthStrategy (float top_extern)
{
    sortedBud s;
    std::vector<sortedBud> budList;

    shootSites (top_extern, &budList);

    // build a sorted list of branches and what will happen to them (no growth, leafy growth, axial growth)
    std::sort(budList.begin(), budList.end(), by_height());

    while (!budList.empty())
    {
        s = budList.back();
        budList.pop_back();

        if (s.b == NULL)
            continue;

        float cost = computeCost (s.b->getBranc());

        if (cost > biomassStock)
        {
            // stop branc
            Scheduler::instance()->stop_process(s.b->getBranc()->getBud()->process_id);
            continue;
        }

        int *h = (int*)s.b->getBranc()->getAdditionalData("growthStatus");
        if (h == NULL)
        {
            h = new int;
            *h = NEW;
            s.b->getBranc()->addAdditionalData("growthStatus", h);
        }

        biomassStock -= cost;
        globalCost += cost;
    }

    // test hooking-death for every branches
    BrancAMAP *b=(BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);

    int phyAgeGU;
    int nbnew=0;
    int nbhooked=0;
    int nbnothooked=0;
    int nbdead=0;
    int nbbranc=0;
    int nbleaf=0;
    int nbovercanopy=0;
    do
    {
        if (b->getCurrentEntnNumberWithinBranch() == 0)
             phyAgeGU = b->phyAgeInit;
        else
            phyAgeGU = b->getCurrentGu()->phyAgeGu;

        if (phyAgeGU >= leafPhyAge)
        {
            nbleaf++;
            continue;
        }

        // test hooking
        int *h = (int*)b->getAdditionalData("growthStatus");
        if (h == NULL)
            continue;

        if (   *h == DEAD
            || *h == DEADANDBENDED)
            continue;


        nbbranc++;

        float p = (float) (rand() % 1000) / 1000;
        // if branch over canopy, no hooking
        if (b->hasAdditionalData("overcanopy"))
            p = 1;

        // find corresponding hookProba associated to bud height
        float hookProba = computeHookProba (b);
        if (   p < hookProba
            && *h != BENDED
            && *h != DEADANDBENDED)
        {
            *h = HOOKED;
            nbhooked++;
        }
        else if (b->hasAdditionalData("overcanopy"))
        {
            *h = NOTHOOKED;
            nbovercanopy++;
        }
        else if (   *h == NOTHOOKED
                 || *h == BENDED)
        {
            if (*h == BENDED)
                *h = DEADANDBENDED;
            else
                *h = DEAD;
            // stop bud
            Scheduler::instance()->stop_process(b->getBud()->process_id);
            nbdead++;
        }
        else
        {
            *h = NOTHOOKED;
            nbnothooked++;
        }
    } while ((b=(BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT)) != NULL);

if (   top_extern > 1
    && nbleaf == 0)
{
//    cerr <<"FAIL "<< top_extern << " " << globalCost << " "<< plant->getConfigData().randSeed << endl;
    cerr <<"FAIL "<< top_extern << " " << globalCost << endl;
    exit (0);
}
cout <<"nbbranc "<<nbbranc<<" nbhooked "<<nbhooked<<" nbnothooked "<<nbnothooked<<" nbdead "<<nbdead<<" nbovercanopy "<<nbovercanopy<<endl;

 if (nbbranc > 2000)
{
    cerr <<"SUCCESS "<< top_extern << " " << globalCost << " nbbranc>2000"<<endl;
    exit (0);
}


NbBranc = nbbranc;
}

float clinkPlugin::computeCost(BrancAMAP *b)
{
    float cost;
    float phyAge, phyAgeLeaf;
    int np, nn, dp, dn, nbentn;
    float dWood, lWood, dLeaf, lLeaf;

    AxisReference *axisref = (AxisReference*)b->getBud()->axisref;
    if (b->getGuNumber() == 0)
        phyAge = b->phyAgeInit;
    else
        phyAge = b->getCurrentGu()->phyAgeGu+1;

    phyAgeLeaf = leafPhyAge;
    b->computeInitialLengthDiameter (&lWood, &dWood);
    lLeaf = axisref->val1DParameter(lnginit, phyAgeLeaf, b);
    dLeaf = axisref->val1DParameter(diaminit, phyAgeLeaf, b);
    np = axisref->val1DParameter (rtprefn1, phyAge, b);
    dp = axisref->val1DParameter (rtprefd1, phyAge, b);
    nbentn = np + dp;

    cost = nbentn * (lWood * M_PI * dWood * dWood / 4 * woodCost + lLeaf * dLeaf * leafCost);
    return cost;
}

void clinkPlugin::removeBrancs()
{
	BrancAMAP *b;
	bool trouve = true;
	Hierarc *h;
	vector<Hierarc *> listHierarc;

	// create the list of hierarcs to be deleted
	while (trouve)
	{
		trouve = false;
		for (vector<GeomBranc *>::iterator it=plant->getGeometry().getGeomBrancStack().begin(); it!=plant->getGeometry().getGeomBrancStack().end(); it++)
		{
			GeomBranc *g = *it;

			FloatData *L=(FloatData*)g->getBranc()->getAdditionalData("growthStatus");
			if (L == NULL)
				continue;
            if (   L->val != DEAD
                && L->val != DEADANDBENDED)
                continue;

			plant->getGeometry().getGeomBrancStack().erase(it);
			h = g->getAxe();
			listHierarc.push_back(h);
			trouve = true;

			break;
		}
	}

	// delete the hierarcs
	// in case of topological simplification, removing a part of a simplified branc makes it different from
	// the other previous siblings. So it has to be put out of the simplified hierarc list.
	//
	for (vector<Hierarc *>::iterator it=listHierarc.begin(); it!=listHierarc.end(); it++)
	{
		h = *it;
		b = (BrancAMAP *)h->getBranc();
		if (b->phyAgeInit >= leafPhyAge-1)
		{
			plant->getTopology().removeBranc(b, 0);
		}
		else
		{
			// stop current bud growth
			int aid = Scheduler::instance()->getActionIndex (b->getBud()->process_id);
			Action ai = Scheduler::instance()->getAction (b->getBud()->process_id);
			Scheduler::instance()->changeAction (aid, ai.pid, Scheduler::instance()->getHotStop()+1, ai.priority, NULL);
		}

	}

}


void clinkPlugin::writeOPF(const std::string &finame)
{
	int i;

	std::stringstream sout;
	sout << finame;
	std::string fname (sout.str());

	GldsFormat *outFormat= new GldsFormat(fname);

	outFormat->initFormat(&(plant->getGeometry()));

	std::cout<<"Output Geometry opf to : "  << fname << ".opf" << std::endl;

	if (plant->getGeometry().getGeomBrancStack().size())
	{
		outFormat->printData(plant->getGeometry().getGeomBrancStack().at(0));
	}
	else
	{
		std::cout<<"WARNING : -------> Empty Geometry"<<std::endl;
	}

	((GldsFormat*)outFormat)->closeFormat();

	delete outFormat;
}

bool clinkPlugin::findHookedDescendent(BrancAMAP *b)
{
    BrancAMAP *borne;
    int *h;

    if (b->phyAgeInit >= leafPhyAge)
        return false;

    if ((h = (int*)b->getAdditionalData("growthStatus")) == NULL)
        return false;

    if (*h == HOOKED)
        return true;

    // check descendents
    for (int i=0; i<b->CurrentHierarcPtr()->borneNumber; i++)
    {
        if (findHookedDescendent((BrancAMAP*)b->CurrentHierarcPtr()->getBorne(i)))
            return true;
    }

    return false;
}

float clinkPlugin::computeHookProba (BrancAMAP *b)
{
    float hookProba = 0;
    float height = computeBudHeight (b);

    if (height > hCanopy)
        hookProba = 0;
    else if (height < hCanopy - hCrown)
//        hookProba = kBeer * 10;
        hookProba = kBeer * 15;
    else
//        hookProba = kBeer * 20;
        hookProba = kBeer * 30;

    return hookProba;
}

float clinkPlugin::computeBudHeight (BrancAMAP *b)
{
    GeomElem *elem;
    BrancAMAP *bearer;
    Hierarc *h;
    int pos;
    GeomBrancAMAP *g;
	if (b->getCurrentEntnNumberWithinBranch() == 0)
	{
        bearer = b->getBearer();
        h = bearer->getCurrentHierarc();
        for (pos=0; pos<h->getNbBorne(); pos++)
        {
            h->getBorne(pos);
            if ((BrancAMAP*)h->bornePtr[pos]->getBranc() == b)
                break;
        }
        if (pos == h->getNbBorne())
        {
cout << "pas de parent" << endl;
            return 0;
        }
        g = findGeomBranc(bearer);
        pos = h->bornePosition[pos];
        elem = g->getElementsGeo()[pos];
	}
	else
	{
        g = findGeomBranc(b);
        elem = g->getElementsGeo()[g->getElementsGeo().size()-1];
	}
    return elem->getMatrix().getTranslation().z()+elem->getMatrix().getMainVector().z()*elem->getLength();
}

GeomBrancAMAP *clinkPlugin::findGeomBranc (BrancAMAP *b)
{
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	for (vector<GeomBranc *>::iterator gb=brcs.begin(); gb!=brcs.end(); gb++)
	{
		GeomBrancAMAP *g = (GeomBrancAMAP*)*gb;

		if ((BrancAMAP*)(g->getBranc()) == b)
            return g;
    }
    return NULL;
}

/**
 *
 *
 * This function get called at the last stage will add lateral deviation
 * according to position of the branch over canopy or not
 *
 *
 *
 */
void ElemGeometryPlugin::on_notify(const state& st, paramMsg *data)
{

    paramElemPosition * p = (paramElemPosition*)data;
    GeomElemCone *gec = (GeomElemCone *)p->gec;
    GeomBrancAMAP * geomBranc = (GeomBrancAMAP *)gec->getPtrBrc();
    BrancAMAP *b = (BrancAMAP *)geomBranc->getBranc();

    if (!b->hasAdditionalData("overcanopy"))
        return;

	float angle;
	float pos, lng=0;

	// compute position of current element and length of the leaf
	pos = b->getCurrentEntnIndexWithinBranch() * gec->getLength();
	lng = b->getTestNumberWithinBranch() * gec->getLength();

    Matrix4 current_triedron(p->p);

    Vector3 translation = current_triedron.getTranslation();
    if (translation.z() < clinkplugin_ptr->HCanopy())
        return;

    //Move to origin
    current_triedron = Matrix4::translation(-translation) * current_triedron;

    Vector3 mainDirection = current_triedron.getMainVector();
    mainDirection.normalize();

	angle = asin (mainDirection.z());
	// apply 10% random noise to horizontal correction
    float rnd = (float) (rand() % 1000) / 10000;
    angle *= rnd;

	// compute a horizontal rotation axis that is orthogonal to main direction and vertical
	Matrix4 matRot;
	geomBranc->determine_plagio (matRot, current_triedron.getMainVector(), Vector3(0,0,1));
	Vector3 v=matRot.getNormalVector();

	// apply bending
	current_triedron = Matrix4::axisRotation(v,-angle) * current_triedron;

	//Go back to original position
	current_triedron = Matrix4::translation(translation) * current_triedron;

	gec->setMatrix(current_triedron);
	p->set (gec, current_triedron);

}

void clinkPlugin::updateNbLeaves()
{
    int *nbLeaves;
    BrancAMAP *leaf, *ba;
	GeomElemCone *e;
	vector<GeomElem *> ges;
	GeomBrancAMAP *g;
	int pos;

    // reset numbers
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
 	for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
		g = (GeomBrancAMAP*)*b;

		BrancAMAP *ba = (BrancAMAP*)g->getBranc();

        if (ba->getCurrentGu()->phyAgeGu >= leafPhyAge-1)
            continue;
        if (ba->getCurrentEntnNumberWithinBranch() == 0)
            continue;

		ges = g->getElementsGeo ();
        for (vector<GeomElem *>::iterator ie=ges.begin(); ie!=ges.end(); ie++)
        {
            e = (GeomElemCone*)*ie;
            if ((nbLeaves = (int*)e->getAdditionalData("nbLeaves")) == NULL)
            {
                nbLeaves = new int;
                e->addAdditionalData("nbLeaves", nbLeaves);
            }
            *nbLeaves = 0;
        }
    }
 	for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
		GeomBrancAMAP *g = (GeomBrancAMAP*)*b;

		BrancAMAP *ba = (BrancAMAP*)g->getBranc();

        if (ba->getCurrentGu()->phyAgeGu < leafPhyAge-1)
            continue;
        if (ba->getCurrentEntnNumberWithinBranch() == 0)
            continue;

		g = (GeomBrancAMAP*)*b;
		do
		{
            pos = g->getPosOnBearer();
            g = (GeomBrancAMAP*)g->getGeomBrancBearer();
            ges = g->getElementsGeo ();
            for (int i=pos; i>=0; i--)
            {
                e = (GeomElemCone*)ges[i];
                nbLeaves = (int*)e->getAdditionalData("nbLeaves");
                *nbLeaves ++;
            }
        } while (g != brcs[0]);
    }
}

float clinkPlugin::computePathResistance(GeomBrancAMAP *leaf)
{
    float resistance=0, l, d;
    int *nbLeaves, pos;
    GeomBrancAMAP *g = leaf;
	GeomElemCone *e;
	vector<GeomElem *> ges;

    do
    {
        pos = g->getPosOnBearer();
        g = (GeomBrancAMAP*)g->getGeomBrancBearer();
        ges = g->getElementsGeo ();
        for (int i=pos; i>=0; i--)
        {
            e = (GeomElemCone*)ges[i];
            l = e->getLength();
            d = e->getBottomDiam();

            nbLeaves = (int*)e->getAdditionalData("nbLeaves");
            resistance += l * d * woodResistivity / *nbLeaves;
        }
    } while (g != plant->getGeometry().getGeomBrancStack()[0]);

    return resistance;
}

