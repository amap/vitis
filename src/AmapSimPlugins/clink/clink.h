/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file clink.h
///			\brief Definition of climber simulation class
///
/// pour bien grimper il faut :
/// - savoir fabriquer des ressources qui proviennent:
///     - des feuilles qui interceptent la lumiere (exponentiellement decroissante dans le houppier).
///     - stock = stockPrec + sum (taille (feuille) * maxLight * exp (-k * max (min (zfeuille, hCanopy), hCanopy*(1-hCrown)) (parameter maxLight, k, hCanopy, hCrown)))
/// - appliquer une boucle de croissance
///     - fabriquer des pousses courtes feuillees. En priorite sur les pousses longues par ordre de hauteur.
///         - une pousse courte feuillee tout au debut (issue d'un stock contenu dans une graine virtuelle).
///         - a leur sommet si elles sont erigees, a leur milieu si elles sont courbees.
///         - chaque pousse courte a un cout (parametrer shortCost).
///         - fabriquer autant de pousses courtes que de sites de pousse ou de stock disponible (parameter reserveRatio).
///         - les pousses courtes poussent vers le haut
///     - effectuer un calcul de production de ressource
///     - fabriquer des pousses longues a la suite des pousses courtes par priorite de hauteur
///         - chaque pousse longue a un cout (parameter longCost).
///         - fabriquer autant de pousses longues que de sites de pousse ou de stock disponible.
///         - fabriquer des pousses courtes si le stock disponible minimum n'a pas ete atteint (les pousses courtes peuvent porter une pousse courte).
///     - effectuer un calcul de production de ressource
///     - la probabilite d'accrocchage est 0.05 * k dans le houppier et 0.1 * k en dessous et 0 au dessus (parameter hCanopy, hCrown).
///     - determiner si les pousses longues ont trouve un point d'accroche.
///     - tuer les pousses qui ont subi deux echecs d'accrochage successifs.
///     - les pousses longues qui n'ont pas trouve de point d'accroche deviennent pendantes.
///     - tuer les feuilles qui ont atteint leur fin de vie.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
using namespace std;

#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "ParamFile.h"
#include <vector>

struct clinkPlugin;

// data to sort buds according to their decreasing height
struct sortedBud
{
    float height;
    Bud *b;
};


#include "defAMAP.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\struct clinkEventData
///			\brief Data that is transmitted from one growth step to the next one
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct ClinkEventData: public EventData
{
public:
	ClinkEventData (int action){this->action = action;};
	int Action (){return action;};
	void setAction (int a){action = a;};
private:
	int action;
};

#define NEW 0
#define HOOKED 1
#define NOTHOOKED 2
#define DEAD 3
#define DEADANDBENDED 4
#define BENDED 5

#define LEAFY 0
#define AXIALIZED 1

enum ShootType {LONG, SHORT};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\struct FloatData
///			\brief Simple floating point data that is able to output (for OPF purpose)
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct FloatData : public PrintableObject
{
public :
	float val;
	std::string to_string ()
	{
		std::ostringstream oss;
		// �crire la valeur dans le flux
		oss << val ;
		// renvoyer une string
		return oss.str();
	};
};


struct ElemGeometryPlugin: public subscriber<messageVitisElemGeometry> {
	clinkPlugin * clinkplugin_ptr;

	ElemGeometryPlugin (void *p) { subscribe(p);};
	virtual ~ElemGeometryPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct ParameterPlugin:public subscriber<messageAxeRef> {
	clinkPlugin * clinkplugin_ptr;

	ParameterPlugin (Plant *p) {subscribe(p);};
	virtual ~ParameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

//struct DecompPlugin:public subscriber<messageVitisDecompAxeLevel> {
//	clinkPlugin * clinkplugin_ptr;
//
//	DecompPlugin (Plant *p) {subscribe(p);};
//	virtual ~DecompPlugin () {unsubscribe();};
//	void on_notify (const state& st, paramMsg * data=NULL);
//};

struct PlantPlugin:public subscriber<messageVitisPlant> {
	clinkPlugin * clinkplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	clinkPlugin * clinkplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

enum messageclinkParameter {SigGetParameter};

// specialized signal class for browsing
struct clinkParameterSignal:public ParameterSignal
{
public:
	float sigGetParameter (float & vv, Parameter *pp, VitisObject *oo);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\struct clinkPlugin
///			\brief Describe a climber module that will control further growth from the current simulated plant \n
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct clinkPlugin : public VProcess
{
public :
	clinkPlugin (const std::string& f, const void *p);
	virtual ~clinkPlugin(){;};
	void setPlant (Plant  *p) {plant = (PlantAMAP*)p;};
	Plant *getPlant () {return plant;};
	void process_event(EventData *msg);
    int WoodMaxPhyAge () {return woodMaxPhyAge;};
    float BendingYoung () {return bendingYoung;};
    bool findHookedDescendent(BrancAMAP *b);
    float HCanopy () {return hCanopy;};

protected :
    bool initClinkParam (const std::string& f);
	void computeBiomass (float top_extern);
    float computeCost (BrancAMAP *b);
    int MaxBranchNumberPerShoot (BrancAMAP *b);
    void shootSites (float top_extern, std::vector<sortedBud> *totalBudList);
    void buildGrowthStrategy (float top_extern);
    void removeBrancs();
    void outputGrowthResults();
	void writeOPF(const std::string &finame);
	float computeBudHeight (BrancAMAP *b );
	float computeHookProba (BrancAMAP *b);
	GeomBrancAMAP *findGeomBranc (BrancAMAP *b);
	void updateNbLeaves ();
    float computePathResistance (GeomBrancAMAP *leaf);
	float globalCost;
	bool overCanopy;
	int NbBranc=0;


	PlantAMAP *plant;
	BrancAMAP *curBranc;

	ParameterPlugin *l;
	PlantPlugin *p;
	PlantDeletePlugin *pp;
	ElemGeometryPlugin *eg;

    std::string clinkParameterFileName;
//	float hookProba;
    // pourcentage de la hauteur totale
    float hCrown;
	float maxLight, hCanopy, kBeer;
	float reserveRatio;
	float woodCost, leafCost;
	int maxBranchNumberPerShoot;
	float bendingYoung;
	float leafResistivity, woodResistivity;

	int leafPhyAge;
	int woodMaxPhyAge;

	float biomassStock, reserveStock;

};

extern "C" CLINK_EXPORT  clinkPlugin * StartPlugin (const std::string& f, const void *p)
{
	std::cout << "loading AmapSim clinkPlugin module with parameter " << f << std::endl;

	return new clinkPlugin (f, p) ;
};

