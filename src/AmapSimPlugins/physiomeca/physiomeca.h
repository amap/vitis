/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "ParamFile.h"
#include "Voxel.h"
#include "Vector.h"
//#include "externF.h"


#include <vector>

class physiomecaPlugin;



#include "defAMAP.h"


class FMassData : public PrintableObject
{
public :
	double mass;
	Vector3 center;
	std::string to_string ()
	{
		std::ostringstream oss;
		// �crire la valeur dans le flux
		oss << mass;
		oss << " ";
		oss << center.x();
		oss << " ";
		oss << center.y();
		oss << " ";
		oss << center.z();
		// renvoyer une string
		return oss.str();
	};
};

/*
struct PHYSIOMECA_EXPORT ParameterPlugin:public subscriber<messageAxeRef> {
	physiomecaPlugin * physiomecaPlugin_ptr;

	ParameterPlugin (Plant *p) {subscribe(p);};
	virtual ~ParameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};
*/

struct PHYSIOMECA_EXPORT DecompPlugin:public subscriber<messageVitisDecompAxeLevel> {
	physiomecaPlugin * physiomecaPlugin_ptr;

	DecompPlugin (Plant *p) {subscribe(p);};
	virtual ~DecompPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct PHYSIOMECA_EXPORT PlantPlugin:public subscriber<messageVitisPlant> {
	physiomecaPlugin * physiomecaPlugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct PHYSIOMECA_EXPORT PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	physiomecaPlugin * physiomecaPlugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

enum messageLightParameter {SigGetParameter};

// specialized signal class for light
class PHYSIOMECA_EXPORT lightParameterSignal:public ParameterSignal
{
public:
	float sigGetParameter (float & vv, Parameter *pp, VitisObject *oo);
};


class PHYSIOMECA_EXPORT physiomecaPlugin : public VProcess
{
public :
	physiomecaPlugin (const std::string& f, const void *p);
	virtual ~physiomecaPlugin(){;};
	void setPlant (Plant  *p) {plant = (PlantAMAP*)p;};
	Plant *getPlant () {return plant;};
	void process_event(EventData *msg);
	void setDecompPlugin (DecompPlugin *p){d = p;};

	float startTime;
	float livingThreshold;
	float ramificationThreshold;
	float cat1Attenuation;
	float cat2Attenuation;
	int cat1Threshold;
	int cat2Threshold;
	int lightningModel;

	float siteAttenuationCoefficient;
	float cloneAttenuationCoefficient;

	// MIR parameters
	float latitude;
	float clearnessIndex;
	int beginingDay, endingDay;
	float refLight;

	float voxelSize;

protected :
    bool initFParam (const std::string& f);
	void computeMass (float top_extern);
	void computeUpperPoint(Vector3 *v);
	float computeCenterPoint (Vector3 *v);
	BrancAMAP* findBranc(int id);
	void writeOPF(const std::string &finame);

	PlantAMAP *plant;
	BrancAMAP *curBranc;

/*	ParameterPlugin *l;
*/
	DecompPlugin *d;
	PlantPlugin *p;
	PlantDeletePlugin *pp;

};

extern "C" PHYSIOMECA_EXPORT  physiomecaPlugin * StartPlugin (const std::string& f, const void *p)
{
	std::cout << "loading AmapSim physiomecaPlugin module with parameter " << f << std::endl;

	return new physiomecaPlugin (f, p) ;
}
