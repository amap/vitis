/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "PlantAMAP.h"
#include "ObserverManager.h"

#ifdef WIN32
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "PlantAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "GeomBrancAMAP.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "GeomElemCone.h"
//#include "externF.h"
#include "externPhysiomeca.h"
//#include "F.h"
#include "physiomeca.h"
#define STATIC_LIB
#include "Output.h"
#include "lightThroughVoxels.h"
#include "GldsFormat.h"
#undef STATIC_LIB

/*
void ParameterPlugin::on_notify (const state& st, paramMsg *data)
{
    //Cast message parameters
    paramAxisRef * p = (paramAxisRef *)data;

    //Retrieve branch
    BrancAMAP * b = (BrancAMAP *)p->b;

	if (p->numVar == lnginit)
	{
		if (b->phyAgeInit >= 1000)
		{
			p->var *= pow(physiomecaPlugin_ptr->siteAttenuationCoefficient,1.5);
			p->var *= pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient,1.5);
//			p->var *= pow(physiomecaPlugin_ptr->siteAttenuationCoefficient,1);
//			p->var *= pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient,1);
		}
		else
		{
			p->var *= pow(physiomecaPlugin_ptr->siteAttenuationCoefficient,1);
			p->var *= pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient,1);
		}
	}
	else if (   p->numVar == diaminit
            && b->phyAgeInit >= 1000)
	{
		p->var *= pow(physiomecaPlugin_ptr->siteAttenuationCoefficient,1.5);
		p->var *= pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient,1.5);
//			p->var *= pow(physiomecaPlugin_ptr->siteAttenuationCoefficient,1);
//			p->var *= pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient,1);
	}


	FLightData *light = (FLightData*)b->getAdditionalData("light");
	if (!light)
		return;

	float top = Scheduler::instance()->getTopClock();

	// entn test number
//	if (   p->numVar == rtprefn1
//	    || p->numVar == rtneopn1
//	    || p->numVar == rtneon1)
	if (p->numVar == rtneon1)
	{
	    if (b->getCurrentHierarc()->ordre == 0)
            p->var *= physiomecaPlugin_ptr->cloneAttenuationCoefficient;
        else
            p->var *= pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient,.3);
		p->var *= physiomecaPlugin_ptr->siteAttenuationCoefficient;
//		p->var *= pow(physiomecaPlugin_ptr->siteAttenuationCoefficient,4);
//		p->var *= pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient,4);

		if (   light->v == 1
		    || top < physiomecaPlugin_ptr->startTime)
			return;

		float d1, d2, v1, v2, coef;
		int cat;
		if (b->phyAgeInit < physiomecaPlugin_ptr->cat1Threshold)
			cat = 1;
		else if ((b->phyAgeInit < physiomecaPlugin_ptr->cat2Threshold))
			cat = 2;
		else
			cat = 3;

		if (light->v < physiomecaPlugin_ptr->livingThreshold)
		{
			v1 = 0;
			v2 = 0;
			d2 = 1;
			d1 = 0;
		}else if (light->v < physiomecaPlugin_ptr->ramificationThreshold)
		{
			d1 = physiomecaPlugin_ptr->livingThreshold;
			d2 = physiomecaPlugin_ptr->ramificationThreshold;
			switch (cat)
			{
			case 1 :
				v1 = 0;
				v2 = physiomecaPlugin_ptr->cat1Attenuation;
				break;
			case 2 :
				v1 = 0;
				v2 = physiomecaPlugin_ptr->cat2Attenuation;
				break;
			case 3 :
				v1 = 0;
				v2 = 1;
				break;
			}
		}else
		{
			d1 = physiomecaPlugin_ptr->ramificationThreshold;
			d2 = 1;
			switch (cat)
			{
			case 1 :
				v1 = physiomecaPlugin_ptr->cat1Attenuation;
				v2 = 1;
				break;
			case 2 :
				v1 = physiomecaPlugin_ptr->cat2Attenuation;
				v2 = 1;
				break;
			case 3 :
				v1 = 1;
				v2 = 1;
				break;
			}
		}
		coef = v1 + (v2 - v1) * (1. - (d2-light->v)/(d2 - d1));
		p->var *= coef;
	}


// CAUTION this set of control is coded
//         assuning lateral production order is branch-leaf-reiteration
//         and both Z1 and Z2 are branched

	// initial branched first state proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpbinitret0
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpbinitret0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret0)
	{
		if (   light->v < physiomecaPlugin_ptr->ramificationThreshold
		    && top >= physiomecaPlugin_ptr->startTime)
			p->var = 1;
		else
		{
//			p->var = 1 - (1-p->var) * sqrt(physiomecaPlugin_ptr->cloneAttenuationCoefficient * physiomecaPlugin_ptr->siteAttenuationCoefficient);
		}
	}
	// initial branched first state proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpbinitret1
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpbinitret1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret1)
	{
		if (   light->v < physiomecaPlugin_ptr->ramificationThreshold
		    && top >= physiomecaPlugin_ptr->startTime)
			p->var = 0;
		else
		{
//			p->var *= sqrt(physiomecaPlugin_ptr->siteAttenuationCoefficient);
//			p->var *= sqrt(physiomecaPlugin_ptr->cloneAttenuationCoefficient);
		}
	}
	// remaining not branched proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_st0
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_st0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st0)
	{
		if (   light->v < physiomecaPlugin_ptr->ramificationThreshold
		    && top >= physiomecaPlugin_ptr->startTime)
			p->var = 1;
		else
		{
//			if (b->phyAgeInit < physiomecaPlugin_ptr->cat1Threshold)
//			p->var = 1 - (1-p->var) * sqrt(physiomecaPlugin_ptr->cloneAttenuationCoefficient * physiomecaPlugin_ptr->siteAttenuationCoefficient);
			p->var = 1 - (1-p->var) * pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient * physiomecaPlugin_ptr->siteAttenuationCoefficient, .25);
		}
	}
	// remaining first state proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_st1
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_st1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st1)
	{
		if (   light->v < physiomecaPlugin_ptr->ramificationThreshold
		    && top >= physiomecaPlugin_ptr->startTime)
			p->var = 0;
		else
		{
//			if (b->phyAgeInit < physiomecaPlugin_ptr->cat1Threshold)
			{
//			p->var *= sqrt(physiomecaPlugin_ptr->siteAttenuationCoefficient);
//			p->var *= sqrt(physiomecaPlugin_ptr->cloneAttenuationCoefficient);
			p->var *= pow(physiomecaPlugin_ptr->siteAttenuationCoefficient, .25);
			p->var *= pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient, .25);
			}
		}
	}
	// remaining second state proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_st2
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st2
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_st2
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st2)
	{
		if (   light->v < physiomecaPlugin_ptr->ramificationThreshold
		    && top >= physiomecaPlugin_ptr->startTime)
			p->var = 0;
	}
	// transition not branched -> first state proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_0_1
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_0_1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_0_1
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_0_1)
	{
		if (   light->v < physiomecaPlugin_ptr->ramificationThreshold
		    && top >= physiomecaPlugin_ptr->startTime)
			p->var = 0;
		else
		{
//			if (b->phyAgeInit < physiomecaPlugin_ptr->cat1Threshold)
			{
//			p->var *= sqrt(physiomecaPlugin_ptr->siteAttenuationCoefficient);
//			p->var *= sqrt(physiomecaPlugin_ptr->cloneAttenuationCoefficient);
			p->var *= pow(physiomecaPlugin_ptr->siteAttenuationCoefficient, .25);
			p->var *= pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient, .25);
			}
		}
	}
	// transition first state -> not branched proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_1_0
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_1_0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_1_0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_1_0)
	{
		if (   light->v < physiomecaPlugin_ptr->ramificationThreshold
		    && top >= physiomecaPlugin_ptr->startTime)
			p->var = 1;
		else
		{
//			p->var *= 1/physiomecaPlugin_ptr->siteAttenuationCoefficient;
//			p->var *= 1/physiomecaPlugin_ptr->cloneAttenuationCoefficient;
//			if (b->phyAgeInit < physiomecaPlugin_ptr->cat1Threshold)
			{
//			p->var = 1 - (1-p->var) * sqrt(physiomecaPlugin_ptr->cloneAttenuationCoefficient * physiomecaPlugin_ptr->siteAttenuationCoefficient);
			p->var = 1 - (1-p->var) * pow(physiomecaPlugin_ptr->cloneAttenuationCoefficient * physiomecaPlugin_ptr->siteAttenuationCoefficient, .25);
			}
		}
	}
	// transition second state -> not branched proba
	else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_2_0
		 || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_2_0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_2_0
		 || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_2_0)
	{
		if (   light->v < physiomecaPlugin_ptr->ramificationThreshold
		    && top >= physiomecaPlugin_ptr->startTime)
			p->var = 1;
	}
	// living proba
	else if (p->numVar == rtpbvie)
	{
		if (   light->v > 0
		    && light->v < physiomecaPlugin_ptr->livingThreshold
		    && top >= physiomecaPlugin_ptr->startTime
			&& b->getBud()->ordrez > 0)
			p->var = 0;
		else
		{
//			p->var *= physiomecaPlugin_ptr->siteAttenuationCoefficient;
//			p->var *= physiomecaPlugin_ptr->cloneAttenuationCoefficient;
		}
	}
}
*/

/////////////////////////////////////////////////////
//
// This method get called for each new topology creation
//
// It stores UT global parameters
//
/////////////////////////////////////////////////////
void DecompPlugin::on_notify (const state& s, paramMsg * data)
{
    //Cast msg parameters
    paramDecompAxeLevel *p = (paramDecompAxeLevel *)data;

    //Retrieve branch
    DecompAxeLevel *I = p->d;

	if (s == SigDelDecompAxeLevel)
	{
		FMassData *d;
		if ((d = (FMassData*)I->getAdditionalData("mass")))
		{
			delete d;
			I->removeAdditionalData("mass");
		}
		return;
	}

    FMassData *L = new FMassData;
    L->mass = 0;
    I->addAdditionalData ("mass", L, VITISGLDSPRINTABLE);

}

/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{

    paramPlant * d = (paramPlant*)data; //Retrieve msg params

	physiomecaPlugin_ptr->setPlant (d->p);
	if (physiomecaPlugin_ptr->getPlant()->getConfigData().cfg_topoSimp > 1)
        physiomecaPlugin_ptr->getPlant()->getConfigData().cfg_topoSimp = 1;

	EventData *e = new EventData;
    Scheduler::instance()->create_process(physiomecaPlugin_ptr,e,1,100);
}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{

	physiomecaPlugin_ptr->setPlant (NULL);
}

void physiomecaPlugin::process_event(EventData *msg)
{
    if (plant == NULL)
        return;

	Scheduler *s = Scheduler::instance();

	/* sets the next external eventData */
	float top_extern = s->getTopClock();

    plant->computeGeometry();
    computeMass (top_extern);

    if (top_extern >= startTime)
        writeOPF (plant->getName());

    s->signal_event(this->getPid(),msg,top_extern,101);

}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
physiomecaPlugin::physiomecaPlugin(const std::string& f, const void *plant) {

    if (!initFParam (f))
    {
		cout << "Failed reading physiomeca parameter file " << f << endl;
		cout << "File sould look like this :" << endl<<endl;
		cout << "# WARNING"<<endl;
		cout << "# The syntax for this parameter file is not even"<<endl<<"# Try to keep the \"SingleValueParameter\" keyword as it is"<<endl;
		cout << "# just adjust the numerical values"<<endl<<endl;
		cout << "# density of pure conductive wood"<<endl<<"defaultDensity SingleValueParameter 0.1"<<endl<<endl;
		cout << "# minimum light for ramification"<<endl<<"ramificationThreshold SingleValueParameter 0.4"<<endl<<endl;
		cout << "# category 1 axis sensibility for light at ramification threshold"<<endl<<"cat1Attenuation SingleValueParameter 0.7"<<endl<<endl;
		cout << "# category 2 axis sensibility for light at ramification threshold"<<endl<<"cat2Attenuation SingleValueParameter 0.9"<<endl<<endl;
		cout << "# parameter attenuation due to site within the same clone"<<endl<<"siteAttenuationCoefficient SingleValueParameter 1"<<endl<<endl;
		cout << "# parameter attenuation between similar clones"<<endl<<"cloneAttenuationCoefficient SingleValueParameter 1"<<endl<<endl;
		cout << "# lightning model type (0 : isolated Tree, 1 : canopy tree, 2 : MIR)"<<endl<<"lightningModel SingleValueParameter 1"<<endl;
 		cout << endl<< "**** Failed reading light parameter file " << f << " ****" << endl;
		return;
	}
	this->plant = (PlantAMAP*)plant;
    //child -> parent binding PTR
/*
	l = new ParameterPlugin (this->plant);
    l->physiomecaPlugin_ptr = this;
*/
	d = new DecompPlugin (this->plant);
    d->physiomecaPlugin_ptr = this;
	p = new PlantPlugin (this->plant);
    p->physiomecaPlugin_ptr = this;
	pp = new PlantDeletePlugin (this->plant);
    pp->physiomecaPlugin_ptr = this;

}

bool physiomecaPlugin::initFParam (const std::string& f) {
	lightningModel = 2;
	ParamSet * param_H = new ParamSet(f);

	if (!param_H->Exists())
		return false;

	startTime = 1;
	if (param_H->getParameter("startTime"))
		startTime = param_H->getParameter("startTime")->value();
	else
	{
		cout << "Failed finding startTime parameter in file " << f << endl;
		cout << "Assuming startTime=1" << endl;
	}

	if (param_H->getParameter("cat1Threshold"))
		cat1Threshold = param_H->getParameter("cat1Threshold")->value();
	else
	{
		cout << "Failed finding cat1Threshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cat2Threshold"))
		cat2Threshold = param_H->getParameter("cat2Threshold")->value();
	else
	{
		cout << "Failed finding cat2Threshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("livingThreshold"))
		livingThreshold = param_H->getParameter("livingThreshold")->value();
	else
	{
		cout << "Failed finding livingThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("ramificationThreshold"))
		ramificationThreshold = param_H->getParameter("ramificationThreshold")->value();
	else
	{
		cout << "Failed finding ramificationThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cat1Attenuation"))
		cat1Attenuation = param_H->getParameter("cat1Attenuation")->value();
	else
	{
		cout << "Failed finding cat1Attenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cat2Attenuation"))
		cat2Attenuation = param_H->getParameter("cat2Attenuation")->value();
	else
	{
		cout << "Failed finding cat2Attenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("siteAttenuationCoefficient"))
		siteAttenuationCoefficient = param_H->getParameter("siteAttenuationCoefficient")->value();
	else
	{
		cout << "Failed finding siteAttenuationCoefficient parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cloneAttenuationCoefficient"))
		cloneAttenuationCoefficient = param_H->getParameter("cloneAttenuationCoefficient")->value();
	else
	{
		cout << "Failed finding cloneAttenuationCoefficient parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("lightningModel"))
		lightningModel = param_H->getParameter("lightningModel")->value();
	else
	{
		cout << "Failed finding lightningModel parameter in file " << f << endl;
		cout << "Assuming Archimed model" << endl;
	}

	switch (lightningModel)
	{
	case 0 :
		cout << "isolated tree simulation" << endl;
		break;
	case 1 :
		cout << "canopy tree simulation" << endl;
		break;
	case 2 :
		cout << "MIR light simulation" << endl;
		break;
	case 3 :
		cout << "voxel light simulation" << endl;
		break;
	}

	if (lightningModel == 3)
	{
        if (param_H->getParameter("voxelSize"))
            VoxelSpace::instance()->VoxelSize(param_H->getParameter("voxelSize")->value());
        else
        {
            cout << "Failed finding voxelSize parameter in file " << f << endl;
            VoxelSpace::instance()->VoxelSize(1);
            return false;
        }
	}

	if (lightningModel == 2)
	{
		if (param_H->getParameter("clearnessIndex"))
			clearnessIndex = param_H->getParameter("clearnessIndex")->value();
		else
		{
			cout << "Failed finding clearnessIndex parameter in file " << f << endl;
			return false;
		}
		if (param_H->getParameter("latitude"))
		{
			latitude = param_H->getParameter("latitude")->value();
			latitude *= M_PI / 180.;
		}
		else
		{
			cout << "Failed finding latitude parameter in file " << f << endl;
			return false;
		}
		if (param_H->getParameter("beginingDay"))
			beginingDay = param_H->getParameter("beginingDay")->value();
		else
		{
			cout << "Failed finding beginingDay parameter in file " << f << endl;
			return false;
		}
		if (param_H->getParameter("endingDay"))
			endingDay = param_H->getParameter("endingDay")->value();
		else
		{
			cout << "Failed finding endingDay parameter in file " << f << endl;
			return false;
		}

	}

	return true;
}

void physiomecaPlugin::computeMass (float top_extern){
}


// assuming upper point is at the top of the trunk
void physiomecaPlugin::computeUpperPoint(Vector3 *v)
{
	GeomBranc *geom;
	GeomElem *g;
	BrancAMAP *trunk;

	(*v)[2] = 0;
	for (int br=0; br<plant->getGeometry().getGeomBrancStack().size(); br++)
	{
		geom = plant->getGeometry().getGeomBrancStack().at(br);
		trunk = (BrancAMAP*)geom->getBranc();
		if (trunk->getAdditionalData("light") == 0)
			continue;

		if (trunk->phyAgeInit == 1)
		{
			g = geom->getElementsGeo().at(geom->getElementsGeo().size()-1);
			(*v).set(g->getMatrix().getMainVector());
			(*v) *= g->getLength();
			(*v) +=(g->getMatrix().getTranslation());
			break;
		}
	}
}

float physiomecaPlugin::computeCenterPoint (Vector3 *v)
{
	GeomBranc *geom;
	GeomElem *g;
	Branc *b;
	Vector3 curPoint;
	float f, sphereRadius=0;

	// assume center point is at half tree height
	computeUpperPoint (v);
	(*v) *= 0.5;

	for (int br=0; br<plant->getGeometry().getGeomBrancStack().size(); br++)
	{
		geom = plant->getGeometry().getGeomBrancStack().at(br);
		b = geom->getBranc();

		if (b->getAdditionalData ("light") == 0)
			continue;

		geom = plant->getGeometry().getGeomBrancStack().at(br);
		g = geom->getElementsGeo().at(geom->getElementsGeo().size()-1);

		curPoint.set(g->getMatrix().getMainVector());
		curPoint *= g->getLength();
		curPoint +=(g->getMatrix().getTranslation());
		curPoint -= (*v);
		f = curPoint.normalize();
		if (f > sphereRadius)
			sphereRadius = f;
	}

	return sphereRadius;
}


BrancAMAP* physiomecaPlugin::findBranc(int id)
{
	BrancAMAP* b;
	b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
	while (b)
	{
		if (b->getId() == id)
		{
			return b;
		}
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}
}


void physiomecaPlugin::writeOPF(const std::string &finame)
{
	int i;

	std::stringstream sout;
	sout << finame;
	std::string fname (sout.str());

	GldsFormat *outFormat= new GldsFormat(fname);

	outFormat->initFormat(&(plant->getGeometry()));

	std::cout<<"F Output Geometry opf to : "  << fname << ".opf" << std::endl;

	if (plant->getGeometry().getGeomBrancStack().size())
	{
		outFormat->printData(plant->getGeometry().getGeomBrancStack().at(0));
	}
	else
	{
		std::cout<<"WARNING : -------> Empty Geometry"<<std::endl;
	}

	((GldsFormat*)outFormat)->closeFormat();

	delete outFormat;
}

