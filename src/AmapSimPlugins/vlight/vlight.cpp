/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "Output.h"
#include "PlantAMAP.h"
#include "ObserverManager.h"

#ifdef WIN32
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "AmapSimMod.h"
#include "PlantAMAP.h"
#include "GeomBrancAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "externvlight.h"
#include "vlight.h"
#define STATIC_LIB
#include "lightThroughVoxels.h"
#include "GldsFormat.h"
#undef STATIC_LIB


void LengthPlugin::on_notify (const state& , paramMsg * data)
{

    //Cast message parameters
    paramElemGeom * p = (paramElemGeom*)data;

    //Retrieve branch
    BrancAMAP *b = (BrancAMAP*)p->gec->getPtrBrc()->getBranc();

	if (b->phyAgeInit > vlightplugin_ptr->WoodMaxPhyAge())
		return;

	if (b->getBud()->ordrez >= 2)
		return;

	float *length;
//	if ((length=(float*)b->getCurrentEntn()->getAdditionalData("length")) == 0)
	if (b->getCurrentGuIndex() == b->getGuNumber()-1)
	{
		length = new float;
		// trunk gets higher
		if (b->getBud()->ordrez == 0)
			p->v *= (1. + vlightplugin_ptr->MortalityLevel()*vlightplugin_ptr->MortalitySensivity());
		// branches get smaller
		else
			p->v *= (1. - vlightplugin_ptr->MortalityLevel()*vlightplugin_ptr->MortalitySensivity());
		*length = p->v;
		if (b->getCurrentEntn()->getAdditionalData("length") == 0)
			b->getCurrentEntn()->addAdditionalData ("length", length);
		else
			b->getCurrentEntn()->modifyAdditionalData ("length", length);
	}
	else
	{
		length=(float*)b->getCurrentEntn()->getAdditionalData("length");
		p->v = *length;
	}

}

/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{

	paramPlant * d = (paramPlant*)data; //Retrieve msg params

	vlightplugin_ptr->setPlant (d->p);

	// to avoid complexity due to simplication
//	d->p->getConfigData().cfg_topoSimp = 0;
//	d->p->getConfigData().cfg_geoSimp = 0;

	VlightEventData *e = new VlightEventData(2);
	Scheduler::instance()->create_process(vlightplugin_ptr,e,1,103);
}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{
	vlightplugin_ptr->setPlant (NULL);
}

void vlightPlugin::process_event(EventData *msg)
{
    if (plant == NULL)
        return;

	VlightEventData *e = (VlightEventData*)msg;

	Scheduler *s = Scheduler::instance();
	float top_extern = s->getTopClock();

	if (e->Action() == 1)
	{
cout << "Voxel space cleaning at " << top_extern << endl;
		// clean voxel space at the end of the step
		voxelSpace->resetContent (plant);
		plant->getGeometry().clear();


		// register for next browsingmap step
		e->setAction (2);
		s->signal_event(this->getPid(),e,s->getTopClock()+1,103);
	}
	else if (e->Action() == 2)
	{

cout << "begin vlight mapping at " << top_extern<<" for "<<plant->getName();
		plant->computeGeometry();

//voxelSpace->displayContent();
		computeOccupationMap (top_extern);
//voxelSpace->displayContent();

		// register for vlight computing
		e->setAction (3);
		s->signal_event(this->getPid(),e,s->getTopClock(),101);
	}
	else if (e->Action() == 3)
	{

cout << "begin vlight at " << top_extern<< " for "<<plant->getName()<<endl;
		//stopBrancs();
		integrateLight();
		if (outputColors)
            writeOPF (plant->getName());

cout << "end vlight at " << top_extern <<endl;

		// register for cleaning at the end of the step
		e->setAction (1);
		s->signal_event(this->getPid(),e,s->getTopClock(),101);
	}
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
vlightPlugin::vlightPlugin(const std::string& f, const void *plant) {

	voxelSpace = VoxelSpace::instance();

    outputColors=false;
	if (!initvlightParam (f))
	{
		cout << "Failed reading vlight parameter file " << f << endl;
		cout << "File sould look like this :" << endl<<endl;
 		cout << endl<< "**** Failed reading vlight parameter file " << f << " ****" << endl;

		return;
	}

     computeColors();

	this->plant = (PlantAMAP*)plant;
	p = new PlantPlugin (this->plant);
	p->vlightplugin_ptr = this;
	pp = new PlantDeletePlugin (this->plant);
	pp->vlightplugin_ptr = this;
	l = new LengthPlugin (this->plant);
	l->vlightplugin_ptr = this;

	srand(this->plant->getConfigData().randSeed);

	mortalityLevel = 0;
}


void vlightPlugin::stopBrancs ()
{
	int i, j, k;
	Vector3 pos = plant->getPosition();
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	Voxel *v;
	GeomElem *e,*e1;
	int nbBrancs=0;
	int nbStoppedBrancs= 0;

	// for each branch, check if there is another plant in the voxel containing the bud
	for (vector<GeomBranc *>::iterator ib=brcs.begin(); ib!=brcs.end(); ib++)
	{
		GeomBranc *g = *ib;
		vector<GeomElem *> ges = g->getElementsGeo ();
		e = g->getElementsGeo().at(g->getElementsGeo().size()-1);

		BrancAMAP *b = (BrancAMAP*)e->getPtrBrc()->getBranc();
		if (b->phyAgeInit > woodMaxPhyAge)
			continue;

		nbBrancs++;

		if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()+pos[0],
		       e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()+pos[1],
		       e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength(),
		       &i, &j, &k))
		{
			v = voxelSpace->addrAt (i, j, k);
			for (vector<GeomElemCone*>::iterator it=v->content.begin(); it != v->content.end(); it++)
			{
				e1 = (GeomElemCone*)*it;
				if (   e1->getPtrBrc()->getBranc()->getPlant() != plant
                                    && !b->getBud()->death
				    && b->getBud()->getPhyAge() >= ((BrancAMAP*)(e1->getPtrBrc()->getBranc()))->getBud()->getPhyAge())
				{
					// stop current bud growth
					b->getBud()->death = 1;
					b->getBud()->deathInstant = Scheduler::instance()->getTopClock();
					b->getBud()->toBePruned = TOTALPRUNING;
					// compute the self pruning moment
					float lagdeath = 9999;
					float lagend = 9999;
					float pruningInstant = Scheduler::instance()->getTopClock();
					lagdeath = b->getBud()->axisref->val1DParameter (rtlagdeath, (int) b->getBud()->getPhyAge(), b);
					lagend = b->getBud()->axisref->val1DParameter (rtlagend, (int) b->getBud()->getPhyAge(), b);
					lagend += (float)(2.*EPSILON);
					if (lagend < lagdeath)
						pruningInstant += lagend;
					else
						pruningInstant += lagdeath;
					int aid = Scheduler::instance()->getActionIndex (b->getBud()->process_id);
					Action ai = Scheduler::instance()->getAction (b->getBud()->process_id);
					Scheduler::instance()->changeAction (aid, ai.pid, pruningInstant, ai.priority, NULL);
					nbStoppedBrancs++;
					break;
				}
			}
		}
	}

	if (nbBrancs > 0)
	{
		mortalityLevel = nbStoppedBrancs;
		mortalityLevel /= nbBrancs;
	}
	else
		mortalityLevel = 0;

cout << plant->getName() << " mortalityLevel : " << mortalityLevel << endl;
}

void vlightPlugin::integrateLight ()
{
	int i, j, k, index;
	Vector3 pos = plant->getPosition();
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	Voxel *v;
	GeomElemCone *e;
	BrancAMAP *b, *bearer;
	Bud *bud;
	InterNodeAMAP *entn;
    GeomBrancAMAP *g;
    LightData *d;
    float light;
    ColorData *c;
    FloatData *l;
    float maxLight=0;

    // reset intercepted light
	b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
	while (b)
	{
		if ((l = (FloatData*)b->getAdditionalData("light")) == NULL)
		{
            l = new FloatData;
            b->addAdditionalData ("light", l, VITISGLDSPRINTABLE);
 		}
        l->val = 0;
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}


	// for each organ, integrate light intercepted by shoot and give color associated to voxels opacity
	for (vector<GeomBranc *>::iterator ib=brcs.begin(); ib!=brcs.end(); ib++)
	{
		g = (GeomBrancAMAP*)*ib;

        b = (BrancAMAP*)g->getBranc();
        b->startPosit();

		vector<GeomElem *> ges = g->getElementsGeo ();

		for (vector<GeomElem *>::iterator it=ges.begin(); it != ges.end(); it++)
		{
		    e = (GeomElemCone*)*it;
            if (voxelSpace->findVoxel ( e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()+pos[0],
                                        e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()+pos[1],
                                        e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength()+pos[2],
                                        &i, &j, &k))
            {
                v = voxelSpace->addrAt (i, j, k);
                d = (LightData*)v->getVoxelData("light");
                light = d->light;
                b->positOnEntnWithinBrancAt(e->getPosInBranc());
                entn = b->getCurrentEntn();
                if (b->phyAgeInit >= woodMaxPhyAge)
                {
                    bearer = b->getBearer();
                    l = (FloatData*)b->getAdditionalData("light");
                    l->val = light * e->getLength() * e->getBottomDiam();
                    if (maxLight < l->val)
                        maxLight = l->val;
                    l = (FloatData*)bearer->getAdditionalData("light");
                    if (l->val < light * e->getLength() * e->getBottomDiam())
                        l->val = light * e->getLength() * e->getBottomDiam();
                }
                if (outputColors)
                {
                    if ((c=(ColorData*)e->getAdditionalData("Color")) == NULL)
                    {
                        c = new ColorData();
                        e->addAdditionalData ("Color", c, VITISGLDSPRINTABLE);
                    }
                    index = light * 499;
                    if (index > 499)
                        index = 499;
                    if (index < 0)
                        index = 0;
                    c->set (color[index]);
                    e->modifyAdditionalData ("Color", c, VITISGLDSPRINTABLE);
                }
            }
		}

		b->endPosit();

	}

    // normalize intercepted light
	b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
	while (b)
	{
		l = (FloatData*)b->getAdditionalData("light");
        l->val /= maxLight;
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}

cout << plant->getName() << " mortalityLevel : " << mortalityLevel << endl;
}

void vlightPlugin::computeOccupationMap (float top_extern){

	float voxelSize = voxelSpace->VoxelSize();

	// compute bounding box dimensions
	Vector3 min = plant->getGeometry().getMin();
	Vector3 max = plant->getGeometry().getMax();
	Vector3 pos = plant->getPosition();

	int x = ceil (max[0] / voxelSize + 0.5) - floor (min[0] / voxelSize - 0.5);
	int y = ceil (max[1] / voxelSize + 0.5) - floor (min[1] / voxelSize - 0.5);
	int z = ceil (max[2] / voxelSize + 0.5) - floor (min[2] / voxelSize - 0.5);
	int indXOrigin = floor ((pos[0]+min[0]) / voxelSize - 0.5);
	int indYOrigin = floor ((pos[1]+min[1]) / voxelSize - 0.5);
	int indZOrigin = floor ((pos[2]+min[2]) / voxelSize - 0.5);
	float xOrig = indXOrigin * voxelSize;
	float yOrig = indYOrigin * voxelSize;
	float zOrig = indZOrigin * voxelSize;

	voxelSpace->adjust (xOrig, yOrig, x, y, z);

	int zMax = 0;
	zMax = 0;
	int zMin = voxelSpace->ZSize();
	int ib, jb, kb;
	int ie, je, ke;
	GeomElemCone *e;
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	vector<Voxel*> vv;
	for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
		GeomBranc *g = *b;

		BrancAMAP *br = (BrancAMAP*)g->getBranc();
		if (br->phyAgeInit > woodMaxPhyAge)
			continue;

		vector<GeomElem *> ges = g->getElementsGeo ();

		for (vector<GeomElem *>::iterator gj=ges.begin(); gj!=ges.end(); gj++)
		{
			vv.clear();
			e = (GeomElemCone*)*gj;
			if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+pos[0],
                                                   e->getMatrix().getTranslation().y()+pos[1],
                                                   e->getMatrix().getTranslation().z(),
                                                   &ib, &jb, &kb))
			{
				if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()+pos[0],
				                           e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()+pos[1],
				                           e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength(),
				                           &ie, &je, &ke))
				{
					if (ib!=ie || jb!=je || kb!=ke)
					{
						voxelSpace->bresenham3D (ib, jb, kb, ie, je, ke, &vv);
					}
					else
						vv.push_back(voxelSpace->addrAt(ib, jb, kb));

					for (vector<Voxel*>::iterator it=vv.begin(); it!=vv.end(); it++)
					{
						Voxel *v = *it;
						v->content.push_back(e);
					}
				}
			}
		}
	}

	voxelSpace->NbPlant(voxelSpace->NbPlant()+1);
}

bool vlightPlugin::initvlightParam (const std::string& f) {
	ParamSet * param_H = new ParamSet(f);

	if (!param_H->Exists())
		return false;

	if (param_H->getParameter("voxelSize"))
		voxelSpace->VoxelSize(param_H->getParameter("voxelSize")->value());
	else
	{
		cout << "Failed finding voxelSize parameter in file " << f << endl;
		voxelSpace->VoxelSize(1);
		return false;
	}

	if (param_H->getParameter("woodMaxPhyAge"))
		woodMaxPhyAge = param_H->getParameter("woodMaxPhyAge")->value();
	else
	{
		cout << "Failed finding woodMaxPhyAge parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("mortalitySensivity"))
		mortalitySensivity = param_H->getParameter("mortalitySensivity")->value()/100;
	else
		mortalitySensivity = 0.;

	if (param_H->getParameter("outputColors"))
	{
		if (param_H->getParameter("outputColors")->value() != 0)
            outputColors = true;
	}

	return true;
}

void vlightPlugin::computeColors()
{
    float coeff;
    ostringstream convert;
    for (int i=0; i<166; i++)
    {
        coeff = i;
        coeff /= 166;
        convert.str("");
        convert << "Color "<<(int)(coeff * 255)<<" 255 0";
        color[i] = convert.str();
        convert.str("");
        convert << "Color 255 "<<255+(int)(coeff * (165-255))<<" 0";
        color[166+i] = convert.str();
        convert.str("");
        convert <<"Color 255 "<<165+(int)(coeff * (-165))<<" 0";
        color[332+i] = convert.str();
    }
    color[498] = color[497];
    color[499] = color[497];
}

void vlightPlugin::writeOPF(const std::string &finame)
{
	int i;

	std::stringstream sout;
	sout << finame;
	std::string fname (sout.str());

	GldsFormat *outFormat= new GldsFormat(fname);

	outFormat->initFormat(&(plant->getGeometry()));

	std::cout<<"vlight Output Geometry opf to : "  << fname << ".opf" << std::endl;

	if (plant->getGeometry().getGeomBrancStack().size())
	{
		outFormat->printData(plant->getGeometry().getGeomBrancStack().at(0));
	}
	else
	{
		std::cout<<"WARNING : -------> Empty Geometry"<<std::endl;
	}

	((GldsFormat*)outFormat)->closeFormat();

	delete outFormat;
}


