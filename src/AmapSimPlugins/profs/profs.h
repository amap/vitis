/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "PlantAMAP.h"


struct profsPlugin;

//struct PROFS_EXPORT ParameterPlugin:public subscriber<messageAxeRef> {
//	profsPlugin * profsplugin_ptr;
//
//	ParameterPlugin (Plant *p) {subscribe(p);};
//	virtual ~ParameterPlugin () {unsubscribe();};
//	void on_notify (const state& st, paramMsg * data=NULL);
//};

struct PROFS_EXPORT LengthPlugin:public subscriber<messageVitisElemLength> {
	profsPlugin * profsplugin_ptr;

	LengthPlugin (void *p) { subscribe(p);};
	virtual ~LengthPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
	double length (double volume, float ShapeA, float ShapeB);
};

struct PROFS_EXPORT PlantPlugin:public subscriber<messageVitisPlant> {
	profsPlugin * profsplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct PROFS_EXPORT PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	profsPlugin * profsplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};



struct couple {
	couple () {phy=0;min=0;max=1;};
	couple (int _phy, float _x, float _y) {min=_x;max=_y;phy=_phy;};
	int phy;
	float min;
	float max;
};

enum TEMPERATURE {BASSE, MOYENNE, BONNE};
enum PLUIE {PEU, MOYEN, BEAUCOUP};

struct sigmoidRange {
	double min, max, slope;
};

class PROFS_EXPORT profsPlugin : public VProcess
{
public :
	profsPlugin(const std::string& f, void *p);
	virtual ~profsPlugin(){;};
	void process_event(EventData *msg);
	PlantAMAP *getPlant(){return plant_ptr;};
	void setPlant(PlantAMAP *p){plant_ptr=p;};
	bool initData();
	double sigmoid (double x, double min, double max, double slope);
	string ParameterFile();
	void adjustParameters (int temperature, int pluie, int climate);
	void initParameters (double fpolycyclisme, double fpousse, double framif, double ffeuille);
	vector<PLUIE> Pluie(){return pluie;};
	vector<TEMPERATURE> Temperature(){return temperature;};
	vector<int> Climate(){return climate;};
	vector<couple>  OrganLength(){return organLength;};

private:
	PlantPlugin *p;
	PlantDeletePlugin *pp;
	LengthPlugin *l;
	PlantAMAP * plant_ptr;

	vector<TEMPERATURE> temperature;
	int currentTemperature;
	vector<PLUIE> pluie;
	int currentPluie;
	string parameterFile;
	vector<int> climate;
	int currentClimate;

	// shoot entn number
	vector<couple> neopn1;
	vector<couple> neopn2;
	vector<couple> neopna;

	vector<couple> neon1;
	vector<couple> neon2;
	vector<couple> neona;

	// organ size
	vector<couple>  organLength;

	// polyclysm
	vector<couple> polyini;
	vector<couple> monocyc;
	vector<couple> polycyc;
	vector<couple> twocyc;
	vector<couple> threecyc;
	vector<couple> fourcyc;

	// branching
	vector<couple> pbaxilreit;
	vector<couple> pbaxil;

};
extern "C" PROFS_EXPORT  profsPlugin * StartPlugin (const std::string& f, void *p)
{
	std::cout << "loading profsPlugin module with parameter " << f << std::endl;

	return new profsPlugin (f, p) ;
}
