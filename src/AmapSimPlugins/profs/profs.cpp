/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <math.h>
#include <string.h>
#include "Output.h"

#include <iostream>
#include <fstream>
using namespace std;

#include "PlantAMAP.h"
#include "InterNodeAMAP.h"
#include "Topology.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "externProfs.h"
#include "profs.h"






void LengthPlugin::on_notify (const state& st, paramMsg *data)
{
    //Cast message parameters
   paramElemGeom * p = (paramElemGeom*)data;

	//Retrieve branch
	BrancAMAP * b = (BrancAMAP *)p->gec->getPtrBrc()->getBranc();

	GrowthUnitAMAP * gu = (GrowthUnitAMAP *)b->getCurrentElementOfSubLevel(GROWTHUNIT);
	InterNodeAMAP * e = (InterNodeAMAP *)b->getCurrentElementOfSubLevel(INTERNODE);
	int top = (int)e->instant;
//cout << "lngInit phy " << gu->phyAgeGu << " birth " << e->instant << " top "<<top<<" pluie " << profsplugin_ptr->Pluie()[top]-1 << " T "<<profsplugin_ptr->Temperature()[top]-1<< " clim "<<profsplugin_ptr->Climate()[top]<<endl;
	int eau = profsplugin_ptr->Pluie()[top]-1 + profsplugin_ptr->Climate()[top];
	if (eau < -4)
		eau = -4;
	if (eau > 4)
		eau = 4;
//float v = profsplugin_ptr->sigmoid(eau, profsplugin_ptr->OrganLength()[top].min, profsplugin_ptr->OrganLength()[top].max, 1.);
	float v = profsplugin_ptr->sigmoid(eau, 0, 2, 1.);
//cout << "initial " << p->v << "  final " << p->v*v << endl;
	p->v *= v;

}

/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{

   paramPlant * d = (paramPlant*)data; //Retrieve msg params

	profsplugin_ptr->setPlant ((PlantAMAP*)d->p);

	if (!profsplugin_ptr->initData())
	{
		std::cout << "error loading parameter file : " << profsplugin_ptr->ParameterFile() << std::endl;
		return;
	}

	Scheduler::instance()->create_process(profsplugin_ptr,NULL,0,100);
//	Scheduler::instance()->create_process(profsplugin_ptr,(EventData*)d->p,Scheduler::instance()->getHotStop(),1);

//	profsplugin_ptr->process_event(NULL);
}
void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{
	profsplugin_ptr->setPlant (NULL);
}

bool profsPlugin::initData()
{
	currentTemperature = 0;
	currentPluie = 0;
	currentClimate = 0;

	fstream  *fic = new fstream (parameterFile.c_str(), ios::in);
	if (!fic->is_open())
	{
		delete fic;
		return false;
	}

	string type;
	double polycyclisme=0, pousse=0, ramif=0, organ=0;
	char str[2056];

	while (!fic->eof())
	{
		*fic >> type;
		if (type[0] == '#')
		{
			;
		}
		else if (type == "TEMPERATURE-PLUIE")
		{
			TEMPERATURE c;
			PLUIE p;
			string cl;
			*fic >> cl;
			if (cl == "BONNE")
				c = BONNE;
			else if (cl == "MOYENNE")
				c = MOYENNE;
			else if (cl == "BASSE")
				c = BASSE;
			else if (cl.length() > 0)
			{
				cout << "type de temperature inconnu " << cl << " (BONNE, MOYENNE, BASSE)" << endl;
				fic->close();
				delete fic;
#ifdef WIN32
	Sleep(1000);
#endif
				return false;
			}
			*fic >> cl;
			if (cl == "PEU")
				p = PEU;
			else if (cl == "MOYEN")
				p = MOYEN;
			else if (cl == "BEAUCOUP")
				p = BEAUCOUP;
			else if (cl.length() > 0)
			{
				cout << "type de pluie inconnu " << cl << " (PEU, MOYEN, BEAUCOUP)" << endl;
				fic->close();
				delete fic;
#ifdef WIN32
	Sleep(1000);
#endif
				return false;
			}

			temperature.push_back(c);
			pluie.push_back(p);
			if (   c-1 < 0
			    && p-1 < 0)
			{
				currentClimate -=2;
				if (currentClimate < -4)
					currentClimate = -4;
			}
			if (   c-1 > 0
			    && p-1 > 0)
			{
				currentClimate +=2;
				if (currentClimate > 4)
					currentClimate = 4;
			}
			else
			{
				if (currentClimate < 0)
					currentClimate ++;
				else if (currentClimate > 0)
					currentClimate --;
			}
			climate.push_back(currentClimate);
		}
		else if (type == "POLYCYCLISME")
		{
			*fic >> polycyclisme;
		}
		else if (type == "POUSSE")
		{
			*fic >> pousse;
		}
		else if (type == "RAMIF")
		{
			*fic >> ramif;
		}
		else if (type == "ORGAN")
		{
			*fic >> organ;
		}
		else if (type.length() > 0)
		{
			cout << "mot cle inconnu " << type << " (TEMPERATURE-PLUIE,POLYCLISME,POUSSE,RAMIF,ORGAN)" << endl;
			fic->close();
			delete fic;
#ifdef WIN32
	Sleep(1000);
#endif
			return false;
		}

		// vider la ligne
		fic->getline(str, 2055);
	}

	fic->close();
	delete fic;

	initParameters (polycyclisme, pousse, ramif, organ);

//	process_event(NULL);

	return true;
}

void profsPlugin::initParameters (double fpolycyclisme, double fpousse, double framif, double forgan)
{
	AmapSimVariable a;
	AxisReference * axeRef = (AxisReference*)plant_ptr->getParamData(string("AMAPSimMod"));

	// shoot length
	for (int i=0; i<(*axeRef)[rtneopn1].nb_pos_current; i++)
	{
		neopn1.push_back(couple((*axeRef)[rtneopn1].currentX[i], (*axeRef)[rtneopn1].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneopn1].currentY[i]*(1+fpousse/100.)));
	}
	for (int i=0; i<(*axeRef)[rtneopn2].nb_pos_current; i++)
	{
		neopn2.push_back(couple((*axeRef)[rtneopn2].currentX[i], (*axeRef)[rtneopn2].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneopn2].currentY[i]*(1+fpousse/100.)));
	}
	for (int i=0; i<(*axeRef)[rtneopna].nb_pos_current; i++)
	{
		neopna.push_back(couple((*axeRef)[rtneopna].currentX[i], (*axeRef)[rtneopna].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneopna].currentY[i]*(1+fpousse/100.)));
	}
	for (int i=0; i<(*axeRef)[rtneon1].nb_pos_current; i++)
	{
		neon1.push_back(couple((*axeRef)[rtneon1].currentX[i], (*axeRef)[rtneon1].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneon1].currentY[i]*(1+fpousse/100.)));
	}
	for (int i=0; i<(*axeRef)[rtneon2].nb_pos_current; i++)
	{
		neon2.push_back(couple((*axeRef)[rtneon2].currentX[i], (*axeRef)[rtneon2].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneon2].currentY[i]*(1+fpousse/100.)));
	}
	for (int i=0; i<(*axeRef)[rtneona].nb_pos_current; i++)
	{
		neona.push_back(couple((*axeRef)[rtneona].currentX[i], (*axeRef)[rtneona].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneona].currentY[i]*(1+fpousse/100.)));
	}

	double min, max;
	// shoot polycylism
	for (int i=0; i<(*axeRef)[rtpolyini].nb_pos_current-1; i++)
	{
		min = (*axeRef)[rtpolyini].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rtpolyini].currentY[i]*(1+fpolycyclisme/100.);
		if (max > 1)
		{
			min = max + min - 1;
			max = 1;
		}
		polyini.push_back(couple((*axeRef)[rtpolyini].currentX[i], min, max));
	}
	for (int i=0; i<(*axeRef)[rtmonocyc].nb_pos_current-1; i++)
	{
		min = (*axeRef)[rtmonocyc].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rtmonocyc].currentY[i]*(1+fpolycyclisme/100.);
		if (max > 1)
		{
			min = max + min - 1;
			max = 1;
		}
		monocyc.push_back(couple((*axeRef)[rtmonocyc].currentX[i], min, max));
	}
	for (int i=0; i<(*axeRef)[rtpolycyc].nb_pos_current-1; i++)
	{
		min = (*axeRef)[rtpolycyc].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rtpolycyc].currentY[i]*(1+fpolycyclisme/100.);
		if (max > 1)
		{
			min = max + min - 1;
			max = 1;
		}
		polycyc.push_back(couple((*axeRef)[rtpolycyc].currentX[i], min, max));
	}
	for (int i=0; i<(*axeRef)[rt2cyc].nb_pos_current; i++)
	{
		min = (*axeRef)[rt2cyc].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rt2cyc].currentY[i]*(1+fpolycyclisme/100.);
		if (max > 1)
		{
			min = max + min - 1;
			max = 1;
		}
		twocyc.push_back(couple((*axeRef)[rt2cyc].currentX[i], min, max));
	}
	for (int i=0; i<(*axeRef)[rt3cyc].nb_pos_current; i++)
	{
		min = (*axeRef)[rt3cyc].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rt3cyc].currentY[i]*(1+fpolycyclisme/100.);
		if (max > 1)
		{
			min = max + min - 1;
			max = 1;
		}
		threecyc.push_back(couple((*axeRef)[rt3cyc].currentX[i], min, max));
	}
	for (int i=0; i<(*axeRef)[rt4cyc].nb_pos_current; i++)
	{
		min = (*axeRef)[rt4cyc].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rt4cyc].currentY[i]*(1+fpolycyclisme/100.);
		if (max > 1)
		{
			min = max + min - 1;
			max = 1;
		}
		fourcyc.push_back(couple((*axeRef)[rt4cyc].currentX[i], min, max));
	}

	// organLength
	for (int i=0; i<(*axeRef)[lnginit].nb_pos_current; i++)
	{
		min = (*axeRef)[lnginit].currentY[i]*(1-forgan/100.);
		max = (*axeRef)[lnginit].currentY[i]*(1+forgan/100.);
		organLength.push_back(couple((*axeRef)[lnginit].currentX[i], min, max));
	}

	// branching
	int index = NB_VAR+repbaxil;
	for (int i=0; i<(*axeRef)[index].nb_pos_current; i++)
	{
		min = (*axeRef)[index].currentY[i]*(1-framif/100.);
		max = (*axeRef)[index].currentY[i]*(1+framif/100.);
		if (max > 1)
		{
			min = max + min - 1;
			max = 1;
		}
		pbaxil.push_back(couple((*axeRef)[index].currentX[i], min, max));
	}
	index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+repbaxil;
	for (int i=0; i<(*axeRef)[index].nb_pos_current; i++)
	{
		min = (*axeRef)[index].currentY[i]*(1-framif/100.);
		max = (*axeRef)[index].currentY[i]*(1+framif/100.);
		if (max > 1)
		{
			min = max + min - 1;
			max = 1;
		}
		pbaxilreit.push_back(couple((*axeRef)[index].currentX[i], min, max));
	}
}

double profsPlugin::sigmoid (double x, double min, double max, double slope)
{
	double y;

	// divide by 8 to have mean value for x = 4, which means 4 years memory
	y = min + (max - min) / (exp(-slope / 4. * x) + 1);

	return y;
}

void profsPlugin::process_event(EventData *msg)
{
    if (plant_ptr == NULL)
        return;

	int top_extern;
	Scheduler *s = Scheduler::instance();
	/* sets the next external eventData */
	top_extern = s->getTopClock();

	//cout << "profs a " << s->getTopClock() << endl;

	if (top_extern >= temperature.size())
	{
		currentTemperature = temperature[temperature.size()-1]-1;
		currentPluie = pluie[temperature.size()-1]-1;
	}
	else
	{
		currentTemperature = temperature[top_extern]-1;
		currentPluie = pluie[top_extern]-1;
	}

	if (   currentTemperature == 1
	    && currentPluie == 1)
	{
		currentClimate += 2;
		if (currentClimate > 4)
			currentClimate = 4;
	}
	else if (   currentTemperature == -1
		 && currentPluie == -1)
	{
		currentClimate -= 2;
		if (currentClimate < -4)
			currentClimate = -4;
	}
	else
	{
		if (currentClimate < 0)
			currentClimate ++;
		if (currentClimate > 0)
			currentClimate --;
	}

cout << "adjust parameters avec temp "<<currentTemperature<< " pluie "<<currentPluie<<" climat "<<currentClimate<<endl;
	adjustParameters (currentTemperature, currentPluie, currentClimate);

	if (s->getTopClock()+1 <= s->getHotStop())
	{
		s->signal_event(this->getPid(),(EventData*)msg,s->getTopClock()+1,100);
	}
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
profsPlugin::profsPlugin(const std::string& f, void *ppp)
{

	plant_ptr = (PlantAMAP *)ppp;

    //child -> parent binding PTR
	p = new PlantPlugin (ppp);
    p->profsplugin_ptr = this;
	pp = new PlantDeletePlugin (ppp);
    pp->profsplugin_ptr = this;

	l = new LengthPlugin (this->plant_ptr);
    l->profsplugin_ptr = this;

	parameterFile = f;
	this->plant_ptr = (PlantAMAP*)p;
}

string profsPlugin::ParameterFile()
{
	return parameterFile;
}

void profsPlugin::adjustParameters (int temperature, int pluie, int climate)
{
	int t, p;
	t = climate + temperature;
	if (t > 4)
		t = 4;
	if (t < -4)
		t = -4;
	p = climate + pluie;
	if (p > 4)
		p = 4;
	if (p < -4)
		p = -4;

	AxisReference * axeRef = (AxisReference*)plant_ptr->getParamData(string("AMAPSimMod"));
	// shoot length
	for (int i=0; i<(*axeRef)[rtneopn1].nb_pos_current; i++)
	{
		(*axeRef)[rtneopn1].set(neopn1[i].phy, sigmoid(t, neopn1[i].min, neopn1[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtneopn2].nb_pos_current; i++)
	{
		(*axeRef)[rtneopn2].set(neopn2[i].phy, sigmoid(t, neopn2[i].min, neopn2[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtneopna].nb_pos_current; i++)
	{
		(*axeRef)[rtneopna].set(neopna[i].phy, sigmoid(t, neopna[i].min, neopna[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtneon1].nb_pos_current; i++)
	{
		(*axeRef)[rtneon1].set(neon1[i].phy, sigmoid(t, neon1[i].min, neon1[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtneon2].nb_pos_current; i++)
	{
		(*axeRef)[rtneon2].set(neon2[i].phy, sigmoid(t, neon2[i].min, neon2[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtneona].nb_pos_current; i++)
	{
		(*axeRef)[rtneona].set(neona[i].phy, sigmoid(t, neona[i].min, neona[i].max, 1.));
	}

	// shoot polycylism
	for (int i=0; i<(*axeRef)[rtpolyini].nb_pos_current-1; i++)
	{
		(*axeRef)[rtpolyini].set(polyini[i].phy, sigmoid(t, polyini[i].min, polyini[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtmonocyc].nb_pos_current-1; i++)
	{
		(*axeRef)[rtmonocyc].set(monocyc[i].phy, sigmoid(-t, monocyc[i].min, monocyc[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtpolycyc].nb_pos_current-1; i++)
	{
		(*axeRef)[rtpolycyc].set(polycyc[i].phy, sigmoid(t, polycyc[i].min, polycyc[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rt2cyc].nb_pos_current; i++)
	{
		(*axeRef)[rt2cyc].set(twocyc[i].phy, sigmoid(-t, twocyc[i].min, twocyc[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rt3cyc].nb_pos_current; i++)
	{
		(*axeRef)[rt3cyc].set(threecyc[i].phy, sigmoid(-t, threecyc[i].min, threecyc[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rt4cyc].nb_pos_current; i++)
	{
		(*axeRef)[rt4cyc].set(fourcyc[i].phy, sigmoid(t, fourcyc[i].min, fourcyc[i].max, 1.));
	}

	// organLength -> to be done at geometry computing, taking into account the birthtime
//	for (int i=0; i<(*axeRef)[lnginit].nb_pos_current; i++)
//	{
//		(*axeRef)[lnginit].set(organLength[i].phy, sigmoid(p, organLength[i].min, organLength[i].max, 1.));
//	}

	// branching
	int index = NB_VAR+repbaxil;
	for (int i=0; i<(*axeRef)[index].nb_pos_current; i++)
	{
		(*axeRef)[index].set(pbaxil[i].phy, sigmoid(t, pbaxil[i].min, pbaxil[i].max, 1.));
	}
	index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+repbaxil;
	for (int i=0; i<(*axeRef)[index].nb_pos_current; i++)
	{
		(*axeRef)[index].set(pbaxilreit[i].phy, sigmoid(t, pbaxilreit[i].min, pbaxilreit[i].max, 1.));
	}
}

