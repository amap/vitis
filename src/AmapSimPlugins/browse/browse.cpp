/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Output.h"
#include "PlantAMAP.h"
#include "ObserverManager.h"

#ifdef WIN32
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "AmapSimMod.h"
#include "PlantAMAP.h"
#include "GeomBrancAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "externBrowse.h"
#include "browse.h"
#define STATIC_LIB
#include "lightThroughVoxels.h"
#include "GldsFormat.h"
#undef STATIC_LIB


#define ALIVE 0
#define TOBEBROWSED 1
#define BROWSED 2

//void ParameterPlugin::on_notify (const state& st, paramMsg *data)
//{
//    //Cast message parameters
//    paramAxisRef * p = (paramAxisRef *)data;
//
//    //Retrieve branch
//    BrancAMAP * b = (BrancAMAP *)p->b;
//
	// entn test number
//    if (p->numVar == rtprefn1)
//	{
//	}
//	// initial branched first state proba
//	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpbinitret0
//		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpbinitret0)
//	{
//	}
//	// initial branched first state proba
//	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpbinitret1
//		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpbinitret1)
//	{
//	}
//	// remaining not branched proba
//	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_st0
//		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_st0)
//	{
//	}
//	// remaining first state proba
//	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_st1
//		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_st1)
//	{
//	}
//	// remaining second state proba
//	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_st2
//		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_st2)
//	{
//	}
//	// transition not branched -> first state proba
//	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_0_1
//		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_0_1)
//	{
//	}
//	// transition first state -> not branched proba
//	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_1_0
//		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_1_0)
//	{
//	}
//	// transition second state -> not branched proba
//	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_2_0
//		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_2_0)
//	{
//	}
//	// living proba
//	else if (p->numVar == rtpbvie)
//	{
//	}
//}

/////////////////////////////////////////////////////
//
// This method get called for each new topology creation
//
// It stores UT global parameters
//
/////////////////////////////////////////////////////
//void DecompPlugin::on_notify (const state& s, paramMsg * data)
//{
 //   //Cast msg parameters
 //   paramDecompAxeLevel *p = (paramDecompAxeLevel *)data;

 //   //Retrieve branch
 //   DecompAxeLevel *I = p->d;

	//if (s == SigDelDecompAxeLevel)
	//{
	//	FloatData *d;
	//	if ((d = (FloatData*)I->getAdditionalData("browsed")))
	//	{
	//		delete d;
	//		I->removeAdditionalData("browsed");
	//	}
	//	if ((d = (FloatData*)I->getAdditionalData("attractive")))
	//	{
	//		delete d;
	//		I->removeAdditionalData("attractive");
	//	}
	//	if ((d = (FloatData*)I->getAdditionalData("accessible")))
	//	{
	//		delete d;
	//		I->removeAdditionalData("accessible");
	//	}
	//	return;
	//}

	////Swith on axe object level (0 stand for branch, etc..)
 //   if (I->getLevel() == 0)
	//{
 //       //Retrieve initial phyAge of the branch (at birth of)
	//	BrancAMAP *b = (BrancAMAP*)I;

	//	int phyAge = b->phyAgeInit;

	//	FloatData *L;
	//	L = new FloatData();
	//	L->val = ALIVE;
	//	I->addAdditionalData ("browsed", L, VITISGLDSPRINTABLE);
	//	L = new FloatData();
	//	L->val = ALIVE;
	//	I->addAdditionalData ("attractive", L, 0);
	//	L = new FloatData();
	//	L->val = ALIVE;
	//	I->addAdditionalData ("accessible", L, 0);
	//}

//}

/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{

	paramPlant * d = (paramPlant*)data; //Retrieve msg params

	browseplugin_ptr->setPlant (d->p);

	// to avoid complexity due to simplication
	d->p->getConfigData().cfg_topoSimp = 0;
	d->p->getConfigData().cfg_geoSimp = 0;

	BrowseEventData *e = new BrowseEventData(2);
	Scheduler::instance()->create_process(browseplugin_ptr,e,browseplugin_ptr->StartBrowsing(),103);
}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{
	browseplugin_ptr->setPlant (NULL);
}

void browsePlugin::process_event(EventData *msg)
{
   if (plant == NULL)
        return;

	BrowseEventData *e = (BrowseEventData*)msg;

	Scheduler *s = Scheduler::instance();
	float top_extern = s->getTopClock();

	if (e->Action() == 1)
	{
		plant->getGeometry().clear();

cout << "Voxel space cleaning at " << top_extern << endl;
		// clean voxel space at the end of the step
		resetContent (plant);

		// register for next browsing step
		e->setAction (2);
		s->signal_event(this->getPid(),e,s->getTopClock()+1,103);
 	}
	else if (e->Action() == 2)
	{

cout << endl << "begin browsing at " << top_extern;
        FloatData *bs;
        bs = (FloatData *)plant->getAdditionalData("addedWoodVolume");
        bs->val = 0;
        bs = (FloatData *)plant->getAdditionalData("addedLeafSurface");
        bs->val = 0;
        bs = (FloatData *)plant->getAdditionalData("leavesPhotosyntheticEfficiency");
        bs->val = 0;
        bs=(FloatData*)plant->getAdditionalData("browsedSurface");
        bs->val = 0;
        bs=(FloatData*)plant->getAdditionalData("ratio");
        bs->val = 0;
        bs=(FloatData*)plant->getAdditionalData("nbTry");
        bs->val = 0;
        bs=(FloatData*)plant->getAdditionalData("nbSuccessfullTry");
        bs->val = 0;


		plant->computeGeometry();

		computeBrowsingMap (top_extern);

		computeBrowsing (top_extern);

        if (intermediateOpf)
        {
            std::ostringstream oss;
            oss << plant->getName() ;
            oss << "_" ;
            oss << top_extern ;
            oss << "_before_browsing" ;
            writeOPF (oss.str());
        }

		resetContent (plant);

		removeBrancs();

		plant->computeGeometry();

		computeMap (top_extern);

		lightThroughVoxelsPlugin LTVP;
		LTVP.updateLightThroughVoxels();

        computePhotosyntheticEfficiency();

        if (intermediateOpf)
        {
            std::ostringstream oss;
            oss.seekp(0);
            oss << plant->getName() ;
            oss << "_" ;
            oss << top_extern ;
            oss << "_after_browsing" ;
            writeOPF (oss.str());
        }
        outputBrowsingResults();


cout << "end browsing at " << top_extern <<endl;

		// register for cleaning at the end of the step
		e->setAction (1);
		s->signal_event(this->getPid(),e,s->getTopClock(),101);
	}
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
browsePlugin::browsePlugin(const std::string& f, const void *plant) {

	browsingMaxHeight = 1;
	leafPhyAge = 1;
	woodMaxPhyAge = 1;

	voxelSpace = VoxelSpace::instance();
	this->plant = (PlantAMAP*)plant;

	if (!initBrowseParam (f))
	{
		cout << "Failed reading browsing parameter file " << f << endl;
		cout << "File sould look like this :" << endl<<endl;
 		cout << endl<< "**** Failed reading browsing parameter file " << f << " ****" << endl;

		return;
	}
	//child -> parent binding PTR
	//l = new ParameterPlugin (this->plant);
	//l->browseplugin_ptr = this;
	//d = new DecompPlugin (this->plant);
	//d->browseplugin_ptr = this;
	p = new PlantPlugin (this->plant);
	p->browseplugin_ptr = this;
	pp = new PlantDeletePlugin (this->plant);
	pp->browseplugin_ptr = this;

//	srand(this->plant->getConfigData().randSeed);
	browseParameterFileName = f;

}

bool browsePlugin::initBrowseParam (const std::string& f) {
	ParamSet * param_H = new ParamSet(f);

	if (!param_H->Exists())
		return false;

	if (param_H->getParameter("protectionSensitivity"))
		protectionSensitivity = param_H->getParameter("protectionSensitivity")->value();
	else
	{
		cout << "Failed finding protectionSensitivity parameter in file " << f << endl;
		cout << "setting default value to 0.02 " << endl;
		protectionSensitivity = 0.02;
	}

	if (param_H->getParameter("budBrowsingProba"))
		budBrowsingProba = param_H->getParameter("budBrowsingProba")->value();
	else
	{
		cout << "Failed finding budBrowsingProba parameter in file " << f << endl;
		cout << "setting default value to 10% " << endl;
		budBrowsingProba = 10;
	}
	if (param_H->getParameter("browsingMaxHeight"))
		browsingMaxHeight = param_H->getParameter("browsingMaxHeight")->value();
	else
	{
		cout << "Failed finding browsingMaxHeight parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("browsedLeafMaxRatio"))
	{
		browsedLeafMaxRatio = param_H->getParameter("browsedLeafMaxRatio")->value();
		browsedLeafMaxRatio /= 100.;
	}
	else
	{
		cout << "Failed finding browsedLeafMaxRatio parameter in file " << f << endl;
		cout << "setting default value to 1 " << endl;
		browsedLeafMaxRatio = 1;
	}
	if (param_H->getParameter("voxelSize"))
		voxelSpace->VoxelSize(param_H->getParameter("voxelSize")->value());
	else
	{
		cout << "Failed finding voxelSize parameter in file " << f << endl;
		voxelSpace->VoxelSize(1);
		return false;
	}

	if (param_H->getParameter("leafPhyAge"))
		leafPhyAge = param_H->getParameter("leafPhyAge")->value();
	else
	{
		cout << "Failed finding leafPhyAge parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("woodMaxPhyAge"))
		woodMaxPhyAge = param_H->getParameter("woodMaxPhyAge")->value();
	else
	{
		cout << "Failed finding woodMaxPhyAge parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("maxOrgansBrowsedPerTry"))
		maxOrgansBrowsedPerTry = param_H->getParameter("maxOrgansBrowsedPerTry")->value();
	else
	{
        maxOrgansBrowsedPerTry = 99999;
		cout << "Failed finding maxLeavesBrowsedPerTry parameter in file " << f << endl;
		cout << "setting default value to 99999 " << endl;
	}
	if (param_H->getParameter("nbBrowsingTryThreshold"))
		nbBrowsingTryThreshold = param_H->getParameter("nbBrowsingTryThreshold")->value();
	else
	{
		cout << "Failed finding nbBrowsingTry parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("nbBrowsedOrgansThreshold"))
		nbBrowsedOrgansThreshold = param_H->getParameter("nbBrowsedOrgansThreshold")->value();
	else
	{
		cout << "Failed finding nbBrowsedOrgansThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("browsedBudIndex"))
		browsedBudIndex = param_H->getParameter("browsedBudIndex")->value();
	else
	{
		browsedBudIndex = -1;
		cout << "Failed finding browsedBudIndex parameter in file " << f << endl;
	}
	if (param_H->getParameter("browsedLeafIndex"))
		browsedLeafIndex = param_H->getParameter("browsedLeafIndex")->value();
	else
	{
		browsedLeafIndex = -1;
		cout << "Failed finding browsedLeafIndex parameter in file " << f << endl;
	}
	if (param_H->getParameter("browsedLeafAxillaryBudPhyAge"))
		browsedLeafAxillaryBudPhyAge = param_H->getParameter("browsedLeafAxillaryBudPhyAge")->value();
	else
	{
		browsedLeafAxillaryBudPhyAge = 9999;
		cout << "Failed finding browsedLeafAxillaryBudPhyAge parameter in file " << f << endl;
		cout << "setting default value to 9999 " << endl;
	}
	if (param_H->getParameter("output"))
		intermediateOpf = param_H->getParameter("output")->value();
	else
	{
		intermediateOpf = 0;
		cout << "Failed finding output parameter in file " << f << endl;
		cout << "setting default value to 0 " << endl;
	}

	if (param_H->getParameter("basitony"))
		basitony = (param_H->getParameter("basitony")->value() == 1);
	else
	{
		basitony = false;
		cout << "Failed finding basitony parameter in file " << f << endl;
		cout << "setting default value to false " << endl;
	}

	if (param_H->getParameter("startBrowsing"))
		startBrowsing = param_H->getParameter("startBrowsing")->value();
	else
	{
		startBrowsing = 1;
		cout << "Failed finding startBrowsing parameter in file " << f << endl;
		cout << "setting default value to 1 " << endl;
	}
	if (startBrowsing < 1)
        startBrowsing = 1;

    if (param_H->getParameter("withMIR"))
    {
        int val = param_H->getParameter("withMIR")->value();
        if (val != 0)
        {
            withMIR = true;
            if (param_H->getParameter("MIRStart"))
            {
                MIRStart = param_H->getParameter("MIRStart")->value();
            }
            else
                MIRStart = startBrowsing;
        }
    }
    if (param_H->getParameter("MIRParam"))
    {
        MIRParam = param_H->getParameter("MIRParam")->toString();
    }
    else
    {
        withMIR = false;
		cout << "Failed finding MIRParam parameter in file " << f << endl;
		cout << "no MIR computing will be carried out" << endl;
    }

    if (param_H->getParameter("MIRScene"))
    {
        MIRScene = param_H->getParameter("MIRScene")->toString();
    }
    else
    {
        withMIR = false;
		cout << "Failed finding MIRScene parameter in file " << f << endl;
		cout << "no MIR computing will be carried out" << endl;
    }

    if (param_H->getParameter("MIRFolder"))
    {
        MIRFolder = param_H->getParameter("MIRFolder")->toString();
    }
    else
    {
        withMIR = false;
		cout << "Failed finding MIRFolder parameter in file " << f << endl;
		cout << "no MIR computing will be carried out" << endl;
    }

    if (param_H->getParameter("MIRBrowsingRate"))
    {
        MIRBrowsingRate = param_H->getParameter("MIRBrowsingRate")->value();
    }
    else
    {
        MIRBrowsingRate = 10;
		cout << "Failed finding MIRBrowsingRate parameter in file " << f << endl;
		cout << "SETTING default value TO 10" << endl;
    }
    if (withMIR)
        MIR = new MIRScript (MIRParam, MIRScene, MIRFolder, MIRStart, false);

    FloatData *bs = new FloatData;
    bs->val = 0;
    plant->addAdditionalData("addedWoodVolume", bs, VITISGLDSPRINTABLE);
    bs = new FloatData;
    bs->val = 0;
    plant->addAdditionalData("addedLeafSurface", bs, VITISGLDSPRINTABLE);
    bs = new FloatData;
    bs->val = 0;
    plant->addAdditionalData("leavesPhotosyntheticEfficiency", bs, VITISGLDSPRINTABLE);
    bs = new FloatData;
    bs->val = 0;
    plant->addAdditionalData("browsedSurface", bs, VITISGLDSPRINTABLE);
    bs = new FloatData;
    bs->val = 0;
    plant->addAdditionalData("ratio", bs, VITISGLDSPRINTABLE);
    bs = new FloatData;
    bs->val = 0;
    plant->addAdditionalData("nbTry", bs, VITISGLDSPRINTABLE);
    bs = new FloatData;
    bs->val = 0;
    plant->addAdditionalData("nbSuccessfullTry", bs, VITISGLDSPRINTABLE);

	return true;
}

vector<Voxel*> browsePlugin::computeCandidateList ()
{
    int nbTry=0;
	int ib, jb, kb;
	Vector3 min = plant->getGeometry().getMin();
	Vector3 max = plant->getGeometry().getMax();
	Vector3 pos = plant->getPosition();
	vector<Voxel*> candidateList;
	int zMax;
	int maxNbTry = nbBrowsingTryThreshold;
	vector<Voxel*> vv;
	vector<Voxel*>::iterator itt;
	Voxel *v;
	bool endBrowse;
	BrowseData *b;
	float biteSize = 0;
int PotentialBrowsedLeafNb=0;

    voxelSpace->findVoxel (pos[0]+(max[0]+min[0])/2., pos[1]+(max[1]+min[1])/2., 0., &iCenter, &jCenter, &zMax);

	// build a list of browsing voxel candidates
	while (   nbTry < 10*maxNbTry
	       && candidateList.size() < maxNbTry)
	{
		nbTry ++;
		voxelSpace->randomInitialVoxel(&ib, &jb, &kb);
		vv.clear();

		voxelSpace->bresenham3D (ib, jb, kb, iCenter, jCenter, kb, &vv);

		endBrowse = false;
		for (vector<Voxel*>::iterator it=vv.begin(); it!=vv.end(); it++)
		{
			v = *it;
			if ((b=(BrowseData*)v->getVoxelData("browse")) == 0)
				continue;

//            if (!voxelSpace->voxelCoordinates(v, &m, &n, &t))
//            {
//cout << "cagouille"<<endl;
//                continue;
//            }
            if (   b->attractive == 0
                && b->repulsive > 0)
                break;

			if (   b->attractive > 0
                || (   budBrowsingProba >0
                    && b->budNumber > 0))
			{
				for (itt=candidateList.begin(); itt!=candidateList.end(); itt++)
				{
					Voxel *vvv = *itt;
					if (vvv == v)
                    {
                        break;
                    }
				}
                if (itt!=candidateList.end())
                    break;

                PotentialBrowsedLeafNb += b->attractive;
                int potential = potentialBrowsingCapacity (b);
				for (itt=candidateList.begin(); itt!=candidateList.end(); itt++)
				{
					Voxel *vvv = *itt;

					if (potentialBrowsingCapacity((BrowseData*)vvv->getVoxelData("browse")) >= potential)
					{
						candidateList.insert(itt, v);
						if (biteSize < potential)
                            biteSize = potential;
						break;
					}
				}
				if (itt == candidateList.end())
                {
                    candidateList.push_back(v);
                    if (biteSize < potential)
                        biteSize = potential;
                }

				endBrowse = true;
				break;
			}
			if (   b->repulsive > 0
                && protectionSensitivity > 0)
			{
				endBrowse = true;
				break;
			}

			if (endBrowse)
				break;
		}
	}

cout << endl << "nb candidates " << candidateList.size() << " nb leaves "<< PotentialBrowsedLeafNb << endl;

	return candidateList;
}

void browsePlugin::computeBrowsing (float top_extern)
{
// pour bien brouter il faut :
// - faire un nombre d'essais max predetermine (en fonction de la structure de la plante ?)
// - s'arreter quand un nombre de succes (on a atteint un voxel qui contient qqchose) a ete atteint ou bien un nombre max d'organes a ete prelevee (ou bien une biomasse ?)
// - determiner la strategie quand on atteint un voxel repulsif
//	- si le voxel est repulsif (selon un seuil ?) ET attractif, on broute ou pas ?
// - determiner la strategie de broutage quand on atteint un voxel prometteur
//	- prelever une quantite de feuilles (aleatoire ?)
//	- prelever les eventuels bourgeons apicaux (aleatoire ?)

	int nbSuccessfullTry = 0;
	int ib, jb, kb;
	vector<Voxel*> vv;
	bool endBrowse, found;
	int nbBrowsedOrgans=0, savnbBrowsedOrgans, MIRsavnbBrowsedOrgans=0;
	int nbTry = 0;
	int indMIR = 1;
	int maxNbTry = nbBrowsingTryThreshold;
	double cumulativeRepulsion;
	BrowseData *b;
	float biteSize = 0;

	Vector3 min = plant->getGeometry().getMin();
	Vector3 max = plant->getGeometry().getMax();
	Vector3 pos = plant->getPosition();
	int zMax;

	float ratio = 0;

	Voxel *v;
	vector<Voxel*>::iterator itt;

	vector<Voxel*> candidateList;

	candidateList = computeCandidateList();
	if (candidateList.size() == 0)
        return;

    if (withMIR)
    {
        writeOPF (plant->getName());
        // run MIR script
        MIR->process_event(NULL);
        // save results
        std::ostringstream oss;
        oss << "mv " << plant->getName() << ".opf ";
        oss << plant->getName() ;
        oss << "_" ;
        oss << top_extern ;
        oss << "_" ;
        oss << indMIR ;
        oss << "_along_browsing.opf" ;
        string str = oss.str();
        system (oss.str().c_str());
        indMIR++;
    }

	vector<Voxel*> candidateListCopy = candidateList;
	// browse within the list
	nbTry = 0;
	Voxel *vprec=NULL;
	float bitingRate=0, feedingRate;
	vector<float> bitingRateList;
	vector<float> feedingRateList;
	while (   nbBrowsedOrgans < nbBrowsedOrgansThreshold
           && nbTry < maxNbTry)
	{
        if (candidateList.size() == 0)
            return;
		Voxel *v = candidateList.back();
		BrowseData *b=(BrowseData*)v->getVoxelData("browse");
		if (vprec == NULL)
            vprec = v;
        bitingRate = 0;
        feedingRate = 0;

		double cumulativeRepulsion = 0;
		// local browsing
        nbTry ++;
        savnbBrowsedOrgans = nbBrowsedOrgans;
		while (   v
		       && browse (v, &nbBrowsedOrgans)
		       && nbBrowsedOrgans - savnbBrowsedOrgans < maxOrgansBrowsedPerTry)
		{

			ratio = nbBrowsedOrgans;
			ratio /= leavesNumber;
			if (ratio >= browsedLeafMaxRatio)
				break;
			cumulativeRepulsion += ((BrowseData*)v->getVoxelData("browse"))->repulsive * protectionSensitivity;
			v = findNeighbour(v, cumulativeRepulsion);
            if (v != vprec)
            {
                bitingRate ++;
                vprec = v;
            }
		}

		if (nbBrowsedOrgans - savnbBrowsedOrgans > 0)
 			nbSuccessfullTry ++;

        if (ratio >= browsedLeafMaxRatio)
            break;
		candidateList.pop_back();
		if (candidateList.empty())
            candidateList = candidateListCopy;
		bitingRateList.push_back(bitingRate);
		feedingRateList.push_back(nbBrowsedOrgans - savnbBrowsedOrgans);

		if (   withMIR
		    && nbTry%MIRBrowsingRate == 0
		    && nbBrowsedOrgans - MIRsavnbBrowsedOrgans > 0)
        {
            MIRsavnbBrowsedOrgans = nbBrowsedOrgans;

            resetContent(plant);

           // update geometry
            removeBrancs();
            plant->getGeometry().clear();
            plant->computeGeometry();
            writeOPF (plant->getName());
            // run MIR script
            MIR->process_event(NULL);
            // save results
            std::ostringstream oss;
            oss << "mv " << plant->getName() << ".opf ";
            oss << plant->getName() ;
            oss << "_" ;
            oss << top_extern ;
            oss << "_" ;
            oss << indMIR ;
            oss << "_along_browsing.opf" ;
            string str = oss.str();
            system (oss.str().c_str());
            indMIR++;

            // reinitialize voxel space
            nbBrowsedOrgans=MIRsavnbBrowsedOrgans=savnbBrowsedOrgans=0;
            computeBrowsingMap (top_extern);
            candidateListCopy = candidateList = computeCandidateList();
            vprec = NULL;
        }

	}
    FloatData *bs=(FloatData*)plant->getAdditionalData("ratio");
    bs->val = ratio;

    bs=(FloatData*)plant->getAdditionalData("nbTry");
    bs->val = nbTry;

    bs=(FloatData*)plant->getAdditionalData("nbSuccessfullTry");
    bs->val = nbSuccessfullTry;


cout << "nb Browsed "<<nbBrowsedOrgans<<"  nbTry "<<nbTry<<" nbSuccessfullTry "<<nbSuccessfullTry<<"  browsed ratio "<<ratio<<endl;
cout << "age" << top_extern << " biteSize "<<biteSize<< endl;
cout << " bitingRate feedingRate"<<endl;
vector<float> ll;
//float acc1=0, acc2=0;
//int step =5;
//for (int i=0; i<bitingRateList.size(); i++)
//{
//    acc1+=bitingRateList.at(i);
//    acc2+=feedingRateList.at(i);
//    if (i%step == 0 && i >0)
//    {
//        acc1/=step;
//        acc2/=step;
//        cout << "age" << top_extern << " bitingRate "<< acc1 << endl;
//        cout << "age" << top_extern << " feedingRate "<<  acc2 << endl;
//        acc1 = acc2 = 0;
//    }
//}
}

int browsePlugin::potentialBrowsingCapacity (BrowseData *b)
{
    int capacity = 0;

    if (   b->repulsive == 0
        || protectionSensitivity <= 0)
        return b->attractive;

    do
    {
        capacity++;
    }while (b->attractive -capacity - capacity*protectionSensitivity*b->repulsive >= 0);
    return capacity;
}

Voxel *browsePlugin::findNeighbour (Voxel *v, double cumulativeRepulsion)
{
	Voxel *neighbour=NULL;

	// return itself if attractive enough
	if (((BrowseData*)v->getVoxelData("browse"))->attractive > cumulativeRepulsion)
        return v;

	int i, j, k;
	if (voxelSpace->voxelCoordinates (v, &i, &j, &k))
	{
		int im = rand();
		for (int m=0; m<14; m++)
		{
		    int imm = (m+im)%14;
			switch (imm)
			{
				case 0:
					neighbour = voxelSpace->addrAt (i, j, k+1);
				break;
				case 1:
					neighbour = voxelSpace->addrAt (i, j, k-1);
				break;
				case 2:
					neighbour = voxelSpace->addrAt (i+1, j, k);
				break;
				case 3:
					neighbour = voxelSpace->addrAt (i-1, j, k);
				break;
				case 4:
					neighbour = voxelSpace->addrAt (i, j+1, k);
				break;
				case 5:
					neighbour = voxelSpace->addrAt (i, j-1, k);
				break;
				case 6:
					neighbour = voxelSpace->addrAt (i-1, j-1, k+1);
				break;
				case 7:
					neighbour = voxelSpace->addrAt (i-1, j+1, k+1);
				break;
				case 8:
					neighbour = voxelSpace->addrAt (i+1, j-1, k+1);
				break;
				case 9:
					neighbour = voxelSpace->addrAt (i+1, j+1, k+1);
				break;
				case 10:
					neighbour = voxelSpace->addrAt (i-1, j-1, k-1);
				break;
				case 11:
					neighbour = voxelSpace->addrAt (i-1, j+1, k-1);
				break;
				case 12:
					neighbour = voxelSpace->addrAt (i+1, j-1, k-1);
				break;
				case 13:
					neighbour = voxelSpace->addrAt (i+1, j+1, k-1);
				break;
				default:
					neighbour = NULL;
				break;
			}
			if (   neighbour == NULL
                || neighbour->getVoxelData("browse") == NULL)
			{
			    neighbour = NULL;
			    continue;
			}
			if (((BrowseData*)neighbour->getVoxelData("browse"))->attractive > cumulativeRepulsion)
				return neighbour;
		}
	}
	return neighbour;
}

GeomElemCone * browsePlugin::browse (Voxel *v, int *nbBrowsedOrgans)
{
	GeomElemCone *browsedOrgan = NULL, *e;
	float browsedSurface = 0;

	if (((BrowseData*)v->getVoxelData("browse"))->attractive > 0)
	{
		// choose an organ to browse an remove it from archiTree
		for (vector<GeomElemCone*>::iterator it=v->content.begin(); it != v->content.end(); it++)
		{
			e = (GeomElemCone*)*it;
			BrancAMAP *b = (BrancAMAP*)e->getPtrBrc()->getBranc();
			if (b->phyAgeInit >= leafPhyAge-1)
			{
				int rep=0, att=0;
				if (b->phyAgeInit < woodMaxPhyAge)
					rep = -1;
				if (b->phyAgeInit >= leafPhyAge-1)
					att = -1;

				// remove the whole leaf
				vector<Voxel*> vv = voxelSpace->removeElem (e, e->getPtrBrc());
				for (int i=0; i<vv.size(); i++)
				{
					((BrowseData*)vv[i]->getVoxelData("browse"))->repulsive += rep;
					((BrowseData*)vv[i]->getVoxelData("browse"))->attractive += att;
				}
				browsedOrgan = e;
				FloatData *L = (FloatData*)e->getPtrBrc()->getBranc()->getAdditionalData ("browse");
				if (   L != 0
				    || L->val == ALIVE)
					L->val = TOBEBROWSED;
				else
				{
					L = new FloatData;
					L->val = TOBEBROWSED;
					e->getPtrBrc()->getBranc()->addAdditionalData("browse", L);
				}
				if (browsedLeafIndex != -1)
					e->setSymbole (browsedLeafIndex);
				*nbBrowsedOrgans +=1;

                if (b->phyAgeInit >= leafPhyAge-1)
                   browsedSurface += e->getLength() * e->getBottomDiam();

				break;
			}
			else if (((BrowseData*)v->getVoxelData("browse"))->budNumber > 0)
            {
                if (e->getPtrBrc()->getElementsGeo()[e->getPtrBrc()->getElementsGeo().size()-1] == e)
                if (rand() <= budBrowsingProba * RAND_MAX / 100)
                {
                    // remove only the last geomElem of the branc
                    vector<Voxel*> vv = voxelSpace->removeElem (e, 0);
                    for (int i=0; i<vv.size(); i++)
                    {
                        ((BrowseData*)vv[i]->getVoxelData("browse"))->budNumber -= 1;
                    }
                    browsedOrgan = e;
                    FloatData *L = (FloatData*)e->getPtrBrc()->getBranc()->getAdditionalData ("browse");
                    if (L != 0)
                        L->val = TOBEBROWSED;
                    else
                    {
                        L = new FloatData;
                        L->val = TOBEBROWSED;
                        e->getPtrBrc()->getBranc()->addAdditionalData("browse", L, VITISGLDSPRINTABLE);
                    }

                    if (browsedBudIndex != -1)
                        e->setSymbole (browsedBudIndex);
                    *nbBrowsedOrgans +=1;
                    break;
                }
            }
		}
	}
//	else if (((BrowseData*)v->getVoxelData("browse"))->budNumber > 0)
//	{
//
//		// choose a bud to browse an remove it from archiTree
//		for (vector<GeomElemCone*>::iterator it=v->content.begin(); it != v->content.end(); it++)
//		{
//			e = (GeomElemCone*)*it;
//			BrancAMAP *b = (BrancAMAP*)e->getPtrBrc()->getBranc();
//			if (   b->phyAgeInit < woodMaxPhyAge
//			    && rand() < budBrowsingProba / 100 * RAND_MAX
//			    && e->getPtrBrc()->getElementsGeo()[e->getPtrBrc()->getElementsGeo().size()-1] == e)
//			{
//				// remove only the last geomElem of the branc
//				vector<Voxel*> vv = voxelSpace->removeElem (e, 0);
//				for (int i=0; i<vv.size(); i++)
//				{
//					((BrowseData*)vv[i]->getVoxelData("browse"))->budNumber -= 1;
//				}
//				browsedOrgan = e;
//				FloatData *L = (FloatData*)e->getPtrBrc()->getBranc()->getAdditionalData ("browse");
//				if (L != 0)
//					L->val = TOBEBROWSED;
//				else
//				{
//					L = new FloatData;
//					L->val = TOBEBROWSED;
//					e->getPtrBrc()->getBranc()->addAdditionalData("browse", L, VITISGLDSPRINTABLE);
//				}
//
//				if (browsedBudIndex != -1)
//					e->setSymbole (browsedBudIndex);
//				*nbBrowsedOrgans +=1;
//				break;
//			}
//		}
//	}
    FloatData *bs = (FloatData*)plant->getAdditionalData("browsedSurface");
    bs->val += browsedSurface;

	return browsedOrgan;
}

void browsePlugin::computeBrowsingMap (float top_extern){

	float voxelSize = voxelSpace->VoxelSize();

	// compute bounding box dimensions
	Vector3 min = plant->getGeometry().getMin();
	Vector3 max = plant->getGeometry().getMax();
	Vector3 pos = plant->getPosition();

	int x = ceil (max[0] / voxelSize + 0.5) - floor (min[0] / voxelSize - 0.5);
	int y = ceil (max[1] / voxelSize + 0.5) - floor (min[1] / voxelSize - 0.5);
	int indXOrigin = floor ((pos[0]+min[0]) / voxelSize - 0.5);
	int indYOrigin = floor ((pos[1]+min[1]) / voxelSize - 0.5);
	float xOrig = indXOrigin * voxelSize;
	float yOrig = indYOrigin * voxelSize;

//cout << "posx "<<pos[0]<<" posy "<<pos[1]<<endl;
//cout << "minx "<<min[0]<<" miny "<<min[1]<<endl;
//cout << "xOrig "<<xOrig<<" yOrig "<<yOrig<<endl;
//cout << "sizex "<<x<<" sizey "<<y<<endl;

	float h = max[2]-min[2];
	if (h > browsingMaxHeight)
		h = browsingMaxHeight;
	int z = ceil (h / voxelSize);

	voxelSpace->adjust (xOrig, yOrig, x, y, z);

	int zMax = 0;
	zMax = 0;
	int zMin = voxelSpace->ZSize();
	int ib, jb, kb;
	int ie, je, ke;
	GeomElemCone *e;
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	vector<Voxel*> vv;
	leavesNumber = 0;

    FloatData *awv = (FloatData*)plant->getAdditionalData("addedWoodVolume");
    FloatData *als = (FloatData*)plant->getAdditionalData("addedLeafSurface");

	for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
		GeomBrancAMAP *g = (GeomBrancAMAP*)*b;

		// sum currentTop production for output
		BrancAMAP *ba = (BrancAMAP*)g->getBranc();
		ba->startPosit();
		ba->positOnLastEntnWithinBranc();
		float l;
		float d;
        double phyAgeGu = ba->getCurrentGu()->phyAgeGu;
        InterNodeAMAP *i;
        while (   (i=ba->getCurrentEntn()) != NULL
               && i->instant > Scheduler::instance()->getTopClock()-1)
        {
            l = g->computeLength();
            d = g->computeBottomDiam();
            if (phyAgeGu >= leafPhyAge-1)
            {
//                d = g->computeBottomDiam();
                als->val += l*d;
            }
            else
            {
                awv->val += l*d*d/4*M_PI;
            }
            if (!ba->positOnPreviousEntnWithinGu())
                break;
        }
        ba->endPosit();

		FloatData *L = (FloatData *)g->getBranc()->getAdditionalData("browse");
		if (L == 0)
		{
			L = new FloatData;
			L->val = ALIVE;
			g->getBranc()->addAdditionalData("browse", L, VITISGLDSPRINTABLE);
		}

		vector<GeomElem *> ges = g->getElementsGeo ();
		// put apical bud into voxelSpace
		e = (GeomElemCone*)ges[ges.size()-1];
		if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()+pos[0],
				e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()+pos[1],
				e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength(),
				&ie, &je, &ke))
		{
			if (zMin > ke)
				zMin = ke;
			if (zMax < ke)
				zMax = ke;

			BrancAMAP *b = (BrancAMAP*)e->getPtrBrc()->getBranc();

			if (   b->phyAgeInit < woodMaxPhyAge
			    && L->val == ALIVE)
			{
				if (voxelSpace->addrAt(ie, je, ke)->getVoxelData("browse") == 0)
				{
					voxelSpace->addrAt(ie, je, ke)->addVoxelData("browse", new BrowseData());
				}
				((BrowseData*)voxelSpace->addrAt(ie, je, ke)->getVoxelData("browse"))->budNumber += 1;
				voxelSpace->addrAt(ie, je, ke)->content.push_back(e);
			}
			if (b->phyAgeInit >= leafPhyAge-1)
			{
				leavesNumber ++;
			}
		}

		for (vector<GeomElem *>::iterator gj=ges.begin(); gj!=ges.end(); gj++)
		{
			vv.clear();
			e = (GeomElemCone*)*gj;
			if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+pos[0], e->getMatrix().getTranslation().y()+pos[1], e->getMatrix().getTranslation().z(), &ib, &jb, &kb))
			{
				if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()+pos[0],
				       e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()+pos[1],
				       e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength(),
				       &ie, &je, &ke))
				{
					BrancAMAP *b = (BrancAMAP*)e->getPtrBrc()->getBranc();
					if (b->phyAgeInit >= leafPhyAge-1)
					{
						if (zMin > ke)
							zMin = ke;
						if (zMax < ke)
							zMax = ke;
					}

					if (ib!=ie || jb!=je || kb!=ke)
					{
						voxelSpace->bresenham3D (ib, jb, kb, ie, je, ke, &vv);
					}
					else
						vv.push_back(voxelSpace->addrAt(ib, jb, kb));

					updatePlantAndVoxels (e, vv);
				}
			}
		}
	}
}
void browsePlugin::computeMap (float top_extern){

	float voxelSize = voxelSpace->VoxelSize();

	// compute bounding box dimensions
	Vector3 min = plant->getGeometry().getMin();
	Vector3 max = plant->getGeometry().getMax();
	Vector3 pos = plant->getPosition();

	int x = ceil (max[0] / voxelSize + 0.5) - floor (min[0] / voxelSize - 0.5);
	int y = ceil (max[1] / voxelSize + 0.5) - floor (min[1] / voxelSize - 0.5);
	int indXOrigin = floor ((pos[0]+min[0]) / voxelSize - 0.5);
	int indYOrigin = floor ((pos[1]+min[1]) / voxelSize - 0.5);
	float xOrig = indXOrigin * voxelSize;
	float yOrig = indYOrigin * voxelSize;

//cout << "posx "<<pos[0]<<" posy "<<pos[1]<<endl;
//cout << "minx "<<min[0]<<" miny "<<min[1]<<endl;
//cout << "xOrig "<<xOrig<<" yOrig "<<yOrig<<endl;
//cout << "sizex "<<x<<" sizey "<<y<<endl;

	float h = max[2]-min[2];
	if (h > browsingMaxHeight)
		h = browsingMaxHeight;
	int z = ceil (h / voxelSize);

	voxelSpace->adjust (xOrig, yOrig, x, y, z);

	int zMax = 0;
	zMax = 0;
	int zMin = voxelSpace->ZSize();
	int ib, jb, kb;
	int ie, je, ke;
	GeomElemCone *e;
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	vector<Voxel*> vv;
	leavesNumber = 0;

	for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
		GeomBrancAMAP *g = (GeomBrancAMAP*)*b;

		vector<GeomElem *> ges = g->getElementsGeo ();
		for (vector<GeomElem *>::iterator gj=ges.begin(); gj!=ges.end(); gj++)
		{
			vv.clear();
			e = (GeomElemCone*)*gj;
			if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+pos[0], e->getMatrix().getTranslation().y()+pos[1], e->getMatrix().getTranslation().z(), &ib, &jb, &kb))
			{
				if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()+pos[0],
				       e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()+pos[1],
				       e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength(),
				       &ie, &je, &ke))
				{
					BrancAMAP *b = (BrancAMAP*)e->getPtrBrc()->getBranc();
					if (b->phyAgeInit >= leafPhyAge-1)
					{
						if (zMin > ke)
							zMin = ke;
						if (zMax < ke)
							zMax = ke;
					}

					if (ib!=ie || jb!=je || kb!=ke)
					{
						voxelSpace->bresenham3D (ib, jb, kb, ie, je, ke, &vv);
					}
					else
						vv.push_back(voxelSpace->addrAt(ib, jb, kb));

					updatePlantAndVoxels (e, vv);
				}
			}
		}
	}
}

void browsePlugin::updatePlantAndVoxels (GeomElemCone *e, vector<Voxel*> vv)
{
	int phyAge;
	BrancAMAP *b = (BrancAMAP*)e->getPtrBrc()->getBranc();
	phyAge = b->phyAgeInit;
	for (vector<Voxel*>::iterator it=vv.begin(); it!=vv.end(); it++)
	{
		Voxel *v = *it;
		v->content.push_back(e);
		if (v->getVoxelData("browse") == 0)
		{
			v->addVoxelData("browse", new BrowseData());
		}
		BrowseData *br = (BrowseData*)v->getVoxelData("browse");
		if (phyAge < woodMaxPhyAge)
			br->repulsive ++;
		if (phyAge >= leafPhyAge-1)
		{
			br->attractive ++;
		}
	}
}

void browsePlugin::removeBrancs()
{
	BrancAMAP *b;
	bool trouve = true;
	Hierarc *h;
	vector<Hierarc *> listHierarc;

	// create the list of hierarcs to be deleted
	while (trouve)
	{
		trouve = false;
		for (vector<GeomBranc *>::iterator it=plant->getGeometry().getGeomBrancStack().begin(); it!=plant->getGeometry().getGeomBrancStack().end(); it++)
		{
			GeomBranc *g = *it;

			FloatData *L=(FloatData*)g->getBranc()->getAdditionalData("browse");
			if (L == 0)
				continue;

			if (L->val != TOBEBROWSED)
				continue;

			L->val = BROWSED;

			plant->getGeometry().getGeomBrancStack().erase(it);
			h = g->getAxe();
			listHierarc.push_back(h);
			trouve = true;

			break;
		}
	}

	// delete the hierarcs
	// in case of topological simplification, removing a part of a simplified branc makes it different from
	// the other previous siblings. So it has to be put out of the simplified hierarc list.
	//
	for (vector<Hierarc *>::iterator it=listHierarc.begin(); it!=listHierarc.end(); it++)
	{
		h = *it;
		b = (BrancAMAP *)h->getBranc();
		if (b->phyAgeInit >= leafPhyAge-1)
		{
			plant->getTopology().removeBranc(b, 0);

			// grow an axilary bud
			createAxillaryBud (b);
		}
		else
		{
			// stop current bud growth
			int aid = Scheduler::instance()->getActionIndex (b->getBud()->process_id);
			Action ai = Scheduler::instance()->getAction (b->getBud()->process_id);
			Scheduler::instance()->changeAction (aid, ai.pid, Scheduler::instance()->getHotStop()+1, ai.priority, NULL);

			// grow a relay bud
			createRelayBud (b, basitony);
		}

	}

}

void browsePlugin::createBud (double phyAge, BrancAMAP *bearer)
{
//cout <<"************************** add bud phy age "<<phyAge<<" at "<<Scheduler::instance()->getTopClock()+1<<endl;
	std::string critere;

	BrancAMAP *bVert= (BrancAMAP *)plant->getPlantFactory()->createInstanceBranc("AMAPSimMod");

	bVert->set(bearer->nature, bearer->lpIndex, critere, phyAge, bearer->reiterationOrder);

	plant->getTopology().fork(bVert, bearer->CurrentHierarcPtr(), 1, 1);

	bVert->addBud((Bud *)plant->getPlantFactory()->createInstanceBud(5,bVert,(int)bearer->getBud()->branchRamificationType, Scheduler::instance()->getTopClock()+1, phyAge, bearer->getBud()->getGrowth()->rythme));

	plant->getTopology().updateSimplifier(bVert,critere);

	float nextInstant = Scheduler::instance()->getTopClock() + 1 + bVert->getBud()->timeForATest;
	if (   nextInstant > Scheduler::instance()->getHotStop()
		&& nextInstant < Scheduler::instance()->getHotStop()+EPSILON)
		nextInstant = Scheduler::instance()->getHotStop();

	Scheduler::instance()->create_process(bVert->getBud(), NULL,nextInstant, Scheduler::instance()->getTopPriority()+11);
}

void browsePlugin::createRelayBud (BrancAMAP *b, bool basitone)
{
	if (browsedLeafAxillaryBudPhyAge == -1)
		return;

    int phyAge = b->getCurrentGu()->phyAgeGu+1;
    int i;
    if (basitone)
    {
        b->startPosit();
        // find a candidate place for reiteration
        Hierarc *h = b->getCurrentHierarc();
        b->positOnFirstEntnWithinBranc();
        int nbborne = h->getNbBorne();
        while (1)
        {
            int pos = b->getCurrentEntnIndexWithinBranch();
            for (i=0; i<nbborne; i++)
            {
                if (   h->bornePosition[i] == pos
                    && ((BrancAMAP *)h->getBorne(i))->phyAgeInit < leafPhyAge)
                {
                    break;
                }
            }
            if (i == nbborne)
                break;

            if (!b->positOnNextEntnWithinBranc())
                break;
        }
    }
	createBud (phyAge-1, b);
    if (basitone)
        b->endPosit();
}

void browsePlugin::createAxillaryBud (BrancAMAP *b)
{
	if (browsedLeafAxillaryBudPhyAge == -1)
		return;

 	createBud ((double)browsedLeafAxillaryBudPhyAge, b->getBearer());
}

void browsePlugin::computePhotosyntheticEfficiency()
{
	Vector3 pos = plant->getPosition();
    FloatData *bs;
    bs = (FloatData *)plant->getAdditionalData("leavesPhotosyntheticEfficiency");
    bs->val = 0;
    float l;
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	vector<GeomElem *> ges;
	GeomElemCone *e;
    BrancAMAP *ba;
    int ie, je, ke;
    Voxel *v;

	for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
		GeomBrancAMAP *g = (GeomBrancAMAP*)*b;

		// is this a leaf ?
		ba = (BrancAMAP*)g->getBranc();
        if (ba->phyAgeInit >= leafPhyAge-1)
        {
            ges = g->getElementsGeo ();
            // put apical bud into voxelSpace
            e = (GeomElemCone*)ges[ges.size()-1];
            if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()/2+pos[0],
                    e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()/2+pos[1],
                    e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength()/2,
                    &ie, &je, &ke))
            {
                v = voxelSpace->addrAt(ie, je, ke);
                if (v->getVoxelData("light") != 0)
                {
                    l = ((LightData*)v->getVoxelData("light"))->light;
                    if (l > 0.3)
                        l = 1;
                    else
                        l /= 0.3;
                    bs->val += l;
                    FloatData *bsl = (FloatData *)ba->getAdditionalData("leavesPhotosyntheticEfficiency");
                    if (bsl == NULL)
                    {
                        bsl = new FloatData;
                        ba->addAdditionalData("leavesPhotosyntheticEfficiency", bsl, VITISGLDSPRINTABLE);
                    }
                    bsl->val = l;
                }
            }
        }
	}
cout << "photosynthetic efficiency " << bs->val << endl;
}



void browsePlugin::writeOPF(const std::string &finame)
{
	int i;

	std::stringstream sout;
	sout << finame;
	std::string fname (sout.str());

	GldsFormat *outFormat= new GldsFormat(fname);

	outFormat->initFormat(&(plant->getGeometry()));

	std::cout<<"Output Geometry opf to : "  << fname << ".opf" << std::endl;

	if (plant->getGeometry().getGeomBrancStack().size())
	{
		outFormat->printData(plant->getGeometry().getGeomBrancStack().at(0));
	}
	else
	{
		std::cout<<"WARNING : -------> Empty Geometry"<<std::endl;
	}

	((GldsFormat*)outFormat)->closeFormat();

	delete outFormat;
}

void browsePlugin::outputBrowsingResults()
{
    std::ostringstream oss;
    oss << plant->getName() ;
    oss << "_" ;
    oss << Scheduler::instance()->getTopClock() ;
    oss << "_" ;
    oss << plant->getConfigData().randSeed;
    oss << "_" ;
    oss << nbBrowsedOrgansThreshold;
    oss << "_" ;
    oss << browsedLeafMaxRatio;
    oss << "_" ;
    oss << nbBrowsingTryThreshold;
    oss << ".browse";
//    int pos = browseParameterFileName.find_last_of("/\\");
//    string s = browseParameterFileName.substr(pos+1);
//    oss << s;


    ofstream fic (oss.str().c_str());

	if (!fic.good())
	{
		perror (oss.str().c_str());
		return;
	}

    cout << "output results to " << oss.str() << endl;
    fic << "species " << plant->getName();
    fic << " age " << Scheduler::instance()->getTopClock();
    fic << " seed " << plant->getConfigData().randSeed;
    fic << " nbBrowsedOrganThreshold " << nbBrowsedOrgansThreshold;
    fic << " browsedLeafMaxRatio " << browsedLeafMaxRatio;
    fic << " nbBrowsingTryThreshold " << nbBrowsingTryThreshold;

    FloatData *bs = (FloatData *)plant->getAdditionalData("addedWoodVolume");
    fic << " addedWoodVolume " << bs->val;
    bs = (FloatData *)plant->getAdditionalData("addedLeafSurface");
    fic << " addedLeafSurface " << bs->val;

    bs=(FloatData*)plant->getAdditionalData("nbTry");
    fic << " nbTry " << bs->val;
    bs=(FloatData*)plant->getAdditionalData("nbSuccessfullTry");
    fic << " nbSuccessfullTry " << bs->val;
    bs=(FloatData*)plant->getAdditionalData("browsedSurface");
    fic << " browsedSurface " << bs->val;
    bs=(FloatData*)plant->getAdditionalData("ratio");
    fic << " nbBrowsedLeavesProportion " << bs->val;
    bs = (FloatData *)plant->getAdditionalData("leavesPhotosyntheticEfficiency");
    fic << " leavesPhotosyntheticEfficiency " << bs->val;
    fic << endl;

    fic.close();
}

void browsePlugin::resetContent (Plant *p)
{
	vector<GeomElemCone*>::iterator it;
    Voxel *v;
	BrowseData *bd;
    int xSize = voxelSpace->XSize();
    int ySize = voxelSpace->YSize();
    int zSize = voxelSpace->ZSize();

	for (int i=0; i<zSize; i++)
	for (int j=0; j<xSize; j++)
	for (int k=0; k<ySize; k++)
	{
        v = voxelSpace->addrAt (i, j, k);
        if (v == 0)
            continue;
        if ((bd=(BrowseData*)v->getVoxelData("browse")) == 0)
            continue;
        for (it=v->content.begin(); it!=v->content.end(); it++)
        {
            GeomElemCone *g = *it;
            if (g->getPtrBrc()->getBranc()->getPlant() == p)
            {
                bd->attractive = 0;
                bd->repulsive = 0;
            }
        }
    }

    voxelSpace->resetContent (p);

}


