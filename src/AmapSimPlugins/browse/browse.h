/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file browse.h
///			\brief Definition of browsing simulation class
///
/// pour bien brouter il faut :
/// - faire un nombre d'essais max predetermine (en fonction de la structure de la plante ?)
/// - s'arreter quand un nombre de succes (on a atteint un voxel qui contient qqchose) a ete atteint ou bien un nombre max d'organes a ete prelevee (ou bien une biomasse ?)
/// - determiner la strategie quand on atteint un voxel repulsif
///	- si le voxel est repulsif (selon un seuil ?) ET attractif, on broute ou pas ?
/// - determiner la strategie de broutage quand on atteint un voxel prometteur
///	- prelever une quantite de feuilles (aleatoire ?)
///	- prelever les eventuels bourgeons apicaux (aleatoire ?)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
using namespace std;

#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "ParamFile.h"
#include "Voxel.h"
#include "MIRScript.h"

//#include "externBrowse.h"
#define STATIC_LIB
#include "lightThroughVoxels.h"
#undef STATIC_LIB


#include <vector>

struct browsePlugin;



#include "defAMAP.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\struct BrowseEventData
///			\brief Data that is transmitted from one browser step to the next one
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct BrowseEventData: public EventData
{
public:
	BrowseEventData (int action){this->action = action;};
	int Action (){return action;};
	void setAction (int a){action = a;};
private:
	int action;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\struct FloatData
///			\brief Simple floating point data that is able to output (for OPF purpose)
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct FloatData : public PrintableObject
{
public :
	float val;
	std::string to_string ()
	{
		std::ostringstream oss;
		// �crire la valeur dans le flux
		oss << val ;
		// renvoyer une string
		return oss.str();
	};
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\struct BrowseData
///			\brief Simple voxel data containt that is able to output (for OPF purpose)
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct BrowseData : public VoxelData
{
public :
	BrowseData() {attractive=repulsive=budNumber=0;};
	double attractive;
	double repulsive;
	int budNumber;
	virtual std::string to_string ()
	{
		std::ostringstream oss;
		// �crire la valeur dans le flux
		oss << attractive ;
		oss << " " ;
		oss << repulsive ;
		oss << " " ;
		oss << budNumber ;
		// renvoyer une string
		return oss.str();
	};
	virtual VoxelData *duplicate (){
        BrowseData *v = new BrowseData;
        v->attractive = attractive;
        v->repulsive = repulsive;
        v->budNumber = budNumber;
        return v;
    };
//	virtual void copyTo (BrowseData **b)
//	{
//		*b = new BrowseData;
//		(*b)->attractive = attractive;
//		(*b)->repulsive = repulsive;
//		(*b)->budNumber = budNumber;
//	};
};


//struct ParameterPlugin:public subscriber<messageAxeRef> {
//	browsePlugin * browseplugin_ptr;
//
//	ParameterPlugin (Plant *p) {subscribe(p);};
//	virtual ~ParameterPlugin () {unsubscribe();};
//	void on_notify (const state& st, paramMsg * data=NULL);
//};

//struct DecompPlugin:public subscriber<messageVitisDecompAxeLevel> {
//	browsePlugin * browseplugin_ptr;
//
//	DecompPlugin (Plant *p) {subscribe(p);};
//	virtual ~DecompPlugin () {unsubscribe();};
//	void on_notify (const state& st, paramMsg * data=NULL);
//};

struct PlantPlugin:public subscriber<messageVitisPlant> {
	browsePlugin * browseplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	browsePlugin * browseplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

enum messageBrowseParameter {SigGetParameter};

// specialized signal class for browsing
struct browseParameterSignal:public ParameterSignal
{
public:
	float sigGetParameter (float & vv, Parameter *pp, VitisObject *oo);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\struct browsePlugin
///			\brief Describe a virtual browser that will remove organs from the current simulated plant \n
///			and prepare the plant reaction to browsing
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct browsePlugin : public VProcess
{
public :
	browsePlugin (const std::string& f, const void *p);
	virtual ~browsePlugin(){;};
	void setPlant (Plant  *p) {plant = (PlantAMAP*)p;};
	Plant *getPlant () {return plant;};
	void process_event(EventData *msg);
    float StartBrowsing(){return startBrowsing;};
//	void setParameterPlugin (ParameterPlugin *p){l = p;};
//	void setDecompPlugin (DecompPlugin *p){d = p;};


protected :
    bool initBrowseParam (const std::string& f);
	void computeBrowsingMap (float top_extern);
	void computeBrowsing (float top_extern);
    void computeMap (float top_extern);
    void removeBrancs();
	void createRelayBud (BrancAMAP *b, bool basitone=false);
	void createAxillaryBud (BrancAMAP *b);
	void createBud (double phyAge, BrancAMAP *bearer);
	Voxel *findNeighbour (Voxel *v, double cumulativeRepulsion);
    void computePhotosyntheticEfficiency();
    void outputBrowsingResults();
    int potentialBrowsingCapacity (BrowseData *b);
    vector<Voxel*> computeCandidateList ();
	void writeOPF(const std::string &finame);
	void resetContent(Plant *p);

	PlantAMAP *plant;
	BrancAMAP *curBranc;

//	ParameterPlugin *l;
//	DecompPlugin *d;
	PlantPlugin *p;
	PlantDeletePlugin *pp;

    std::string browseParameterFileName;
    bool firstrun = true;
	int nbBrowsingTryThreshold;
	int nbBrowsedOrgansThreshold;
	float budBrowsingProba;
	float browsingMaxHeight;
	float browsedLeafMaxRatio;
	int leafPhyAge;
	int woodMaxPhyAge;
	int browsedBudIndex;
	int browsedLeafIndex;
	int browsedLeafAxillaryBudPhyAge;
	float protectionSensitivity;
	int intermediateOpf;
	bool basitony;
	float startBrowsing;
	int maxOrgansBrowsedPerTry;

	int leavesNumber;

	VoxelSpace *voxelSpace;
	int iCenter, jCenter;
	void updatePlantAndVoxels (GeomElemCone *e, vector<Voxel*> vv);
	GeomElemCone * browse (Voxel *v, int *nbBrowsedOrgans);

	MIRScript *MIR;
	bool withMIR=false;
	std::string MIRParam, MIRScene, MIRFolder;
	float MIRStart;
	int MIRBrowsingRate;
};

extern "C" BROWSE_EXPORT  browsePlugin * StartPlugin (const std::string& f, const void *p)
{
	std::cout << "loading AmapSim browsePlugin module with parameter " << f << std::endl;

	return new browsePlugin (f, p) ;
};

