/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"


struct BTPlugin;



struct PositionPlugin: public subscriber<messageVitisElemPosition> {
	BTPlugin * btplugin_ptr;

	PositionPlugin (void *p) { subscribe(p);};
	virtual ~PositionPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct ParameterPlugin: public subscriber<messageAxeRef> {
	BTPlugin * btplugin_ptr;

	ParameterPlugin (void *p) { subscribe(p);};
	virtual ~ParameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct DiamPlugin: public subscriber<messageVitisElemDiameter> {
	BTPlugin * btplugin_ptr;

	DiamPlugin (void *p) { subscribe(p);};
	virtual ~DiamPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct LengthPlugin: public subscriber<messageVitisElemLength> {
	BTPlugin * btplugin_ptr;

	LengthPlugin (void *p) { subscribe(p);};
	virtual ~LengthPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct couple {
	couple (float _x, float _y) {x=_x;y=_y;};
	float x;
	float y;
};

class BTparameter {
public:
	vector<couple> v;
	float getValue (float _x);
};

enum geomType {length, diameter};
struct BTPlugin {
	BTPlugin(const std::string& f, void *p);
	virtual ~BTPlugin(){;};
	void reset();
	PositionPlugin *position;
	DiamPlugin *dp;
	LengthPlugin *lp;
	ParameterPlugin *parameter;

	bool isLeaf (int phy);
	bool isTiller (int phy);
	float computeBending (int phy, float pos, float elementLng, float branchLng, float time);
	float computeTorsion (int phy, float pos, float elementLng, float branchLng, float time);
	void shrink(const geomType& st, paramMsg *data);

	long maxLeafPhyAge;
	long minLeafPhyAge;
	float bendingCenter, angleBendingRatio;
	int finalBendingAngleNumber;
	BTparameter finalBendingAngle;
	BTparameter bendingWidth;
	BTparameter totalTorsionAngle;
	float beginPositionTorsion;
	float CenterPositionTorsion;
	long maxTillerPhyAge;
	long minTillerPhyAge;
	BTparameter tillerStraighteningAngle;
	BTparameter tillerStraighteningArea;

	float leafShrinkingBeginningAge;
	float leafShrinkingTime;
	float leafWidthShrinkingRatio;
	float leafLengthShrinkingRatio;
};
extern "C" BT_EXPORT  BTPlugin * StartPlugin (const std::string& f, void *p)
{
	std::cout << "loading AxeBendingTorsionPlugin module with parameter " << f << std::endl;

	return new BTPlugin (f, p) ;
}
