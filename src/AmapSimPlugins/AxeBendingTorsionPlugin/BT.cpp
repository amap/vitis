/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <math.h>
#include <iostream>
#include <fstream>
using namespace std;

#include "Plant.h"
#include "PlantAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "BrancAMAP.h"
#include "externBT.h"
#include "BT.h"
#include "GrowthUnitAMAP.h"
#include "GrowthEngineAMAP.h"
#include "GeomBrancAMAP.h"
#include "Bud.h"
#include "UtilMath.h"






void ParameterPlugin::on_notify(const state& st, paramMsg *data)
{

    paramAxisRef * p = (paramAxisRef*)data;

	if (btplugin_ptr->isTiller(p->b->phyAgeInit))
	{
		// straightening angle
		if (p->numVar == rgredres)
		{
			float age = Scheduler::instance()->getTopClock() - p->b->getBud()->birthTime;
			p->var = btplugin_ptr->tillerStraighteningAngle.getValue(age);
		}
		// straightening area
		else if (p->numVar == rgplagredre)
		{
			float age = Scheduler::instance()->getTopClock() - p->b->getBud()->birthTime;
			p->var = btplugin_ptr->tillerStraighteningArea.getValue(age);
		}
		return;
	}


	if (!btplugin_ptr->isLeaf (p->b->phyAgeInit))
		return;

	if (p->numVar == rgyoung)
		p->var = 1000000;
	else if (p->numVar == rgconicite)
		p->var = 0;

}

/**
 *
 *
 * This function get called at the last stage will add lateral deviation
 * according to wind strength and branch initial direction
 *
 *
 *
 */
void PositionPlugin::on_notify(const state& st, paramMsg *data)
{

    paramElemPosition * p = (paramElemPosition*)data;

    GeomElemCone *gec = (GeomElemCone *)p->gec;

    GeomBrancAMAP * geomBranc = (GeomBrancAMAP *)gec->getPtrBrc();

    BrancAMAP *b = (BrancAMAP *)geomBranc->getBranc();

    //Retrieve phyAge
    int phyAge = (int)b->getCurrentGu()->phyAgeGu;

	if (!btplugin_ptr->isLeaf (phyAge))
		return;

	float angle;
	float pos, lng=0;

	// compute position of current element and length of the leaf
	pos = b->getCurrentEntnIndexWithinBranch() * gec->getLength();
	lng = b->getTestNumberWithinBranch() * gec->getLength();

	Matrix4 * current_triedron_pt = &p->p;

    //??
    Matrix4 & current_triedron = *current_triedron_pt;

    //??
    Vector3 translation = current_triedron.getTranslation();

    //Move to origin
    current_triedron = Matrix4::translation(-translation) * current_triedron;

	angle = btplugin_ptr->computeBending (phyAge, pos, gec->getLength(),lng, Scheduler::instance()->getTopClock()-b->getBud()->birthTime);

	// compute a horizontal rotation axis that is orthogonal to main direction and vertical
	Matrix4 matRot;
	geomBranc->determine_plagio (matRot, current_triedron.getMainVector(), Vector3(0,0,-1));
	Vector3 v=matRot.getNormalVector();

	// apply bending
	current_triedron = Matrix4::axisRotation(v,angle) * current_triedron;

	angle = btplugin_ptr->computeTorsion (phyAge, pos, gec->getLength(), lng, Scheduler::instance()->getTopClock()-b->getBud()->birthTime);
	// apply torsion
	current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),angle) * current_triedron;

	//Go back to original position
	current_triedron = Matrix4::translation(translation) * current_triedron;

	p->set(gec, current_triedron);

}


/**
 *
 *
 *
 *
 *
 */
void DiamPlugin::on_notify(const state& st, paramMsg *data)
{
	btplugin_ptr->shrink (diameter, data);
}

void LengthPlugin::on_notify(const state& st, paramMsg *data)
{
	btplugin_ptr->shrink (length, data);
}


void BTPlugin::shrink(const geomType& st, paramMsg *data)
{
    paramElemGeom * p = (paramElemGeom*)data;

    GeomElemCone *gec = (GeomElemCone *)p->gec;

    GeomBrancAMAP * geomBranc = (GeomBrancAMAP *)gec->getPtrBrc();

    BrancAMAP *b = (BrancAMAP *)geomBranc->getBranc();

    //Retrieve phyAge
    int phyAge = (int)b->getCurrentGu()->phyAgeGu;

	if (!isLeaf (phyAge))
		return;

	//Retrieve Leaf Age (actual time - birth time)
	float age, begin, time;

	age = Scheduler::instance()->getTopClock() - b->getCurrentEntn()->instant;

	begin = leafShrinkingBeginningAge;
	time = leafShrinkingTime;
	if (age < begin)
		return;

	float ratio;
	float size = p->v;

	// is length or width computing ?
	if (st == diameter)
		ratio = leafWidthShrinkingRatio;
	else
		ratio = leafLengthShrinkingRatio;

	if (age > time + begin)
		size *= ratio;
	else
		size *= (ratio-1.) * age / time + (time - (ratio-1.)*begin) / time;

	p->v = size;
}


bool BTPlugin::isLeaf(int phy)
{
	if (   phy > maxLeafPhyAge
		|| phy < minLeafPhyAge)
		return false;

	return true;
}

bool BTPlugin::isTiller(int phy)
{
	if (   phy > maxTillerPhyAge
		|| phy < minTillerPhyAge)
		return false;

	return true;
}

float BTPlugin::computeBending(int phy, float pos, float elementLng, float branchLng, float time)
{
	float angle;
	float maxBendingBegin = bendingCenter * branchLng - bendingWidth.getValue(time) * branchLng / 2;
	if (pos <= maxBendingBegin)
		return 0;

	float maxBendingEnd = bendingCenter * branchLng + bendingWidth.getValue(time) * branchLng / 2;
	if (pos <= maxBendingEnd)
	{
		float totalAngle = angleBendingRatio * finalBendingAngle.getValue(time);
		angle = totalAngle * elementLng / (bendingWidth.getValue(time) * branchLng);
		return angle;
	}
	else
	{
		float totalAngle = (1-angleBendingRatio) * finalBendingAngle.getValue(time);
		angle = totalAngle * elementLng / (branchLng - maxBendingEnd);
		return angle;
	}
}

float BTPlugin::computeTorsion(int phy, float pos, float elementLng, float branchLng, float time)
{
	float angle;
	float torsionBegin = beginPositionTorsion * branchLng;

	if (pos <= torsionBegin)
		return 0;

	float maxTorsionPos = CenterPositionTorsion * branchLng;
	float maxTorsionAngle = 2.* totalTorsionAngle.getValue(time) / (branchLng - torsionBegin);
	if (pos <= maxTorsionPos)
		angle = maxTorsionAngle * (pos - torsionBegin) / (maxTorsionPos - torsionBegin);
	else
		angle = maxTorsionAngle * (branchLng - pos) / (branchLng - maxTorsionPos);

	angle *= elementLng;

	return angle;
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
BTPlugin::BTPlugin(const std::string& f, void *p) {

    //child -> parent binding PTR
	position = new PositionPlugin (p);
    position->btplugin_ptr = this;
	dp = new DiamPlugin (p);
    dp->btplugin_ptr = this;
	lp = new LengthPlugin (p);
    lp->btplugin_ptr = this;
	parameter = new ParameterPlugin (p);
    parameter->btplugin_ptr = this;

	maxLeafPhyAge = 999;
	minLeafPhyAge = 1;

	fstream fic;
	string s;
	int nb;
	float x, y;
	fic.open(f.c_str(), ios_base::in);

	fic >> s >> maxLeafPhyAge;
	fic >> s >> minLeafPhyAge;
	fic >> s >> bendingCenter;
	fic >> s >> angleBendingRatio;
	fic >> s >> nb;
	for (int i=0; i<nb; i++)
	{
		fic >> x >> y;
		y *= M_PI / 180.;
		finalBendingAngle.v.push_back(couple(x,y));
	}
	fic >> s >> nb;
	for (int i=0; i<nb; i++)
	{
		fic >> x >> y;
		bendingWidth.v.push_back(couple(x,y));
	}
	fic >> s >> nb;
	for (int i=0; i<nb; i++)
	{
		fic >> x >> y;
		y *= M_PI / 180.;
		totalTorsionAngle.v.push_back(couple(x,y));
	}
	fic >> s >> beginPositionTorsion;
	fic >> s >> CenterPositionTorsion;

	fic >> s >> maxTillerPhyAge;
	fic >> s >> minTillerPhyAge;
	fic >> s >> nb;
	for (int i=0; i<nb; i++)
	{
		fic >> x >> y;
		y *= M_PI / 180.;
		tillerStraighteningAngle.v.push_back(couple(x,y));
	}
	fic >> s >> nb;
	for (int i=0; i<nb; i++)
	{
		fic >> x >> y;
		tillerStraighteningArea.v.push_back(couple(x,y));
	}
	if (fic.eof())
	{
		leafShrinkingBeginningAge = 999999;
	}
	else
	{
		fic >> s >> leafShrinkingBeginningAge;
	}
	if (fic.eof())
	{
		leafShrinkingTime = 0;
	}
	else
	{
		fic >> s >> leafShrinkingTime;
	}
	if (fic.eof())
	{
		leafWidthShrinkingRatio = 1;
	}
	else
	{
		fic >> s >> leafWidthShrinkingRatio;
	}
	if (fic.eof())
	{
		leafLengthShrinkingRatio = 1;
	}
	else
	{
		fic >> s >> leafLengthShrinkingRatio;
	}

	fic.close();
}

float BTparameter::getValue(float _x)
{
	int i;
	for (i=0; i<v.size(); i++)
	{
		if (v.at(i).x >= _x)
			break;
	}
	if (i == v.size())
		return v.at(v.size()-1).y;
	else if (i == 0)
		return v.at(0).y;
	else
	{
		float x1, x2, y1, y2;
		x1 = v.at(i-1).x;
		y1 = v.at(i-1).y;
		x2 = v.at(i).x;
		y2 = v.at(i).y;
		return y1 + (_x - x1) * (y2 - y1) / (x2 - x1);
	}
}

