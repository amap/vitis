/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <math.h>
#include <iostream>
#include <fstream>
using namespace std;

#include "Plant.h"
#include "PlantAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "BrancAMAP.h"
#include "externWind.h"
#include "wind.h"
#include "GrowthUnitAMAP.h"
#include "GrowthEngineAMAP.h"
#include "GeomBrancAMAP.h"
#include "Bud.h"






/////////////////////////////////////////////////////
//
// This methods get called in geometry context, when <Length> parameter
// is retrieved.
//
// We can override AmapSim normal behavior here, or just let
// the <Length> parameter as is.
//
/////////////////////////////////////////////////////
void LengthPlugin::on_notify (const state& , paramMsg * data)
{
	windplugin_ptr->computeSize (data);
}


float WindPlugin::computeSize (paramMsg * data)
{
    paramElemGeom * p = (paramElemGeom*)data;
	float coef = windSizeEffect * windStrength;
	if (coef > .4)
		coef = .4;

	if (windModel == 1)
		p->v *= (1-coef);
	else
	{
		GeomElemCone *gec = (GeomElemCone *)p->gec;

		GeomBrancAMAP * geomBranc = (GeomBrancAMAP *)gec->getPtrBrc();

		BrancAMAP *b = (BrancAMAP *)geomBranc->getBranc();

		if (b->getCurrentHierarc()->ordre == 0)
			p->v *= (1-coef);
		else
		{
			int phyAge = (int)b->getCurrentGu()->phyAgeGu;
			if (phyAge < maxBrancPhyAge)
				p->v *= (1-2*coef);
			else
				p->v *= (1-2.5*coef);
		}
	}
	return p->v;
}



/////////////////////////////////////////////////////
//
//
// This methods get called in geometry context, when <Diameter> parameter
// is retrieved.
//
/////////////////////////////////////////////////////
void DiameterPlugin::on_notify (const state& , paramMsg * data)
{
	windplugin_ptr->computeSize (data);
}









/**
 *
 *
 * This function get called at the last stage will add lateral deviation
 * according to wind strength and branch initial direction
 *
 *
 *
 */
void PositionPlugin::on_notify(const state& st, paramMsg *data) {

    paramElemPosition * p = (paramElemPosition*)data;

    GeomElemCone *gec = (GeomElemCone *)p->gec;

    GeomBrancAMAP * geomBranc = (GeomBrancAMAP *)gec->getPtrBrc();

    BrancAMAP *b = (BrancAMAP *)geomBranc->getBranc();

    //Retrieve phyAge
    int phyAge = (int)b->getCurrentGu()->phyAgeGu;
	float angle, orderEffect;


	if (phyAge < windplugin_ptr->maxBrancPhyAge)
	{

		Matrix4 * current_triedron_pt = &p->p;

        //??
        Matrix4 & current_triedron = *current_triedron_pt;

        //??
        Vector3 translation = current_triedron.getTranslation();

        //Move to origin
        current_triedron = Matrix4::translation(-translation) * current_triedron;

		float azimuth = current_triedron.getAzimuth();
		Vector3 main = current_triedron.getMainVector();
		main.normalize();
		float elevation = main[2];

		azimuth = (azimuth - 3.14) / 3.14;

        if (b->getCurrentHierarc()->ordre > 0)
            orderEffect = windplugin_ptr->branchEffect;
        else
            orderEffect = windplugin_ptr->trunkEffect;

//		if (fabs (azimuth) < .5)
		{

			if (windplugin_ptr->windModel == 1)
			{
				if(gec->getPosInGeomBranc() >= 0)
				{ // -1 -> we are on geom Init
				 // apply vertical deviation

					angle = 1. - fabs (azimuth);
					if (azimuth < 0)
                        angle = -angle;
					angle *= windplugin_ptr->windStrength * orderEffect * 0.1507;
					current_triedron = Matrix4::axisRotation(Vector3(0,0,1),angle) * current_triedron; //Rotation autour de la direction Y
				}
			}
			else
			{
				angle = (2. - fabs (elevation)) * .01;
				angle *= windplugin_ptr->windStrength * orderEffect;
				current_triedron = Matrix4::axisRotation(Vector3(0,1,0),angle) * current_triedron; //Rotation autour de la direction Y
			}
		}

		if (windplugin_ptr->windModel == 1)
		{
			// Apply HORIZONTAL deviation First model
            angle = .15 * (elevation);
		}
		else
		{
			// Apply HORIZONTAL deviation Second model
			azimuth = current_triedron.getAzimuth();
			azimuth = (azimuth-3.14)*2/3.14;
			if (azimuth > 1)
				angle = .05 * (2-azimuth);
			else if (azimuth < -1)
				angle = .05 * (-2-azimuth);
			else
				angle = .05 * azimuth;
		}


		angle *= windplugin_ptr->windStrength * orderEffect;
		current_triedron = Matrix4::axisRotation(Vector3(0,1,0),angle) * current_triedron; //Rotation around vertical

        //Go back to original position
		current_triedron = Matrix4::translation(translation) * current_triedron;

		p->set(gec, current_triedron);

    }
}




/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
WindPlugin::WindPlugin(const std::string& f, void *p) {

	plant_ptr = (Plant *)p;

    //child -> parent binding PTR
	length = new LengthPlugin (p);
    length->windplugin_ptr = this;
	diam = new DiameterPlugin (p);
    diam->windplugin_ptr = this;
	position = new PositionPlugin (p);
    position->windplugin_ptr = this;

	maxBrancPhyAge = 78;
	windSizeEffect = .3;
	windStrength = .5;
	trunkEffect = 0;
	branchEffect = 1;

	fstream fic;
	string s;
	fic.open(f.c_str(), ios_base::in);

	fic >> s >> maxBrancPhyAge;
	fic >> s >> windSizeEffect;
	fic >> s >> windStrength;
	fic >> s >> windModel;
	fic >> s >> trunkEffect;
	fic >> s >> branchEffect;
	windStrength /= 100.;

    windModel = 1;

	fic.close();
}

