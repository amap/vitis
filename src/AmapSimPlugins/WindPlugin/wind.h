/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"


struct WindPlugin;



struct PositionPlugin: public subscriber<messageVitisElemPosition> {
	WindPlugin * windplugin_ptr;

	PositionPlugin (void *p) { subscribe(p);};
	virtual ~PositionPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct LengthPlugin:public subscriber<messageVitisElemLength> {
	WindPlugin * windplugin_ptr;
	LengthPlugin (void *p) { subscribe(p);};
	virtual ~LengthPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};


struct DiameterPlugin:public subscriber<messageVitisElemDiameter> {
	WindPlugin * windplugin_ptr;
	DiameterPlugin (void *p) { subscribe(p);};
	virtual ~DiameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct WindPlugin {
	WindPlugin(const std::string& f, void *p);
	virtual ~WindPlugin(){;};
	void reset();
	float computeSize (paramMsg * data);
	DiameterPlugin *diam;
	LengthPlugin *length;
	PositionPlugin *position;

	Plant * plant_ptr; //Used for mersenne
	double windSizeEffect;
	long maxBrancPhyAge;
	double windStrength;
	long windModel;
	double trunkEffect;
	double branchEffect;
};
extern "C" WIND_EXPORT  WindPlugin * StartPlugin (const std::string& f, void *p)
{
	std::cout << "loading WindPlugin module with parameter " << f << std::endl;

	return new WindPlugin (f, p) ;
}
