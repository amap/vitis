/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file timidity.h
///			\brief Definition of timidity simulation class
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
using namespace std;

#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "ParamFile.h"
#include "Voxel.h"


#include <vector>

struct timidityPlugin;



#include "defAMAP.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\struct BrowseEventData
///			\brief Data that is transmitted from one browser step to the next one
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct TimidityEventData: public EventData
{
public:
	TimidityEventData (int action){this->action = action;};
	int Action (){return action;};
	void setAction (int a){action = a;};
private:
	int action;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\struct FloatData
///			\brief Simple floating point data that is able to output (for OPF purpose)
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct FloatData : public PrintableObject
{
public :
	float val;
	std::string to_string ()
	{
		std::ostringstream oss;
		// �crire la valeur dans le flux
		oss << val ;
		// renvoyer une string
		return oss.str();
	};
};

struct PlantPlugin:public subscriber<messageVitisPlant> {
	timidityPlugin * timidityplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	timidityPlugin * timidityplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct TIMIDITY_EXPORT LengthPlugin:public subscriber<messageVitisElemLength> {
	timidityPlugin * timidityplugin_ptr;

	LengthPlugin (Plant *p) {subscribe(p);};
	virtual ~LengthPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\struct timidityPlugin
///			\brief Describe a virtual timidity that will stop growth from the current simulated axe \n
///			if there is something belonging to another plant in its neighbourhood
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
struct timidityPlugin : public VProcess
{
public :
	timidityPlugin (const std::string& f, const void *p);
	virtual ~timidityPlugin(){;};
	void setPlant (Plant  *p) {plant = (PlantAMAP*)p;};
	Plant *getPlant () {return plant;};
	void process_event(EventData *msg);
	void setLentgthPlugin (LengthPlugin *p){l = p;};
	float MortalityLevel (){return mortalityLevel;};
	float MortalitySensivity (){return mortalitySensivity;};
	int WoodMaxPhyAge () {return woodMaxPhyAge;};

protected :
	void computeTimidityMap (float top_extern);
	void computeTimidity (float top_extern);
	void stopBrancs();
	bool initTimidityParam (const std::string& f);

	PlantAMAP *plant;
	BrancAMAP *curBranc;

	PlantPlugin *p;
	PlantDeletePlugin *pp;
	LengthPlugin *l;

	VoxelSpace *voxelSpace;
	int iCenter, jCenter;

	int woodMaxPhyAge;
	float mortalitySensivity;
	float maxDelay;

	float mortalityLevel;

};

extern "C" TIMIDITY_EXPORT  timidityPlugin * StartPlugin (const std::string& f, const void *p)
{
	std::cout << "loading AmapSim timidityPlugin module with parameter " << f << std::endl;

	return new timidityPlugin (f, p) ;
};

