/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifndef __EXPORTTIMIDITY
#define __EXPORTTIMIDITY


#pragma warning ( disable : 4251 )

#if !defined STATIC_LIB
	#if defined WIN32

			#ifdef TIMIDITY_EXPORTS
				#define TIMIDITY_EXPORT __declspec(dllexport)
			#else
				#define TIMIDITY_EXPORT __declspec(dllimport)
			#endif

	#else
		#define TIMIDITY_EXPORT
	#endif

#else
		#define TIMIDITY_EXPORT
#endif



#endif
