/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "Output.h"
#include "PlantAMAP.h"
#include "ObserverManager.h"

#ifdef WIN32
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "AmapSimMod.h"
#include "Bud.h"
#include "PlantAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "externTimidity.h"
#include "timidity.h"
#define STATIC_LIB
#include "GldsFormat.h"
#undef STATIC_LIB



void LengthPlugin::on_notify (const state& , paramMsg * data)
{

    //Cast message parameters
    paramElemGeom * p = (paramElemGeom*)data;

    //Retrieve branch
    BrancAMAP *b = (BrancAMAP*)p->gec->getPtrBrc()->getBranc();

	if (b->phyAgeInit > timidityplugin_ptr->WoodMaxPhyAge())
		return;

	if (b->getBud()->ordrez >= 2)
		return;

	float *length;
//	if ((length=(float*)b->getCurrentEntn()->getAdditionalData("length")) == 0)
	if (b->getCurrentGuIndex() == b->getGuNumber()-1)
	{
		length = new float;
		// trunk gets higher
		if (b->getBud()->ordrez == 0)
			p->v *= (1. + timidityplugin_ptr->MortalityLevel()*timidityplugin_ptr->MortalitySensivity());
		// branches get smaller
		else
			p->v *= (1. - timidityplugin_ptr->MortalityLevel()*timidityplugin_ptr->MortalitySensivity());
		*length = p->v;
		if (b->getCurrentEntn()->getAdditionalData("length") == 0)
			b->getCurrentEntn()->addAdditionalData ("length", length);
		else
			b->getCurrentEntn()->modifyAdditionalData ("length", length);
	}
	else if (b->getCurrentEntn()->getAdditionalData("length") == 0)
	{
		length = new float;
		// trunk gets higher
		if (b->getBud()->ordrez == 0)
			p->v *= (1. + timidityplugin_ptr->MortalityLevel()*timidityplugin_ptr->MortalitySensivity());
		// branches get smaller
		else
			p->v *= (1. - timidityplugin_ptr->MortalityLevel()*timidityplugin_ptr->MortalitySensivity());
		*length = p->v;
        b->getCurrentEntn()->addAdditionalData ("length", length);
	}
	else
	{
		length=(float*)b->getCurrentEntn()->getAdditionalData("length");
		p->v = *length;
	}

}

/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{

	paramPlant * d = (paramPlant*)data; //Retrieve msg params

	timidityplugin_ptr->setPlant (d->p);

	// to avoid complexity due to simplication
	if (d->p->getConfigData().cfg_topoSimp > 1)
        d->p->getConfigData().cfg_topoSimp = 1;
//	d->p->getConfigData().cfg_geoSimp = 0;

	TimidityEventData *e = new TimidityEventData(2);
	Scheduler::instance()->create_process(timidityplugin_ptr,e,1,103);
}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{

	timidityplugin_ptr->setPlant (NULL);
}

void timidityPlugin::process_event(EventData *msg)
{

    if (plant == NULL)
        return;

	TimidityEventData *e = (TimidityEventData*)msg;

	Scheduler *s = Scheduler::instance();
	float top_extern = s->getTopClock();

	if (e->Action() == 1)
	{
cout <<endl<< "Voxel space cleaning at " << top_extern << " for "<<plant->getName()<<endl;
		// clean voxel space at the end of the step
		voxelSpace->resetContent (plant);
		plant->getGeometry().clear();

		// register for next browsingmap step
		e->setAction (2);
		s->signal_event(this->getPid(),e,s->getTopClock()+1,103);
	}
	else if (e->Action() == 2)
	{

cout <<endl<< "begin timidity mapping at " << top_extern<<" for "<<plant->getName();
        int cfgsav = plant->getConfigData().cfg_geoSimp;
        plant->getConfigData().cfg_geoSimp = 0;
        if (cfgsav != 0)
            plant->getGeometry().HasToBeComputed(true);
		plant->computeGeometry();
		plant->getConfigData().cfg_geoSimp = cfgsav;
        if (cfgsav != 0)
            plant->getGeometry().HasToBeComputed(true);

//voxelSpace->displayContent();
		computeTimidityMap (top_extern);
//voxelSpace->displayContent();

		// register for timidity computing
		e->setAction (3);
		s->signal_event(this->getPid(),e,s->getTopClock(),102);
	}
	else if (e->Action() == 3)
	{

cout <<endl<< "begin timidity at " << top_extern<< " for "<<plant->getName()<<endl;
		stopBrancs();

//cout << "end timidity at " << top_extern <<endl;

		// register for cleaning at the end of the step
		e->setAction (1);
		s->signal_event(this->getPid(),e,s->getTopClock(),101);
	}
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
timidityPlugin::timidityPlugin(const std::string& f, const void *plant) {

	voxelSpace = VoxelSpace::instance();

	if (!initTimidityParam (f))
	{
		cout << "Failed reading timidity parameter file " << f << endl;
		cout << "File sould look like this :" << endl<<endl;
 		cout << endl<< "**** Failed reading timidity parameter file " << f << " ****" << endl;

		return;
	}

	this->plant = (PlantAMAP*)plant;
	p = new PlantPlugin (this->plant);
	p->timidityplugin_ptr = this;
	pp = new PlantDeletePlugin (this->plant);
	pp->timidityplugin_ptr = this;
	l = new LengthPlugin (this->plant);
	l->timidityplugin_ptr = this;

	srand(this->plant->getConfigData().randSeed);

	mortalityLevel = 0;
}


void timidityPlugin::stopBrancs ()
{
	int i, j, k;
	Vector3 pos = plant->getPosition();
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	Voxel *v;
	GeomElem *e,*e1;
	int nbBrancs=0;
	int nbStoppedBrancs= 0;
	bool bothered;

	// for each branch, check if there is another plant in the voxel containing the bud
	for (vector<GeomBranc *>::iterator ib=brcs.begin(); ib!=brcs.end(); ib++)
	{
		GeomBranc *g = *ib;
		vector<GeomElem *> ges = g->getElementsGeo ();
		vector<GeomElemCone*>::iterator it;
		e = g->getElementsGeo().at(g->getElementsGeo().size()-1);

		BrancAMAP *b = (BrancAMAP*)e->getPtrBrc()->getBranc();
		if (b->phyAgeInit > woodMaxPhyAge)
			continue;

		nbBrancs++;

		if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()+pos[0],
		       e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()+pos[1],
		       e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength(),
		       &i, &j, &k))
		{
			v = voxelSpace->addrAt (i, j, k);
			bothered = false;
			for (it=v->content.begin(); it != v->content.end(); it++)
			{
				e1 = (GeomElemCone*)*it;
				BrancAMAP *brb=((BrancAMAP*)(e1->getPtrBrc()->getBranc()));
				Bud *bud = ((BrancAMAP*)(e1->getPtrBrc()->getBranc()))->getBud();
				if (bud == NULL)
                    continue;

				if (   e1->getPtrBrc()->getBranc()->getPlant() != plant
//                    && !b->getBud()->death
				    && b->getBud()->getPhyAge() >= bud->getPhyAge())
				{
//				    b->getBud()->death = 1;
//                        b->getBud()->toBePruned = TOTALPRUNING;
//                        // compute the self pruning moment
//                        float lagdeath = 9999;
//                        float lagend = 9999;
//                        float pruningTime = Scheduler::instance()->getTopClock();
//                        lagdeath = b->getBud()->axisref->val1DParameter (rtlagdeath, (int) b->getBud()->getPhyAge(), b);
//                        lagend = b->getBud()->axisref->val1DParameter (rtlagend, (int) b->getBud()->getPhyAge(), b);
//                        lagend += (float)(2.*EPSILON);
//                        if (lagend < lagdeath)
//                            pruningTime += lagend;
//                        else
//                            pruningTime += lagdeath;
//                        int aid = Scheduler::instance()->getActionIndex (b->getBud()->process_id);
//                        Action ai = Scheduler::instance()->getAction (b->getBud()->process_id);
//                        Scheduler::instance()->changeAction (aid, ai.pid, pruningTime, ai.priority, NULL);
//                        nbStoppedBrancs++;
//                        break;
					if (b->getBud()->death == 0)// else stop current bud growth, put next event after next timidity checking
					{
                        b->getBud()->deathInstant = Scheduler::instance()->getTopClock();
                        b->getBud()->death = 1;
                        b->getBud()->toBePruned = WAITING;
                        // postpone selfpruning
                        int aid = Scheduler::instance()->getActionIndex (b->getBud()->process_id);
                        if (aid >= 0)
                        {
                            try
                            {
                                Action ai = Scheduler::instance()->getAction (b->getBud()->process_id);
                                float newTime = ai.time;
                                newTime += 1;
                                Scheduler::instance()->changeAction (aid, ai.pid, newTime, ai.priority, NULL);
                                nbStoppedBrancs++;
                            } catch (Vitis::CException e)
                            {
                                std::cout <<e.what()<<std::endl;
                            };
                        }
					}
					// if bud has been sleeping for maxDelay make it dead and start selfpruning process
					if (   b->getBud()->death == 1
                        && Scheduler::instance()->getTopClock() - b->getBud()->deathInstant >= maxDelay)
                    {
                        b->getBud()->toBePruned = TOTALPRUNING;
                        // compute the self pruning moment
                        float lagdeath = 9999;
                        float lagend = 9999;
                        float pruningTime = Scheduler::instance()->getTopClock();
                        lagdeath = b->getBud()->axisref->val1DParameter (rtlagdeath, (int) b->getBud()->getPhyAge(), b);
                        lagend = b->getBud()->axisref->val1DParameter (rtlagend, (int) b->getBud()->getPhyAge(), b);
                        lagend += (float)(2.*EPSILON);
                        if (lagend < lagdeath)
                            pruningTime += lagend;
                        else
                            pruningTime += lagdeath;
                        int aid = Scheduler::instance()->getActionIndex (b->getBud()->process_id);
                        if (aid >= 0)
                        {
                            try
                            {
                                Action ai = Scheduler::instance()->getAction (b->getBud()->process_id);
                                Scheduler::instance()->changeAction (aid, ai.pid, pruningTime, ai.priority, NULL);
                            } catch (Vitis::CException e)
                            {
                                std::cout <<e.what()<<std::endl;
                            };
                        }
//                        nbStoppedBrancs++;
                    }
                    bothered = true;
                    break;
				}
			}

			// if bud was stopped and is not anymore, make it grow again
			if (!bothered)
			{
                if (   b->getBud()->death == 1
                    && b->getBud()->toBePruned == WAITING)
                {
                    b->getBud()->death = 0;
                    b->getBud()->toBePruned = NOPRUNING;
                    int aid = Scheduler::instance()->getActionIndex (b->getBud()->process_id);
                    if (aid >= 0)
                    {
                            try
                            {
                                Action ai = Scheduler::instance()->getAction (b->getBud()->process_id);
                                float newTime = Scheduler::instance()->getTopClock()+1;
                                Scheduler::instance()->changeAction (aid, ai.pid, newTime, ai.priority, NULL);
                                nbStoppedBrancs--;
                            } catch (Vitis::CException e)
                            {
                                std::cout <<e.what()<<std::endl;
                            };
                    }
                }
			}
		}
	}

	if (nbBrancs > 0)
	{
		mortalityLevel = nbStoppedBrancs;
		mortalityLevel /= nbBrancs;
	}
	else
		mortalityLevel = 0;

cout << plant->getName() << " mortalityLevel : " << mortalityLevel << endl;
}

void timidityPlugin::computeTimidityMap (float top_extern){

	float voxelSize = voxelSpace->VoxelSize();

	// compute bounding box dimensions
	Vector3 min = plant->getGeometry().getMin();
	Vector3 max = plant->getGeometry().getMax();
	Vector3 pos = plant->getPosition();

	int x = ceil (max[0] / voxelSize + 0.5) - floor (min[0] / voxelSize - 0.5);
	int y = ceil (max[1] / voxelSize + 0.5) - floor (min[1] / voxelSize - 0.5);
	int z = ceil (max[2] / voxelSize + 0.5) - floor (min[2] / voxelSize - 0.5);
	int indXOrigin = floor ((pos[0]+min[0]) / voxelSize - 0.5);
	int indYOrigin = floor ((pos[1]+min[1]) / voxelSize - 0.5);
	int indZOrigin = floor ((pos[2]+min[2]) / voxelSize - 0.5);
	float xOrig = indXOrigin * voxelSize;
	float yOrig = indYOrigin * voxelSize;
	float zOrig = indZOrigin * voxelSize;

	voxelSpace->adjust (xOrig, yOrig, x, y, z);

	int zMax = 0;
	zMax = 0;
	int zMin = voxelSpace->ZSize();
	int ib, jb, kb;
	int ie, je, ke;
	GeomElemCone *e;
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
	vector<Voxel*> vv;
	for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
		GeomBranc *g = *b;

		BrancAMAP *br = (BrancAMAP*)g->getBranc();
		if (br->phyAgeInit > woodMaxPhyAge)
			continue;

		vector<GeomElem *> ges = g->getElementsGeo ();

		for (vector<GeomElem *>::iterator gj=ges.begin(); gj!=ges.end(); gj++)
		{
			vv.clear();
			e = (GeomElemCone*)*gj;
			if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+pos[0],
                                                   e->getMatrix().getTranslation().y()+pos[1],
                                                   e->getMatrix().getTranslation().z(),
                                                   &ib, &jb, &kb))
			{
				if (voxelSpace->findVoxel (e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()+pos[0],
				                           e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()+pos[1],
				                           e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength(),
				                           &ie, &je, &ke))
				{
					if (ib!=ie || jb!=je || kb!=ke)
					{
						voxelSpace->bresenham3D (ib, jb, kb, ie, je, ke, &vv);
					}
					else
						vv.push_back(voxelSpace->addrAt(ib, jb, kb));

					for (vector<Voxel*>::iterator it=vv.begin(); it!=vv.end(); it++)
					{
						Voxel *v = *it;
						v->content.push_back(e);
					}
				}
			}
		}
	}


//	voxelSpace->displayContent();

	voxelSpace->NbPlant(voxelSpace->NbPlant()+1);

}

bool timidityPlugin::initTimidityParam (const std::string& f) {
	ParamSet * param_H = new ParamSet(f);

	if (!param_H->Exists())
		return false;

	if (param_H->getParameter("voxelSize"))
		voxelSpace->VoxelSize(param_H->getParameter("voxelSize")->value());
	else
	{
		cout << "Failed finding voxelSize parameter in file " << f << endl;
		voxelSpace->VoxelSize(1);
		return false;
	}

	if (param_H->getParameter("woodMaxPhyAge"))
		woodMaxPhyAge = param_H->getParameter("woodMaxPhyAge")->value();
	else
	{
		cout << "Failed finding woodMaxPhyAge parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("mortalitySensivity"))
		mortalitySensivity = param_H->getParameter("mortalitySensivity")->value()/100;
	else
		mortalitySensivity = 0.;

	if (param_H->getParameter("maxDelay"))
		maxDelay = param_H->getParameter("maxDelay")->value();
	else
		maxDelay = 0.;

	return true;
}
