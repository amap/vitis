/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <math.h>
#include <string.h>
#include "Output.h"

#include <iostream>
#include <fstream>
using namespace std;

#include "PlantAMAP.h"
#include "Topology.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "externPinalep.h"
#include "pinalep.h"






/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{

	pinalepplugin_ptr->setPlant (NULL);
}

void PlantPlugin::on_notify (const state& , paramMsg * data)
{
    paramPlant * d = (paramPlant*)data; //Retrieve msg params

	pinalepplugin_ptr->setPlant ((PlantAMAP*)d->p);

	if (!pinalepplugin_ptr->initData())
	{
		std::cout << "error loading parameter file : " << pinalepplugin_ptr->ParameterFile() << std::endl;
		return;
	}

	Scheduler::instance()->create_process(pinalepplugin_ptr,NULL,0,100);
//	Scheduler::instance()->create_process(pinalepplugin_ptr,(EventData*)d->p,Scheduler::instance()->getHotStop(),1);

//	pinalepplugin_ptr->process_event(NULL);
}

bool pinalepPlugin::initData()
{
	currentClimate = 0;

	fstream  *fic = new fstream (parameterFile.c_str(), ios::in);
	if (!fic->is_open())
	{
		delete fic;
		return false;
	}

	string type;
	double polycyclisme=0, pousse=0, ramif=0, feuille=0;
	char str[2056];

	while (!fic->eof())
	{
		*fic >> type;
		if (type[0] == '#')
		{
			;
		}
		else if (type == "CLIMAT")
		{
			CLIMAT c;
			string cl;
			*fic >> cl;
			if (cl == "BON")
				climat.push_back(BON);
			else if (cl == "MOYEN")
				climat.push_back(MOYEN);
			else if (cl == "MAUVAIS")
				climat.push_back(MAUVAIS);
			else if (cl.length() > 0)
			{
				cout << "type de climat inconnu " << cl << " (BON, MOYEN, MAUVAIS)" << endl;
				fic->close();
				delete fic;
#ifdef WIN32
	Sleep(1000);
#endif
				return false;
			}
		}
		else if (type == "POLYCYCLISME")
		{
			*fic >> polycyclisme;
		}
		else if (type == "POUSSE")
		{
			*fic >> pousse;
		}
		else if (type == "RAMIF")
		{
			*fic >> ramif;
		}
		else if (type == "FEUILLE")
		{
			*fic >> feuille;
		}
		else if (type.length() > 0)
		{
			cout << "mot cle inconnu " << type << "(CLIMAT,POLYCLISME,POUSSE,RAMIF,FEUILLE)" << endl;
			fic->close();
			delete fic;
#ifdef WIN32
	Sleep(1000);
#endif
			return false;
		}

		// vider la ligne
		fic->getline(str, 2055);
	}

	fic->close();
	delete fic;

	initParameters (polycyclisme, pousse, ramif, feuille);

//	process_event(NULL);

	return true;
}

void pinalepPlugin::initParameters (double fpolycyclisme, double fpousse, double framif, double ffeuille)
{
	AmapSimVariable a;
	AxisReference * axeRef = (AxisReference*)plant_ptr->getParamData(string("AMAPSimMod"));

	// shoot length
	for (int i=0; i<(*axeRef)[rtneopn1].nb_pos_current; i++)
	{
		neopn1.push_back(couple((*axeRef)[rtneopn1].currentX[i], (*axeRef)[rtneopn1].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneopn1].currentY[i]*(1+fpousse/100.)));
	}
	for (int i=0; i<(*axeRef)[rtneopn2].nb_pos_current; i++)
	{
		neopn2.push_back(couple((*axeRef)[rtneopn2].currentX[i], (*axeRef)[rtneopn2].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneopn2].currentY[i]*(1+fpousse/100.)));
	}
	for (int i=0; i<(*axeRef)[rtneopna].nb_pos_current; i++)
	{
		neopna.push_back(couple((*axeRef)[rtneopna].currentX[i], (*axeRef)[rtneopna].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneopna].currentY[i]*(1+fpousse/100.)));
	}
	for (int i=0; i<(*axeRef)[rtneon1].nb_pos_current; i++)
	{
		neon1.push_back(couple((*axeRef)[rtneon1].currentX[i], (*axeRef)[rtneon1].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneon1].currentY[i]*(1+fpousse/100.)));
	}
	for (int i=0; i<(*axeRef)[rtneon2].nb_pos_current; i++)
	{
		neon2.push_back(couple((*axeRef)[rtneon2].currentX[i], (*axeRef)[rtneon2].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneon2].currentY[i]*(1+fpousse/100.)));
	}
	for (int i=0; i<(*axeRef)[rtneona].nb_pos_current; i++)
	{
		neona.push_back(couple((*axeRef)[rtneona].currentX[i], (*axeRef)[rtneona].currentY[i]*(1-fpousse/100.), (*axeRef)[rtneona].currentY[i]*(1+fpousse/100.)));
	}

	double min, max;
	// shoot polycylism
	for (int i=0; i<(*axeRef)[rtpolyini].nb_pos_current-1; i++)
	{
		min = (*axeRef)[rtpolyini].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rtpolyini].currentY[i]*(1+fpolycyclisme/100.);
		polyini.push_back(couple((*axeRef)[rtpolyini].currentX[i], min, max));
	}
	for (int i=0; i<(*axeRef)[rtmonocyc].nb_pos_current-1; i++)
	{
		min = (*axeRef)[rtmonocyc].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rtmonocyc].currentY[i]*(1+fpolycyclisme/100.);
		monocyc.push_back(couple((*axeRef)[rtmonocyc].currentX[i], min, max));
	}
	for (int i=0; i<(*axeRef)[rtpolycyc].nb_pos_current-1; i++)
	{
		min = (*axeRef)[rtpolycyc].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rtpolycyc].currentY[i]*(1+fpolycyclisme/100.);
		polycyc.push_back(couple((*axeRef)[rtpolycyc].currentX[i], min, max));
	}
	for (int i=0; i<(*axeRef)[rt2cyc].nb_pos_current; i++)
	{
		min = (*axeRef)[rt2cyc].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rt2cyc].currentY[i]*(1+fpolycyclisme/100.);
		twocyc.push_back(couple((*axeRef)[rt2cyc].currentX[i], min, max));
	}
	for (int i=0; i<(*axeRef)[rt3cyc].nb_pos_current; i++)
	{
		min = (*axeRef)[rt3cyc].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rt3cyc].currentY[i]*(1+fpolycyclisme/100.);
		threecyc.push_back(couple((*axeRef)[rt3cyc].currentX[i], min, max));
	}
	for (int i=0; i<(*axeRef)[rt4cyc].nb_pos_current; i++)
	{
		min = (*axeRef)[rt4cyc].currentY[i]*(1-fpolycyclisme/100.);
		max = (*axeRef)[rt4cyc].currentY[i]*(1+fpolycyclisme/100.);
		fourcyc.push_back(couple((*axeRef)[rt4cyc].currentX[i], min, max));
	}

	// leafLength
	leafLength.phy = 80;
	leafLength.min = (*axeRef).val1DParameter(lnginit, 80, NULL) * (1-ffeuille/100.);
	leafLength.max = (*axeRef).val1DParameter(lnginit, 80, NULL) * (1+ffeuille/100.);

	// branching
	int index = NB_VAR+repbaxil;
	for (int i=0; i<(*axeRef)[index].nb_pos_current; i++)
	{
		min = (*axeRef)[index].currentY[i]*(1-framif/100.);
		max = (*axeRef)[index].currentY[i]*(1+framif/100.);
		if (max > 1)
		{
			max = 1;
		}
		pbaxil.push_back(couple((*axeRef)[index].currentX[i], min, max));
	}
	index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+repbaxil;
	for (int i=0; i<(*axeRef)[index].nb_pos_current; i++)
	{
		min = (*axeRef)[index].currentY[i]*(1-framif/100.);
		max = (*axeRef)[index].currentY[i]*(1+framif/100.);
		if (max > 1)
		{
			max = 1;
		}
		pbaxilreit.push_back(couple((*axeRef)[index].currentX[i], min, max));
	}
}

double pinalepPlugin::sigmoid (double x, double min, double max, double slope)
{
	double y;

	// divide by 8 to have mean value for x = 4, which means 4 years memory
	y = min + (max - min) / (exp(-slope / 4. * x) + 1);

	return y;
}

void pinalepPlugin::process_event(EventData *msg)
{
    if (plant_ptr == NULL)
        return;

	int top_extern;
	Scheduler *s = Scheduler::instance();
	/* sets the next external eventData */
	top_extern = s->getTopClock();

	CLIMAT c;
	if (top_extern >= climat.size())
		c = climat[climat.size()-1];
	else
		c = climat[top_extern];

	if (c == MOYEN)
	{
		if (currentClimate < 0)
			currentClimate += 1;
		else if (currentClimate > 0)
			currentClimate -= 1;
	}
	else if (c == BON)
	{
		if (currentClimate <= 2)
			currentClimate += 2;
	}
	else if (c == MAUVAIS)
	{
		if (currentClimate >= -2)
			currentClimate -= 2;
	}

	cout << endl<<"pinalep a " << s->getTopClock() << " climate " << c << "  cumule " << currentClimate <<endl;

	adjustParameters (currentClimate);

	if (s->getTopClock()+1 <= s->getHotStop())
	{
		s->signal_event(this->getPid(),(EventData*)msg,s->getTopClock()+1,100);
	}
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
pinalepPlugin::pinalepPlugin(const std::string& f, void *ppp)
{

	plant_ptr = (PlantAMAP *)ppp;

    //child -> parent binding PTR
	p = new PlantPlugin (ppp);
    p->pinalepplugin_ptr = this;
	pp = new PlantDeletePlugin (ppp);
    pp->pinalepplugin_ptr = this;

	parameterFile = f;
	this->plant_ptr = (PlantAMAP*)p;
}

string pinalepPlugin::ParameterFile()
{
	return parameterFile;
}

void pinalepPlugin::adjustParameters (int climate)
{
	AxisReference * axeRef = (AxisReference*)plant_ptr->getParamData(string("AMAPSimMod"));
	// shoot length
	for (int i=0; i<(*axeRef)[rtneopn1].nb_pos_current; i++)
	{
		(*axeRef)[rtneopn1].set(neopn1[i].phy, sigmoid(climate, neopn1[i].min, neopn1[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtneopn2].nb_pos_current; i++)
	{
		(*axeRef)[rtneopn2].set(neopn2[i].phy, sigmoid(climate, neopn2[i].min, neopn2[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtneopna].nb_pos_current; i++)
	{
		(*axeRef)[rtneopna].set(neopna[i].phy, sigmoid(climate, neopna[i].min, neopna[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtneon1].nb_pos_current; i++)
	{
		(*axeRef)[rtneon1].set(neon1[i].phy, sigmoid(climate, neon1[i].min, neon1[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtneon2].nb_pos_current; i++)
	{
		(*axeRef)[rtneon2].set(neon2[i].phy, sigmoid(climate, neon2[i].min, neon2[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtneona].nb_pos_current; i++)
	{
		(*axeRef)[rtneona].set(neona[i].phy, sigmoid(climate, neona[i].min, neona[i].max, 1.));
	}

	// shoot polycylism
	for (int i=0; i<(*axeRef)[rtpolyini].nb_pos_current-1; i++)
	{
		(*axeRef)[rtpolyini].set(polyini[i].phy, sigmoid(climate, polyini[i].min, polyini[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtmonocyc].nb_pos_current-1; i++)
	{
		(*axeRef)[rtmonocyc].set(monocyc[i].phy, sigmoid(-climate, monocyc[i].min, monocyc[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rtpolycyc].nb_pos_current-1; i++)
	{
		(*axeRef)[rtpolycyc].set(polycyc[i].phy, sigmoid(climate, polycyc[i].min, polycyc[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rt2cyc].nb_pos_current; i++)
	{
//cout <<"2 cycles "<< axeRef->val1DParameter (rt2cyc, twocyc[i].phy, NULL, 0) << " devient "<< sigmoid(-climate, twocyc[i].min, twocyc[i].max, 1.) << " entre "<< twocyc[i].min<<" et "<< twocyc[i].max<<endl;
		(*axeRef)[rt2cyc].set(twocyc[i].phy, sigmoid(-climate, twocyc[i].min, twocyc[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rt3cyc].nb_pos_current; i++)
	{
		(*axeRef)[rt3cyc].set(threecyc[i].phy, sigmoid(-climate, threecyc[i].min, threecyc[i].max, 1.));
	}
	for (int i=0; i<(*axeRef)[rt4cyc].nb_pos_current; i++)
	{
		(*axeRef)[rt4cyc].set(fourcyc[i].phy, sigmoid(climate, fourcyc[i].min, fourcyc[i].max, 1.));
	}

	// leafLength
	double y = sigmoid(climate, leafLength.min, leafLength.max, 1.);
	(*axeRef)[lnginit].set(79, sigmoid(climate, leafLength.min, leafLength.max, 1.));

	// branching
	int index = NB_VAR+repbaxil;
	for (int i=0; i<(*axeRef)[index].nb_pos_current; i++)
	{
		(*axeRef)[index].set(pbaxil[i].phy, sigmoid(climate, pbaxil[i].min, pbaxil[i].max, 1.));
	}
	index = NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+repbaxil;
	for (int i=0; i<(*axeRef)[index].nb_pos_current; i++)
	{
		(*axeRef)[index].set(pbaxilreit[i].phy, sigmoid(climate, pbaxilreit[i].min, pbaxilreit[i].max, 1.));
	}
}

