/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "PlantAMAP.h"


struct pinalepPlugin;

struct PINALEP_EXPORT PlantPlugin:public subscriber<messageVitisPlant> {
	pinalepPlugin * pinalepplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct PINALEP_EXPORT PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	pinalepPlugin * pinalepplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};



struct couple {
	couple () {phy=0;min=0;max=1;};
	couple (int _phy, float _x, float _y) {min=_x;max=_y;phy=_phy;};
	int phy;
	float min;
	float max;
};

enum CLIMAT {MAUVAIS, MOYEN, BON};

struct sigmoidRange {
	double min, max, slope;
};

class PINALEP_EXPORT pinalepPlugin : public VProcess
{
public :
	pinalepPlugin(const std::string& f, void *p);
	virtual ~pinalepPlugin(){;};
	void process_event(EventData *msg);
	PlantAMAP *getPlant(){return plant_ptr;};
	void setPlant(PlantAMAP *p){plant_ptr=p;};
	bool initData();
	double sigmoid (double x, double min, double max, double slope);
	string ParameterFile();
	void adjustParameters (int climate);
	void initParameters (double fpolycyclisme, double fpousse, double framif, double ffeuille);

private:
	PlantPlugin *p;
	PlantDeletePlugin *pp;
	PlantAMAP * plant_ptr;

	vector<CLIMAT> climat;
	int currentClimate;
	string parameterFile;

	// shoot entn number
	vector<couple> neopn1;
	vector<couple> neopn2;
	vector<couple> neopna;

	vector<couple> neon1;
	vector<couple> neon2;
	vector<couple> neona;

	// leaf size
	couple leafLength;

	// polyclysm
	vector<couple> polyini;
	vector<couple> monocyc;
	vector<couple> polycyc;
	vector<couple> twocyc;
	vector<couple> threecyc;
	vector<couple> fourcyc;

	// branching
	vector<couple> pbaxilreit;
	vector<couple> pbaxil;

};
extern "C" PINALEP_EXPORT  pinalepPlugin * StartPlugin (const std::string& f, void *p)
{
	std::cout << "loading pinalepPlugin module with parameter " << f << std::endl;

	return new pinalepPlugin (f, p) ;
}
