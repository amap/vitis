/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
#include <sys/time.h>
using namespace std;

#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "ParamFile.h"
#include "externFW.h"


#include <vector>

class FWPlugin;



#include "defAMAP.h"

class FWEventData: public EventData
{
public:
	FWEventData (int action){this->action = action;};
	int Action (){return action;};
	void setAction (int a){action = a;};
private:
	int action; // 1:compute geometry; 2:read light
};


struct FW_EXPORT AmapSimParameterPlugin:public subscriber<messageAxeRef> {
	FWPlugin * FWplugin_ptr;

	AmapSimParameterPlugin (Plant *p) {subscribe(p);};
	virtual ~AmapSimParameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
	int getParameterValues (BrancAMAP *b, float *attenuation, float *deathThreshold, float *ramificationThreshold);
};

struct FW_EXPORT DigRParameterPlugin:public subscriber<messageParam> {
	FWPlugin * FWplugin_ptr;

	DigRParameterPlugin (Plant *p) {subscribe(p);};
	virtual ~DigRParameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
	int getParameterValues (Root *b, float *attenuation, float *deathThreshold, float *ramificationThreshold);
};

struct FW_EXPORT PlantPlugin:public subscriber<messageVitisPlant> {
	FWPlugin * FWplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct FW_EXPORT PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	FWPlugin * FWplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct FW_EXPORT EndGeomPlugin {
public:
	FWPlugin * FWplugin_ptr;

    EndGeomPlugin(void *p) {;};
    virtual ~EndGeomPlugin() {;};
    void on_notify();
    int getBrancCategory (BrancAMAP *b);
};

class FW_EXPORT FWPlugin : public VProcess
{
public :
	FWPlugin (const std::string& f, const void *p);
	virtual ~FWPlugin(){;};
	void setPlant (Plant  *p) {plant = (WholePlant*)p;};
	Plant *getPlant () {return plant;};
	void process_event(EventData *msg);
	void setAmapSimParameterPlugin (AmapSimParameterPlugin *p){al = p;};
	void setDigRParameterPlugin (DigRParameterPlugin *p){dl = p;};
	void outputRandomParameterFile (string name);

	// plant growth attenuation function parameters
	// aerial part is split in 3 categories (trunk, branch, short shoot according to phyage thresholds
	int trunkThreshold=-1;
	int branchThreshold=-1;
	int shortshootThreshold=-1;
	int leafThreshold=-1;

	// attenuation function
	// forbids ramification when growthRate is less than RamificationThreshold
	// cause death when growthrate is less than deathThreshold
	// moderates apical growth from 0 to attenuation between deathThreshold and ramificationThreshold
	// moderates apical growth from attenuation to 1 between ramificationThreshold and 1
	float trunkAttenuation=-1;
	float trunkDeathThreshold=-1;
	float trunkRamificationThreshold=-1;
	float branchAttenuation=-1;
	float branchDeathThreshold=-1;
	float branchRamificationThreshold=-1;
	float shortshootAttenuation=-1;
	float shortshootDeathThreshold=-1;
	float shortshootRamificationThreshold=-1;

	ParametricParameter rootTypesCategory;
	float taprootAttenuation=-1;
	float lateralrootAttenuation=-1;
	float finerootAttenuation=-1;
	float taprootDeathThreshold=-1;
	float lateralrootDeathThreshold=-1;
	float finerootDeathThreshold=-1;
	float taprootRamificationThreshold=-1;
	float lateralrootRamificationThreshold=-1;
	float finerootRamificationThreshold=-1;

	// environment parameters
	ParametricParameter waterContent;
	ParametricParameter light;
	float airCarbon=-1;

	// plant species parameters
	float Lmax=-1;
	float aPhotosynthesis=-1, KPhotosynthesis=-1;
	float rootAbsorbingLength=-1;
	float rhizoCylinderDiameter=-1;

	float startTime;

	float LAI, RAI;

	float growthRateBranch, growthRateRoot;

protected :
    bool initFWParam (const std::string& f);
	float computeLAI(float *s);
	float computeRAI(float *v);
	float computeH2O(float *v);
	void extractVectorBase(string f);
//	void extractVectorBase(string f);

	WholePlant *plant;

	AmapSimParameterPlugin *al;
	DigRParameterPlugin *dl;
	PlantPlugin *p;
	PlantDeletePlugin *pp;
	EndGeomPlugin *eg;
};

extern "C" FW_EXPORT  FWPlugin * StartPlugin (const std::string& f, const void *p)
{
	std::cout << "loading FWPlugin module with parameter " << f << std::endl;

	return new FWPlugin (f, p) ;
}
