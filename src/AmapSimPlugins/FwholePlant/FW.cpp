/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "PlantAMAP.h"
#include "ObserverManager.h"

#ifdef WIN32
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "WholePlant.h"
#include "Topology.h"
#include "Branc.h"
#include "GeomBrancAMAP.h"
#include "GeomRoot.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
//#include "DigRSignalInterface.h"
#include "corresp.h"
#include "GeomElemCone.h"
#include "externFW.h"
#include "FW.h"


int DigRParameterPlugin::getParameterValues (Root *b, float *attenuation, float *deathThreshold, float *ramificationThreshold)
{
    int cat = FWplugin_ptr->rootTypesCategory.value(b->Type(), 0);
    switch (cat)
    {
        case 0 :
            *attenuation = FWplugin_ptr->taprootAttenuation;
            *deathThreshold = FWplugin_ptr->taprootDeathThreshold;
            *ramificationThreshold = FWplugin_ptr->taprootRamificationThreshold;
            break;
        case 1 :
            *attenuation = FWplugin_ptr->lateralrootAttenuation;
            *deathThreshold = FWplugin_ptr->lateralrootDeathThreshold;
            *ramificationThreshold = FWplugin_ptr->lateralrootRamificationThreshold;
            break;
        default :
            *attenuation = FWplugin_ptr->finerootAttenuation;
            *deathThreshold = FWplugin_ptr->finerootDeathThreshold;
            *ramificationThreshold = FWplugin_ptr->finerootRamificationThreshold;
            break;
    }
    return cat;
}

void DigRParameterPlugin::on_notify (const state& st, paramMsg *data)
{
    //Cast message parameters
    paramParameter * p = (paramParameter *)data;

    //Retrieve branch
    Root * b = (Root *)p->o;

    if (b == 0)
        return;

    float top = Scheduler::instance()->getTopClock();

    float deathThreshold, attenuation, ramificationThreshold;

    if (p->p->Name().compare(0, 11, "growthSpeed") == 0)
    {
        int cat = getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        float v1, v2, d1, d2, coef;
        if (FWplugin_ptr->growthRateRoot < deathThreshold)
        {
            v1 = 0;
            v2 = 0;
            d2 = 1;
            d1 = 0;
        }
        else if (FWplugin_ptr->growthRateRoot < ramificationThreshold)
        {
            d1 = deathThreshold;
            d2 = ramificationThreshold;
            v1 = 0;
            v2 = attenuation;
        }
        else
        {
            d1 = ramificationThreshold;
            d2 = 1;
            v1 = attenuation;
            v2 = 1;
        }
        coef = v1 + (v2 - v1) * (FWplugin_ptr->growthRateRoot - d1)/(d2 - d1);
        p->v *= coef;
    }
//    else if (p->p->Name().compare(0, 18, "interRamifDistance") == 0)
//    {
//        int cat = getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
//        p->v *= ;
//    }
    else if (p->p->Name().compare(0, 13, "typeFrequency") == 0)
    {
        int cat = getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        if (FWplugin_ptr->growthRateRoot < ramificationThreshold)
            p->v = 0;
    }
    else if (p->p->Name().compare(0, 10, "deathProba") == 0)
    {
        int cat = getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
//cout << "cat proba "<< cat << endl;
        if (FWplugin_ptr->growthRateRoot < deathThreshold)
        {
            p->v = 0;
//cout << "dead "<<p->v<<" -> 0" << endl;
        }
    }

}

int AmapSimParameterPlugin::getParameterValues (BrancAMAP *b, float *attenuation, float *deathThreshold, float *ramificationThreshold)
{
    if (b->phyAgeInit < FWplugin_ptr->trunkThreshold)
    {
        *attenuation = FWplugin_ptr->trunkAttenuation;
        *deathThreshold = FWplugin_ptr->trunkDeathThreshold;
        *ramificationThreshold = FWplugin_ptr->trunkRamificationThreshold;
        return 0;
    }
    else if (b->phyAgeInit < FWplugin_ptr->branchThreshold)
    {
        *attenuation = FWplugin_ptr->branchAttenuation;
        *deathThreshold = FWplugin_ptr->branchDeathThreshold;
        *ramificationThreshold = FWplugin_ptr->branchRamificationThreshold;
        return 1;
    }
    else if  (b->phyAgeInit < FWplugin_ptr->shortshootThreshold)
    {
        *attenuation = FWplugin_ptr->shortshootAttenuation;
        *deathThreshold = FWplugin_ptr->shortshootDeathThreshold;
        *ramificationThreshold = FWplugin_ptr->shortshootRamificationThreshold;
        return 2;
    }
    else
    {
        *attenuation = 1;
        *deathThreshold = 0;
        *ramificationThreshold = 1;
        return 3;
    }
}

void AmapSimParameterPlugin::on_notify (const state& st, paramMsg *data)
{
    //Cast message parameters
    paramAxisRef * p = (paramAxisRef *)data;

    //Retrieve branch
    BrancAMAP * b = (BrancAMAP *)p->b;

    float top = Scheduler::instance()->getTopClock();

    float deathThreshold, attenuation, ramificationThreshold;

    if (   p->numVar == rtneon1
        /*|| p->numVar == rtprefn1*/)
    {
        if (top < FWplugin_ptr->startTime)
            return;

        getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);

        float d1, d2, v1, v2, coef;
        int cat;
        if (FWplugin_ptr->growthRateBranch < deathThreshold)
        {
            v1 = 0;
            v2 = 0;
            d2 = 1;
            d1 = 0;
        }
        else if (FWplugin_ptr->growthRateBranch < ramificationThreshold)
        {
            d1 = deathThreshold;
            d2 = ramificationThreshold;
            v1 = 0;
            v2 = attenuation;
        }
        else
        {
            d1 = ramificationThreshold;
            d2 = 1;
            v1 = attenuation;
            v2 = 1;
        }
        coef = v1 + (v2 - v1) * (FWplugin_ptr->growthRateBranch - d1)/(d2 - d1);
        p->var *= coef;
    }


// CAUTION this set of control is coded
//         assuning lateral production order is branch-leaf-reiteration
//         and both Z1 and Z2 are branched

    // initial branched first state proba
    else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpbinitret0
         || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret0
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpbinitret0
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret0)
    {
        getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        if (   FWplugin_ptr->growthRateBranch < ramificationThreshold
            && top >= FWplugin_ptr->startTime)
            p->var = 1;
    }
    // initial branched first state proba
    else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpbinitret1
         || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret1
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpbinitret1
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpbinitret1)
    {
        getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        if (   FWplugin_ptr->growthRateBranch < ramificationThreshold
            && top >= FWplugin_ptr->startTime)
            p->var = 0;
    }
    // remaining not branched proba
    else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_st0
         || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st0
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_st0
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st0)
    {
        getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        if (   FWplugin_ptr->growthRateBranch < ramificationThreshold
            && top >= FWplugin_ptr->startTime)
            p->var = 1;
    }
    // remaining first state proba
    else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_st1
         || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st1
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_st1
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st1)
    {
        getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        if (   FWplugin_ptr->growthRateBranch < ramificationThreshold
            && top >= FWplugin_ptr->startTime)
            p->var = 0;
    }
    // remaining second state proba
    else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_st2
         || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st2
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_st2
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_st2)
    {
        getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        if (   FWplugin_ptr->growthRateBranch < ramificationThreshold
            && top >= FWplugin_ptr->startTime)
            p->var = 0;
    }
    // transition not branched -> first state proba
    else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_0_1
         || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_0_1
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_0_1
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_0_1)
    {
        getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        if (   FWplugin_ptr->growthRateBranch < ramificationThreshold
            && top >= FWplugin_ptr->startTime)
            p->var = 0;
    }
    // transition first state -> not branched proba
    else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_1_0
         || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_1_0
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_1_0
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_1_0)
    {
        getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        if (   FWplugin_ptr->growthRateBranch < ramificationThreshold
            && top >= FWplugin_ptr->startTime)
            p->var = 1;
    }
    // transition second state -> not branched proba
    else if (   p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+rcpb_2_0
         || p->numVar == NB_VAR+NB_VAR_LP*0+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_2_0
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+rcpb_2_0
         || p->numVar == NB_VAR+NB_VAR_LP*2+NB_GLOBAL_LP_VAR+NB_ZONE_VAR+rcpb_2_0)
    {
        getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        if (   FWplugin_ptr->growthRateBranch < ramificationThreshold
            && top >= FWplugin_ptr->startTime)
            p->var = 1;
    }
    // living proba
    else if (p->numVar == rtpbvie)
    {
        getParameterValues (b, &attenuation, &deathThreshold, &ramificationThreshold);
        if (   FWplugin_ptr->growthRateBranch > 0
            && FWplugin_ptr->growthRateBranch < deathThreshold
            && top >= FWplugin_ptr->startTime)
            p->var = 0;
    }
}

int EndGeomPlugin::getBrancCategory (BrancAMAP *b)
{
    if (b->phyAgeInit < FWplugin_ptr->trunkThreshold)
    {
        return 0;
    }
    else if (b->phyAgeInit < FWplugin_ptr->branchThreshold)
    {
        return 1;
    }
    else if  (b->phyAgeInit < FWplugin_ptr->shortshootThreshold)
    {
        return 2;
    }
    else
    {
        return 3;
    }
}

void EndGeomPlugin::on_notify() {
	float top = Scheduler::instance()->getTopClock();

	if ((int)top == 1)
	{
        ofstream tfic ("targets.csv", std::ofstream::out | std::ofstream::trunc);

        if (!tfic.good())
        {
            perror ("targets.csv");
            return;
        }

        tfic << "water";
        for (int i=0; i<10; i++){
            tfic << " " << FWplugin_ptr->waterContent.value(i);
        }
        tfic << endl;
        tfic << "light";
        for (int i=0; i<10; i++){
            tfic << " " << FWplugin_ptr->light.value(i);
        }
        tfic << endl;
        tfic << "shoot1 " << FWplugin_ptr->trunkDeathThreshold<<" "<<FWplugin_ptr->trunkRamificationThreshold<<" "<<FWplugin_ptr->trunkAttenuation<<endl;
        tfic << "shoot2 " << FWplugin_ptr->branchDeathThreshold<<" "<<FWplugin_ptr->branchRamificationThreshold<<" "<<FWplugin_ptr->branchAttenuation<<endl;
        tfic << "shoot3 " << FWplugin_ptr->shortshootDeathThreshold<<" "<<FWplugin_ptr->shortshootRamificationThreshold<<" "<<FWplugin_ptr->shortshootAttenuation<<endl;
        tfic << "root1 " << FWplugin_ptr->taprootDeathThreshold<<" "<<FWplugin_ptr->taprootRamificationThreshold<<" "<<FWplugin_ptr->taprootAttenuation<<endl;
        tfic << "root2 " << FWplugin_ptr->lateralrootDeathThreshold<<" "<<FWplugin_ptr->lateralrootRamificationThreshold<<" "<<FWplugin_ptr->lateralrootAttenuation<<endl;
        tfic << "root3 " << FWplugin_ptr->finerootDeathThreshold<<" "<<FWplugin_ptr->finerootRamificationThreshold<<" "<<FWplugin_ptr->finerootAttenuation<<endl;
        tfic << "Lmax-K "<<FWplugin_ptr->Lmax<<" "<<FWplugin_ptr->KPhotosynthesis<<endl;

        tfic.close();
    }

    ofstream hfic;
    if ((int)top == 1)
        hfic.open ("history.csv", std::ofstream::out | std::ofstream::trunc);
    else
        hfic.open ("history.csv", std::ofstream::out | std::ofstream::app);

	if (!hfic.good())
	{
		perror ("history.csv");
		return;
	}

	vector<GeomBranc *> brcs = FWplugin_ptr->getPlant()->getGeometry().getGeomBrancStack();
	int cnt=1;
    for (vector<GeomBranc *>::iterator gb=brcs.begin(); gb!=brcs.end(); gb++, cnt++)
	{
 		GeomBranc *g = (GeomBranc*)*gb;

        if (g->getBranc()->getClassType() == "AMAPSimMod")
        {
            GeomBrancAMAP *ga = (GeomBrancAMAP *)g;
            BrancAMAP *ba = (BrancAMAP*)g->getBranc();
            ba->startPosit();
            ba->positOnLastEntnWithinBranc();
            double phyAgeGu = ba->getCurrentGu()->phyAgeGu;
            ba->endPosit();

            if (phyAgeGu >= FWplugin_ptr->leafThreshold-1)
                hfic<<cnt<<" leaf ";
            else
            {
                int cat = getBrancCategory (ba);
                switch (cat)
                {
                    case 0 :
                        hfic<<cnt<<" shoot1 ";
                    break;
                    case 1 :
                        hfic<<cnt<<" shoot2 ";
                    break;
                    case 2 :
                        hfic<<cnt<<" shoot3 ";
                    break;
                }
            }
            hfic << top<<" ";

            vector<GeomElem *> ges = ga->getElementsGeo ();
            GeomElemCone *e = (GeomElemCone*)ges[0];
            hfic << e->getMatrix().getTranslation().x()<<" ";
            hfic << e->getMatrix().getTranslation().y()<<" ";
            hfic << e->getMatrix().getTranslation().z()<<" ";

            e = (GeomElemCone*)ges[ges.size()-1];
            hfic << e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()<<" ";
            hfic << e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()<<" ";
            hfic << e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()<<" ";
            if (phyAgeGu >= FWplugin_ptr->leafThreshold-1)
            {
                float l = ga->computeLength();
                float d = ga->computeBottomDiam();
                hfic <<  l*d<<endl;
            }
            else
            {
                hfic << " -"<<endl;
            }
        } else
        {
            Root *b = (Root*)g->getBranc();
            GeomRoot *ga = (GeomRoot *)g;
            int cat = FWplugin_ptr->rootTypesCategory.value(b->Type(), 0);
            switch (cat)
            {
                case 0 :
                    hfic<<cnt<<" root1 ";
                break;
                case 1 :
                    hfic<<cnt<<" root2 ";
                break;
                default :
                    hfic<<cnt<<" root3 ";
                break;
            }

            hfic << top<<" ";

            vector<GeomElem *> ges = ga->getElementsGeo ();
            GeomElemCone *e = (GeomElemCone*)ges[0];
            hfic << e->getMatrix().getTranslation().x()<<" ";
            hfic << e->getMatrix().getTranslation().y()<<" ";
            hfic << e->getMatrix().getTranslation().z()<<" ";

            e = (GeomElemCone*)ges[ges.size()-1];
            hfic << e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength()<<" ";
            hfic << e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()<<" ";
            hfic << e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength()<<" ";

            float volume;
            if (ga->Length() > FWplugin_ptr->rootAbsorbingLength)
                volume = FWplugin_ptr->rootAbsorbingLength;
            else
                volume = ga->Length();
            volume *= M_PI * FWplugin_ptr->rhizoCylinderDiameter * FWplugin_ptr->rhizoCylinderDiameter / 4;

            hfic << volume<<endl;
        }
    }

    hfic.close();

}

/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{
    paramPlant * d = (paramPlant*)data; //Retrieve msg params

	FWplugin_ptr->setPlant (d->p);
	if (FWplugin_ptr->getPlant()->getConfigData().cfg_topoSimp > 1)
        FWplugin_ptr->getPlant()->getConfigData().cfg_topoSimp = 1;

	FWEventData *e = new FWEventData(1);
    Scheduler::instance()->create_process(FWplugin_ptr,e,1,103);
}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{
	FWplugin_ptr->setPlant (NULL);
}

void FWPlugin::process_event(EventData *msg)
{
    if (plant == NULL)
        return;
	FWEventData *e = (FWEventData*)msg;

	Scheduler *s = Scheduler::instance();

	/* sets the next external eventData */
	float top_extern = s->getTopClock();
	int top = (int) top_extern;
    plant->computeGeometry();
    eg->on_notify();

    float leafSurface;
    float rootVolume;
    LAI = computeLAI(&leafSurface);
//    RAI = computeRAI(&rootVolume);

    float C = leafSurface * airCarbon;
    float water = waterContent.value (top);
    float H2O = rootVolume * waterContent.value (top);
    H2O = computeH2O(&rootVolume);
    float toplight = light.value(top);
    if (toplight > Lmax)
        toplight = Lmax;

/*
    float sugar = (C < H2O ? C : H2O) * toplight * (1 - aPhotosynthesis * exp(-KPhotosynthesis*LAI));
    float maxSugar = (C < rootVolume ? C : rootVolume) * Lmax;
    float sugarBranch = sugar * H2O / (C + H2O);
    float sugarRoot = sugar * C / (C + H2O);
    float maxSugarBranch = maxSugar * rootVolume / (C + rootVolume);
    float maxSugarRoot = maxSugar * C / (C + rootVolume);

    growthRateBranch = sugarBranch / maxSugar * 1.5;
    growthRateBranch = growthRateBranch > 1 ? 1 : growthRateBranch;
    growthRateRoot = sugarRoot / maxSugar * 1.5;
    growthRateRoot = growthRateRoot > 1 ? 1 : growthRateRoot;
if (   isnan(growthRateBranch)
    || isnan(growthRateRoot))
    exit(0);
*/
    float sugar = (C < H2O ? C : H2O) * toplight * (1 - aPhotosynthesis * exp(-KPhotosynthesis*LAI));
    float maxSugar = (C < rootVolume ? C : rootVolume) * Lmax * (1 - aPhotosynthesis * exp(-KPhotosynthesis*LAI));
    float sugarBranch = sugar * H2O / (C + H2O);
    float sugarRoot = sugar * C / (C + H2O);
    float maxSugarBranch = maxSugar * rootVolume / (C + rootVolume);
    float maxSugarRoot = maxSugar * C / (C + rootVolume);

    // je ne comprends pas pourquoi j'ai ecrit ca dans la premiere version
    //growthRateBranch = sugarBranch / maxSugar * 1.5;
    growthRateBranch = sugarBranch / maxSugarBranch;
    growthRateBranch = growthRateBranch > 1 ? 1 : growthRateBranch;
    //growthRateRoot = sugarRoot / maxSugar * 1.5;
    growthRateRoot = sugarRoot / maxSugarRoot;
    growthRateRoot = growthRateRoot > 1 ? 1 : growthRateRoot;

cout << "t= "<<top_extern<<" H2O= "<<H2O<<" C= "<<C<< " grb= "<<growthRateBranch<<" grr= "<<growthRateRoot<<endl;
    if (   isnan(growthRateBranch)
        || isnan(growthRateRoot))
        exit(0);
    s->signal_event(this->getPid(),e,top_extern+1,101);
}


struct paramfw {
    float water[10];
    float light[10];
    float k;
    float att[6][3];
};

void generateRandomParameterValues(FWPlugin *plug)
{
    float r;
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    srand (ms);
        plug->startTime = 0;
		plug->trunkThreshold = 10;
		plug->branchThreshold = 20;
		plug->shortshootThreshold = 160;
		plug->leafThreshold = 100;
		plug->rootTypesCategory.set (0, 0, 0);
		plug->rootTypesCategory.set (1, 0, 1);
		plug->rootTypesCategory.set (2, 0, 2);
//    fic << "rootTypesCategory ParametricParameter 0 3 0 0 1 0 0 1 0 1 0 1 2 0 1 0 2"<<endl<<endl;

/* plastic plant  with
    unsensible trunk
    lateral branch sensible in apical growth
    un sensible short axes
    sensible taproot in apical growth
    unsensible lateral root
    fine root sensible to death
    cout << "plastic parameter file, random climate";
		plug->trunkAttenuation = 1;
		plug->branchAttenuation = 0;
		plug->shortshootAttenuation = 1;
		plug->trunkDeathThreshold = 0.1;
		plug->branchDeathThreshold = 0.1;
		plug->shortshootDeathThreshold = 0.1;
		plug->trunkRamificationThreshold = 0.2;
		plug->branchRamificationThreshold = 0.2;
		plug->shortshootRamificationThreshold = 0.2;

		plug->taprootAttenuation = 0;
		plug->lateralrootAttenuation = 1;
		plug->finerootAttenuation = 1;
		plug->taprootDeathThreshold = 0.1;
 		plug->lateralrootDeathThreshold = 0.1;
		plug->finerootDeathThreshold = 0.3;
		plug->taprootRamificationThreshold = 0.2;
		plug->lateralrootRamificationThreshold = 0.2;
		plug->finerootRamificationThreshold = plug->finerootDeathThreshold + 0.1;
/* */
/* totaly random plant */
    cout << "random parameter file";
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.2 + 0.8*r;
		plug->trunkAttenuation = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.2 + 0.8*r;
		plug->branchAttenuation = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.2 + 0.8*r;
		plug->shortshootAttenuation = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.1 + r*0.4;
		plug->trunkDeathThreshold = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.1 + r*0.4;
		plug->branchDeathThreshold = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.1 + r*0.4;
		plug->shortshootDeathThreshold = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
		plug->trunkRamificationThreshold = plug->trunkDeathThreshold + r * 0.4;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
		plug->branchRamificationThreshold = plug->branchDeathThreshold + r * 0.4;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
		plug->shortshootRamificationThreshold = plug->shortshootDeathThreshold + r * 0.4;

    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.2 + 0.8*r;
		plug->taprootAttenuation = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.2 + 0.8*r;
		plug->lateralrootAttenuation = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.2 + 0.8*r;
		plug->finerootAttenuation = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.1 + r*0.4;
		plug->taprootDeathThreshold = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.1 + r*0.4;
		plug->lateralrootDeathThreshold = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.1 + r*0.4;
		plug->finerootDeathThreshold = r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
		plug->taprootRamificationThreshold = plug->taprootDeathThreshold + r * 0.4;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
		plug->lateralrootRamificationThreshold = plug->lateralrootDeathThreshold + r * 0.4;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
		plug->finerootRamificationThreshold = plug->finerootDeathThreshold + r * 0.4;
/* */
		plug->Lmax = 200;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = plug->Lmax * (0.3 + 0.7*r);
		plug->light.set (1, 0, r);
    cout << " light " << r;
    r = rand();
    r = ((int)r % RAND_MAX);
    r /= RAND_MAX;
    r = 0.3 + 0.7*r;
		plug->waterContent.set (1, 0, r);
    cout << " water " << r << endl;
		plug->airCarbon = 0.2;
		plug->rootAbsorbingLength = 2;
		plug->rhizoCylinderDiameter = 0.5;
		plug->aPhotosynthesis = 1;
		plug->KPhotosynthesis = 100;
}
void generateParameterFile(string name, paramfw *p)
{
    ofstream fic (name, std::ofstream::out | std::ofstream::trunc);
    fic << "#fichier parametre de FW prevu pour testfw.fpa et testfw.fpd"<<endl<<endl;

    fic << "# maximum phyage value for trunk"<<endl;
    fic << "trunkThreshold SingleValueParameter 10"<<endl;
    fic << "# maximum phyage value for branch"<<endl;
    fic << "branchThreshold SingleValueParameter 20"<<endl;
    fic << "# maximum phyage value for short shoot"<<endl;
    fic << "shortshootThreshold SingleValueParameter 160"<<endl;
    fic << "# minimum phyage value for leaves"<<endl;
    fic << "leafThreshold SingleValueParameter 100"<<endl<<endl;

    fic << "rootTypesCategory ParametricParameter 0 3 0 0 1 0 0 1 0 1 0 1 2 0 1 0 2"<<endl<<endl;


    fic << "# minimum growth rate for trunk life"<<endl;
    fic << "trunkDeathThreshold SingleValueParameter "<<p->att[0][0]<<endl;
    fic << "# minimum growth rate for trunk branching"<<endl;
    fic << "trunkRamificationThreshold SingleValueParameter "<<p->att[0][1]<<endl;
    fic << "# trunk sensibility for growth rate at branching threshold"<<endl;
    fic << "trunkAttenuation SingleValueParameter "<<p->att[0][2]<<endl<<endl;

    fic << "# minimum growth rate for branch life"<<endl;
    fic << "branchDeathThreshold SingleValueParameter "<<p->att[1][0]<<endl;
    fic << "# minimum growth rate for branch branching"<<endl;
    fic << "branchRamificationThreshold SingleValueParameter "<<p->att[1][1]<<endl;
    fic << "# branch sensibility for growth rate at branching threshold"<<endl;
    fic << "branchAttenuation SingleValueParameter "<<p->att[1][2]<<endl<<endl;

    fic << "# minimum growth rate for shortshoot life"<<endl;
    fic << "shortshootDeathThreshold SingleValueParameter "<<p->att[2][0]<<endl;
    fic << "# minimum growth rate for shortshoot branching"<<endl;
    fic << "shortshootRamificationThreshold SingleValueParameter "<<p->att[2][1]<<endl;
    fic << "# shortshoot sensibility for growth rate at branching threshold"<<endl;
    fic << "shortshootAttenuation SingleValueParameter "<<p->att[2][2]<<endl<<endl;

    fic << "# minimum growth rate for taproot life"<<endl;
    fic << "taprootDeathThreshold SingleValueParameter "<<p->att[3][0]<<endl;
    fic << "# minimum growth rate for taproot branching"<<endl;
    fic << "taprootRamificationThreshold SingleValueParameter "<<p->att[3][1]<<endl;
    fic << "# taproot sensibility for growth rate at branching threshold"<<endl;
    fic << "taprootAttenuation SingleValueParameter "<<p->att[3][2]<<endl<<endl;

    fic << "# minimum growth rate for lateralroot life"<<endl;
    fic << "lateralrootDeathThreshold SingleValueParameter "<<p->att[4][0]<<endl;
    fic << "# minimum growth rate for lateralroot branching"<<endl;
    fic << "lateralrootRamificationThreshold SingleValueParameter "<<p->att[4][1]<<endl;
    fic << "# lateralroot sensibility for growth rate at branching threshold"<<endl;
    fic << "lateralrootAttenuation SingleValueParameter "<<p->att[4][2]<<endl<<endl;

    fic << "# minimum growth rate for fineroot life"<<endl;
    fic << "finerootDeathThreshold SingleValueParameter "<<p->att[5][0]<<endl;
    fic << "# minimum growth rate for fineroot branching"<<endl;
    fic << "finerootRamificationThreshold SingleValueParameter "<<p->att[5][1]<<endl;
    fic << "# fineroot sensibility for growth rate at branching threshold"<<endl;
    fic << "finerootAttenuation SingleValueParameter "<<p->att[5][2]<<endl<<endl;

    fic << "# light threshold for maximum photosynthesis"<<endl;
    fic << "Lmax SingleValueParameter 200"<<endl<<endl;

    fic << "# exponential parameter for Beer law"<<endl;
    fic << "KPhotosynthesis SingleValueParameter "<<p->k<<endl<<endl;

    fic << "light ParametricParameter 0 10 ";
    for (int i=0; i<10; i++)
    {
        fic <<" "<<  i <<" 0 1 0 "<<p->light[i];
    }
    fic <<endl<<endl;

    fic << "waterContent ParametricParameter 0 10 ";
    for (int i=0; i<10; i++)
    {
        fic <<" "<<  i <<" 0 1 0 "<<p->water[i];
    }
    fic <<endl<<endl;

    fic << "# air carbon content"<<endl;
    fic << "airCarbon SingleValueParameter .05"<<endl;

    fic << "# rhizocylinder diameter"<<endl;
    fic << "rhizoCylinderDiameter SingleValueParameter 0.5"<<endl;
    fic << "# water absorbing root length"<<endl;
    fic << "rootAbsorbingLength SingleValueParameter 2"<<endl;

    fic << "# proportional parameter for Beer law"<<endl;
    fic << "aPhotosynthesis SingleValueParameter 1"<<endl;


    fic.close();
}
void generatescript()
{
    ofstream fic ("builddatabase", std::ofstream::out | std::ofstream::trunc);
    int cnt=1;
    string name;
    paramfw p;

    //Paramètres Eau : 4 modalités [4,6,8,10]
    for (int water=4; water<11; water+=2)
    {
        for (int j=0; j<10; j++)
            p.water[j] = 0.1 * water;

    //Paramètres Lumière : 3 modalités [50,100,150]
    for (int light=100; light<201; light+=50)
    {
        for (int k=0; k<10; k++)
            p.light[k] = light;

        //Paramètres Tronc
        for (int l00=1; l00<6; l00+=4)
        {
            p.att[0][0] = 0.1 * l00;
        for (int l01=l00; l01<9; l01+=4)
        {
            p.att[0][1] = 0.1 * l01 + 0.1;
        for (int l02=1; l02<10; l02+=4)
        {
            p.att[0][2] = 0.1 * l02;

        //Paramètres Rameaux longs
        for (int l10=1; l10<6; l10+=4)
        {
            p.att[1][0] = 0.1 * l10;
        for (int l11=l10; l11<9; l11+=4)
        {
            p.att[1][1] = 0.1 * l11 + 0.1;
        for (int l12=1; l12<10; l12+=4)
        {
            p.att[1][2] = 0.1 * l12;

        //Paramètres Rameaux courts
        //Pas de branchement sur les rameaux courts : Sm = Sr, Al = 0
        for (int l20=1; l20<6; l20+=4)
        {
            p.att[2][0] = 0.1 * l20;
            p.att[2][1] = 0.1 * l20;
            p.att[2][2] = 0;

        //Paramètres Racines structurantes
        for (int l30=1; l30<6; l30+=4)
        {
            p.att[3][0] = 0.1 * l30;
        for (int l31=l30; l31<9; l31+=4)
        {
            p.att[3][1] = 0.1 * l31 + 0.1;
        for (int l32=1; l32<10; l32+=4)
        {
            p.att[3][2] = 0.1 * l32;

        //Paramètres Racines secondaires
        for (int l40=1; l40<6; l40+=4)
        {
            p.att[4][0] = 0.1 * l40;
        for (int l41=l40; l41<9; l41+=4)
        {
            p.att[4][1] = 0.1 * l41 + 0.1;
        for (int l42=1; l42<10; l42+=4)
        {
            p.att[4][2] = 0.1 * l42;

        //Paramètres Racines absorbantes
        //Pas de branchements sur les racines absorbantes Sm = Sr, Al = 0
        for (int l50=1; l50<6; l50+=4)
        {
            p.att[5][0] = 0.1 * l50;
            p.att[5][1] = 0.1 * l50;
            p.att[5][2] = 0;

        //Paramètres coefficient d'extinction de la lumière.
        //Fixé à 1

            p.k = 1;
            stringstream ss;
            ss << cnt;
            name="parameter"+ss.str();
            generateParameterFile(name, &p);
            cnt++;
        }
        }
        }
        }
        }
        }
        }
        }
        }
        }
        }
        }
        }
        }
        }
    }
    int cntmax = cnt;
/*
    for (cnt=1; cnt<cntmax; cnt++)
    {
        fic << "gaspcpp /home/barczi/wholeplant/testfwnp.fpa /home/barczi/wholeplant/testfw.fpd 10  -c/home/barczi/bin/libFW.so -e/home/barczi/wholeplant/parameter"<<cnt<<"  -c/home/barczi/bin/libextractBiomass.so -e/home/barczi/wholeplant/biomassExtract -fs1 -fo/home/barczi/bin/libOutputVoxelSpace.so -ovoxelspace"<<endl;
        fic << "mkdir /home/barczi/wholeplant/dataset/"<<cnt<<endl;
        fic << "mv ./history.csv ./targets.csv voxelspace* /home/barczi/wholeplant/dataset/"<<cnt<<endl;
    }
*/
    fic.close();
}

#include <dirent.h>
#include <fstream>
#include <sys/stat.h>

void addData (string ficname, float *out, int offset, int size, int *minorig)
{
    fstream in;
    in.open(ficname.c_str());

    int sizein[3], origin[3];
    int i, j, k, a, b, c, pos;
    float abs, wood;
    string s, s1, s2, s3, s4;
    char ch[20];

    string line;
    istringstream iss(line);

    getline (in, line);
    getline (in, line);

    getline (in, line);

    getline (in, line);
    getline (in, line);

    getline (in, line);
    getline (in, line);
    int numline = 0;
    while (getline (in, line))
    {
        abs = wood = 0;
        if (sscanf (line.c_str(),"%d %d %d %s %f %f", &i, &j, &k, ch, &abs, &wood) < 3)
            continue;
        // put the values at the right place
        a = (i - minorig[0])/10;
        b = (j - minorig[1])/10;
        c = (k - minorig[2])/10;
        pos = offset + a *size*size + b *size + c;
        if (pos > 13499)
            continue;
        out[pos] = abs;
        out[pos+13500] = wood;
        numline++;
    }

    in.close();

}
void extractMultiVector (string name, float water, float light)
{
    int orig[3];
    orig[0] = orig[1] = 0;
    orig[2] = 8;


    //name = "/media/barczi/databasewholepl/datasets/1";
    //cout << name << endl;

    string s;
    for (int i=0; i<10; i++)
    {
        float vxl[27000];
        for (int i=0; i<27000; i++)
            vxl[i] = 0;

        string inname = name + "/voxelspace_" + to_string(i+1) + "_0.vxl";
        addData (inname, vxl, 0, 30, orig);
        vxl[0] = water;
        vxl[1] = light;

        s = name;
        s.append(string("/imgabswood"));
        s.append(to_string(i+1));
        s.append(string(".vct"));
        fstream out;
        out.open (s.c_str(),  ios::out |  ios::trunc | ios::binary);
        if (out.is_open())
        {
            for (int i=0; i<27; i++)
                out.write ((char*)&vxl[i*1000], 1000*sizeof(float));
            out.close();
        }
        inname = "gzip " + name + "/voxelspace_" + to_string(i+1) + "_0.vxl";
        system (inname.c_str());
    }
}

void extractVector (string name, float water, float light)
{
struct {int size[10]; int orig[10][3];} dim;
dim.size[0] = 3;
dim.size[1] = 4;
dim.size[2] = 5;
dim.size[3] = 6;
dim.size[4] = 6;
dim.size[5] = 7;
dim.size[6] = 8;
dim.size[7] = 9;
dim.size[8] = 10;
dim.size[9] = 11;

dim.orig[0][0] = -10;
dim.orig[0][1] = -10;
dim.orig[0][2] = -10;
dim.orig[1][0] = -20;
dim.orig[1][1] = -20;
dim.orig[1][2] = -10;
dim.orig[2][0] = -20;
dim.orig[2][1] = -20;
dim.orig[2][2] = -20;
dim.orig[3][0] = -30;
dim.orig[3][1] = -30;
dim.orig[3][2] = -20;
dim.orig[4][0] = -30;
dim.orig[4][1] = -30;
dim.orig[4][2] = -20;
dim.orig[5][0] = -40;
dim.orig[5][1] = -40;
dim.orig[5][2] = -20;
dim.orig[6][0] = -40;
dim.orig[6][1] = -40;
dim.orig[6][2] = -20;
dim.orig[7][0] = -50;
dim.orig[7][1] = -50;
dim.orig[7][2] = -30;
dim.orig[8][0] = -50;
dim.orig[8][1] = -50;
dim.orig[8][2] = -30;
dim.orig[9][0] = -60;
dim.orig[9][1] = -50;
dim.orig[9][2] = -30;

//offset to write in the global voxel file
//absorbing parts will be written at z=0 offset, wood parts will be written at z=15 offset

int offset[10];
offset[0] = 0;
for (int i=1; i<10; i++)
    offset[i] = offset[i-1] + pow(dim.size[i-1], 3);

    float vxl[27000];
    for (int i=0; i<27000; i++)
        vxl[i] = 0;

//name = "/media/barczi/databasewholepl/datasets/1";
//cout << name << endl;

    string s;
    for (int i=0; i<10; i++)
    {
        string inname = name + "/voxelspace_" + to_string(i+1) + "_0.vxl";
        addData (inname, vxl, offset[i], dim.size[i], dim.orig[i]);
        vxl[offset[i]] = water;
        vxl[offset[i]+1] = light;
    }

    s = name;
    s.append(string("/imgabswood.vct"));
    fstream out;
    out.open (s.c_str(),  ios::out |  ios::trunc | ios::binary);
    if (out.is_open())
    {
        for (int i=0; i<27; i++)
            out.write ((char*)&vxl[i*1000], 1000*sizeof(float));
        out.close();
    }
}

void FWPlugin::extractVectorBase(string f)
{
//    char baseDirectory[] = "/media/barczi/databasewholepl/datasets";
    char baseDirectory[] = "/home/barczi/wholeplant/datasets";


    DIR* rep = opendir(baseDirectory);

int i=0;
    if ( rep != NULL )
    {
        struct dirent* ent;

        fstream *fic;
        char line[2056];
        int counter = 0;
        while ( (ent = readdir(rep) ) != NULL )
        {
            if (   strcmp (ent->d_name, ".") == 0
                || strcmp (ent->d_name,"..") == 0)
                continue;
            if (strncmp (ent->d_name,  &f.c_str()[1], f.size()-1) != 0)
                continue;
            float pwater=0, plight=0;
            string str = string(baseDirectory)+string("/")+string(ent->d_name) + "/targets.csv";
i++;
//cout << ent->d_name<<" "<<i<<endl;

            fic = new fstream(str);
            *fic >> str;
            *fic >> pwater;
            fic->getline(line, 2056);
            *fic >> str;
            *fic >> plight;
            fic->close();
            delete fic;

            struct stat buffer;
            string name = string(baseDirectory)+string("/")+ent->d_name+string("/imgabswood.vct");
//            if (stat (name.c_str(), &buffer) == 0)
//                continue;

            extractVector(string(baseDirectory)+string("/")+ent->d_name, pwater, plight);
//            extractMultiVector(string(baseDirectory)+string("/")+ent->d_name, pwater, plight);
            cout << counter++ << " : processed "<<string(baseDirectory)+string("/")+ent->d_name<<endl;
        }

        closedir(rep);
    }

}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
FWPlugin::FWPlugin(const std::string& f, const void *plant) {

	growthRateBranch = 1;
	growthRateRoot = 1;

    if (f.length() == 1)
    {
        generateRandomParameterValues(this);
        outputRandomParameterFile ("FWRandomParameter");
    }
    else if (!initFWParam (f))
    {
		cout << "Failed reading wholePlant parameter file " << f << endl;
		cout << "File sould look like this :" << endl<<endl;
		cout << "# WARNING"<<endl;
		cout << "# The syntax for this parameter file is not even"<<endl;
		cout << "# Keep the \"SingleValueParameter\" keyword as it is"<<endl;
		cout << "# just adjust the numerical values"<<endl<<endl;
		cout << "# maximum phyage value for trunk"<<endl<<"trunkThreshold SingleValueParameter 60"<<endl;
		cout << "# maximum phyage value for branch"<<endl<<"branchThreshold SingleValueParameter 100"<<endl;
		cout << "# maximum phyage value for short shoot"<<endl<<"shortshootThreshold SingleValueParameter 160"<<endl<<endl;;
		cout << "# minimum phyage value for leaves"<<endl<<"leafThreshold SingleValueParameter 500"<<endl<<endl;;
		cout << "# minimum growth rate for trunk life"<<endl<<"trunkDeathThreshold SingleValueParameter 0.1"<<endl;
		cout << "# minimum growth rate for branch life"<<endl<<"branchDeathThreshold SingleValueParameter 0.1"<<endl;
		cout << "# minimum growth rate for short shoot life"<<endl<<"shortshootDeathThreshold SingleValueParameter 0.1"<<endl<<endl;
		cout << "# minimum growth rate for trunk branching"<<endl<<"trunkRamificationThreshold SingleValueParameter 0.4"<<endl;
		cout << "# minimum growth rate for branch branching"<<endl<<"branchRamificationThreshold SingleValueParameter 0.4"<<endl;
		cout << "# minimum growth rate for short shoot branching"<<endl<<"shortshootRamificationThreshold SingleValueParameter 0.4"<<endl<<endl;
		cout << "# trunk sensibility for growth rate at branching threshold"<<endl<<"trunkAttenuation SingleValueParameter 0.7"<<endl;
		cout << "# branch sensibility for growth rate at branching threshold"<<endl<<"branchAttenuation SingleValueParameter 0.9"<<endl;
		cout << "# short shoot sensibility for growth rate at branching threshold"<<endl<<"shortshootAttenuation SingleValueParameter 0.9"<<endl<<endl<<endl;
 		cout << "# structure of ParametricParameter is tricky !" << endl;
 		cout << "# interpolation X " << endl;
 		cout << "# nbX" << endl;
 		cout << "#    valueX1" << endl;
 		cout << "#    interpolationY1 " << endl;
 		cout << "#    nbY1" << endl;
 		cout << "#       pos1" << endl;
 		cout << "#       val1" << endl;
 		cout << "#       pos2" << endl;
 		cout << "#       val2" << endl;
 		cout << "#       ." << endl;
 		cout << "#       ." << endl;
 		cout << "#       ." << endl;
 		cout << "#" << endl;
 		cout << "#    valueX2" << endl;
 		cout << "#    interpolationY2 " << endl;
 		cout << "#    nbY2" << endl;
 		cout << "#       pos1" << endl;
 		cout << "#       val1" << endl;
 		cout << "#       pos2" << endl;
 		cout << "#       val2" << endl;
 		cout << "#       ." << endl;
 		cout << "#       ." << endl;
 		cout << "#       ." << endl;
 		cout << "#   ." << endl;
 		cout << "#   ." << endl;
 		cout << "#   ." << endl;
 		cout << "# here we put 6 couples (second number) with couple values " << endl;
 		cout << "# <0,0> (third and seventh number) " << endl;
 		cout << "# <1,0> (heighth and twelvth number)" << endl;
 		cout << "# <2,1> (thirteenth and seventeenth number)" << endl;
 		cout << "# <3,1> (heighteenth and twentysecond number)" << endl;
 		cout << "# <4,2> (twentythird and twentyheighth number)" << endl;
 		cout << "# <5,2> (twentynineth and thirtythird number)" << endl;
 		cout << "# DigR root types category (0:taproot, 1:lateral root, 2:fine root)"<<endl<<"rootTypesCategory ParametricParameter 0 6 0 0 1 0 0 1 0 1 0 0 2 0 1 0 1 3 0 1 0 1 4 0 1 0 2 5 0 1 0 2"<<endl;
 		cout << "# minimum growth rate for taproot life"<<endl<<"taprootDeathThreshold SingleValueParameter 0.1"<<endl;
		cout << "# minimum growth rate for lateral root life"<<endl<<"lateralrootDeathThreshold SingleValueParameter 0.1"<<endl;
		cout << "# minimum growth rate for fine root life"<<endl<<"finerootDeathThreshold SingleValueParameter 0.1"<<endl<<endl;
		cout << "# minimum growth rate for taproot branching"<<endl<<"taprootRamificationThreshold SingleValueParameter 0.4"<<endl;
		cout << "# minimum growth rate for lateral root branching"<<endl<<"lateralrootRamificationThreshold SingleValueParameter 0.4"<<endl;
		cout << "# minimum growth rate for fine root branching"<<endl<<"finerootRamificationThreshold SingleValueParameter 0.4"<<endl<<endl;
		cout << "# taproot sensibility for growth rate at branching threshold"<<endl<<"taprootAttenuation SingleValueParameter 0.7"<<endl;
		cout << "# lateral root sensibility for growth rate at branching threshold"<<endl<<"lateralrootAttenuation SingleValueParameter 0.9"<<endl;
		cout << "# fine root sensibility for growth rate at branching threshold"<<endl<<"finerootAttenuation SingleValueParameter 0.9"<<endl<<endl;
		cout << "# here we put 2 couples (second number) with couple values " << endl;
		cout << "# <0,100> (third and seventh number)" << endl;
		cout << "# <10,600> (heighth and twelvth number)" << endl;
		cout << "# yearly light level (here we simulate an increase from year 10)"<<endl<<"light ParametricParameter 0 100 10 600"<<endl<<endl;
		cout << "# here we put 3 couples (second number) with couple values " << endl;
		cout << "# <0,1> (third and seventh number) " << endl;
		cout << "# <10,0.5> (heighth and twelvth number)" << endl;
		cout << "# <11,1> (thirteenth and seventeenth number)" << endl;
		cout << "# air carbon content"<<endl<<"airCarbon SingleValueParameter 400"<<endl;
		cout << "# yearly soil water content (here we simulate a drigh year 10)"<<endl<<"waterContent ParametricParameter 0 1 10 0.5 11 1"<<endl<<endl;
		cout << "# light threshold for maximum photosynthesis"<<endl<<"Lmax SingleValueParameter 400"<<endl;
		cout << "# a factor for Beer law"<<endl<<"aPhotosynthesis SingleValueParameter 1"<<endl;
		cout << "# K factor for Beer law"<<endl<<"KPhotosynthesis SingleValueParameter 400"<<endl;
		cout << "# water absorbing root length"<<endl<<"rootAbsorbingLength SingleValueParameter 1"<<endl;
		cout << "# rhizocylinder diameter"<<endl<<"rhizoCylinderDiameter SingleValueParameter 1"<<endl<<endl;
		cout << "# start time for F function (optionnal, default : 0)"<<endl<<"startTime SingleValueParameter 3"<<endl<<endl;;
		cout << endl<< "**** Failed reading whole plant parameter file " << f << " ****" << endl;

//generatescript();
    extractVectorBase(f);
		return;
	}
	this->plant = (WholePlant*)plant;
    //child -> parent binding PTR
	al = new AmapSimParameterPlugin (this->plant);
    al->FWplugin_ptr = this;
	dl = new DigRParameterPlugin (this->plant);
    dl->FWplugin_ptr = this;
	p = new PlantPlugin (this->plant);
    p->FWplugin_ptr = this;
	pp = new PlantDeletePlugin (this->plant);
    pp->FWplugin_ptr = this;
    eg = new EndGeomPlugin (this->plant);
    eg->FWplugin_ptr = this;

}

bool FWPlugin::initFWParam (const std::string& f) {
	ParamSet * param_H = new ParamSet(f);
	// for first step !!!
	growthRateBranch = 1;
	growthRateRoot = 1;

	if (!param_H->Exists())
	{
        delete param_H;
		return false;
    }

	startTime = 0;
	if (param_H->getParameter("startTime"))
		startTime = param_H->getParameter("startTime")->value();
	else
	{
		cout << "Failed finding startTime parameter in file " << f << endl;
		cout << "Assuming startTime=0" << endl;
	}

	if (param_H->getParameter("trunkThreshold"))
		trunkThreshold = param_H->getParameter("trunkThreshold")->value();
	else
	{
		cout << "Failed finding trunkThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("branchThreshold"))
		branchThreshold = param_H->getParameter("branchThreshold")->value();
	else
	{
		cout << "Failed finding branchThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("shortshootThreshold"))
		shortshootThreshold = param_H->getParameter("shortshootThreshold")->value();
	else
	{
		cout << "Failed finding shortshootThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("leafThreshold"))
		leafThreshold = param_H->getParameter("leafThreshold")->value();
	else
	{
		cout << "Failed finding leafThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("trunkAttenuation"))
		trunkAttenuation = param_H->getParameter("trunkAttenuation")->value();
	else
	{
		cout << "Failed finding trunkAttenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("branchAttenuation"))
		branchAttenuation = param_H->getParameter("branchAttenuation")->value();
	else
	{
		cout << "Failed finding branchAttenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("shortshootAttenuation"))
		shortshootAttenuation = param_H->getParameter("shortshootAttenuation")->value();
	else
	{
		cout << "Failed finding shortshootAttenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("trunkDeathThreshold"))
		trunkDeathThreshold = param_H->getParameter("trunkDeathThreshold")->value();
	else
	{
		cout << "Failed finding trunkDeathThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("branchDeathThreshold"))
		branchDeathThreshold = param_H->getParameter("branchDeathThreshold")->value();
	else
	{
		cout << "Failed finding branchDeathThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("shortshootDeathThreshold"))
		shortshootDeathThreshold = param_H->getParameter("shortshootDeathThreshold")->value();
	else
	{
		cout << "Failed finding shortshootDeathThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("trunkRamificationThreshold"))
		trunkRamificationThreshold = param_H->getParameter("trunkRamificationThreshold")->value();
	else
	{
		cout << "Failed finding trunkRamificationThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("branchRamificationThreshold"))
		branchRamificationThreshold = param_H->getParameter("branchRamificationThreshold")->value();
	else
	{
		cout << "Failed finding branchRamificationThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("shortshootRamificationThreshold"))
		shortshootRamificationThreshold = param_H->getParameter("shortshootRamificationThreshold")->value();
	else
	{
		cout << "Failed finding shortshootRamificationThreshold parameter in file " << f << endl;
		return false;
	}


	if (param_H->getParameter("rootTypesCategory"))
	{
		rootTypesCategory.copy (param_H->getParameter("rootTypesCategory"));
	}
	else
	{
		cout << "Failed finding rootTypesCategory parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("taprootAttenuation"))
		taprootAttenuation = param_H->getParameter("taprootAttenuation")->value();
	else
	{
		cout << "Failed finding taprootAttenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("lateralrootAttenuation"))
		lateralrootAttenuation = param_H->getParameter("lateralrootAttenuation")->value();
	else
	{
		cout << "Failed finding lateralrootAttenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("finerootAttenuation"))
		finerootAttenuation = param_H->getParameter("finerootAttenuation")->value();
	else
	{
		cout << "Failed finding finerootAttenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("taprootDeathThreshold"))
		taprootDeathThreshold = param_H->getParameter("taprootDeathThreshold")->value();
	else
	{
		cout << "Failed finding taprootDeathThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("lateralrootDeathThreshold"))
		lateralrootDeathThreshold = param_H->getParameter("lateralrootDeathThreshold")->value();
	else
	{
		cout << "Failed finding lateralrootDeathThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("finerootDeathThreshold"))
		finerootDeathThreshold = param_H->getParameter("finerootDeathThreshold")->value();
	else
	{
		cout << "Failed finding finerootDeathThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("taprootRamificationThreshold"))
		taprootRamificationThreshold = param_H->getParameter("taprootRamificationThreshold")->value();
	else
	{
		cout << "Failed finding taprootRamificationThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("lateralrootRamificationThreshold"))
		lateralrootRamificationThreshold = param_H->getParameter("lateralrootRamificationThreshold")->value();
	else
	{
		cout << "Failed finding lateralrootRamificationThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("finerootRamificationThreshold"))
		finerootRamificationThreshold = param_H->getParameter("finerootRamificationThreshold")->value();
	else
	{
		cout << "Failed finding finerootRamificationThreshold parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("finerootRamificationThreshold"))
		finerootRamificationThreshold = param_H->getParameter("finerootRamificationThreshold")->value();
	else
	{
		cout << "Failed finding finerootRamificationThreshold parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("Lmax"))
		Lmax = param_H->getParameter("Lmax")->value();
	else
	{
		cout << "Failed finding Lmax parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("light"))
		light.copy (param_H->getParameter("light"));
	else
	{
		cout << "Failed finding light parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("waterContent"))
    {
		waterContent.copy (param_H->getParameter("waterContent"));
    }
	else
	{
		cout << "Failed finding waterContent parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("airCarbon"))
		airCarbon = param_H->getParameter("airCarbon")->value();
	else
	{
		cout << "Failed finding airCarbon parameter in file " << f << endl;
		return false;
	}

	if (param_H->getParameter("rootAbsorbingLength"))
		rootAbsorbingLength = param_H->getParameter("rootAbsorbingLength")->value();
	else
	{
		cout << "Failed finding rootAbsorbingLength parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("rhizoCylinderDiameter"))
		rhizoCylinderDiameter = param_H->getParameter("rhizoCylinderDiameter")->value();
	else
	{
		cout << "Failed finding rhizoCylinderDiameter parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("aPhotosynthesis"))
		aPhotosynthesis = param_H->getParameter("aPhotosynthesis")->value();
	else
	{
		cout << "Failed finding aPhotosynthesis parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("KPhotosynthesis"))
		KPhotosynthesis = param_H->getParameter("KPhotosynthesis")->value();
	else
	{
		cout << "Failed finding KPhotosynthesis parameter in file " << f << endl;
		return false;
	}
	delete param_H;
	return true;
}

float FWPlugin::computeLAI (float *surface)
{
    float leafSurface = 0;
    float maxDiam = 0;
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();

    for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
 		GeomBrancAMAP *g = (GeomBrancAMAP*)*b;
		if (g->getBranc()->getClassType() != "AMAPSimMod")
            continue;

		vector<GeomElem *> ges = g->getElementsGeo ();
		GeomElemCone *e = (GeomElemCone*)ges[ges.size()-1];

        BrancAMAP *ba = (BrancAMAP*)g->getBranc();
		ba->startPosit();
		ba->positOnLastEntnWithinBranc();
        double phyAgeGu = ba->getCurrentGu()->phyAgeGu;
        ba->endPosit();
        if (phyAgeGu >= leafThreshold-1)
        {
            float l = g->computeLength();
            float d = g->computeBottomDiam();
            leafSurface += l*d;
        }
        float x = e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength();
        float y = e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength();
        float ext = sqrt(x*x + y*y);
        if (ext > maxDiam)
            maxDiam = ext;
    }

    *surface = leafSurface;
    if (maxDiam > 0)
        return leafSurface / (M_PI * maxDiam*maxDiam / 4);
    else
        return 0;
}

float FWPlugin::computeRAI (float *volume)
{
    float absorbingLength = 0;
    float maxDiam = 0;
    float maxDepth = 0;
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
    float top = Scheduler::instance()->getTopClock();

    for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
 		GeomRoot *g = (GeomRoot*)*b;
		if (g->getBranc()->getClassType() != "DigRMod")
            continue;

		vector<GeomElem *> ges = g->getElementsGeo ();
		GeomElemCone *e = (GeomElemCone*)ges[ges.size()-1];

        Root *ba = (Root*)g->getBranc();

        float x = e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength();
        float y = e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength();
        float z = e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength();
        float ext = sqrt(x*x + y*y);
        if (ext > maxDiam)
            maxDiam = ext;
        if (abs(z) > maxDepth)
            maxDepth = abs(z);

        Root *r = (Root*)g->getBranc();
        r->startPosit();
        r->positOnLastSegmentWithinRoot();
        Segment *s = r->getCurrentSegment();
        float age = top - s->Time();
        while (age <= 1)
        {
            absorbingLength += s->Length();
            if (r->positOnPreviousSegmentWithinRoot() == 0)
                break;
            s = r->getCurrentSegment();
            age = top - s->Time();
        }
        r->endPosit();

        ba->endPosit();
    }

    *volume = absorbingLength * M_PI * rhizoCylinderDiameter * rhizoCylinderDiameter / 4.;
    float soilVolume = M_PI * maxDiam * maxDiam / 4. * maxDepth;
    if (   maxDiam > 0
        && maxDepth > 0)
    {
        float gr = absorbingLength * rhizoCylinderDiameter * rhizoCylinderDiameter / (maxDiam * maxDiam * maxDepth);
        if (   *volume > soilVolume
            && soilVolume > .001)
            *volume = soilVolume;
        return (gr < 1. ? gr : 1.);
    }
    else
        return 0;
}

float FWPlugin::computeH2O (float *volume)
{
    float absorbingLength = 0;
    float maxDiam = 0;
    float maxDepth = 0;
	vector<GeomBranc *> brcs = plant->getGeometry().getGeomBrancStack();
    float top = Scheduler::instance()->getTopClock();
	int inttop = (int) top;
	float H2O = 0;

	float water;
	float section =  M_PI * rhizoCylinderDiameter * rhizoCylinderDiameter / 4.;

    for (vector<GeomBranc *>::iterator b=brcs.begin(); b!=brcs.end(); b++)
	{
 		GeomRoot *g = (GeomRoot*)*b;
		if (g->getBranc()->getClassType() != "DigRMod")
            continue;

		vector<GeomElem *> ges = g->getElementsGeo ();
		GeomElemCone *e = (GeomElemCone*)ges[ges.size()-1];

        Root *ba = (Root*)g->getBranc();

        float x = e->getMatrix().getTranslation().x()+e->getMatrix().getMainVector().x()*e->getLength();
        float y = e->getMatrix().getTranslation().y()+e->getMatrix().getMainVector().y()*e->getLength();
        float z = e->getMatrix().getTranslation().z()+e->getMatrix().getMainVector().z()*e->getLength();
        float ext = sqrt(x*x + y*y);
        if (ext > maxDiam)
            maxDiam = ext;
        if (abs(z) > maxDepth)
            maxDepth = abs(z);
        water = waterContent.value (top, abs(z));

        Root *r = (Root*)g->getBranc();
        r->startPosit();
        r->positOnLastSegmentWithinRoot();
        Segment *s = r->getCurrentSegment();
        float age = top - s->Time();
        while (age <= 1)
        {
            absorbingLength += s->Length();
            H2O += water * s->Length() * section;
            if (r->positOnPreviousSegmentWithinRoot() == 0)
                break;
            s = r->getCurrentSegment();
            age = top - s->Time();
        }
        r->endPosit();

        ba->endPosit();
    }

    *volume = absorbingLength * section;
    float soilVolume = M_PI * maxDiam * maxDiam / 4. * maxDepth;
    if (   maxDiam > 0
        && maxDepth > 0)
    {
        float gr = absorbingLength * rhizoCylinderDiameter * rhizoCylinderDiameter / (maxDiam * maxDiam * maxDepth);
        if (   *volume > soilVolume
            && soilVolume > .001)
            *volume = soilVolume;
    }
    return H2O;
}

void FWPlugin::outputRandomParameterFile (string name)
{
    ParamSet *param_H;
    param_H = new ParamSet;

    SingleValueParameter *param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (startTime);
    param->setName ("startTime");
    param_H->addParameter ("startTime", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (trunkThreshold);
    param->setName ("trunkThreshold");
    param_H->addParameter ("trunkThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (branchThreshold);
    param->setName ("branchThreshold");
    param_H->addParameter ("branchThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (shortshootThreshold);
    param->setName ("shortshootThreshold");
    param_H->addParameter ("shortshootThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (leafThreshold);
    param->setName ("leafThreshold");
    param_H->addParameter ("leafThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (trunkAttenuation);
    param->setName ("trunkAttenuation");
    param_H->addParameter ("trunkAttenuation", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (branchAttenuation);
    param->setName ("branchAttenuation");
    param_H->addParameter ("branchAttenuation", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (shortshootAttenuation);
    param->setName ("shortshootAttenuation");
    param_H->addParameter ("shortshootAttenuation", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (trunkDeathThreshold);
    param->setName ("trunkDeathThreshold");
    param_H->addParameter ("trunkDeathThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (branchDeathThreshold);
    param->setName ("branchDeathThreshold");
    param_H->addParameter ("branchDeathThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (shortshootDeathThreshold);
    param->setName ("shortshootDeathThreshold");
    param_H->addParameter ("shortshootDeathThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (trunkRamificationThreshold);
    param->setName ("trunkRamificationThreshold");
    param_H->addParameter ("trunkRamificationThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (branchRamificationThreshold);
    param->setName ("branchRamificationThreshold");
    param_H->addParameter ("branchRamificationThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (shortshootRamificationThreshold);
    param->setName ("shortshootRamificationThreshold");
    param_H->addParameter ("shortshootRamificationThreshold", param);

    ParametricParameter *paramp = (ParametricParameter*)ParamFactory::instance().createParameter("ParametricParameter");
	paramp->copy (&rootTypesCategory);
    paramp->setName ("rootTypesCategory");
    param_H->addParameter ("rootTypesCategory", paramp);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (taprootAttenuation);
    param->setName ("taprootAttenuation");
    param_H->addParameter ("taprootAttenuation", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (lateralrootAttenuation);
    param->setName ("lateralrootAttenuation");
    param_H->addParameter ("lateralrootAttenuation", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (finerootAttenuation);
    param->setName ("finerootAttenuation");
    param_H->addParameter ("finerootAttenuation", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (taprootDeathThreshold);
    param->setName ("taprootDeathThreshold");
    param_H->addParameter ("taprootDeathThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (lateralrootDeathThreshold);
    param->setName ("lateralrootDeathThreshold");
    param_H->addParameter ("lateralrootDeathThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (finerootDeathThreshold);
    param->setName ("finerootDeathThreshold");
    param_H->addParameter ("finerootDeathThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (taprootRamificationThreshold);
    param->setName ("taprootRamificationThreshold");
    param_H->addParameter ("taprootRamificationThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (lateralrootRamificationThreshold);
    param->setName ("lateralrootRamificationThreshold");
    param_H->addParameter ("lateralrootRamificationThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (finerootRamificationThreshold);
    param->setName ("finerootRamificationThreshold");
    param_H->addParameter ("finerootRamificationThreshold", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (Lmax);
    param->setName ("Lmax");
    param_H->addParameter ("Lmax", param);

    paramp = (ParametricParameter*)ParamFactory::instance().createParameter("ParametricParameter");
	paramp->copy (&light);
    paramp->setName ("light");
    param_H->addParameter ("light", paramp);

    paramp = (ParametricParameter*)ParamFactory::instance().createParameter("ParametricParameter");
	paramp->copy (&waterContent);
    paramp->setName ("waterContent");
    param_H->addParameter ("waterContent", paramp);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (airCarbon);
    param->setName ("airCarbon");
    param_H->addParameter ("airCarbon", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (rootAbsorbingLength);
    param->setName ("rootAbsorbingLength");
    param_H->addParameter ("rootAbsorbingLength", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (rhizoCylinderDiameter);
    param->setName ("rhizoCylinderDiameter");
    param_H->addParameter ("rhizoCylinderDiameter", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (aPhotosynthesis);
    param->setName ("aPhotosynthesis");
    param_H->addParameter ("aPhotosynthesis", param);

    param = (SingleValueParameter*)ParamFactory::instance().createParameter("SingleValueParameter");
    param->set (KPhotosynthesis);
    param->setName ("KPhotosynthesis");
    param_H->addParameter ("KPhotosynthesis", param);

    param_H->setName (name);
    param_H->write();

    delete param_H;
}





