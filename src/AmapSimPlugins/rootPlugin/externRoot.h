#ifndef __EXPORTROOT_H__
#define __EXPORTROOT_H__


#pragma warning ( disable : 4251 )

#if !defined STATIC_LIB
	#if defined WIN32

			#ifdef ROOT_EXPORTS
				#define ROOT_EXPORT __declspec(dllexport)
			#else
				#define ROOT_EXPORT __declspec(dllimport)
			#endif

	#else
		#define ROOT_EXPORT
	#endif

#else
		#define ROOT_EXPORT
#endif



#endif
