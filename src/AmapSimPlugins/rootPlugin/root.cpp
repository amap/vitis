/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

//#include <windows.h>
#include <iostream>
using namespace std;

//#include "AmapSimModSignalInterface.h"
#include "Bud.h"
#include "externRoot.h"
#include "corresp.h"
#include "taille_efficace.h"

struct  RootSymbolNumber:public subscriber<messageAxeRef>
{

	RootSymbolNumber (void *p) {subscribe(p);};
	virtual ~RootSymbolNumber () {unsubscribe();};
	void on_notify (const state& , paramMsg * data=NULL) {

		paramAxisRef *p = (paramAxisRef *)data;
		if (p->numVar == rtnumsymb)
		{
			BrancAMAP *b = (BrancAMAP *)p->b;
			int phyAge = (int)b->getCurrentGu()->phyAgeGu;
			int portee = b->getCurrentEntnNumberWithinBranch()-b->getCurrentEntnIndexWithinBranch();
cout << "numsymb "<< p->var << " age_phy " << phyAge << " portee " << portee  << endl;
			if (   phyAge >= MinPhyAge
			    && phyAge <= MaxPhyAge
				&& portee >= taille_efficace_min[phyAge - MinPhyAge]
				&& portee <= taille_efficace_max[phyAge - MinPhyAge])
			{
				p->var += 200;
				cout << "numsymb "<< p->var << " age_phy " << phyAge << " portee " << portee  << endl;
			}
		}
		else if (p->numVar == rgpcntlng)
		{
				cout << "pcntgross "<< p->var << endl;
		}
#if 0
		switch (st)
		{
			case Val1DParam :
//				printf ("%d %f\n",p->numVar, p->var);
				break;
			case Val2DParam :
//				printf ("plus complique\n");
				break;
			default:
				printf ("inconnu\n");
				break;
		};
#endif
	};
};


extern "C" ROOT_EXPORT  RootSymbolNumber * StartPlugin (void *p)
{
	std::cout << "loading root module" << std::endl;
	return new RootSymbolNumber (p) ;
}
