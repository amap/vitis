/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#ifdef WINDOWS
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "PlantAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "BrancAMAP.h"
#include "externGLAmapSim.h"
#include "GLAmapSim.h"
#include "InterNodeAMAP.h"
#include "GrowthUnitAMAP.h"
#include "GrowthEngineAMAP.h"
#include "GeomBrancAMAP.h"
#include "Bud.h"


#ifdef GL3-4
void GL4ParameterPlugin::on_notify (const state& st, paramMsg *data)
{
    //Cast message parameters
    paramAxisRef * p = (paramAxisRef *)data;

    //Retrieve branch
    BrancAMAP * b = (BrancAMAP *)p->b;

	// elongation probability
    if (   p->numVar == rtprefp1
		|| p->numVar == rtneob1
		|| p->numVar == rtneopp1)
	{
		double QD=this->glplugin_ptr->getParam()->getQDThreshold();
		if (this->glplugin_ptr->Demand() == 0)
			QD = 0;
		else if (this->glplugin_ptr->Demand() > 0)
			QD = this->glplugin_ptr->Matter()/this->glplugin_ptr->Demand();
		if (QD < this->glplugin_ptr->getParam()->getQDThreshold())
			p->var *= QD/this->glplugin_ptr->getParam()->getQDThreshold();

/*
		if (this->glplugin_ptr->Demand() > 0)
			cout << "elongation proba : Q/D " << this->glplugin_ptr->Matter()/this->glplugin_ptr->Demand() << endl;
		else
			cout << "elongation proba : no demand " << endl;
*/
	}
	// metamorphosis probability
    else if (p->numVar == pbremain)
	{
	}
	// metamorphosis jump
    else if (   p->numVar == transitionjump1
		     || p->numVar == transitionjump2
		     || p->numVar == transitionjump3
		     || p->numVar == transitionjump4
			 || p->numVar == transitionjump5)
	{
	}
	// initial phyage first state
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcsautramiret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*1+NB_GLOBAL_LP_VAR+rcsautramiret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcsautramiret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*3+NB_GLOBAL_LP_VAR+rcsautramiret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*4+NB_GLOBAL_LP_VAR+rcsautramiret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*5+NB_GLOBAL_LP_VAR+rcsautramiret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*6+NB_GLOBAL_LP_VAR+rcsautramiret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*7+NB_GLOBAL_LP_VAR+rcsautramiret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*8+NB_GLOBAL_LP_VAR+rcsautramiret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*9+NB_GLOBAL_LP_VAR+rcsautramiret1)
	{
	}
	// initial phyage second state
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcsautramiret2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*1+NB_GLOBAL_LP_VAR+rcsautramiret2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcsautramiret2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*3+NB_GLOBAL_LP_VAR+rcsautramiret2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*4+NB_GLOBAL_LP_VAR+rcsautramiret2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*5+NB_GLOBAL_LP_VAR+rcsautramiret2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*6+NB_GLOBAL_LP_VAR+rcsautramiret2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*7+NB_GLOBAL_LP_VAR+rcsautramiret2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*8+NB_GLOBAL_LP_VAR+rcsautramiret2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*9+NB_GLOBAL_LP_VAR+rcsautramiret2)
	{
	}
	// initial not branched proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpbinitret0
		     /*|| p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*1+NB_GLOBAL_LP_VAR+rcpbinitret0*/
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpbinitret0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*3+NB_GLOBAL_LP_VAR+rcpbinitret0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*4+NB_GLOBAL_LP_VAR+rcpbinitret0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*5+NB_GLOBAL_LP_VAR+rcpbinitret0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*6+NB_GLOBAL_LP_VAR+rcpbinitret0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*7+NB_GLOBAL_LP_VAR+rcpbinitret0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*8+NB_GLOBAL_LP_VAR+rcpbinitret0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*9+NB_GLOBAL_LP_VAR+rcpbinitret0)
	{
		double QD=0;
		if (this->glplugin_ptr->Demand() == 0)
			QD = 0;
		else if (this->glplugin_ptr->Demand() > 0)
			QD = this->glplugin_ptr->Matter()/this->glplugin_ptr->Demand();
		if (QD > this->glplugin_ptr->getParam()->getQDThreshold())
		{
//			cout << "Q/D "<<QD<< "pb init non ramif = 0" << endl;
			p->var = 0;
		}
		else
		{
//			cout << "Q/D "<<QD<< "pb init non ramif = 1" << endl;
			p->var = 1-QD/this->glplugin_ptr->getParam()->getQDThreshold();
		}
	}
	// initial branched first state proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpbinitret1
		     /*|| p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*1+NB_GLOBAL_LP_VAR+rcpbinitret1*/
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpbinitret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*3+NB_GLOBAL_LP_VAR+rcpbinitret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*4+NB_GLOBAL_LP_VAR+rcpbinitret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*5+NB_GLOBAL_LP_VAR+rcpbinitret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*6+NB_GLOBAL_LP_VAR+rcpbinitret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*7+NB_GLOBAL_LP_VAR+rcpbinitret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*8+NB_GLOBAL_LP_VAR+rcpbinitret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*9+NB_GLOBAL_LP_VAR+rcpbinitret1)
	{
		double QD=0;
		if (this->glplugin_ptr->Demand() == 0)
			QD = 0;
		else if (this->glplugin_ptr->Demand() > 0)
			QD = this->glplugin_ptr->Matter()/this->glplugin_ptr->Demand();
		if (QD <= this->glplugin_ptr->getParam()->getQDThreshold())
		{
//			cout << "Q/D "<<QD<<" pb init ramif = 0" << endl;
			p->var = QD/this->glplugin_ptr->getParam()->getQDThreshold();
		}
		else
		{
//			cout << "Q/D "<<QD<< " pb init ramif = 1" << endl;
			p->var = 1;
		}
	}
	// remaining not branched proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_st0
		     /*|| p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*1+NB_GLOBAL_LP_VAR+rcpb_st0*/
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_st0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*3+NB_GLOBAL_LP_VAR+rcpb_st0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*4+NB_GLOBAL_LP_VAR+rcpb_st0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*5+NB_GLOBAL_LP_VAR+rcpb_st0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*6+NB_GLOBAL_LP_VAR+rcpb_st0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*7+NB_GLOBAL_LP_VAR+rcpb_st0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*8+NB_GLOBAL_LP_VAR+rcpb_st0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*9+NB_GLOBAL_LP_VAR+rcpb_st0)
	{
		double QD=0;
		if (this->glplugin_ptr->Demand() == 0)
			QD = 0;
		else if (this->glplugin_ptr->Demand() > 0)
			QD = this->glplugin_ptr->Matter()/this->glplugin_ptr->Demand();
		if (QD <= this->glplugin_ptr->getParam()->getQDThreshold())
		{
//			cout << "Q/D "<<QD<<" pb init ramif = 0" << endl;
			p->var = 1-QD/this->glplugin_ptr->getParam()->getQDThreshold();
		}
		else
		{
//			cout << "Q/D "<<QD<< " pb init ramif = 1" << endl;
			p->var = 0;
		}
	}
	// remaining first state proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_st1
		     /*|| p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*1+NB_GLOBAL_LP_VAR+rcpb_st1*/
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_st1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*3+NB_GLOBAL_LP_VAR+rcpb_st1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*4+NB_GLOBAL_LP_VAR+rcpb_st1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*5+NB_GLOBAL_LP_VAR+rcpb_st1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*6+NB_GLOBAL_LP_VAR+rcpb_st1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*7+NB_GLOBAL_LP_VAR+rcpb_st1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*8+NB_GLOBAL_LP_VAR+rcpb_st1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*9+NB_GLOBAL_LP_VAR+rcpb_st1)
	{
		double QD=0;
		if (this->glplugin_ptr->Demand() == 0)
			QD = 0;
		else if (this->glplugin_ptr->Demand() > 0)
			QD = this->glplugin_ptr->Matter()/this->glplugin_ptr->Demand();
		if (QD <= this->glplugin_ptr->getParam()->getQDThreshold())
		{
//			cout << "Q/D "<<QD<<" pb init ramif = 0" << endl;
			p->var = QD/this->glplugin_ptr->getParam()->getQDThreshold();
		}
		else
		{
//			cout << "Q/D "<<QD<< " pb init ramif = 1" << endl;
			p->var = 1;
		}
	}
	// remaining second state proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_st2
		     /*|| p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*1+NB_GLOBAL_LP_VAR+rcpb_st2*/
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_st2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*3+NB_GLOBAL_LP_VAR+rcpb_st2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*4+NB_GLOBAL_LP_VAR+rcpb_st2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*5+NB_GLOBAL_LP_VAR+rcpb_st2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*6+NB_GLOBAL_LP_VAR+rcpb_st2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*7+NB_GLOBAL_LP_VAR+rcpb_st2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*8+NB_GLOBAL_LP_VAR+rcpb_st2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*9+NB_GLOBAL_LP_VAR+rcpb_st2)
	{
	}
	// transition not branched -> first state proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_0_1
		     /*|| p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*1+NB_GLOBAL_LP_VAR+rcpb_0_1*/
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_0_1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*3+NB_GLOBAL_LP_VAR+rcpb_0_1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*4+NB_GLOBAL_LP_VAR+rcpb_0_1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*5+NB_GLOBAL_LP_VAR+rcpb_0_1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*6+NB_GLOBAL_LP_VAR+rcpb_0_1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*7+NB_GLOBAL_LP_VAR+rcpb_0_1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*8+NB_GLOBAL_LP_VAR+rcpb_0_1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*9+NB_GLOBAL_LP_VAR+rcpb_0_1)
	{
	}
	// transition first state -> not branched proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_1_0
		     /*|| p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*1+NB_GLOBAL_LP_VAR+rcpb_1_0*/
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_1_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*3+NB_GLOBAL_LP_VAR+rcpb_1_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*4+NB_GLOBAL_LP_VAR+rcpb_1_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*5+NB_GLOBAL_LP_VAR+rcpb_1_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*6+NB_GLOBAL_LP_VAR+rcpb_1_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*7+NB_GLOBAL_LP_VAR+rcpb_1_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*8+NB_GLOBAL_LP_VAR+rcpb_1_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*9+NB_GLOBAL_LP_VAR+rcpb_1_0)
	{
	}
	// transition second state -> not branched proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_2_0
		     /*|| p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*1+NB_GLOBAL_LP_VAR+rcpb_2_0*/
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_2_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*3+NB_GLOBAL_LP_VAR+rcpb_2_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*4+NB_GLOBAL_LP_VAR+rcpb_2_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*5+NB_GLOBAL_LP_VAR+rcpb_2_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*6+NB_GLOBAL_LP_VAR+rcpb_2_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*7+NB_GLOBAL_LP_VAR+rcpb_2_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*8+NB_GLOBAL_LP_VAR+rcpb_2_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*9+NB_GLOBAL_LP_VAR+rcpb_2_0)
	{
	}
}
#endif

GLAmapSimPlugin::GLAmapSimPlugin(const std::string& f, void *p):GLPlugin (f, p) {;}



void GLAmapSimPlugin::outTarget ()
{
	BrancAMAP *b = (BrancAMAP *)getPlant()->getTopology().seekTree(TopoManager::RESET);
	double agePlant = Scheduler::instance()->getHotStop();
	double bladeVolume, bladeSurface, petioleVolume, fruitVolume;
	int nbRamif, indRamif;
	int indWithinBranc;
	bool organ;

	if(b == NULL)
		return;

	string name = getPlant()->getName();
	name += ".tgt";

cout << "output to "<<name<<endl;

	fstream  fic;

	fic.open(name.c_str(), ios_base::out|ios_base::trunc);

	fic << "Single Fitting sur cible d'\E2ge " << agePlant << endl;
	fic << "%For each target: plant age for the target and type of target" << endl <<endl;;


	fic << "%************************************************" << endl;
	fic << " (Cible)       "<<Scheduler::instance()->getHotStop()<<"       0" << endl;
	fic << "%************************************************" << endl<< endl;

	fic << " (Root_Mass)       0" << endl<< endl;

	fic << "% Pour chaque Zone de l'UC :" << endl;
	fic << "%      GU |    Nb of Met |     Nb of Ax |        Id Ax |      In Mass |    In Length |      In Diam |     Bla Surf |     Bla Mass |     Pet Mass |    FeFr Mass |    MaFr Mass |" << endl;

	do
	{
		if (b->getCurrentEntnNumberWithinBranch() == 0)
			continue;

		if (   kindOf (b) == BLADEORGAN
			|| kindOf (b) == PETIOLEORGAN
			|| kindOf (b) == FRUITORGAN)
			continue;

		b->startPosit();

		b->positOnFirstEntnWithinBranc();
		fic << "-------------------------------------" << endl;
		fic << " (Axe)	"<<b->V_idBranc() <<"	"<< kindOf(b) <<"	"<< agePlant-b->getBud()->birthTime<< endl;
		fic << "-------------------------------------" << endl;

		Hierarc *h = b->getCurrentHierarc();
		b->positOnFirstGuWithinBranc();
		do
		{
			GrowthUnitAMAP *Gu=b->getCurrentGu();
			if(!b->positOnFirstEntnWithinGu())
				continue;
			bool deb = true;
			fic << " (UC)	"<<b->getCurrentGuIndex();
			do
			{
				// find out ramifications, blades and fruits
				nbRamif = 0;
				indRamif = -1;
				bladeVolume = bladeSurface = 0;
				petioleVolume = 0;
				fruitVolume = 0;
				indWithinBranc = b->getCurrentEntnIndexWithinBranch();
				for (int i=0; i<h->getNbBorne(); i++)
				{
					if (   h->bornePosition[i] == indWithinBranc
						&& ((BrancAMAP*)h->getBorne(i))->getCurrentEntnNumberWithinBranch() > 0)
					{
						BrancAMAP *borne = (BrancAMAP*)h->getBorne(i);
						borne->startPosit();
						organ = false;
						if (kindOf (borne) == BLADEORGAN)
						{
							organ = true;
							borne->positOnFirstEntnWithinBranc();
							LeafData *v = (LeafData*)borne->getCurrentEntn()->getAdditionalData("leafVolume");
							bladeVolume += v->v[BLADE];
						}
						if (kindOf (borne) == PETIOLEORGAN)
						{
							organ = true;
							borne->positOnFirstEntnWithinBranc();
							LeafData *v = (LeafData*)borne->getCurrentEntn()->getAdditionalData("leafVolume");
							petioleVolume += v->v[PETIOLE];
						}
						if (kindOf (borne) == FRUITORGAN)
						{
							organ = true;
							borne->positOnFirstEntnWithinBranc();
							FruitData *v = (FruitData*)borne->getCurrentEntn()->getAdditionalData("fruitVolume");
							fruitVolume += v->v;
						}
						if (!organ)
						{
							nbRamif ++;
							indRamif = borne->V_idBranc();
						}

						borne->endPosit();
					}
				}

				EntnData *e = (EntnData*)b->getCurrentEntn()->getAdditionalData("woodVolume");
				if (deb)
				{
					fic << "	";
					deb = false;
				}
				else
					fic << "				";
				fic << "1	";
				// axes
				fic<< nbRamif<<"	"<<indRamif<<"	";
				//internodes
				fic<<e->volume[PITH]<<"	"<<"0"<<"	"<<"0"<<"	";
				//blade
				bladeSurface = bladeVolume / getParam()->getParameter("BladeThickness")->value();
				fic <<bladeSurface<<"	"<<bladeVolume<<"	";
				// petiole
				fic<<petioleVolume<<"	";
				// fruit;
				fic<<fruitVolume<<"	"<<"0";
				fic<<endl;
			}while (Gu->positOnNextElementOfSubLevel(INTERNODE));

		}while(b->positOnNextGuWithinBranc());

		b->endPosit();

	}while((b=(BrancAMAP *)getPlant()->getTopology().seekTree(TopoManager::NEXT))!=NULL);

	fic.close();

//	cout << endl<<"Strike any key to continue"<<endl;
//	getchar();
}

OrganKind GLAmapSimPlugin::kindOf (Branc *b)
{
	int phyAge = ((BrancAMAP *)b)->phyAgeInit;
	ParamSet *p = (ParamSet*)getParam();
	if (   phyAge >= p->getParameter ("MinBlade")->value()
		&& phyAge <= p->getParameter ("MaxBlade")->value())
		return BLADEORGAN;
	else if (   phyAge >= p->getParameter ("MinPetiole")->value()
		     && phyAge <= p->getParameter ("MaxPetiole")->value())
		return PETIOLEORGAN;
	else if (   phyAge >= p->getParameter ("MinFruit")->value()
		     && phyAge <= p->getParameter ("MaxFruit")->value())
		return FRUITORGAN;
	else
		return WOODORGAN;
}

int GLAmapSimPlugin::getCategory (Branc *b)
{
	return (int)((BrancAMAP*)b)->getCurrentGu()->phyAgeGu - 1;
}

float GLAmapSimPlugin::getTime (DecompAxeLevel *d)
{
	InterNodeAMAP *e = (InterNodeAMAP *)d;
	return e->instant;
}

bool GLAmapSimPlugin::checkParameterSet ()
{
	bool ret = GLPlugin::checkParameterSet();
	ret &= param_H->checkParameter("MinBlade");
	ret &= param_H->checkParameter("MaxBlade");
	ret &= param_H->checkParameter("MinPetiole");
	ret &= param_H->checkParameter("MaxPetiole");
	ret &= param_H->checkParameter("MinFruit");
	ret &= param_H->checkParameter("MaxFruit");

	param_H->setValid(ret);

	return ret;
}

