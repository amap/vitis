/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"

#include "gl1.h"

#include <vector>

class GLPlugin;


//struct GL4ParameterPlugin:public subscriber<messageAxeRef> {
//	GLPlugin * glplugin_ptr;
//
//	GL4ParameterPlugin () {subscribe();};
//	virtual ~GL4ParameterPlugin () {unsubscribe();};
//	void on_notify (const state& st, paramMsg * data=NULL);
//};

#include "defAMAP.h"

class GLAmapSimPlugin : public GLPlugin
{
public :
	GLAmapSimPlugin (const std::string& f, void *p);
	virtual OrganKind kindOf (Branc *b);
	virtual int getCategory (Branc *b);
	virtual DecompAxeLevel *getCurrentGu (Branc *b) {return ((BrancAMAP*)b)->getCurrentGu();};
protected:
	virtual void outTarget ();
	virtual void setGuLevel () {guLevel = GROWTHUNIT;};
	virtual int MaxCategory (){return (int)(*((AxisReference*)plant_ptr->getParamData(string("AMAPSimMod"))))[glmaxref].ymin;};
	virtual float getTime (DecompAxeLevel *d);
	bool checkParameterSet ();
};

extern "C" GLAMAPSIM_EXPORT  GLAmapSimPlugin * StartPlugin (const std::string& f, void *p)
{
	std::cout << "loading AmapSim GLPlugin module with parameter " << f << std::endl;

	return new GLAmapSimPlugin (f, p) ;
}
