#include <string>
#include "tinyxml.h"
#include <iostream>
#include <map>
#include <vector>
#include "xmlIO.h"

using namespace std;


Checkpoint::Checkpoint (string serialized_checkpoint) {
	
	vector<string> v = split(serialized_checkpoint,"=");

    if(v.size()!=2) {
        //throw std::logic_error( "Vector incorrect" );
		cerr << "Vector incorrect" << std::endl;
    }

	string pos = v.at(0);
	string val = v.at(1);

	position = atof( pos.c_str() );
	value = atof ( val.c_str() );
}


Checkpoint::Checkpoint (double pos,double val) {
	position = pos;
	value = val;
}



//--------------------------


CheckpointS::CheckpointS (string serialized_checkpoints) {
    vector<string> v = split(serialized_checkpoints,",");

    for (size_t i=0; i<v.size(); i++) {
        string serialized_checkpoint = v.at(i);

        Checkpoint * c = new Checkpoint(serialized_checkpoint);

        listCheckpoint.push_back(*c);
    }
}


CheckpointS::CheckpointS (double pos1,double val1,double pos2,double val2) {
    Checkpoint * c1 = new Checkpoint(pos1,val1);
    Checkpoint * c2 = new Checkpoint(pos2,val2);

    listCheckpoint.push_back(*c1);
    listCheckpoint.push_back(*c2);
}








/**
 *
 * Notes:
 *
 *   - The first checkpoint is used on the far left 
 *   - The last checkpoint is used on the far right
 *
 */
double CheckpointS::getValue(double pos) {
    double value = 0;
    Checkpoint c;
    Checkpoint c1,c2;
    bool found = false;
    size_t i = 0;
    int mode = 1; //0 -> cste, 1 -> linear


	//Loop on checkpoints in ascendant order
    for (i=0; i<listCheckpoint.size(); i++) { 
        c = listCheckpoint.at(i);

        if(pos<c.position) { //Main test 
            found = true;
            break;
        }
    }

    if (found) {
        if(i==0) {
            //Pos is beyond the first checkpoint
            
            c = listCheckpoint.at(i); //The first checkpoint is used on the far left 
            value = c.value;
        } else {
            //"in between" checkpoint
            
            if(mode=0) {
                //Constant interpolation
                
                c = listCheckpoint.at(i-1); 
                value = c.value;
            } else if (mode=1) {
                //Linear interpolation
                
                c1 = listCheckpoint.at(i-1); 
                c2 = listCheckpoint.at(i); 

                value = c1.value + ((pos - c1.position) * ((c2.value - c1.value) / (c2.position - c1.position))); 
            }
        }
    } else {
        //Pos is beyond the last checkpoint
        
        if(listCheckpoint.size()==0) {
            value = 0;
        } else {
            c = listCheckpoint.at(listCheckpoint.size()-1); //The last checkpoint is used on the far right
            value = c.value;
        }
    }

    //Debug
    //cout << "value ==> " << value << ", vector_size ==>" << listCheckpoint.size() << endl;

    return value;
}

//--------------------------

vector<string> split(string s, string sep) {
   std::vector<std::string> r;
   size_t b = 0, e = 0;
   for (;;)
   {
      b = s.find_first_not_of(sep, e);
      e = s.find_first_of(sep, b);
      if (b == s.npos) break;
      r.push_back(s.substr(b, e-b));
   }
   return r;
}


//-------------------------

/**
 *
 *
 *
 *
 */
map<string, string> getScalarParametersFromXMLFile (const std::string & f) {

	TiXmlDocument *doc=new TiXmlDocument( f.c_str() );
	TiXmlElement* pElem;
	TiXmlNode* node = 0;
	const char * value;
	const char * organ;
	const char * function;
	const char * name;
	string type;


	//Parsing
	bool loadOkay = doc->LoadFile();

	//Check
	if ( !loadOkay ) {
		//throw
		//return
        std::cout << "nametoto2" << std::endl;
	} 

	//Retrieve root
	node = doc->RootElement();

	//Check
	if (!node) {
        //throw std::logic_error( "Node incorrect" );
		cerr << "Node incorrect" << std::endl;
	}

	//Debug
    /*
	pElem = node->ToElement();
	name = pElem->Value();
	std::cout << name << std::endl;
	std::cout << "nametoto" << std::endl;
    */

    map<string, string> paramS;


	//
	node=node->FirstChild("parameter");
	while (node) {
		pElem = node->ToElement();

		//Retrieve attributes
		organ = pElem->Attribute("organ");
		function = pElem->Attribute("function");
		name = pElem->Attribute("name");
		value = pElem->Attribute("value");
		type = pElem->Attribute("type");

        if(type!="vector1D") {

            //Debug
            //std::cout << value << std::endl;


            //Store parameters values in hashmap
            string key = organ;
            key.append("#");
            key.append(function);
            key.append("#");
            key.append(name);
            paramS[key]=value;
        }


		node=node->NextSiblingElement();
	}

	doc->Clear();
	doc->ClearError();

	delete doc;

	return paramS;
}




/**
 *
 *
 *
 *
 */
map<string, CheckpointS *> get1DParametersFromXMLFile (const std::string & f) {

	TiXmlDocument doc( f.c_str() );
	TiXmlElement* pElem;
	TiXmlNode* node = 0;
	const char * value;
	const char * organ;
	const char * function;
	const char * name;
	string type;


	//Parsing
	bool loadOkay = doc.LoadFile();

	//Check
	if ( !loadOkay ) {
		//throw
		//return
	} 

	//Retrieve root
	node = doc.RootElement();

	//Check
	if (!node) {
        //throw std::logic_error( "Node incorrect" );
		cerr << "Node incorrect" << std::endl;
	}

	//Debug
    /*
	pElem = node->ToElement();
	name = pElem->Value();
	std::cout << name << std::endl;
	std::cout << "nametoto" << std::endl;
    */

    map<string, CheckpointS *> paramS;

	//
	node=node->FirstChild("parameter");
	while (node) {
		pElem = node->ToElement();

		//Retrieve attributes
		organ = pElem->Attribute("organ");
		function = pElem->Attribute("function");
		name = pElem->Attribute("name");
		value = pElem->Attribute("value");
		type = pElem->Attribute("type");

		if(type=="vector1D") {

            //Debug
            //std::cout << value << std::endl;


            //Store parameters values in hashmap
            string key = organ;
            key.append("#");
            key.append(function);
            key.append("#");
            key.append(name);

            CheckpointS * c = new CheckpointS(value);
            paramS[key] = c;
        }

		node=node->NextSiblingElement();
	}

	return paramS;
}



			
