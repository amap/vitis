#include <iostream>
#include <fstream>
#include "UtilMath.h"
using namespace std;

#include "Plant.h"
#include "PlantAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "BrancAMAP.h"
#include "externPalm.h"
#include "palm.h"
#include "GrowthUnitAMAP.h"
#include "GrowthEngineAMAP.h"
#include "GeomBrancAMAP.h"
#include "Bud.h"
#include "UtilMathPalmier.h"



float angle_point_c = 90;



/////////////////////////////////////////////////////
//
// Called when a parameter is retrieved
//
// We override here, for example, the normal phyAge jump behavior 
//
/////////////////////////////////////////////////////
void ParameterPlugin::on_notify (const state& st, paramMsg *data) {
    int pos_on_stem=0; 
    int pos_on_crowns=0; 
    int pos_on_palm=0; 
    int pos=0; 
    int rachisInternodeNumber=0;
    double DiameterRatio=0;
    AxisReference * axeRef = (AxisReference *)palmplugin_ptr->plant_ptr->getParamData(string("AMAPSimMod"));


    //Cast message parameters
    paramAxisRef * p = (paramAxisRef *)data;


    //Retrieve branch
    BrancAMAP * b = (BrancAMAP *)p->b;

	int base = b->getCurrentHierarc()->ordre+2;
	int compt = base+b->getCurrentEntnIndexWithinBranch()%(NBCOMPTRAND-base);



    //Parameter ID switch   
 /*
	if (p->numVar == gllnginit) {
        //Add std_dev on internode_initial_length (not for each stem internode, only the first one)

        //Retrieve parameter
        double internode_initial_length_std_dev = palmplugin_ptr->getSCALARParameter("stem#internode_length#internode_initial_length_standard_deviation");

        //Override
        p->var = palmplugin_ptr->addStandardDeviation(p->var,internode_initial_length_std_dev);

        //Check validity (internode length can't be negative)
        if (p->var < 0) {
            p->var = 0;
        }

    } else*/ //if ( p->numVar == NB_VAR+LP_STUMP*NB_VAR_ENT+rclnginitret1 ) {
	if (p->numVar == lnginit) {
        //Add std_dev on stump_length (obsolete: has been moved in lengthPlugin)
    
        /*
        int phyAge = (int)b->phyAgeInit; 
        if ( phyAge == STUMP ) {

            //Retrieve external_modules parameters
            double std_dev = palmplugin_ptr->getSCALARParameter("stem_stump#stump_length#stump_length_standard_deviation");

            //Override current value
            p->var = palmplugin_ptr->addStandardDeviation(p->var,std_dev);
        }
        */




    } else if (p->numVar == rgyoung) {
        //Override nervure_initial_young_module


        int phyAge = (int)b->phyAgeInit; 
        //int toto = (int)b->getCurrentGu()->phyAgeGu;
        if ( phyAge == RACHIS_LEFT || phyAge == RACHIS_RIGHT ) {

            //Retrieve internode position on palm
//            pos_on_palm = b->getCurrentEntnIndexWithinBranch();






            //------- Retrieve YOUNG MODULE parameters (RELATIVE TO THE CROWNS) --------

            //Retrieve external_module parameter
            double std_dev = palmplugin_ptr->getSCALARParameter("frond_nervure#frond_nervure_flexion#nervure_initial_young_module_standard_deviation");
            double initial_ym = p->var; //VERRUE -> move fpa_sequential to external_module : palmplugin_ptr->getSCALARParameter("frond_nervure#frond_nervure_flexion#nervure_initial_young_module");
			initial_ym = palmplugin_ptr->addStandardDeviation(initial_ym,std_dev,compt);
            double final_ym = palmplugin_ptr->getSCALARParameter("frond_nervure#frond_nervure_flexion#nervure_final_young_module");
            double variation_time = palmplugin_ptr->getSCALARParameter("frond_nervure#frond_nervure_flexion#nervure_young_module_variation_time");

            //Retrieve frond apparition speed from FPA parameter
//            double rythme = (*axeRef)[NB_VAR+LP_LEFT_RACHIS*NB_VAR_ENT+rerythme].ymin;
//            double rythme = axeRef->val1DParameter (rtrythme, RACHIS_LEFT-1, NULL, NULL);

            //Retrieve palm position on stem (palm number)
            pos_on_stem = b->getBearer()->getCurrentEntnIndexWithinBranch();

            //-------------------------------------------------

            double rythme=1;

            //Compute palm position relative to the crown
			pos_on_crowns = b->getBearer()->getCurrentEntnNumberWithinBranch()- pos_on_stem -2;

            //--------------------------------------------------------------------------------------------------
            

			if (pos_on_crowns/rythme > variation_time)
				p->var = final_ym;
			else
				p->var = initial_ym + (final_ym-initial_ym) * pos_on_crowns/rythme / variation_time;

            //DEBUG
            /*
            if(pos_on_palm==0)
                cout << "rythme=" << rythme << ", crowns_palm_number=" << crowns_palm_number << ", total_palm_number=" << total_palm_number << ", truncated_palm_number=" << truncated_palm_number << ", age => " << age << ", pos => " << pos_on_crowns << ", young module => " << p->var << endl;
            */




        } else if ( phyAge == FOLIOLE_LEFT_TOP || phyAge == FOLIOLE_RIGHT_TOP ) {

            //Retrieve internode position on palm
            pos_on_palm = b->getBearer()->getCurrentEntnIndexWithinBranch();


            //------- Retrieve YOUNG MMODULE parameters --------

            //Retrieve external_module parameter
            double std_dev = palmplugin_ptr->getSCALARParameter("upper_leaflet_on_rachis#leaflet_flexion#leaflet_initial_young_module_standard_deviation");
            double initial_ym = palmplugin_ptr->getSCALARParameter("upper_leaflet_on_rachis#leaflet_flexion#leaflet_initial_young_module");
 			initial_ym = palmplugin_ptr->addStandardDeviation(initial_ym,std_dev,compt);
            double final_ym = palmplugin_ptr->getSCALARParameter("upper_leaflet_on_rachis#leaflet_flexion#leaflet_final_young_module");
            double variation_time = palmplugin_ptr->getSCALARParameter("upper_leaflet_on_rachis#leaflet_flexion#leaflet_young_module_variation_time");

            //Retrieve frond apparition speed from FPA parameter
            double rythme = axeRef->val1DParameter (rtrythme, RACHIS_LEFT-1, NULL, NULL);

 			pos_on_stem = b->getBearer()->getBearer()->getCurrentEntnIndexWithinBranch();

            //-------------------------------------------------

            rythme=1;

            //Compute palm position relative to the crown
			pos_on_crowns = b->getBearer()->getCurrentEntnNumberWithinBranch()- pos_on_stem - 2;
            //-------------------------------------------------

			if (pos_on_crowns/rythme > variation_time)
				p->var = final_ym;
			else
				p->var = initial_ym + (final_ym-initial_ym) * pos_on_crowns/rythme / variation_time;

            //Debug
            //cout << "UPPER: p->var => " << p->var << endl; 

        } else if ( phyAge == FOLIOLE_LEFT_MIDDLE || phyAge == FOLIOLE_RIGHT_MIDDLE ) {


            //Retrieve internode position on palm
            pos_on_palm = b->getBearer()->getCurrentEntnIndexWithinBranch();


            //------- Retrieve YOUNG MODULE parameters --------

            //Retrieve external_module parameter
            double std_dev = palmplugin_ptr->getSCALARParameter("middle_leaflet_on_rachis#leaflet_flexion#leaflet_initial_young_module_standard_deviation");
            double initial_ym = palmplugin_ptr->getSCALARParameter("middle_leaflet_on_rachis#leaflet_flexion#leaflet_initial_young_module");
			initial_ym = palmplugin_ptr->addStandardDeviation(initial_ym,std_dev,compt);
            double final_ym = palmplugin_ptr->getSCALARParameter("middle_leaflet_on_rachis#leaflet_flexion#leaflet_final_young_module");
            double variation_time = palmplugin_ptr->getSCALARParameter("middle_leaflet_on_rachis#leaflet_flexion#leaflet_young_module_variation_time");

            //Retrieve frond apparition speed from FPA parameter
            //double rythme = (*axeRef)[NB_VAR+LP_LEFT_RACHIS*NB_VAR_ENT+rerythme].ymin;
//            double rythme = axeRef->val1DParameter (rtrythme, RACHIS_LEFT-1, NULL, NULL);

 			pos_on_stem = b->getBearer()->getBearer()->getCurrentEntnIndexWithinBranch();

            //-------------------------------------------------

            double rythme=1;

            //Compute palm position relative to the crown
			pos_on_crowns = b->getBearer()->getCurrentEntnNumberWithinBranch()- pos_on_stem - 2;
            //-------------------------------------------------
            
			if (pos_on_crowns/rythme > variation_time)
				p->var = final_ym;
			else
				p->var = initial_ym + (final_ym-initial_ym) * pos_on_crowns/rythme / variation_time;

            //Debug
            //cout << "MIDDLE: p->var => " << p->var << endl; 

        } else if ( phyAge == FOLIOLE_LEFT_BOTTOM || phyAge == FOLIOLE_RIGHT_BOTTOM ) {


            //Retrieve internode position on palm
            pos_on_palm = b->getBearer()->getCurrentEntnIndexWithinBranch();


            //------- Retrieve YOUNG MMODULE parameters --------

            //Retrieve external_module parameter
            double std_dev = palmplugin_ptr->getSCALARParameter("lower_leaflet_on_rachis#leaflet_flexion#leaflet_initial_young_module_standard_deviation");
            double initial_ym = palmplugin_ptr->getSCALARParameter("lower_leaflet_on_rachis#leaflet_flexion#leaflet_initial_young_module");
			initial_ym = palmplugin_ptr->addStandardDeviation(initial_ym,std_dev,compt);
            double final_ym = palmplugin_ptr->getSCALARParameter("lower_leaflet_on_rachis#leaflet_flexion#leaflet_final_young_module");
            double variation_time = palmplugin_ptr->getSCALARParameter("lower_leaflet_on_rachis#leaflet_flexion#leaflet_young_module_variation_time");

            //Retrieve frond apparition speed from FPA parameter
            //double rythme = (*axeRef)[NB_VAR+LP_LEFT_RACHIS*NB_VAR_ENT+rerythme].ymin;
//            double rythme = axeRef->val1DParameter (rtrythme, RACHIS_LEFT-1, NULL, NULL);

  			pos_on_stem = b->getBearer()->getBearer()->getCurrentEntnIndexWithinBranch();

            //-------------------------------------------------


            double rythme=1;

            //Compute palm position relative to the crown
			pos_on_crowns = b->getBearer()->getCurrentEntnNumberWithinBranch()- pos_on_stem - 2;

			if (pos_on_crowns/rythme > variation_time)
				p->var = final_ym;
			else
				p->var = initial_ym + (final_ym-initial_ym) * pos_on_crowns/rythme / variation_time;

            //Debug
            //cout << "LOWER: p->var => " << p->var << endl; 
        }



    } else if (p->numVar == rginflexion) {
        //Override rginflexion

        int phyAge = (int)b->phyAgeInit; 

        if ( phyAge == FOLIOLE_LEFT_TOP || phyAge == FOLIOLE_RIGHT_TOP ) {

            //ICIJ

        } else if ( phyAge == FOLIOLE_LEFT_MIDDLE || phyAge == FOLIOLE_RIGHT_MIDDLE ) {

            //ICIJ

        } else if ( phyAge == FOLIOLE_LEFT_BOTTOM || phyAge == FOLIOLE_RIGHT_BOTTOM ) {

            //ICIJ

        }


    } else if (p->numVar == rgconicite) {
        //Override rgconicite

        int phyAge = (int)b->phyAgeInit; 

        if ( phyAge == FOLIOLE_LEFT_TOP || phyAge == FOLIOLE_RIGHT_TOP ) {

            //ICIJ

        } else if ( phyAge == FOLIOLE_LEFT_MIDDLE || phyAge == FOLIOLE_RIGHT_MIDDLE ) {

            //ICIJ

        } else if ( phyAge == FOLIOLE_LEFT_BOTTOM || phyAge == FOLIOLE_RIGHT_BOTTOM ) {

            //ICIJ

        }




    } else if (p->numVar == anginsinit) {
        //Add std_dev on petiole_insertion_angle (angle between the petiole and the stem)
    
        int phyAge = (int)b->phyAgeInit; 
		if (   phyAge == RACHIS_LEFT
			|| phyAge == RACHIS_RIGHT) {

			//Retrieve external_modules parameters
			double std_dev = palmplugin_ptr->getSCALARParameter("frond_nervure#petiole_insertion_angle#petiole_initial_insertion_angle_on_the_stem_standard_deviation");

			//Radian conversion
			std_dev = degreeToRadian(std_dev);

			//Override current value
			p->var = palmplugin_ptr->addStandardDeviation(p->var,std_dev,compt);
		}





    } else if (   (p->numVar == NB_VAR+LP_LEFT_RACHIS*(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)+rgphyllotaxie)
               || (p->numVar == NB_VAR+LP_RIGHT_RACHIS*(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)+rgphyllotaxie) 
               || (p->numVar == NB_VAR+LP_STUMP*(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)+rgphyllotaxie) ) {  //STUMP NEED PHYLLOTAXY TOO !
        //Override palm on stem phyllotaxy angle (average and std_dev)
    



        //Retrieve current position on the stem
        int phyAge = (int)b->phyAgeInit;
        if ( phyAge == RACHIS_LEFT || phyAge == RACHIS_RIGHT || STUMP ) {
            pos = b->getBearer()->getCurrentEntnIndexWithinBranch();

        } else if ( phyAge >= FOLIOLE_LEFT_TOP && phyAge <= SPINE_RIGHT_BOTTOM ) { //All spines / leaflets (maybe obsolete: to be checked)
            pos = b->getBearer()->getBearer()->getCurrentEntnIndexWithinBranch(); //Retrieve entn index
        }


        //Retrieve external_modules parameters
        double angle = (palmplugin_ptr->external_module_parameters_1D["frond_nervure#frond_nervure_phyllotaxy#frond_nervure_phyllotaxic_angle_on_the_stem"])->getValue(pos);
        double std_dev = palmplugin_ptr->getSCALARParameter("frond_nervure#frond_nervure_phyllotaxy#frond_nervure_phyllotaxic_angle_on_the_stem_standard_deviation");

        //Radian conversion
        angle = degreeToRadian(angle);
        std_dev = degreeToRadian(std_dev);

        //Override
        p->var = palmplugin_ptr->addStandardDeviation(angle,std_dev,compt);


    } 
#if 0
	else if (   p->numVar == diaminit) {
  
        //Retrieve parameter
		int pos_on_stem = b->getBearer()->getCurrentEntnIndexWithinBranch();
        double std_dev = (palmplugin_ptr->external_module_parameters_1D["frond_nervure#frond_nervure_diameter#frond_nervure_base_diameter_standard_deviation"])->getValue(pos_on_stem);
        double diam = (palmplugin_ptr->external_module_parameters_1D["frond_nervure#frond_nervure_diameter#frond_nervure_base_diameter"])->getValue(pos_on_stem);

        //Override
		p->var = palmplugin_ptr->addStandardDeviation(diam,std_dev,(b->getCurrentHierarc()->ordre+2)%NBCOMPTRAND);


    } 
#endif
	else if (p->numVar == rtlagend) { //Palm number on the stem
        //Patch to take into account the "rythme"
        
        /*



        int phyAge = (int)b->phyAgeInit; 
        if ( phyAge == 1 ) {


            //Retrieve age
            double age = Scheduler::instance().getHotStop();


            p->var = p->var / age;
        }

        */

    } else if (p->numVar == rtprolgud) { //Stump dormancy
        //Patch to take into account the "rythme"

        /*



        int phyAge = (int)b->phyAgeInit; 
        if ( phyAge == 1000 ) {


            //Retrieve age
            double age = Scheduler::instance().getHotStop();


            p->var = p->var / age;
        }


        */




    } else if (p->numVar == glsurfhoup) {
        //Add std_dev on internode_initial_diameter

        //Retrieve parameter
        double internode_initial_diameter_std_dev = palmplugin_ptr->getSCALARParameter("stem#internode_diameter#internode_initial_diameter_standard_deviation");

        //Override
        p->var = palmplugin_ptr->addStandardDeviation(p->var,internode_initial_diameter_std_dev,compt);

    } else if (p->numVar == glangins) {
        //Add std_dev on palm_plantation_angle

        //Retrieve parameter
        double std_dev = palmplugin_ptr->getSCALARParameter("palm_tree#initial_plantation_direction#initial_plantation_direction_standard_deviation");
        std_dev = degreeToRadian(std_dev);

        //Override
        p->var = palmplugin_ptr->addStandardDeviation(p->var,std_dev,compt);

    } 
#if 0
	else if (p->numVar == rgpcntgross) { //rachis internode diameter
        //Compute "rachis entn diameter" following "rachis entn position/rang"

        //Retrieve phyAge
        int phyAge = (int)b->getCurrentGu()->phyAgeGu;

        //PhyAge switch 
        if (phyAge == RACHIS_LEFT || phyAge == RACHIS_RIGHT ) {

            pos = b->getCurrentEntnIndexWithinBranch(); //Retrieve entn index
            rachisInternodeNumber = b->getCurrentEntnNumberWithinBranch(); //Retrieve internode number

            //Compute rachis diameter
            //ICIJ
            //DiameterRatio= 1.0 / (double)sqrt(pos+1);
            //p->var = (float) (INITIAL_RACHIS_DIAMETER * DiameterRatio);
        }


    } 
#endif
	else if ( p->numVar == rtagelim ) { //Max UT number
        //
        // For the spine, we have to limit here the number of UT
        // This limit can't be set from AmapSim, because 
        // we transform foliole to spine during runtime
        // (this spine parameter is retrieved from the initial foliole,
        // thus the spine have the foliole value, not the spine value)
        //
        
        BrancAMAP * bearer = b->getBearer();
        GrowthUnitAMAP * gu = bearer->getCurrentGu();
        if(gu!=NULL) { //We are only interested for the case where gu is not null

            double phyAgeBearer = gu->phyAgeGu;

            if(phyAgeBearer==RACHIS_LEFT || phyAgeBearer==RACHIS_RIGHT) { //Rachis

                pos = bearer->getCurrentEntnIndexWithinBranch();                            //Retrieve new entn index
                //if ( isPetiole(pos,100) ) //spine only on the petiole
                //{
                //  p->var = 1; //Max GU = 1 (Max UT number = Max GU number)
                //} 
            }
        }
    } else if (   p->numVar == NB_VAR+LP_LEFT_LEAFLET*(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)+NB_GLOBAL_LP_VAR+rcsautramiant
               || p->numVar == NB_VAR+LP_RIGHT_LEAFLET*(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)+NB_GLOBAL_LP_VAR+rcsautramiant) // -> saut ramification immediate (2 et 3 correspondent aux plans lateraux folioles G et D)
    {
        //We manage here specifiq phyAge jump not supported by AmapSim (spine,-1,0,1).
        //In AmapSim, rachis create lateral production with only one phyAge.
        //As we need differents phyAge for folioles on the rachis, we force
        //the specials phyAge jumps here.

        

        //check if we get called in rachis context
        if (b->phyAgeInit == RACHIS_LEFT || b->phyAgeInit == RACHIS_RIGHT) {

            pos = b->getCurrentEntnIndexWithinBranch();                             //Retrieve new entn index
            //rachisInternodeNumber = b->getCurrentEntnNumberWithinBranch();        //Retrieve bearer internode index

            BundleTab * btab = (BundleTab *)b->getAdditionalData("bundle_tab");
            int typeFoliole = btab->fullPalmElemList.at(pos).folioleType;

            //phyAge switch
            if (p->var==FOLIOLE_LEFT_TOP) {

                if ( isPetiole(b,pos) ) {
                   if (typeFoliole == 2) {    
                        p->var = SPINE_LEFT_BOTTOM;
                    } else if (typeFoliole == 3)    {   
                        p->var = SPINE_LEFT_MIDDLE;
                    } else if (typeFoliole == 4){   
                        p->var = SPINE_LEFT_TOP;
                    }
                } else {
                    if (typeFoliole == -1) {    
                        p->var = FOLIOLE_LEFT_BOTTOM;
                    } else if (typeFoliole == 0)    {   
                        p->var = FOLIOLE_LEFT_MIDDLE;
                    } else if (typeFoliole == 1){   
                        p->var = FOLIOLE_LEFT_TOP;
                    }
                }


            } else if (p->var==FOLIOLE_RIGHT_TOP) {
                if ( isPetiole(b,pos) ) {
                   if (typeFoliole == 2) {    
                        p->var = SPINE_RIGHT_BOTTOM;
                    } else if (typeFoliole == 3)    {   
                        p->var = SPINE_RIGHT_MIDDLE;
                    } else if (typeFoliole == 4){   
                        p->var = SPINE_RIGHT_TOP;
                    }
                } else {
                    if (typeFoliole == -1) {    
                        p->var = FOLIOLE_RIGHT_BOTTOM;
                    } else if (typeFoliole == 0)    {   
                        p->var = FOLIOLE_RIGHT_MIDDLE;
                    } else if (typeFoliole == 1){   
                        p->var = FOLIOLE_RIGHT_TOP;
                    }
                }


			} else if (p->var==SPINE_LEFT_TOP) {
                if (typeFoliole == 2) {    
                    p->var = SPINE_LEFT_BOTTOM;
                } else if (typeFoliole == 3)    {   
                    p->var = SPINE_LEFT_MIDDLE;
                } else if (typeFoliole == 4){   
                    p->var = SPINE_LEFT_TOP;
                }
			} else if (p->var==SPINE_RIGHT_TOP) {
                if (typeFoliole == 2) {    
                    p->var = SPINE_RIGHT_BOTTOM;
                } else if (typeFoliole == 3)    {   
                    p->var = SPINE_RIGHT_MIDDLE;
                } else if (typeFoliole == 4){   
                    p->var = SPINE_RIGHT_TOP;
                }
			} else {
                //Bloc never reach
    
                cerr << "phyAge incorrect: " << p->var << endl;
			}
        } else if (   b->phyAgeInit == SPINE_LEFT_TOP || b->phyAgeInit == SPINE_LEFT_MIDDLE || b->phyAgeInit == SPINE_LEFT_BOTTOM
				   || b->phyAgeInit == SPINE_RIGHT_TOP || b->phyAgeInit == SPINE_RIGHT_MIDDLE || b->phyAgeInit == SPINE_RIGHT_BOTTOM) {
            p->var=b->phyAgeInit;
        } else if (   b->phyAgeInit == FOLIOLE_LEFT_TOP || b->phyAgeInit == FOLIOLE_LEFT_MIDDLE || b->phyAgeInit == FOLIOLE_LEFT_BOTTOM
				   || b->phyAgeInit == FOLIOLE_RIGHT_TOP || b->phyAgeInit == FOLIOLE_RIGHT_MIDDLE || b->phyAgeInit == FOLIOLE_RIGHT_BOTTOM) {
            p->var=b->phyAgeInit;
		}
		;
    } else if (p->numVar == rtprefn1) { //Z1 -> first zone distribution (n -> nombre  de tirage -> loi binomiale!!!)

        int phyAge = (int)b->getCurrentGu()->phyAgeGu; //Retrieve phyAge


        //phyAge switch
        if (phyAge == RACHIS_LEFT || phyAge == RACHIS_RIGHT ) {
            BundleTab * t = (BundleTab*)b->getAdditionalData("bundle_tab");         
            p->var = t->fullpalm_internode_number;
        }
    } 

    
    /*
    else    if ( p->numVar == NB_VAR+2*NB_VAR_ENT+rclnginitant || p->numVar == NB_VAR+3*NB_VAR_ENT+rclnginitant) {
            
            //Retrieve phyAge
            int phyAge = (int)b->getCurrentGu()->phyAgeGu;
            if (p->var == RACHIS_LEFT || p->var == RACHIS_RIGHT ) {

                // Tips: to simulate palm "foliole" bundle, we alter "rachis entn" length !!!
                //Retrieve foliole bundle informations
                BundleTab * t = (BundleTab*)b->getAdditionalData("bundle_tab");         

                //Retrieve "rachis entn" position
                int pos = b->getCurrentEntnIndexWithinBranch();

                double lg = t->fullPalmElemList.at(pos).lenght;


                //Compute "entn" length
                p->var = lg;

            }


    }
    */
}




/**
 *
 *
 *
 */
bool ParameterPlugin::isPetiole(BrancAMAP * b,int pos) {

    BundleTab * btab = (BundleTab *)b->getAdditionalData("bundle_tab");
    PalmElem relem = btab->fullPalmElemList.at(pos);    

    if(relem.folioleType < 2) 
        return false;
    else
        return true;
}



























/////////////////////////////////////////////////////
//
// This method get called for each new topology creation
//
// It stores UT global parameters 
//
/////////////////////////////////////////////////////
void DecompPlugin::on_notify (const state& , paramMsg * data) {
    int phyAge;
    

    //Cast msg parameters
    paramDecompAxeLevel *p = (paramDecompAxeLevel *)data;

    //Retrieve branch 
    BrancAMAP *b = (BrancAMAP *) p->d;

    //To move ASAP 
    palmplugin_ptr->plant_ptr = b->getPlant();
    //palmplugin_ptr->plant_ptr->setSeed(random());



    //Swith on axe object level (0 stand for branch, etc..)
    if (b->getLevel() == 0) {

        //Retrieve initial phyAge of the branch (at birth of)
        phyAge = (int)b->phyAgeInit;
        
		int base = b->getCurrentHierarc()->ordre+2;
		int compt = base+b->getCurrentEntnIndexWithinBranch()%(NBCOMPTRAND-base);


        //Switch on phyAge
        if (phyAge == RACHIS_LEFT) {




            //Retrieve palm_length
//            double palm_length_average = palmplugin_ptr->getSCALARParameter("frond_nervure#frond_nervure_length#frond_nervure_initial_length");
//            double palm_length_std_dev = palmplugin_ptr->getSCALARParameter("frond_nervure#frond_nervure_length#frond_nervure_initial_length_standard_deviation");
			int pos = b->getBearer()->getCurrentEntnIndexWithinBranch();
            double palm_length_average = (palmplugin_ptr->external_module_parameters_1D["frond_nervure#frond_nervure_length#frond_nervure_total_length"])->getValue(pos);
            double palm_length_std_dev = (palmplugin_ptr->external_module_parameters_1D["frond_nervure#frond_nervure_length#frond_nervure_total_length_standard_deviation"])->getValue(pos);
            palm_length = palmplugin_ptr->addStandardDeviation(palm_length_average, palm_length_std_dev,compt); //class member

            //Retrieve petiole length
            double petiole_length_ratio_average = palmplugin_ptr->getSCALARParameter("frond_nervure#petiole_on_nervure_length_ratio#petiole_on_frond_nervure_length_ratio");
            double petiole_length_ratio_std_dev = palmplugin_ptr->getSCALARParameter("frond_nervure#petiole_on_nervure_length_ratio#petiole_on_frond_nervure_length_ratio_standard_deviation");
            double petiole_length_ratio = palmplugin_ptr->addStandardDeviation(petiole_length_ratio_average, petiole_length_ratio_std_dev,compt);
            petiole_length = petiole_length_ratio * palm_length; //class member





            //Manage bundle
            BundleTab * t = computeBundle (palm_length,petiole_length,b); 

            //Debug
            //cout << "Left rachis computing" << endl;
            //t->toString();

            //Store all the stuff in a map for later use`
            if (p->d->getAdditionalData ("bundle_tab"))
				p->d->removeAdditionalData ("bundle_tab");
            p->d->addAdditionalData ("bundle_tab", t);

            //ICIJ
            //When will the map stuff delete occurs ??
        

        } else if (phyAge == RACHIS_RIGHT) {


            //Manage bundle
            BundleTab *t = computeBundle (palm_length,petiole_length, b); 

            //Debug
            //cout << "Right rachis computing" << endl;
            //t->toString();

            //Store all the stuff in a map for later use
            if (p->d->getAdditionalData ("bundle_tab"))
				p->d->removeAdditionalData ("bundle_tab");
			p->d->addAdditionalData ("bundle_tab", t);

            //ICIJ
            //When will the map stuff delete occurs ??
        }
    }
}









/////////////////////////////////////////////////////
//
// Make folioles bundle distribution 
//
/////////////////////////////////////////////////////
BundleTab * DecompPlugin::computeBundle (float fullpalm_length,double petiole_length, BrancAMAP *br) {
    int type = 0;
    int groupNumber = 0;
    int groupItem = 0;
    double cumul = 0;
    int rang = 0;

    BundleTab * b = new BundleTab; //Main container

	int base = br->getCurrentHierarc()->ordre+2;
	int compt = base+br->getCurrentEntnIndexWithinBranch()%(NBCOMPTRAND-base);

    double inter_group_distance = 0;
    double intra_group_distance = 0;
    double inter_group_distance_stddev = 0;
    double intra_group_distance_stddev = 0;


    //Retrieve average parameter
    double inter_group_distance_average;
    double intra_group_distance_average;
    //double inter_group_distance_average = palmplugin_ptr->getSCALARParameter("leaflet_on_rachis#leaflet_succession_on_rachis#inter_leaflets_groups_average_distance");
    //double intra_group_distance_average = palmplugin_ptr->getSCALARParameter("leaflet_on_rachis#leaflet_succession_on_rachis#inter_leaflets_inside_groups_average_distance");

    //Retrieve std_dev parameter
    //double inter_group_distance_stddev = palmplugin_ptr->getSCALARParameter("leaflet_on_rachis#leaflet_succession_on_rachis#inter_leaflets_groups_distance_standard_deviation");
    //double intra_group_distance_stddev = palmplugin_ptr->getSCALARParameter("leaflet_on_rachis#leaflet_succession_on_rachis#inter_leaflets_inside_groups_average_distance_standard_deviation");
    //double inter_spine_distance_stddev = palmplugin_ptr->getSCALARParameter("spine_on_petiole#spine_succession_on_petiole#inter-spine_distance_standard_deviation_on_petiole");

    

    /*** GROUP SIZE *****/
    int group_length[4];
    group_length[0] = 1;
    group_length[1] = 2;
    group_length[2] = 3;
    group_length[3] = 4;


    /*** GROUP **********/
    int groups[4][4];

    groups[0][0]=3;

    groups[1][0]=2;
    groups[1][1]=4;

    groups[2][0]=2;
    groups[2][1]=3;
    groups[2][2]=4;

    groups[3][0]=2;
    groups[3][1]=3;
    groups[3][2]=3;
    groups[3][3]=4;


    //Spine on petiole stuff
    //Retrieve group frequency
    CheckpointS * cg1 = palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_succession_on_petiole#1_spines_group_frequency"];
    CheckpointS * cg2 = palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_succession_on_petiole#2_spines_group_frequency"];
    CheckpointS * cg3 = palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_succession_on_petiole#3_spines_group_frequency"];
    CheckpointS * cg4 = palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_succession_on_petiole#4_spines_group_frequency"];

	double g1,g2,g3,g4;
    double current_length, pos_ajusted_to_100;
	int pos, zone_pos;

    rang = 0;
    groupItem = 1;
	groupNumber = 0;
	while(cumul<petiole_length) 
	{
        //Compute length / position
		//Retrieve current position on the full palm (petiole + rachis)
		pos = br->getBearer()->getCurrentEntnIndexWithinBranch();
		//Compute RELATIVE position on the petiole
		zone_pos = b->getRelativePositionOnPetiole(pos);
		//Scale conversion !!! (position parameters are stored as pourcent ratio, DON'T FORGET!!!)
		// pos_ajusted_to_100 = b->petiolePositionNORMALIZE(zone_pos);
		pos_ajusted_to_100 = b->positionNORMALIZE(cumul, petiole_length);
	    
        if(groupItem == group_length[groupNumber]) 
		{
            inter_group_distance_average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_succession_on_petiole#inter_spines_groups_average_distance"])->getValue(pos_ajusted_to_100);
            inter_group_distance_stddev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_succession_on_petiole#inter_spines_groups_distance_standard_deviation"])->getValue(pos_ajusted_to_100);
            inter_group_distance = palmplugin_ptr->addStandardDeviation(inter_group_distance_average, inter_group_distance_stddev,compt); //Add standard deviation
            cumul += inter_group_distance;
            current_length = inter_group_distance;

			g1 = cg1->getValue(pos_ajusted_to_100);
			g2 = cg2->getValue(pos_ajusted_to_100);
			g3 = cg3->getValue(pos_ajusted_to_100);
			g4 = cg4->getValue(pos_ajusted_to_100);
            groupNumber = getGroupNumber(g1, g2, g3, g4); //Get new group
			groupItem = 0;
        } 
		else 
		{
			intra_group_distance_average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_succession_on_petiole#inter_spines_inside_groups_average_distance"])->getValue(pos_ajusted_to_100);
			intra_group_distance_stddev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_succession_on_petiole#inter_spines_inside_groups_average_distance_standard_deviation"])->getValue(pos_ajusted_to_100);
            intra_group_distance = palmplugin_ptr->addStandardDeviation(intra_group_distance_average, intra_group_distance_stddev,compt); //Add standard deviation
            cumul += intra_group_distance;
            current_length = intra_group_distance;
        }

        if(cumul>petiole_length) 
		{
            b->addRachisElem(rang,petiole_length,3,petiole_length-(cumul-current_length)); //Treat last one as a special case
        } 
		else 
		{
			 //Retrieve foliole type (-1,0,1)
			type = groups[groupNumber][groupItem];       
			b->addRachisElem(rang,cumul,type,current_length); 
        }
        
        groupItem++;

        rang++;
    }



    groups[0][0]=0;

    groups[1][0]=1;
    groups[1][1]=-1;

    groups[2][0]=1;
    groups[2][1]=0;
    groups[2][2]=-1;

    groups[3][0]=1;
    groups[3][1]=0;
    groups[3][2]=0;
    groups[3][3]=-1;

   //Set petiole internode number
    b->petiole_internode_number = rang;

    //Retrieve group frequency
    cg1 = palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_succession_on_rachis#1_leaflets_group_frequency"];
    cg2 = palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_succession_on_rachis#2_leaflets_group_frequency"];
    cg3 = palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_succession_on_rachis#3_leaflets_group_frequency"];
    cg4 = palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_succession_on_rachis#4_leaflets_group_frequency"];

    //Foliole on rachis stuff

    groupNumber = getGroupNumber(g1, g2, g3, g4);
    groupItem = 0;
    while(cumul<fullpalm_length) 
	{ 
        //Compute length / position
		//Retrieve current position on the full palm (petiole + rachis)
		pos = br->getBearer()->getCurrentEntnIndexWithinBranch();
		//Compute RELATIVE position on the petiole
		zone_pos = b->getRelativePositionOnRachis(pos);
		//Scale conversion !!! (position parameters are stored as pourcent ratio, DON'T FORGET!!!)
		// pos_ajusted_to_100 = b->rachisPositionNORMALIZE(zone_pos);
		pos_ajusted_to_100 = b->positionNORMALIZE(cumul-petiole_length, fullpalm_length-petiole_length);
	    
        if(groupItem == group_length[groupNumber]) 
		{
            inter_group_distance_average = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_succession_on_rachis#inter_leaflets_groups_average_distance"])->getValue(pos_ajusted_to_100);
            inter_group_distance_stddev = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_succession_on_rachis#inter_leaflets_groups_distance_standard_deviation"])->getValue(pos_ajusted_to_100);
            inter_group_distance = palmplugin_ptr->addStandardDeviation(inter_group_distance_average, inter_group_distance_stddev,compt); //Add standard deviation
            cumul += inter_group_distance;
            current_length = inter_group_distance;

			g1 = cg1->getValue(pos_ajusted_to_100);
			g2 = cg2->getValue(pos_ajusted_to_100);
			g3 = cg3->getValue(pos_ajusted_to_100);
			g4 = cg4->getValue(pos_ajusted_to_100);
            groupNumber = getGroupNumber(g1, g2, g3, g4); //Get new group
			groupItem = 0;
        } 
		else 
		{
			intra_group_distance_average = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_succession_on_rachis#inter_leaflets_inside_groups_average_distance"])->getValue(pos_ajusted_to_100);
			intra_group_distance_stddev = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_succession_on_rachis#inter_leaflets_inside_groups_average_distance_standard_deviation"])->getValue(pos_ajusted_to_100);
            intra_group_distance = palmplugin_ptr->addStandardDeviation(intra_group_distance_average, intra_group_distance_stddev,compt); //Add standard deviation
            cumul += intra_group_distance;
            current_length = intra_group_distance;
        }

        if(cumul>fullpalm_length) 
		{
            b->addRachisElem(rang,fullpalm_length,0,fullpalm_length-(cumul-current_length)); //Treat last one as a special case
        } 
		else 
		{
			 //Retrieve foliole type (-1,0,1)
			type = groups[groupNumber][groupItem];       
			b->addRachisElem(rang,cumul,type,current_length); 
        }
        
        groupItem++;

        rang++;
    }



    //Set rachis length
    b->rachis_length=fullpalm_length - petiole_length;

    //Set petiole length
    b->petiole_length=petiole_length;

    //Set fullpalm length
    b->fullpalm_length=fullpalm_length;

    //Set fullpalm internode number
    b->fullpalm_internode_number = rang;

    //Set rachis internode number
    b->rachis_internode_number = (rang - b->petiole_internode_number);

    //Set rachis initial diameter
    	AxisReference * axeRef = (AxisReference *)palmplugin_ptr->plant_ptr->getParamData(string("AMAPSimMod"));
	int pos_on_stem = br->getBearer()->getCurrentEntnIndexWithinBranch();
	//double v = axeRef->val2DParameter(NB_VAR+LP_LEFT_RACHIS*NB_VAR_ENT+rcgrossinitret1, 1, pos_on_stem, br);
	double v = (palmplugin_ptr->external_module_parameters_1D["frond_nervure#frond_nervure_diameter#frond_nervure_initial_diameter"])->getValue(pos_on_stem);
	double std_dev = (palmplugin_ptr->external_module_parameters_1D["frond_nervure#frond_nervure_diameter#frond_nervure_initial_diameter_standard_deviation"])->getValue(pos_on_stem);
	b->rachis_initial_diameter = palmplugin_ptr->addStandardDeviation(v,std_dev,compt);

    return b;
}






/**
 *
 *
 *
 */
int DecompPlugin::getGroupNumber(double g1, double g2, double g3, double g4) {
    int group = 0;
    double random = palmplugin_ptr->plant_ptr->getConfigData().getRandomGenerator()->Random();


    //Total effectif 
    double total = g1 + g2 + g3 + g4;

    //Check
    if( abs(total-100)> .001 ) {
        cout << "EJ30001: total incorrect (total=" << total << ")" << endl;
        //char ccc; cin >> ccc;
        //throw 30001;
    }

    //Compute frequency
    double freq_g1 = g1 / total;
    double freq_g2 = g2 / total;
    double freq_g3 = g3 / total;
    double freq_g4 = g4 / total;

    //Compute frequency cumul
    double freq_cumul_g1 = freq_g1;
    double freq_cumul_g2 = freq_g2 + freq_cumul_g1;
    double freq_cumul_g3 = freq_g3 + freq_cumul_g2;
    double freq_cumul_g4 = freq_g4 + freq_cumul_g3;

    //Debug
    //cout << freq_cumul_g1 << " " << freq_cumul_g2 << " " << freq_cumul_g3 << " " << freq_cumul_g4 << endl;

    if((0<=random) && (random<=freq_cumul_g1)) {
        group = 0;
    } else if ((freq_cumul_g1<random) && (random<=freq_cumul_g2)) {
        group = 1;
    } else if ((freq_cumul_g2<random) && (random<=freq_cumul_g3)) {
        group = 2;
    } else if ((freq_cumul_g3<random) && (random<=freq_cumul_g4)) {
        group = 3;
    } else {
        cout << "EJ30002" << endl;  
    }

    //cout << group << endl;

    return group;
}






/////////////////////////////////////////////////////
//
// This methods get called in geometry context, when <Length> parameter 
// is retrieved. 
// 
// We can override AmapSim normal behavior here, or just let 
// the <Length> parameter as is.
//
/////////////////////////////////////////////////////
void LengthPlugin::on_notify (const state& , paramMsg * data) {
    int pos = 0;
    double lg = 0;
    double std_dev = 0;
    double average = 0;

    //Cast message parameters
    paramElemGeom * p = (paramElemGeom*)data;

    //Retrieve branch 
    BrancAMAP *b = (BrancAMAP *)p->gec->getPtrBrc()->getBranc();

	int base = b->getCurrentHierarc()->ordre+2;
	int compt = base+b->getCurrentEntnIndexWithinBranch()%(NBCOMPTRAND-base);

	//Retrieve PA (Retrieve TU/GU from branch then retrieve PA)
    int phyAge = (int)b->getCurrentGu()->phyAgeGu; 



    if (phyAge >= STIPE_START && phyAge <= STIPE_END) {
        /**** Uncomment here if you want std_dev for each stem internode ***/

        //Retrieve parameter
//        double internode_length_std_dev = palmplugin_ptr->getSCALARParameter("stem#internode_length#internode_initial_length_standard_deviation");
		int pos = b->getCurrentEntnIndexWithinBranch();
        double internode_length_std_dev = (palmplugin_ptr->external_module_parameters_1D["stem#internode_length#internode_length_inside_the_stem_standard_deviation"])->getValue(pos);

        //Override
        p->v = palmplugin_ptr->addStandardDeviation(p->v,internode_length_std_dev,compt);

        //Check validity
        if (p->v < 0) {
            p->v = 0;
		/**/
        }
    } else if (phyAge == ARROW) {

        double lg = palmplugin_ptr->getSCALARParameter("arrow_frond#arrow_frond_length#arrow_frond_length");

        p->v = ( lg / 100 ); //Needed, because the parameter represent the full arrow length, while p->v represent only 1 internode (the arrow is 100 internode long)

    } else if (phyAge == STUMP) {
        //Add std_dev on stump_length
    
        //Retrieve external_modules parameters
        double std_dev = palmplugin_ptr->getSCALARParameter("stem_stump#stump_length#stump_length_standard_deviation");
        double lg = palmplugin_ptr->getSCALARParameter("stem_stump#stump_length#stump_length");

        //Add std_dev
        p->v = palmplugin_ptr->addStandardDeviation(lg,std_dev,compt);

        //Total stump length to internode length (1 stump is composed by 10 internode..)
        p->v = p->v / 10;

        //Validity check
        if (p->v<0) {
            p->v = 0;
        }

    } else if (phyAge == RACHIS_LEFT || phyAge == RACHIS_RIGHT ) {
        // Tips: to simulate palm "foliole" bundle (leaflet groups), we alter "rachis entn" length !!!
        //Retrieve foliole bundle informations
        //ICIJ -> come back here after a check in topo_decomp stuff
        BundleTab * t = (BundleTab*)b->getAdditionalData("bundle_tab");         

        //Retrieve "rachis entn" position
        pos = b->getCurrentEntnIndexWithinBranch();

        //Retrieve pre-computed "entn" length
        lg = t->fullPalmElemList.at(pos).lenght;
        p->v = lg;

    } else if (   (phyAge >= SPINE_LEFT_TOP && phyAge <= SPINE_LEFT_BOTTOM+5)
			   || (phyAge >= SPINE_RIGHT_TOP && phyAge <= SPINE_RIGHT_BOTTOM+5)) {
        //We manage here the spine length

        //Debug
        //cout << "LENGTH: before p->v => " << p->v;
        
        //Retrieve internode number (spine length is indexed on position)
        BundleTab * t = (BundleTab*)b->getBearer()->getAdditionalData("bundle_tab");         

        if(t->fullpalm_internode_number>0) { //This test avoid divided_by_zero exception


            //Add standard deviation (for the first spine internode only, not for each spine internode)
            if(b->getCurrentEntnIndexWithinBranch()==0) {
                //We are on the first internode of a new spine

                //Retrieve current position on the full palm (petiole + rachis)
                pos = b->getBearer()->getCurrentEntnIndexWithinBranch();

                //Compute RELATIVE position on the petiole
                //int petiole_pos = t->getRelativePositionOnPetiole(pos);

                //Scale conversion !!! (position parameters are stored as pourcent ratio, DON'T FORGET!!!)
                double pos_ajusted_to_100 = t->petiolePositionNORMALIZE(pos);

                //Retrieve the length corresponding to the position
                average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_length#spine_length"])->getValue(pos_ajusted_to_100);
                std_dev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_length#spine_length_standard_deviation"])->getValue(pos_ajusted_to_100);

                static_spine_length = palmplugin_ptr->addStandardDeviation(average, std_dev,compt); 
            }

            //Validity check
            if (static_spine_length<0) {
                static_spine_length = 0;
            }

        } else {
            static_spine_length = 0; //When no internode at all, we set length to zero.
        }


        //Total spine length to internode length (1 spine is composed by 10 internodes)
        double spine_internode_length = static_spine_length / 10;


        //Override
        p->v = p->v * spine_internode_length;

        //Debug
        //cout << ", after p->v => " << p->v << ", spine_internode_length => " << spine_internode_length << endl;
        
    } else if (   (phyAge >= FOLIOLE_LEFT_TOP && phyAge <= FOLIOLE_LEFT_BOTTOM+5)
			   || (phyAge >= FOLIOLE_RIGHT_TOP && phyAge <= FOLIOLE_RIGHT_BOTTOM+5)) {
        //We manage here the foliole length 

        //Retrieve internode number (foliole length is indexed on position)
        BundleTab * t = (BundleTab*)b->getBearer()->getAdditionalData("bundle_tab");         

        if(t->fullpalm_internode_number>0) { //This test avoid divided_by_zero exception

            //Add standard deviation (for the first leaflet internode only, not for each leaflet internode)
            if(b->getCurrentEntnIndexWithinBranch()==0) { 

                //Retrieve current position on the full palm (petiole + rachis)
                pos = b->getBearer()->getCurrentEntnIndexWithinBranch();
				int pos_on_stem = b->getBearer()->getBearer()->getCurrentEntnIndexWithinBranch() + 1;


                //Compute RELATIVE position on the rachis 
                // int rachis_pos = t->getRelativePositionOnRachis(pos);

                //Scale conversion !!! (position parameters are stored as pourcent ratio, DON'T FORGET!!!)
                double pos_ajusted_to_100 = t->rachisPositionNORMALIZE(pos);

                //Retrieve the length corresponding to the position
                double coef = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_length#ratio_applied_on_leaflet_maximum_length_for_leaflets_on_rachis"])->getValue(pos_ajusted_to_100);
                average = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_length#leaflet_maximum_length_on_rachis"])->getValue(pos_on_stem);
				average *= coef;
                std_dev = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_length#leaflet_length_on_rachis_standard_deviation"])->getValue(pos_ajusted_to_100);


                static_leaflet_length = palmplugin_ptr->addStandardDeviation(average, std_dev,compt);
            }

            //Validity check
            if (static_leaflet_length<0) {
                static_leaflet_length = 0;
            }
      
        } else {
            static_leaflet_length = 0; //When no internode at all, we set length to zero.
        }


        //Manage leaflet RATIO
        int pos_on_leaflet = b->getCurrentEntnIndexWithinBranch() + 1; //Index begins at 1 in PARAMEDIT, while internode index begins at 0 !!!
        double ratio = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_length#ratio_applied_on_leaflet_length_for_leaflet_segments_lengthes"])->getValue(pos_on_leaflet);
            

        p->v = static_leaflet_length * ratio;
    }
}






















/////////////////////////////////////////////////////
//
//
// This methods get called in geometry context, when <Diameter> parameter 
// is retrieved.
//
/////////////////////////////////////////////////////
void DiameterPlugin::on_notify (const state& , paramMsg * data) {
    int pos = 0;
    double std_dev = 0;
    double average = 0;

    paramElemGeom * p = (paramElemGeom*)data; //Retrieve msg params

    BrancAMAP *b = (BrancAMAP *)p->gec->getPtrBrc()->getBranc(); //Retrieve current branch

    int phyAge = (int)b->getCurrentGu()->phyAgeGu; //Retrieve current branch physiological age

	int base = b->getCurrentHierarc()->ordre+2;
	int compt = base+b->getCurrentEntnIndexWithinBranch()%(NBCOMPTRAND-base);

    //PA switch
    if (phyAge >= STIPE_START && phyAge <= STIPE_END) {
        ////Retrieve entn position
        //int pos = b->getCurrentEntnIndexWithinBranch();

        ////Bulbe stuff
        //if (pos <= BULBE_END_POS) 
        //  p->v = bulbe (p->v, pos);
        



    } 
	else if (phyAge == RACHIS_LEFT || phyAge == RACHIS_RIGHT ) {
        BundleTab * t = (BundleTab*)b->getAdditionalData("bundle_tab");         
		double pos_ajusted_to_100 = t->fullpalmPositionNORMALIZE(b->getCurrentEntnIndexWithinBranch());
/*
		AxisReference * axeRef = ((PlantAMAP *)palmplugin_ptr->plant_ptr)->getAxisReference();
		double v = axeRef->val2DParameter(NB_VAR+LP_LEFT_RACHIS*NB_VAR_ENT+rcgrossinitret1, 1, (int)pos_ajusted_to_100, b);
		int pos_on_stem = b->getBearer()->getCurrentEntnIndexWithinBranch();
        double std_dev = (palmplugin_ptr->external_module_parameters_1D["frond_nervure#frond_nervure_diameter#frond_nervure_initial_diameter_standard_deviation"])->getValue(pos_on_stem);
		p->v = palmplugin_ptr->addStandardDeviation(v*coef,std_dev,(b->getCurrentHierarc()->ordre+2)%NBCOMPTRAND);
*/
    		AxisReference * axeRef = (AxisReference *)palmplugin_ptr->plant_ptr->getParamData(string("AMAPSimMod"));
		double coef = axeRef->val2DParameter(rgpcntgross, phyAge, (int)pos_ajusted_to_100, b);


        //Override
		p->v = t->rachis_initial_diameter * coef;


	}
	else if (   (phyAge >= SPINE_LEFT_TOP && phyAge <= SPINE_LEFT_BOTTOM+5)
			 || (phyAge >= SPINE_RIGHT_TOP && phyAge <= SPINE_RIGHT_BOTTOM+5)) { 
       //We manage here the spine diameter

        //Debug
        //cout << "DIAMETER: before p->v => " << p->v;


        //Retrieve internode number (spine diameter is indexed on position)
        BundleTab * t = (BundleTab*)b->getBearer()->getAdditionalData("bundle_tab");         

        if(t->fullpalm_internode_number>0) { //This test avoid divided_by_zero exception


            //Add standard deviation (for new spine only, not for each spine internodes)
            if(b->getCurrentEntnIndexWithinBranch()==0) { 
                //We are on the first internode of a new spine

                //Retrieve current position on the full palm (petiole + rachis)
                pos = b->getBearer()->getCurrentEntnIndexWithinBranch();

                //Compute RELATIVE position on the petiole
                //int petiole_pos = t->getRelativePositionOnPetiole(pos);

                //Scale conversion !!! (position parameters are stored as pourcent ratio, DON'T FORGET!!!)
                double pos_ajusted_to_100 = t->petiolePositionNORMALIZE(pos);
                
                //Retrieve the diameter_ratio corresponding to the position
                average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_diameter#spine_base_diameter"])->getValue(pos_ajusted_to_100);
                std_dev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_diameter#spine_base_diameter_standard_deviation"])->getValue(pos_ajusted_to_100);

                static_spine_diameter = palmplugin_ptr->addStandardDeviation(average, std_dev,compt); 
            }

            //Validity check
            if (static_spine_diameter<0) {
                static_spine_diameter = 0;
            }

        } else {
            static_spine_diameter = 0; //When no internode at all, we set diameter to zero.
        }

        //Override
        p->v = p->v * static_spine_diameter;

        //Debug
        //cout << ", after p->v => " << p->v << ", static_spine_diameter => " << static_spine_diameter << endl;

	 } else if (   (phyAge >= FOLIOLE_LEFT_TOP && phyAge <= FOLIOLE_LEFT_BOTTOM+5) 
			    || (phyAge >= FOLIOLE_RIGHT_TOP && phyAge <= FOLIOLE_RIGHT_BOTTOM+5) ) {
	//We manage here the leaflet width

        //Debug
        //cout << "DIAMETER: before p->v => " << p->v;


        //Retrieve internode number (spine diameter is indexed on position)
        BundleTab * t = (BundleTab*)b->getBearer()->getAdditionalData("bundle_tab");         

        if(t->fullpalm_internode_number>0) { //This test avoid divided_by_zero exception



            //Add standard deviation (for new spine only, not for each spine internodes)
            if(b->getCurrentEntnIndexWithinBranch()==0) { 
                //We are on the first internode of a new spine
                //Retrieve current position on the full palm (petiole + rachis)
                pos = b->getBearer()->getCurrentEntnIndexWithinBranch();
				int pos_on_stem = b->getBearer()->getBearer()->getCurrentEntnIndexWithinBranch() + 1;

                //Compute RELATIVE position on the petiole
                //int rachis_pos = t->getRelativePositionOnRachis(pos);

                //Scale conversion !!! (position parameters are stored as pourcent ratio, DON'T FORGET!!!)
                double pos_ajusted_to_100 = t->rachisPositionNORMALIZE(pos);

                //Retrieve the diameter_ratio corresponding to the position
//                average = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_width#leaflet_initial_width_on_rachis"])->getValue(pos_ajusted_to_100);
                double coef = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_width#ratio_applied_on_leaflet_maximum_width_for_leaflets_on_rachis"])->getValue(pos_ajusted_to_100);
                average = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_width#leaflet_maximum_width_on_rachis"])->getValue(pos_on_stem);
				average *= coef;
                std_dev = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_width#leaflet_width_on_rachis_standard_deviation"])->getValue(pos_ajusted_to_100);

                static_leaflet_width = palmplugin_ptr->addStandardDeviation(average, std_dev,compt); 
            }




            //Validity check
            if (static_leaflet_width<0) {
                static_leaflet_width = 0;
            }

        } else {
            static_leaflet_width = 0; //When no internode at all, we set diameter to 1
        }


        //Manage leaflet RATIO
        int pos_on_leaflet = b->getCurrentEntnIndexWithinBranch() + 1; //Index begins at 1 in PARAMEDIT, while internode index begins at 0 !!!
        double ratio = (palmplugin_ptr->external_module_parameters_1D["leaflet_on_rachis#leaflet_width#ratio_applied_on_leaflet_width_for_leaflet_segments_widthes"])->getValue(pos_on_leaflet);


        //Override
        p->v = static_leaflet_width * ratio;


        //Debug
        //cout << ", after p->v => " << p->v << ", static_spine_diameter => " << static_spine_diameter << endl;
    }


}









/**
 *
 *
 * This function get called at the last stage
 *
 *
 *
 */
void PositionPlugin::on_notify(const state& st, paramMsg *data) {
    double angle = 0;
    double average = 0;
    double std_dev = 0;

    AxisReference * axeRef = (AxisReference *)palmplugin_ptr->plant_ptr->getParamData(string("AMAPSimMod"));

    paramElemPosition * p = (paramElemPosition*)data;

    GeomElemCone *gec = (GeomElemCone *)p->gec;
    
    GeomBrancAMAP * geomBranc = (GeomBrancAMAP *)gec->getPtrBrc();

    BrancAMAP *b = (BrancAMAP *)geomBranc->getBranc();  

    //Retrieve phyAge
    int phyAge = (int)b->getCurrentGu()->phyAgeGu;	
	//Modify petiole flexion
	if(phyAge==RACHIS_LEFT) {
		
		BundleTab * t = (BundleTab*)b->getAdditionalData("bundle_tab");		
		if(gec->getPosInGeomBranc() >= 1 && gec->getPosInGeomBranc() < t->petiole_internode_number) {			
			
			int pos_on_palm = b->getBearer()->getCurrentEntnIndexWithinBranch();

            //Retrieve external_module parameter
              
			double initial_a = palmplugin_ptr->getSCALARParameter("frond_nervure#petiole_bending_angle#petiole_initial_bending_angle");
            double final_a = palmplugin_ptr->getSCALARParameter("frond_nervure#petiole_bending_angle#petiole_final_bending_angle");
			double variation_time = palmplugin_ptr->getSCALARParameter("frond_nervure#petiole_bending_angle#petiole_bending_angle_variation_time");
			//Retrieve frond apparition speed from FPA parameter
			//double rythme = (*axeRef)[NB_VAR+LP_LEFT_RACHIS*NB_VAR_ENT+rerythme].ymin;
            double rythme = axeRef->val1DParameter (rtrythme, RACHIS_LEFT-1, NULL, NULL);

			//Retrieve palm position on stem (palm number)
			int pos_on_stem = b->getBearer()->getCurrentEntnIndexWithinBranch();

            //-------------------------------------------------

           //Compute palm position relative to the crown
			int pos_on_crowns = b->getBearer()->getCurrentEntnNumberWithinBranch()- pos_on_stem - 2;

			if (pos_on_crowns/rythme > variation_time)
				angle_point_c = final_a;
			else
				angle_point_c = initial_a + (final_a-initial_a) * pos_on_crowns/rythme / variation_time;


            float angle_intn = angle_point_c/((float)t->petiole_internode_number-1);
			  
			  GeomElemCone *gecprec = static_cast<GeomElemCone *>(geomBranc->getElementsGeo().at(gec->getPosInGeomBranc()-1));
			  Matrix4 prec_triedron = gecprec->getMatrix();			  
			  Matrix4 * current_triedron_pt = &p->p;			  
			  Matrix4 & current_triedron = *current_triedron_pt;			  
			  Vector3 translation = prec_triedron.getTranslation();
			  //Move to origin
			  current_triedron = Matrix4::translation(-translation) * prec_triedron;
			  current_triedron = Matrix4::axisRotation(prec_triedron.getNormalVector(), degreeToRadian(-angle_intn)) * current_triedron;
			  current_triedron = Matrix4::translation(translation) * current_triedron;			  
			  p->set(gec,current_triedron);
		}
		

	} else {

		if (phyAge==SPINE_RIGHT_TOP || phyAge==SPINE_RIGHT_MIDDLE || phyAge==SPINE_RIGHT_BOTTOM || 
			phyAge==FOLIOLE_RIGHT_TOP || phyAge==FOLIOLE_RIGHT_MIDDLE || phyAge==FOLIOLE_RIGHT_BOTTOM || 
			phyAge==SPINE_LEFT_TOP || phyAge==SPINE_LEFT_MIDDLE || phyAge==SPINE_LEFT_BOTTOM || 
			phyAge==FOLIOLE_LEFT_TOP || phyAge==FOLIOLE_LEFT_MIDDLE || phyAge==FOLIOLE_LEFT_BOTTOM) { 


			if(gec->getPosInGeomBranc() == -1) { // -1 -> we are on geom Init

				Matrix4 * current_triedron_pt = NULL, trans = Matrix4::IDENTITY;
				int base = b->getCurrentHierarc()->ordre+2;
				int compt = base+geomBranc->getPosOnBearer()%(NBCOMPTRAND-base);


				//------- TO BE MERGED --------------
				//Retrieve RIGHT triedron 
				if  (phyAge==SPINE_RIGHT_TOP || phyAge==SPINE_RIGHT_MIDDLE || phyAge==SPINE_RIGHT_BOTTOM || 
					 phyAge==FOLIOLE_RIGHT_TOP || phyAge==FOLIOLE_RIGHT_MIDDLE || phyAge==FOLIOLE_RIGHT_BOTTOM ) {                   
					current_triedron_pt = (Matrix4 *)geomBranc->getAdditionalData("translation");   
				} 
	        
				//Retrieve LEFT triedron 
				if(current_triedron_pt == NULL) {
					current_triedron_pt = &p->p;
					//// add bearer node length;
					//GeomBrancAMAP *bear = (GeomBrancAMAP*)(geomBranc->getGeomBrancBearer());
					//GeomElem *gbear = bear->getElementsGeo().at(geomBranc->getPosOnBearer());
					//float l = gbear->getLength();
					//Vector3 v = gbear->getMatrix().getMainVector();
					//v = v*l;
					//trans = Matrix4::translation(v);
					//trans = Matrix4::translation(gbear->getMatrix().getMainVector()*gbear->getLength());
				}
				//-------------------------------------



				//??
				Matrix4 & current_triedron = *current_triedron_pt;
				current_triedron = current_triedron * trans;
	            
	            
				//??
				Vector3 translation = current_triedron.getTranslation();


				//Move to origin
				current_triedron = Matrix4::translation(-translation) * current_triedron;




				//Retrieve additional DATA
				BundleTab * t = (BundleTab*)b->getBearer()->getAdditionalData("bundle_tab");


				//Normalize position
				double pos_ajusted_to_100 = t->fullpalmPositionNORMALIZE(position_plugin_counter); 



				if (phyAge==SPINE_LEFT_TOP || phyAge==SPINE_RIGHT_TOP ) {
					//VERTICAL
					average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#upper_spine_insertion_angle#upper_spine_vertical_insertion_angle_on_petiole"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#upper_spine_insertion_angle#upper_spine_vertical_insertion_angle_on_petiole_standard_deviation"])->getValue(pos_ajusted_to_100);

					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					//Patch left rachis
					if (phyAge == SPINE_LEFT_TOP) {
						average = - average;
					}

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),angle) * current_triedron; //Rotation autour de la direction Y


					//HORIZONTAL
					average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#upper_spine_insertion_angle#upper_spine_horizontal_insertion_angle_on_petiole"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#upper_spine_insertion_angle#upper_spine_horizontal_insertion_angle_on_petiole_standard_deviation"])->getValue(pos_ajusted_to_100);
					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getNormalVector(),angle) * current_triedron; //Rotation autour de la direction X
					if (phyAge == SPINE_LEFT_TOP) {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(-90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}
					else {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}

	                
				} else if (phyAge==SPINE_LEFT_MIDDLE || phyAge==SPINE_RIGHT_MIDDLE ) {

					//VERTICAL
					average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#middle_spine_insertion_angle#middle_spine_vertical_insertion_angle_on_petiole"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#middle_spine_insertion_angle#middle_spine_vertical_insertion_angle_on_petiole_standard_deviation"])->getValue(pos_ajusted_to_100);

					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					//Patch left rachis
					if (phyAge == SPINE_LEFT_MIDDLE) {
						average = - average;
					}
					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),angle) * current_triedron; //Rotation autour de la direction Y


					//HORIZONTAL
					average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#middle_spine_insertion_angle#middle_spine_horizontal_insertion_angle_on_petiole"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#middle_spine_insertion_angle#middle_spine_horizontal_insertion_angle_on_petiole_standard_deviation"])->getValue(pos_ajusted_to_100);
					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getNormalVector(),angle) * current_triedron; //Rotation autour de la direction X
					if (phyAge == SPINE_LEFT_MIDDLE) {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(-90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}
					else {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}
				} else if (phyAge==SPINE_LEFT_BOTTOM || phyAge==SPINE_RIGHT_BOTTOM) {

					//VERTICAL
					average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#lower_spine_insertion_angle#lower_spine_vertical_insertion_angle_on_petiole"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#lower_spine_insertion_angle#lower_spine_vertical_insertion_angle_on_petiole_standard_deviation"])->getValue(pos_ajusted_to_100);

					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					//Patch left rachis
					if (phyAge == SPINE_LEFT_BOTTOM) {
						average = - average;
					}

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),angle) * current_triedron; //Rotation autour de la direction Y


					//HORIZONTAL
					average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#lower_spine_insertion_angle#lower_spine_horizontal_insertion_angle_on_petiole"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#lower_spine_insertion_angle#lower_spine_horizontal_insertion_angle_on_petiole_standard_deviation"])->getValue(pos_ajusted_to_100);
					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getNormalVector(),angle) * current_triedron; //Rotation autour de la direction X
					if (phyAge == SPINE_LEFT_BOTTOM) {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(-90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}
					else {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}

					//double vert_angle_spine_average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_insertion_on_petiole#spine_vertical_insertion_angle_on_petiole"])->getValue(pos_ajusted_to_100);
					//double hori_angle_spine_average = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_insertion_on_petiole#spine_horizontal_insertion_angle_on_petiole"])->getValue(pos_ajusted_to_100);
					//double vert_angle_spine_std_dev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_insertion_on_petiole#spine_vertical_insertion_angle_on_petiole_standard_deviation"])->getValue(pos_ajusted_to_100);
					//double hori_angle_spine_std_dev = (palmplugin_ptr->external_module_parameters_1D["spine_on_petiole#spine_insertion_on_petiole#spine_horizontal_insertion_angle_on_petiole_standard_deviation"])->getValue(pos_ajusted_to_100);




					////---------------------- PATCH --------------------------------------------

					////Compute vertical complementary angle for SPINE_RIGHT 
					//if(phyAge==SPINE_RIGHT) {
					//	vert_angle_spine_average = (360 - vert_angle_spine_average);
					//}


					////Compute horizontal angle
					////hori_angle_spine_average = hori_angle_spine_average - 90;
	    //            
	    //            
					////Compute horizontal complementary angle 
					////hori_angle_spine_average = 180 - hori_angle_spine_average;
	    //            
	    //           
					////hori_angle_spine_average = 90 - hori_angle_spine_average;

					////cout << "hori_angle_spine_average => " << hori_angle_spine_average << endl;

					////-------------------------------------------------------------------------




					//vert_angle_spine_average = degreeToRadian(vert_angle_spine_average);
					//hori_angle_spine_average = degreeToRadian(hori_angle_spine_average);
					//vert_angle_spine_std_dev = degreeToRadian(vert_angle_spine_std_dev);
					//hori_angle_spine_std_dev = degreeToRadian(hori_angle_spine_std_dev);

					//double vert_angle_spine = palmplugin_ptr->addStandardDeviation(vert_angle_spine_average,vert_angle_spine_std_dev,(b->getCurrentHierarc()->ordre+2)%NBCOMPTRAND);
					//double hori_angle_spine = palmplugin_ptr->addStandardDeviation(hori_angle_spine_average,hori_angle_spine_std_dev,(b->getCurrentHierarc()->ordre+2)%NBCOMPTRAND);

					//current_triedron = Matrix4::axisRotation(current_triedron.getNormalVector(),hori_angle_spine) * current_triedron; //Rotation autour de la direction X
					//current_triedron = Matrix4::axisRotation(current_triedron.getSecondaryVector(),vert_angle_spine) * current_triedron; //Rotation autour de la direction Y

				} else if (phyAge==FOLIOLE_RIGHT_TOP || phyAge==FOLIOLE_LEFT_TOP) { 

					//VERTICAL
					average = (palmplugin_ptr->external_module_parameters_1D["upper_leaflet_on_rachis#leaflet_insertion_angle#leaflet_vertical_insertion_angle_on_rachis"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["upper_leaflet_on_rachis#leaflet_insertion_angle#leaflet_vertical_insertion_angle_on_rachis_standard_deviation"])->getValue(pos_ajusted_to_100);

					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					//Patch left rachis
					if (phyAge == FOLIOLE_LEFT_TOP) {
						average = - average;
					}

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),angle) * current_triedron; //Rotation autour de la direction Y


					//HORIZONTAL
					average = (palmplugin_ptr->external_module_parameters_1D["upper_leaflet_on_rachis#leaflet_insertion_angle#leaflet_horizontal_insertion_angle_on_rachis"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["upper_leaflet_on_rachis#leaflet_insertion_angle#leaflet_horizontal_insertion_angle_on_rachis_standard_deviation"])->getValue(pos_ajusted_to_100);
					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getNormalVector(),angle) * current_triedron; //Rotation autour de la direction X
					if (phyAge == FOLIOLE_LEFT_TOP) {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(-90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}
					else {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}

	                
				} else if (phyAge==FOLIOLE_RIGHT_MIDDLE || phyAge==FOLIOLE_LEFT_MIDDLE) {

					//VERTICAL
					average = (palmplugin_ptr->external_module_parameters_1D["middle_leaflet_on_rachis#leaflet_insertion_angle#leaflet_vertical_insertion_angle_on_rachis"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["middle_leaflet_on_rachis#leaflet_insertion_angle#leaflet_vertical_insertion_angle_on_rachis_standard_deviation"])->getValue(pos_ajusted_to_100);

					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					//Patch left rachis
					if (phyAge == FOLIOLE_LEFT_MIDDLE) {
						average = - average;
					}

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),angle) * current_triedron; //Rotation autour de la direction Y




					//HORIZONTAL
					average = (palmplugin_ptr->external_module_parameters_1D["middle_leaflet_on_rachis#leaflet_insertion_angle#leaflet_horizontal_insertion_angle_on_rachis"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["middle_leaflet_on_rachis#leaflet_insertion_angle#leaflet_horizontal_insertion_angle_on_rachis_standard_deviation"])->getValue(pos_ajusted_to_100);

					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getNormalVector(),angle) * current_triedron; //Rotation autour de la direction X

					if (phyAge == FOLIOLE_LEFT_MIDDLE) {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(-90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}
					else {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}


				} else if (phyAge==FOLIOLE_RIGHT_BOTTOM || phyAge==FOLIOLE_LEFT_BOTTOM) {

					//VERTICAL
					average = (palmplugin_ptr->external_module_parameters_1D["lower_leaflet_on_rachis#leaflet_insertion_angle#leaflet_vertical_insertion_angle_on_rachis"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["lower_leaflet_on_rachis#leaflet_insertion_angle#leaflet_vertical_insertion_angle_on_rachis_standard_deviation"])->getValue(pos_ajusted_to_100);

					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					//Patch left rachis
					if (phyAge == FOLIOLE_LEFT_BOTTOM) {
						average = - average;
					}

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),angle) * current_triedron; //Rotation autour de la direction Y




					//HORIZONTAL
					average = (palmplugin_ptr->external_module_parameters_1D["lower_leaflet_on_rachis#leaflet_insertion_angle#leaflet_horizontal_insertion_angle_on_rachis"])->getValue(pos_ajusted_to_100);
					std_dev = (palmplugin_ptr->external_module_parameters_1D["lower_leaflet_on_rachis#leaflet_insertion_angle#leaflet_horizontal_insertion_angle_on_rachis_standard_deviation"])->getValue(pos_ajusted_to_100);

					average = degreeToRadian(average);
					std_dev = degreeToRadian(std_dev);

					angle = palmplugin_ptr->addStandardDeviation(average,std_dev,compt);
					current_triedron = Matrix4::axisRotation(current_triedron.getNormalVector(),angle) * current_triedron; //Rotation autour de la direction X
					if (phyAge == FOLIOLE_LEFT_BOTTOM) {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(-90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}
					else {
						current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),degreeToRadian(90)) * current_triedron; //Rotation autour de la direction Y pour remettre le symbole avec une normale positive
					}
				}

				current_triedron = Matrix4::translation(translation) * current_triedron;
	            
				p->set(gec, current_triedron);




				//Manage counter
				int last_idx = t->fullPalmElemList.size() - 1;  //Retrieve last index in the vector
				if ( position_plugin_counter < last_idx ) { //Is there still one item to process
					position_plugin_counter++;
				} else {
					//REINIT for the right rachis
	                
					position_plugin_counter=0;
				}



			}
		}
	}
	
}

/////////////////////////////////////////////////////
//
// That methods get called at the beginning of a geometrical branc
// (for each bearer vector)
//
// 
/////////////////////////////////////////////////////
void GeomBeginBrancPlugin::on_notify (const state& st, paramMsg *data) {

    paramGeomBrancCone * p = (paramGeomBrancCone*)data; //Cast msg parameters

    BrancAMAP * b = (BrancAMAP *)p->gbc->getBranc(); 

    int phyAge = (int)b->phyAgeInit;
    
    if (phyAge==RACHIS_RIGHT) {

        geombeginbrancplugin_counter = 0;
        geombeginbrancplugin_lateral_internode_deviation=0;
        geombeginbrancplugin_internode_rotation_angle=0;


    } else if (phyAge==RACHIS_LEFT) {

        geombeginbrancplugin_counter = 0;  
        geombeginbrancplugin_lateral_internode_deviation=0;
        geombeginbrancplugin_internode_rotation_angle=0;

		if (b->getAdditionalData("rachis_geometry") != 0)
			b->removeAdditionalData("rachis_geometry");
        b->addAdditionalData ("rachis_geometry", p->gbc);

        rachisGauche = b;

    } else  if (phyAge==SPINE_RIGHT_TOP || phyAge==SPINE_RIGHT_MIDDLE || phyAge==SPINE_RIGHT_BOTTOM || 
		        phyAge==FOLIOLE_RIGHT_TOP || phyAge==FOLIOLE_RIGHT_MIDDLE || phyAge==FOLIOLE_RIGHT_BOTTOM) {
        
        if(rachisGauche != NULL) {

            //Other way to retrieve LEFT RACHIS
            //because there are a verticille of 3 elements (right rachis, left rachis, chicot)
            //3*b->getCurrentHierarc()->indexOnBearerAtLowestLevel+1;
            //cout << "val 1 => " << b->getBearer()->getCurrentHierarc()->indexOnBearerAtLowestLevel << endl;


            //Retrieve RIGHT foliole bundle informations
            BundleTab *t = (BundleTab*)b->getBearer()->getAdditionalData("bundle_tab");

            //Retrieve LEFT rachis (GeomBrancAMAP). Needed, because right foliole are beared by left rachis !!
            GeomBrancAMAP * geomBrancLeft = (GeomBrancAMAP *) rachisGauche->getAdditionalData("rachis_geometry");

            //Set LEFT rachis as the bearer of the RIGHT palm items
            p->gbc->setGeomBrancBearer(geomBrancLeft); 

            //Retrieve the place where to put the foliole / spine
            GeomElem transformation = geomBrancLeft->getTranslationForCurvilinearDistance(t->fullPalmElemList[geombeginbrancplugin_counter].position);
//			GeomElem bearerTransformation = geomBrancLeft->getTranslationForCurvilinearDistance(t->fullPalmElemList[geombeginbrancplugin_counter].position+t->fullPalmElemList[geombeginbrancplugin_counter].lenght);

            //Set the place where to put the palm item (on the LEFT rachis)
            p->gbc->setPosOnBearer(transformation.getPosInGeomBranc()); 
//            p->gbc->setPosOnBearer(bearerTransformation.getPosInGeomBranc()); 
			// reset position on bearer
			geomBrancLeft->setCurrentTransform (transformation.getMatrix().getData());
//			geomBrancLeft->setCurrentTransform (bearerTransformation.getMatrix().getData());


            //Used later in positionPlugin
            if (p->gbc->getAdditionalData("translation"))
				p->gbc->removeAdditionalData("translation");
            p->gbc->addAdditionalData("translation", new Matrix4(transformation.getMatrix().getData()));
//            p->gbc->addAdditionalData("translation", new Matrix4(bearerTransformation.getMatrix().getData()));
        }

        geombeginbrancplugin_counter++;
    
    } else if (phyAge==SPINE_LEFT_TOP || phyAge==SPINE_LEFT_MIDDLE || phyAge==SPINE_LEFT_BOTTOM || 
		       phyAge==FOLIOLE_LEFT_TOP || phyAge==FOLIOLE_LEFT_MIDDLE || phyAge==FOLIOLE_LEFT_BOTTOM) {

        //TORSION DU RACHIS GAUCHE \C0 CHAQUE INSERTION DE FOLIOLE!!!
        if(rachisGauche != NULL) {

            //Retrieve LEFT foliole bundle informations
            BundleTab * t = (BundleTab*)b->getBearer()->getAdditionalData("bundle_tab");

            //Retrieve left rachis
            GeomBrancAMAP * geomBrancLeft = (GeomBrancAMAP *)rachisGauche->getAdditionalData("rachis_geometry");            

            //Retrieve current internode
            GeomElem * current_geomElem = geomBrancLeft->getElementsGeo().at(geombeginbrancplugin_counter);

            //Retrieve internode transformation Matrix
            Matrix4 & current_triedron = current_geomElem->getMatrix();

            //Retrieve vector from matrix
            Vector3 translation = current_triedron.getTranslation();



            //Move the internode in the opposit direction ???
            current_triedron = Matrix4::translation(-translation) * current_triedron;


            //Normalize position_on_palm
            double pos_ajusted_to_100 = t->fullpalmPositionNORMALIZE(geombeginbrancplugin_counter);
                

            //Deviation on tourne autour de l'axe vertical ----------------------
            double lateral_deviation_angle = (palmplugin_ptr->external_module_parameters_1D["frond_nervure#frond_nervure_lateral_deviation#frond_nervure_horizontal_lateral_deviation"])->getValue(pos_ajusted_to_100);
            lateral_deviation_angle = degreeToRadian(lateral_deviation_angle);


            //Left or right direction decision
            if (geombeginbrancplugin_counter==0) {

                //Compute direction
                //double random = palmplugin_ptr->plant_ptr->getConfigData().getRandomGenerator()->Random(); //[0-1]
                //random = random - 0.5; //[-0.5,0.5]


                //if(random<0) {
                //    //Compute random ratio
                //    double ran = palmplugin_ptr->plant_ptr->getConfigData().getRandomGenerator()->Random()*0.1; //[0-1]
                //    //random_and_direction_for_lateral_deviation = ran - 1.5; //[-1.5,-0.5]
                //    random_and_direction_for_lateral_deviation = -1. + ran; //[-1.5,-0.5]
                //} else {

                    //Compute random ratio
                    double ran = palmplugin_ptr->plant_ptr->getConfigData().getRandomGenerator()->Random()+0.5; //[0-1]
                     random_and_direction_for_lateral_deviation = ran; //[0,1]
                //}
            }


			if (phyAge==FOLIOLE_LEFT_TOP || phyAge==FOLIOLE_LEFT_MIDDLE || phyAge==FOLIOLE_LEFT_BOTTOM)
				geombeginbrancplugin_lateral_internode_deviation += random_and_direction_for_lateral_deviation * lateral_deviation_angle; //static
			else
				geombeginbrancplugin_lateral_internode_deviation = random_and_direction_for_lateral_deviation * lateral_deviation_angle; //static

            current_triedron = Matrix4::axisRotation(Vector3(0,0,1),geombeginbrancplugin_lateral_internode_deviation) * current_triedron;
            //-------------------------------------------------------------------



            //Rotation autour de la direction principale (spine and leaflets PHYLLOTAXY. Warning: instead of make the frond_nervure rotating, we simulate virtualy it with spine and leaflets rotation)
            double rotation_angle = (palmplugin_ptr->external_module_parameters_1D["frond_nervure#nervure_rotation_angle#rotation_of_leaflets_planes_on_nervures"])->getValue(pos_ajusted_to_100);
            rotation_angle = -1 * rotation_angle;
            rotation_angle = degreeToRadian(rotation_angle);


            //Left or right direction decision
            if (geombeginbrancplugin_counter==0) {

                /*
                //Retrieve direction from lateral angle !!! 
                if(random_and_direction_for_lateral_deviation<=0) { //Rotation and lateral_deviation angle are inversed
                    //Negative rotation

                    //Compute random ratio
                    double ran = palmplugin_ptr->plant_ptr->getConfigData().getRandomGenerator()->Random(); //[0-1]
                    random_and_direction_for_rotation = ran - 1.5; //[-1.5,-0.5]
                } else {
                    //Positive rotation

                    //Compute random ratio
                    double ran = palmplugin_ptr->plant_ptr->getConfigData().getRandomGenerator()->Random(); //[0-1]
                    random_and_direction_for_rotation = ran + 0.5; //[0.5,1.5]
                }
                */

                //Compute random ratio
                /*
                double ran2 = palmplugin_ptr->plant_ptr->getConfigData().getRandomGenerator()->Random(); //[0-1]
                //ran2 = ran2 + 0.5; //[0.5,1.5]
                random_for_rotation = ran2;
                */
            }


			if (phyAge==FOLIOLE_LEFT_TOP || phyAge==FOLIOLE_LEFT_MIDDLE || phyAge==FOLIOLE_LEFT_BOTTOM)
            //geombeginbrancplugin_internode_rotation_angle = geombeginbrancplugin_internode_rotation_angle + (random_and_direction_for_rotation * rotation_angle); //static
				geombeginbrancplugin_internode_rotation_angle += random_and_direction_for_lateral_deviation * rotation_angle; //static
			else
				geombeginbrancplugin_internode_rotation_angle = random_and_direction_for_lateral_deviation * rotation_angle; //static


            current_triedron = Matrix4::axisRotation(current_triedron.getMainVector(),geombeginbrancplugin_internode_rotation_angle) * current_triedron;

            //cout << "random_and_direction_for_lateral_deviation => " << random_and_direction_for_lateral_deviation << ", random_and_direction_for_rotation" << random_and_direction_for_rotation << endl;
            //-------------------------------------------------------------------


            //
            current_triedron = Matrix4::translation(translation) * current_triedron;



            //Manage counter
            int last_idx = t->fullPalmElemList.size() - 1;  //Retrieve last index in the vector
            if ( geombeginbrancplugin_counter < last_idx ) { //Is there still one item to process
                //Next palm item
                
                geombeginbrancplugin_counter++;
            } else {
                //REINIT 
                
                geombeginbrancplugin_counter=0;
                geombeginbrancplugin_lateral_internode_deviation=0;
                geombeginbrancplugin_internode_rotation_angle=0;
            }
        }
    } 
}



/////////////////////////////////////////////////
// 
// That methods get called at the end of the geometry computing
// 
/////////////////////////////////////////////////
void GeometryEndPlugin::on_notify (const state& st, paramMsg * data) {
    
    //Cast msg parameters
    paramGeomCompute * p = (paramGeomCompute*)data;
    GeomBrancAMAP * geomRachisLeft = NULL;
    //Retrieve Geometry manager
    GeomManager * geomManager = p->gm;
    std::vector<GeomBranc *> & vecGeomBranc = geomManager->getGeomBrancStack();
    std::vector<GeomBranc *>::iterator elem_iterator = vecGeomBranc.begin();
    for ( ; elem_iterator != vecGeomBranc.end(); ++elem_iterator) {
        GeomBrancAMAP * geomBranc = static_cast<GeomBrancAMAP *>((*elem_iterator));     
        int phyAge=static_cast<BrancAMAP *>(geomBranc->getBranc())->getCurrentGu()->phyAgeGu;


        
        //On doit passer dans le rachis gauche d'abord !!!
        if(phyAge == RACHIS_LEFT) {
            geomRachisLeft = geomBranc;
        }

        if(phyAge == RACHIS_RIGHT) {

            if (geomRachisLeft != NULL) {
                //BrancAMAP * correspondingRachisLeft = static_cast<BrancAMAP *>(geomBranc->getAxe()->getBearer()->getBorne(0));
                std::vector<GeomElem *> & vecGeomElem = geomBranc->getElementsGeo();
                std::vector<GeomElem *>::iterator geomElem_iterator = vecGeomElem.begin();          
                for ( ; geomElem_iterator != vecGeomElem.end(); ++geomElem_iterator) {
                    GeomElem * currentGeomElem = (*geomElem_iterator);
                    vector<GeomBranc *> & childrenGeomElem = currentGeomElem->getGeomChildren();                    
                    vector<GeomBranc *> & childrenGeomElemBearer = geomRachisLeft->getElementsGeo().at(0)->getGeomChildren();
                    childrenGeomElemBearer.insert(childrenGeomElemBearer.end(), childrenGeomElem.begin(), childrenGeomElem.end());
                    
                }

                geomBranc->getElementsGeo().clear();
                geomRachisLeft = NULL;
            }
		}

		std::vector<GeomElem *> & vecGeomElem = geomBranc->getElementsGeo();
        std::vector<GeomElem *>::iterator geomElem_iterator = vecGeomElem.begin(); 
		if(vecGeomElem.size()>1) {
			 GeomElemCone * prevGeomElem = static_cast<GeomElemCone *>(*geomElem_iterator);
			++geomElem_iterator;
			for ( ; geomElem_iterator != vecGeomElem.end(); ++geomElem_iterator) {
				GeomElemCone * currentGeomElem = static_cast<GeomElemCone *>(*geomElem_iterator);
				//for all elmements do geometrical interpolation		
				prevGeomElem->setTopDiam(currentGeomElem->getBottomDiam());
				prevGeomElem = currentGeomElem;
			}
		}
	}

}


void GeometryBeginPlugin::on_notify (const state& st, paramMsg * data) 
{
	pp->reset();
}
















//----------------------- PALM PLUGIN CLASS -------------------------//



/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
double PalmPlugin::getSCALARParameter(string paramName) {
    string val = external_module_parameters[paramName];
    return atof(val.c_str());
}








/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
PalmPlugin::PalmPlugin(const std::string& f, void *pp) {

	plant_ptr = (Plant *)pp;
   //Retrieve plugin parameters from XML file
    external_module_parameters = getScalarParametersFromXMLFile(f);
    external_module_parameters_1D = get1DParametersFromXMLFile(f);
    //child -> parent binding PTR
	length = new LengthPlugin (pp);
    length->palmplugin_ptr = this;
	diam = new DiameterPlugin (pp);
    diam->palmplugin_ptr = this;
	d = new DecompPlugin (pp);
    d->palmplugin_ptr = this;
	parameter = new ParameterPlugin (pp);
    parameter->palmplugin_ptr = this;
	geomBeginBrancPlug = new GeomBeginBrancPlugin (pp);
    geomBeginBrancPlug->palmplugin_ptr = this;
	position = new PositionPlugin (pp);
    position->palmplugin_ptr = this; 
    
	geomBeginPlug = new GeometryBeginPlugin (pp);
    geomBeginPlug->pp = this;

	geomEndPlug = new GeometryEndPlugin (pp);
}

void PalmPlugin::reset()
{
	geomBeginBrancPlug->rachisGauche = 0;
	position->position_plugin_counter = 0;
}

/////////////////////////////////////////////////////
//
// Compute rachis length
//
/////////////////////////////////////////////////////
float PalmPlugin::addStandardDeviation(double average, double stdDeviation) {
    float randNormal = loi_norm(plant_ptr->getConfigData().getRandomGenerator());
//	float randNormal = plant_ptr->getConfigData().getRandomGenerator()->Random();

    return average + stdDeviation*randNormal;
}

float PalmPlugin::addStandardDeviation(double average, double stdDeviation, int compt) {

	int savCompt = plant_ptr->getConfigData().getRandomGenerator()->getCurrentIdx();

	if (compt != savCompt)
		plant_ptr->getConfigData().getRandomGenerator()->setState(compt);

    float val = addStandardDeviation(average, stdDeviation);

	if (compt != savCompt)
		plant_ptr->getConfigData().getRandomGenerator()->setState(savCompt);

	return val;

}




















//----------------------- BUNDLE TAB CLASS ----------------------//

void BundleTab::addRachisElem(int rang,double position,int folioleType, double lenght) {

    //cout << "Foliole type = " << folioleType << endl;

    PalmElem * palmElem = new PalmElem();
    palmElem->rang=rang;
    palmElem->position=position;
    palmElem->lenght=lenght;
    palmElem->folioleType=folioleType;
    fullPalmElemList.push_back(*palmElem);
}

void BundleTab::toString() {

    cout << "fullpalm_length = " << fullpalm_length << ",rachis_internode_number = " << rachis_internode_number << ", petiole_internode_number = " << petiole_internode_number << ", fullpalm_internode_number = " << fullpalm_internode_number << endl;
}


/**
 *
 * Transform position relative to full_palm to position relative to petiole
 *
 */
int BundleTab::getRelativePositionOnPetiole(int fullpalm_pos) {

    int petiole_pos = fullpalm_pos; //petiole_pos is the same as fullpalm_pos, because the petiole is at the beginning of the palm (beginning index is the same)

    return petiole_pos; 
}

/**
 *
 * Transform position relative to full_palm to position relative to rachis
 *
 */
int BundleTab::getRelativePositionOnRachis(int fullpalm_pos) {

    int rachis_pos = fullpalm_pos - petiole_internode_number; 

    return rachis_pos; 
}


/**
 *
 * Normalize rachis pos (transform position relative to rachis length to position relative to 100)
 *
 */
double BundleTab::rachisPositionNORMALIZE(int rachis_pos) {

    //double pos_ajusted_to_100 = ((fullPalmElemList[rachis_pos].position-petiole_length)*100)/rachis_length;

    //return pos_ajusted_to_100; 
	return positionNORMALIZE (fullPalmElemList[rachis_pos].position-petiole_length, rachis_length);
}


/**
 *
 * Normalize petiole pos (transform position relative to petiole length to position relative to 100)
 *
 */
double BundleTab::petiolePositionNORMALIZE(int petiole_pos) {

    //double pos_ajusted_to_100 = (fullPalmElemList[petiole_pos].position*100)/petiole_length;

    //return pos_ajusted_to_100; 
	return positionNORMALIZE (fullPalmElemList[petiole_pos].position, petiole_length);
}



/**
 *
 * Normalize petiole pos (transform position relative to petiole length to position relative to 100)
 *
 */
double BundleTab::positionNORMALIZE(double pos, double length) {

    double pos_ajusted_to_100 = (pos*100)/length;

    return pos_ajusted_to_100; 
}



/**
 *
 * Normalize fullpalm pos (transform position relative to palm length to position relative to 100)
 *
 */
double BundleTab::fullpalmPositionNORMALIZE(int fullpalm_pos) {
    double pos_ajusted_to_100 = (fullPalmElemList[fullpalm_pos].position*100)/fullpalm_length;
    
    return pos_ajusted_to_100; 
}









//------------------------------








