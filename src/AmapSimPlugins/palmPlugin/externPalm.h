#ifndef __EXPORTPALM
#define __EXPORTPALM


#pragma warning ( disable : 4251 )

#if !defined STATIC_LIB
	#if defined WIN32

			#ifdef PALM_EXPORTS
				#define PALM_EXPORT __declspec(dllexport)
			#else
				#define PALM_EXPORT __declspec(dllimport)
			#endif

	#else
		#define PALM_EXPORT
	#endif

#else
		#define PALM_EXPORT
#endif



#endif
