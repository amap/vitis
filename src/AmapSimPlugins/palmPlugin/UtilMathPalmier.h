#ifndef __util_math_palmier_h__
#define __util_math_palmier_h__

#include "UtilMath.h"


#define FCONV (float)0.0174533 //radian equivalent to 1 degree


/**
 *
 *
 *
 */
double radianToDegree(double rad) {

    double deg = rad / FCONV;

    return deg;
}

/**
 *
 *
 *
 */
double degreeToRadian(double deg) {

    double rad = deg*FCONV;

    return rad;
}



/*
   +-----------------------------------------------------------------------+
   |                       l o i _ n o r m ( )                             |
   +-----------------------------------------------------------------------+
   |  RES:     float    variable aleatoire normale reduite                 |
   |           (algorithme dans Proba. de l'Ingenieur, N.Bouleau p.371)    |
   +-----------------------------------------------------------------------+
*/
//randomX and randomY are 2 random values [0,1]
float loi_norm(TRandomMersenne * randMersenne) {
	float       x, y, z, val;
	float standard_deviation_max = 3;
	val=standard_deviation_max+1;
	
	while (fabs(val)>standard_deviation_max) {
		do {
			x = randMersenne->Random() * 2. - 1.;
			y = randMersenne->Random() * 2. - 1.;
			z = (x*x) + (y*y);
		} while (z > 1.);
		
		val =(sqrt(-2.*log(z)/z) * x);
	}
	
	return val;
}

  


#endif
