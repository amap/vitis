#include <map>
#include <iostream>
#include <vector>

#ifndef __XMLIO
#define __XMLIO
using namespace std;

/////////////////////////////////////////////////////
//
// 
//
/////////////////////////////////////////////////////
struct Checkpoint {
    Checkpoint () {};
    Checkpoint (string serialized_checkpoint);
    Checkpoint (double pos,double val);
	double position;
	double value;
};

/////////////////////////////////////////////////////
//
// 
//
/////////////////////////////////////////////////////
struct CheckpointS {
    CheckpointS (string serialized_checkpoints);
    CheckpointS (double pos1,double val1,double pos2,double val2);
	vector<Checkpoint> listCheckpoint;
    double getValue(double pos);
};



extern map<string, string> getScalarParametersFromXMLFile(const std::string & f);
extern map<string, CheckpointS *> get1DParametersFromXMLFile(const std::string & f);

extern vector<string> split(string s, string sep);



#endif
