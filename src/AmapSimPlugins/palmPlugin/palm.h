#include <iostream>
using namespace std;

#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"

#include "xmlIO.h"
#include <map>

#define STIPE_START 1 
#define STIPE_END 999

#define RACHIS_LEFT 1001
#define RACHIS_RIGHT 2001

#define ARROW 2007

#define STUMP 1000

#define FOLIOLE_LEFT_TOP 3101 //Default 
#define FOLIOLE_LEFT_MIDDLE 3111
#define FOLIOLE_LEFT_BOTTOM 3121
#define SPINE_LEFT_TOP 3131
#define SPINE_LEFT_MIDDLE 3141
#define SPINE_LEFT_BOTTOM 3151

#define FOLIOLE_RIGHT_TOP 3201 //Default
#define FOLIOLE_RIGHT_MIDDLE 3211
#define FOLIOLE_RIGHT_BOTTOM 3221
#define SPINE_RIGHT_TOP 3231
#define SPINE_RIGHT_MIDDLE 3241
#define SPINE_RIGHT_BOTTOM 3251

#define BULBE_BEGIN_POS 0 //Where the bulb start in term of stipe entn
#define BULBE_END_POS 5	  //Where the bulb stop in term of stipe entn
#define BULBE_BEGIN_DIAMETER 5
#define BULBE_END_DIAMETER 1 

#define INITIAL_RACHIS_DIAMETER 1

#define LP_ARROW 4
#define LP_STUMP 5
#define LP_LEFT_RACHIS 0
#define LP_RIGHT_RACHIS 1
#define LP_LEFT_LEAFLET 2
#define LP_RIGHT_LEAFLET 3

struct PalmPlugin;




/////////////////////////////////////////////////////
//
// 
//
/////////////////////////////////////////////////////
struct PalmElem {
	int rang;        //Rank
	double position; //Length from the beginning of the palm
	double lenght;   //Internode length
	int folioleType; //2,3,4 is spine, {0,1,-1} are foliole
};






/////////////////////////////////////////////////////
//
// Contains informations concerning "foliole" bundle
//
/////////////////////////////////////////////////////
struct BundleTab {
	vector<PalmElem> fullPalmElemList;
	void addRachisElem(int rang,double position,int typeFoliole, double lenght);

	double fullpalm_length;
	double petiole_length;
    double rachis_length;
    double rachis_initial_diameter;

    int rachis_internode_number;
    int petiole_internode_number;
    int fullpalm_internode_number;

    void toString();
    int getRelativePositionOnPetiole(int fullpalm_pos);
    int getRelativePositionOnRachis(int fullpalm_pos);
    double rachisPositionNORMALIZE(int rachis_pos);
    double petiolePositionNORMALIZE(int petiole_pos);
    double fullpalmPositionNORMALIZE(int fullpalm_pos);
	double positionNORMALIZE(double pos, double length);
};

struct GeomBeginBrancPlugin: public subscriber<messageVitisBeginGeomBrancCone> {	
    int geombeginbrancplugin_counter; //Store current internode being processed
    double geombeginbrancplugin_lateral_internode_deviation; //Store current internode lateral deviation angle
    double geombeginbrancplugin_internode_rotation_angle; //Store current internode rotation angle

    double random_and_direction_for_lateral_deviation; //Store lateral_deviation add-on stuff
    double random_and_direction_for_rotation; //Store rotation add-on stuff
    double random_for_rotation; //Store rotation add-on stuff

	PalmPlugin * palmplugin_ptr;
    	BrancAMAP * rachisGauche;
	GeomBeginBrancPlugin (void *p) { subscribe(p); rachisGauche = NULL;};
	void reset () {rachisGauche = NULL;};
	virtual ~GeomBeginBrancPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL); 	
};

//struct	GeomEndBrancPlugin: public subscriber<messageVitisEndGeomBrancCone>
//{	
//	GeomEndBrancPlugin () { subscribe();};
//	virtual ~GeomEndBrancPlugin () {unsubscribe();};
//	void on_notify (const state& st, paramMsg * data=NULL); 	
//
//};

struct PositionPlugin: public subscriber<messageVitisElemPosition> {
    int position_plugin_counter; //Store current internode being processed
	PalmPlugin * palmplugin_ptr;

	PositionPlugin (void *p) { position_plugin_counter=0; subscribe(p);};
	virtual ~PositionPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL); 	
//	double computePetioleAngleFlexion(double initial_a, double final_a, double variation_time, int pos);
	double computePetioleAngleFlexion(double initial_a, double final_a, double deb_time, double end_time, int pos);
};

struct GeometryEndPlugin: public subscriber<messageVitisEndGeomCompute> {
	GeometryEndPlugin (void *p) { subscribe(p);};
	virtual ~GeometryEndPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);

};

struct GeometryBeginPlugin: public subscriber<messageVitisBeginGeomCompute> {
	GeometryBeginPlugin (void *p) { subscribe(p);};
	virtual ~GeometryBeginPlugin () {unsubscribe();};

	PalmPlugin *pp;
	void on_notify (const state& st, paramMsg * data=NULL); 
};

struct LengthPlugin:public subscriber<messageVitisElemLength> {
	PalmPlugin * palmplugin_ptr;
    double static_spine_length;
    double static_leaflet_length;

	LengthPlugin (void *p) { subscribe(p);};
	virtual ~LengthPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL); 
};


struct DiameterPlugin:public subscriber<messageVitisElemDiameter> {
	PalmPlugin * palmplugin_ptr;
    double static_spine_diameter;
    double static_leaflet_width;

	DiameterPlugin (void *p) { subscribe(p);};
	virtual ~DiameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL); 
};

struct  DecompPlugin:public subscriber<messageVitisDecompAxeLevel> {
	PalmPlugin * palmplugin_ptr;

	double palm_length; //Compute by the left one and used by the right one
    double petiole_length;

	DecompPlugin (void *p) {subscribe(p);};
	virtual ~DecompPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
	BundleTab * computeBundle (float fullpalm_length,double petiole_length, BrancAMAP *br);
    int getGroupNumber(double g1, double g2, double g3, double g4);
};

struct ParameterPlugin:public subscriber<messageAxeRef> {
	PalmPlugin * palmplugin_ptr;
	ParameterPlugin (void *p) {subscribe(p);};
	virtual ~ParameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
	bool isPetiole(BrancAMAP * b,int internodeNumber);
//    double computeYoungModule(double final_ym, double initial_ym, double std_dev, double variation_time, double rythme, int pos);
    double computeYoungModule(double initial_ym, double final_ym, double std_dev, double deb_time, double end_time, double rythme, int pos);
};

struct PalmPlugin {
	PalmPlugin(const std::string& f, void *p);
	virtual ~PalmPlugin(){;};
	void reset();
	DecompPlugin *d;
	DiameterPlugin *diam;
	LengthPlugin *length;
	PositionPlugin *position;
	ParameterPlugin *parameter;
	GeomBeginBrancPlugin *geomBeginBrancPlug;
	//GeomEndBrancPlugin geomEndBrancPlug;
	//GeomElemPlugin geomElemPlug;
	GeometryBeginPlugin *geomBeginPlug;
	GeometryEndPlugin *geomEndPlug;

	double getSCALARParameter(string paramName);

	float addStandardDeviation(double average, double stdDeviation);
	float addStandardDeviation(double average, double stdDeviation, int compt);
	
	Plant * plant_ptr; //Used for mersenne

	map<string, string> external_module_parameters; //contains scalar parameters
	map<string, CheckpointS *> external_module_parameters_1D; //contains 1D parameters
};
extern "C" PALM_EXPORT  PalmPlugin * StartPlugin (const std::string& f, void *p)
{
	std::cout << "loading PalmPlugin module with parameter " << f << std::endl;

	return new PalmPlugin (f, p) ;
}
