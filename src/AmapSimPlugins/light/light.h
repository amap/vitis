/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <iostream>
using namespace std;

#include "Notifier.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "ParamFile.h"
#include "externLight.h"


#include <vector>

class lightPlugin;



#include "defAMAP.h"

class LightEventData: public EventData
{
public:
	LightEventData (int action){this->action = action;};
	int Action (){return action;};
	void setAction (int a){action = a;};
private:
	int action; // 1:compute geometry; 2:read light
};

class LightData : public PrintableObject
{
public :
	double v;
	std::string to_string ()
	{
		std::ostringstream oss;
		// �crire la valeur dans le flux
		oss << v ;
		// renvoyer une string
		return oss.str();
	};
};


struct LIGHT_EXPORT ParameterPlugin:public subscriber<messageAxeRef> {
	lightPlugin * lightplugin_ptr;

	ParameterPlugin (Plant *p) {subscribe(p);};
	virtual ~ParameterPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct LIGHT_EXPORT DecompPlugin:public subscriber<messageVitisDecompAxeLevel> {
	lightPlugin * lightplugin_ptr;

	DecompPlugin (Plant *p) {subscribe(p);};
	virtual ~DecompPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct LIGHT_EXPORT PlantPlugin:public subscriber<messageVitisPlant> {
	lightPlugin * lightplugin_ptr;

	PlantPlugin (void *p) {subscribe(p);};
	virtual ~PlantPlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

struct LIGHT_EXPORT PlantDeletePlugin:public subscriber<messageVitisPlantDelete> {
	lightPlugin * lightplugin_ptr;

	PlantDeletePlugin (void *p) {subscribe(p);};
	virtual ~PlantDeletePlugin () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL);
};

enum messageLightParameter {SigGetParameter};

// specialized signal class for light
class LIGHT_EXPORT lightParameterSignal:public ParameterSignal
{
public:
	float sigGetParameter (float & vv, Parameter *pp, VitisObject *oo);
};


class LIGHT_EXPORT lightPlugin : public VProcess
{
public :
	lightPlugin (const std::string& f, const void *p);
	virtual ~lightPlugin(){;};
	void setPlant (Plant  *p) {plant = (PlantAMAP*)p;};
	Plant *getPlant () {return plant;};
	void process_event(EventData *msg);
	void setParameterPlugin (ParameterPlugin *p){l = p;};
	void setDecompPlugin (DecompPlugin *p){d = p;};

	float livingThreshold;
	float ramificationThreshold;
	float cat1Attenuation;
	float cat2Attenuation;
	int cat1Threshold;
	int cat2Threshold;
	int lightningModel;

	// MIR parameters
	float latitude;
	float clearnessIndex;
	int beginingDay, endingDay;
	float refLight;
protected :
    bool initLightParam (const std::string& f);
	void computeInterception (float top_extern);
	void computeInterceptionModelIsolated (float top_extern);
	void computeInterceptionModelCanopy (float top_extern);
	void computeInterceptionModelMIR (float top_extern);
	void computeUpperPoint(Vector3 *v);
	float computeCenterPoint (Vector3 *v);
	void affectLight (int id, float light);
    BrancAMAP* findBranc(int id);
	float computeRefLight();
	float extraTerrestrialDaily (int doy);
	double SunSetHourAngle (float declination);

	void updateInterceptedLight (const std::string& f);

 	void writeOPF(const std::string &finame);

	PlantAMAP *plant;
	BrancAMAP *curBranc;

	ParameterPlugin *l;
	DecompPlugin *d;
	PlantPlugin *p;
	PlantDeletePlugin *pp;
};

extern "C" LIGHT_EXPORT  lightPlugin * StartPlugin (const std::string& f, const void *p)
{
	std::cout << "loading AmapSim lightPlugin module with parameter " << f << std::endl;

	return new lightPlugin (f, p) ;
}
