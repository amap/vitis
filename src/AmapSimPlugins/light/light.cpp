/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "PlantAMAP.h"
#include "ObserverManager.h"

#ifdef WIN32
#include <conio.h>
#endif
#include <iostream>
#include <fstream>
#include <UtilMath.h>
using namespace std;

#include "Plant.h"
#include "PlantAMAP.h"
#include "Topology.h"
#include "Branc.h"
#include "DecompAxeLevel.h"
#include "VitisCoreSignalInterface.h"
#include "AmapSimModSignalInterface.h"
#include "corresp.h"
#include "externLight.h"
#include "light.h"
#define STATIC_LIB
#include "Output.h"
#include "GldsFormat.h"
#undef STATIC_LIB


void ParameterPlugin::on_notify (const state& st, paramMsg *data)
{
    //Cast message parameters
    paramAxisRef * p = (paramAxisRef *)data;

    //Retrieve branch
    BrancAMAP * b = (BrancAMAP *)p->b;

	LightData *light = (LightData*)b->getAdditionalData("light");
	if (!light)
		return;


	// entn test number
    if (   p->numVar == rtprefn1
        || p->numVar == rtneon1)
	{
		if (light->v == 1)
			return;

		float d1, d2, v1, v2, coef;
		int cat;
		if (b->phyAgeInit < lightplugin_ptr->cat1Threshold)
			cat = 1;
		else if ((b->phyAgeInit < lightplugin_ptr->cat2Threshold))
			cat = 2;
		else
			cat = 3;

		if (light->v < lightplugin_ptr->livingThreshold)
		{
			v1 = 0;
			v2 = 0;
			d2 = 1;
			d1 = 0;
		}else if (light->v < lightplugin_ptr->ramificationThreshold)
		{
			d1 = lightplugin_ptr->livingThreshold;
			d2 = lightplugin_ptr->ramificationThreshold;
			switch (cat)
			{
			case 1 :
				v1 = 0;
				v2 = lightplugin_ptr->cat1Attenuation;
				break;
			case 2 :
				v1 = 0;
				v2 = lightplugin_ptr->cat2Attenuation;
				break;
			case 3 :
				v1 = 0;
				v2 = 1;
				break;
			}
		}else
		{
			d1 = lightplugin_ptr->ramificationThreshold;
			d2 = 1;
			switch (cat)
			{
			case 1 :
				v1 = lightplugin_ptr->cat1Attenuation;
				v2 = 1;
				break;
			case 2 :
				v1 = lightplugin_ptr->cat2Attenuation;
				v2 = 1;
				break;
			case 3 :
				v1 = 1;
				v2 = 1;
				break;
			}
		}
		coef = v1 + (v2 - v1) * (1. - (d2-light->v)/(d2 - d1));
		p->var *= coef;
	}
	// initial branched first state proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpbinitret0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpbinitret0)
	{
		if (light->v < lightplugin_ptr->ramificationThreshold)
			p->var = 1;
	}
	// initial branched first state proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpbinitret1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpbinitret1)
	{
		if (light->v < lightplugin_ptr->ramificationThreshold)
			p->var = 0;
	}
	// remaining not branched proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_st0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_st0)
	{
		if (light->v < lightplugin_ptr->ramificationThreshold)
			p->var = 1;
	}
	// remaining first state proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_st1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_st1)
	{
		if (light->v < lightplugin_ptr->ramificationThreshold)
			p->var = 0;
	}
	// remaining second state proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_st2
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_st2)
	{
		if (light->v < lightplugin_ptr->ramificationThreshold)
			p->var = 0;
	}
	// transition not branched -> first state proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_0_1
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_0_1)
	{
		if (light->v < lightplugin_ptr->ramificationThreshold)
			p->var = 0;
	}
	// transition first state -> not branched proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_1_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_1_0)
	{
		if (light->v < lightplugin_ptr->ramificationThreshold)
			p->var = 1;
	}
	// transition second state -> not branched proba
	else if (   p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*0+NB_GLOBAL_LP_VAR+rcpb_2_0
		     || p->numVar == NB_VAR+(NB_GLOBAL_LP_VAR + 2*NB_ZONE_VAR)*2+NB_GLOBAL_LP_VAR+rcpb_2_0)
	{
		if (light->v < lightplugin_ptr->ramificationThreshold)
			p->var = 1;
	}
	// living proba
	else if (p->numVar == rtpbvie)
	{
		if (light->v < lightplugin_ptr->livingThreshold)
			p->var = 0;
	}
}

/////////////////////////////////////////////////////
//
// This method get called for each new topology creation
//
// It stores UT global parameters
//
/////////////////////////////////////////////////////
void DecompPlugin::on_notify (const state& s, paramMsg * data)
{
    //Cast msg parameters
    paramDecompAxeLevel *p = (paramDecompAxeLevel *)data;

    //Retrieve branch
    DecompAxeLevel *I = p->d;

	if (s == SigDelDecompAxeLevel)
	{
		LightData *d;
		if ((d = (LightData*)I->getAdditionalData("light")))
		{
			delete d;
			I->removeAdditionalData("light");
		}
		return;
	}

	//Swith on axe object level (0 stand for branch, etc..)
    if (I->getLevel() == 0)
	{
        //Retrieve initial phyAge of the branch (at birth of)
		BrancAMAP *b = (BrancAMAP*)I;

		int phyAge = b->phyAgeInit;

		if (phyAge >= 600)
			return;

		LightData *L = new LightData;
		L->v = 1;
		I->addAdditionalData ("light", L, VITISGLDSPRINTABLE);
	}

}

/////////////////////////////////////////////////////
//
//
// This methods get called  when the plant is created
//
/////////////////////////////////////////////////////
void PlantPlugin::on_notify (const state& , paramMsg * data)
{

    paramPlant * d = (paramPlant*)data; //Retrieve msg params

	lightplugin_ptr->setPlant (d->p);
	if (lightplugin_ptr->getPlant()->getConfigData().cfg_topoSimp > 1)
        lightplugin_ptr->getPlant()->getConfigData().cfg_topoSimp = 1;

	LightEventData *e = new LightEventData(1);
	Scheduler::instance()->create_process(lightplugin_ptr,e,1,103);
//	Scheduler::instance()->create_process(lightplugin_ptr,NULL,1,2);
//	Scheduler::instance()->create_process(lightplugin_ptr,(EventData*)d->p,Scheduler::instance().getHotStop(),1);
}

void PlantDeletePlugin::on_notify (const state& , paramMsg * data)
{
	lightplugin_ptr->setPlant (NULL);
}

void lightPlugin::process_event(EventData *msg)
{
    if (plant == NULL)
        return;

	LightEventData *e = (LightEventData*)msg;

	Scheduler *s = Scheduler::instance();

	/* sets the next external eventData */
	float top_extern = s->getTopClock();

	if (e->Action() == 1)
	{
		plant->computeGeometry();

		writeOPF (plant->getName());

		e->setAction (2);
		s->signal_event(this->getPid(),e,s->getTopClock(),101);
	}
	else if (e->Action() == 2)
	{
		cout << "compute interception a time "<< top_extern <<endl;
		/* computes light interception for the next cycle */
		computeInterception (top_extern);

		e->setAction (1);
		if (s->getTopClock()+1 <= s->getHotStop())
		{
			s->signal_event(this->getPid(),e,s->getTopClock()+1,103);
		}
	}
}

/////////////////////////////////////////////////////
//
//
//
/////////////////////////////////////////////////////
lightPlugin::lightPlugin(const std::string& f, const void *plant) {

    if (!initLightParam (f))
    {
		cout << "Failed reading light parameter file " << f << endl;
		cout << "File sould look like this :" << endl<<endl;
		cout << "# WARNING"<<endl;
		cout << "# The syntax for this parameter file is not even"<<endl<<"# Try to keep the \"SingleValueParameter\" keyword as it is"<<endl;
		cout << "# just adjust the numerical values"<<endl<<endl;
		cout << "# maximum phyage value for category one axis"<<endl<<"cat1Threshold SingleValueParameter 60"<<endl<<endl;;
		cout << "# maximum phyage value for category two axis"<<endl<<"cat2Threshold SingleValueParameter 80"<<endl<<endl;
		cout << "# minimum light for life"<<endl<<"livingThreshold SingleValueParameter 0.1"<<endl<<endl;
		cout << "# minimum light for ramification"<<endl<<"ramificationThreshold SingleValueParameter 0.4"<<endl<<endl;
		cout << "# category 1 axis sensibility for light at ramification threshold"<<endl<<"cat1Attenuation SingleValueParameter 0.7"<<endl<<endl;
		cout << "# category 2 axis sensibility for light at ramification threshold"<<endl<<"cat2Attenuation SingleValueParameter 0.9"<<endl<<endl;
		cout << "# lightning model type (0 : isolated Tree, 1 : canopy tree, 2 : MIR)"<<endl<<"lightningModel SingleValueParameter 1"<<endl;
 		cout << endl<< "**** Failed reading light parameter file " << f << " ****" << endl;

		return;
	}
	this->plant = (PlantAMAP*)plant;
    //child -> parent binding PTR
	l = new ParameterPlugin (this->plant);
    l->lightplugin_ptr = this;
	d = new DecompPlugin (this->plant);
    d->lightplugin_ptr = this;
	p = new PlantPlugin (this->plant);
    p->lightplugin_ptr = this;
	pp = new PlantDeletePlugin (this->plant);
    pp->lightplugin_ptr = this;

}

bool lightPlugin::initLightParam (const std::string& f) {
	ParamSet * param_H = new ParamSet(f);
	lightningModel = 1;

	if (!param_H->Exists())
		return false;

	if (param_H->getParameter("cat1Threshold"))
		cat1Threshold = param_H->getParameter("cat1Threshold")->value();
	else
	{
		cout << "Failed finding cat1Threshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cat2Threshold"))
		cat2Threshold = param_H->getParameter("cat2Threshold")->value();
	else
	{
		cout << "Failed finding cat2Threshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("livingThreshold"))
		livingThreshold = param_H->getParameter("livingThreshold")->value();
	else
	{
		cout << "Failed finding livingThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("ramificationThreshold"))
		ramificationThreshold = param_H->getParameter("ramificationThreshold")->value();
	else
	{
		cout << "Failed finding ramificationThreshold parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cat1Attenuation"))
		cat1Attenuation = param_H->getParameter("cat1Attenuation")->value();
	else
	{
		cout << "Failed finding cat1Attenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("cat2Attenuation"))
		cat2Attenuation = param_H->getParameter("cat2Attenuation")->value();
	else
	{
		cout << "Failed finding cat2Attenuation parameter in file " << f << endl;
		return false;
	}
	if (param_H->getParameter("lightningModel"))
		lightningModel = param_H->getParameter("lightningModel")->value();


	cout << "Parameters value" << endl;
	cout << "maximum phyage value for category one axis :                      " << cat1Threshold  << endl;
	cout << "maximum phyage value for category two axis :                      " << cat2Threshold  << endl;
	cout << "minimum light for life :                                          " << livingThreshold  << endl;
	cout << "minimum light for ramification :                                  " << ramificationThreshold  << endl;
	cout << "category 1 axis sensibility for light at ramification threshold : " << cat1Attenuation  << endl;
	cout << "category 2 axis sensibility for light at ramification threshold : " << cat2Attenuation  << endl;
	switch (lightningModel)
	{
	case 0 :
		cout << "isolated tree simulation" << endl;
		break;
	case 1 :
		cout << "canopy tree simulation" << endl;
		break;
	case 2 :
		cout << "MIR light simulation" << endl;
		break;
	}

	if (lightningModel == 2)
	{
		if (param_H->getParameter("clearnessIndex"))
			clearnessIndex = param_H->getParameter("clearnessIndex")->value();
		else
		{
			cout << "Failed finding clearnessIndex parameter in file " << f << endl;
			return false;
		}
		if (param_H->getParameter("latitude"))
		{
			latitude = param_H->getParameter("latitude")->value();
			latitude *= M_PI / 180.;
		}
		else
		{
			cout << "Failed finding latitude parameter in file " << f << endl;
			return false;
		}
		if (param_H->getParameter("beginingDay"))
			beginingDay = param_H->getParameter("beginingDay")->value();
		else
		{
			cout << "Failed finding beginingDay parameter in file " << f << endl;
			return false;
		}
		if (param_H->getParameter("endingDay"))
			endingDay = param_H->getParameter("endingDay")->value();
		else
		{
			cout << "Failed finding endingDay parameter in file " << f << endl;
			return false;
		}

		refLight = computeRefLight ();
	}

	return true;
}

void lightPlugin::computeInterception (float top_extern){
	switch (lightningModel)
	{
	case 0:
		computeInterceptionModelIsolated(top_extern);
		break;
	case 1:
		computeInterceptionModelCanopy(top_extern);
		break;
	case 2:
		computeInterceptionModelMIR(top_extern);
		break;
	}
}

void lightPlugin::computeInterceptionModelIsolated (float top_extern){
	LightData *L;
	GeomBranc *geom;
	GeomElem *g;
	Branc *b;
	float surfLeaf, sphereRadius=0, v;
	Vector3 refPoint, curPoint;

	// isolated tree : compute center and sphere diameter
	sphereRadius = computeCenterPoint (&refPoint);

	for (int br=0; br<plant->getGeometry().getGeomBrancStack().size(); br++)
	{
		geom = plant->getGeometry().getGeomBrancStack().at(br);
		b = geom->getBranc();

		if (b->getAdditionalData ("light") == 0)
			continue;

		g = geom->getElementsGeo().at(geom->getElementsGeo().size()-1);

		curPoint.set(g->getMatrix().getMainVector());
		curPoint *= g->getLength();
		curPoint +=(g->getMatrix().getTranslation());

		surfLeaf = 0;
		Hierarc *h = b->HierarcPtrs()[0];
		for (int i=0; i<h->borneNumber; i++){
			if (h->getBorne(i)->getAdditionalData ("light") == 0)
			{
				surfLeaf += 1;
			}
		}

		L = (LightData*)b->getAdditionalData ("light");

		curPoint -= refPoint;
		v = curPoint.normalize();
		L->v = v / sphereRadius;

	}
}

void lightPlugin::computeInterceptionModelCanopy (float top_extern){
	LightData *L;
	GeomBranc *geom;
	GeomElem *g;
	Branc *b;
	BrancAMAP *trunk;
	Branc *borne;
	float surfLeaf;
	Vector3 refPoint, curPoint;

	// find the upper point
	computeUpperPoint(&refPoint);

	for (int br=0; br<plant->getGeometry().getGeomBrancStack().size(); br++)
	{
		geom = plant->getGeometry().getGeomBrancStack().at(br);
		b = geom->getBranc();

		if (b->getAdditionalData ("light") == 0)
			continue;

		g = geom->getElementsGeo().at(geom->getElementsGeo().size()-1);

		curPoint.set(g->getMatrix().getMainVector());
		curPoint *= g->getLength();
		curPoint +=(g->getMatrix().getTranslation());

		surfLeaf = 0;
		Hierarc *h = b->HierarcPtrs()[0];
		for (int i=0; i<h->borneNumber; i++){
			if (h->getBorne(i)->getAdditionalData ("light") == 0)
			{
				surfLeaf += 1;
			}
		}

		L = (LightData*)b->getAdditionalData ("light");

		if (curPoint[2] < refPoint[2]/2.)
			L->v = 0;
		else
			L->v = 2. * (curPoint[2] - refPoint[2]/2.) / refPoint[2];
	}
}

void lightPlugin::computeInterceptionModelMIR(float top_extern)
{
	updateInterceptedLight (string(plant->getName()+".opf"));
}

// assuming upper point is at the top of the trunk
void lightPlugin::computeUpperPoint(Vector3 *v)
{
	GeomBranc *geom;
	GeomElem *g;
	BrancAMAP *trunk;

	(*v)[2] = 0;
	for (int br=0; br<plant->getGeometry().getGeomBrancStack().size(); br++)
	{
		geom = plant->getGeometry().getGeomBrancStack().at(br);
		trunk = (BrancAMAP*)geom->getBranc();
		if (trunk->getAdditionalData("light") == 0)
			continue;

		if (trunk->phyAgeInit == 1)
		{
			g = geom->getElementsGeo().at(geom->getElementsGeo().size()-1);
			(*v).set(g->getMatrix().getMainVector());
			(*v) *= g->getLength();
			(*v) +=(g->getMatrix().getTranslation());
			break;
		}
	}
}

float lightPlugin::computeCenterPoint (Vector3 *v)
{
	GeomBranc *geom;
	GeomElem *g;
	Branc *b;
	Vector3 curPoint;
	float f, sphereRadius=0;

	// assume center point is at half tree height
	computeUpperPoint (v);
	(*v) *= 0.5;

	for (int br=0; br<plant->getGeometry().getGeomBrancStack().size(); br++)
	{
		geom = plant->getGeometry().getGeomBrancStack().at(br);
		b = geom->getBranc();

		if (b->getAdditionalData ("light") == 0)
			continue;

		geom = plant->getGeometry().getGeomBrancStack().at(br);
		g = geom->getElementsGeo().at(geom->getElementsGeo().size()-1);

		curPoint.set(g->getMatrix().getMainVector());
		curPoint *= g->getLength();
		curPoint +=(g->getMatrix().getTranslation());
		curPoint -= (*v);
		f = curPoint.normalize();
		if (f > sphereRadius)
			sphereRadius = f;
	}

	return sphereRadius;
}

void lightPlugin::updateInterceptedLight (const std::string& f)
{
	ifstream fic (f.c_str(), ios_base::in);
	char line[50000];
	char *substr;
	int id, idBr, idBearer[100], curOrder=0;
	float l;
	BrancAMAP *bearer, *brLight[100];

	if (!fic.good())
	{
		perror (f.c_str());
		return;
	}

	BrancAMAP *b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
	LightData *light;
	while (b)
	{
		if ((light = (LightData*)b->getAdditionalData("light")) != NULL)
			light->v = 0;
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}

	curBranc = NULL;
	bool newlight = false;
	while (!fic.eof())
	{
		fic.getline (line, 50000);

		if ((substr=strstr (line, "<light>")) != NULL)
		{
			newlight = true;
		}
		else if (   newlight
				 && (substr=strstr (line, "topoId")) != NULL)
		{
			newlight = false;
			sscanf (&substr[7], "%d", &id);
			brLight[curOrder-1] = findBranc(id);
			curBranc = brLight[curOrder-1];
		}
		else if ((substr=strstr (line, "<branch")) != NULL)
		{
			brLight[curOrder] = NULL;
			curOrder++;
		}
		else if ((substr=strstr (line, "</branch")) != NULL)
		{
			brLight[curOrder] = NULL;
			int i = curOrder;
			while (brLight[i] == NULL)
				i--;
			curBranc = brLight[i];
			curOrder--;
		}
		else if ((substr=strstr (line, "MJ_m2>")) != NULL)
		{
			sscanf (&substr[6], "%f", &l);
			if (l > refLight)
				l = refLight;

			if ((light = (LightData*)curBranc->getAdditionalData("light")) != NULL)
			{
				if (light->v < l/refLight)
					light->v = l/refLight;
			}
		}
	}

	// update light for new born buds (with no internodes)
	b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
	while (b)
	{
		if (b->getTestNumberWithinBranch() == 0)
		{
			bearer = (BrancAMAP*)b->getCurrentHierarc()->getBearer()->getBranc();
			if ((light = (LightData*)bearer->getAdditionalData("light")) != NULL)
			{
				l = light->v;
				light = (LightData*)(b->getAdditionalData("light"));
				light->v = l;
			}
		}
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}

}

BrancAMAP* lightPlugin::findBranc(int id)
{
	BrancAMAP* b;
	b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::RESET);
	while (b)
	{
		if (b->getId() == id)
		{
			return b;
		}
		b = (BrancAMAP*)plant->getTopology().seekTree(TopoManager::NEXT);
	}
}

void lightPlugin::affectLight (int id, float l)
{

	DecompAxeLevel *d=NULL;

	if (curBranc != NULL)
		d = curBranc->getDecompAxeLevel (id);
	if (d == NULL)
		d = plant->getDecompAxeLevel (id);

	LightData *light;
	BrancAMAP *b;
	if ((light = (LightData*)d->getAdditionalData("light")) != NULL)
	{
		curBranc = (BrancAMAP*)d;
		if (light->v < l)
			light->v = l;
	}
	else
	{
		b = (BrancAMAP*)d->getParentOfLevel();
		b = (BrancAMAP*)b->getCurrentHierarc()->getBearer()->getBranc();
		if ((light = (LightData*)b->getAdditionalData("light")) != NULL)
		{
			curBranc = b;
			if (light->v < l)
				light->v = l;
		}
	}
}

float lightPlugin::extraTerrestrialDaily (int doy)
{
	float solarCste= 0.0820f;	// in MJ m-2 min-1 (<=> SolarCste= 1367 in J m-2 s-1)
	double doyAngle=  2 * M_PI * doy/365.;
	double declination= 0.409 * sin(doyAngle-1.39); // !!!
	double sun_earth= 1 + (0.033 * cos(doyAngle));		// inverse relative distance
	double sunSetHourAngle= SunSetHourAngle ((float) declination);

	double extra_rad;
	extra_rad  = sunSetHourAngle * sin(latitude) * sin(declination);
	extra_rad += cos(latitude)*cos(declination)*sin(sunSetHourAngle);
	extra_rad *= (24*60*solarCste*sun_earth/M_PI);

	return (float) extra_rad;
}

double lightPlugin::SunSetHourAngle (float declination)
{
	double sunSetHourAngle= -tan(latitude)*tan(declination);
	if (sunSetHourAngle < -1)
		sunSetHourAngle = -1;
	if (sunSetHourAngle > 1)
		sunSetHourAngle = 1;
	//sunSetHourAngle= max(-1, sunSetHourAngle);
	//sunSetHourAngle= min( 1, sunSetHourAngle);
	sunSetHourAngle= acos(sunSetHourAngle);

	return sunSetHourAngle;
}

float lightPlugin::computeRefLight()
{
	float refLight=0;

	for (int i=beginingDay; i<endingDay; i++)
	{
		refLight += extraTerrestrialDaily (i);
	}

	return refLight * clearnessIndex;
}

void lightPlugin::writeOPF(const std::string &finame)
{
	int i;

	std::stringstream sout;
	sout << finame;
	std::string fname (sout.str());

	GldsFormat *outFormat= new GldsFormat(fname);

	outFormat->initFormat(&(plant->getGeometry()));

	std::cout<<"Output Geometry opf to : "  << fname << ".opf" << std::endl;

	if (plant->getGeometry().getGeomBrancStack().size())
	{
		outFormat->printData(plant->getGeometry().getGeomBrancStack().at(0));
	}
	else
	{
		std::cout<<"WARNING : -------> Empty Geometry"<<std::endl;
	}

	((GldsFormat*)outFormat)->closeFormat();

	delete outFormat;
}


