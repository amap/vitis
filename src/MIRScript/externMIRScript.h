#if !defined STATIC_LIB

#ifndef __MIRSCRIPTDLL_H__
#define __MIRSCRIPTDLL_H__
	

#if !defined STATIC_LIB
	#if defined WIN32 

#pragma warning ( disable : 4251 )

			#ifdef MIRSCRIPT_EXPORTS
				#define MIRSCRIPT_EXPORT __declspec(dllexport)
			#else
				#define MIRSCRIPT_EXPORT __declspec(dllimport)
			#endif
			
			
	#else
			#define MIRSCRIPT_EXPORT
	#endif

#else 
		#define MIRSCRIPT_EXPORT
#endif



#endif

#else
	#define MIRSCRIPT_EXPORT 
#endif
 
#ifndef WIN32
#define HMODULE void*
#endif
