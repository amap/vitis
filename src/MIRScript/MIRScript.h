/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\file  MIRScript.h
///			\brief lugin to load the MIR part of Archimed.
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __MIRSCRIPT_H__
#define __MIRSCRIPT_H__
#include "externMIRScript.h"
#include "scheduler.h"
#include <string>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///
///			\class MIRScript
///			\brief Plugin to load the MIR part of Archimed
///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class MIRSCRIPT_EXPORT MIRScript : public VProcess
{


	protected:

		/// \brief The fullname for the MIR parameter file
		std::string MIRParamFileName;

		/// \brief The name of the input/output directory
		std::string sceneFileName;

		/// \brief The name of the input/output directory
		std::string simeoRootDirectory;

		/// \brief startTime
		float startTime;


	public:

		/// \brief MIRScript constructor
		/// \param nameP The name of the MIR parameter file
		/// \param nameD The fullname for the input/output directory
		MIRScript(const std::string & nameP, const std::string & nameD, const std::string & nameR, float st=1, bool process=true);

		/// \brief Plant constructor
		///
		/// Delete the manager of geometry and topology
		virtual ~MIRScript(void){};

		// \brief Set the fullname for the parameter file
		inline void setParamName(const std::string & str){ MIRParamFileName=str;}

		// \brief Set the fullname for the parameter file
		inline void setSceneFileName(const std::string & str){ sceneFileName=str;}

		// \brief Set the fullname for the parameter file
		inline void setSsimeoRootDirectory(const std::string & str){ simeoRootDirectory=str;}

		/// \return The fullname for the parameter file
		inline const std::string & getParamName () { return MIRParamFileName;}

		/// \return The fullname for the parameter file
		inline const std::string & getSceneFileName () { return sceneFileName;}

		/// \return The fullname for the parameter file
		inline const std::string & simeoDirectoryName () { return simeoRootDirectory;}

		/// \brief  init the plant
		virtual void init();

		void process_event(EventData *msg);
};


#endif
