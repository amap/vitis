/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
//#include <wait.h>
//#include <unistd.h>
#ifdef WIN32
#include <process.h>
#endif
#include "Plugin.h"
#include "MIRScript.h"

using namespace std;

MIRScript::MIRScript(const std::string & nameP, const std::string & nameD, const std::string & nameR, float st, bool process)
{

	MIRParamFileName=nameP;
	sceneFileName=nameD;
	simeoRootDirectory= nameR;
	startTime = st;
	if (process)
        init();
}



void MIRScript::init()
{	/*add init function*/
	Scheduler::instance()->create_process(this,NULL,startTime,0);
}


void MIRScript::process_event(EventData *msg)
{
	Scheduler *s = Scheduler::instance();

	std::cout << "MIR script at "<<s->getTopClock()<<std::endl;

	string sceneFile;
	string cmd;
	cmd.append ("cd ");
	cmd.append (simeoRootDirectory);
//	cmd.append ("d:\\workspace");
#ifdef WIN32
	cmd.append (" & simeo -p script jeeb.simeo.module.edito.scripts.ScriptTristan2012 ");
#else
	cmd.append (" ; sh simeo.sh -p script jeeb.simeo.module.edito.scripts.ScriptTristan2012 ");
#endif

	cmd.append (sceneFileName);
//	cmd.append ("d:/mir/tristan.ops");
	cmd.append(" ");
	cmd.append(MIRParamFileName);
//	cmd.append("d:/mir/archimed.config");

	system(cmd.c_str());

	/* sets the next external eventData */
	float top_extern = s->getTopClock();

	if (   s->getTopClock()+1 <= s->getHotStop()
	    && msg != NULL)
	{
		s->signal_event(this->getPid(),msg,s->getTopClock()+1,102);
	}
}
