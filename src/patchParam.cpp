#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "AxisReference.h"


void usage ()
{
	cout << "usage : patchParam inParamFile outParamFile paramIndex [PhyAge [pos]] val" << endl;
	cout << "       inParamFile	: path to the original Amapsim file" << endl;
	cout << "       outParamFile	: path to the output ApamSim file" << endl;
	cout << "       paramIndex	: index of parameter into AmapSim parameter set (see corresp.h file)" << endl;
	cout << "       phyAge		: physiological age where to put a value for 1D and 2D parameters" << endl;
	cout << "       pos		: in case of 2D parameter, position where to put a value" << endl;
	cout << "       val		: value to be put" << endl;
}

int main (int argc, char **argv)
{
	if (argc < 5)
	{
		usage ();
		exit(1);
	}
	
	string inParamFile(argv[1]), outParamFile(argv[2]);
	int paramIndex, phyAge, pos;
	float val;
	
	AxisReference axis(inParamFile);
	if (!axis.isSet())
	{
		cout << "***** unable to read " << inParamFile << "AmapSim parameter file" << endl;
		usage();
		exit(1);
	}
	
	paramIndex = atoi(argv[3]);
	phyAge = atoi(argv[4])-1;
	
	if (argc == 5)
	{
		val = atof(argv[4]);
		axis[paramIndex].ymin = val;
	}
	else if (argc == 6)
	{
		val = atof(argv[5]);
		axis.set1DParameter (paramIndex, phyAge, val);
		
	}
	else
	{
		pos = atoi(argv[5])-1;
		val = atof(argv[6]);
		axis.set2DParameter (paramIndex, phyAge, pos, val);
	}
	
	axis.setFileName (outParamFile);
	axis.writeAxis();
	
	exit(0);
}


