/***************************************************************************
  this file is part of AMAPsim software
  -------------------
begin                : 1995
copyright            : (C) 2018 by jef
email                : barczi@cirad.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU Lesser General Public License as        *
 *   published by the Free Software Foundation; either version 3 of the    *
 *   License, or (at your option) any later version.                       *
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>
#include <string.h>
#include "Output.h"
#include "RootSystem.h"
#include "tinyxml.h"
#include "Root.h"


OutputManager outpMan;

void usage(void)
{
	printf ("usage : gaspp plant age [seed] [-sGeomSimplification] [-STopoSimplification [-deltaDelta]] [-g[step]] [-gl[step]] [-a[step]] [-oOutputFileName] [-cModule [-eModuleData]] [[-fsstep] -foCustomOutputFormat]\n");
	printf ("        plant : name of the plant (if single name, .cfg file will be used for parameter file location)\n");
	printf ("        age : age to be computed\n");
	printf ("        seed : random number initialisation number\n");
	printf ("        -s : GeomSimplification is the simplification level on geometrical output (from 0 to 4)\n");
	printf ("        -S : TopoSimplification is the simplification level on topological computing (from 0 to 3)\n");
	printf ("        -delta : birth date threshold to build simplified classes\n");
	printf ("        step : output step (only at simulation end by default)\n");
	printf ("        -o : output to a particular OutputFileName file (without extension), this MUST be a complete path/filename\n");
	printf ("        -g : output linetree format\n");
	printf ("        -gl : output opf format\n");
	printf ("        -a : output mtg format\n");
	printf ("        -n : output natFX format\n");
	printf ("        -c : dynamicaly link the module file Module to the simulation kernel\n");
	printf ("        -e : provide the current Module with ModuleData as private data configuration file\n");
	printf ("        -swap : change bytes output order (switch between little and big endian\n");
	printf ("        -fo : load a custom output format module\n");
	printf ("        -fs : custom output step\n");
#ifdef WIN32
	Sleep(10000);

#endif
	exit (0);
}

void read_arguments(long  nbarg, char **arguments, RootSystem & plt)
{
	bool trouve, outplOk=false, outpglOk=false, outpmOk=false, outpcOk=false, outpnfxOk=false, swap_bytes=false;;
	long  i;
	float f;
	string nameOutput, outpcName;
	float step_LineTree=0;
	float step_Natfx=0;
	float step_AmapMod=0;
	float step_Custom=0;
	float step_Glds=0;

	outpcName.empty();
	nameOutput.empty();

	if(nbarg<=1)
	{
		usage();
	}
	else
	{

		do
		{
			trouve=0;


			if (strncmp (arguments[nbarg-1], "-o", 2) == 0)
			{
				nameOutput=&arguments[nbarg-1][2];
				nbarg --;
				trouve = true;
			}



			if (strncmp (arguments[nbarg-1], "-gl", 3) == 0)
			{
			    outpglOk = true;
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][3], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][3], "%f", &f) > 0)
#endif
				{
					step_Glds = f;
				}
				nbarg --;
				trouve = 1;
			}
			if (strncmp (arguments[nbarg-1], "-swap", 5) == 0)
			{

				swap_bytes = true;
				nbarg --;
				trouve = 1;
			}
			if (strncmp (arguments[nbarg-1], "-g", 2) == 0)
			{
			    outplOk = true;
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][2], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%f", &f) > 0)
#endif
				{
					step_LineTree = f;
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-a", 2) == 0)
			{
			    outpmOk = true;
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][2], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%f", &f) > 0)
#endif
				{
					step_AmapMod = f;
				}
				nbarg --;
				trouve = 1;
			}



			if (strncmp (arguments[nbarg-1], "-n", 2) == 0)
			{
			    outpnfxOk = true;
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][2], "%f", &f) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%f", &f) > 0)
#endif
				{
					step_Natfx = f;
				}
				nbarg --;
				trouve = 1;
			}



			if (strncmp (arguments[nbarg-1], "-s", 2) == 0)
			{
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][2], "%ld", &i) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%ld", &i) > 0)
#endif
				{
					if (   i >= 0
						&& i < 5)
						plt.getConfigData().cfg_geoSimp = (USint)i;
					else if (i >= 5)
					{
						plt.getConfigData().cfg_geoSimp = 1;
						plt.getConfigData().cfg_supersimplif = (USint)i;
					}
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-S", 2) == 0)
			{
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][2], "%ld", &i) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%ld", &i) > 0)
#endif
				{
					if (i < 0)
						i = 0;
					if (i > 3)
						i = 3;
					plt.getConfigData().cfg_topoSimp = (USint)i;
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-delta", 6) == 0)
			{
#ifdef WIN32
				if (sscanf_s (&arguments[nbarg-1][6], "%ld", &i) > 0)
#else
				if (sscanf (&arguments[nbarg-1][2], "%ld", &i) > 0)
#endif
				{
					plt.getConfigData().cfg_birthDateThresholdTopoSimp = (float)i;
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-fo", 3) == 0)
			{
			    outpcOk = true;
				outpcName = &arguments[nbarg-1][3];
				nbarg --;
				trouve = 1;
				if (strncmp (arguments[nbarg-1], "-fs", 3) == 0)
				{

#ifdef WIN32
					if (sscanf_s (&arguments[nbarg-1][2], "%f", &f) > 0)
#else
					if (sscanf (&arguments[nbarg-1][2], "%f", &f) > 0)
#endif
					{
						step_Custom= f;
					}
					nbarg --;
					trouve = 1;
				}
			}

			if (strncmp (arguments[nbarg-1], "-e", 2) == 0)
			{
				if (strncmp (arguments[nbarg-2], "-c", 2) == 0)
				{
					plt.getConfigData().addExternalModule (&arguments[nbarg-2][2], &arguments[nbarg-1][2]);
					nbarg -=2;
					trouve = 1;
				}
				else
				{
//					plt.getConfigData().addExternalModule (&arguments[nbarg-1][2]);
					nbarg --;
					trouve = 1;
				}
			}

			if (strncmp (arguments[nbarg-1], "-c", 2) == 0)
			{
				plt.getConfigData().addExternalModule (&arguments[nbarg-1][2]);
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-h", 2) == 0)
			{
				usage ();
			}

		} while (trouve);


		if (nbarg > 2)
		{
			string acro (arguments[1]);

			plt.setParamName(string("DigRMod"), acro);
			plt.setHotStop(atof(arguments[2]));
			nbarg -= 2;

			if (nbarg > 1)
			{
				plt.setSeed((USint)atoi(arguments[3]));
			}
		}
		else
		{
			usage ();

		}

		if (nameOutput.size() == 0)
			nameOutput = "amapsim";

		plt.setName(nameOutput);

		if(!step_LineTree)
			step_LineTree=(float)Scheduler::instance()->getHotStop();
		if(!step_Natfx)
			step_Natfx=(float)Scheduler::instance()->getHotStop();
		if(!step_AmapMod)
			step_AmapMod=(float)Scheduler::instance()->getHotStop();
		if(!step_Glds)
			step_Glds=(float)Scheduler::instance()->getHotStop();
		if(!step_Custom)
			step_Custom=(float)Scheduler::instance()->getHotStop();
		if(outplOk)
		{
#ifndef WIN32
			outpMan.addOutputDevice("libOutputLineTreeAmapsim.so");
#else
			outpMan.addOutputDevice("OutputLineTreeAmapsim.dll");
#endif
			outpMan.create_and_schedule_output(&plt,outpMan.getNumberOfOutputDevice()-1,nameOutput,step_LineTree,swap_bytes);
		}
		if(outpglOk)
		{
#ifndef WIN32
			outpMan.addOutputDevice("libOutputOpf.so");
#else
			outpMan.addOutputDevice("OutputRawOpf.dll");
#endif
			outpMan.create_and_schedule_output(&plt,outpMan.getNumberOfOutputDevice()-1,nameOutput,step_Glds);
		}
		if(outpmOk)
		{
#ifndef WIN32
			outpMan.addOutputDevice("libOutputMtgAmapsim.so");
#else
			outpMan.addOutputDevice("OutputMtgAmapsim.dll");
#endif
			outpMan.create_and_schedule_output(&plt,outpMan.getNumberOfOutputDevice()-1,nameOutput,step_AmapMod);
		}
		if(outpcOk)
		{
			outpMan.addOutputDevice(outpcName);
			outpMan.create_and_schedule_output(&plt,outpMan.getNumberOfOutputDevice()-1,nameOutput,step_Custom);
		}
		if(outpnfxOk)
		{
#ifndef WIN32
			outpMan.addOutputDevice("libOutputTreeNatfx.so");
#else
			outpMan.addOutputDevice("OutputLineTreeNatfx.dll");
#endif
			outpMan.create_and_schedule_output(&plt,outpMan.getNumberOfOutputDevice()-1,nameOutput,step_Natfx);
		}
	}
}

int main ( int argc, char **argv)
{

	RootSystem plt(0);

//cout << "debut" << endl;

	read_arguments(argc, argv, plt);

//cout << "avant init" << endl;

	plt.init();

//cout << "avant run" << endl;

	plt.run();

//cout << "avant simu" << endl;

	Scheduler::instance()->run_simulation();

//cout << "apres simu" << endl;


	exit(1);
}

