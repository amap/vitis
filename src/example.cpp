#include "Output.h"
#include "PlantExample.h"


void read_arguments(long  nbarg, char **arguments, Plant & plt);
/*
	struct Toto:public subscriber<messageAxeRef>
	{

	Toto () {subscribe();};
	~Toto () {unsubscribe();};
	void on_notify (const state& st, paramMsg * data=NULL) {
	paramAxisRef *p = (paramAxisRef *)data;
	switch (st)
	{
	case Val1DParam :
	printf ("%d %f\n",p->numVar, p->var);
	break;
	case Val2DParam :
	printf ("plus complique\n");
	break;
	default:
	printf ("inconnu\n");
	break;
	};
	};
	};*/


int main ( int argc, char **argv)
{

	PlantExample plt;
	//	read_arguments(argc, argv, plt);
	read_arguments(argc, argv, plt);

	//	Toto toto;

	plt.run();


	return 1;
}

void usage(void)
{
}

void read_arguments(long  nbarg, char **arguments, Plant & plt)
{
	bool trouve;
	long  i;
	float f;
	char buffer[256];
	float step_geometry=0;
	static OutputManager outpMan;
	static OutputManager outpMang;
	static OutputManager outpManm;
	Output * outpl=NULL;
	Output * outpg=NULL;
	Output * outpm=NULL;


	if(nbarg<=1)
	{
		usage();
	}
	else
	{
#ifndef WIN32
		outpMan.addOutputDevice("libOutputLineTree.0.0.0");
#else
		outpMan.addOutputDevice("OutputLineTree.dll");
#endif

		do
		{


			trouve=0;




			if (strncmp (arguments[nbarg-1], "-o", 2) == 0)
			{
				strcpy (buffer, &arguments[nbarg-1][2]);
				nbarg --;
				trouve = true;
				outpl=outpMan.create_output(&plt,0,to_string(buffer));
			}





			if (strncmp (arguments[nbarg-1], "-g", 2) == 0)
			{
				if (sscanf (&arguments[nbarg-1][2], "%f", &f) > 0)
				{
					step_geometry = f;

				}
				nbarg --;
				trouve = 1;
			}



			if (strncmp (arguments[nbarg-1], "-h", 2) == 0)
			{
				usage ();
			}



		} while (trouve);



		if (nbarg >= 2)
		{
			std::string acro (arguments[1]);
			plt.setName(acro);
			plt.setParamName(acro);
			plt.setHotStop(atof(arguments[2]));
			nbarg -= 2;

			if (nbarg > 1)
			{
				plt.setSeed(atoi(arguments[3]));
			}
		}
		else
		{
			usage ();

		}


		if(!step_geometry)
			step_geometry=plt.getScheduler().getHotStop();
		if(outpl)
		{
			outpMan.scheduler_output(&plt,outpl,step_geometry);
		}

		plt.getConfigData().cfg_supersimplif=0;
		plt.setHotStart(0);

		plt.init();
	}//end else

}



