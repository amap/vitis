#include "Output.h"
#include "PlantAMAP.h"
#include "ObserverManager.h"
#include "single.h"

//#include "AmapSimModSignalInterface.h"
#include "Notifier.h"
#include "AxisReference.h"
/// AXISREF
enum messageAxeRef {Val1DParam, Val2DParam };
//message param
struct paramAxisRef:public paramMsg
{
	int numVar;
	float var;
	BrancAMAP *b;

	paramAxisRef (int nv, float v, BrancAMAP *bb) { numVar=nv; var=v; b=bb;}
};

void read_arguments(long  nbarg, char **arguments, Plant & plt);

//notifier<messageAxeRef> &n = notifier<messageAxeRef>::instance();
//
//	struct Toto:public subscriber<messageAxeRef>
//	{
//	Toto () { subscribe();};
//	~Toto () {unsubscribe();};
//	void on_notify (const state& st, paramMsg * data=NULL) {
//	paramAxisRef *p = (paramAxisRef *)data;
//	switch (st)
//	{
//	case Val1DParam :
//	printf ("%d %f\n",p->numVar, p->var);
//	break;
//	case Val2DParam :
//	printf ("plus complique\n");
//	break;
//	default:
//	printf ("inconnu\n");
//	break;
//	};
//	};
//	};
//
//    Toto t;
template <> int* single<int>::sglt = NULL;
int *s=single<int>::instance();

int main ( int argc, char **argv)
{

	PlantAMAP plt;
	read_arguments(argc, argv, plt);

#ifndef WIN32
	plt.getConfigData().addExternalModule ("libroot.0.0.0");
#else
//	plt.getConfigData().addExternalModule ("d:/lib&dll/rootPlugin.dll");
#endif

	plt.run();

	return 1;
}

void usage(void)
{
	printf ("usage : gaspp plant age [seed] [-sGeomSimplification] [-STopoSimplification] [-g[step]] [-oOutputFileName]\n");
	printf ("        plant : name of the plant (if single name, .cfg file will be used for parameter file location)\n");
	printf ("        age : age to be computed\n");
	printf ("        seed : random number initialisation number\n");
	printf ("        -s : GeomSimplification is the simplification level on geometrical output (from 0 to 4)\n");
	printf ("        -S : TopoSimplification is the simplification level on topological computing (from 0 to 3)\n");
	printf ("        step : output step (only at the end by default)\n");
	printf ("        -o : output to a particular OutputFileName file (without extension), this MUST be a complete path/filename\n");
	exit (0);
}

void read_arguments(long  nbarg, char **arguments, Plant & plt)
{
	bool trouve;
	long  i;
	float f;
	char buffer[256];
	float step_geometry=0;
	static OutputManager outpMan;
	static OutputManager outpMang;
	static OutputManager outpManm;
	Output * outpl=NULL;
	Output * outpg=NULL;
	Output * outpm=NULL;


	if(nbarg<=1)
	{
		usage();
	}
	else
	{
#ifndef WIN32
//		outpMang.addOutputDevice("libOutputGlds.0.0.0");
//		outpMan.addOutputDevice("libOutputLineTree.0.0.0");
		outpMan.addOutputDevice("libOutputLineTreeAmapsim.0.0.0");
//		outpManm.addOutputDevice("libOutputMtg.0.0.0");
//		outpManm.addOutputDevice("libOutputMtgAmapsim.0.0.0");
#else
//		outpMan.addOutputDevice("OutputLineTreeNatfx.dll");
//		outpManm.addOutputDevice("OutputLineTreeAmapSim.dll");
//		outpManm.addOutputDevice("OutputLineTree.dll");
//		outpManm.addOutputDevice("OutputMtgAmapsim.dll");
//		outpMang.addOutputDevice("OutputGlds.dll");
#endif

		do
		{


			trouve=0;




			if (strncmp (arguments[nbarg-1], "-o", 2) == 0)
			{
				strcpy (buffer, &arguments[nbarg-1][2]);
				nbarg --;
				trouve = true;
//				outpl=outpManm.create_output(&plt,0,to_string(buffer));
//				outpg=outpMang.create_output(&plt,0,to_string(buffer));
//`				outpm=outpManm.create_output(&plt,0,to_string(buffer));
			}





			if (strncmp (arguments[nbarg-1], "-g", 2) == 0)
			{
				if (sscanf (&arguments[nbarg-1][2], "%f", &f) > 0)
				{
					step_geometry = f;

				}
				nbarg --;
				trouve = 1;
			}



			if (strncmp (arguments[nbarg-1], "-s", 2) == 0)
			{
				if (sscanf (&arguments[nbarg-1][2], "%ld", &i) > 0)
				{
					if (   i >= 0
							&& i < 5)
						plt.getConfigData().cfg_geoSimp = i;
				}
				nbarg --;
				trouve = 1;
			}

			if (strncmp (arguments[nbarg-1], "-S", 2) == 0)
			{
				if (sscanf (&arguments[nbarg-1][2], "%ld", &i) > 0)
				{
					if (i < 0)
						i = 0;
					if (i > 3)
						i = 3;
					plt.getConfigData().cfg_topoSimp = i;
				}
				nbarg --;
				trouve = 1;
			}


			if (strncmp (arguments[nbarg-1], "-h", 2) == 0)
			{
				usage ();
			}



		} while (trouve);



		if (nbarg >= 2)
		{
			string acro (arguments[1]);
			plt.setName(acro);
			plt.setParamName(acro);
			plt.setHotStop(atof(arguments[2]));
			nbarg -= 2;

			if (nbarg >= 1)
			{
				plt.setSeed(atoi(arguments[3]));
			}
		}
		else
		{
			usage ();

		}


		if(!step_geometry)
			step_geometry=plt.getScheduler().getHotStop();
		if(outpl)
		{
			outpMan.scheduler_output(&plt,outpl,step_geometry);
		}
		if(outpg)
		{
			outpMang.scheduler_output(&plt,outpg,step_geometry);
		}
		if(outpm)
		{
			outpManm.scheduler_output(&plt,outpm,step_geometry);
		}

		plt.getConfigData().cfg_supersimplif=10;
		plt.setHotStart(0);

		plt.init();
	}//end else

}



